package com.jksmarthome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.jksmarthome.R;
import com.jksmarthome.entity.ImageOrVideoInfo;
import java.util.List;

public class ImageOrVideoAdapter extends BaseAdapter {

    private List<ImageOrVideoInfo> list;
    private LayoutInflater mInflater;
    private InnerItemOnclickListener mListener;

    public ImageOrVideoAdapter(Context context, List<ImageOrVideoInfo> list) {
        this.list = list;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ImageOrVideoInfo info = list.get(position);
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.gridview_item, null);
            viewHolder = new ViewHolder();
            viewHolder.image = (ImageView) convertView.findViewById(R.id.img);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Glide.with(parent.getContext())
                .load(info.img)
                .placeholder(R.color.common_bg)
                .error(R.drawable.event_list_fail_pic)
                .into(viewHolder.image);

        viewHolder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.itemClick(info);
            }
        });

        return convertView;
    }

    static class ViewHolder {
        public ImageView image;
    }

    public interface InnerItemOnclickListener {
        void itemClick(ImageOrVideoInfo info);
    }

    public void setOnInnerItemOnClickListener(InnerItemOnclickListener listener){
        this.mListener=listener;
    }

}

package com.jksmarthome.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jksmarthome.R;

import java.io.File;
import java.util.List;

public class GirdAdapter extends BaseAdapter {

    private List<String> paths;
    private Context ctx;
    private LayoutInflater mInflater;

    public GirdAdapter(Context context, List<String> paths) {
        ctx = context;
        this.paths = paths;
        mInflater = LayoutInflater.from(context);
    }

    public void addItem(String path){
        if(paths != null){
            paths.add(path);
        }
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return paths.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return paths.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        String path = paths.get(position);
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.gridview_item, null);
            viewHolder = new ViewHolder();
            viewHolder.image = (ImageView) convertView.findViewById(R.id.img);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        File file = new File(path);
        Bitmap bm = BitmapFactory.decodeFile(path,
                getHeapOpts(file));
        viewHolder.image.setImageBitmap(bm);
        return convertView;
    }

    static class ViewHolder {
        public ImageView image;
    }


    // 图片加载的缓存工具类，安卓自带的方法
    public static BitmapFactory.Options getHeapOpts(File file) {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        // 数字越大读出的图片占用的heap必须越小，不然总是溢出
        if (file.length() < 20480) { // 0-20k
            opts.inSampleSize = 1;// 这里意为缩放的大小 ，数字越多缩放得越厉害
        } else if (file.length() < 51200) { // 20-50k
            opts.inSampleSize = 2;
        } else if (file.length() < 307200) { // 50-300k
            opts.inSampleSize = 4;
        } else if (file.length() < 819200) { // 300-800k
            opts.inSampleSize = 6;
        } else if (file.length() < 1048576) { // 800-1024k
            opts.inSampleSize = 8;
        } else {
            opts.inSampleSize = 10;
        }
        return opts;
    }
}

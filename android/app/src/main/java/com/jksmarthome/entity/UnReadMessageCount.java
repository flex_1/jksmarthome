package com.jksmarthome.entity;

import com.google.gson.annotations.SerializedName;

public class UnReadMessageCount {
    @SerializedName("code")
    public int code;
    @SerializedName("msg")
    public String msg;
    @SerializedName("result")
    public int result;

    @Override
    public String toString() {
        return "UnReadMessageCount{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", result=" + result +
                '}';
    }
}

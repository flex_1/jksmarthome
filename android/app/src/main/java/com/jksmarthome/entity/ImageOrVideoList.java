package com.jksmarthome.entity;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

public class ImageOrVideoList {

    @SerializedName("totalPageNum")
    public int totalPageNum; //总页数

    @SerializedName("totalNum")
    public int totalNum; //总条数

    @SerializedName("curPage")
    public int curPage; //当前页

    @SerializedName("list")
    public ImageOrVideoInfo[] list;

    @Override
    public String toString() {
        return "ImageOrVideoList{" +
                "totalPageNum=" + totalPageNum +
                ", totalNum=" + totalNum +
                ", curPage=" + curPage +
                ", list=" + Arrays.toString(list) +
                '}';
    }
}

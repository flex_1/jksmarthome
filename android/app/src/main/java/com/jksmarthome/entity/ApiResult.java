package com.jksmarthome.entity;

import com.google.gson.annotations.SerializedName;

public class ApiResult {
    @SerializedName("code")
    public int code;
    @SerializedName("msg")
    public String msg;
    @SerializedName("result")
    public String result;

    @Override
    public String toString() {
        return "UnReadMessageCount{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", result=" + result +
                '}';
    }
}

package com.jksmarthome.entity;

import com.google.gson.annotations.SerializedName;

public class MessageInfo {
    @SerializedName("id")
    public int id;

    @SerializedName("createTime")
    public long createTime;

    @SerializedName("modifyTime")
    public long modifyTime;

    @SerializedName("devSerial")
    public String devSerial;

    @SerializedName("alarmId")
    public String alarmId;

    @SerializedName("alarmStartTime")
    public String alarmStartTime;

    @SerializedName("alarmType")
    public int alarmType;

    @SerializedName("url")
    public String url;

    @SerializedName("isRead")
    public int isRead;

    @SerializedName("channel")
    public int channel;

    @SerializedName("channelName")
    public String channelName;

    @SerializedName("crypt")
    public int crypt;

    @SerializedName("recState")
    public int recState;

    @SerializedName("delayTime")
    public int delayTime;

    @SerializedName("preTime")
    public int preTime;

    @SerializedName("deviceName")
    public String deviceName;

    @SerializedName("category")
    public String category;

    @SerializedName("customType")
    public String customType;

    @SerializedName("customInfo")
    public String customInfo;

    @Override
    public String toString() {
        return "MessageInfo{" +
                "id=" + id +
                ", createTime=" + createTime +
                ", modifyTime=" + modifyTime +
                ", devSerial='" + devSerial + '\'' +
                ", alarmId='" + alarmId + '\'' +
                ", alarmStartTime='" + alarmStartTime + '\'' +
                ", alarmType='" + alarmType + '\'' +
                ", url='" + url + '\'' +
                ", isRead=" + isRead +
                ", channel=" + channel +
                ", channelName='" + channelName + '\'' +
                ", crypt=" + crypt +
                ", recState=" + recState +
                ", delayTime=" + delayTime +
                ", preTime=" + preTime +
                ", deviceName='" + deviceName + '\'' +
                ", category='" + category + '\'' +
                ", customType='" + customType + '\'' +
                ", customInfo='" + customInfo + '\'' +
                '}';
    }
}

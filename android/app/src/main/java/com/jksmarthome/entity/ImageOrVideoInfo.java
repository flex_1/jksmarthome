package com.jksmarthome.entity;

import com.google.gson.annotations.SerializedName;

public class ImageOrVideoInfo {

    @SerializedName("id")
    public int id;

    @SerializedName("createTime")
    public long createTime;

    @SerializedName("modifyTime")
    public long modifyTime;

    @SerializedName("img")
    public String img;

    @SerializedName("video")
    public String video;

    @SerializedName("floor")
    public String floor;

    @SerializedName("type")
    public int type;

    @SerializedName("roomName")
    public String roomName;

    @SerializedName("taccountId")
    public int taccountId;

    @SerializedName("tdeviceId")
    public int tdeviceId;

    @SerializedName("thouseId")
    public int thouseId;

    @SerializedName("troomId")
    public int troomId;

    @SerializedName("size")
    public int size;

    @SerializedName("time")
    public String time;

    @SerializedName("deviceName")
    public String deviceName;

    @SerializedName("duration")
    public String duration;

    @Override
    public String toString() {
        return "ImageOrVideoInfo{" +
                "id=" + id +
                ", createTime=" + createTime +
                ", modifyTime=" + modifyTime +
                ", img='" + img + '\'' +
                ", video='" + video + '\'' +
                ", floor='" + floor + '\'' +
                ", type=" + type +
                ", roomName='" + roomName + '\'' +
                ", taccountId=" + taccountId +
                ", tdeviceId=" + tdeviceId +
                ", thouseId=" + thouseId +
                ", troomId=" + troomId +
                ", size=" + size +
                ", time='" + time + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", duration='" + duration + '\'' +
                '}';
    }
}

package com.jksmarthome.entity;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

public class MessageList {

    @SerializedName("totalPageNum")
    public int totalPageNum;

    @SerializedName("curPage")
    public int curPage;

    @SerializedName("totalNum")
    public int totalNum;

    @SerializedName("list")
    public MessageInfo[] list;

    @Override
    public String toString() {
        return "MessageList{" +
                "totalPageNum=" + totalPageNum +
                ", curPage=" + curPage +
                ", totalNum=" + totalNum +
                ", list=" + Arrays.toString(list) +
                '}';
    }
}

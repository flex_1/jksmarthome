package com.jksmarthome.wificonnect;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import com.espressif.iot.esptouch.EsptouchTask;
import com.espressif.iot.esptouch.IEsptouchResult;
import com.espressif.iot.esptouch.IEsptouchTask;
import com.espressif.iot.esptouch.util.ByteUtil;
import com.espressif.iot.esptouch.util.TouchNetUtil;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.annotation.Nonnull;

/**
 * WiFi配网
 */
public class WifiConfigureModule extends ReactContextBaseJavaModule {

    private static final String TAG = "WifiConfigureModule_Log";

    private final static String MODULE_NAME = "JKConnectWifiUtils";

    private final static String WifiConnectSuccess = "WifiConnectSuccess";

    private final static String WifiConnectFail = "WifiConnectFail";

    public static ReactContext mReactContext;

    private EsptouchAsyncTask4 mTask;

    public WifiConfigureModule(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
        mReactContext = reactContext;
    }

    @Nonnull
    @Override
    public String getName() {
        return MODULE_NAME;
    }

    @ReactMethod
    public void gotoSystemWifiList() {
        if (getCurrentActivity() != null) {
            Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
            getCurrentActivity().startActivity(intent);
        }
    }

    @ReactMethod
    public void startConnectWifi(String ssid, String bssid, String password) {
        Log.e(TAG, "startConnectWifi：" + ssid + " , " + bssid + " , " + password);
        if (getCurrentActivity() != null) {
                //已连接的是该WiFi，进行配网
                Log.e(TAG, "wifi已经连接, 配网中。。。。");
            executeEsptouch(ssid, bssid, password, getCurrentActivity());
        }
    }

    @ReactMethod
    public void stopConnectWifi() {
        if (mTask != null) {
            Log.e(TAG, "取消配网");
            mTask.cancelEsptouch();
        }
    }

    private void executeEsptouch(String mSsid, String mBssid, String mPassword, Activity activity) {
        byte[] ssid = ByteUtil.getBytesByString(mSsid);
        byte[] password = mPassword == null ? null : ByteUtil.getBytesByString(mPassword);
        byte[] bssid = TouchNetUtil.parseBssid2bytes(mBssid);
        byte[] deviceCount = new byte[0];
        byte[] broadcast = {(byte) 1};

        if (mTask != null) {
            mTask.cancelEsptouch();
        }
        mTask = new EsptouchAsyncTask4(activity);
        mTask.execute(ssid, bssid, password, deviceCount, broadcast);
    }

    private static class EsptouchAsyncTask4 extends AsyncTask<byte[], IEsptouchResult, List<IEsptouchResult>> {
        private final WeakReference<Activity> mActivity;

        private final Object mLock = new Object();
        private AlertDialog mResultDialog;
        private IEsptouchTask mEsptouchTask;

        EsptouchAsyncTask4(Activity activity) {
            mActivity = new WeakReference<>(activity);
        }

        void cancelEsptouch() {
            cancel(true);
            if (mEsptouchTask != null) {
                mEsptouchTask.interrupt();
            }
        }

        @Override
        protected void onPreExecute() {
            Log.e(TAG, "onPreExecute");
        }

        @Override
        protected void onProgressUpdate(IEsptouchResult... values) {
            IEsptouchResult result = values[0];
            Log.e(TAG, "EspTouchResult: " + result);
            if (result != null) {
                WritableMap connectEvent = Arguments.createMap();
                //传递的参数
                connectEvent.putString("bssid", result.getBssid());
                connectEvent.putString("address", result.getInetAddress().getHostAddress());
                mReactContext
                        .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                        .emit(WifiConnectSuccess, connectEvent);
            }
        }

        @Override
        protected List<IEsptouchResult> doInBackground(byte[]... params) {
            Log.e(TAG, "doInBackground");
            int taskResultCount;
            synchronized (mLock) {
                byte[] apSsid = params[0];
                byte[] apBssid = params[1];
                byte[] apPassword = params[2];
                byte[] deviceCountData = params[3];
                byte[] broadcastData = params[4];
                taskResultCount = deviceCountData.length == 0 ? -1 : Integer.parseInt(new String(deviceCountData));
                Context context = mReactContext;
                mEsptouchTask = new EsptouchTask(apSsid, apBssid, apPassword, context);
                mEsptouchTask.setPackageBroadcast(broadcastData[0] == 1);
                mEsptouchTask.setEsptouchListener(this::publishProgress);
            }
            return mEsptouchTask.executeForResults(taskResultCount);
        }

        @Override
        protected void onPostExecute(List<IEsptouchResult> result) {
            Log.e(TAG, "onPostExecute");
            if (result == null) {
                Log.e(TAG, "建立 EspTouch 任务失败, 端口可能被其他程序占用");
                sendFailEvent("建立 EspTouch 任务失败, 端口可能被其他程序占用");
                return;
            }

            // check whether the task is cancelled and no results received
            IEsptouchResult firstResult = result.get(0);
            if (firstResult.isCancelled()) {
                return;
            }
            // the task received some results including cancelled while
            // executing before receiving enough results

            if (!firstResult.isSuc()) {
                Log.e(TAG, "EspTouch 配网失败");
                sendFailEvent("EspTouch 配网失败");
                return;
            }

//            ArrayList<CharSequence> resultMsgList = new ArrayList<>(result.size());
//            String bssid = "";
//            String address = "";
//            for (IEsptouchResult touchResult : result) {
//                bssid = touchResult.getBssid();
//                address = touchResult.getInetAddress().getHostAddress();
//                String message = "BSSID: " + touchResult.getBssid() + ", 地址: " + touchResult.getInetAddress().getHostAddress();
//                resultMsgList.add(message);
//            }
//            CharSequence[] items = new CharSequence[resultMsgList.size()];

//            Log.e(TAG, "EspTouch 完成, " + bssid + " , " + address);

//            WritableMap connectEvent = Arguments.createMap();
//            //传递的参数
//            connectEvent.putString("bssid", bssid);
//            connectEvent.putString("address", address);
//            mReactContext
//                    .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
//                    .emit(WifiConnectSuccess, connectEvent);
        }
    }

    private static void sendFailEvent(String reason) {
        WritableMap connectEvent = Arguments.createMap();
        //传递的参数
        connectEvent.putString("reason", reason);
        mReactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(WifiConnectFail, connectEvent);
    }

}

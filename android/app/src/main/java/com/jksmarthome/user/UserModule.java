package com.jksmarthome.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import javax.annotation.Nonnull;

import static com.jksmarthome.utils.Constants.DATA_NAME_USER_TOKEN;
import static com.jksmarthome.utils.Constants.USER_TOKEN;

/**
 * 用户信息相关
 */
public class UserModule extends ReactContextBaseJavaModule {

    private final static String USER_MODULE_NAME = "UserModule";

    public UserModule(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Nonnull
    @Override
    public String getName() {
        return USER_MODULE_NAME;
    }

    /**
     * 检查本地token信息
     * （做2.6及之前版本的兼容）
     * 本地是否有token，没有：存起来，有：跳过
     */
    @ReactMethod
    public void checkLocalUserToken(String token) {
        try {
            SharedPreferences sharedPreferences = getReactApplicationContext().getSharedPreferences(DATA_NAME_USER_TOKEN, Context .MODE_PRIVATE);
            String userToken = sharedPreferences.getString(USER_TOKEN,"");

            if (!TextUtils.isEmpty(userToken)) {
                return ;
            }

            this.setUserToken(token);

        } catch (Exception e) {

        }
    }

    /**
     * 写入数据
     * token
     */
    @ReactMethod
    public void setUserToken(String token) {
        try {
            SharedPreferences sharedPreferences = getReactApplicationContext().getSharedPreferences(DATA_NAME_USER_TOKEN, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(USER_TOKEN, token);
            editor.commit();
        } catch (Exception e) {

        }
    }

    /**
     * 清空数据
     * token
     */
    @ReactMethod
    public void clearUserToken() {
        try {
            SharedPreferences sharedPreferences = getReactApplicationContext().getSharedPreferences(DATA_NAME_USER_TOKEN, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.commit();
        } catch (Exception e) {

        }
    }

}

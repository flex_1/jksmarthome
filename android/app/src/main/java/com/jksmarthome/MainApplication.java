package com.jksmarthome;

import android.content.Context;

import androidx.multidex.MultiDex;


import com.facebook.react.ReactApplication;
import com.rnfs.RNFSPackage;
import com.reactnativecommunity.cameraroll.CameraRollPackage;
import com.tradle.react.UdpSocketsModule;
import com.reactlibrary.rnwifi.RNWifiPackage;
import com.asterinet.react.tcpsocket.TcpSocketPackage;
import com.jksmarthome.floatbutton.FloatButtonPackage;
import com.jksmarthome.push.PushPackage;
import com.jksmarthome.wificonnect.WifiConfigurePackage;
import com.lzf.easyfloat.EasyFloat;
import com.reactcommunity.rndatetimepicker.RNDateTimePickerPackage;
import cn.jiguang.plugins.push.JPushModule;
import cn.jiguang.plugins.push.JPushPackage;
import com.google.gson.Gson;
import com.jksmarthome.user.UserPackage;
import com.jksmarthome.utils.Constants;
import com.jksmarthome.utils.CustomReactPackage;
import com.jksmarthome.utils.VoiceReactPackage;
import com.jksmarthome.utils.DplusReactPackage;
import com.jksmarthome.utils.ShareReactPackage;
import com.reactnativecommunity.viewpager.RNCViewPagerPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.PlatformConfig;
import com.videogo.openapi.EZOpenSDK;
import com.videogo.openapi.EzvizAPI;
import com.zmxv.RNSound.RNSoundPackage;
import com.rnim.rn.audio.ReactNativeAudioPackage;
import com.beefe.picker.PickerViewPackage;
import com.reactnativecommunity.slider.ReactSliderPackage;
import com.horcrux.svg.SvgPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.imagepicker.ImagePickerPackage;
import org.reactnative.camera.RNCameraPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import java.util.Arrays;
import java.util.List;
import ezviz.ezopensdk.demo.SdkInitParams;
import ezviz.ezopensdk.demo.SdkInitTool;
import ezviz.ezopensdk.demo.SpTool;
import ezviz.ezopensdk.demo.ValueKeys;
import ezviz.ezopensdkcommon.common.BaseApplication;

public class MainApplication extends BaseApplication implements ReactApplication {//old extends Application

  public static String mAppKey;
  public static String mAccessToken;
  public static String mOpenApiServer;
  public static String mOpenAuthApiServer;

  public static MainApplication mainApplication;

  public static EZOpenSDK getOpenSDK() {
    return EZOpenSDK.getInstance();
  }


  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNFSPackage(),
            new CameraRollPackage(),
            new UdpSocketsModule(),
            new RNWifiPackage(),
            new TcpSocketPackage(),
            new RNDateTimePickerPackage(),
            new JPushPackage(),
          new RNCViewPagerPackage(),
          new RNDeviceInfo(),
          new RNCWebViewPackage(),
          new SplashScreenReactPackage(),
          new RNSoundPackage(),
          new ReactNativeAudioPackage(),
          new PickerViewPackage(),
          new ReactSliderPackage(),
          new SvgPackage(),
          new PickerPackage(),
          new ImagePickerPackage(),
          new RNCameraPackage(),
          new AsyncStoragePackage(),
          new RNGestureHandlerPackage(),
          new CustomReactPackage(),
          new ShareReactPackage(),
          new DplusReactPackage(),
          new UserPackage(),
          voiceReactPackage,
              new PushPackage(),
              new FloatButtonPackage(),
              new WifiConfigurePackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  private static VoiceReactPackage voiceReactPackage = new VoiceReactPackage();

  public static VoiceReactPackage getVoiceReactPackage() {
    return voiceReactPackage;
  }

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    MultiDex.install(this);
  }

  public static MainApplication getApplicationC() {
    return mainApplication;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    mainApplication = this;
    SoLoader.init(this, /* native exopackage */ false);

    UMConfigure.preInit(this,Constants.UM_APPKEY,"Umeng");
    init();

    //判断ABIS是x86就不初始化萤石云, 萤石云不支持x86系统
//    if (!NativeUtils.isEmulator()) {
      initSDK();
      EzvizAPI.getInstance().setServerUrl(mOpenApiServer, mOpenAuthApiServer);
//    }

    Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
      @Override
      public void uncaughtException(Thread thread, Throwable throwable) {
        throwable.printStackTrace();
      }
    });

    //调用此方法：点击通知让应用从后台切到前台
    JPushModule.registerActivityLifecycle(this);

//    EasyFloat.init(this, false);
  }

  private void init() {
    SpTool.init(getApplicationContext());
    //  if (!loadLastSdkInitParams()){
    LoadDefaultSdkInitParams();
    //   }
  }

  private boolean loadLastSdkInitParams() {
    String sdkInitParamStr = SpTool.obtainValue(ValueKeys.SDK_INIT_PARAMS);
    if (sdkInitParamStr != null){
      SdkInitParams sdkInitParams = new Gson().fromJson(sdkInitParamStr, SdkInitParams.class);
      if (sdkInitParams != null && sdkInitParams.appKey != null){
        mAppKey = sdkInitParams.appKey;
        mAccessToken = sdkInitParams.accessToken;
        mOpenApiServer = sdkInitParams.openApiServer;
        mOpenAuthApiServer = sdkInitParams.openAuthApiServer;
        return true;
      }
    }
    return false;
  }

  private void LoadDefaultSdkInitParams(){
    mAppKey = "ca3bf8d196184dba938f406edf2c88b9";
    mAccessToken = "";
    mOpenApiServer = "https://open.ys7.com";
    mOpenAuthApiServer = "https://openauth.ys7.com";
  }

  /**
   * 初始化SDK
   */
  private void initSDK() {
    SdkInitParams initParams = new SdkInitParams();
    initParams.appKey = mAppKey;
    SdkInitTool.initSdk(this, initParams);
  }

  {
    PlatformConfig.setWeixin(Constants.WX_APPKEY, Constants.WX_APPSECRECT);
    PlatformConfig.setQQZone(Constants.QQ_APPID, Constants.QQ_APPKEY);
  }
}

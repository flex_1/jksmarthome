package com.jksmarthome.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.jksmarthome.adapter.ImageListAdapter;
import com.jksmarthome.entity.ImageOrVideoInfo;
import com.jksmarthome.entity.ImageOrVideoList;
import com.jksmarthome.entity.ApiResult;
import com.jksmarthome.entity.ResultInfo;
import com.jksmarthome.utils.Constants;
import com.jksmarthome.utils.NativeUtils;
import com.videogo.constant.Constant;
import com.videogo.constant.IntentConsts;
import com.videogo.exception.ErrorCode;
import com.jksmarthome.ui.util.ActivityUtils;
import com.jksmarthome.ui.util.VerifyCodeInput;
import com.videogo.util.ConnectionDetector;
import com.videogo.widget.CheckTextButton;
import com.jksmarthome.widget.PinnedSectionListView;
import com.jksmarthome.widget.PullToRefreshFooter;
import com.jksmarthome.widget.PullToRefreshFooter.Style;
import com.jksmarthome.widget.PullToRefreshHeader;
import com.videogo.widget.TitleBar;
import com.jksmarthome.widget.WaitDialog;
import com.jksmarthome.widget.pulltorefresh.IPullToRefresh.Mode;
import com.jksmarthome.widget.pulltorefresh.IPullToRefresh.OnRefreshListener;
import com.jksmarthome.widget.pulltorefresh.LoadingLayout;
import com.jksmarthome.widget.pulltorefresh.PullToRefreshBase;
import com.jksmarthome.widget.pulltorefresh.PullToRefreshBase.LoadingLayoutCreator;
import com.jksmarthome.widget.pulltorefresh.PullToRefreshBase.Orientation;
import com.jksmarthome.widget.pulltorefresh.PullToRefreshPinnedSectionListView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jksmarthome.R;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * 图片列表
 */
public class ImageListActivity extends RootActivity implements VerifyCodeInput.VerifyCodeErrorListener{
    private static final String TAG = "ImageListActivity";

    public static final int ERROR_WEB_NO_ERROR = 100000; // /< No error
    public static final int ERROR_WEB_NO_DATA = 100000 - 2; // /< The data is empty or does not exist

    private TitleBar mTitleBar;
    private PullToRefreshPinnedSectionListView mMessageListView;
    private ViewGroup mMainLayout;
    private ViewGroup mNoMessageLayout;
    private ImageView mNoMessageImg;
    private TextView mNoMessageTextView;
    private Button mNoMessageButton;
    private ViewGroup mRefreshLayout;
    private Button mRefreshButton;
    private TextView mRefreshTipView;
    private View mNoMoreView;
    private CheckTextButton mCheckModeButton;

    private ViewGroup mCheckModeTopLayout;
    private View mCheckModeTopDivider;
    private CheckBox mCheckAllView;
    private ViewGroup mCheckModeBottomLayout;
    private View mCheckModeBottomDivider;
    private Button mDeleteButton;
    private Button mReadButton;
    private FrameLayout mFrameLayout;
    private ImageView bigImageView;

    private ImageListAdapter mAdapter;
    private List<ImageOrVideoInfo> mMessageList;

    private long mLastLoadTime;
    private UIHandler mUIHandler;

    private int mDataType = Constant.MESSAGE_LIST;
    private String mDeviceSerial;
    private int mMenuPosition;
    private boolean mCheckMode;
    private String userToken;

    private String baseUrl = "";

    private int type = 1;

    //列表总页数
    private int alarmTotalPage = 1;

    // 屏幕宽度（像素）
    private int widthPixels;

    //获取图片或视频列表
    public interface ImagesOrVideoListService {
        @POST("api/content/getImagesUpload")
        Call<ResultInfo<ImageOrVideoList>> getImagesOrVideoList(@Body RequestBody body);
    }

    //删除
    public interface DeleteImageService {
        @POST("api/content/deleteImage")
        Call<ApiResult> deleteImage(@Body RequestBody body);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_list);

        NativeUtils.setWindowStatusBarColor(this);

        userToken = NativeUtils.getUserToken(this);

        getScreenWidth();

        findViews();
        initData();

        initTitleBar();
        initViews();
        setListner();
    }

    private void getScreenWidth() {
        WindowManager wm = (WindowManager) ImageListActivity.this.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        // 屏幕宽度（像素）
        widthPixels = dm.widthPixels;
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constant.ALARM_MESSAGE_DISPLAY_ACTION);

        if (mAdapter.getCount() > 0) {
            setRefreshLayoutVisibility(false);
            setNoMessageLayoutVisibility(false);
        }

        if (mDataType == Constant.MESSAGE_LIST
                && System.currentTimeMillis() - mLastLoadTime >= Constant.RELOAD_INTERVAL) {
            refreshButtonClicked();
        }

        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void findViews() {
        mTitleBar = (TitleBar) findViewById(R.id.title_bar);
        mMessageListView = (PullToRefreshPinnedSectionListView) findViewById(R.id.message_list);
        mNoMessageLayout = (ViewGroup) findViewById(R.id.no_message_layout);
        mNoMessageImg = (ImageView) findViewById(R.id.no_message_img);
        mNoMessageTextView = (TextView) findViewById(R.id.no_message_text);
        mNoMessageButton = (Button) findViewById(R.id.no_message_button);
        mRefreshLayout = (ViewGroup) findViewById(R.id.refresh_layout);
        mRefreshButton = (Button) mRefreshLayout.findViewById(R.id.retry_button);
        mRefreshTipView = (TextView) mRefreshLayout.findViewById(R.id.error_prompt);
        mMainLayout = (ViewGroup) findViewById(R.id.main_layout);

        mCheckModeTopLayout = (ViewGroup) findViewById(R.id.check_mode_top);
        mCheckModeTopDivider = findViewById(R.id.check_mode_top_divider);
        mCheckAllView = (CheckBox) findViewById(R.id.check_all);
        mCheckModeBottomLayout = (ViewGroup) findViewById(R.id.check_mode_bottom);
        mCheckModeBottomDivider = findViewById(R.id.check_mode_bottom_divider);
        mDeleteButton = (Button) findViewById(R.id.del_button);
        mReadButton = (Button) findViewById(R.id.read_button);

        mFrameLayout = (FrameLayout)findViewById(R.id.ll_bg);
        bigImageView = (ImageView)findViewById(R.id.iv_big_image);

        mTitleBar.setBackgroundColor(getResources().getColor(R.color.white));
    }

    private void initData() {
        mMessageList = new ArrayList<ImageOrVideoInfo>();

        mDeviceSerial = getIntent().getStringExtra(IntentConsts.EXTRA_DEVICE_ID);
        baseUrl = getIntent().getStringExtra(Constants.EXTRA_BASE_URL);
        type = getIntent().getIntExtra(Constants.EXTRA_IMAGE_TYPE,1);

        mAdapter = new ImageListAdapter(this, mMessageList, mDeviceSerial,this);
        mAdapter.setNoMenu(mDataType != Constant.MESSAGE_LIST);
        mUIHandler = new UIHandler();
    }

    public int getStatusBarHeight() {
        int statusBarHeight = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = getResources().getDimensionPixelSize(resourceId);
        }
        return statusBarHeight;
    }

    private void initTitleBar() {
        mTitleBar.setPadding(0, getStatusBarHeight(),0,0);
        if (mDataType == Constant.MESSAGE_LIST) {
            mTitleBar.setTitle(type == 2 ? "云视频" : "云相册");
            mCheckModeButton = mTitleBar.addRightCheckedText(getText(R.string.edit_txt), getText(R.string.cancel),
                    new OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                            }
                            setCheckMode(isChecked);
                        }
                    });
        }
        mTitleBar.addBackButton(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initViews() {
        if (mDataType == Constant.MESSAGE_LIST) {
            mNoMoreView = getLayoutInflater().inflate(R.layout.no_more_footer, null);

            mMessageListView.setLoadingLayoutCreator(new LoadingLayoutCreator() {

                @Override
                public LoadingLayout create(Context context, boolean headerOrFooter, Orientation orientation) {
                    if (headerOrFooter)
                        return new PullToRefreshHeader(context);
                    else
                        return new PullToRefreshFooter(context, Style.EMPTY_NO_MORE);
                }
            });
            mMessageListView.setMode(Mode.BOTH);
            mMessageListView.setOnRefreshListener(new OnRefreshListener<PinnedSectionListView>() {

                @Override
                public void onRefresh(PullToRefreshBase<PinnedSectionListView> refreshView, boolean headerOrFooter) {
                    getAlarmMessageList(true, headerOrFooter);
                }
            });

            mMessageListView.getRefreshableView().addFooterView(mNoMoreView);
            mMessageListView.setAdapter(mAdapter);
            mMessageListView.getRefreshableView().removeFooterView(mNoMoreView);

            mCheckModeButton.setVisibility(View.GONE);
        }
    }

    private void setListner() {
        OnClickListener clickListener = new OnClickListener() {

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.retry_button:
                        refreshButtonClicked();
                        break;

                    case R.id.no_message_layout:
                        refreshButtonClicked();
                        break;

                    case R.id.check_mode_top:
                        mCheckAllView.toggle();
                    case R.id.check_all:
                        if (mCheckAllView.isChecked()) {
                        }
                        if (mCheckAllView.isChecked())
                            mAdapter.checkAll();
                        else
                            mAdapter.uncheckAll();
                        setupCheckModeLayout(false);
                        break;

                    case R.id.del_button: //删除
                        deleteMessage(mAdapter.getCheckedIds());
                        break;

                    case R.id.read_button: //设为已读
//                        new CheckAlarmInfoTask2(true).execute(mAdapter.getCheckedIds());
                        break;

                    case R.id.no_message_button:
//                        WebUtils.openYsStore(EZMessageActivity2.this, null);
                        break;

                    case R.id.ll_bg: //大图背景
                        if (mFrameLayout.getVisibility() == View.VISIBLE)
                            mFrameLayout.setVisibility(View.GONE);
                        break;
                }
            }
        };

        mRefreshButton.setOnClickListener(clickListener);
        mNoMessageLayout.setOnClickListener(clickListener);
        mNoMessageButton.setOnClickListener(clickListener);
        mCheckModeTopLayout.setOnClickListener(clickListener);
        mCheckAllView.setOnClickListener(clickListener);
        mDeleteButton.setOnClickListener(clickListener);
        mReadButton.setOnClickListener(clickListener);
        mFrameLayout.setOnClickListener(clickListener);

        mAdapter.setOnClickListener(new ImageListAdapter.OnClickListener() {

            @Override
            public void onItemLongClick(BaseAdapter adapter, View view, int position) {
                mMenuPosition = position;
            }

            @Override
            public void onItemClick(BaseAdapter adapter, View view, int position) {
                ImageOrVideoInfo info = (ImageOrVideoInfo)adapter.getItem(position);

                if (type == 1) {
                    showBigImage(info.img);
                } else {
                    if (info.video != null) {
                        Intent intent = new Intent(ImageListActivity.this, VideoPlayActivity.class);
                        intent.putExtra(Constants.EXTRA_VIDEO_URL, info.video);
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onCheckClick(BaseAdapter adapter, View view, int position, boolean checked) {
                setupCheckModeLayout(false);
            }
        });

    }

    //显示大图
    private void showBigImage(String img) {
        if (mFrameLayout.getVisibility() == View.GONE)
            mFrameLayout.setVisibility(View.VISIBLE);

        Glide.with(ImageListActivity.this)
                .load(img)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {

                        if (bigImageView == null) {
                            return false;
                        }
                        if (bigImageView.getScaleType() != ImageView.ScaleType.FIT_CENTER) {
                            bigImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        }
                        int sWidth = widthPixels - 120;
                        ViewGroup.LayoutParams params = bigImageView.getLayoutParams();
                        params.height = (int)(sWidth * 0.56);
                        params.width = sWidth;
                        bigImageView.setLayoutParams(params);

                        return false;
                    }
                })
//                .placeholder(R.color.common_bg)
                .error(R.drawable.event_list_fail_pic)
                .into(bigImageView);
    }

    private void deleteMessage(final Object param) {
        Context context = (getParent() == null ? this : getParent());

        new AlertDialog.Builder(context).setMessage(R.string.delete_confirm)
                .setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).setNegativeButton(R.string.delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new DeleteAlarmMessageTask().execute(param);
            }
        }).show();
    }

    private void refreshButtonClicked() {
        setRefreshLayoutVisibility(false);
        setNoMessageLayoutVisibility(false);
        getAlarmMessageList(false, true);
    }

    private void getAlarmMessageList(boolean pullOrClick, boolean headerOrFooter) {
        if (pullOrClick) {
//            String lastTime = "";
            if (!headerOrFooter) {
//                if (mMessageList != null && mMessageList.size() > 0)
//                    lastTime = mMessageList.get(mMessageList.size() - 1).getAlarmStartTime();
            }
            new GetAlarmMessageTask(headerOrFooter).execute();
        } else {
            mMessageListView.setRefreshing();
        }
    }

    public void setCheckMode(boolean checkMode) {
        if (mCheckMode != checkMode) {
            mCheckMode = checkMode;
//            mCheckModeTopLayout.setVisibility(mCheckMode ? View.VISIBLE : View.GONE);
            mCheckModeTopDivider.setVisibility(mCheckMode ? View.VISIBLE : View.GONE);
            mCheckModeBottomLayout.setVisibility(mCheckMode ? View.VISIBLE : View.GONE);
            mCheckModeBottomDivider.setVisibility(mCheckMode ? View.VISIBLE : View.GONE);

            if (mCheckMode)
                setupCheckModeLayout(true);
            mAdapter.setCheckMode(mCheckMode);
        }
    }

    private void setNoMessageLayoutVisibility(boolean visible) {
        if (mCheckModeButton != null) {
// Edit TextCheck           mCheckModeButton.setVisibility((visible || mMainLayout.getVisibility() != View.VISIBLE) ? View.GONE
//                    : View.VISIBLE);
        }
        mNoMessageLayout.setVisibility(visible ? View.VISIBLE : View.GONE);

        if (visible) {
            if (type == 1) { //图片
                mNoMessageImg.setImageDrawable(getResources().getDrawable(R.mipmap.empty_image));
                mNoMessageTextView.setText("暂无相册");
            } else { //视频
                mNoMessageImg.setImageDrawable(getResources().getDrawable(R.mipmap.empty_video));
                mNoMessageTextView.setText("暂无视频");
            }
            mNoMessageLayout.setVisibility(View.VISIBLE);
        } else {
            mNoMessageLayout.setVisibility(View.GONE);
        }

    }

    private void setRefreshLayoutVisibility(boolean visible) {
        if (mCheckModeButton != null) {
//            mCheckModeButton.setVisibility(visible ? View.GONE : View.VISIBLE);
        }
        mRefreshLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private void setupCheckModeLayout(boolean init) {
        if (mCheckMode) {
            List<String> ids = new ArrayList<String>();
            boolean checkAll = false;

            if (!init) {
                ids.addAll(mAdapter.getCheckedIds());
                checkAll = mAdapter.isCheckAll();
            }

            mCheckAllView.setChecked(checkAll);

            if (ids.size() == 0) {
                mDeleteButton.setText(R.string.delete);
                mDeleteButton.setEnabled(false);

                mReadButton.setEnabled(true);
            } else {
                mDeleteButton.setText(getString(R.string.delete) + '（' + ids.size() + '）');
                mDeleteButton.setEnabled(true);

                mReadButton.setEnabled(true);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (mAdapter == null || mAdapter.getCount() == 0) {
            return true;
        }
        ImageOrVideoInfo alarmInfo = (ImageOrVideoInfo) mAdapter.getItem(mMenuPosition);
        if (alarmInfo == null) {
            return true;
        }
        switch (item.getItemId()) {
            case ImageListAdapter.MENU_DEL_ID:
                deleteMessage(alarmInfo);
                break;

//            case MessageListAdapter.MENU_MORE_ID:
//                mCheckModeButton.setChecked(true);
//                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.setResult(RESULT_OK);
        this.finish();
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        if (getParent() == null)
            super.startActivityForResult(intent, requestCode);
        else
            getParent().startActivityForResult(intent, requestCode);
    }

    private void deleteAlarmFromList(String alarmId) {
        if(mMessageList != null && mMessageList.size() > 0) {
            for(int i = 0 ; i < mMessageList.size(); i ++) {
                ImageOrVideoInfo info = mMessageList.get(i);
                if((info.id+"").equals(alarmId)) {
                    mMessageList.remove(info);
                }
            }
        }
    }

    @Override
    public void verifyCodeError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mAdapter != null){
                    mAdapter.setVerifyCodeDialog();
                }
            }
        });
    }

    private class GetAlarmMessageTask extends AsyncTask<String, Void, List<ImageOrVideoInfo>> {
        private boolean mHeaderOrFooter;
        private int mErrorCode = 100000;// ErrorCode.ERROR_WEB_NO_ERROR;

        public GetAlarmMessageTask(boolean headerOrFooter) {
            mHeaderOrFooter = headerOrFooter;
        }

        @Override
        protected List<ImageOrVideoInfo> doInBackground(String... params) {
            if (mHeaderOrFooter) {
            } else {
            }

            if (!ConnectionDetector.isNetworkAvailable(ImageListActivity.this)) {
                mErrorCode = ErrorCode.ERROR_WEB_NET_EXCEPTION;
                return null;
            }

            final List<ImageOrVideoInfo> result = new ArrayList<>();

            int pageSize = 10;
            int pageStart = 0;
            if(mHeaderOrFooter) {
                pageStart = 0;
            } else {
                pageStart = mAdapter.getCount() / pageSize;
            }

            if (pageStart >= alarmTotalPage) {
                return result;
            }

            try {

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                ImagesOrVideoListService service = retrofit.create(ImagesOrVideoListService.class);

                JSONObject root = new JSONObject();
                root.put("token", userToken);
                root.put("devSerial", mDeviceSerial);
                root.put("type", type);
                root.put("pageNum", pageStart + 1);
                root.put("pageSize", pageSize);

                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"),root.toString());

                Call<ResultInfo<ImageOrVideoList>> call = service.getImagesOrVideoList(requestBody);

                //同步请求
                Response<ResultInfo<ImageOrVideoList>> response = call.execute();

                if (response.body() != null && response.body().result != null) {
                    ImageOrVideoList msgList = response.body().result;

                    alarmTotalPage = response.body().result.totalPageNum;

                    for(int i = 0;i < msgList.list.length; i ++){
                        ImageOrVideoInfo msg = msgList.list[i];
                        result.add(msg);
                    }
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

                mErrorCode = ERROR_WEB_NO_DATA;
            }

            return result;
        }

        @Override
        protected void onPostExecute(List<ImageOrVideoInfo> result) {
            super.onPostExecute(result);
            mMessageListView.onRefreshComplete();
            int pageSize = 10;

            if (result == null){
                if (mErrorCode != ERROR_WEB_NO_ERROR)
                    onError(mErrorCode);
                return;
            }
            if (mHeaderOrFooter
                    && (mErrorCode == ERROR_WEB_NO_ERROR || mErrorCode == ERROR_WEB_NO_DATA)) {
                CharSequence dateText = DateFormat.format("yyyy-MM-dd kk:mm:ss", new Date());
                for (LoadingLayout layout : mMessageListView.getLoadingLayoutProxy(true, false).getLayouts()) {
                    ((PullToRefreshHeader) layout).setLastRefreshTime(":" + dateText);
                }
                mMessageListView.setFooterRefreshEnabled(true);
                mMessageListView.getRefreshableView().removeFooterView(mNoMoreView);
//                mAdapter.();
                mMessageList.clear();
            }

            if(mAdapter.getCount() == 0 && result.size() == 0) {
                // show no message ui
            } else if (result.size() < pageSize) {
                mMessageListView.setFooterRefreshEnabled(false);
                mMessageListView.getRefreshableView().addFooterView(mNoMoreView);
            } else if (mHeaderOrFooter) {
                mMessageListView.setFooterRefreshEnabled(true);
                mMessageListView.getRefreshableView().removeFooterView(mNoMoreView);
            }

            if (result != null && result.size() > 0) {
                mMessageList.addAll(result);
                mAdapter.setList(mMessageList);
                setupCheckModeLayout(false);
                mAdapter.notifyDataSetChanged();

                mLastLoadTime = System.currentTimeMillis();
            } else {
                mErrorCode = ERROR_WEB_NO_DATA;
            }
            sendUIMessage(mUIHandler, UI_MSG_LIST_CHANGE, 0, 0);

            if (mMessageList.size() > 0) {
                setNoMessageLayoutVisibility(false);
            }

            if (mErrorCode != ERROR_WEB_NO_ERROR)
                onError(mErrorCode);
        }

        protected void onError(int errorCode) {
            switch (errorCode) {
                case ERROR_WEB_NO_DATA:
                    if (mMessageList.size() == 0) {
                        setRefreshLayoutVisibility(false);
                        setNoMessageLayoutVisibility(true);
                        mMessageListView.getRefreshableView().removeFooterView(mNoMoreView);
                    } else {
                        setRefreshLayoutVisibility(false);
                        mMessageListView.setFooterRefreshEnabled(false);
                        mMessageListView.getRefreshableView().addFooterView(mNoMoreView);
                    }
                    break;

                /*case ErrorCode.ERROR_WEB_SESSION_ERROR:
                    ActivityUtils.handleSessionException(EZMessageActivity2.this);
                    break;

                case ErrorCode.ERROR_WEB_HARDWARE_SIGNATURE_ERROR:
                    ActivityUtils.handleHardwareError(EZMessageActivity2.this, null);
                    break;

                case ErrorCode.ERROR_WEB_SERVER_EXCEPTION:
                    showError(getText(R.string.message_refresh_fail_server_exception));
                    break;

                case ErrorCode.ERROR_WEB_NET_EXCEPTION:
                    showError(getText(R.string.message_refresh_fail_network_exception));
                    break;*/

                default:
                    showError(getErrorTip(R.string.get_message_fail_service_exception, errorCode));
                    break;
            }
        }

        private void showError(CharSequence text) {
            if (mHeaderOrFooter && mMessageList.size() == 0) {
                mRefreshTipView.setText(text);
                setRefreshLayoutVisibility(true);
            } else {
                showToast(text);
            }
        }
    }

    private class DeleteAlarmMessageTask extends AsyncTask<Object, Void, Object> {

        private Dialog mWaitDialog;
        private int mErrorCode = ERROR_WEB_NO_ERROR;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mWaitDialog = new WaitDialog(ImageListActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
            mWaitDialog.setCancelable(false);
            mWaitDialog.show();
        }

        @Override
        protected Object doInBackground(Object... params) {
            if (!ConnectionDetector.isNetworkAvailable(ImageListActivity.this)) {
                mErrorCode = ErrorCode.ERROR_WEB_NET_EXCEPTION;
                return null;
            }

            List<String> deleteIds = new ArrayList<String>();
            if (params[0] instanceof ImageOrVideoInfo) {
                // 单个删除 Single delete
                ImageOrVideoInfo info = (ImageOrVideoInfo) params[0];
                if (info.id == 0) {
                } else {
                    deleteIds.add(info.id + "");
                }

            } else if (params[0] instanceof List<?>) {
                // 批量删除 batch deletion
                List<String> ids = (List<String>) params[0];
                if (ids != null && ids.size() > 0)
                    deleteIds.addAll(ids);
            }

            try {
                // 删除
                StringBuilder idString = new StringBuilder();

                for(int i = 0;i < deleteIds.size(); i ++){
                    if (i != 0) {
                        idString.append(",");
                    }
                    idString.append(deleteIds.get(i));
                }

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                DeleteImageService service = retrofit.create(DeleteImageService.class);

                JSONObject root = new JSONObject();
                root.put("token", userToken);
                root.put("id", idString.toString());

                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"),root.toString());

                Call<ApiResult> call = service.deleteImage(requestBody);

                //同步请求
                Response<ApiResult> response = call.execute();

            } catch (Exception e) {
                e.printStackTrace();

                mErrorCode = ERROR_WEB_NO_DATA;
            }

            return params[0];
        }

        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);
            mWaitDialog.dismiss();

            if (result != null) {
                if (result instanceof ImageOrVideoInfo) {
                    ImageOrVideoInfo info = (ImageOrVideoInfo) result;
                    //mAlarmLogInfoManager.deleteAlarmLogList(info);
                    //mj if (mDataType == Constant.MESSAGE_INNER_PUSH)
                    if (mDataType == Constant.MESSAGE_LIST)
                        mMessageList.remove(info);
                } else if (result instanceof List<?>) {
                    List<String> ids = (List<String>) result;
                    for (String id : ids) {
                        deleteAlarmFromList(id);
                    }
                }

                // 如果删除到最后会重新获取的 If deleted to the last will be re-acquired
                if (mDataType == Constant.MESSAGE_LIST && mMessageList.size() == 0) {
                    setNoMessageLayoutVisibility(true);
                    refreshButtonClicked();
                }

                mAdapter.setList(mMessageList);
                setupCheckModeLayout(false);
                mAdapter.notifyDataSetChanged();
                showToast(getText(R.string.alarm_message_del_success_txt));
                mCheckModeButton.setChecked(false);
            }

            if (mErrorCode != ERROR_WEB_NO_ERROR)
                onError(mErrorCode);
        }

        protected void onError(int errorCode) {
            switch (errorCode) {
                case ErrorCode.ERROR_WEB_SESSION_ERROR:
                    ActivityUtils.handleSessionException(ImageListActivity.this);
                    break;

                case ErrorCode.ERROR_WEB_HARDWARE_SIGNATURE_ERROR:
                    ActivityUtils.handleSessionException(ImageListActivity.this);
                    break;

                case ErrorCode.ERROR_WEB_SERVER_EXCEPTION:
                    showToast(getText(R.string.alarm_message_del_fail_txt));
                    break;

                case ErrorCode.ERROR_WEB_NET_EXCEPTION:
                    showToast(getText(R.string.alarm_message_del_fail_network_exception));
                    break;

                default:
                    showToast(R.string.alarm_message_del_fail_txt, errorCode);
                    break;
            }
        }
    }

    private void sendUIMessage(Handler handler, int what, int arg1, int arg2) {
        Message msg = Message.obtain();
        msg.what = what;
        msg.arg1 = arg1;
        msg.arg2 = arg2;
        handler.sendMessage(msg);
    }
    private static final int UI_MSG_BASE = 500;
    private static final int UI_MSG_EMPTY_LIST = UI_MSG_BASE + 1;
    private static final int UI_MSG_LIST_CHANGE = UI_MSG_BASE + 2;

    private class UIHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case UI_MSG_EMPTY_LIST:
                    break;
                case UI_MSG_LIST_CHANGE:
                    if(mMessageList.size() == 0) {
                        mCheckModeButton.setVisibility(View.GONE);
                    } else {
                        mCheckModeButton.setVisibility(View.VISIBLE);
                    }
                    break;
                default:
                    break;
            }
        }

    }
}
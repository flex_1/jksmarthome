package com.jksmarthome.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.annotation.Nullable;
import com.jksmarthome.R;
import com.jksmarthome.view.CustomVideoView;
import com.jksmarthome.utils.Constants;
import com.jksmarthome.utils.NativeUtils;
import com.videogo.widget.CheckTextButton;
import com.videogo.widget.TitleBar;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class VideoPlayActivity extends Activity implements View.OnClickListener,
        MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnPreparedListener, MediaPlayer.OnInfoListener, SeekBar.OnSeekBarChangeListener {

    private static final String TAG = "VideoPlayActivity";

    private String videoUrl = "";

    private CustomVideoView videoView;

    private TitleBar mTitleBar, mLandscapeTitleBar;

    private ImageButton playBtn, soundBtn;

    private ProgressBar progressBar;

    private TextView play_tip_tv, current_time_tv, time_tv;

    private SeekBar progress_seekbar;

    private ImageView play_iv, replay_btn;

    private FrameLayout menu_fl, landscape_fl;

    private ProgressBar play_bottom_progressbar;

    private CheckTextButton fullscreenBtn;

    private int intPositionWhenPause;
    //是否静音
    private boolean isSoundNull = false;

    private AudioManager audioManager;
    //当前音量
    private int currentVolume = 0;

    private Timer mTimer;

    private static Handler handler = new Handler();

    private Runnable runnable = new Runnable() {
        public void run() {
            if (videoView.isPlaying()) {
                int current = videoView.getCurrentPosition();

                //videoView.getCurrentPosition()是异步的，有时候获取不准
                if (dateFormat(current).equals(dateFormat(videoView.getDuration()))) {
                    progress_seekbar.setProgress(videoView.getDuration());
                    play_bottom_progressbar.setProgress(videoView.getDuration());
                } else {
                    progress_seekbar.setProgress(current);
                    play_bottom_progressbar.setProgress(current);
                }

                current_time_tv.setText(dateFormat(current));
            }
            handler.postDelayed(runnable, 500);
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_play);

        NativeUtils.setWindowStatusBarColor(this);

        videoUrl = getIntent().getStringExtra(Constants.EXTRA_VIDEO_URL);

        findView();

        initTitleBar();

        setListener();

        //设置url
        videoView.setVideoURI(Uri.parse(videoUrl));

        getAudioVolume();

        progress_seekbar.setOnSeekBarChangeListener(this);

    }

    //设置监听
    @SuppressLint("NewApi")
    private void setListener() {
        //设置播放完成之后监听
        videoView.setOnCompletionListener(this);
        //设置发生错误监听，如果不设置videoview会向用户提示发生错误
        videoView.setOnErrorListener(this);
        //设置在视频在载入完毕以后的回调函数
        videoView.setOnPreparedListener(this);
        //设置videoView的触摸监听
//        videoView.setOnTouchListener(this);
        //监听视频卡顿和停止卡顿
        videoView.setOnInfoListener(this);

        //全屏按钮
        fullscreenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!fullscreenBtn.isChecked()) {
                    //设置videoView竖屏播放
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    fullscreenBtn.setChecked(false);
                } else {
                    //设置videoView横屏播放
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    fullscreenBtn.setChecked(true);
                }
            }
        });

        //视频点击事件，控制titlebar、menu、底部进度条的显示
        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Configuration mConfiguration = VideoPlayActivity.this.getResources().getConfiguration(); //获取设置的配置信息
                int ori = mConfiguration.orientation; //获取屏幕方向

                if (menu_fl.getVisibility() == View.VISIBLE) {
                    menu_fl.setVisibility(View.GONE);
                    play_bottom_progressbar.setVisibility(View.VISIBLE);
                    landscape_fl.setVisibility(View.GONE);

                } else {
                    play_bottom_progressbar.setVisibility(View.GONE);
                    menu_fl.setVisibility(View.VISIBLE);

                    if (ori == mConfiguration.ORIENTATION_LANDSCAPE) { //横屏
                        landscape_fl.setVisibility(View.VISIBLE);
                    } else if (ori == mConfiguration.ORIENTATION_PORTRAIT) { //竖屏
                        landscape_fl.setVisibility(View.GONE);
                    }

                    unShowProgress();
                }

            }
        });

        mLandscapeTitleBar.addBackButton(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (videoView == null) {
            return;
        }

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) { //竖屏
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

            landscape_fl.setVisibility(View.GONE);
            mTitleBar.setVisibility(View.VISIBLE);

            RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, NativeUtils.dip2px(this,230));
            videoView.setLayoutParams(params2);

        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) { //横屏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

            mTitleBar.setVisibility(View.GONE);
            landscape_fl.setVisibility(View.VISIBLE);

            DisplayMetrics DisplayMetrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(DisplayMetrics);
            int videoHeight = DisplayMetrics.heightPixels;
            int videoWidth = DisplayMetrics.widthPixels - 400;
            RelativeLayout.LayoutParams LayoutParams = new RelativeLayout.LayoutParams(videoWidth, videoHeight);
            LayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            videoView.setLayoutParams(LayoutParams);

        }

    }

    //获取当前页面音量
    private void getAudioVolume () {
        try {
            audioManager = (AudioManager) getSystemService(Service.AUDIO_SERVICE);
            currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //如果当前页面暂停则存储当前播放位置，全局变量存储
        intPositionWhenPause = videoView.getCurrentPosition();
        //停止回放视频
        videoView.stopPlayback();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //跳转到暂停时存储的位置
        if (intPositionWhenPause >= 0) {
            videoView.seekTo(intPositionWhenPause);
            //初始播放位置
            intPositionWhenPause = -1;
        }
    }

    @Override
    public void onBackPressed() {
        if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//设置videoView竖屏播放
            fullscreenBtn.setChecked(false);
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
        //停止播放并释放资源
        videoView.stopPlayback();
        //退出页面时恢复声音设置
        setVoiceVolume(currentVolume);
    }

    private void findView() {
        mTitleBar = (TitleBar) findViewById(R.id.title_bar);
        mTitleBar.setBackgroundColor(getResources().getColor(R.color.white));
        videoView = (CustomVideoView) findViewById(R.id.videoView);
        //初始化进度条
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //播放器中心提示文本
        play_tip_tv = (TextView) findViewById(R.id.play_tip_tv);
        //播放器中心播放图标
        play_iv = (ImageView) findViewById(R.id.play_iv);
        //播放器中心重播图标
        replay_btn = (ImageView) findViewById(R.id.replay_btn);
        progress_seekbar = (SeekBar) findViewById(R.id.progress_seekbar);
        current_time_tv = (TextView) findViewById(R.id.current_time_tv);
        time_tv = (TextView) findViewById(R.id.all_time_tv);
        playBtn = (ImageButton) findViewById(R.id.play_btn);
        soundBtn = (ImageButton) findViewById(R.id.sound_btn);
        fullscreenBtn = (CheckTextButton) findViewById(R.id.fullscreen_button);
        menu_fl = (FrameLayout) findViewById(R.id.menu_fl);
        play_bottom_progressbar = (ProgressBar) findViewById(R.id.play_bottom_progressbar);
        landscape_fl = (FrameLayout) findViewById(R.id.landscape_fl);

        mLandscapeTitleBar = (TitleBar) findViewById(R.id.pb_notlist_title_bar_landscape);
        mLandscapeTitleBar.setStyle(Color.rgb(0xff, 0xff, 0xff), getResources().getDrawable(R.color.dark_bg_80p), null);
    }

    public int getStatusBarHeight() {
        int statusBarHeight = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = getResources().getDimensionPixelSize(resourceId);
        }
        return statusBarHeight;
    }

    private void initTitleBar() {
        mTitleBar.setTitle("云视频");
        mTitleBar.setPadding(0, getStatusBarHeight(),0,0);
        mTitleBar.addBackButton(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.replay_btn: //屏幕中间重播
                replay_btn.setVisibility(View.GONE);
                play_iv.setVisibility(View.GONE);
                play_tip_tv.setText("");
                onPlayBtn();
                break;
            case R.id.play_iv: //屏幕中间播放
            case R.id.play_btn: //播放
                onPlayBtn();
                break;
            case R.id.sound_btn: //声音
                if (!isSoundNull) { //静音
                    soundBtn.setBackgroundResource(R.drawable.remote_list_soundoff_btn_selector);
                    setVoiceVolume(0);
                } else { //有声音
                    soundBtn.setBackgroundResource(R.drawable.remote_list_soundon_btn_selector);
                    setVoiceVolume(currentVolume);
                }
                isSoundNull = !isSoundNull;
                break;
            default:
                break;
        }
    }

    /**
     * 使用AudioManager控制音量
     * value：音量
     */
    private void setVoiceVolume(int value) {
        try {
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, value, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 点击播放按钮
     */
    private void onPlayBtn() {
        boolean isPlaying = videoView.isPlaying();
        if (isPlaying) {
            pauseVideo();
            return;
        }

        playVideo();
    }

    /**
     * 播放视频
     */
    private void playVideo() {
        //设置屏幕显示信息
        replay_btn.setVisibility(View.GONE);
        play_iv.setVisibility(View.GONE);
        play_tip_tv.setText("");
        playBtn.setBackgroundResource(R.drawable.remote_list_pause_btn_selector);
        handler.postDelayed(runnable, 0);
        videoView.start();
        progress_seekbar.setMax(videoView.getDuration());
        play_bottom_progressbar.setMax(videoView.getDuration());

        unShowProgress();
    }

    /**
     * 暂停播放
     */
    private void pauseVideo() {
        replay_btn.setVisibility(View.GONE);
        play_iv.setVisibility(View.VISIBLE);
        play_tip_tv.setText("");
        playBtn.setBackgroundResource(R.drawable.remote_list_play_btn_selector);
        videoView.pause();
    }

    //定时隐藏进度条
    private void unShowProgress() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }

        Runnable r = new Runnable() {
            @Override
            public void run() {
                mTimer = new Timer();
                mTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                menu_fl.setVisibility(View.GONE);
                                landscape_fl.setVisibility(View.GONE);
                                play_bottom_progressbar.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }, 4000);
            }
        };
        runOnUiThread(r);
    }

    /**
     * 监听视频装载完成的事件
     */
    @Override
    public void onPrepared(MediaPlayer mp) {
        //如果视频载入成功,隐藏载入进度条
        progressBar.setVisibility(View.GONE);

        //设置屏幕提示信息为空
        play_tip_tv.setText("");

        time_tv.setText(dateFormat(mp.getDuration()));

        menu_fl.setVisibility(View.VISIBLE);
        play_bottom_progressbar.setVisibility(View.GONE);

        onPlayBtn();
    }

    /**
     * 监听播放完成的事件
     */
    @Override
    public void onCompletion(MediaPlayer mp) {
        //设置屏幕提示信息
        replay_btn.setVisibility(View.VISIBLE);
        menu_fl.setVisibility(View.VISIBLE);
        play_bottom_progressbar.setVisibility(View.GONE);
        play_iv.setVisibility(View.GONE);
        play_tip_tv.setText("");
        playBtn.setBackgroundResource(R.drawable.remote_list_play_btn_selector);

        Configuration mConfiguration = VideoPlayActivity.this.getResources().getConfiguration(); //获取设置的配置信息
        int ori = mConfiguration.orientation; //获取屏幕方向

        if (ori == mConfiguration.ORIENTATION_LANDSCAPE) { //横屏
            landscape_fl.setVisibility(View.VISIBLE);
        } else if (ori == mConfiguration.ORIENTATION_PORTRAIT) { //竖屏
            landscape_fl.setVisibility(View.GONE);
        }
    }

    /**
     * 监听视频卡顿和停止卡顿
     */
    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        switch (what) {
            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                //设置屏幕显示信息，开始卡顿
                play_tip_tv.setText("视频卡顿，加载中.....");
                break;
            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                //设置屏幕显示信息，卡顿结束
                play_tip_tv.setText("");
                break;
        }
        return true;
    }

    /**
     * 监听播放发生错误时候的事件
     */
    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {

        //设置屏幕显示信息
        play_tip_tv.setText("视频未知错误");

        switch (what) {
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                Log.d(TAG, "发生未知错误");

                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                Log.d(TAG, "媒体服务器宕机");
                break;
            default:
                Log.d(TAG, "onError + " + what);
                break;
        }
        switch (extra) {
            case MediaPlayer.MEDIA_ERROR_IO:
                //io读写错误
                Log.d(TAG, "IO操作错误");
                break;
            case MediaPlayer.MEDIA_ERROR_MALFORMED:
                //格式不支持
                Log.d(TAG, "格式不符合相关规范");
                break;
            case MediaPlayer.MEDIA_ERROR_TIMED_OUT:
                //一些操作需要太长时间来完成,通常超过3 - 5秒。
                Log.d(TAG, "操作超时");
                break;
            case MediaPlayer.MEDIA_ERROR_UNSUPPORTED:
                //格式符合相关规范,但媒体框架不支持该功能
                Log.d(TAG, "格式符合相关规范,但媒体框架不支持该功能");
                break;
            default:
                Log.d(TAG, "onError + " + extra);
                break;
        }

        return false;
    }

    //时间格式转换
    private String dateFormat(long millionSeconds) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss");
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(millionSeconds);
        return simpleDateFormat.format(c.getTime());
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // 取得当前进度条的刻度
        int progress = seekBar.getProgress();
        if (videoView.isPlaying()) {
            // 设置当前播放的位置
            videoView.seekTo(progress);
        }
    }
}

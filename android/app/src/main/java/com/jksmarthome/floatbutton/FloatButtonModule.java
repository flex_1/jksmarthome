package com.jksmarthome.floatbutton;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.jksmarthome.R;
import com.lzf.easyfloat.EasyFloat;
import com.lzf.easyfloat.enums.ShowPattern;
import com.lzf.easyfloat.enums.SidePattern;
import com.lzf.easyfloat.interfaces.OnFloatCallbacks;
import com.lzf.easyfloat.interfaces.OnPermissionResult;
import com.lzf.easyfloat.permission.PermissionUtils;

import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;

/**
 * 悬浮按钮
 */
public class FloatButtonModule extends ReactContextBaseJavaModule {

    private static final String TAG = "FloatButtonModule_Log";

    private final static String FLOAT_MODULE_NAME = "FloatButtonModule";

    private final static String FLOAT_EMITTER_NAME = "FloatButtonEmitter";

    private final static String FLOAT_TAG = "mic_float_window";

    public static ReactContext mReactContext;

    private boolean isDowning = false;

    private boolean isOpenPerm = false;

    public FloatButtonModule(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
        mReactContext = reactContext;
    }

    @Nonnull
    @Override
    public String getName() {
        return FLOAT_MODULE_NAME;
    }

    /**
     * 获得屏幕宽度
     */
    public static int getScreenWidth() {
        WindowManager wm = (WindowManager) mReactContext
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.widthPixels;
    }

    /**
     * 获得屏幕高度
     */
    public static int getScreenHeight() {
        WindowManager wm = (WindowManager) mReactContext
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.heightPixels;
    }

    @ReactMethod
    public void showFloat() {

        if (getCurrentActivity() != null && !isOpenPerm) {
            // 权限检测
            boolean isOpenPermission = PermissionUtils.checkPermission(getCurrentActivity());
//            Log.e(TAG, "权限检测 = " + isOpenPermission);

            isOpenPerm = true;

            if (!isOpenPermission) {
                // 权限申请，参数2为权限回调接口
                PermissionUtils.requestPermission(getCurrentActivity(), new OnPermissionResult() {
                    @Override
                    public void permissionResult(boolean b) {
//                        Log.e(TAG, "权限回调接口 = " + b);
                        if (!b) {
                            Toast.makeText(getCurrentActivity(), "你拒绝了弹窗打开权限", Toast.LENGTH_SHORT).show();
                            WritableMap upEvent = Arguments.createMap();
                            //传递的参数
                            upEvent.putString("permission", "false");
                            mReactContext
                                    .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                                    .emit(FLOAT_EMITTER_NAME, upEvent);
                        } else {
                            WritableMap upEvent = Arguments.createMap();
                            //传递的参数
                            upEvent.putString("permission", "true");
                            mReactContext
                                    .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                                    .emit(FLOAT_EMITTER_NAME, upEvent);
                            createFloat();
                        }
                    }
                });
            } else {
                createFloat();
            }
        }

    }

    private void createFloat() {
        try {
            if (EasyFloat.getAppFloatView(FLOAT_TAG) != null) {
                EasyFloat.showAppFloat(FLOAT_TAG);
                return;
            }

            EasyFloat.with(getReactApplicationContext())
                    // 设置浮窗xml布局文件，并可设置详细信息
                    .setLayout(R.layout.layout_float_window_mic)
                    // 设置浮窗显示类型，默认只在当前Activity显示，可选一直显示、仅前台显示
                    .setShowPattern(ShowPattern.ALL_TIME)
                    // 设置吸附方式，共15种模式，详情参考SidePattern
                    .setSidePattern(SidePattern.RESULT_HORIZONTAL)
                    // 设置浮窗的标签，用于区分多个浮窗
                    .setTag(FLOAT_TAG)
                    // 设置浮窗是否可拖拽
                    .setDragEnable(true)
                    .registerCallbacks(new OnFloatCallbacks() {
                        @Override
                        public void createdResult(boolean b, @org.jetbrains.annotations.Nullable String s, @org.jetbrains.annotations.Nullable View view) {
//                            Log.e(TAG, "showFloatWindow createdResult");
                        }

                        @Override
                        public void show(@NotNull View view) {
//                            Log.e(TAG, "showFloatWindow show");
                        }

                        @Override
                        public void hide(@NotNull View view) {
//                            Log.e(TAG, "showFloatWindow hide");
                        }

                        @Override
                        public void dismiss() {
//                            Log.e(TAG, "showFloatWindow dismiss");
                        }

                        @Override
                        public void touchEvent(@NotNull View view, @NotNull MotionEvent motionEvent) {
                            switch (motionEvent.getAction()) {
                                case MotionEvent.ACTION_DOWN:
                                    Log.e(TAG, "showFloatWindow DOWN");
                                    if (!isDowning) {
                                        isDowning = true;
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {

                                                try {
                                                    Thread.sleep(500); // 休眠0.5秒
                                                } catch (InterruptedException e) {
                                                    e.printStackTrace();
                                                }

                                                if (isDowning) {
                                                    WritableMap downEvent = Arguments.createMap();
                                                    //传递的参数
                                                    downEvent.putString("action", "down");
                                                    mReactContext
                                                            .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                                                            .emit(FLOAT_EMITTER_NAME, downEvent);
                                                }
                                            }
                                        }).start();
                                    }
                                    break;
                                case MotionEvent.ACTION_MOVE:
//                                    Log.e(TAG, "showFloatWindow ACTION_MOVE");
                                    break;
                                case MotionEvent.ACTION_CANCEL:
                                case MotionEvent.ACTION_UP:
                                    Log.e(TAG, "showFloatWindow ACTION_UP");
                                    if (isDowning) {
                                        isDowning = false;
                                        WritableMap upEvent = Arguments.createMap();
                                        //传递的参数
                                        upEvent.putString("action", "up");
                                        mReactContext
                                                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                                                .emit(FLOAT_EMITTER_NAME, upEvent);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }

                        @Override
                        public void drag(@NotNull View view, @NotNull MotionEvent motionEvent) {
//                            Log.e(TAG, "showFloatWindow drag");
                        }

                        @Override
                        public void dragEnd(@NotNull View view) {
//                            Log.e(TAG, "showFloatWindow dragEnd");
                        }
                    })
                    .setLocation((int) (getScreenWidth() * 0.84), (int) (getScreenHeight() * 0.5))
                    .setGravity(Gravity.END, (int) (getScreenWidth() * 0.84), (int) (getScreenHeight() * 0.5))
                    .show();

        } catch (Exception e) {
            Log.e(TAG, "showFloat error = " + e.getMessage());
        }
    }

    @ReactMethod
    public void hideFloat() {
        if (EasyFloat.appFloatIsShow(FLOAT_TAG)) {
            EasyFloat.hideAppFloat(FLOAT_TAG);
            isDowning = false;
            isOpenPerm = false;
        }
    }

}

package com.jksmarthome.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.umeng.socialize.media.UMMin;
import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import java.util.Map;

public class JKUMAndShareUtils extends ReactContextBaseJavaModule {

    private Activity currentActivity = null;

    private Context context;

    public Callback callback;

    public Callback loginCallback;

    private UMShareAPI umShareAPI ;

    public JKUMAndShareUtils(ReactApplicationContext reactContext) {
        super(reactContext);
        context = reactContext;
        //umShareAPI = UMShareAPI.get(reactContext);
    }

    @Override
    public String getName() {
        return "JKUMAndShareUtils";
    }

    /**
     * 从JS页面跳转到原生activity 同时也可以从JS传递相关数据到原生
     */
    @ReactMethod
    public void share(String shareTitle, String shareIcon, String shareLink, String shareContent, Callback callback){
        this.callback = callback;
        initPermission(shareTitle,shareIcon,shareLink,shareContent);
    }

    //分享小程序
    /**
     *  miniProgramType: 0-正式版  1-开发版  2-体验版
     */
    @ReactMethod
    public void shareToMinProgram(String shareTitle, String shareIcon, String shareLink, String shareContent, Integer miniProgramType, Callback callback){
        this.callback = callback;
//        if(Build.VERSION.SDK_INT>=23){
//            String[] mPermissionList = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                    Manifest.permission.READ_LOGS,Manifest.permission.READ_PHONE_STATE,
//                    Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.SET_DEBUG_APP,
//                    Manifest.permission.SYSTEM_ALERT_WINDOW,Manifest.permission.GET_ACCOUNTS,
//                    Manifest.permission.WRITE_APN_SETTINGS};
//            ActivityCompat.requestPermissions(getCurrentActivity(),mPermissionList,123);
//        }
        UMMin umMin = new UMMin(shareLink);
        if(shareIcon != null){
            UMImage image = new UMImage(getCurrentActivity(), shareIcon);//网络图片
            image.compressStyle = UMImage.CompressStyle.QUALITY;
            umMin.setThumb(image);
        }
        umMin.setTitle(shareTitle);
        umMin.setDescription(shareContent);
        umMin.setPath(shareLink);
        umMin.setUserName("gh_a47f3532b7cb");
        new ShareAction(getCurrentActivity())
                .withMedia(umMin)
                .setPlatform(SHARE_MEDIA.WEIXIN)
                .setCallback(shareListener)
                .share();
    }

    @ReactMethod
    public void auth(final int sharemedia, final Callback successCallback){
        this.loginCallback = successCallback;
        switch (sharemedia){
            case 0://QQ
                UMShareAPI.get(context).getPlatformInfo(getCurrentActivity(), SHARE_MEDIA.QQ, authListener) ;
                break;
            case 2://WX
                UMShareAPI.get(context).getPlatformInfo(getCurrentActivity(), SHARE_MEDIA.WEIXIN, authListener) ;
                break;
        }
    }

    @ReactMethod
    public void openMinProgram(){

        String appId = "wx8506a6d915bb3b58"; // 填移动应用(App)的 AppId，非小程序的 AppID
        IWXAPI api = WXAPIFactory.createWXAPI(context, appId);

        WXLaunchMiniProgram.Req req = new WXLaunchMiniProgram.Req();
        req.userName = "gh_33803f9942a5"; // 填小程序原始id
        req.miniprogramType = WXLaunchMiniProgram.Req.MINIPTOGRAM_TYPE_RELEASE;// 可选打开 开发版，体验版和正式版
        api.sendReq(req);
    }

    /**
     * android 6.0 以上需要动态申请权限
     */
    private void initPermission(String shareTitle, String shareIcon, String shareLink, String shareContent) {
        //Toast.makeText(getCurrentActivity(), "shareTitle:"+shareTitle+",shareIcon="+shareIcon+",shareLink:"+shareLink+",shareContent:"+shareContent, Toast.LENGTH_SHORT).show();
        if(Build.VERSION.SDK_INT>=23){
            String[] mPermissionList = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_LOGS,Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.SET_DEBUG_APP,
                    Manifest.permission.SYSTEM_ALERT_WINDOW,Manifest.permission.GET_ACCOUNTS,
                    Manifest.permission.WRITE_APN_SETTINGS};
            ActivityCompat.requestPermissions(getCurrentActivity(),mPermissionList,123);
        }
        UMImage image = new UMImage(getCurrentActivity(), shareIcon);//网络图片
        if(shareLink != null ){
            UMWeb web = new UMWeb(shareLink);
            web.setTitle(shareTitle);//标题
            web.setThumb(image);  //缩略图
            web.setDescription(shareContent);//描述
            new ShareAction(getCurrentActivity())
                    .setPlatform(SHARE_MEDIA.WEIXIN)//传入平台
                    .withMedia(web)
                    .setCallback(shareListener)//回调监听器
                    .share();
        }else{
            new ShareAction(getCurrentActivity())
                    .setPlatform(SHARE_MEDIA.WEIXIN)//传入平台
                    .withText(shareTitle)//分享标题
                    .withText(shareContent)//分享内容
                    .withMedia(image)
                    .setCallback(shareListener)//回调监听器
                    .share();
        }
    }

    /*@Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

    }*/


    private UMShareListener shareListener = new UMShareListener() {
        /**
         * @descrption 分享开始的回调
         * @param platform 平台类型
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {

        }

        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {
            callback.invoke(200,"分享成功");
        }

        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            callback.invoke(100,"分享失败");
        }

        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {
            callback.invoke(100,"取消分享");
        }
    };

    UMAuthListener authListener = new UMAuthListener() {
        /**
         * @desc 授权开始的回调
         * @param platform 平台名称
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {

        }

        /**
         * @desc 授权成功的回调
         * @param platform 平台名称
         * @param action 行为序号，开发者用不上
         * @param data 用户资料返回
         */
        @Override
        public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {
            loginCallback.invoke(200,data, "登陆成功");
            Toast.makeText(getCurrentActivity(), "成功:"+data.get("uid"), Toast.LENGTH_LONG).show();

        }

        /**
         * @desc 授权失败的回调
         * @param platform 平台名称
         * @param action 行为序号，开发者用不上
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, int action, Throwable t) {
            loginCallback.invoke(100,"","登陆失败");
            Toast.makeText(getCurrentActivity(), "失败:", Toast.LENGTH_LONG).show();
        }

        /**
         * @desc 授权取消的回调
         * @param platform 平台名称
         * @param action 行为序号，开发者用不上
         */
        @Override
        public void onCancel(SHARE_MEDIA platform, int action) {
            loginCallback.invoke(100,"","取消登陆");
            Toast.makeText(getCurrentActivity(), "取消登陆:", Toast.LENGTH_LONG).show();
        }
    };

}

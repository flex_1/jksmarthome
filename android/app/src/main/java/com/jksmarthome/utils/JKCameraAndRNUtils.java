package com.jksmarthome.utils;

import android.app.Activity;
import android.app.AppOpsManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.ezviz.stream.EZStreamClientManager;
import com.facebook.react.bridge.JSApplicationIllegalArgumentException;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.jksmarthome.MainApplication;
import com.jksmarthome.R;
import com.jksmarthome.ui.realplay.EZRealPlayActivity;
import com.jksmarthome.ui.util.ActivityUtils;
import com.jksmarthome.ui.util.EZUtils;
import com.videogo.constant.IntentConsts;
import com.videogo.errorlayer.ErrorInfo;
import com.videogo.exception.BaseException;
import com.videogo.exception.ErrorCode;
import com.videogo.openapi.EzvizAPI;
import com.videogo.openapi.bean.EZAccessToken;
import com.videogo.openapi.bean.EZCameraInfo;
import com.videogo.openapi.bean.EZDeviceInfo;
import com.videogo.util.LogUtil;
import com.videogo.util.Utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import ezviz.ezopensdk.demo.SdkInitParams;
import ezviz.ezopensdk.demo.SdkInitTool;
import ezviz.ezopensdk.demo.ServerAreasEnum;
import ezviz.ezopensdk.demo.SpTool;
import ezviz.ezopensdk.demo.ValueKeys;

import static com.ezviz.stream.EZError.EZ_OK;
import static com.jksmarthome.MainApplication.getOpenSDK;

public class JKCameraAndRNUtils extends ReactContextBaseJavaModule {

    private Activity currentActivity = null;

    private ServerAreasEnum mCurrentServerArea = ServerAreasEnum.getAllServers().get(0);
    private Context appContext;

    public JKCameraAndRNUtils(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "JKCameraAndRNUtils";
    }

    /**
     * 从JS页面跳转到原生activity 同时也可以从JS传递相关数据到原生
     * @param deviceSerial
     */
    @ReactMethod
    public void openCameraView(String deviceSerial, String key, String token, Integer cameraNo, String cameraName,String baseUrl){
        LogUtil.d("JKCameraAndRNUtils", "JKCameraAndRNUtils: deviceSerial = " + deviceSerial + " , key = " + key +
                " , token = " + token + " , baseUrl = " + baseUrl);

        try{
            currentActivity = getCurrentActivity();

//            if (currentActivity != null && NativeUtils.isEmulator()) {
//                Toast.makeText(currentActivity, "该设备不支持摄像头功能", Toast.LENGTH_SHORT).show();
//                return;
//            }

//            if(SpTool.obtainValue(ValueKeys.SDK_INIT_PARAMS) != null) {
//                updateSDKParams(key, token, deviceSerial);
//                jumpToEZRealPlay(deviceSerial, baseUrl);
//            }else{
                onClickStartExperience(key, token, deviceSerial, cameraNo, cameraName, baseUrl);
//            }
        }catch(Exception e){
            throw new JSApplicationIllegalArgumentException(
                    "不能打开Activity : "+e.getMessage());
        }
    }

    @ReactMethod
    public void getSystemNoticeStatus(Promise promise) {
        promise.resolve(hasPermission("OP_POST_NOTIFICATION"));
    }

    @ReactMethod
    public void openSystemNoticeView(){
        currentActivity = getCurrentActivity();

        try {
            // 跳转到通知设置界面
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);

            //这种方案适用于 API 26, 即8.0（含8.0）以上可以用
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, currentActivity.getPackageName());
            intent.putExtra(Settings.EXTRA_CHANNEL_ID, currentActivity.getApplicationInfo().uid);

            //这种方案适用于 API21——25，即 5.0——7.1 之间的版本可以使用
            intent.putExtra("app_package", currentActivity.getPackageName());
            intent.putExtra("app_uid", currentActivity.getApplicationInfo().uid);

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            currentActivity.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            // 出现异常则跳转到应用设置界面：锤子
            Intent intent = new Intent();

            //下面这种方案是直接跳转到当前应用的设置界面。
            //https://blog.csdn.net/ysy950803/article/details/71910806
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", this.appContext.getPackageName(), null);
            intent.setData(uri);

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            this.appContext.startActivity(intent);
        }
    }

    private boolean hasPermission(String appOpsServiceId) {

        Context context = getReactApplicationContext();
        if (Build.VERSION.SDK_INT >= 24) {
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);
            return mNotificationManager.areNotificationsEnabled();
        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            AppOpsManager mAppOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            ApplicationInfo appInfo = context.getApplicationInfo();

            String pkg = context.getPackageName();
            int uid = appInfo.uid;
            Class appOpsClazz;

            try {
                appOpsClazz = Class.forName(AppOpsManager.class.getName());
                Method checkOpNoThrowMethod = appOpsClazz.getMethod("checkOpNoThrow", Integer.TYPE, Integer.TYPE,
                        String.class);
                Field opValue = appOpsClazz.getDeclaredField(appOpsServiceId);
                int value = opValue.getInt(Integer.class);
                Object result = checkOpNoThrowMethod.invoke(mAppOps, value, uid, pkg);
                return Integer.parseInt(result.toString()) == AppOpsManager.MODE_ALLOWED;
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    //跳转到播放页面
    public void jumpToEZRealPlay(String deviceSerial, String url, Integer cameraNo, String cameraName) {
        String baseUrl = url + "/";
        try {
            EZDeviceInfo deviceInfo = getOpenSDK().getDeviceInfo(deviceSerial);
            deviceInfo.setDeviceName(cameraName);

            if (deviceInfo.getCameraNum() <= 0 || deviceInfo.getCameraInfoList() == null || deviceInfo.getCameraInfoList().size() <= 0) {
                LogUtil.d("JKCameraAndRNUtils", "cameralist is null or cameralist size is 0");
                return;
            }
            if (deviceInfo.getCameraInfoList() != null) {
                LogUtil.d("JKCameraAndRNUtils", "cameralist have one camera");
                final EZCameraInfo cameraInfo = EZUtils.getCameraInfoFromDevice(deviceInfo, cameraNo);
                if (cameraInfo == null) {
                    return;
                }
                int ret = EZStreamClientManager.create(currentActivity.getApplication().getApplicationContext()).clearTokens();
                if (EZ_OK == ret) {
                    Log.i("JKCameraAndRNUtils", "clearTokens: ok");
                } else {
                    Log.d("JKCameraAndRNUtils", "clearTokens: fail");
                }

                EZAccessToken ezAccessToken = getOpenSDK().getEZAccessToken();
                Log.e("JKCameraAndRNUtils",  " -------- " + ezAccessToken.getAccessToken() );

                Intent intent = new Intent(currentActivity, EZRealPlayActivity.class);
                intent.putExtra(IntentConsts.EXTRA_CAMERA_INFO, cameraInfo);
                intent.putExtra(IntentConsts.EXTRA_DEVICE_INFO, deviceInfo);

                intent.putExtra(Constants.EXTRA_BASE_URL, baseUrl);
                currentActivity.startActivity(intent);
            }

        } catch (BaseException e) {
//            e.printStackTrace();
            ErrorInfo errorInfo = (ErrorInfo) e.getObject();
            LogUtil.debugLog("JKCameraAndRNUtils", "doInBackground Exception = " + errorInfo.toString());
            if (errorInfo.errorCode != 0) {
                onError(errorInfo.errorCode);
            }
        }
    }

    protected void onError(int errorCode) {
        switch (errorCode) {
            case ErrorCode.ERROR_WEB_SESSION_ERROR:
            case ErrorCode.ERROR_WEB_SESSION_EXPIRE:
                ActivityUtils.handleSessionException(currentActivity);
                break;
            case ErrorCode.ERROR_WEB_DEVICE_NOT_HAVE:
                Utils.showToast(currentActivity, R.string.device_not_have, errorCode);
                break;
            default:
                    /*if (mAdapter.getCount() == 0) {
                        mListView.setVisibility(View.GONE);
                        mNoCameraTipLy.setVisibility(View.GONE);
                        mCameraFailTipTv.setText(Utils.getErrorTip(EZRealPlayActivity.this, R.string.get_camera_list_fail, errorCode));
                        mGetCameraFailTipLy.setVisibility(View.VISIBLE);
                    } else {*/
                Utils.showToast(currentActivity, R.string.get_camera_list_fail, errorCode);
                //}
                break;
        }
    }

    /**
     * 通过AccessToken进行体验
     */
    public void onClickStartExperience(final String key, final String token, final String deviceSerial, final Integer cameraNo, final String cameraName,final String baseUrl) {

        initSDK(key, token);
        switchServerToCurrent();

        new Thread(new Runnable() {
            @Override
            public void run() {
                //showLoginAnim(true);
                if (checkAppKeyAndAccessToken()){
                    // 初始化成功后，保存相关信息
                    updateSDKParams(deviceSerial);

                    jumpToEZRealPlay(deviceSerial, baseUrl, cameraNo, cameraName);
                }
                //showLoginAnim(false);
            }
        }).start();

    }

    private void initSDK(String key, String token) {
        SdkInitParams sdkInitParams = new SdkInitParams();
        sdkInitParams.appKey = key;
        sdkInitParams.accessToken = token ;
        SdkInitTool.initSdk(currentActivity.getApplication(), sdkInitParams);
    }


    //更新SDK的key和token
    private void updateSDKParams(String deviceSerial) {
        SdkInitParams sdkInitParams = new SdkInitParams();
        sdkInitParams.deviceSerial = deviceSerial;
        sdkInitParams.serverAreaId = mCurrentServerArea.id;
        sdkInitParams.openApiServer = mCurrentServerArea.openApiServer;
        sdkInitParams.openAuthApiServer = mCurrentServerArea.openAuthApiServer;
        saveLastSdkInitParams(sdkInitParams);
    }

    private void switchServerToCurrent() {
        EzvizAPI.getInstance().setServerUrl(mCurrentServerArea.openApiServer, mCurrentServerArea.openAuthApiServer);
        Log.d("JKCameraAndRNUtils", "switched server area!!!\n" + mCurrentServerArea.toString());
    }

    /**
     * 保存上次sdk初始化的参数
     */
    private void saveLastSdkInitParams(SdkInitParams sdkInitParams) {
        SpTool.storeValue(ValueKeys.SDK_INIT_PARAMS, sdkInitParams.toString());
    }


    /**
     * 通过调用服务接口判断AppKey和AccessToken且有效
     * @return 是否依旧有效
     */
    private boolean checkAppKeyAndAccessToken() {
        boolean isValid = false;
        try {
            EzvizAPI.getInstance().getUserName();
            isValid = true;
        } catch (BaseException e) {
            e.printStackTrace();
            Log.d("JKCameraAndRNUtils", "Error code is " + e.getErrorCode());
            int errCode = e.getErrorCode();
            String errMsg;
            switch (errCode){
                case 400031:
                    errMsg = currentActivity.getApplicationContext().getString(ezviz.ezopensdkcommon.R.string.tip_of_bad_net);
                    break;
                default:
                    errMsg = currentActivity.getApplicationContext().getString(ezviz.ezopensdkcommon.R.string.login_expire);
                    break;
            }
            Toast.makeText(currentActivity, errMsg, Toast.LENGTH_SHORT).show();
        }
        return isValid;
    }
}

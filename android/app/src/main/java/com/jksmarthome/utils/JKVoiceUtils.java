package com.jksmarthome.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.baidu.aip.asrwakeup3.core.inputstream.InFileStream;
import com.baidu.aip.asrwakeup3.core.mini.AutoCheck;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.jksmarthome.MainApplication;

import com.baidu.aip.asrwakeup3.core.R;
import com.baidu.aip.asrwakeup3.core.inputstream.InFileStream;
import com.baidu.speech.EventListener;
import com.baidu.speech.EventManager;
import com.baidu.speech.EventManagerFactory;
import com.baidu.speech.asr.SpeechConstant;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class JKVoiceUtils extends ReactContextBaseJavaModule implements EventListener {

    private Activity currentActivity = null;

    private Context context;

    private EventManager wakeupManager;
    private EventManager asr;

    public JKVoiceUtils(ReactApplicationContext reactContext) {
        super(reactContext);
        context = reactContext;

    }

    @Override
    public String getName() {
        return "JKVoiceUtils";
    }

    @ReactMethod
    public void settingUp(){
        wakeupManager = EventManagerFactory.create(getCurrentActivity(), "wp");
        wakeupManager.registerListener(this);
        wakeupStart();

        asr = EventManagerFactory.create(getCurrentActivity(), "asr");
        asr.registerListener(this);
    }

    @ReactMethod
    public void startRecord(){
        recogizeStart();
    }

    @ReactMethod
    public void pauseRecongize(){
        if(asr == null){
            return;
        }
        asr.send(SpeechConstant.ASR_STOP, null, null, 0, 0);
        asr.send(SpeechConstant.ASR_CANCEL, null, null, 0, 0);
    }

    @ReactMethod
    public void stopRecongize(){
        if(asr == null){
            return;
        }
        asr.send(SpeechConstant.ASR_STOP, null, null, 0, 0);
        asr.send(SpeechConstant.ASR_CANCEL, null, null, 0, 0);
        asr.unregisterListener(this);
    }

    @ReactMethod
    public void stopWake(){
        if(wakeupManager == null){
            return;
        }
        wakeupManager.send(SpeechConstant.WAKEUP_STOP, null, null, 0, 0);
        wakeupManager.unregisterListener(this);
    }

    // 开始语音识别
    private void wakeupStart() {
        // 基于SDK唤醒词集成第2.1 设置唤醒的输入参数
        Map<String, Object> params = new TreeMap<String, Object>();
        params.put(SpeechConstant.ACCEPT_AUDIO_VOLUME, false);
        params.put(SpeechConstant.WP_WORDS_FILE, "assets:///WakeUp.bin");
        // "assets:///WakeUp.bin" 表示WakeUp.bin文件定义在assets目录下
        InFileStream.setContext(getCurrentActivity());
        String json = null; // 这里可以替换成你需要测试的json
        json = new JSONObject(params).toString();
        wakeupManager.send(SpeechConstant.WAKEUP_START, json, null, 0, 0);
    }

    private void recogizeStart() {

        Map<String, Object> params = new LinkedHashMap<String, Object>();
        String event = null;
        event = SpeechConstant.ASR_START; // 替换成测试的event

        // 基于SDK集成2.1 设置识别参数
        params.put(SpeechConstant.ACCEPT_AUDIO_VOLUME, false);
        // params.put(SpeechConstant.NLU, "enable");
        // params.put(SpeechConstant.VAD_ENDPOINT_TIMEOUT, 0); // 长语音

        // params.put(SpeechConstant.IN_FILE, "res:///com/baidu/android/voicedemo/16k_test.pcm");
         params.put(SpeechConstant.VAD, SpeechConstant.VAD_DNN);
         params.put(SpeechConstant.PID, 1537); // 中文输入法模型，有逗号

        /* 语音自训练平台特有参数 */
        // params.put(SpeechConstant.PID, 8002);
        // 语音自训练平台特殊pid，8002：模型类似开放平台 1537  具体是8001还是8002，看自训练平台页面上的显示
        params.put(SpeechConstant.LMID,12373); // 语音自训练平台已上线的模型ID，https://ai.baidu.com/smartasr/model
        // 注意模型ID必须在你的appId所在的百度账号下
        /* 语音自训练平台特有参数 */

        /* 测试InputStream*/
//         InFileStream.setContext(getCurrentActivity());
//         params.put(SpeechConstant.IN_FILE, "#com.baidu.aip.asrwakeup3.core.inputstream.InFileStream.createMyPipedInputStream()");

        // 请先使用如‘在线识别’界面测试和生成识别参数。 params同ActivityRecog类中myRecognizer.start(params);
        // 复制此段可以自动检测错误
//        (new AutoCheck(context, new Handler() {
//            public void handleMessage(Message msg) {
//                if (msg.what == 100) {
//                    AutoCheck autoCheck = (AutoCheck) msg.obj;
//                    synchronized (autoCheck) {
//                        String message = autoCheck.obtainErrorMessage(); // autoCheck.obtainAllMessage();
//
//                        // 可以用下面一行替代，在logcat中查看代码
//                        // Log.w("AutoCheckMessage", message);
//
//                    }
//                }
//            }
//        }, false)).checkAsr(params);
        String json = null; // 可以替换成自己的json
        json = new JSONObject(params).toString(); // 这里可以替换成你需要测试的json
        asr.send(event, json, null, 0, 0);
    }

    // 基于SDK唤醒词集成第4.1 发送停止事件
    private void wakeupStop() {
        wakeupManager.send(SpeechConstant.WAKEUP_STOP, null, null, 0, 0); //
    }

    // 向原生发送 事件
    public void sendEvent(String eventName, @Nullable Object params){
        getReactApplicationContext()
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);

//        ReactContext reactContext = MainApplication.getApplicationC().getReactNativeHost()
//                .getReactInstanceManager().getCurrentReactContext();
//        if (reactContext == null){
//            return;
//        }
//        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
//                .emit(eventName, params);
    }

    /**
     * android 6.0 以上需要动态申请权限
     */
    private void initPermission() {
        String[] permissions = {Manifest.permission.RECORD_AUDIO,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.INTERNET,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        ArrayList<String> toApplyList = new ArrayList<String>();

        for (String perm : permissions) {
            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(getCurrentActivity(), perm)) {
                toApplyList.add(perm);
                // 进入到这里代表没有权限.

            }
        }
        String[] tmpList = new String[toApplyList.size()];
        if (!toApplyList.isEmpty()) {
            ActivityCompat.requestPermissions(getCurrentActivity(), toApplyList.toArray(tmpList), 123);
        }
    }

    @Override
    public void onEvent(String name, String params, byte[] data, int offset, int length) {

        if(name.equals(SpeechConstant.CALLBACK_EVENT_WAKEUP_SUCCESS)){
            sendEvent("VocieWakeUpNotification", 1);
        }else if (name.equals(SpeechConstant.CALLBACK_EVENT_ASR_PARTIAL)) {
            // 识别相关的结果都在这里
            if (params == null || params.isEmpty()) {
                return;
            }
            if (params.contains("\"nlu_result\"")) {
                // 一句话的语义解析结果
                if (length > 0 && data.length > 0) {

                }
            } else if (params.contains("\"partial_result\"")) {

                params=params.substring(0,params.length()-1);
                params+=",\"isFinished\":false";
                params+="}";
                // 一句话的临时识别结果
                sendEvent("VocieSendTextNotification", params);
                sendEvent("VocieWakeUpNotification", 2);

            }  else if (params.contains("\"final_result\""))  {

                params=params.substring(0,params.length()-1);
                params+=",\"isFinished\":true";
                params+="}";
                // 一句话的最终识别结果
                sendEvent("VocieSendTextNotification", params);
                sendEvent("VocieWakeUpNotification", 3);

            }  else {
                // 一般这里不会运行
                if (data != null) {

                }
            }
        } else {
            if(name.equals(SpeechConstant.CALLBACK_EVENT_ASR_ERROR)){
                sendEvent("VocieWakeUpNotification", 0);
            }
            if(params != null && params.contains("\"sub_error\"")){
                sendEvent("VocieWakeUpNotification", 0);
            }

            // 识别开始，结束，音量，音频数据回调
            if (params != null && !params.isEmpty()){

            }
            if (data != null) {

            }
        }
    }
}

package com.jksmarthome.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.jksmarthome.R;

import static com.jksmarthome.utils.Constants.DATA_NAME_USER_TOKEN;
import static com.jksmarthome.utils.Constants.USER_TOKEN;

/**
 * 原生方法合集
 */
public class NativeUtils {

    /**
     * 获取存储的用户token
     */
    public static String getUserToken(Activity activity) {
        String userToken = "";

        try {
            if (activity == null) {
                return null;
            }

            SharedPreferences sharedPreferences = activity.getSharedPreferences(DATA_NAME_USER_TOKEN, Context.MODE_PRIVATE);
            userToken = sharedPreferences.getString(USER_TOKEN,"");

        } catch (Exception e) {

        }
        return userToken;
    }

    //设置Activity对应的顶部状态栏的颜色 支持API 21（5.0）以上
    public static void setWindowStatusBarColor(Activity activity) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = activity.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                //白色背景
                window.setStatusBarColor(activity.getResources().getColor(R.color.white));
                //黑色字体
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //判断ABIS是x86
    public static boolean isEmulator() {
        String abi = null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            abi = Build.CPU_ABI;
        } else {
            abi = Build.SUPPORTED_ABIS[0];
        }

        if (abi.equals("x86") || abi.equals("x86_64")) {
            return true;
        }

        return false;
    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(Activity activity, float dpValue) {
        final float scale = activity.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    //获取屏幕宽高
    public void getScr(Activity activity) {
        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        final int width = dm.widthPixels;         // 屏幕宽度（像素）
        int height = dm.heightPixels;       // 屏幕高度（像素）
        float density = dm.density;         // 屏幕密度（0.75 / 1.0 / 1.5）
        int densityDpi = dm.densityDpi;     // 屏幕密度dpi（120 / 160 / 240）
        // 屏幕宽度算法:屏幕宽度（像素）/屏幕密度
        int screenWidth = (int) (width / density);  // 屏幕宽度(dp)
        int screenHeight = (int) (height / density);// 屏幕高度(dp)
    }

}

package com.jksmarthome.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedprefUtil {

	/**
	 * 保存string类型值
	 * @param ctx
	 * @param key
	 * @param value
	 */
	public static void save(Context ctx , String key , String value){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx) ;
		SharedPreferences.Editor editor = settings.edit() ;
		editor.putString(key, value) ;
		editor.commit() ;
	}
	
	
	/**
	 * 保存int类型值
	 * @param ctx
	 * @param key
	 * @param value
	 */
	public static void save(Context ctx , String key , int value){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx) ;
		SharedPreferences.Editor editor = settings.edit() ;
		editor.putInt(key, value) ;
		editor.commit() ;
	}
	
	/**
	 * 保存long类型值
	 * @param ctx
	 * @param key
	 * @param value
	 */
	public static void savelong(Context ctx , String key , long value){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx) ;
		SharedPreferences.Editor editor = settings.edit() ;
		editor.putLong(key, value) ;
		editor.commit() ;
	}
	
	/**
	 * 保存float类型值
	 * @param ctx
	 * @param key
	 * @param value
	 */
	public static void save(Context ctx , String key , float value){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx) ;
		SharedPreferences.Editor editor = settings.edit() ;
		editor.putFloat(key, value) ;
		editor.commit() ;
	}
	
	/**
	 * 保存boolean类型的值
	 * @param ctx
	 * @param key
	 * @param value
	 */
	public static void saveBoolean(Context ctx , String key , boolean value){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx) ;
		SharedPreferences.Editor editor = settings.edit() ;
		editor.putBoolean(key, value) ;
		editor.commit() ;
	}
	
	/**
	 * 获取string类型值
	 * @param ctx
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static String get(Context ctx, String key, String defValue){
		if (ctx == null) {
			return "";
		}
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx) ;
		return settings.getString(key, defValue) ;
	}
	
	/**
	 * 获取int类型的值
	 * @param ctx
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static int get(Context ctx, String key, int defValue){
		if (ctx == null) {
			return -1;
		}
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx) ;
		return settings.getInt(key, defValue) ;
	}
	
	/**
	 * 获取long类型的值
	 * @param ctx
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static long getlong(Context ctx, String key, long defValue){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx) ;
		return settings.getLong(key, defValue) ;
	}
	/**
	 * 获取float的值
	 * @param ctx
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static float get(Context ctx, String key, float defValue){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx) ;
		return settings.getFloat(key, defValue) ;
	}
	
	/**
	 * 获取boolean类型的值
	 * @param ctx
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static boolean getBoolean(Context ctx, String key, boolean defValue){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx) ;
		return settings.getBoolean(key, defValue) ;
	}

	/**
	 * 2.6及之前版本，视频页面截图都存到了本地
	 * 2.7版本开始要将图片全部上传到服务器，上传完清除掉本地的数据
	 */
	public static void clear(Context ctx) {
		SharedPreferences sharedPreferences = ctx.getSharedPreferences(ctx.getPackageName() + "_preferences", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.clear();
		editor.commit();
	}

}

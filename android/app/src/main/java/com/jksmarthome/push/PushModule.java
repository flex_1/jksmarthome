package com.jksmarthome.push;

import android.util.Log;
import androidx.annotation.Nullable;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import javax.annotation.Nonnull;

/**
 * 推送相关
 */
public class PushModule extends ReactContextBaseJavaModule {

    private static final String TAG = "PushModule_Log";

    private final static String USER_MODULE_NAME = "PushModule";

    public static ReactContext mReactContext;

    public PushModule(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
        mReactContext = reactContext;
    }

    @Nonnull
    @Override
    public String getName() {
        return USER_MODULE_NAME;
    }

    //定义发送事件的函数
    public static void sendEvent(String eventName, @Nullable WritableMap params) {
        if (mReactContext == null) {
            Log.e(TAG, "mReactContext == null");
        }else{
//            Log.e(TAG,"sendADEvent: "+ params.toString());
            mReactContext
                    .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                    .emit(eventName, params);
        }
    }

}

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    NativeModules, 
    Dimensions, 
    DeviceEventEmitter,
    View,
    BackHandler, 
    AppState
} from 'react-native';
import LocalTokenHandler from '../util/LocalTokenHandler';
import AsyncStorage from '@react-native-community/async-storage';
import JPush from 'jpush-react-native';
import {NotificationKeys, LocalStorageKeys} from '../common/Constants';
import DoubulePicker from 'react-native-picker';
import GuidePage from './CommonPage/GuidePage';
import SplashScreen from 'react-native-splash-screen';
import {MainControler,LoginControler} from './Controllers';
import AppConfigs from '../util/AppConfigs';
import { connect } from 'react-redux';
import { updateAppTheme } from '../redux/actions/ThemeAction';
import CustomPicker from '../common/CustomComponent/CustomPicker';
import ToastManager from '../common/CustomComponent/ToastManager';
import VoicePop from '../component/Voice/VoicePop';
import VoiceAnimateView from './Voice/VoiceAnimateView';
import {DraggableComponent} from '../common/CustomComponent/DraggableManager';
import deviceInfo from '../util/DeviceInfo';
import netCustom from '../util/NetworkCustom';
// import VoiceForAndroidBackGround from './Voice/VoiceForAndroidBackGround';

const JKRNUtils = NativeModules.JKRNUtils;

const JKUser = Platform.select({
    ios: NativeModules.JKRNUtils,
    android: NativeModules.UserModule
});
const JKUMengShare = Platform.select({
    ios: NativeModules.UMShareModule,
    android: NativeModules.UMShareModule
})
// 安卓语音 悬浮按钮
// const FloatButton = Platform.select({
//     ios: null,
//     android: NativeModules.FloatButtonModule
// })

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: null,
            showGuidePage: false,
            appState: AppState.currentState
        }
        window.DoubulePicker = DoubulePicker
        window.CustomPicker = CustomPicker
    }

    // 处理安卓返回按钮
    onBackAndroid = () => {
        if (this.lastBackPressed && this.lastBackPressed + 1500 >= Date.now()) {
            BackHandler.exitApp();
        }else{
            this.lastBackPressed = Date.now();
            ToastManager.show('再按一次退出应用', 1000);
            return true;
        }
    }

    componentWillMount() {
        // 初始化网络参数(仅本地环境)
        netCustom.getAndSaveUrl()
        
        // App版本监测
        this.checkVersion();

        //检查原生代码中存储的token, 主要是做2.6及之前版本的兼容
        AppConfigs.checkNativeToken();

        //ios 配置shortCuts
        AppConfigs.checkShortCuts()

        //配置 暗黑模式
        this.getAppTheme()

        // 获取 登录信息
        this.getLocalTokenInfo();

        //登录通知
        DeviceEventEmitter.addListener(NotificationKeys.kLoginNotification, () => {
            this.setState({
                isLogin: true
            })
            // 配置极光推送
            this.initJpush()
            //存储原生中的用户token
            AppConfigs.checkNativeToken();
            AppConfigs.checkShortCuts()
            // if(Platform.OS == 'android'){
            //     let androidRecorder = new VoiceForAndroidBackGround()
            //     androidRecorder.prepareForRecord()
            // }
        }); 

        //登出通知
        DeviceEventEmitter.addListener(NotificationKeys.kLogoutNotification, () => {
            // 清除可能存在的 可选弹框
            window.DoubulePicker.hide()
            setTimeout(() => {
                this.setState({
                    isLogin: false
                })
            }, 200);
            //清除原生中的用户token
            JKUser.clearUserToken();
            if(Platform.OS == 'ios'){
                JKRNUtils.removeShortCutsRegister()
            }
        });
    }

    componentDidMount() {
        // 监听App状态
        AppState.addEventListener("change", this._handleAppStateChange);

        if (Platform.OS === 'android'){
            BackHandler.addEventListener('hardwareBackPress', this.onBackAndroid);
        }
    }

    componentWillUnmount() {
        AppState.removeEventListener("change", this._handleAppStateChange);
        DeviceEventEmitter.removeAllListeners(NotificationKeys.kLoginNotification)
        DeviceEventEmitter.removeAllListeners(NotificationKeys.kLogoutNotification)
        JPush.removeListener(this.connectListener)
        JPush.removeListener(this.notificationListener)
        if (Platform.OS === 'android'){
            BackHandler.removeEventListener('hardwareBackPress',this.onBackAndroid)
        }
    }

    // 处理App状态
    _handleAppStateChange = nextAppState => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            console.log('App从后台进入前台。')
            if(Platform.OS == 'android'){
                // 解决安卓从后台进入前台后 无法连接websocket的BUG。
                DeviceEventEmitter.emit(NotificationKeys.kWebsocketReconnectNotification)
            }
        }
        this.setState({ appState: nextAppState });
    }

    //配置极光推送
    initJpush(){
        JPush.init();
        JPush.setBadge({badge:0, appBadge:0});
        //连接状态
        this.connectListener = result => {
            console.log("connectListener:" + JSON.stringify(result))
        };
        JPush.addConnectEventListener(this.connectListener);
        //通知回调
        this.notificationListener = result => {
            console.log("notificationListener:" + JSON.stringify(result))
            this.handleTheNotification(result)
        };
        JPush.addNotificationListener(this.notificationListener);
    }

    // 处理点击通知推送的回调
    handleTheNotification(result){
        JPush.setBadge({badge:0, appBadge:0});
        if(result && result.notificationEventType == 'notificationOpened'){
            // 点击推送
            setTimeout(() => {
                DeviceEventEmitter.emit(NotificationKeys.kJpushMessageNotification)
            }, 500);
        }else if(result && result.notificationEventType == 'notificationArrived'){
            //应用在前台收到推送

        }
    }

    // App主题
    async getAppTheme(){
        let appThemeIsDark = await AsyncStorage.getItem(LocalStorageKeys.kAppThemeIsDark)
        if(appThemeIsDark == '1'){
            this.props.updateAppTheme(true)
        }
    }

    // 检查看APP是否要显示开机引导页
    // async getOpenGuideInfo(){
    //     let openGuideInfo = await AsyncStorage.getItem(LocalStorageKeys.kAppGuidePageInfo)
    //     //openGuideInfo = null
    //     if(!openGuideInfo){
    //         // 引导页 从未出现过
    //         this.setState({
    //             showGuidePage: true
    //         },()=>{
    //             SplashScreen.hide()
    //         })
    //         let guideInfo = DeviceInfo.getVersion()
    //         AsyncStorage.setItem(LocalStorageKeys.kAppGuidePageInfo,guideInfo)
    //     }else{
    //         this.getLocalTokenInfo()
    //     }
    // }

    //获取本地token
    async getLocalTokenInfo() {
        const userToken = await LocalTokenHandler.get()
        const isLogin = userToken ? true : false
        this.setState({
            isLogin: isLogin
        },()=>{
            SplashScreen.hide()
        })
        if(isLogin){
            // 配置极光推送
            this.initJpush()
        }
    }

    // 看用户是否点击过用户协议,如果点击过才更新版本
    async checkVersion(){
        if(Platform.OS == 'ios'){
            deviceInfo.init()
            AppConfigs.checkVersion()
            return
        }
        let info = await AsyncStorage.getItem(LocalStorageKeys.kLoginAgreement)
        if(info){
            deviceInfo.init()
            JKUMengShare.init()
            AppConfigs.checkVersion()
        }
    }

    skipGuide(){
        this.setState({
            showGuidePage: false,
            isLogin: false
        })
    }

    renderDraggable(){
        return(
            <DraggableComponent
                onPress={()=>{
                    this.voiceAnimate._startAnimated()
                }}
            />
        )
    }

    renderVoiceAnimateView(){
        return(
            <VoiceAnimateView 
                ref={e => this.voiceAnimate = e}
                isDark={this.props.themeInfo.isDark}
            />
        )
    }

    render() {
        if(this.state.isLogin == null){
            return <View/>
        }
        if (this.state.showGuidePage){
            return <GuidePage skipPress={()=>{ this.skipGuide() }}/>
        } else if(this.state.isLogin){
            return (
                <View style={{flex: 1}}>
                    <MainControler/>
                    {/* <VoicePop/> */}
                    {this.renderDraggable()}
                    {this.renderVoiceAnimateView()}
                </View>
            )
        } else{
            return <LoginControler/>
        }        
    }
}

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    }),
    (dispatch) => ({
        updateAppTheme: (isDark)=> dispatch(updateAppTheme(isDark)),
    })
)(App)



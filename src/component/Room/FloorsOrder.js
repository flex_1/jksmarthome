/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	FlatList,
	Dimensions,
	DeviceEventEmitter,
	RefreshControl
} from 'react-native';
import {Colors, NetUrls, NotificationKeys} from '../../common/Constants';
import {postJson} from "../../util/ApiRequest";
import ToastManager from "../../common/CustomComponent/ToastManager";
import {ListLoading,ListError } from "../../common/CustomComponent/ListLoading";
import { connect } from 'react-redux';
import SpinnerManager from '../../common/CustomComponent/SpinnerManager';
import {AutoDragSortableView, DragSortableView} from 'react-native-drag-sort';
import DraggableGrid from '../../third/Sort';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { BottomSafeMargin } from '../../util/ScreenUtil';

const screenW = Dimensions.get('window').width;
const screenH = Dimensions.get('window').height;

// 暂未使用
class FloorsOrder extends Component{

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'楼层排序'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text:'保存',onPress:()=>{
                        
                    }},
                ]}/>
            ),
            headerBackground: <HeaderBackground/>
        }
    };

	constructor(props) {
		super(props);
        const {getParam,setParams} = this.props.navigation
		this.state = {
			listData: this.getFloorListData(getParam('floors')),
            allowScroll: true
		}
	}

	componentDidMount(){
		
	}

	componentWillUnmount(){
        
    }

    getFloorListData(floors){
        let listData = []
        
        for (const floor of floors) {
            
            listData.push({name:floor.floorText, key:floor.floor})
        }
        return listData
    }
    
	// 根据 楼层获取房间列表
	async requestRoomList() {
		let params = {}
		if(this.props.currentFloor){
			params.floor = this.props.currentFloor
		}
		try {
			let data = await postJson({
				url: NetUrls.roomlistDetails,
				params: params
			});
			this.setState({
				loading: false,
				isRefreshing: false,
			})
			if (data.code == 0) {
				this.setState({
					listData: data.result || []
				})
			} else {
				ToastManager.show(data.msg);
			}
		} catch (e) {
			this.setState({
				isRefreshing:false,
				loading: false
			})
			ToastManager.show(JSON.stringify(e));
		}
    }

    // 房间排序
	async requestRoomSort(type, roomId) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.roomSort,
				params: {
                    floor: this.props.currentFloor,
                    type: type,
                    roomId: roomId
                }
			});
			SpinnerManager.close()
			if (data.code == 0) {
                this.setState({orderingId: roomId})
				this.requestRoomList()
			} else {
				ToastManager.show(data.msg);
			}
		} catch (e) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(e));
		}
    }

    renderItem(name, index){
        return(
            <View
                style={styles.item}
                key={index}
            >
                <Text style={styles.item_text}>{name}</Text>
            </View>
        )
    }

    setData(data){

    }

	getFloorList(){
		let roomData = this.state.listData

        // return(
        //     <AutoDragSortableView
        //         dataSource={roomData}
        //         parentWidth={300}
        //         childrenWidth= {280}
        //         childrenHeight={50}
        //         keyExtractor={(item,index)=> index}
        //         renderItem={(item,index)=>{
        //             return this.renderItem(item,index)
        //         }}
        //     />
        // )

		// 正常列表
		return (
            <DraggableGrid
                numColumns={4}
                renderItem={({ name, key }) => this.renderItem(name, key)}
                data={roomData}
                onDragStart={()=>{
                    this.setState({allowScroll: false})
                }}
                onDragRelease={(data) => {
                    this.setState({
                        listData: data,
                        allowScroll: true
                    });
                    console.log(data);
                }}
            />
        )
	}

	render(){
		return (
            <ScrollView
                contentContainerStyle={styles.contentStyle}
                scrollEnabled={this.state.allowScroll} 
            >
                <View style={styles.wrapper}>
                    {this.getFloorList()}
                </View>
            </ScrollView>
        )
	}
}

const styles = StyleSheet.create({
    contentStyle:{
        paddingBottom: BottomSafeMargin + 20
    },
    wrapper:{
        paddingTop:20,
        width:'100%',
        height:'100%',
        justifyContent:'center',
        paddingHorizontal: 16
      },
      item:{
        width: screenW*0.2,
        height: 50,
        borderRadius:8,
        backgroundColor: Colors.themPurpe,
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal: 5
      },
      item_text:{
        fontSize: 16,
        color:'#FFFFFF',
        textAlign: 'center'
      },
});


export default FloorsOrder


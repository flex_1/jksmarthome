/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	FlatList,
	Dimensions,
	DeviceEventEmitter,
	RefreshControl
} from 'react-native';
import {Colors, NetUrls, NotificationKeys} from '../../common/Constants';
import {postJson} from "../../util/ApiRequest";
import ToastManager from "../../common/CustomComponent/ToastManager";
import {ListLoading,ListError,FooterLoading,FooterEnd } from "../../common/CustomComponent/ListLoading";
import {ColorsLight, ColorsDark, ImagesDark, ImagesLight} from '../../common/Themes';
import SpinnerManager from '../../common/CustomComponent/SpinnerManager';

const screenW = Dimensions.get('window').width;
//房间背景图的宽度
const roomBgW = (screenW - 32); // 32 为两边间距
//房间背景图的高度
const roomBgH = roomBgW * 0.618;
// 大图与缩略图 模式切换通知
const RoomThumbChangeNoti = 'RoomThumbChangeNoti'

class RoomList extends Component{

	constructor(props) {
		super(props);
		this.state = {
			listData: null,
			loading: true,
            isRefreshing: false,
            numColumns: props.numColumns || 1,
            orderingId: null,
            currentPage: 0,
            totalPage: 0
		}
	}

	componentDidMount(){
		this.requestRoomList()
		// 房间信息发生变化
		this.roomInfoChangeNoti = DeviceEventEmitter.addListener(NotificationKeys.kRoomInfoNotification,()=>{
            // this.setState({loading: true})
			this.requestRoomList()
        })
        // 楼层缩略图模式 切换通知
        this.thumbModelChangeNoti = DeviceEventEmitter.addListener(RoomThumbChangeNoti,(number)=>{
            this.setState({ numColumns: number })
        })
	}

    componentWillReceiveProps(nextProps){
        if(!nextProps.isOrderEdit){
            this.state.orderingId = null
        }else{
            SpinnerManager.show()
            this.requestRoomList({isOrderByFloor:1, page:1})
        }
    }

	componentWillUnmount(){
        this.roomInfoChangeNoti.remove()
        this.thumbModelChangeNoti.remove()
    }
    
	// 根据 楼层获取房间列表
	async requestRoomList(params) {
		params = params || {}
		if(this.props.currentFloor){
			params.floor = this.props.currentFloor
		}
        if(this.props.isOrderEdit){
            params.isOrderByFloor = 1
            params.page = 1
        }
		try {
			let data = await postJson({
				url: NetUrls.roomlistDetails,
				params: params
			});
            SpinnerManager.close()
			this.setState({
				loading: false,
				isRefreshing: false,
			})
			if (data.code == 0) {
                let roomList = []
                if(params.page && params.page > 1){
                    roomList = this.state.listData.concat(data.result.list)
                }else{
                    roomList = data.result?.list || []
                }
				this.setState({
					listData: roomList,
                    currentPage: data.result?.curPage,
                    totalPage: data.result?.totalPageNum
				})
			} else {
				ToastManager.show(data.msg);
			}
		} catch (e) {
			this.setState({
				isRefreshing:false,
				loading: false
			})
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(e));
		}
    }

    // 房间排序
	async requestRoomSort(type, roomId) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.roomSort,
				params: {
                    floor: this.props.currentFloor,
                    type: type,
                    roomId: roomId
                }
			});
			if (data.code == 0) {
                this.setState({orderingId: roomId})
				this.requestRoomList()
			} else {
                SpinnerManager.close()
				ToastManager.show(data.msg);
			}
		} catch (e) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(e));
		}
    }

    onRefresh(){
        if (this.state.isRefreshing) return
        this.setState({ isRefreshing: true })
        this.requestRoomList()
    }

    //加载下一页
    onLoadNextPage() {
        if(this.state.loading){
            return
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return
        }
        this.requestRoomList({page: this.state.currentPage+1})
    }
    
    // 获取楼层房间信息
    _renderFloorText(rowData){
        const Colors = this.props.isDark ? ColorsDark : ColorsLight

        return (
            <Text style={[styles.floorText,{color: Colors.themeTextLight}]}>
                {rowData.floorText}
            </Text>
        )
    }

    // 获取 value 数值信息
    _renderValueText(rowData, isTable){
        if(!rowData.roomDesc){
            return null
        }
        if(this.props.isOrderEdit){
            return null
        }
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        let leftSpace = rowData.floor ? '   ' : ''
        if(isTable){
            leftSpace = ''  
        }
        return(
            <Text style={[styles.valueText,{color: Colors.themeTextLight}]}>
                {leftSpace + rowData.roomDesc}
            </Text>
        )
    }

    //获取设备开启数
    _renderDeviceCount(rowData, isTable){
        if(rowData.deviceCount == null){
            return null
        }
        if(this.props.isOrderEdit){
            return null
        }
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        let leftSpace = rowData.floor ? '   ' : ''
        if(isTable){
            leftSpace = ''  
        }
        return(
            <Text style={[styles.valueText,{color: Colors.themeTextLight}]}>
                {leftSpace}开启设备: {rowData.runDeviceCount}/{rowData.deviceCount}
            </Text>
        )
    }

    renderRowSettings(roomId, goToSetting){
        // 普通成员无法设置房屋
        if(this.props.isJuniorUser){
            return null
        }

        const isOrderEdit = this.props.isOrderEdit
        const tintColor = this.props.isDark ? {tintColor: Colors.white} : {}

        if(isOrderEdit){
            return(
                <View style={styles.orderWrapper}>
                    <TouchableOpacity style={styles.orderBtnTouch} onPress={()=>{
                        this.requestRoomSort(1,roomId)
                    }}>
                        <Image style={[styles.orderImg,tintColor]} source={require('../../images/room/top.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.orderBtnTouch} onPress={()=>{
                        this.requestRoomSort(2,roomId)
                    }}>
                        <Image style={[styles.orderImg,tintColor]} source={require('../../images/room/up.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.orderBtnTouch} onPress={()=>{
                        this.requestRoomSort(3,roomId)
                    }}>
                        <Image style={[styles.orderImg,tintColor]} source={require('../../images/room/down.png')}/>
                    </TouchableOpacity>
                </View>
            )
        }else{
            return(
                <TouchableOpacity style={styles.listSettingTouch} onPress={()=>{ goToSetting() }}>
                    <Image style={styles.settingIcon} source={require('../../images/setting_light.png')}/>
                </TouchableOpacity>
            )
        }
    }

    renderTableSettings(roomId, goToSetting){
        // 普通成员无法设置房屋
        if(this.props.isJuniorUser){
            return null
        }
        
        const isOrderEdit = this.props.isOrderEdit
        const tintColor = this.props.isDark ? {tintColor: Colors.white} : {}

        if(isOrderEdit){
            return(
                <View style={{flexDirection: 'row',position:'absolute',top:0,right:0,height:'100%',width:'70%'}}>
                    <TouchableOpacity style={[styles.tableOrderTouch,{width:'50%'}]} onPress={()=>{
                        this.requestRoomSort(1,roomId)
                    }}>
                        <Image style={[styles.orderImg,tintColor]} source={require('../../images/room/top.png')}/>
                    </TouchableOpacity>
                    <View style={{height:'100%',width:'50%'}}>
                        <TouchableOpacity style={[styles.tableOrderTouch,{width:'100%'}]} onPress={()=>{
                            this.requestRoomSort(2,roomId)
                        }}>
                            <Image style={[styles.orderImg,tintColor]} source={require('../../images/room/up.png')}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.tableOrderTouch,{width:'100%'}]} onPress={()=>{
                            this.requestRoomSort(3,roomId)
                        }}>
                            <Image style={[styles.orderImg,tintColor]} source={require('../../images/room/down.png')}/>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }else{
            return(
                <TouchableOpacity style={styles.tableSettingTouch} onPress={()=>{ goToSetting() }}>
                    <Image style={styles.settingIcon} source={require('../../images/setting_light.png')}/>
                </TouchableOpacity>
            )
        }
    }

    // 长条
    renderListItem(rowData, goToDetail, goToSetting){
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        const defaultImg = this.props.isDark ? ImagesDark.default : ImagesLight.default
        const roomIcon = this.props.isDark ? rowData.nightImg : rowData.dayImg
        let orderingBorder = {}
        if(this.props.isOrderEdit && this.state.orderingId && (rowData.id == this.state.orderingId)){
            orderingBorder = {borderColor:Colors.newTheme, borderWidth: 1}
        }
        
        return(
            <View style={styles.itemWrapper}>
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={[styles.itemTouch,orderingBorder,{backgroundColor: Colors.themeBg}]} 
                    onPress={() => { goToDetail() }}
                >
                    <Image style={styles.img} defaultSource={defaultImg} source={{uri: roomIcon}}/>
                    <View style={{marginLeft: 15,flex:1}}>
                        <View style={{flexDirection: 'row',alignItems:'center'}}>
                            <Text style={[styles.name,{color: Colors.themeText}]}>{rowData.name}</Text>
                            {rowData.isHuman ? <Image style={styles.humanIcon} source={require('../../images/roomMan.png')}/> : null} 
                        </View>
                        <View style={{marginTop: 5,flexDirection:'row',alignItems:'center'}}>
                            {this._renderFloorText(rowData)}
                            {this._renderValueText(rowData)}
                            {this._renderDeviceCount(rowData)}
                        </View>
                    </View>
                    {this.renderRowSettings(rowData.id, goToSetting)}
                </TouchableOpacity>
            </View>
        )
    }

    // 方格
    renderTableItem(rowData, index, goToDetail, goToSetting){
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        const defaultImg = this.props.isDark ? ImagesDark.default : ImagesLight.default
        const roomIcon = this.props.isDark ? rowData.nightImg : rowData.dayImg
        let orderingBorder = {}
        if(this.props.isOrderEdit && this.state.orderingId && (rowData.id == this.state.orderingId)){
            orderingBorder = {borderColor:Colors.newTheme, borderWidth: 1}
        }
        
        return(
            <View style={[styles.thumbItemWrapper,{paddingLeft: index%2 ? 8:16, paddingRight: index%2 ? 16:8}]}>
                <TouchableOpacity 
                    style={[styles.thumbItem,orderingBorder,{backgroundColor: Colors.themeBg}]} 
                    activeOpacity={1} 
                    onPress={()=>{ goToDetail() }}
                >
                    <View style={{flexDirection: 'row',alignItems:'flex-end'}}>
                        <Image style={styles.tableImg} defaultSource={defaultImg} source={{uri: roomIcon}}/>
                        {this._renderFloorText(rowData)}
                    </View>
                    <View style={{flex: 1}}/>
                    <View style={{flexDirection: 'row',alignItems:'center',paddingRight: 5}}>
                        <Text style={[styles.name,{color: Colors.themeText}]}>{rowData.name}</Text>
                        {rowData.isHuman ? <Image style={styles.humanIcon} source={require('../../images/roomMan.png')}/> : null} 
                    </View>
                    <View style={{marginTop: 8,flexDirection:'row',alignItems:'center'}}>
                        {this._renderValueText(rowData, true)}
                        <View style={{flex:1}}/>
                        {this._renderDeviceCount(rowData, true)}
                    </View>
                    {this.renderTableSettings(rowData.id, goToSetting)}
                </TouchableOpacity>
            </View>
        )
    }

    //footer
    renderFooter() {
        if(!this.state.listData || this.state.listData.length <= 0){
            return null
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return <FooterEnd/>
        }
        return <FooterLoading/>
    }

	// 渲染每一行
	renderRowItem(rowData, index) {
        const {navigate} = this.props.navigation
        let roomImg = this.props.isDark ? rowData.nightImg : rowData.dayImg

        let goToDetail = ()=>{
            navigate('RoomDetail',{
                title: rowData.name,
                roomId: rowData.id,
                currentFloor: rowData.floor,
                name: rowData.name,
                roomImg: roomImg
            })
        }
        let goToSetting = ()=>{
            navigate('AddRoom',{
                title: '房间设置',
                roomId: rowData.id,
                currentFloor: rowData.floor,
                floorText: rowData.floorText,
                name: rowData.name,
                roomImg: roomImg,
                remark: rowData.remark
            })
        }
        if(this.state.numColumns == 1){
            return this.renderListItem(rowData, goToDetail, goToSetting)
        }else{
            return this.renderTableItem(rowData, index, goToDetail, goToSetting)
        }
	}

	//房间列表
	getRoomList(){
		let roomData = this.state.listData

		//正在加载
		if (this.state.loading && !roomData) {
			return <ListLoading/>
		}
		// 网络错误
		if (!roomData) {
			return <ListError onPress={()=>{ this.requestRoomList() }}/>
        }
        
		// 正常列表
		return (
            <FlatList
                contentContainerStyle={{paddingBottom: 10}}
                numColumns={this.state.numColumns}
                key={this.state.numColumns}
                scrollIndicatorInsets={{right: 1}}
                removeClippedSubviews={false}
                data={roomData}
                keyExtractor={(item, index)=> "roomInfo_index_" + index}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
                initialNumToRender={10}
                onEndReached={() => this.onLoadNextPage()}
                onEndReachedThreshold={0.1}
                ListFooterComponent={() => this.renderFooter()}
                refreshControl={
                    <RefreshControl
                        colors={[Colors.themeBGInactive]}
                        tintColor={Colors.themeBGInactive}
                        refreshing={this.state.isRefreshing}
                        onRefresh={this.onRefresh.bind(this)}
                    />
                }
            />
        )
	}

	render(){
		return this.getRoomList()
	}
}

const styles = StyleSheet.create({
    itemWrapper:{ 
		width: '100%', 
        marginTop: 15,
        justifyContent: 'center', 
        alignItems: 'center'
    },
    itemTouch: {
		flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 5,
        width: '90%',
        height: 70,
        paddingLeft: 16
    },
    img:{
        width: 40,
        height: 40,
        resizeMode: 'contain'
    },
    floorText:{
        fontSize: 13
    },
    valueText:{
        fontSize: 12
    },
    name:{
        fontSize: 16,
        fontWeight: 'bold'
    },
    listSettingTouch:{
        height: '100%',
        paddingHorizontal: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    settingIcon:{
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
    thumbItemWrapper:{
        width:'50%',
        height:120,
        marginTop: 15
    },
    thumbItem:{
        width:'100%',
        height:'100%',
        padding: 16,
        borderRadius: 4
    },
    tableImg:{
        width: 36,
        height: 36,
        resizeMode: 'contain',
        marginRight: 8
    },
    tableSettingTouch:{
        position:'absolute',
        top: 0,
        right: 0,
        padding: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },
    orderWrapper:{
        height: '100%',
        alignItems:'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    orderBtnTouch:{
        paddingHorizontal: 12,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    tableOrderTouch:{
        justifyContent: 'center',
        alignItems: 'center',
        height:'50%'
    },
    orderImg:{
        width:20,
        height:20,
        resizeMode:'contain'
    },
    humanIcon:{
        width:18,
        height:16,
        resizeMode: 'contain',
        marginLeft: 5
    }
});


export default RoomList


/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	Alert,
	Dimensions,
    NativeModules
} from 'react-native';
import { postJson, postPicture } from '../../util/ApiRequest';
import { NetUrls,NotificationKeys,NetParams } from '../../common/Constants';
import { Colors } from '../../common/Constants';
import ActionSheet from 'react-native-actionsheet';
import ToastManager from '../../common/CustomComponent/ToastManager';
import {DecimalTheNumber} from '../../util/NumberUtil';
import SceneList from '../Scene/SceneList';
import DeviceList from '../Home/DeviceList';
import SmartList from "../Smart/SmartList";
import FilterModal from '../CommonPage/FilterModal';
import {HSTips} from '../../common/Strings';

import { connect } from 'react-redux';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';

const screenW = Dimensions.get('window').width;
const roomBgW = screenW - 32; // 32 为两边间距
const roomBgH = roomBgW * 0.618;

const JKCameraRNUtils = Platform.select({
    ios: NativeModules.JKCameraRNUtils,
    android: NativeModules.JKCameraAndRNUtils
})

class RoomDetail extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {icon: require('../../images/share.png'),noImgTint:true,onPress:()=>{
                        navigation.getParam('shareBtnClick')()
                    }},
                    {icon: require('../../images/add.png'),onPress:()=>{
                        navigation.getParam('addRoomClick')()
                    }},
                ]}/>
            ),
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
    }

	constructor(props) {
		super(props);
		const {getParam,setParams} = this.props.navigation
		this.roomId = getParam('roomId')
		this.name = getParam('name')
        this.roomImg = getParam('roomImg')
		this.currentFloor = getParam('currentFloor')

		this.state = {
			roomData: null,
			isRefreshing: false,
			indicators: [{name:'场景',type:1},{name:'设备',type:2},{name:'智能',type:3}],
            currentIndicator:1,
            cameraInfo: null,
            numColumns: 2,
            enviInfo: {},

            filterVisible: false,
            filters: null,
            filterSelected: [],
            powerType: 0,  // 0-日能耗 1-周能耗 2-月能耗 3-年能耗
            specailFilters: null
        }
        
        setParams({
            addRoomClick: this.addRoomClick.bind(this),
            shareBtnClick: this.shareBtnClick.bind(this)
        })
	}

	componentDidMount() {
        this.requestRoomDetail()
        this.requestFilterCategory()
	}

	componentWillUnmount(){
		
    }
    
    addRoomClick(){
        if(this.props.userInfo.memberType == 2){
            Alert.alert(
                '提示',
                HSTips.limitsAddDeviceForRoom,
                [
                    {text: '知道了', onPress: () => {}, style: 'cancel'}
                ]
            )
        }else{
            this.setState({filterVisible: false})
		    this.device_actionSheet && this.device_actionSheet.show()
        }
        
    }

    shareBtnClick(){
        if(!this.state.roomData){
            ToastManager.show('数据获取中，请稍后再试。')
            return
        }
        if(!this.state.roomData.shareInfo){
            ToastManager.show('您无权限分享此房间。')
            return
        }

        const { navigate } = this.props.navigation
        let roomData = {roomImg: this.roomImg, name: this.name}

        navigate('Setting_Share',{
            title: '房间分享',
            id: this.roomId,
            roomData: roomData,
            shareData: this.state.roomData.shareInfo,
            shareType: 3,   // 1设备 2场景 3房间
        })
    }

	// 获取房间信息
	async requestRoomDetail() {
		try {
			let data = await postJson({
				url: NetUrls.roomDetail,
				params: {
					id: this.roomId
				}
			});
			if (data.code == 0) {
                // 环境数据
                let enviInfo = {}
                if(data.result.status){
                    enviInfo = JSON.parse(data.result.status)
                }
				this.setState({
					roomData: data.result || {},
                    cameraInfo: data.result && data.result.cameraInfo,
                    powerType: data.result.energyType || 0,
                    enviInfo: enviInfo
				})

			} else {
				ToastManager.show(data.msg);
			}
		} catch (e) {
			ToastManager.show(JSON.stringify(e));
		}
    }
    
    //获取筛选类型
    async requestFilterCategory(){
        try {
			let data = await postJson({
				url: NetUrls.natural_classification,
				params: {
                    roomId: this.roomId
				}
			});
			if (data.code == 0) {
                let filters = data.result?.categoryList || []
                let specailFilters = [{ nameEn: 'ONLINE', name: '在线', count: data.result?.onLineCount },
                                      { nameEn: 'OFFLINE', name: '离线', count: data.result?.offLineCount }]
                if(data.result?.hiddenCount > 0){
                    specailFilters.push({ nameEn: 'HIDDEN', name: '隐藏设备', count: data.result?.hiddenCount })
                }
                this.setState({
                    filters: filters,
                    specailFilters: specailFilters
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    //设置 日周月年 能耗
    async requestSettingEnergyType(){
        try {
			let data = await postJson({
				url: NetUrls.energyType,
				params: {
                    type: this.state.powerType
				}
			});
			if (data.code == 0) {
                
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    //判断是否有环境参数
    isExistEnviValues(){

    }

	getDeviceActionSheet() {
		return (
			<ActionSheet
				ref={e => this.device_actionSheet = e}
				options={['添加场景', '选择设备','添加智能','取消']}
				cancelButtonIndex={3}
				onPress={(index) => {
					const { navigate } = this.props.navigation
					if (index == 0) {
						navigate('AddScene', { title:'添加场景',roomId: this.roomId })
					} else if (index == 1) {
						navigate('RoomSelectDevice',{roomId: this.roomId})
					} else if (index == 2) {
                        navigate('AddSmart',{title:'新增智能', roomId:this.roomId})
                    }
				}}
			/>
		)
    }

    _renderFilterModal(){
        if(!this.state.filterVisible){
            return null
        }
        if(!this.state.filters || this.state.filters.length<=0){
            return null
        }

        return(
            <FilterModal
                style={{top: 50}}
                isDark={this.props.themeInfo.isDark}
                filters={this.state.filters}
                specailFilters={this.state.specailFilters}
                filterSelected={this.state.filterSelected}
                specialClass={'HIDDEN'}
                confirmBtnClick={(filterSelected)=>{
                    this.setState({
                        filterVisible: false,
                        filterSelected: filterSelected
                    },()=>{
                        // let params = { naturalClassification: filterSelected.toString() }
                        this.devices_list.reloadDeviceList()
                    })
                }}
                dissmissModal={()=>{
                    this.setState({filterVisible: false})
                }}
            />
        )
    }

    _renderFilterBtn(){
        if(!this.state.filters || this.state.filters.length<=0){
            return null
        }
        if(this.state.currentIndicator != 2){
            return null
        }

        const Colors = this.props.themeInfo.colors
        let tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}
        if(this.state.filterSelected && this.state.filterSelected.length>0){
            tintColor = {tintColor: Colors.newTheme}
        }

        return(
            <TouchableOpacity style={styles.cameraTouch} onPress={()=>{
                this.setState({filterVisible: !this.state.filterVisible})
            }}>
                <Image style={[styles.filterImg,tintColor]} source={require('../../images/deviceIcon/filter.png')}/>
            </TouchableOpacity>
        )
    }

    _renderRowLineBtn(){
        if(this.state.currentIndicator == 3){
            return null
        }
        const thumbIcon = this.state.numColumns == 1 ? require('../../images/homeBG/list.png') :
        require('../../images/homeBG/table.png')
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}

        return(
            <TouchableOpacity style={styles.rightTouchL} onPress={()=>{
                let target = this.state.numColumns%2 + 1
                this.setState({ numColumns: target })
            }}>
                <Image style={[styles.rightIcon,tintColor]} source={thumbIcon}/>
            </TouchableOpacity>
        )
    }
    
    // _renderCameraInfo(){
    //     if(!this.state.cameraInfo){
    //         return null
    //     }
    //     const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.white} : {}
    
    //     return(
    //         <TouchableOpacity style={styles.cameraTouch} onPress={()=>{
    //             // 摄像头 特殊处理
    //             let serialStr = this.state.cameraInfo.serialNumber
    //             let appKey = this.state.cameraInfo.ysAppKey
    //             let accessToken = this.state.cameraInfo.ysAccessToken
    //             if (Platform.OS === 'android') {
    //                 JKCameraRNUtils.openCameraView(serialStr,appKey,accessToken, NetParams.baseUrl);
    //                 return;
    //             }
    //             JKCameraRNUtils.openCameraView(serialStr,appKey,accessToken, this.props.themeInfo.isDark);
    //         }}>
    //             <Image style={[styles.cameraIcon,tintColor]} source={require('../../images/homeBG/camera.png')}/>
    //         </TouchableOpacity>
    //     )
    // }

    getInfo(num,imgSrc,level){
        if(num == null){
            return null
        }
        const Colors = this.props.themeInfo.colors
        let value = num
        let valueColor = Colors.themeText

        if(level == '1'){
            valueColor = Colors.lightGreen
        }else if(level == '2'){
            valueColor = Colors.yellow
        }else if(level == '3'){
            valueColor = Colors.red
        }
        return (
			<View style={styles.infoWrapper}>
				<Image style={[styles.infoIcon,{tintColor: Colors.themeText} ]} source={imgSrc} />
				<Text style={[styles.infoText,{color: valueColor}]}>{value}</Text>
			</View>
        )
    }

    // 能耗数据
    _renderPowerDetail(){
        let roomData = this.state.roomData || {}
        const Colors = this.props.themeInfo.colors
        const {daytotalEnergy,weektotalEnergy,monthtotalEnergy,yeartotalEnergy} = roomData

        if(!(daytotalEnergy || weektotalEnergy || monthtotalEnergy || yeartotalEnergy)){
            return null
        }

        let powerTypeText = '本日'
        let powerValue = daytotalEnergy==null ? '-': (daytotalEnergy+'kWh')
        if(this.state.powerType%4 == 1){
            powerTypeText = '本周'
            powerValue = weektotalEnergy==null ? '-': (weektotalEnergy+'kWh')
        }else if(this.state.powerType%4 == 2){
            powerTypeText = '本月'
            powerValue = monthtotalEnergy==null ? '-': (monthtotalEnergy+'kWh')
        }else if(this.state.powerType%4 == 3){
            powerTypeText = '本年'
            powerValue = yeartotalEnergy==null ? '-': (yeartotalEnergy+'kWh')
        }

        return(
            <View style={styles.powerWrapper}>
                <Image style={{width: 11,height: 11,resizeMode:'contain'}} source={require('../../images/homeBG/power.png')}/>
                <Text style={{fontSize: 13,marginLeft: 5,color: Colors.themeText}}>{powerValue}</Text>
                <TouchableOpacity style={styles.changedTouch} onPress={()=>{
                    this.setState({
                        powerType: (this.state.powerType + 1)%4
                    },()=>{
                        this.requestSettingEnergyType()
                    })
                }}>
                    <Text style={{fontSize: 11,color: Colors.themeButton}}>({powerTypeText}</Text>
                    <Image style={{width:13,height:13,resizeMode:'contain',tintColor:Colors.themeButton}} source={require('../../images/changed.png')}/>
                    <Text style={{fontSize: 12,color: Colors.themeButton}}>)</Text>
                </TouchableOpacity>
            </View>
        )
    }

	// 环境数据
	_renderAirInfo(){        
        const Colors = this.props.themeInfo.colors
        const enviInfo = this.state.enviInfo

        const {temperature, humidity, PM2_5_text, CH2O_NH3_text, CO2_text} = enviInfo
        if(!(temperature || humidity || PM2_5_text || CH2O_NH3_text || CO2_text)){
            return null
        }

        let roomTemperature = temperature ? (DecimalTheNumber(temperature, 1, true) + '℃') : null
        let humidityText = humidity ? (DecimalTheNumber(humidity, 2, true) + '%') : null
        let pm25 = PM2_5_text
        let formaldehyde = CH2O_NH3_text
        // let co2 = CO2_text
        
        return(
            <View style={styles.enviWrapper}>
                {this.getInfo(roomTemperature, require('../../images/homeBG/temp.png'), enviInfo['temperature_label'])}
                {this.getInfo(humidityText, require('../../images/homeBG/humidity.png'), enviInfo['humidity_label'])}
                {this.getInfo(pm25, require('../../images/homeBG/pm.png'), enviInfo['PM2_5_label'])}
                {/* {this.getInfo(co2, require('../../images/homeBG/co2.png'), enviInfo['CO2_label'])} */}
                {this.getInfo(formaldehyde, require('../../images/homeBG/formaldehyde.png'), enviInfo['CH2O_NH3_label'])}
                {/* {this._renderCameraInfo()} */}
                {/* {this._renderFilterBtn()} */}
            </View>
        )
    }

	getIndicator(){
        const Colors = this.props.themeInfo.colors

		let indicators = this.state.indicators.map((val,index)=>{
			let textStyle = {fontSize:16,color:Colors.themeTextLight}
			if(val.type == this.state.currentIndicator){
				textStyle = {fontSize:20,color:Colors.themeText,fontWeight:'bold'}
            }
            // const showSplit = index != this.state.indicators.length - 1
			return(
                <View key={'indicator_index_'+index} style={styles.indiTouchWrapper}>
                    <TouchableOpacity style={styles.indicatorTouch} onPress={()=>{
					    this.setState({
                            filterVisible: false,
                            filterSelected: [],
						    currentIndicator: val.type
					    })
				    }}>
					    <Text style={textStyle}>{val.name}</Text>
				    </TouchableOpacity>
                    {/* {showSplit ? <View style={[styles.split,{backgroundColor: Colors.tabInactive}]}/> : null} */}
                </View>
			)
        })
        
		return(
			<View style={styles.indicatorHead}>
				{indicators}
                <View style={{flex: 1}}/>
                {this._renderFilterBtn()}
                {this._renderRowLineBtn()}
			</View>
		)
	}

	getContentView(){
		if(this.state.currentIndicator == 1){
			return (
				<SceneList
					navigation={this.props.navigation}
                    roomId={this.roomId}
                    numColumns={this.state.numColumns}
                    isDark={this.props.themeInfo.isDark}
                    isJuniorUser={this.props.userInfo.memberType == 2}
				/>
			)
		}else if(this.state.currentIndicator == 2){
			return (
				<DeviceList
                    ref={e => this.devices_list = e}
					navigation={this.props.navigation}
                    params={{roomId: this.roomId}}
                    numColumns={this.state.numColumns}
                    isDark={this.props.themeInfo.isDark}
                    naturalClassification={this.state.filterSelected}
                    isJuniorUser={this.props.userInfo.memberType == 2}
                    refreshFilter={()=>{ 
                        this.setState({
                            filterSelected: []
                        },()=>{
                            this.requestFilterCategory()
                        })
                    }}
				/>
			)
		}else if(this.state.currentIndicator == 3){
            return(
                <SmartList 
                    navigation={this.props.navigation}
                    roomId={this.roomId}
                    isDark={this.props.themeInfo.isDark}
                    isJuniorUser={this.props.userInfo.memberType == 2}
                />
            )
        }
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                <View style={[styles.enviTouch,{backgroundColor: Colors.themeBg}]}>
                    {this._renderAirInfo()}
                    {this._renderPowerDetail()}
                </View>
                <View style={{flex: 1}}>
                    {this.getIndicator()}
				    {this.getContentView()}
                    {this._renderFilterModal()}
                </View>
				{this.getDeviceActionSheet()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex:1
	},
    indicatorHead:{
        width:'100%',
        height:50,
        alignItems:'center',
        flexDirection: 'row',
        paddingLeft: 5
    },
    indiTouchWrapper:{
        justifyContent:'center',
        alignItems:'center',
        height:'100%'
    },
	indicatorTouch:{
        width: 70,
		justifyContent:'center',
		alignItems:'center',
        height:'100%'
    },
    infoWrapper:{ 
        flexDirection: 'row', 
        alignItems: 'center',
        justifyContent:'center',
        marginRight:10
    },
    infoIcon:{ 
        width: 15, 
        height: 15,
        resizeMode:'contain' 
    },
    infoText: { 
        fontSize: 14,
        marginLeft:5 
    },
    enviTouch:{
        flexDirection:'row',
        paddingLeft: 16,
        alignItems: 'center',
    },
    enviWrapper:{
        flexDirection:'row',
        alignItems: 'center',
        flex: 1,
        height: 45
    },
    powerWrapper:{
        flexDirection: 'row', 
        alignItems:'center',
        height: 45,
    },
    cameraTouch:{
        paddingHorizontal: 16,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    cameraIcon:{
        width: 25,
        height: 25,
        resizeMode: 'contain'
    },
    split:{
        position: 'absolute',
        right: 0,
        top: '33%',
        width: 1,
        height: '30%'
    },
    filterImg:{
        width:14,
        height:14,
        resizeMode:'contain'
    },
    rightTouchL:{
        height: '100%',
        paddingHorizontal: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },
    rightIcon:{
        width: 16,
        height: 16,
        resizeMode: 'contain'
    },
    changedTouch:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        paddingRight: 16,
        marginLeft: 5,
        height: '100%'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
        userInfo: state.userInfo
    })
)(RoomDetail)

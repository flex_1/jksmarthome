/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	TextInput,
	DeviceEventEmitter,
	ImageBackground
} from 'react-native';

import { Colors } from '../../common/Constants';
import HeaderLeftView from '../../common/CustomComponent/HeaderLeftView';
import ToastManager from "../../common/CustomComponent/ToastManager";
import { StatusBarHeight } from '../../util/ScreenUtil';
import { connect } from 'react-redux';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';

class SelectFloor extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'楼层选择'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text:'确定',onPress:()=>{
                        navigation.getParam('sureBtnClick')()
                    }},
                ]}/>
            ),
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
    }

	constructor(props) {
		super(props);
		const {getParam,setParams} = this.props.navigation
		this.callBack = getParam('callBack')
		this.state = {
			currentFloors : getParam('currentFloors') || [],
			floors :[-3,-2,-1,1,2,3,4,5,6]
        }
        
        setParams({
            sureBtnClick: this.sureBtnClick.bind(this)
        })
    }
    
    sureBtnClick(){
        if(this.state.currentFloors.length <= 0){
            ToastManager.show('请至少选择一个楼层')
            return
        }
        const {pop} = this.props.navigation
        this.callBack && this.callBack( this.state.currentFloors.sort((a,b)=> a-b) )
        pop()
    }

	componentDidMount() {
		
	}

	componentWillUnmount(){
		
	}

	getFloorList(){
        const Colors = this.props.themeInfo.colors
        
		let floors = this.state.floors.map((val,index)=>{
			let selectIndex = this.state.currentFloors.indexOf( val )
			let selected = null
			if( selectIndex != -1 ){
				selected = <Image style={{width:18,height:18,marginRight:20,resizeMode:'contain'}} source={require('../../images/select.png')}/>
			}
			return(
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    key={'floor_list_'+index} 
                    style={[styles.inputWrapper,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        if(selectIndex == -1){
                            this.state.currentFloors.push(val)
                        }else{
                            this.state.currentFloors.splice(selectIndex,1)
                        }
                        this.setState({
                            ...this.state
                        })    
				    }}
                >
					<Text style={{fontSize:15,color:Colors.themeText,marginLeft:20,flex:1}}>{val+'楼'}</Text>
					{selected}
				</TouchableOpacity>
			)
		})
		return (
			<View style={{paddingHorizontal:16}}>
				{floors}
			</View>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
            <ScrollView 
                style={[styles.container,{backgroundColor: Colors.themeBaseBg}]} 
                contentContainerStyle={styles.scrollContent}
            >
				{this.getFloorList()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex:1
	},
	scrollContent:{
		paddingBottom: 50
	},
	inputWrapper:{
		width: '100%',
		height: 60,
		backgroundColor:Colors.white,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:10
	},
	navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 15,
        paddingLeft: 10,
        marginRight: 5
    },
    navText: {
        color: Colors.tabActiveColor,
        fontSize: 15
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SelectFloor)

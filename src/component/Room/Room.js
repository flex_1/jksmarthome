/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
    DeviceEventEmitter,
    RefreshControl,
    Alert
} from 'react-native';
import {Colors, NetUrls, NotificationKeys} from '../../common/Constants';
import {postJson} from "../../util/ApiRequest";
import ToastManager from "../../common/CustomComponent/ToastManager";
import {ListNoContent,ListLoading,ListError } from "../../common/CustomComponent/ListLoading";
import ScrollableTabView from 'react-native-scrollable-tab-view';
import RoomList from './RoomList';
import { connect } from 'react-redux';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import {ImagesLight, ImagesDark} from '../../common/Themes';
import FloorScrollTab from '../../common/CustomComponent/FloorScrollTab';
import {HSTips} from '../../common/Strings';

const RoomThumbChangeNoti = 'RoomThumbChangeNoti'

class Room extends Component {

    static navigationOptions = ({ navigation }) => {
        let isOrderEdit = navigation.getParam('isOrderEdit')
        let orderText = isOrderEdit ? '完成' : null
        let orderIcon = isOrderEdit ? null : require('../../images/order.png')

        return {
            headerLeft: <HeaderLeft navigation={navigation} title={'房间'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text: orderText,icon:orderIcon,onPress:()=>{
                        navigation.getParam('orderBtnClick')()
                    }},
                    {icon:require('../../images/add.png'),onPress:()=>{
                        navigation.getParam('addBtnClick')()
                    }}
                ]}/>
            ),
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
    };

	constructor(props) {
        super(props);
        const {setParams} = props.navigation

		this.state = {
			currentFloor: 0,
            currentIndex: 0,
            floors: null,
            isRefreshing: false,
            loading: true,
            numColumns: 1,   // 1-显示大图列表  2-显示缩略图
        }

        setParams({
            orderBtnClick: this.orderBtnClick.bind(this),
            addBtnClick: this.addBtnClick.bind(this)
        })
	}

    orderBtnClick(){
        const {setParams, getParam, navigate} = this.props.navigation

        // navigate('FloorsOrder',{floors: this.state.floors})
        // return

        if(this.props.userInfo.memberType == 2){
            Alert.alert(
                '提示',
                HSTips.limitsOrderRoom,
                [
                    {text: '知道了', onPress: () => {}, style: 'cancel'}
                ]
            )
        }else{
            setParams({
                isOrderEdit: !getParam('isOrderEdit')
            })
        }
    }

    addBtnClick(){
        if(this.props.userInfo.memberType == 2){
            Alert.alert(
                '提示',
                HSTips.limitsCreatRoom,
                [
                    {text: '知道了', onPress: () => {}, style: 'cancel'}
                ]
            )
        }else{
            this.props.navigation.navigate('AddRoom',{title:'添加房间'})
        }
    }

	componentDidMount() {
		this.getRoomFloor()
		
		// 房间楼层发生变化时(删除房屋,新增房屋都可能改变楼层)
		this.roomFloorChangeNoti =  DeviceEventEmitter.addListener(NotificationKeys.kRoomFloorChangeNotification,()=>{
			this.setState({
                currentFloor: 0,
                currentIndex: null,
                loading: true,
				floors: null
			},()=>{
				this.getRoomFloor()
			})
		})
	}

	componentWillUnmount(){
		this.roomFloorChangeNoti.remove()
	}

	// 获取有房间所有的楼层
    async getRoomFloor(){
		try {
			let data = await postJson({
				url: NetUrls.roomFloor,
				params: {
				}
            });
            this.setState({
                isRefreshing: false,
                loading: false
            })
			if (data.code == 0) {
                let floors = []
                if(data.result && data.result.length > 0){
                    floors = [{ floor: 0, floorText: '整屋' }].concat(data.result)
                }
                this.setState({
                    floors: floors,
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            this.setState({
                isRefreshing: false,
                loading: false
            })
			ToastManager.show('网络错误')
		}
    }
    
	//头部向导
	getHeaderView() {
		if(!this.state.floors || this.state.floors.length<=0){
			return null
        }
        const Colors = this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}
        let switchThumbImg = this.state.numColumns == 1 ? require('../../images/homeBG/list.png') : 
            require('../../images/homeBG/table.png')
        
		return (
			<View style={[styles.sectionHead,{backgroundColor: Colors.themeBg,borderBottomColor: Colors.themeBaseBg}]}>
                <FloorScrollTab
                    floors = {this.state.floors}
                    currentFloor = {this.state.currentFloor}
                    currentIndex = {this.state.currentIndex}
                    isDark = {this.props.themeInfo.isDark}
                    onClick = {(floor, index)=>{
                        if(this.props.navigation.getParam('isOrderEdit')){
                            ToastManager.show('您目前正在排序，暂无法切换楼层。')
                            return
                        }
                        this.setState({
                            currentFloor: floor,
                            currentIndex: index
                        })
                        this.scrollableTab?.goToPage(index)
                    }}
                />
                <View style={{width: 1,height:'50%',backgroundColor:Colors.split}}/>
                <TouchableOpacity style={styles.thumbnailTouch} onPress={()=>{
                    let target = this.state.numColumns%2 + 1
                    this.setState({ numColumns: target })
                    DeviceEventEmitter.emit(RoomThumbChangeNoti, target)
                }}>
                    <Image style={[styles.thumbIcon,tintColor]} source={switchThumbImg}/>
                 </TouchableOpacity>
			</View>
		)
    }

    // 显示添加设备按钮 还是 暂无文字
    renderSubContent(){
        const {navigate} = this.props.navigation

        if(this.props.userInfo.memberType == 2){
            return (
                <Text style={{fontSize: 14, color: Colors.themeTextLightGray, marginTop: 10}}>暂无房间</Text>
            )
        }else{
            return (
                <TouchableOpacity style={[styles.moreTouch,{marginTop:5}]} onPress={()=>{
                    navigate('AddRoom',{title:'添加房间'})
                }}>
                    <Text style={{fontSize:14,color:Colors.themeTextLightGray}}>添加房间</Text>
                    <Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
                </TouchableOpacity>
            )
        }
    }
    
    renderNoContent(){
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight

        return(
            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.isRefreshing}
                        onRefresh={() => {
                            if (this.state.isRefreshing) return
                            this.setState({ isRefreshing: true })
                            this.getRoomFloor()
                        }}
                    />
                }
            >
                <View style={{alignItems:'center'}}>
					<ListNoContent
						style={{ marginTop: '10%'}}
						img={Images.noRoom} 
					/>
					{this.renderSubContent()}
				</View>
            </ScrollView>
        )
    }

	getRoomListScrollView(){
		if(!this.state.floors && this.state.loading){
			return <ListLoading/>
        }
        if(!this.state.floors){
            return <ListError onPress={()=>{ this.getRoomFloor() }}/>
        }
		// 暂无房间
		if (this.state.floors.length<=0) {
			return this.renderNoContent()
        }
        const isOrderEdit = this.props.navigation.getParam('isOrderEdit')

		let rooms = this.state.floors.map((val, index)=>{
			return(
				<View key={'scrol_tab_index'+index} style={{flex:1}}>
                    <RoomList 
                        navigation={this.props.navigation} 
                        currentFloor={val.floor} 
                        numColumns={this.state.numColumns}
                        isDark={this.props.themeInfo.isDark}
                        isOrderEdit={isOrderEdit}
                        isJuniorUser={this.props.userInfo.memberType == 2}
                    />
				</View>
			)
		})

		return(
			<ScrollableTabView
				ref={e => this.scrollableTab = e}
				initialPage={0}
				prerenderingSiblingsNumber={0}
                locked={isOrderEdit}
            	onChangeTab={(obj)=>{
					let currentFloor = this.state.floors[obj.i]['floor']
					this.setState({
                        currentFloor: currentFloor,
                        currentIndex: obj.i
                    })
				}}
				renderTabBar={()=><View/>}
          	>
				{rooms}
			</ScrollableTabView>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getHeaderView()}
				{this.getRoomListScrollView()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex:1
	},
	navTitle: {
		fontSize: 24,
		fontWeight: 'bold',
		marginLeft: 15,
		textAlignVertical: 'center',
		color: Colors.themeTextBlack,
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 16,
		paddingLeft: 10
	},
	navImg: {
		width: 20,
		height: 20
	},
	sectionHead: {
		width: '100%',
		height: 40,
        flexDirection:'row',
        borderBottomWidth: 1,
        alignItems: 'center'
	},
	headScroll: {
        height: '100%',
        marginRight: 10
	},
	headScrollContainer: {
		alignItems: 'center',
		paddingLeft: 10,
		paddingRight:20
	},
	floorTouch: {
		alignItems: 'center',
		paddingHorizontal: 10,
		marginRight: 5
	},
	underLine: {
		width: 12,
		height: 3,
		backgroundColor: Colors.tabActiveColor,
		borderRadius: 0,
		marginTop: 5
	},
	floorText: {
		fontSize: 15,
		fontWeight: 'bold'
	},
	moreTouch:{
		borderColor:Colors.borderLightGray,
		borderWidth:1,
		width:120,
		height:30,
		borderRadius:15,
		justifyContent:'center',
		alignItems:'center',
		flexDirection:'row'
	},
	arrow:{
		width:6,
		height:11,
		marginLeft:10,
		resizeMode:'contain'
    },
    thumbnailTouch:{  
        height:'100%',
        alignItems: 'center',
        paddingHorizontal: 16,
        justifyContent:'center'
    },
    thumbIcon:{
        width:16,
        height:16,
        resizeMode:'contain'
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
        userInfo: state.userInfo
    })
)(Room)

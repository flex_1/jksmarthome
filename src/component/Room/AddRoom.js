/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    TextInput,
    DeviceEventEmitter,
    Alert,
    Modal,
    Keyboard
} from 'react-native';
import { postJson, postPicture } from '../../util/ApiRequest';
import { NetUrls, NotificationKeys,Colors } from '../../common/Constants';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet';
import { BottomSafeMargin,StatusBarHeight } from '../../util/ScreenUtil';
import ToastManager from '../../common/CustomComponent/ToastManager';
import SpinnerManager from '../../common/CustomComponent/SpinnerManager';
import { connect } from 'react-redux';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';

class AddRoom extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text:'保存',onPress:()=>{
                        navigation.getParam('addRoomBtnClick')()
                    }},
                ]}/>
            ),
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
    }

    constructor(props) {
        super(props);
        const { getParam,setParams } = this.props.navigation
        this.roomId = getParam('roomId')
        this.originFloor = getParam('currentFloor') || getParam('floorText')

        this.state = {
            modalVisible: false,
            avatarSource: getParam('avatarSource'),
            name: getParam('name') || '',
            remark: getParam('remark') || '',
            currentFloor: getParam('currentFloor'),
            floors: getParam('currentFloor') ? [getParam('currentFloor')] : [],

            roomImg: getParam('roomImg'),
            imgId: null,
            isCustomFloor: this.roomId && getParam('currentFloor') == null,
            customFloor: getParam('floorText') || '',
        }

        setParams({
            addRoomBtnClick: ()=>{ this.requestAddRoom() }
        })
    }

    componentDidMount() {
        
    }

    componentWillUnmount() {
        
    }

    //新增 修改 房屋
    async requestAddRoom() {
        // if (!this.state.avatarSource) {
        //     ToastManager.show('请选择图片')
        //     return
        // }
        Keyboard.dismiss()
        if (!this.state.name) {
            ToastManager.show('请输入房间名称')
            return
        }
        if(this.state.isCustomFloor){
            if(this.state.customFloor == null){
                ToastManager.show('请输入楼层名称')
                return
            }
        }else{
            if (this.state.floors.length < 1) {
                ToastManager.show('请选择楼层')
                return
            }
        }
        SpinnerManager.show()
        let params = {}
        if (this.roomId) {
            params.id = this.roomId
        }
        if(this.state.imgId){
            params.imgId = this.state.imgId
        }
        if(this.state.isCustomFloor){
            params.floor = this.state.customFloor
        }else{
            params.floor = this.state.floors.toString()
        }
        if(this.state.remark){
            params.remark = this.state.remark
        }
        try {
            let data = await postJson({
                url: NetUrls.addRoom,
                params: {
                    ...params,
                    name: this.state.name
                }
            })
            SpinnerManager.close()
            if (data && data.code == 0) {
                const { popToTop } = this.props.navigation
                this.roomId ? ToastManager.show('房间已更新') : ToastManager.show('添加房间成功')
                popToTop()
                if(!this.roomId){
                    DeviceEventEmitter.emit(NotificationKeys.kRoomFloorChangeNotification)
                }else if(this.originFloor && (this.originFloor != params.floor)){
                    DeviceEventEmitter.emit(NotificationKeys.kRoomFloorChangeNotification)
                }
                DeviceEventEmitter.emit(NotificationKeys.kRoomInfoNotification)
                DeviceEventEmitter.emit(NotificationKeys.kSceneFloorNotification)
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

    //删除房间
    async deleteRoom() {
        const { popToTop } = this.props.navigation
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.deleteRoom,
                params: {
                    id: this.roomId
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                ToastManager.show('成功删除房间');
                popToTop()
                DeviceEventEmitter.emit(NotificationKeys.kRoomFloorChangeNotification)
                DeviceEventEmitter.emit(NotificationKeys.kRoomInfoNotification)
                DeviceEventEmitter.emit(NotificationKeys.kSceneFloorNotification)
            } else {
                ToastManager.show(data.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(e));
        }
    }

    // 检查是否为数字
    isNumber(val) {
        var regPos = /^\d+(\.\d+)?$/; //非负浮点数
        var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
        if(regPos.test(val) || regNeg.test(val)) {
            return true;
        } else {
            return false;
        }
    }

    // picker 选择器
    showFloorPicker() {
        let data = ['-3楼', '-2楼', '-1楼', '1楼', '2楼', '3楼', '4楼', '5楼', '6楼'];
        
        window.DoubulePicker.init({
            pickerData: data,
            pickerConfirmBtnText: '确定',
            pickerCancelBtnText: '取消',
            pickerTitleText: '请选择楼层',
            pickerFontSize: 20,
            pickerRowHeight:40,
            pickerCancelBtnColor: [255, 255, 255, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            selectedValue: [this.state.floors + '楼'],
            onPickerConfirm: data => {
                this.setState({
                    currentFloor: data[0].replace('楼', ''),
                    floors: data[0].replace('楼', ''),
                    modalVisible: false
                })
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })
            },
            onPickerSelect: data => {
                console.log(data);
            }
        });
        window.DoubulePicker.show()
    }

    getPickImageView(){
        const Colors = this.props.themeInfo.colors
        const { navigate } = this.props.navigation

        let centerView = (
            <View style={[styles.headerPick,{backgroundColor: Colors.themeBg}]}>
                <Image style={{ width: 70, height: 70 }} source={require('../../images/cameral_icon.png')} />
                <Text style={{ fontSize: 14, color: Colors.themeText, marginTop: 10 }}>选择图片</Text>
            </View>
        )
        if (this.state.roomImg) {
            centerView = (
                <View style={[styles.headerPick,{backgroundColor: Colors.themeBg}]}>
                    <Image style={styles.roomImg}  source={{uri: this.state.roomImg}} />
                </View>
            )
        }
        return(
            <View style={{alignItems:'center',marginTop: 20}}>
                <TouchableOpacity onPress={()=>{
                    navigate('SelectDefaultImage',{callBack:(data)=>{
                        let roomImg = this.props.themeInfo.isDark ? data.nightImg : data.dayImg
                        this.setState({
                            roomImg: roomImg,
                            imgId: data.id,
                            name: this.state.name || data.name
                        })
                    }})
                }}>
                    {centerView}
                </TouchableOpacity>
            </View>
        )
    }

    // 自定义楼层
    renderFloorInput(){
        if(!this.state.isCustomFloor){
            return null
        }
        const Colors = this.props.themeInfo.colors
        let defaultFloor = this.state.customFloor ? this.state.customFloor+'' : ''
        if(parseInt(defaultFloor) <= 0){
            defaultFloor = ''
        }

        return(
            <View style={[styles.inputWrapper,{marginTop: 15,backgroundColor: Colors.themeBg}]}>
                <Text style={{ marginLeft: 20, fontSize: 15, color: Colors.themeText }}>楼层名称</Text>
                <TextInput
                    defaultValue={defaultFloor}
                    style={[styles.input,{color: Colors.themeText}]}
                    placeholder={'请输入楼层名称'}
                    // maxLength={2}
                    placeholderTextColor={Colors.themeInactive}
                    onChangeText={(text) => {
                        this.state.customFloor = text
                    }}
                    returnKeyType={'done'}
                />
            </View>
        )
    }

    // 选择楼层
    renderFloorSelect(){
        if(this.state.isCustomFloor){
            return null
        }
        const { navigate } = this.props.navigation
        const Colors = this.props.themeInfo.colors
        let floorTextView = <Text style={{ fontSize: 15, color: Colors.themeInactive }}>请选择房间所在的楼层</Text>
        if (this.state.floors.length > 0) {
            floorText = this.state.floors.toString() + '楼'
            floorTextView = <Text style={{ fontSize: 15, color: Colors.themeText }}>{floorText}</Text>
        }

        return(
            <View style={[styles.inputWrapper, { marginTop: 15,backgroundColor: Colors.themeBg }]}>
                <Text style={{ marginLeft: 20, fontSize: 15, color: Colors.themeText }}>楼层选择</Text>
                <TouchableOpacity style={styles.floorTouch} onPress={() => {
                    if (this.roomId) {
                        this.setState({
                            modalVisible: true
                        })
                    } else {
                        // 防止指针
                        let floors = JSON.parse(JSON.stringify(this.state.floors))
                        navigate('SelectFloor', {
                            currentFloors: floors, 
                            callBack: (floors) => {
                                this.setState({ floors: floors })
                            }
                        })
                    }
                }}>
                    {floorTextView}
                    <Image style={styles.rightArrow} source={require('../../images/enter_light.png')} />
                </TouchableOpacity>
            </View>
        )
    }

    renderSwitchBtn(){
        const Colors = this.props.themeInfo.colors
        const tips = this.state.isCustomFloor ? '选择系统楼层' : '自定义楼层'

        return(
            <TouchableOpacity style={styles.switchBtn} onPress={()=>{
                this.setState({
                    isCustomFloor: !this.state.isCustomFloor
                })
            }}>
                <Text style={{fontSize: 15, color: Colors.themeButton}}>{tips}</Text>
            </TouchableOpacity>
        )
    }

    // 房间备注
    renderRemark() {
        if (!this.roomId) return null

        const Colors = this.props.themeInfo.colors
        return (
            <View style={[styles.inputWrapper,{backgroundColor: Colors.themeBg,marginTop: 15}]}>
                <Text style={{ marginLeft: 20, fontSize: 15, color: Colors.themeText }}>房间备注</Text>
                <TextInput
                    style={[styles.input,{color: Colors.themeText}]}
                    placeholder={'输入备注信息(可选填)'}
                    defaultValue={this.state.remark}
                    placeholderTextColor={Colors.themeInactive}
                    onChangeText={(text) => {
                        this.state.remark = text
                    }}
                    returnKeyType={'done'}
                />
            </View>
        )
    }

    // 房间名称
    getInfoTextInput() {
        const Colors = this.props.themeInfo.colors
        return (
            <View style={{ marginTop: 25, paddingVertical: 10, paddingHorizontal: 16 }}>
                <View style={[styles.inputWrapper,{backgroundColor: Colors.themeBg}]}>
                    <Text style={{ marginLeft: 20, fontSize: 15, color: Colors.themeText }}>房间名称</Text>
                    <TextInput
                        style={[styles.input,{color: Colors.themeText}]}
                        placeholder={'请输入房间名(不超过8个字)'}
                        defaultValue={this.state.name}
                        placeholderTextColor={Colors.themeInactive}
                        onChangeText={(text) => {
                            this.state.name = text
                        }}
                        returnKeyType={'done'}
                    />
                </View>
                {this.renderRemark()}
                {this.renderFloorInput()}
                {this.renderFloorSelect()}
                {this.renderSwitchBtn()}
            </View>
        )
    }

    // 删除按钮
    getDeleteButton() {
        if (!this.roomId) return null

        const Colors = this.props.themeInfo.colors
        return (
            <View style={styles.bottomBtnWrapper}>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.deleteTouch,{backgroundColor: Colors.themeBg}]} 
                    onPress={() => {
                        Alert.alert(
                            '提示',
                            '确认删除该房间?',
                            [
                                { text: '取消', onPress: () => { }, style: 'cancel' },
                                { text: '删除', onPress: () => { this.deleteRoom() } },
                            ]
                        )
                    }}
                >
                    <Text style={{color: Colors.red,fontSize: 15}}>删除</Text>
                </TouchableOpacity>
            </View>
        )
    }

    getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showFloorPicker()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                <ScrollView keyboardDismissMode={'on-drag'}>
                    {this.getPickImageView()}
                    {this.getInfoTextInput()}
                    {this.getModal()}
                </ScrollView>
                {this.getDeleteButton()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.themBGLightGray,
        flex: 1
    },
    headerPick: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
        borderRadius: 4
    },
    headerPickTouch: {
        marginTop: 23,
        paddingHorizontal: 16,
    },
    inputWrapper: {
        width: '100%',
        height: 55,
        borderRadius: 5,
        alignItems: 'center',
        flexDirection: 'row'
    },
    input: {
        flex: 1,
        height: 40,
        marginLeft: 5,
        marginRight: 10,
        textAlign: 'right',
        paddingRight: 16
    },
    floorTouch: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: '90%',
        flex: 1,
        paddingRight: 10
    },
    deleteTouch: {
        width: '100%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    bottomBtnWrapper: {
        position: 'absolute',
        left: 0,
        bottom: BottomSafeMargin,
        width: '100%',
        paddingHorizontal: 16,
        paddingVertical: 10
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent 
    },
    roomImg:{
        width: 50,
        height: 50,
        resizeMode: 'contain'
    },
    rightArrow: { 
        marginLeft: 10, 
        width: 7, 
        height: 13, 
        marginRight: 10,
        resizeMode:'contain' 
    },
    switchBtn:{
        marginTop: 10,
        paddingHorizontal: 16,
        paddingVertical: 10,
        alignSelf:'flex-end'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(AddRoom)

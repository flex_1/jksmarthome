/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	DeviceEventEmitter,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	FlatList,
	Alert,
	TextInput,
	Keyboard
} from 'react-native';
import { Colors, NotificationKeys, NetUrls } from '../../common/Constants';
import { StatusBarHeight, NavigationBarHeight } from '../../util/ScreenUtil';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import { connect } from 'react-redux';

class RoomSelectDevice extends Component {

	static navigationOptions = {
		header: null,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
	}

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.roomId = getParam('roomId')
        this.callBack = getParam('callBack')
        
		this.state = {
			deviceList: null,
			selectedIds: [],
			searchText: ''
		}
	}

	componentDidMount() {
		this.requestDeviceList()
	}

	componentWillUnmount() {
		
	}

	confirm(){
		// 房间里面 添加已有设备
		this.requestUpdateRooms()
	}

	// 获取设备列表
	async requestDeviceList(){
		let params = {}
		if(this.state.searchText){
			params.deviceName = this.state.searchText
		}
		params.deviceId = 0
		// roomId = -1 时表示只显示整屋的设备（不在房间里的设备）
		params.roomid = -1
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.listSecond,
				params: params
			});
			SpinnerManager.close()
			if (data.code == 0) {
				this.setState({
					deviceList: data.result || []
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error));
		}
	}

	// 绑定设备到房间
	async requestUpdateRooms(){
		if(this.state.selectedIds.length<=0){
			ToastManager.show('请选择您要添加的设备');
			return
		}
		const { pop } = this.props.navigation
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.bindDevicesToRoom,
				params: {
					roomId: this.roomId,
					deviceId: this.state.selectedIds.toString()
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
				ToastManager.show('添加成功')
				pop()
				this.callBack && this.callBack()
				DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error));
		}
	}

	getSearchHeader(){
        const {pop} = this.props.navigation
        const Colors = this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}

		return(
            <View style={[styles.topWrapper,{backgroundColor:Colors.themeBg, borderBottomColor: Colors.themeBaseBg}]}>
                <TouchableOpacity style={styles.backBtn} onPress={()=>{
                    pop()
                }}>
                    <Image style={[styles.backIcon,tintColor]} source={require('../../images/back.png')}/>
                </TouchableOpacity>
                <View style={styles.searchWrapper}>
                    <Image style={styles.searchIcon} source={require('../../images/search_gray.png')}/>
                    <TextInput
                        style={{flex:1,paddingLeft:10}}
                        value={this.state.searchText}
                        placeholder={'请输入关键字'}
                        placeholderTextColor={Colors.themeBGInactive}
                        returnKeyType={'search'}
                        clearButtonMode={'while-editing'}
                        maxLength={16}
                        onChangeText={(text) => {
                            this.setState({
                                searchText : text
                            },()=>{
                                this.requestDeviceList()
                            })
                        }}
                        onSubmitEditing={()=>{
                            if(!this.state.searchText){
                                ToastManager.show('请输入关键字')
                                return
                            }
                        }}
                    />
                </View>
                <TouchableOpacity style={styles.cancelBtn} onPress={()=>{
                    Keyboard.dismiss()
                    this.confirm()
                }}>
                    <Text style={{fontSize:15,color:Colors.themeText}}>确定</Text>
                </TouchableOpacity>
            </View>
		)
	}

	// 设备列表
	getDeviceListView(){
        const Colors = this.props.themeInfo.colors

		if(!this.state.deviceList){
			return null
		}
		if(this.state.deviceList && this.state.deviceList.length<=0){
			return (
				<View style={{alignItems:'center',flex:1,marginTop:'40%'}}>
					<Text style={{color:Colors.themeTextLight}}>暂无可选设备</Text>
				</View>
			)
        }

        
        
		let deviceList = this.state.deviceList.map((val,index)=>{
			// 该房间 已存在 的设备，不显示
			if(this.roomId == val.roomId){
				return
			}
			let selected = <View style={styles.noselected} />
			let select_index = this.state.selectedIds.indexOf(val.deviceId)
			// 判断 右上角 的标识
			if(val.islock){
				// 如果被锁定
				selected = <Image style={{width:16,height:20}} source={require('../../images/deviceIcon/lock.png')}/>
				// 正常显示 被选中
			}else if(select_index >= 0){
				selected = <Image style={{width:18,height:18}} source={require('../../images/select.png')}/>
			}
			
			// 描述位置
			let descripe = null
			if(val.floorText && val.roomName){
				descripe = val.floorText + '  ' + val.roomName
            }
            
            //设备标识
            let deviceIcon = this.props.themeInfo.isDark ? val.nightImg : val.dayImg
			return(
				<View key={'device_list_'+index} style={styles.deviceItem}>
                    <TouchableOpacity 
                        activeOpacity={0.7} 
                        style={[styles.deviceClassItem,{backgroundColor: Colors.themeBg}]} 
                        onPress={()=>{
						    if(val.islock){
							    ToastManager.show('锁定设备无法添加，请先解锁。')
							    return
						    }
						    if(select_index >= 0){
							    this.state.selectedIds.splice(select_index,1)
						    }else{
							    this.state.selectedIds.push(val.deviceId)
						    }
						    this.setState({
							    selectedIds: this.state.selectedIds
						    })
					    }}
                    >
						<Image style={styles.deviceIcon} source={{uri: deviceIcon}}/>
						<View style={{flex:1,marginLeft:15}}>
							<Text style={{fontSize:14,color:Colors.themeText}}>{val.deviceName}</Text>
							{descripe?<Text style={styles.floorText}>{descripe}</Text>:null}
						</View>
						{selected}
					</TouchableOpacity>
				</View>
			)
		})
		return(
			<ScrollView 
				contentContainerStyle={{paddingBottom: 50}}
				scrollIndicatorInsets={{right: 1}}
				keyboardDismissMode={'on-drag'}
			>
				<View style={styles.deviceWrapper}>
					{deviceList}
				</View>
			</ScrollView>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getSearchHeader()}
				{this.getDeviceListView()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	backBtn:{
		paddingHorizontal:16,
		paddingRight:20,
		height:'100%',
		justifyContent:'center',
		alignItems:'center'
    },
    backIcon:{
        width:12,
        height:20,
        resizeMode:'contain'
    },
    searchIcon:{
        width:15,
        height:15,
        resizeMode:'contain'
    },
	status:{
		width:'100%',
        height: StatusBarHeight,
        backgroundColor: Colors.white
	},
	topWrapper:{
		width:'100%',
		alignItems:'center',
        flexDirection:'row',
        borderBottomWidth: 1,
        paddingTop: Platform.select({
            ios: StatusBarHeight,
            android: StatusBarHeight + 10
        }),
        paddingBottom: Platform.select({
            ios: 0,
            android: 5
        }),
        height: Platform.select({
            ios: 50 + StatusBarHeight,
            android: NavigationBarHeight + StatusBarHeight
        })
	},
	searchWrapper:{
		backgroundColor:Colors.themBGLightGray,
		flex:1,
		borderRadius: 4,
		alignItems:'center',
		flexDirection:'row',
        paddingHorizontal: 10,
        paddingVertical: Platform.select({
            android: 0,
            ios: 10
        })
	},
	cancelBtn:{
		paddingRight: 16,
		paddingLeft: 16,
		marginLeft: 5,
		height:'100%',
		justifyContent:'center',
		alignItems:'center'
	},
	deviceWrapper:{
		paddingHorizontal:16,
		marginTop:10
	},
	deviceItem:{
		width: '100%',
		marginBottom: 5,
		paddingVertical: 5,
	},
	deviceClassItem: {
		width: '100%',
		height: 60,
		backgroundColor: Colors.white,
		borderRadius: 5,
		alignItems: 'center',
		flexDirection: 'row',
		paddingHorizontal: 16
	},
	deviceIcon:{
		width:40,
		height:40,
		resizeMode:'contain'
	},
	switchTouch:{
		marginTop:10,
		width:100,
		height:30,
		borderRadius:15,
		borderColor:Colors.borderLightGray,
		borderWidth:1,
		justifyContent:'center',
		alignItems:'center'
	},
	noselected:{ 
		width: 18, 
		height: 18, 
		borderColor: Colors.borderLightGray, 
		borderWidth: 1, 
		borderRadius: 9
	},
	floorText:{
		marginTop:10,
		fontSize:12,
		color:Colors.themeTextLightGray
	}

});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(RoomSelectDevice)

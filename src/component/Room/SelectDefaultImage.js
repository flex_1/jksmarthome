/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	TextInput,
	DeviceEventEmitter,
	ImageBackground
} from 'react-native';
import { postJson } from '../../util/ApiRequest';
import { Colors,NetUrls } from '../../common/Constants';
import HeaderLeftView from '../../common/CustomComponent/HeaderLeftView';
import ToastManager from "../../common/CustomComponent/ToastManager";
import { StatusBarHeight } from '../../util/ScreenUtil';
import SpinnerManager from '../../common/CustomComponent/SpinnerManager';
import { connect } from 'react-redux';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';

const screenW = Dimensions.get('window').width;
const roomBgW = (screenW - 40)/2;
const roomBgH = roomBgW * 0.45;

class SelectDefaultImage extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'选择默认图片'}/>,
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
    }

	constructor(props) {
		super(props);
		const {getParam} = this.props.navigation
		this.callBack = getParam('callBack')

		this.state = {
			images: []
		}
	}

	componentDidMount() {
		this.requestRoomImage()
	}

	componentWillUnmount(){
		
	}

    async requestRoomImage() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.getRoomPicture,
                params: {
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.setState({
					images: data.result || []
				})
            } else {
                ToastManager.show(data.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(e));
        }
    }

	ImageList(){
        const {pop} = this.props.navigation
        const Colors = this.props.themeInfo.colors
        
		let images = this.state.images.map((val,index)=>{

            let roomIcon = this.props.themeInfo.isDark ? val.nightImg : val.dayImg

			return (
				<TouchableOpacity key={'room_images_'+index} style={styles.imgTouch} onPress={()=>{
					this.callBack && this.callBack(val)
					pop()
				}}>
					<Image source={{uri: roomIcon}} style={styles.roomImage}/>
                    <Text style={[styles.roomName,{color: Colors.themeText}]}>{val.name}</Text>
				</TouchableOpacity>
			)
		})
		return (
			<View style={{paddingVertical:10,paddingHorizontal:10,flexDirection:'row',flexWrap:'wrap',}}>
				{images}
			</View>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
            <ScrollView 
                style={[styles.container,{backgroundColor: Colors.themeBaseBg}]} 
                contentContainerStyle={styles.scrollContent}
            >
				{this.ImageList()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex:1
	},
	scrollContent:{
		paddingBottom: 50
	},
	inputWrapper:{
		width: '100%',
		height: 60,
		backgroundColor:Colors.white,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:15,
		//阴影四连
		shadowOffset: { width: 0, height: 2 },
		shadowColor: '#070F26',
		shadowOpacity: 0.15,
		shadowRadius: 5,
		elevation: 5
	},
	navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 15,
        paddingLeft: 10,
        marginRight: 5
    },
    navText: {
        color: Colors.tabActiveColor,
        fontSize: 15
    },
    imgTouch:{
        width:'33%',
        marginTop: 20,
        justifyContent:'center',
        alignItems: 'center'
    },
	roomImage:{
		width:50,
        height:50,
        resizeMode: 'contain'
    },
    roomName:{
        marginTop: 10,
        fontSize: 15
    },
	roomImageStyle:{
		resizeMode:'contain',
		backgroundColor:Colors.themBGLightGray,
		borderRadius: 4
	},
	imageWrapper:{
		width:'100%',
		height:'100%',
		justifyContent:'center',
		alignItems:'center',
		borderRadius:4,
		backgroundColor: Colors.translucent
	}
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SelectDefaultImage)

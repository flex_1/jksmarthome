/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Image, Text, View, Platform} from 'react-native';
import {createStackNavigator, createAppContainer, createBottomTabNavigator} from "react-navigation";
import { StatusBarHeight,NavigationBarHeight,iPhoneHeaderHeightOffset,isNewiPhone } from '../util/ScreenUtil';
import {Colors} from '../common/Constants';
import {ScreenAdapt} from '../util/ScreenUtil';

import Login from './Login/Login';
import ForgetPsw from './Login/ForgetPsw';
import BindPhone from './Login/BindPhone';
import LoginByMsg from './Login/LoginByMsg';
import Home from './Home/Home';
import Room from './Room/Room';
import User from './User/User';
import AddDeviceQRCode from './Home/AddDeviceQRCode';
import AddDevice from './Home/AddDevice';
import DeviceManage from './User/DeviceManages/DeviceManage';
import MyHouse from './Home/MyHouse';
import AddHouse from './Home/AddHouse';
import UserSetting from './User/UserSetting';
import AddRoom from './Room/AddRoom';
import Device from './Home/Device';
import Scene from './Scene/Scene';
import SelectThumb from "./Scene/SelectThumb";
import AddScene from './Scene/AddScene';
import SceneSetting from './Scene/SceneSetting';
import SenceDeviceList from './Scene/SenceDeviceList';
import RoomDetail from './Room/RoomDetail';
import SelectFloor from './Room/SelectFloor';
import SelectDefaultImage from './Room/SelectDefaultImage';
import SelectDevice from './Scene/SelectDevice';
import DeviceCombination from './Scene/DeviceCombination';
import DeviceSetting from './Home/DeviceSetting';
import Energy from './User/Energy';
import Seting_Room from './CommonPage/DeviceSettings/Setting_Room';
import Setting_Maxopenning from './CommonPage/DeviceSettings/Setting_Maxopenning';
import Setting_Timeout from './CommonPage/DeviceSettings/Setting_Timeout';
import Setting_Overelectric from './CommonPage/DeviceSettings/Setting_Overelectric';
import Setting_Timing from './CommonPage/DeviceSettings/Setting_Timing';
import Setting_Timing_AddTiming from './CommonPage/DeviceSettings/Setting_Timing_AddTiming';
import Setting_Voltage from './CommonPage/DeviceSettings/Setting_Voltage';
import Setting_ParamsSetting from './CommonPage/DeviceSettings/Setting_ParamsSetting';
import Setting_AboutController from './CommonPage/DeviceSettings/Setting_AboutController';
import Setting_Share from './CommonPage/DeviceSettings/Setting_Share';
import Setting_Dependency from './CommonPage/DeviceSettings/Setting_Dependency';
import Setting_NobodyTime from './CommonPage/DeviceSettings/Setting_NobodyTime';
import ShareDetail from './CommonPage/DeviceSettings/ShareDetail';
import Setting_Log from './CommonPage/DeviceSettings/Setting_Log';
import Setting_Attributes from './CommonPage/DeviceSettings/Setting_Attributes';
import Setting_PowerStatus from './CommonPage/DeviceSettings/Setting_PowerStatus';
import ControllerSetting from './User/DeviceManages/ControllerSetting';
import PriceSetting from './User/PriceSetting';
import Member from './User/Member';
import Message from './User/Message/Message';
import MessageSetting from './User/Message/MessageSetting';
import MessageDetail from './User/Message/MessageDetail';
import MessageDetailList from './User/Message/MessageDetailList';
import MessageManage from './User/Message/MessageManage';
import DeviceMessageManage from './User/Message/DeviceMessageManage';
import ControllerSetting_Producer from './User/DeviceManages/ControllerSetting_Producer';
import ControllerSetting_Location from './User/DeviceManages/ControllerSetting_Location';
import ControllerSetting_StartUsing from './User/DeviceManages/ControllerSetting_StartUsing';
import ControllerSetting_HardwareUpdate from './User/DeviceManages/ControllerSetting_HardwareUpdate';
import ControllerSetting_Mac from './User/DeviceManages/ControllerSetting_Mac';
import MemberSetting from './User/MemberSetting';
import DevicePermission from './User/DevicePermission';
import RoomPermission from './User/RoomPermission';
import AddMember from './User/AddMember';
import Transfer from './User/Transfer';
import TransferWarrning from './User/TransferWarrning';
import Protocol from './Login/Protocol';
import HouseSetting from './Home/HouseSetting';
import HouseLayout from './Home/HouseLayout';
import Setting from './User/Setting';
import EnvironmentObservation from './Home/EnvironmentObservation';
import SearchPage from './CommonPage/SearchPage';
import ElectricityDetail from './Home/ElectricityDetail';
import EnvironmentParams from './Home/EnvironmentParams';
import RoomSelectDevice from './Room/RoomSelectDevice';
import GroupManageList from './User/DeviceManages/GroupManageList';
import GatewaySetting from './User/DeviceManages/GatewaySetting';
import ApplyforDebug from './User/ApplyforDebug';
import ApplyDebugRecord from './User/ApplyDebugRecord';
import Smart from './Smart/Smart';
import AddSmart from './Smart/AddSmart';
import AddCondition from './Smart/AddCondition';
import AddExecute from './Smart/AddExecute';
import ExecuteSmart from './Smart/ExecuteSmart';
import ExecuteScene from './Smart/ExecuteScene';
import ExecuteDevice from './Smart/ExecuteDevice';
import ExecuteDeviceList from './Smart/ExecuteDeviceList';
import SmartTiming from './Smart/SmartTiming';
import EffectTime from './Smart/EffectTime';
import SmartOutdoorWeather from './Smart/SmartOutdoorWeather';
import SmartDuring from './Smart/SmartDuring';
import SmartDevices from './Smart/SmartDevices';
import Logs from './CommonPage/Logs';
import Questions from './User/Questions';
import Camera from './CommonPage/Security/Camera';
import CameraList from './CommonPage/Security/CameraList';
import SwitchList from './Scene/SwitchList';
import SwitchBinding from './Scene/SwitchBinding';
import SwitchBindingResult from './Scene/SwitchBindingResult';
import SceneRecommend from './Scene/SceneRecommend';
import SceneRecommendDetail from './Scene/SceneRecommendDetail';
import SettingOtherName from './CommonPage/SettingOtherName';
import SmartRecommend from './Smart/SmartRecommend';
import NetworkSetting from './User/DeviceManages/NetworkSetting';
import AccountLoginRecord from './User/AccountLoginRecord';
import MemberLoginRecord from './User/MemberLoginRecord';
import Authorize from './CommonPage/Security/Authorize';
import ReName from './CommonPage/ReName';
import ElectricityRank from './Home/ElectricityRank';
import MultiFunctionRelay from './User/DeviceManages/MultiFunctionRelay';
import AddMultiFunctionDevice from './User/DeviceManages/AddMultiFunctionDevice';
import LaboratoryList from './User/LaboratoryList';
import AboutUs from './User/AboutUs';
import ShortCutList from './User/ShortCutList';
import SenceSmartList from './Scene/SenceSmartList';
import SenceChooseSmart from './Scene/SenceChooseSmart';
import ControllerThumb from './User/DeviceManages/ControllerThumb';
import FloorsOrder from './Room/FloorsOrder';
import MideaAuthonList from './Home/MideaAuthonList';
import CloseAccount from '../component/User/CloseAccount';
import FindDevices from '../component/User/DeviceManages/FindDevices';
import UnbindGateway from '../component/User/DeviceManages/UnbindGateway';
import WechatBind from '../component/User/WechatBind';
import LogDetail from '../component/CommonPage/DeviceSettings/LogDetail';
import TimingManager from '../component/User/TimingManager';
import FirmwareUpgrade from '../component/User/DeviceManages/FirmwareUpgrade';
import FirmwareUpgradeLog from '../component/User/DeviceManages/FirmwareUpgradeLog';
import ReplaceDevice from '../component/User/DeviceManages/ReplaceDevice';
import ExecuteNotification from '../component/Smart/ExecuteNotification';
import SmartVoiceBoxLocation from '../component/CommonPage/SmartVoiceBoxLocation';
import SmartVoiceBoxLog from '../component/CommonPage/SmartVoiceBoxLog';
import BleConfigWebPage from '../component/User/DeviceManages/BleConfigWebPage';

//Demo Pages
import DebugPage from '../common/Debug/DebugPage';
import SVGDemo from '../common/Debug/SVGDemo';
import ScrollViewEnable from '../common/Debug/ScrollViewEnable';

//设备控制面板
import LightPanel from './CommonPage/ControlPanel/LightPanel';
import WindowCurtains from './CommonPage/ControlPanel/WindowCurtains';
import Television from './CommonPage/ControlPanel/Television';
import Healthy from './CommonPage/ControlPanel/Healthy';
import Music from './CommonPage/ControlPanel/Music';
import AirConditioner from './CommonPage/ControlPanel/AirConditioner';
import SwitchPannel from './CommonPage/ControlPanel/SwitchPannel';
import ScrollCurtains from './CommonPage/ControlPanel/ScrollCurtains';
import Ventilation from './CommonPage/ControlPanel/Ventilation';
import AirSwitch from './CommonPage/ControlPanel/AirSwitch';
import ScrollAngleCurtains from './CommonPage/ControlPanel/ScrollAngleCurtains';
import RFController from './CommonPage/ControlPanel/RFController';
import RIController from './CommonPage/ControlPanel/RIController';
import RemoteControl from './CommonPage/ControlPanel/RemoteControl';
import LoraPannel from './CommonPage/ControlPanel/LoraPannel';
import FlashLightWebPannel from './CommonPage/ControlPanel/FlashLightWebPannel';
import ColorLightPanel from './CommonPage/ControlPanel/ColorLightPanel';
import FloorHeating from './CommonPage/ControlPanel/FloorHeating';
import DoubleLightPanel from './CommonPage/ControlPanel/DoubleLightPanel';
import TVRemoteController from './CommonPage/ControlPanel/TVRemoteController';
import OnePRelay from './CommonPage/ControlPanel/OnePRelay';
import SolarEnergy from './CommonPage/ControlPanel/SolarEnergy';
import Sensor from './CommonPage/ControlPanel/Sensor';
import TCP from './CommonPage/TCP/TCP';
import TCPSetting from './CommonPage/TCP/TCPSetting';
import TCPHome from './CommonPage/TCP/TCPHome';
import TCPSceneList from './CommonPage/TCP/TCPSceneList';
import TCPDeviceList from './CommonPage/TCP/TCPDeviceList';
import UDP from './CommonPage/UDP/UDP';
import UDPDebug from './CommonPage/UDP/UDPDebug';
import RISelfLearn from './CommonPage/ControlPanel/RISelfLearn';
import DoorLock from './CommonPage/ControlPanel/DoorLock';
import DoorLockAddPassWord from './CommonPage/DoorLockAddPassWord';
import Elevator from './CommonPage/ControlPanel/Elevator';
import VoiceBox from './CommonPage/ControlPanel/VoiceBox';
import Dehumidifier from './CommonPage/ControlPanel/Dehumidifier';
import AirOutdoorUnit from './CommonPage/ControlPanel/AirOutdoorUnit';
import IPTVController from './CommonPage/ControlPanel/IPTVController';
import Cupboard from './CommonPage/ControlPanel/Cupboard';
import DoorYale from './CommonPage/ControlPanel/DoorYale';
import WiFiGuide from './CommonPage/ConnectWiFi/WiFiGuide';
import WiFiPassword from './CommonPage/ConnectWiFi/WiFiPassword';
import WiFiConnecting from './CommonPage/ConnectWiFi/WiFiConnecting';
import WiFiSettingRoom from './CommonPage/ConnectWiFi/WiFiSettingRoom';
import WiFiUDPPrepare from './CommonPage/ConnectWiFi/WiFiUDPPrepare';
import WiFiList from './CommonPage/ConnectWiFi/WiFiList';
import WiFiAddRoom from './CommonPage/ConnectWiFi/WiFiAddRoom';
import WiFiSettingName from './CommonPage/ConnectWiFi/WiFiSettingName';
import WiFiDeviceCategory from './CommonPage/ConnectWiFi/WiFiDeviceCategory';
import WiFiUDPConnecting from './CommonPage/ConnectWiFi/WiFiUDPConnecting';
import PanelButtonBind from './CommonPage/ControlPanel/PanelButtonBind';
import RobotHome from './CommonPage/Robot/RobotHome';
import RobotCall from './CommonPage/Robot/RobotCall';
import RobotOrderDetail from './CommonPage/Robot/RobotOrderDetail';
import RobotOrderList from './CommonPage/Robot/RobotOrderList';
import RobotLocation from './CommonPage/Robot/RobotLocation';
import ScrollCurtainsDebug from './CommonPage/ControlPanel/ScrollCurtainsDebug';
import SmartPanel from './CommonPage/485SmartPanel/SmartPanel';
import ButtonSetting from './CommonPage/485SmartPanel/ButtonSetting';
import PanelSetting from './CommonPage/485SmartPanel/PanelSetting';
import ButtonSettingDetail from './CommonPage/485SmartPanel/ButtonSettingDetail';
import WifiPanelButtonList from './User/DeviceManages/WifiPanelButtonList';
import WifiButtonBind from './User/DeviceManages/WifiButtonBind';
import ZigbeePanelBindDevice from './CommonPage/Zigbee/ZigbeePanelBindDevice';
import ProtocolControlSetting from "./User/DeviceManages/ProtocolControlSetting";
import ProtocolControlList from './User/DeviceManages/ProtocolControlList';
import ProtocolControlCreat from './User/DeviceManages/ProtocolControlCreat';
import ProtocolController from './CommonPage/ControlPanel/ProtocolController';
import RIStudyController from './CommonPage/ControlPanel/RIStudyController';
import RadioCurtains from './CommonPage/ControlPanel/RadioCurtains';
import RIStudySetting from './User/DeviceManages/RIStudySetting';
import Invertor from './CommonPage/ControlPanel/Invertor';
import Coulometer from './CommonPage/ControlPanel/Coulometer';
import SmartVoiceBox from './CommonPage/ControlPanel/SmartVoiceBox';
import CombineWindow from './CommonPage/ControlPanel/CombineWindow';
import DreamShutter from './CommonPage/ControlPanel/DreamShutter';
import WebPannel from './CommonPage/ControlPanel/WebPannel';
import BleGroupLight from './CommonPage/ControlPanel/BleGroupLight';
//zigbee
import GatewayHomePage from './CommonPage/Zigbee/GatewayHomePage';
import ZigbeeDoorLock from './CommonPage/Zigbee/ZigbeeDoorLock';
import ZigbeeDoorFingerList from './CommonPage/Zigbee/ZigbeeDoorFingerList';
import ZigbeeConfigWifi from './CommonPage/Zigbee/ZigbeeConfigWifi';
import ZigbeeSmokeAlarm from './CommonPage/Zigbee/ZigbeeSmokeAlarm';
import ZigbeeHumanInduction from './CommonPage/Zigbee/ZigbeeHumanInduction';
import ZigbeeAirQuality from './CommonPage/Zigbee/ZigbeeAirQuality';

// 语音控制
import VoiceControlPage from './Voice/VoiceControlPage';
import VoiceQuestion from './Voice/VoiceQuestion';
import VoiceSetting from './Voice/VoiceSetting';

import BadgeTabManager,{BadgeTab} from '../common/CustomComponent/BadgeTab';
import MainTabBar from './MainTabBar';
import StackViewStyleInterpolator from 'react-navigation-stack/src/views/StackView/StackViewStyleInterpolator';

const homeIcon = require('../images/bottom_tab/home_inactive.png')
const homeIcon_select = require('../images/bottom_tab/xiaosa.png')
const sceneIcon = require('../images/bottom_tab/scene_inactive.png')
const sceneIcon_select = require('../images/bottom_tab/scene_active.png')
const roomIcon = require('../images/bottom_tab/room_inactive.png')
const roomIcon_select = require('../images/bottom_tab/room_active.png')
const userIcon = require('../images/bottom_tab/user_inactive.png')
const userIcon_select = require('../images/bottom_tab/user_active.png')
const smartIcon = require('../images/bottom_tab/smart_inactive.png')
const smartIcon_select = require('../images/bottom_tab/smart_active.png')
const center_icon = require('../images/bottom_tab/center.png')



//TCP相关页面
const TCPViews = {
    TCP: {screen: TCP},
    TCPSetting: {screen: TCPSetting},
    TCPHome: {screen: TCPHome},
    TCPSceneList: {screen: TCPSceneList},
    TCPDeviceList: {screen: TCPDeviceList},
    UDP: {screen: UDP},
    UDPDebug: {screen: UDPDebug},
    SVGDemo: {screen: SVGDemo},
    ScrollViewEnable: {screen: ScrollViewEnable}
}

//设备控制面板页面
const DevicesPanel = {
    SwitchPannel:{screen: SwitchPannel},
    LightPanel:{screen:LightPanel},
    WindowCurtains:{screen:WindowCurtains},
    Television:{screen:Television},
    Healthy:{screen:Healthy},
    Music:{screen:Music},
    AirConditioner:{screen:AirConditioner},
    ScrollCurtains:{screen:ScrollCurtains},
    Ventilation:{screen:Ventilation},
    AirSwitch:{screen:AirSwitch},
    ScrollAngleCurtains:{screen:ScrollAngleCurtains},
    RFController:{screen:RFController},
    RIController:{screen:RIController},
    RemoteControl:{screen:RemoteControl},
    LoraPannel:{screen:LoraPannel},
    FlashLightWebPannel:{screen:FlashLightWebPannel},
    ColorLightPanel:{screen:ColorLightPanel},
    FloorHeating:{screen:FloorHeating},
    DoubleLightPanel:{screen:DoubleLightPanel},
    TVRemoteController:{screen:TVRemoteController},
    OnePRelay:{screen:OnePRelay},
    SolarEnergy:{screen:SolarEnergy},
    Sensor:{screen:Sensor},
    RISelfLearn:{screen:RISelfLearn},
    DoorLock:{screen:DoorLock},
    DoorLockAddPassWord:{screen:DoorLockAddPassWord},
    Elevator:{screen:Elevator},
    VoiceBox:{screen:VoiceBox},
    Dehumidifier:{screen:Dehumidifier},
    AirOutdoorUnit:{screen:AirOutdoorUnit},
    IPTVController:{screen:IPTVController},
    Cupboard:{screen:Cupboard},
    DoorYale:{screen:DoorYale},
    PanelButtonBind:{screen:PanelButtonBind},
    RobotHome: {screen:RobotHome},
    RobotCall: {screen:RobotCall},
    RobotOrderDetail: {screen:RobotOrderDetail},
    RobotOrderList: {screen: RobotOrderList},
    RobotLocation: {screen:RobotLocation},
    ScrollCurtainsDebug: {screen: ScrollCurtainsDebug},
    SmartPanel: {screen: SmartPanel},
    ButtonSetting: {screen: ButtonSetting},
    PanelSetting: {screen: PanelSetting},
    ButtonSettingDetail: {screen: ButtonSettingDetail},
    ProtocolController:{screen: ProtocolController},
    RIStudyController:{screen:RIStudyController},
    RadioCurtains:{screen: RadioCurtains},
    GatewayHomePage:{screen:GatewayHomePage},
    ZigbeeDoorLock:{screen:ZigbeeDoorLock},
    ZigbeeDoorFingerList:{screen: ZigbeeDoorFingerList},
    ZigbeeSmokeAlarm: {screen:ZigbeeSmokeAlarm},
    ZigbeeHumanInduction: {screen:ZigbeeHumanInduction},
    ZigbeeAirQuality: {screen: ZigbeeAirQuality},
    Invertor:{screen:Invertor},
    Coulometer:{screen:Coulometer},
    SmartVoiceBox:{screen: SmartVoiceBox},
    SmartVoiceBoxLocation:{screen: SmartVoiceBoxLocation},
    SmartVoiceBoxLog: {screen: SmartVoiceBoxLog},
    CombineWindow: {screen: CombineWindow},
    DreamShutter: {screen: DreamShutter},
    WebPannel: {screen:WebPannel},
    BleGroupLight: {screen:BleGroupLight}
}

// 智能相关的所有页面
const SmartViews = {
    AddSmart: {screen:AddSmart},
    AddCondition: {screen:AddCondition},
    AddExecute: {screen:AddExecute},
    ExecuteSmart: {screen:ExecuteSmart},
    ExecuteScene: {screen:ExecuteScene},
    ExecuteDevice: {screen:ExecuteDevice},
    ExecuteDeviceList: {screen:ExecuteDeviceList},
    SmartTiming: {screen:SmartTiming},
    SmartDuring: {screen: SmartDuring},
    SmartDevices:{screen:SmartDevices},
    EffectTime: {screen:EffectTime},
    SmartOutdoorWeather: {screen:SmartOutdoorWeather},
    AddHouse: {screen:AddHouse},
    ExecuteNotification: {screen:ExecuteNotification}
}

//安防相关的所有页面
const SecurityViews = {
    Camera: {screen: Camera},
    CameraList: {screen: CameraList},
    Authorize: {screen: Authorize}
}

//语音相关页面
const VoiceViews = {
    //语音控制
    VoiceControlPage:{screen:VoiceControlPage},
    VoiceQuestion:{screen:VoiceQuestion},
    VoiceSetting:{screen: VoiceSetting}
}

// 公用页面
const common = {
    AddScene: {screen: AddScene},
    SceneSetting: {screen:SceneSetting},
    SenceDeviceList: {screen:SenceDeviceList},
    SelectThumb: {screen: SelectThumb},
    Device: {screen: Device},
    SelectDevice: {screen: SelectDevice},
    DeviceCombination: {screen: DeviceCombination},
    DeviceSetting: {screen: DeviceSetting},
    HouseSetting:{screen: HouseSetting},
    HouseLayout:{screen: HouseLayout},
    SwitchList:{screen:SwitchList},
    SwitchBinding: {screen: SwitchBinding},
    SwitchBindingResult: {screen: SwitchBindingResult},
    SceneRecommend: {screen: SceneRecommend},
    SceneRecommendDetail: {screen: SceneRecommendDetail},
    Logs: {screen: Logs},
    BindPhone: {screen: BindPhone},
    ReName: {screen: ReName},
    SettingOtherName: {screen: SettingOtherName},
    SenceSmartList: {screen: SenceSmartList},
    SenceChooseSmart: {screen: SenceChooseSmart},

    Setting_Timing:{screen:Setting_Timing},
    Seting_Room:{screen:Seting_Room},
    Setting_Maxopenning:{screen:Setting_Maxopenning},
    Setting_Timeout:{screen:Setting_Timeout},
    Setting_Overelectric:{screen:Setting_Overelectric},
    Setting_Voltage:{screen:Setting_Voltage},
    Setting_ParamsSetting:{screen:Setting_ParamsSetting},
    Setting_AboutController:{screen:Setting_AboutController},
    Setting_Share:{screen:Setting_Share},
    ShareDetail:{screen:ShareDetail},
    Setting_Timing_AddTiming:{screen:Setting_Timing_AddTiming},
    Setting_Log:{screen:Setting_Log},
    LogDetail: {screen: LogDetail},
    PriceSetting:{screen: PriceSetting},
    Setting_Dependency:{screen:Setting_Dependency},
    Setting_Attributes:{screen:Setting_Attributes},
    Setting_NobodyTime:{screen:Setting_NobodyTime},
    Setting_PowerStatus:{screen:Setting_PowerStatus},

    ExecuteDevice: {screen:ExecuteDevice},
    WiFiGuide: {screen:WiFiGuide},
    WiFiPassword: {screen:WiFiPassword},
    WiFiConnecting: {screen:WiFiConnecting},
    WiFiSettingRoom: {screen:WiFiSettingRoom},
    WiFiUDPPrepare: {screen:WiFiUDPPrepare},
    WiFiList: {screen:WiFiList},
    WiFiAddRoom:{screen:WiFiAddRoom},
    WiFiUDPConnecting:{screen:WiFiUDPConnecting},
    SelectDefaultImage:{screen:SelectDefaultImage},
    WiFiSettingName:{screen:WiFiSettingName},
    WiFiDeviceCategory:{screen:WiFiDeviceCategory},
    UnbindGateway: {screen:UnbindGateway},
    WifiPanelButtonList: {screen: WifiPanelButtonList},
    WifiButtonBind: {screen: WifiButtonBind},
    ZigbeePanelBindDevice: {screen:ZigbeePanelBindDevice},
    GatewaySetting: {screen:GatewaySetting},
}

//登录
const LoginStackNavigator = createStackNavigator({
    ...TCPViews,
    ...DevicesPanel,
    Login: {screen: Login},
    ForgetPsw: {screen: ForgetPsw},
    Protocol: {screen: Protocol},
    BindPhone: {screen: BindPhone},
    LoginByMsg: {screen: LoginByMsg},
    DebugPage: {screen: DebugPage},
}, {
    initialRouteName: 'Login',
    transitionConfig: ()=> ({
        screenInterpolator: StackViewStyleInterpolator.forHorizontal
    }),
    headerMode: 'float',
    defaultNavigationOptions: {
        headerRight: <View/>,
        headerStyle: {
            borderBottomWidth: 0,
            shadowOpacity: 0,
            elevation: 0,
            paddingTop: Platform.select({ 
                'android': StatusBarHeight,
                'ios': isNewiPhone ? StatusBarHeight : 0
            }),
            height: Platform.select({
                'android': NavigationBarHeight + StatusBarHeight,
                'ios': NavigationBarHeight + iPhoneHeaderHeightOffset
            }),
            backgroundColor: Colors.transparency
        },
    },
})

//首页
const HomeStackNavigator = createStackNavigator({
    ...common,
    ...SmartViews,
    ...SecurityViews,
    ...DevicesPanel,
    ...VoiceViews,
	Home:{screen:Home},
    AddDeviceQRCode:{screen:AddDeviceQRCode},
    AddDevice:{screen:AddDevice},
    AddHouse:{screen:AddHouse},
    MyHouse:{screen:MyHouse},
    EnvironmentObservation:{screen:EnvironmentObservation},
    EnvironmentParams:{screen:EnvironmentParams},
    ElectricityDetail:{screen:ElectricityDetail},
    ElectricityRank:{screen:ElectricityRank},
    Scene: {screen: Scene},
    SearchPage: {screen: SearchPage},
    Protocol: {screen: Protocol},
    MideaAuthonList: {screen: MideaAuthonList},
    GroupManageList: {screen:GroupManageList}
},{
    initialRouteName:'Home',
    transitionConfig: ()=> ({
        screenInterpolator: StackViewStyleInterpolator.forHorizontal
    }),
    headerMode: 'float',
    defaultNavigationOptions:{
        headerRight: <View/>,
        headerStyle: {
            borderBottomWidth: 0,
            shadowOpacity: 0,
            elevation: 0,
            paddingTop: Platform.select({ 
                'android': StatusBarHeight,
                'ios': isNewiPhone ? StatusBarHeight : 0
            }),
            height: Platform.select({
                'android': NavigationBarHeight + StatusBarHeight,
                'ios': NavigationBarHeight + iPhoneHeaderHeightOffset
            })
        },
    },
	navigationOptions: ({navigation, screenProps}) => ({
        tabBarVisible: navigation.state.index === 0,
	})
})

//智能
const SmartStackNavigator = createStackNavigator({
    ...SmartViews,
    ...VoiceViews,
    Smart: {screen:Smart},
    SmartRecommend: {screen:SmartRecommend},
    Logs: {screen: Logs},
},{
    initialRouteName: 'Smart',
    transitionConfig: ()=> ({
        screenInterpolator: StackViewStyleInterpolator.forHorizontal
    }),
    headerMode: 'float',
    defaultNavigationOptions: {
        headerRight: <View/>,
        headerStyle: {
            borderBottomWidth: 0,
            shadowOpacity: 0,
            elevation: 0,
            paddingTop: Platform.select({ 
                'android': StatusBarHeight,
                'ios': isNewiPhone ? StatusBarHeight : 0
            }),
            height: Platform.select({
                'android': NavigationBarHeight + StatusBarHeight,
                'ios': NavigationBarHeight + iPhoneHeaderHeightOffset
            })
        },
    },
    navigationOptions: ({navigation, screenProps}) => ({
        tabBarVisible: navigation.state.index === 0
    })
})

//房间
const RoomStackNavigator = createStackNavigator({
    ...common,
    ...SmartViews,
    ...SecurityViews,
    ...DevicesPanel,
    ...VoiceViews,
    AddRoom:{screen:AddRoom},
    Room:{screen:Room},
    RoomDetail:{screen:RoomDetail},
    AddDeviceQRCode:{screen:AddDeviceQRCode},
    AddDevice:{screen:AddDevice},
    SelectFloor:{screen:SelectFloor},
    RoomSelectDevice: {screen:RoomSelectDevice},
    Protocol: {screen: Protocol},
    FloorsOrder: {screen: FloorsOrder}
}, {
    initialRouteName: 'Room',
    transitionConfig: ()=> ({
        screenInterpolator: StackViewStyleInterpolator.forHorizontal
    }),
    headerMode: 'float',
    defaultNavigationOptions: {
        headerRight: <View/>,
        headerStyle: {
            borderBottomWidth: 0,
            shadowOpacity: 0,
            elevation: 0,
            paddingTop: Platform.select({ 
                'android': StatusBarHeight,
                'ios': isNewiPhone ? StatusBarHeight : 0
            }),
            height: Platform.select({
                'android': NavigationBarHeight + StatusBarHeight,
                'ios': NavigationBarHeight + iPhoneHeaderHeightOffset
            })
        },
    },
    navigationOptions: ({navigation, screenProps}) => ({
        tabBarVisible: navigation.state.index === 0,
    })
})

//用户
const UserStackNavigator = createStackNavigator({
    ...common,
    ...SecurityViews,
    ...VoiceViews,
    ...TCPViews,
    ...DevicesPanel,
    ForgetPsw: {screen: ForgetPsw},
    User: {screen: User},
    UserSetting: {screen: UserSetting},
    DeviceManage: {screen: DeviceManage},
    ControllerSetting:{screen: ControllerSetting},
    Energy: {screen: Energy},
    Member: {screen: Member},
    Message: {screen: Message},
    MessageSetting: {screen: MessageSetting},
    MessageDetail: {screen: MessageDetail},
    MessageDetailList: {screen: MessageDetailList},
    DeviceMessageManage: {screen: DeviceMessageManage},
    AddDeviceQRCode:{screen:AddDeviceQRCode},
    AddDevice:{screen:AddDevice},
    ControllerSetting_Producer:{screen:ControllerSetting_Producer},
    ControllerSetting_Location:{screen:ControllerSetting_Location},
    ControllerSetting_StartUsing:{screen:ControllerSetting_StartUsing},
    ControllerSetting_HardwareUpdate:{screen:ControllerSetting_HardwareUpdate},
    ControllerSetting_Mac:{screen:ControllerSetting_Mac},
    MemberSetting: {screen: MemberSetting},
    DevicePermission: {screen: DevicePermission},
    RoomPermission: {screen: RoomPermission},
    AddMember: {screen: AddMember},
    Transfer: {screen: Transfer},
    TransferWarrning: {screen: TransferWarrning},
    GroupManageList: {screen:GroupManageList},
    ApplyforDebug: {screen:ApplyforDebug},
    ApplyDebugRecord: {screen:ApplyDebugRecord},
    Setting:{screen:Setting},
    Protocol: {screen:Protocol},
    Questions: {screen:Questions},
    AboutUs: {screen:AboutUs},
    DebugPage: {screen: DebugPage},
    MessageManage: {screen: MessageManage},
    NetworkSetting: {screen: NetworkSetting},
    AccountLoginRecord: {screen: AccountLoginRecord},
    MemberLoginRecord: {screen: MemberLoginRecord},
    MultiFunctionRelay: {screen:MultiFunctionRelay},
    AddMultiFunctionDevice: {screen:AddMultiFunctionDevice},
    LaboratoryList: {screen: LaboratoryList},
    ShortCutList: {screen: ShortCutList},
    ControllerThumb: {screen: ControllerThumb},
    CloseAccount: {screen:CloseAccount},
    FindDevices: {screen:FindDevices},
    EffectTime: {screen:EffectTime},
    WechatBind: {screen:WechatBind},
    ProtocolControlSetting:{screen:ProtocolControlSetting},
    ProtocolControlList:{screen:ProtocolControlList},
    ProtocolControlCreat:{screen:ProtocolControlCreat},
    RIStudySetting: {screen:RIStudySetting},
    ZigbeeConfigWifi: {screen:ZigbeeConfigWifi},
    TimingManager: {screen:TimingManager},
    FirmwareUpgrade: {screen: FirmwareUpgrade},
    FirmwareUpgradeLog: {screen: FirmwareUpgradeLog},
    ReplaceDevice: {screen:ReplaceDevice},
    EnvironmentObservation:{screen:EnvironmentObservation},
    BleConfigWebPage: {screen: BleConfigWebPage}
},{
    initialRouteName: 'User',
    transitionConfig: ()=> ({
        screenInterpolator: StackViewStyleInterpolator.forHorizontal
    }),
    headerMode: 'float',
    defaultNavigationOptions: {
        headerRight: <View/>,
        headerStyle: {
            borderBottomWidth: 0,
            shadowOpacity: 0,
            elevation: 0,
            paddingTop: Platform.select({ 
                'android': StatusBarHeight,
                'ios': isNewiPhone ? StatusBarHeight : 0
            }),
            height: Platform.select({
                'android': NavigationBarHeight + StatusBarHeight,
                'ios': NavigationBarHeight + iPhoneHeaderHeightOffset
            })
        },
    },
    navigationOptions: ({navigation, screenProps}) => ({
        tabBarVisible: navigation.state.index === 0,
    })
})

const MainTabNavigator = createBottomTabNavigator({
    Home: {
        screen: HomeStackNavigator,
        navigationOptions: {
            tabBarLabel: ({focused}) => (
                focused ? null : <Text style={styles.labelInactive}>收藏</Text>
            ),
            tabBarIcon: ({focused}) => (
                <Image
                    source={focused ? homeIcon_select : homeIcon}
                    style={focused ? {width:40,height:37,resizeMode:'contain'} : styles.icon  }
                />
            ),
        },
    },
    Room: {
        screen: RoomStackNavigator,
        navigationOptions: {
            tabBarLabel: '房间',
            tabBarIcon: ({focused}) => (
                <Image
                    source={focused ? roomIcon_select : roomIcon}
                    style={styles.icon}
                />
            ),
        },
    },
    Center: {
        screen:Home,
        navigationOptions: {
			tabBarLabel: <View/>,
            tabBarIcon: ({focused}) => (
                <Image
                    source={center_icon}
                    style={{height:44,width:44}}
                />
            ),
            tabBarOnPress: ({navigation}) => {
                navigation.navigate('VoiceControlPage')
                return
            }
        },
    },
    Smart: {
        screen: SmartStackNavigator,
        navigationOptions: {
            tabBarLabel: '智能',
            tabBarIcon: ({focused}) => (
                <Image
                    source={focused ? smartIcon_select : smartIcon}
                    style={styles.icon}
                />
            ),
        },
    },
    User: {
        screen: UserStackNavigator,
        navigationOptions: {
            tabBarLabel: '我的',
            tabBarIcon: ({focused}) => (
                <BadgeTab tabImage={
                    <Image
                        source={focused ? userIcon_select : userIcon}
                        style={styles.icon}
                    />
                }/>
            ),
        },
    }
}, {
    animationEnabled: false, // 切换页面时不显示动画
    swipeEnabled: false, // 禁止左右滑动
    backBehavior: 'none', // 按 back 键是否跳转到第一个 Tab， none 为不跳转
    initialRouteName: 'Home',
    lazy: true,
    tabBarComponent: MainTabBar,
    // tabBarOptions: {
    //     activeTintColor: Colors.themeTextBlack,
    //     inactiveTintColor: Colors.themeTextLightGray,
    //     showIcon: true, // android 默认不显示 icon, 需要设置为 true 才会显示
    //     indicatorStyle: {height: 0}, // android 中TabBar下面会显示一条线，高度设为 0 后就不显示线了
    //     style: {
    //         backgroundColor: Colors.white, // TabBar 背景色
    //     },
    //     labelStyle: {
    //         fontSize: ScreenAdapt.tabLabelFontSize,
    //         bottom: 3
    //     },
    // },
})

export const MainControler = createAppContainer(MainTabNavigator);
export const LoginControler = createAppContainer(LoginStackNavigator);

const styles = StyleSheet.create({
    icon: {
        width: 24,
        height: 24,
        resizeMode: 'contain',
    },
    labelInactive:{
        fontSize: ScreenAdapt.tabLabelFontSize,
        marginTop: 3,
        bottom: 5,
        color:Colors.themeTextLightGray,
        alignSelf:'center'
    }
});

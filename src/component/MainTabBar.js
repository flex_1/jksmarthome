/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Image, Text, View, TouchableOpacity,SafeAreaView,Animated, Platform} from 'react-native';
import { connect } from 'react-redux';
import {ScreenAdapt} from '../util/ScreenUtil';

const homeIcon = require('../images/bottom_tab/home_inactive.png')
const homeIcon_select = require('../images/bottom_tab/xiaosa.png')
const homeIcon_dark = require('../images/bottom_tab/home_inactive_dark.png')

const roomIcon = require('../images/bottom_tab/room_inactive.png')
const roomIcon_select = require('../images/bottom_tab/room_active.png')
const roomIcon_dark = require('../images/bottom_tab/room_inactive_dark.png')
const roomIcon_select_dark = require('../images/bottom_tab/room_active_dark.png')

const userIcon = require('../images/bottom_tab/user_inactive.png')
const userIcon_select = require('../images/bottom_tab/user_active.png')
const userIcon_dark = require('../images/bottom_tab/user_inactive_dark.png')
const userIcon_select_dark = require('../images/bottom_tab/user_active_dark.png')

const smartIcon = require('../images/bottom_tab/smart_inactive.png')
const smartIcon_select = require('../images/bottom_tab/smart_active.png')
const smartIcon_dark = require('../images/bottom_tab/smart_inactive_dark.png')
const smartIcon_select_dark = require('../images/bottom_tab/smart_active_dark.png')

const center_icon = require('../images/bottom_tab/center.png')

class MainTabBar extends Component {
    
    constructor(props) {
		super(props);
		this.state = {
		}
    }

    getIconImage(focused,index){
        const isDark = this.props.themeInfo.isDark

        if(isDark){
            if(index == 0){
                return focused ? homeIcon_select : homeIcon_dark
            }else if(index == 1){
                return focused ? roomIcon_select_dark : roomIcon_dark
            }
            else if(index == 2){
                return null
                // return center_icon
            }
            else if(index == 3){
                return focused ? smartIcon_select_dark : smartIcon_dark
            }else if(index == 4){
                return focused ? userIcon_select_dark : userIcon_dark
            }
        }else{
            if(index == 0){
                return focused ? homeIcon_select : homeIcon
            }else if(index == 1){
                return focused ? roomIcon_select : roomIcon
            }
            else if(index == 2){
                return null
                // return center_icon
            }
            else if(index == 3){
                return focused ? smartIcon_select : smartIcon
            }else if(index == 4){
                return focused ? userIcon_select : userIcon
            }
        }
    }

    _renderIcon = ({ route, focused, index, isDark }) => {
        let iconStyle = styles.icon

        if(index == 0 && focused){
            iconStyle = {width:40,height:37,resizeMode:'contain'}
        }
        // else if(index == 2){
        //     iconStyle = {height:44,width:44,resizeMode:'contain'}
        // }

        return(
            <Image style={iconStyle} source={this.getIconImage(focused,index)}/>
        )
    }

    _renderLabel = ({ route, focused, index }) => {
        const Colors = this.props.themeInfo.colors
        const {
            labelStyle,
            showLabel,
            showIcon,
            allowFontScaling,
        } = this.props;
    
        if (showLabel === false) {
            return null;
        }
        if(index == 2){
            return null
        }
    
        const label = this.props.getLabelText({ route });
        const tintColor = focused ? Colors.themeText : Colors.tabInactive
    
        let labelText = ''
        if (typeof label === 'string') {
            labelText = label
        }
        if (typeof label === 'function') {
            if(focused){
                return null
            }else{
                labelText = '收藏'
            }
        }
        return (
            <Animated.Text
                numberOfLines={1}
                style={[
                    styles.labelInactive,
                    { color: tintColor },
                ]}
                allowFontScaling={allowFontScaling}
            >
                {labelText}
            </Animated.Text>
        )
    }

    renderTas(){
        const Colors = this.props.themeInfo.colors
        const isDark = this.props.themeInfo.isDark
        const {
            navigation,
            keyboardHidesTabBar,
            onTabPress,
            onTabLongPress,
            safeAreaInset,
            style,
            tabStyle,
        } = this.props;

        const { navigate } = navigation
        const { routes } = navigation.state;

        let tabs = routes.map((route, index) => {

            if(index == 2){
                return null
            }

            const focused = index === navigation.state.index;
            const scene = { route, focused, index, isDark };

            return(
                <TouchableOpacity
                    activeOpacity={0.7}
                    key={route.key}
                    style={styles.tab}
                    onPress={()=>{onTabPress({ route })
                }}>
                    {this._renderIcon(scene)}
                    {this._renderLabel(scene)}
                </TouchableOpacity>
            )
        })

        return (
            <View style={[
                styles.wrapper,
                {borderTopColor: Colors.split}]
            }>
                {tabs}
            </View>
        )
    }

    render(){
        const Colors = this.props.themeInfo.colors

        return(
            <SafeAreaView style={{backgroundColor: Colors.themeBg}}>
                {this.renderTas()}
            </SafeAreaView>
        )
    }

}

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(MainTabBar)


const styles = StyleSheet.create({
    wrapper:{
        width:'100%',
        flexDirection:'row',
        borderTopWidth: 1,
        justifyContent: 'center',
        alignItems:'center',
        height: Platform.select({
            ios: 50,
            android: 50
        })
    },
    tab:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
    },
    icon: {
        width: 24,
        height: 24,
        resizeMode: 'contain',
    },
    labelInactive:{
        fontSize: ScreenAdapt.tabLabelFontSize,
        alignSelf:'center',
        marginTop: 2
    }
});

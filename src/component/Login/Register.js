/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ImageBackground,
    TextInput,
    Button
} from 'react-native';
import {DeviceEventEmitter} from "react-native";
import {Colors, NetUrls} from '../../common/Constants';
import LocalTokenHandler from '../../util/LocalTokenHandler';
import {postJson} from '../../util/ApiRequest';
import {StatusBarHeight, NavigationBarHeight} from '../../util/ScreenUtil';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeftView from "../../common/CustomComponent/HeaderLeftView";
import md5 from 'react-native-md5';

const dismissKeyboard = require('dismissKeyboard');

const codeTime = 60;  //倒计时初始值
let isShow = false

class Register extends Component {

    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: <HeaderLeftView navigation={navigation} text={'注册账号'}/>
        }
    };

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation
        this.callBack = getParam('callBack')
        this.state = {
            phoneNumber: '',
            code:'',
            pass: '',
            pass2: '',
            hideOrshow: true,
            timerCount: codeTime,
            timerTitle: '获取验证码',
            isCounting: false,
            isChecked: false
        }
    }

    componentDidMount() {

    }

    componentWillUnmount() {
        this.interval && clearInterval(this.interval);
    }

    async getMsg() {
        dismissKeyboard()
        if (!this.checkPhone()) return
        SpinnerManager.show()
        try {
            const {pop} = this.props.navigation;
            let res = await postJson({
                url: NetUrls.getMsg,
                useToken: false,
                params: {
                    'mobile': this.state.phoneNumber,
                    'type': 4, //1.找回密码 2-邀请成员 3-过户 4-注册
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                this.setState({
                    timerCount: codeTime,
                    isCounting: true
                })
                this.onTimes();
                ToastManager.show("获取验证码成功");
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误，请稍后重试');
        }

    }

    async sendRegister() {
        dismissKeyboard()
        if (!this.checkPhone()) return
        if (!this.state.code) {
            ToastManager.show('请输入验证码')
            return
        }
        if (!this.state.pass || this.state.pass.length<6) {
            ToastManager.show('请输入6至16位密码')
            return
        }
        if (!this.state.isChecked) {
            ToastManager.show('请先同意 用户协议 和 隐私政策')
            return
        }
        const {pop} = this.props.navigation;
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.register,
                useToken: false,
                params: {
                    'phone': this.state.phoneNumber,
                    'pwd': md5.hex_md5(this.state.pass),
                    // 'pwd2': md5.hex_md5(this.state.pass2),
                    'code': this.state.code
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                ToastManager.show("注册成功");
                this.callBack && this.callBack(this.state.phoneNumber);
                pop()
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(e));
        }

    }

    //号码校验
    checkPhone() {
        var phone = this.state.phoneNumber;
        if (!phone) {
            ToastManager.show('请输入手机号码')
            return false;
        }
        if (!(/^1[3456789]\d{9}$/.test(phone))) {
            ToastManager.show('请输入正确的号码')
            return false;
        }
        return true
    }

    /**
     * 倒计时
     */
    onTimes() {
        this.interval = setInterval(() => {
            var timer = this.state.timerCount - 1;
            if (timer <= 0) {
                this.interval && clearInterval(this.interval);
                this.setState({
                    timerTitle: '重新获取',
                    isCounting: false
                })
            } else {
                this.setState({timerCount: timer,})
            }
        }, 1000)
    }

    getTitles(){
        const {pop} = this.props.navigation;
        return (
            <View style={{marginTop:30}}>
                <TouchableOpacity style={styles.backTouch} onPress={() => {
                    pop()
                }}>
                    <Image style={{ width: 10, height: 18 }} source={require('../../images/back.png')} />
                    <Text style={{ marginLeft: 15, fontSize: 24, color: Colors.themeTextBlack, fontWeight: 'bold' }}>注册账号</Text>
                </TouchableOpacity>
            </View>
        )
    }

    getProtocol(){
        const {navigate} = this.props.navigation;
        let checkIcon = require('../../images/check_blue.png')
        if(!this.state.isChecked){
            checkIcon = require('../../images/uncheck_blue.png')
        }

        return(
            <View style={{flexDirection: 'row',width:'100%',paddingHorizontal:10,marginTop:20,alignItems:'center'}}>
                <TouchableOpacity style={{padding: 10}} onPress={()=>{
                    this.setState({isChecked: !this.state.isChecked})
                }}>
                    <Image style={{width:14,height:14}} source={checkIcon}/>
                </TouchableOpacity>
                <Text style={{fontSize:14}}>我已阅读并同意</Text>
                <TouchableOpacity style={{paddingVertical: 10}} onPress={()=>{
                    navigate('Protocol',{title:'用户协议',type:1})
                }}>
                    <Text style={{fontSize:14,color:Colors.tabActiveColor}}>《用户协议》</Text>
                </TouchableOpacity>
                <Text>和</Text>
                <TouchableOpacity style={{paddingVertical: 10}} onPress={()=>{
                    navigate('Protocol',{title:'隐私政策',type:2})
                }}>
                    <Text style={{fontSize:14,color:Colors.tabActiveColor}}>《隐私政策》</Text>
                </TouchableOpacity>
            </View>
        )
    }

    onTextInputView() {
        const inputIcon = this.state.hideOrshow?require('../../images/login/hide_icon.png'):require('../../images/login/show_icon.png')
        
        return (
            <View style={{marginTop:20}}>
                <View style={{paddingHorizontal: 20 }}>
                    <View style={styles.inputWrapper}>
                        <TextInput
                            style={styles.input}
                            keyboardType={'numeric'}
                            maxLength={11}
                            placeholder="请输入11位手机号"
                            clearButtonMode={'while-editing'}
                            placeholderTextColor={Colors.themeBGInactive}
                            onChangeText={(text) => {
                                this.state.phoneNumber = text
                            }}
                        />
                    </View>
                    <View style={[styles.inputWrapper, {marginTop: 20, justifyContent: 'space-between'}]}>
                        <TextInput
                            style={styles.input}
                            placeholder="请输入验证码"
                            keyboardType={'numeric'}
                            clearButtonMode={'while-editing'}
                            placeholderTextColor={Colors.themeBGInactive}
                            maxLength={6}
                            onChangeText={(text) => {
                                this.state.code = text
                            }}
                        />
                        <TouchableOpacity activeOpacity={0.7} style={{height:40,justifyContent:'center',alignItems:'center'}} onPress={() => {
                            if(this.state.isCounting){
                                return
                            }
                            this.getMsg()
                        }}>
                            <Text style={{marginRight: 20, color: Colors.tabActiveColor, fontSize: 14}}>
                                {this.state.isCounting ? this.state.timerCount : this.state.timerTitle}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.inputWrapper, {marginTop: 20, justifyContent: 'space-between'}]}>
                        <TextInput
                            style={styles.input}
                            maxLength={16}
                            placeholder="请输入6至16位密码"
                            clearButtonMode={'while-editing'}
                            placeholderTextColor={Colors.themeBGInactive}
                            secureTextEntry={this.state.hideOrshow}
                            onChangeText={(text) => {
                                this.state.pass = text
                            }}
                        />
                        <TouchableOpacity onPress={() => {
                            this.state.hideOrshow = isShow
                            this.setState(
                                {...this.state}
                            )
                            isShow = !isShow
                        }}>
                            <Image style={{width: 20, height: 20, marginRight: 20}}
                                   source={inputIcon} resizeMode={'contain'}/>
                        </TouchableOpacity>
                    </View>
                </View>
                
                <TouchableOpacity style={styles.loginBtn} onPress={() => {
                    this.sendRegister()
                }}>
                    <Text style={{fontSize: 16, color: Colors.white}}>注册</Text>
                </TouchableOpacity>
                {this.getProtocol()}
            </View>
        );
    }

    render() {
        return (
            <KeyboardAwareScrollView behavior="padding" style={styles.container} keyboardShouldPersistTaps="handled">
                {this.onTextInputView()}
            </KeyboardAwareScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    imageStyle: {
        height: 170,
        width: '30%',
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.white,
    },
    loginBtn: {
        height: 40,
        backgroundColor: Colors.tabActiveColor,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 30
    },
    input: {
        flex: 1,
        paddingLeft: 20,
        height:40,
        fontSize: 16
    },
    inputWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        borderRadius: 10,
        paddingVertical:4,
        backgroundColor: Colors.inputTextColor
    },
    inputTitle: {
        fontSize: 18,
        color: Colors.themeTextBlack
    },
    backTouch: {
        marginLeft: 20,
        alignItems: 'center',
        flexDirection: 'row'
    },
});

export default Register

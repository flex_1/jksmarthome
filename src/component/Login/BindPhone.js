/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ImageBackground,
    TextInput,
    Button
} from 'react-native';
import {DeviceEventEmitter} from "react-native";
import {Colors, NetUrls, NotificationKeys, LocalStorageKeys} from '../../common/Constants';
import LocalTokenHandler from '../../util/LocalTokenHandler';
import {postJson} from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import AsyncStorage from '@react-native-community/async-storage';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import { connect } from 'react-redux';
import AppConfigs from '../../util/AppConfigs';
import { updateUserInfo } from '../../redux/actions/UserInfoAction';

const dismissKeyboard = require('dismissKeyboard');
const codeTime = 60;  //倒计时初始值

class BindPhone extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'手机号绑定'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation
        this.type = getParam('type')  // type: 2-微信   0-QQ聊天
        this.uid = getParam('uid')
        this.infoParmas = getParam('infoParmas')

        this.isChangePhone = getParam('isChangePhone')  //是否是(换)绑定手机号
        this.updateUserInfo = getParam('updateUserInfo') // 更新用户数据回调

        this.state = {
            phoneNumber: '',
            code:'',
            timerCount: codeTime,
            timerTitle: '获取验证码',
            isCounting: false
        }
    }

    componentDidMount() {

    }

    componentWillUnmount() {
        this.interval && clearInterval(this.interval);
    }

    async getMsg() {
        dismissKeyboard()
        if (!this.checkPhone()) return
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.getMsg,
                useToken: false,
                params: {
                    'mobile': this.state.phoneNumber,
                    'type': this.isChangePhone ? 8:5, //1-找回密码 2-邀请成员 3-过户 4-注册 5-手机绑定 8-修改手机号
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                this.setState({
                    timerCount: codeTime,
                    isCounting: true
                })
                this.onTimes();
                ToastManager.show("获取验证码成功");
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误，请稍后重试');
        }

    }

    // 第三方登录绑定手机
    async thirdLoginBindPhone() {
        dismissKeyboard()
        if (!this.checkPhone()) return
        if (!this.state.code) {
            ToastManager.show('请输入验证码')
            return
        }
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.thirdLoginBind,
                useToken: false,
                params: {
                    phone: this.state.phoneNumber,
                    code: this.state.code,
                    uid: this.uid,
                    type: this.type,
                    ...this.infoParmas
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                LocalTokenHandler.save(res.result, () => {
                    DeviceEventEmitter.emit(NotificationKeys.kLoginNotification)
                    AsyncStorage.setItem(LocalStorageKeys.kLoginAgreement,'true')
                    AppConfigs.pushRegisterId()
                })
                ToastManager.show("绑定成功");
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(e));
        }
    }

    // 普通绑定手机号
    async bindPhone() {
        dismissKeyboard()
        if (!this.checkPhone()) return
        if (!this.state.code) {
            ToastManager.show('请输入验证码')
            return
        }
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.changePhoneNumber,
                params: {
                    phone: this.state.phoneNumber,
                    code: this.state.code
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                let userToken = await LocalTokenHandler.get()
                userToken.phone = res.result
                LocalTokenHandler.save(userToken, () => {
                    this.updateUserInfo && this.updateUserInfo(userToken)
                    this.props.updateUserInfo()
                    this.props.navigation.pop()
                })
                ToastManager.show("绑定成功");
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(e));
        }
    }

    //号码校验
    checkPhone() {
        var phone = this.state.phoneNumber;
        if (!phone) {
            ToastManager.show('请输入手机号码')
            return false;
        }
        if (!(/^1[3456789]\d{9}$/.test(phone))) {
            ToastManager.show('请输入正确的号码')
            return false;
        }
        return true
    }

    /**
     * 倒计时
     */
    onTimes() {
        this.interval = setInterval(() => {
            var timer = this.state.timerCount - 1;
            if (timer <= 0) {
                this.interval && clearInterval(this.interval);
                this.setState({
                    timerTitle: '重新获取',
                    isCounting: false
                })
            } else {
                this.setState({timerCount: timer,})
            }
        }, 1000)
    }
    
    onTextInputView() {
        const Colors = this.props.themeInfo.colors
        return (
            <View style={{paddingHorizontal: 20, marginTop: 20 }}>
                <Text style={{fontSize:14,color:Colors.themeTextLight}}>该账户未绑定手机号，请先完成绑定</Text>
                <View style={[styles.inputWrapper,{marginTop: 20,backgroundColor: Colors.themeBg}]}>
                    <TextInput
                        style={[styles.input,{color: Colors.themeText}]}
                        keyboardType={'numeric'}
                        maxLength={11}
                        placeholder="请输入11位手机号"
                        clearButtonMode={'while-editing'}
                        placeholderTextColor={Colors.themeInactive}
                        onChangeText={(text) => {
                            this.state.phoneNumber = text
                        }}
                    />
                </View>
                <View style={[styles.inputWrapper, {marginTop: 20, justifyContent: 'space-between',backgroundColor: Colors.themeBg}]}>
                    <TextInput
                        style={[styles.input,{color: Colors.themeText}]}
                        placeholder="请输入验证码"
                        keyboardType={'numeric'}
                        clearButtonMode={'while-editing'}
                        placeholderTextColor={Colors.themeInactive}
                        maxLength={6}
                        onChangeText={(text) => {
                            this.state.code = text
                        }}
                    />
                    <TouchableOpacity activeOpacity={0.7} style={{height:40,justifyContent:'center',alignItems:'center'}} onPress={() => {
                        if(this.state.isCounting){
                            return
                        }
                        this.getMsg()
                    }}>
                        <Text style={{marginHorizontal: 20, color: Colors.themeButton, fontSize: 14}}>
                            {this.state.isCounting ? this.state.timerCount : this.state.timerTitle}
                        </Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={styles.loginBtn} onPress={() => {
                    if(this.isChangePhone){
                        this.bindPhone()
                    }else{
                        this.thirdLoginBindPhone()
                    }
                }}>
                    <Text style={{fontSize: 16, color: Colors.white}}>绑定</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.onTextInputView()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    imageStyle: {
        height: 170,
        width: '30%',
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.white,
    },
    loginBtn: {
        height: 40,
        backgroundColor: Colors.tabActiveColor,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        marginTop: 30
    },
    input: {
        flex: 1,
        paddingLeft: 20,
        height:40,
        fontSize: 16
    },
    inputWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        borderRadius: 10,
        paddingVertical:4
    },
    inputTitle: {
        fontSize: 18,
        color: Colors.themeTextBlack
    },
    backTouch: {
        marginLeft: 20,
        alignItems: 'center',
        flexDirection: 'row'
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    }),
    (dispatch) => ({
        updateUserInfo: ()=> dispatch(updateUserInfo())
    })
)(BindPhone)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
} from 'react-native';
import { connect } from 'react-redux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import md5 from 'react-native-md5';
import {NetUrls,CurrentState} from '../../common/Constants';
import {postJson} from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';

const dismissKeyboard = require('dismissKeyboard');
const codeTime = 60;  //倒计时初始值

class ForgetPsw extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

    constructor(props) {
        super(props)
        const {getParam} = props.navigation

        this.phone = getParam('phone') || ''
        this.isSettingPass = getParam('isSettingPass')

        this.state = {
            mobile: this.phone,
            code: '',
            pass: '',
            pass2: '',
            timerCount: codeTime,
            timerTitle: '获取验证码',
            isCounting: false
        };
    }

    componentWillUnmount() {
        this.interval && clearInterval(this.interval);
    }

    async getMsg() {
        dismissKeyboard()
        if (!this.checkPhone()) return
        if (!this.state.mobile) {
            ToastManager.show('请输入手机号')
            return
        }
        SpinnerManager.show()
        try {
            const {pop} = this.props.navigation;
            let res = await postJson({
                url: NetUrls.getMsg,
                useToken: false,
                params: {
                    'mobile': this.state.mobile,
                    'type': 1, //1.找回密码 2-邀请成员
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                this.setState({
                    isCounting: true,
                    timerCount: codeTime
                })
                this.onTimes();
                ToastManager.show("获取验证码成功");
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(e));
        }

    }

    async forgetPwd() {
        dismissKeyboard()
        if(!this.isSettingPass && !this.checkPhone()){
            return
        }
        if (!this.state.mobile) {
            ToastManager.show('请输入手机号')
            return
        }
        if (!this.state.code) {
            ToastManager.show('请输入验证码')
            return
        }
        if (!this.state.pass) {
            ToastManager.show('请输入新密码')
            return
        }
        SpinnerManager.show()
        try {
            const {pop} = this.props.navigation;
            let res = await postJson({
                url: NetUrls.forgetPwd,
                useToken: false,
                params: {
                    'phone': this.state.mobile,
                    'code': this.state.code,
                    'pwd1': md5.hex_md5(this.state.pass),
                    // 'pwd2': md5.hex_md5(this.state.pass2),
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                ToastManager.show("修改密码成功");
                pop()
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(e));
        }
    }

    onTextInputView() {
        const Colors = this.props.themeInfo.colors
        return (
            <View>
                <View style={{paddingHorizontal: 20, marginTop: 20}}>
                    <View style={[styles.inputWrapper, {backgroundColor: Colors.themeBg}]}>
                        <TextInput
                            defaultValue={this.phone}
                            editable={!this.isSettingPass}
                            style={[styles.input,{color: Colors.themeText}]}
                            placeholder="请输入您的手机号"
                            keyboardType={'numeric'}
                            maxLength={11}
                            clearButtonMode={'while-editing'}
                            placeholderTextColor={Colors.themeInactive}
                            onChangeText={(text) => {
                                this.state.mobile = text
                            }}
                        />
                    </View>

                    <View style={[styles.inputWrapper, {marginTop: 20, justifyContent: 'space-between', backgroundColor: Colors.themeBg}]}>
                        <TextInput
                            style={[styles.input,{color: Colors.themeText}]}
                            placeholder="请输入验证码"
                            clearButtonMode={'while-editing'}
                            placeholderTextColor={Colors.themeInactive}
                            maxLength={6}
                            keyboardType={'numeric'}
                            onChangeText={(text) => {
                                this.state.code = text
                            }}
                        />
                        <TouchableOpacity activeOpacity={0.7} style={{height:40,justifyContent:'center',alignItems:'center'}} onPress={() => {
                            if(this.state.isCounting){
                                return
                            }
                            this.getMsg()
                        }}>
                            <Text style={{marginRight: 20, color: Colors.newTheme, fontSize: 14}}>
                                {this.state.isCounting ? this.state.timerCount : this.state.timerTitle}
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={[styles.inputWrapper, {marginTop: 20, backgroundColor: Colors.themeBg}]}>
                        <TextInput
                            style={[styles.input,{color: Colors.themeText}]}
                            placeholder="请输入密码"
                            clearButtonMode={'while-editing'}
                            placeholderTextColor={Colors.themeInactive}
                            secureTextEntry={true}
                            onChangeText={(text) => {
                                this.state.pass = text
                            }}
                        />
                    </View>
                </View>
                <TouchableOpacity style={[styles.loginBtn, {backgroundColor: Colors.newTheme}]} 
                    onPress={() => {
                        // this.login()
                        this.forgetPwd()
                    }}
                    onLongPress={()=>{
                        if(CurrentState == 'inner_ip'){
                            this.props.navigation.navigate('DebugPage')
                        }
                    }}
                >
                    <Text style={{fontSize: 16, color: Colors.white}}>确定</Text>
                </TouchableOpacity>
            </View>
        );
    }

    /**
     * 倒计时
     */
    onTimes() {
        this.interval = setInterval(() => {
            var timer = this.state.timerCount - 1;
            if (timer <= 0) {
                this.interval && clearInterval(this.interval);
                this.setState({
                    timerTitle: '重新获取',
                    isCounting: false
                })
            } else {
                this.setState({timerCount: timer,})
            }
        }, 1000)
    }

    //号码校验
    checkPhone() {
        var phone = this.state.mobile;
        if (!phone) {
            ToastManager.show('请输入手机号码')
            return false;
        }
        if (!(/^1[3456789]\d{9}$/.test(phone))) {
            ToastManager.show('请输入正确的号码')
            return false;
        }
        return true
    }

    render() {
        const Colors = this.props.themeInfo.colors
        return (
            <KeyboardAwareScrollView
                style={{backgroundColor: Colors.themeBaseBg}} 
                behavior="padding"
                keyboardShouldPersistTaps="handled"
            >
                {this.onTextInputView()}
            </KeyboardAwareScrollView>
        )
    }
}

const styles = StyleSheet.create({
    loginBtn: {
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        marginHorizontal: 16,
        marginTop: 30
    },
    input: {
        flex: 1,
        paddingLeft: 20,
        height:40,
        fontSize: 16
    },
    inputWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        borderRadius: 10,
        paddingVertical:4,
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ForgetPsw)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ImageBackground,
    TextInput,
    Button,
} from 'react-native';
import {DeviceEventEmitter} from "react-native";
import {Colors, NetUrls, NotificationKeys, LocalStorageKeys} from '../../common/Constants';
import LocalTokenHandler from '../../util/LocalTokenHandler';
import {postJson} from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import AsyncStorage from '@react-native-community/async-storage';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import { connect } from 'react-redux';
import AppConfigs from '../../util/AppConfigs';
import deviceInfo from '../../util/DeviceInfo';

const dismissKeyboard = require('dismissKeyboard');
const codeTime = 60;  //倒计时初始值

class LoginByMsg extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'验证码登录'}/>,
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation

        this.state = {
            phoneNumber: '',
            code:'',
            timerCount: codeTime,
            timerTitle: '获取验证码',
            isCounting: false,
            isChecked: false,
        }
    }

    componentDidMount() {

    }

    componentWillUnmount() {
        this.interval && clearInterval(this.interval);
    }

    async getMsg() {
        dismissKeyboard()
        if (!this.checkPhone()) return
        SpinnerManager.show()
        try {
            const {pop} = this.props.navigation;
            let res = await postJson({
                url: NetUrls.getMsg,
                useToken: false,
                params: {
                    'mobile': this.state.phoneNumber,
                    'type': 6, //1-找回密码 2-邀请成员 3-过户 4-注册 5-手机绑定 6-注册
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                this.setState({
                    timerCount: codeTime,
                    isCounting: true
                })
                this.onTimes();
                ToastManager.show("获取验证码成功");
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误，请稍后重试');
        }

    }

    async loginByMsg() {
        dismissKeyboard()
        if (!this.checkPhone()) return
        if (!this.state.code) {
            ToastManager.show('请输入验证码')
            return
        }
        let parmas = this.getDeviceParams()
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.loginByMsg,
                useToken: false,
                params: {
                    phone: this.state.phoneNumber,
                    pwd: this.state.code,
                    ...parmas
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                LocalTokenHandler.save(res.result, () => {
                    DeviceEventEmitter.emit(NotificationKeys.kLoginNotification)
                    AsyncStorage.setItem(LocalStorageKeys.kLastLoginAccount,this.state.phoneNumber)
                    AsyncStorage.setItem(LocalStorageKeys.kLoginAgreement,'true')
                    AppConfigs.pushRegisterId()
                })
                AppConfigs.saveAccountNumber(this.state.phoneNumber)
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(e));
        }

    }

    //获取设备信息参数
    getDeviceParams(){
        let phoneModel = ''
        let phoneBrand = ''
        let phoneId = ''
        let deviceName = ''
        try {
            phoneModel = deviceInfo.phoneModel
            phoneBrand = deviceInfo.phoneBrand
            phoneId = deviceInfo.phoneId
            deviceName = deviceInfo.deviceName
        } catch (error) {
            return {}
        }
        return {phoneModel, phoneBrand, phoneId, deviceName}
    }

    //号码校验
    checkPhone() {
        var phone = this.state.phoneNumber;
        if (!phone) {
            ToastManager.show('请输入手机号码')
            return false;
        }
        if (!(/^1[3456789]\d{9}$/.test(phone))) {
            ToastManager.show('请输入正确的号码')
            return false;
        }
        return true
    }

    /**
     * 倒计时
     */
    onTimes() {
        this.interval = setInterval(() => {
            var timer = this.state.timerCount - 1;
            if (timer <= 0) {
                this.interval && clearInterval(this.interval);
                this.setState({
                    timerTitle: '重新获取',
                    isCounting: false
                })
            } else {
                this.setState({timerCount: timer,})
            }
        }, 1000)
    }

    onTextInputView() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={{paddingHorizontal: 20 }}>
                <Text style={{fontSize: 15,color: Colors.themeTextLight,marginVertical: 20}}>提示: 登录即注册成功</Text>
                <View style={[styles.inputWrapper,{backgroundColor: Colors.themeBg}]}>
                    <TextInput
                        style={[styles.input,{color: Colors.themeText}]}
                        keyboardType={'numeric'}
                        maxLength={11}
                        placeholder="请输入11位手机号"
                        clearButtonMode={'while-editing'}
                        placeholderTextColor={Colors.themeInactive}
                        onChangeText={(text) => {
                            this.state.phoneNumber = text
                        }}
                    />
                </View>
                <View style={[styles.inputWrapper, {marginTop: 20, justifyContent: 'space-between', backgroundColor: Colors.themeBg}]}>
                    <TextInput
                        style={[styles.input,{color: Colors.themeText}]}
                        placeholder="请输入验证码"
                        keyboardType={'numeric'}
                        clearButtonMode={'while-editing'}
                        placeholderTextColor={Colors.themeInactive}
                        maxLength={6}
                        onChangeText={(text) => {
                            this.state.code = text
                        }}
                    />
                    <TouchableOpacity activeOpacity={0.7} style={styles.codeWrapper} onPress={() => {
                        if(this.state.isCounting){
                            return
                        }
                        this.getMsg()
                    }}>
                        <Text style={{marginHorizontal: 20, color: Colors.newTheme, fontSize: 14}}>
                            {this.state.isCounting ? this.state.timerCount : this.state.timerTitle}
                        </Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={styles.loginBtn} onPress={() => {
                    this.loginByMsg()
                }}>
                    <Text style={{fontSize: 16, color: Colors.white}}>登录</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.onTextInputView()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        
    },
    imageStyle: {
        height: 170,
        width: '30%',
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.white,
    },
    loginBtn: {
        height: 40,
        backgroundColor: Colors.newTheme,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        marginTop: 30
    },
    input: {
        flex: 1,
        paddingLeft: 20,
        height:40,
        fontSize: 16
    },
    inputWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        borderRadius: 10,
        paddingVertical:4
    },
    inputTitle: {
        fontSize: 18,
        color: Colors.themeTextBlack
    },
    backTouch: {
        marginLeft: 20,
        alignItems: 'center',
        flexDirection: 'row'
    },
    codeWrapper:{
        height:40,
        justifyContent:'center',
        alignItems:'center'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(LoginByMsg)

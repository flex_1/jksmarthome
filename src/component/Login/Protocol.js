/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    View,
} from 'react-native';
import { connect } from 'react-redux';
import { WebView } from 'react-native-webview';
import {postJson} from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeftView from "../../common/CustomComponent/HeaderLeftView";
import { NetUrls } from '../../common/Constants';
import {StatusBarHeight} from '../../util/ScreenUtil';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';

class Protocol extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation
        this.type = getParam('type')

        this.state = {
            content: ''
        }
    }

    componentDidMount() {
        this.requestProtocol()
    }

    componentWillUnmount() {
        
    }

    async requestProtocol() {
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.getProtocol,
                useToken: false,
                params: {
                    type: this.type 
                }
            });
            SpinnerManager.close()
            let htmlString = this.formatHtml(res.result)
            this.setState({
                content: htmlString
            })
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误');
        }

    }

    formatHtml(html){
        return '<html lang="zh-CN"><head>'+
        '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, shrink-to-fit=no">'+
        '</head><body>' +
        html +
        '</body></html>'
    }

    getWebview(){
        if(!this.state.content){
            return null
        }
        return(
            <WebView
                style={styles.container}
                useWebKit={true}
                containerStyle={styles.scorllContent}
                source={{ html: this.state.content }}
                contentContainerStyle={styles.scrollContent}
            />
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.getWebview()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    scorllContent:{
        paddingBottom: 40
    },
    content:{
        fontSize: 16,
        lineHeight: 22
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Protocol)

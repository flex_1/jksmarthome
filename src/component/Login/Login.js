/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ImageBackground,
    TextInput,
    Button,
    Keyboard,
    Animated,
    Dimensions,
    NativeModules,
    requireNativeComponent,
    DeviceEventEmitter,
    Alert,
    ScrollView
} from 'react-native';
import {Colors, NotificationKeys, NetUrls, LocalStorageKeys} from '../../common/Constants';
import LocalTokenHandler from '../../util/LocalTokenHandler';
import {postJson} from '../../util/ApiRequest';
import {StatusBarHeight, NavigationBarHeight, BottomSafeMargin} from '../../util/ScreenUtil';
import md5 from 'react-native-md5';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import AsyncStorage from '@react-native-community/async-storage';
import AppConfigs from '../../util/AppConfigs';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import { connect } from 'react-redux';
import deviceInfo from '../../util/DeviceInfo';
import {ImagesLight, ImagesDark} from '../../common/Themes';
import PersonPrivacyModal from '../CommonPage/PersonPrivacyModal';

const dismissKeyboard = require('dismissKeyboard');

const keybordHeight = Platform.select({
    ios: 120,
    android: 150
})
const JKUMengShare = Platform.select({
    ios: NativeModules.UMShareModule,
    android: NativeModules.UMShareModule
})
const NativeView = requireNativeComponent('SignWithApple');

//import ShareUtile from '../../util/ShareUtil'

class Login extends Component {

    static navigationOptions = {
        header: null,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    }

    constructor(props) {
        super(props);

        const {getParam} = props.navigation
        this.isLogin = getParam('isLogin')
        this.state = {
            phoneNumber: '',
            pass: '',
            isChecked: false,
            translateValue:new Animated.ValueXY({x:0,y:0}),
            showAccountExpand: false,

            phones: [],
            phoneMatching: null,
            privacyModalVisible: false
        }
    }

    componentDidMount() {
        // this.keyboardDidShowListener = Keyboard.addListener('keyboardWillShow', this._keyboardDidShow);
        // this.keyboardDidHideListener = Keyboard.addListener('keyboardWillHide', this._keyboardDidHide);
        this.getStoragePhone()

        if(!this.isLogin){
            this.checkAgreement()
        }
        
        this.checkPrivacyModal()
    }

    //普通登录
    async login() {
        if (this.state.phoneNumber == '192.168.0.1' || this.state.phoneNumber == '192.168.1.1'){
            this.props.navigation.navigate('TCPSetting')
            return
        }
        if (!this.state.phoneNumber) {
            ToastManager.show('请输入11位手机号')
            return
        }
        if (!this.state.pass || this.state.pass.length<6) {
            ToastManager.show('请输入6至16位密码')
            return
        }
        if (!this.state.isChecked) {
            ToastManager.show('请先同意 用户协议 和 隐私政策')
            return
        }
        let params = this.getDeviceParams()
        SpinnerManager.show()
        try {
            let loginInfo = await postJson({
                url: NetUrls.login,
                useToken: false,
                params: {
                    'phone': this.state.phoneNumber,
                    'pwd': md5.hex_md5(this.state.pass),
                    ...params
                }
            });
            SpinnerManager.close()
            if (loginInfo.code == 0) {
                LocalTokenHandler.save(loginInfo.result, () => {
                    DeviceEventEmitter.emit(NotificationKeys.kLoginNotification)
                    AsyncStorage.setItem(LocalStorageKeys.kLastLoginAccount,this.state.phoneNumber)
                    AsyncStorage.setItem(LocalStorageKeys.kLoginAgreement,'true')
                    AppConfigs.pushRegisterId()
                })
                AppConfigs.saveAccountNumber(this.state.phoneNumber)
            } else {
                ToastManager.show(loginInfo.msg);
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误，请稍后重试');
        }
    }

    // 协议勾勾
    async checkAgreement(){
        let info = await AsyncStorage.getItem(LocalStorageKeys.kLoginAgreement)
        let lastAccount = await AsyncStorage.getItem(LocalStorageKeys.kLastLoginAccount)
        if(info){
            this.setState({isChecked: true})
        }
        if(lastAccount){
            this.setState({phoneNumber: lastAccount})
        }
    }

    // 读取登录过的号码
    async getStoragePhone(){
        let phones = await AsyncStorage.getItem(LocalStorageKeys.kStorageAccountNumber)
        try {
            this.setState({
                phones: JSON.parse(phones)
            })
        } catch (error) {
            
        }
    }

    // 是否弹出 信息保护指引 弹框
    async checkPrivacyModal(){
        let haveBeenShow = await AsyncStorage.getItem(LocalStorageKeys.kPrivacyProtocol)
        !haveBeenShow && this.setState({privacyModalVisible: true})
    }

    // 第三方登录
    async loginWithThird(type,data){
        SpinnerManager.show()
        const {navigate} = this.props.navigation
        let params = this.getDeviceParams()
        try {
            let res = await postJson({
                url: NetUrls.thirdLogin,
                useToken: false,
                params: {
                    type: type,
                    jsonString: JSON.stringify(data),
                    ...params
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                // 直接登录
                LocalTokenHandler.save(res.result, () => {
                    DeviceEventEmitter.emit(NotificationKeys.kLoginNotification)
                    AsyncStorage.setItem(LocalStorageKeys.kLoginAgreement,'true')
                    AppConfigs.pushRegisterId()
                })
            } else if(res.code == -2){
                // 去绑定
                navigate('BindPhone',{type:type, uid: data.uid, infoParmas:params})
            } else{
                ToastManager.show(res.msg);
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误，请稍后重试');
        }
    }

    //获取设备信息参数
    getDeviceParams(){
        let phoneModel = ''
        let phoneBrand = ''
        let phoneId = ''
        let deviceName = ''
        try {
            phoneModel = deviceInfo.phoneModel
            phoneBrand = deviceInfo.phoneBrand
            phoneId = deviceInfo.phoneId
            deviceName = deviceInfo.deviceName
        } catch (error) {
            
        }
        return {phoneModel, phoneBrand, phoneId, deviceName}
    }

    loginFromPlatform(type){
        // type: 2-微信   0-QQ聊天   3-Apple登录
        JKUMengShare.auth(type,(code,res,msg)=>{
            if(code == 200){
                this.loginWithThird(type,res)
            }else{
                ToastManager.show(msg)
            }
        })
    }

    // 动画向上位移
    animatedYtoTop(){
        Animated.timing(
            this.state.translateValue,
            {
                toValue: {
                    x:0,
                    y:-keybordHeight
                },
                duration: 300,
                delay:0,
            }
        ).start();
    }

    // 动画 还原
    animatedYtoOrigin(){
        Animated.timing(
            this.state.translateValue,
            {
                toValue: {
                    x:0,
                    y:0
                },
                duration: 300,
                delay:0,
            }
        ).start();
    }

    renderHeader() {
        return (
            <View style={styles.header}>
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Image style={styles.imageStyle} source={require('../../images/login/login_logo.png')}/>
                </View>
            </View>
        );
    }

    renderMainView() {
        return (
            <View style={{width:'100%', paddingHorizontal: 30, marginTop: 20}}>
                {this.renderAccountInput()}
                {this.renderAccountExpand()}
                {this.renderPassWordInput()}
                {this.getProtocol()}
                {this.getLoginButton()}
            </View>      
        )
    }

    // 账号输入框
    renderAccountInput(){
        const Colors = this.props.themeInfo.colors
        return(
            <View style={[styles.inputWrapper,{backgroundColor: Colors.themeBg}]}>
                <TextInput
                    style={[styles.input,{color: Colors.themeText}]}
                    placeholder="请输入11位手机号"
                    keyboardType={'numeric'}
                    value={this.state.phoneNumber}
                    maxLength={11}
                    clearButtonMode={'while-editing'}
                    placeholderTextColor={Colors.themeInactive}
                    onChangeText={(text) => {
                        this.setState({
                            phoneNumber: text
                        })
                        if(!this.state.phones || this.state.phones.length<=0){
                            return
                        }
                        let phoneMatching = []
                        for(let i=0; i<this.state.phones.length; i++){
                            let phone = this.state.phones[i]
                            if(phone.includes(text)){
                                phoneMatching.push(phone)
                            }
                        }
                        if(text && phoneMatching.length > 0){
                            this.setState({
                                showAccountExpand: true,
                                phoneMatching: phoneMatching
                            })
                        }else{
                            this.setState({
                                showAccountExpand: false,
                                phoneMatching: null
                            })
                        }
                    }}
                    onFocus={()=>{
                        this.animatedYtoTop()
                    }}
                    onBlur={()=>{
                        this.animatedYtoOrigin()
                        this.setState({
                            showAccountExpand: false
                        })
                    }}
                />
            </View>
        )
    }

    // 账号补全 列表
    renderAccountExpand(){
        if(!this.state.showAccountExpand){
            return null
        }
        if(!this.state.phoneMatching || this.state.phoneMatching.length<=0){
            return null
        }

        const Colors = this.props.themeInfo.colors

        let phones = this.state.phoneMatching.map((value, index)=>{
            return(
                <TouchableOpacity
                    activeOpacity={0.7}
                    key={'expand_'+index} 
                    style={styles.inputTouch} 
                    onPress={()=>{
                        this.setState({
                            phoneNumber: value,
                            showAccountExpand: false,
                            phoneMatching: null
                        })
                    }}
                >
                    <Text style={{fontSize: 15, color: Colors.themeText}}>{value}</Text>
                </TouchableOpacity>
            )
        })
        if(this.state.phoneMatching.length <= 2){
            return (
                <View style={[styles.expandWrapper,{backgroundColor: Colors.themeBg}]}>
                    {phones}
                </View>
            )
        }else{
            return(
                <View style={[styles.expandWrapper,{backgroundColor: Colors.themeBg,height: 150}]}>
                    <ScrollView keyboardShouldPersistTaps="handled">
                        {phones}
                    </ScrollView>
                </View>
            )
        }
    }

    // 密码输入框
    renderPassWordInput(){
        if(this.state.showAccountExpand){
            return null
        }
        const Colors = this.props.themeInfo.colors
        return(
            <View style={[styles.inputWrapper, {marginTop: 20,backgroundColor: Colors.themeBg}]}>
                <TextInput
                    style={[styles.input,{color: Colors.themeText}]}
                    defaultValue={this.state.pass}
                    maxLength={16}
                    placeholder="请输入6至16位密码"
                    clearButtonMode={'while-editing'}
                    secureTextEntry={true}
                    placeholderTextColor={Colors.themeInactive}
                    onChangeText={(text) => {
                        this.state.pass = text
                    }}
                    onFocus={()=>{
                        this.animatedYtoTop()
                    }}
                    onBlur={()=>{
                        this.animatedYtoOrigin()
                    }}
                />
            </View>
        )
    }

    // 协议
    getProtocol(){
        if(this.state.showAccountExpand){
            return null
        }
        const Colors = this.props.themeInfo.colors
        const {navigate} = this.props.navigation

        let checkIcon = this.state.isChecked ? require('../../images/check_blue.png')
        : require('../../images/uncheck_blue.png')

        return(
            <View style={{flexDirection: 'row',width:'100%',marginTop:10,alignItems:'center'}}>
                <TouchableOpacity style={{padding: 10,marginLeft:-10}} onPress={()=>{
                    this.setState({isChecked: !this.state.isChecked})
                }}>
                    <Image style={[{width:14,height:14}]} source={checkIcon}/>
                </TouchableOpacity>
                <Text style={{fontSize:12,color:Colors.themeText}}>我已阅读并同意</Text>
                <TouchableOpacity style={{paddingVertical: 10}} onPress={()=>{
                    navigate('Protocol',{title:'用户协议',type:1})
                }}>
                    <Text style={{fontSize:12,color:Colors.newTheme}}>《用户协议》</Text>
                </TouchableOpacity>
                <Text style={{fontSize:12,color:Colors.themeText}}>和</Text>
                <TouchableOpacity style={{paddingVertical: 10}} onPress={()=>{
                    navigate('Protocol',{title:'隐私政策',type:2})
                }}>
                    <Text style={{fontSize:12,color:Colors.newTheme}}>《隐私政策》</Text>
                </TouchableOpacity>
            </View>
        )
    }

    //登录按钮
    getLoginButton(){
        if(this.state.showAccountExpand){
            return null
        }
        return(
            <TouchableOpacity style={styles.loginBtn} onPress={() => {
                dismissKeyboard()
                this.login()
            }}>
                <Text style={{fontSize: 16, color: Colors.white}}>登录</Text>
            </TouchableOpacity>
        )
    }

    // 验证码登录 忘记密码
    getSubChooiceView(){
        if(this.state.showAccountExpand){
            return null
        }
        const {navigate} = this.props.navigation

        return(
            <View style={styles.subBtnWrapper}>
                <TouchableOpacity onPress={() => {
                    if (!this.state.isChecked) {
                        ToastManager.show('请先同意 用户协议 和 隐私政策')
                        return
                    }
                    navigate('LoginByMsg')
                }}>
                    <Text style={{color: Colors.newTheme}}>验证码登录/注册</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{alignItems: 'center'}} onPress={() => {
                    navigate('ForgetPsw',{title: '忘记密码'});
                }}>
                    <Text style={{color: Colors.newTheme}}>忘记密码?</Text>
                </TouchableOpacity>
            </View>
        )
    }

    getAppleLoginBtn(){
        if(Platform.OS == 'android'){
            return null
        }
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
        const systemVersion = deviceInfo.systemVersion
        let systemAvalible = parseInt(systemVersion) >= 13

        if(!this.state.isChecked || !systemAvalible){
            return (
                <TouchableOpacity style={{marginLeft: 60}} activeOpacity={0.7} onPress={()=>{
                    if(!this.state.isChecked){
                        ToastManager.show('请先同意 用户协议 和 隐私政策')
                    }else{
                        Alert.alert('提示','您的iOS系统版本低于13.0,请更新到最新版本后再试。')
                    }
                }}>
                    <Image style={styles.loginIcon} source={Images.appleLog}/>
                </TouchableOpacity>
            )
        }

        return(
            <TouchableOpacity style={{marginLeft: 60}} activeOpacity={0.7}>
                <NativeView
                    onClick={(info)=>{
                        if(info.nativeEvent.success){
                            // access: 1 当用户使用 apple 登录时，不需要绑定手机号就能直接登入
                            let appleData = {uid: info.nativeEvent.success, access: 1}
                            this.loginWithThird(3, appleData)
                        }else if(info.nativeEvent.error){
                            ToastManager.show(info.nativeEvent.error)
                        }
                    }
                }>
                    <Image style={styles.loginIcon} source={Images.appleLog}/>
                </NativeView>
            </TouchableOpacity>
        )
    }

    getThirdLoginView(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={styles.bottomLogin}>
                <View style={styles.bottomTitle}>
                    <View style={[styles.bottomLine,{backgroundColor: Colors.split}]}/>
                    <Text style={styles.bottomtText}>第三方平台登录</Text>
                    <View style={[styles.bottomLine,{backgroundColor: Colors.split}]}/>
                </View>
                <View style={styles.bottomTouchWrapper}>
                    <TouchableOpacity style={{marginRight: 60}} activeOpacity={0.7} onPress={()=>{
                        if (!this.state.isChecked) {
                            ToastManager.show('请先同意 用户协议 和 隐私政策')
                            return
                        }
                        this.loginFromPlatform(2)
                    }}>
                        <Image style={styles.loginIcon} source={require('../../images/login/wechat.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} onPress={()=>{
                        if (!this.state.isChecked) {
                            ToastManager.show('请先同意 用户协议 和 隐私政策')
                            return
                        }
                        this.loginFromPlatform(0)
                    }}>
                        <Image style={styles.loginIcon} source={require('../../images/login/QQ.png')}/>
                    </TouchableOpacity>
                    {this.getAppleLoginBtn()}
                </View>
            </View>
        )
    }

    // 显示 隐私政策 弹框
    renderPersonPrivacyModal(){
        if(Platform.OS == 'ios'){
            return null
        }
        if(!this.state.privacyModalVisible){
            return null
        }
        return (
            <PersonPrivacyModal
                navigation = {this.props.navigation}
                callBack={()=>{
                    this.setState({privacyModalVisible: false})
                    deviceInfo.init()
                    JKUMengShare.init()
                    AsyncStorage.setItem(LocalStorageKeys.kPrivacyProtocol,'true')
                    AppConfigs.checkVersion()
                }}
            />
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors
        
        return (
            <View style={styles.container}>
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={[styles.container,{backgroundColor: Colors.themeBaseBg}]} 
                    onPress={()=>{dismissKeyboard()}}
                >
                    <Animated.View style={{flex: 1,marginTop:this.state.translateValue.y}}>
                        {this.renderHeader()}
                        {this.renderMainView()}
                        {this.getSubChooiceView()}
                        {this.getThirdLoginView()}
                    </Animated.View>
                </TouchableOpacity>
                {this.renderPersonPrivacyModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    imageStyle: {
        height: 150,
        width: 150,
        resizeMode: 'contain'
    },
    header: {
        width: '100%',
        marginTop: StatusBarHeight + NavigationBarHeight,
    },
    loginBtn: {
        height: 40,
        backgroundColor: Colors.newTheme,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        marginTop: 15
    },
    input: {
        flex: 1,
        paddingLeft: 20,
        height:40,
        fontSize: 16
    },
    inputWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        borderRadius: 10,
        paddingVertical:4
    },
    subBtnWrapper:{
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20,
        paddingHorizontal: 30
    },
    bottomLogin:{
        position: 'absolute',
        bottom: BottomSafeMargin + 30,
        width: '100%',
        justifyContent:'center',
        alignItems:'center'
    },
    bottomTitle:{
        flexDirection: 'row',
        justifyContent:'center',
        alignItems: 'center'
    },
    bottomLine:{
       width: '25%',
       height: 1
    },
    bottomtText:{
        color: Colors.themeTextLightGray,
        fontSize: 13,
        marginHorizontal: '5%'
    },
    loginIcon:{
        width: 38,
        height: 38,
        resizeMode: 'contain'
    },
    bottomTouchWrapper:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30
    },
    expandWrapper:{
        marginTop: 10,
        width:'100%',
        borderRadius: 4
    },
    inputTouch: {
        height:50,
        width:'100%',
        justifyContent:'center',
        paddingHorizontal: 20
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Login) 

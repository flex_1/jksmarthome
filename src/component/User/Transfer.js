/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    TextInput,
} from 'react-native';
import {DeviceEventEmitter} from "react-native";
import {Colors, NotificationKeys, NetUrls, NetParams} from '../../common/Constants';
import {postJson} from '../../util/ApiRequest';
import {StatusBarHeight} from '../../util/ScreenUtil';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import { connect } from 'react-redux';
import { updateUserInfo } from '../../redux/actions/UserInfoAction';

const dismissKeyboard = require('dismissKeyboard');
const codeTime = 120

class Transfer extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'过户'}/>,
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props)
        this.state = {
            phone: "",
            code: '',
            timerCount: codeTime,
            timerTitle: '获取验证码',
        };
    }

    componentWillUnmount() {
        this.interval && clearInterval(this.interval);
    }

    async homeOwner() {
        dismissKeyboard()
        if (!this.state.phone) {
            ToastManager.show('请输入接收方账号')
            return
        }
        if (!this.state.code) {
            ToastManager.show('请输入验证码')
            return
        }
        SpinnerManager.show()
        try {
            const {popToTop,navigate,replace} = this.props.navigation;
            let res = await postJson({
                url: NetUrls.homeOwner,
                timeout: NetParams.networkTransferOwner,
                params: {
                    "phone": this.state.phone,
                    "code": this.state.code
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                ToastManager.show('过户成功')

                this.props.updateUserInfo()

                // 切换 楼层 场景 等信息
                DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
                DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
                DeviceEventEmitter.emit(NotificationKeys.kSmartListChangeNotification) 
                DeviceEventEmitter.emit(NotificationKeys.kRoomFloorChangeNotification)

                //刷新首页
                DeviceEventEmitter.emit(NotificationKeys.kHouseInfoNotification)

                //重新连接 websocket
                DeviceEventEmitter.emit(NotificationKeys.kWebsocketReconnectNotification)

                popToTop()
                navigate('Home')
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(e));
        }
    }

    async getMsg() {
        dismissKeyboard()
        if (!this.state.phone) {
            ToastManager.show('请输入接收方账号')
            return
        }
        SpinnerManager.show()
        try {
            const {pop} = this.props.navigation;
            let res = await postJson({
                url: NetUrls.getMsg,
                useToken: false,
                params: {
                    'mobile': this.state.phone,
                    'type': 3, //1.找回密码 2-邀请成员 3-过户
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                this.onTimes();
                ToastManager.show("获取验证码成功");
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(e));
        }
    }

    

    inputAccount() {
        const Colors = this.props.themeInfo.colors;
        return (
            <View style={{flexDirection: 'row', alignItems: 'center', paddingHorizontal: 20,marginTop:20}}>
                <TextInput
                    style={[styles.input,{color: Colors.themeText}]}
                    placeholder="请输入接收方账号"
                    placeholderTextColor={Colors.themeInactive}
                    clearButtonMode={'while-editing'}
                    maxLength={11}
                    onChangeText={(text) => {
                        this.state.phone = text
                    }}
                />
            </View>
        )
    }

    inputCode() {
        const Colors = this.props.themeInfo.colors;
        return (
            <View style={styles.inputStyle}>
                <TextInput
                    style={[styles.input,{color: Colors.themeText}]}
                    placeholder="请输入验证码"
                    placeholderTextColor={Colors.themeInactive}
                    clearButtonMode={'while-editing'}
                    maxLength={6}
                    keyboardType={'numeric'}
                    onChangeText={(text) => {
                        this.state.code = text
                    }}
                />
                 <TouchableOpacity style={{marginLeft: 20,}} onPress={() => {
                    if (this.state.timerCount == codeTime) {
                        this.getMsg()
                    }
                }}>
                    <Text style={{color: Colors.themeButton, fontSize: 16}}>
                        {this.state.timerCount == codeTime ? this.state.timerTitle : this.state.timerCount}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }

    getSureButton() {
        return (
            <TouchableOpacity activeOpacity={0.7} style={styles.sureBtn} onPress={() => {
                this.homeOwner()
            }}>
                <Text style={{fontSize: 15, color: Colors.white}}>确认过户</Text>
            </TouchableOpacity>
        )
    }

    
    onTimes() {
        this.interval = setInterval(() => {
            var timer = this.state.timerCount - 1;
            if (timer === 0) {
                this.interval && clearInterval(this.interval);
                this.setState({timerCount: codeTime, timerTitle: '重新获取'})
            } else {
                this.setState({timerCount: timer,})
            }
        }, 1000)
    }

    render() {
        const Colors = this.props.themeInfo.colors;

        return(
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.inputAccount()}
                <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                {this.inputCode()}
                <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                {this.getSureButton()}
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    input: {
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 16,
        flex: 1,
    },
    customItemTouch: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
        marginTop: 15
    },
    sureBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        marginHorizontal: 20,
        marginTop: 50,
        backgroundColor: Colors.tabActiveColor,
        borderRadius: 4,
    },
    inputStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop:20,
        paddingTop: 5,
        paddingHorizontal: 20
    },
    line:{
        
        width:'90%',
        marginHorizontal: '5%',
        height: 1
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    }),
    (dispatch) => ({
        updateUserInfo: ()=> dispatch(updateUserInfo())
    })
)(Transfer)

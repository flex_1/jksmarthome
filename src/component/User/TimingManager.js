/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    SectionList,
    ImageBackground,
    Alert
} from 'react-native';
import { NetUrls,Colors } from '../../common/Constants';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import ToastManager from "../../common/CustomComponent/ToastManager";
import { postJson } from "../../util/ApiRequest";
import { connect } from 'react-redux';
import SwitchButton from '../../common/CustomComponent/SwitchButton';
import {ListNoContent,FooterEnd,ListLoading } from "../../common/CustomComponent/ListLoading";

class TimingManager extends Component {
    
    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={'定时管理'}/>,
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation

        this.userData = getParam('userData') || {}
        
        this.state={
            timingList: null,
            loading: false
        }
        
    }

    componentDidMount() {
        this.requestTimingData()
    }

    componentWillUnmount() {
        
    }

    async requestTimingData() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.getTimerTimeAxis,
                params: {

                }
            });
            SpinnerManager.close()
            this.setState({loading: false})
            if (data.code == 0) {
                const timeListData = data.result
                let timingList = []
                let index = 0
                for (const key in timeListData) {
                    let item = {data: [], index: index, title: ''}
                    if(timeListData[key]){
                        item.data = timeListData[key]
                        if(key == 'one'){
                            item.title = '今天'
                        }else if(key == 'two'){
                            item.title = '明天'
                        }else if(key == 'three'){
                            item.title = '后天'
                        }else{
                            item.title = '以后'
                        }
                        timingList.push(item)
                        index++
                    }
                }
                this.setState({
                    timingList: timingList
                })
            } else {
                ToastManager.show(data.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            this.setState({loading: false})
            ToastManager.show('网络错误');
        }
    }

    async requestTimingEnable(id, status, callBack) {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.setTimerEnable,
                params: {
                    id: id,
                    status: status
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                callBack && callBack()
            } else {
                ToastManager.show(data.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误');
        }
    }

    changeTimerStatus(rowData, index, section){
        let target = rowData.status == 1 ? 0:1
        this.requestTimingEnable(rowData.timerId, target, ()=>{
            let newSection = JSON.parse(JSON.stringify(section))
            newSection.data[index].status = target
            this.state.timingList[newSection.index] = newSection
            this.setState({
                timingList: this.state.timingList
            })
        })
    }

    renderSwicthButton(rowData, index, section){
        return(
            <SwitchButton
                style={{width: 50, height: 30,marginRight: '15%'}}
                bgColorOn={'#6AC064'}
                value = {rowData.status == 1}
                onPress = {()=>{
                    if(rowData.isCyclic == 1){
                        let tips = '该定时为周期性定时，打开后后面的周期都会执行，确认打开吗?'
                        let btnText = '继续打开'
                        if(rowData.status == 1){
                            tips = '该定时为周期性定时，关闭后后面的周期都不会执行，确认关闭吗?'
                            btnText = '继续关闭'
                        }

                        Alert.alert(
                            '提示',
                            tips,
                            [   
                                { text: '取消', onPress: () => { }, style: 'cancel' },
                                { text: btnText, onPress: () => { 
                                    this.changeTimerStatus(rowData, index, section)
                                }}
                            ]
                        )
                    }else{
                        this.changeTimerStatus(rowData, index, section)
                    }
                }}
            />
        )
    }

    renderSectionHeader(section){
        const Colors = this.props.themeInfo.colors
        return(
            <View style={[styles.sectionWrapper,{backgroundColor: Colors.themeBg}]}>
				<Text style={[styles.titleText,{color: Colors.themeText}]}>{section.title}</Text>
			</View>
        )
    }

	renderRowItem(rowData, index, section){
        const Colors = this.props.themeInfo.colors
        let dotIcon = rowData.status == 1 ? require('../../images/user_manage/dot_on.png') : require('../../images/user_manage/dot_off.png')
        let bgImg = rowData.status == 1 ? require('../../images/user_manage/bg_on.png') : require('../../images/user_manage/bg_off.png')
        let textColor = rowData.status == 1 ? Colors.newBtnBlueBg : '#7C7C7D'

        return(
            <View style={[styles.itemWrapper,{backgroundColor: Colors.themeBg}]}>
                <View style={styles.dotWrapper}>
                    <View style={[styles.verticalLine, index== 0 ? {backgroundColor: Colors.fullyTransparent} : {}]}/>
                    <Image style={styles.dotIcon} source={dotIcon}/>
                    <View style={[styles.verticalLine, index == (section.data?.length - 1) ? {backgroundColor: Colors.fullyTransparent} : {}]}/>
                </View>
				<Text style={[styles.timeText,{color: textColor}]}>{rowData.time}</Text>
                <View style={styles.bgWrapper}>
                    <ImageBackground style={styles.imgBg} imageStyle={{resizeMode: 'contain'}} source={bgImg}>
                        <View style={styles.nameWrapper}>
                            <Text style={[styles.titleText,{color: textColor}]}>{rowData.floor || ''}{rowData.room || '整屋'}</Text>
                            <Text style={styles.subName}>{rowData.name}</Text>
                        </View>
                        {this.renderSwicthButton(rowData, index, section)}
                    </ImageBackground>
                </View>
			</View>
        )
    }

    renderList(){
        const Colors = this.props.themeInfo.colors

        let data = this.state.timingList
        if(!data){
			return <ListLoading/>
		}
        if(data.length <= 0){
            return (
                <View style={styles.noContent}>
                    <Text style={{fontSize: 15, color: Colors.themeTextLight}}>暂无定时</Text>
                </View>
            )
		}

        return(
			<SectionList
				style={{flex: 1}}
				contentContainerStyle={{ paddingBottom: 50}}
				stickySectionHeadersEnabled={true}
  				renderItem={({ item, index, section }) => this.renderRowItem(item, index, section)}
  				renderSectionHeader={({ section }) => this.renderSectionHeader(section)}
				sections={data}
				scrollIndicatorInsets={{right: 1}}
				initialNumToRender={5}
				keyExtractor={(item, index) => 'timing_list'+index}
                onEndReachedThreshold={0.1}
                ListFooterComponent={FooterEnd}
			/>
		)
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBg}]} >
                {this.renderList()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    sectionWrapper:{
        height: 50,
        paddingHorizontal: 16,
        justifyContent: 'center'
    },
    itemWrapper:{
        height: 100,
        flexDirection: 'row',
        paddingHorizontal: 16,
        alignItems: 'center'
    },
    verticalLine:{
        width: 1.5, 
        flex: 1,
        backgroundColor: '#C8C9CC'
    },
    dotWrapper:{
        width: 16, 
        height: '100%',
        alignItems: 'center'
    },
    dotIcon:{
        width:16,
        height:16,
        marginVertical: 4
    },
    timeText:{
        marginLeft: 16,
        fontSize:13,
        fontWeight:'400',
        width: 40
    },
    bgWrapper:{
        flex: 1,
        height: 120,
        justifyContent:'center'
    },
    imgBg:{
        width:'100%',
        height: 96,
        flexDirection: 'row',
        alignItems: 'center'
    },
    nameWrapper:{
        flex: 1,
        marginLeft: '15%'
    },
    titleText:{
        fontSize:16,
        fontWeight:'500'
    },
    subName:{
        fontSize:12,
        fontWeight:'400',
        marginTop: 6,
        color: '#7C7C7D'
    },
    noContent:{
        alignItems: 'center', 
        justifyContent: 'center', 
        flex: 1
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(TimingManager)

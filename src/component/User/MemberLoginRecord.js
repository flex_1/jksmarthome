import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity,
    DeviceEventEmitter,
    FlatList,
    SectionList,
    Alert,
    RefreshControl
} from 'react-native';
import { connect } from 'react-redux';
import { Colors, NetUrls } from '../../common/Constants';
import { postJson } from "../../util/ApiRequest";
import SpinnerManager from '../../common/CustomComponent/SpinnerManager';
import ToastManager from "../../common/CustomComponent/ToastManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import DateUtil from '../../util/DateUtil';
import {ListNoContent,ListLoading,ListError } from "../../common/CustomComponent/ListLoading";
import {ImagesLight, ImagesDark} from '../../common/Themes';

class MemberLoginRecord extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={'登录设备管理'}/>,
        headerBackground: <HeaderBackground/>
    })

	constructor(props) {
		super(props);
        const {getParam} = this.props.navigation

        this.id = getParam('id')
        this.state={
            sectionData: null,
            loginData: null,
            isRefreshing: false
        }
    }

    componentDidMount(){
        this.requestDeviceInfo()
    }
	
    async requestDeviceInfo() {
        if(!this.state.isRefreshing){
            SpinnerManager.show()
        }
		try {
			let data = await postJson({
				url: NetUrls.memberLoginRecord,
				params: {
					accountId: this.id
				}
            });
            this.setState({isRefreshing: false})
            SpinnerManager.close()
			if (data.code == 0) {
                this.setState({
                    sectionData: data.result || [],
                    loginData: data.result.data || []
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            this.setState({isRefreshing: false})
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    //停用 拉黑
    async requestOperation(type,status,accountId,phoneId) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.memberLoginRecordOperation,
				params: {
                    type: type,   // 1-停启用账号 2-停启用设备 3-踢出
                    status: status,  // 1-启用 0-停用
                    accountId: accountId,
                    phoneId: phoneId
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
                if(data.result){
                    this.requestDeviceInfo()
                }
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    //delete record
    async deleteRecord(id){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.deleteMemberLoginRecord,
				params: {
                    id: id
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
                this.requestDeviceInfo()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    onRefresh(){
        this.setState({isRefreshing: true},()=>{
            this.requestDeviceInfo()
        })
    }

    _renderStatusView(rowData){
        const Colors = this.props.themeInfo.colors;

        if(!rowData.deviceStatus){
            return (
                <Image style={[styles.statusIcon,{tintColor: Colors.themeTextLight}]} source={require('../../images/user_manage/blackMan.png')} />
            )
        }

        if(rowData.isOnline){
            return (
                <Text style={[styles.statusLabel,{color: Colors.lightGreen}]}>在线</Text>
            )
        }else{
            return (
                <Text style={[styles.statusLabel,{color: Colors.themeTextLight}]}>离线</Text>
            )
        }
    }

    _renderStartUseBtn(section){
        const Colors = this.props.themeInfo.colors;
        let btnText = section.status ? '停用' : '启用'
        let btnBg = section.status ? Colors.red : Colors.themeButton

        return(
            <TouchableOpacity activeOpacity={0.7} style={[styles.startUseBtn]} onPress={()=>{
                let target = section.status ? 0 : 1
                let warnningText = ''
                let sureBtnText = ''
                if(target){
                    warnningText = '确定启用该账号?'
                    sureBtnText = '启用'
                }else{
                    warnningText = '停用该账号会导致登录该账号的用户被强制退出，且不能再登录该账号。确定停用该账号?'
                    sureBtnText = '停用'
                }

                Alert.alert(
                    '提示',
                    warnningText,
                    [
                        {text: '取消', onPress: () => {}, style: 'cancel'},
                        {text: sureBtnText, onPress: () => {
                            this.requestOperation(1, target, this.id, null)
                        }},
                    ]
                )
            }}>
                <View style={[styles.useBtn,{backgroundColor: btnBg}]}>
                    <Text style={{fontSize: 13,color: Colors.white}}>{btnText}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    _renderDeleteBtn(rowData){
        const Colors = this.props.themeInfo.colors;

        if(rowData.isOnline){
            return(
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.kickTouch,{borderColor: Colors.red}]}
                    onPress={()=>{
                        Alert.alert(
                            '提示',
                            '确定将该设备上的该账户踢出下线？',
                            [
                                {text: '取消', onPress: () => {}, style: 'cancel'},
                                {text: '踢出', onPress: () => {
                                    this.requestOperation(3, 0, this.id, rowData.phoneId)
                                }},
                            ]
                        )
                    }}
                >
                    <Image style={[styles.blackmanImg,{tintColor: Colors.red}]} source={require('../../images/user_manage/kickOut.png')}/>
                    <Text style={{fontSize: 13, color: Colors.red}}>踢出</Text>
                </TouchableOpacity>
            )
        }else{
            return(
                <TouchableOpacity style={styles.deleteTouch} onPress={()=>{
                    this.deleteRecord(rowData.id)
                }}>
                    <Text style={styles.deleteText}>删除</Text>
                </TouchableOpacity>
            )
        }
    }

    _renderDeviceController(rowData, index){
        if(this.state.sectionData && !this.state.sectionData.status){
            return null
        }
        
        const Colors = this.props.themeInfo.colors;
        const statusText = rowData.deviceStatus ? '加入黑名单' : '移出黑名单'
        
        return(
            <View style={{paddingTop: 15,flexDirection: 'row'}}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.blackBtnTouch,{borderColor: Colors.themeText}]}
                    onPress={()=>{
                        let target = rowData.deviceStatus ? 0 : 1
                        let warnningText = ''
                        let sureBtnText = ''
                        if(target){
                            warnningText = '确定将该设备移出黑名单?'
                            sureBtnText = '移出黑名单'
                        }else{
                            warnningText = '拉黑该设备后，该设备将无法登录该账号。 确定拉黑该设备?'
                            sureBtnText = '加入黑名单'
                        }

                        Alert.alert(
                            '提示',
                            warnningText,
                            [
                                {text: '取消', onPress: () => {}, style: 'cancel'},
                                {text: sureBtnText, onPress: () => {
                                    this.requestOperation(2, target, this.id, rowData.phoneId)
                                }},
                            ]
                        )
                    }}
                >
                    <Image style={[styles.blackmanImg,{tintColor: Colors.themeText}]} source={require('../../images/user_manage/blackMan.png')}/>
                    <Text style={{fontSize: 13, color: Colors.themeText}}>{statusText}</Text>
                </TouchableOpacity>
                {rowData.isOnline ? <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.blackBtnTouch,{borderColor: Colors.red}]}
                    onPress={()=>{
                        Alert.alert(
                            '提示',
                            '确定将该设备上的该账户踢出下线？',
                            [
                                {text: '取消', onPress: () => {}, style: 'cancel'},
                                {text: '踢出', onPress: () => {
                                    this.requestOperation(3, 0, this.id, rowData.phoneId)
                                }},
                            ]
                        )
                    }}
                >
                    <Image style={[styles.blackmanImg,{tintColor: Colors.red}]} source={require('../../images/user_manage/kickOut.png')}/>
                    <Text style={{fontSize: 13, color: Colors.red}}>踢出</Text>
                </TouchableOpacity> : null}
                <View style={{flex: 1}}/>
                {this._renderDeleteBtn(rowData)}
            </View>
        )
    }

    renderSectionHeader(){
        let section = this.state.sectionData
        if(!section){
            return null
        }

        const Colors = this.props.themeInfo.colors;
        let onlineAccount = section.onlineAccount || 0
        let onlineNumber = onlineAccount + '/' + section.data.length

        let arrow = <Image style={styles.arrow} source={require('../../images/user_manage/right.png')}/>
        if(section.expand){
            arrow = <Image style={styles.arrow} source={require('../../images/user_manage/downArrow.png')}/>
        }

        let userIcon = require('../../images/user_manage/memberIcon.png')
        if(section.photo){
            userIcon = {uri: section.photo}
        }

        const phoneColor = section.status ? Colors.themeText : Colors.themeTextLight
        const onlineColor = section.status ? Colors.themeButton : Colors.themeTextLight

        return(
            <View style={[styles.sectionWrapper,{backgroundColor: Colors.themeBg,borderColor: Colors.themeBaseBg}]}>
                <Image style={styles.sectionIcon} source={userIcon}/>
                <View style={{marginLeft: 15}}>
                    <View style={{flexDirection: 'row',alignItems:'center'}}>
                        <Text style={{fontSize: 15, color: phoneColor}}>{section.phone}</Text>
                        <Text style={[styles.onlineNumber,{color: onlineColor}]}>{onlineNumber}</Text>
                    </View>
                    {section.memberRemark ? <Text style={[styles.nickName,{color: Colors.themeTextLight}]}>{section.memberRemark}</Text> : null}
                </View>
                <View style={{flex: 1}}/>
				{this._renderStartUseBtn(section)}
			</View>
        )
    }

    renderRowItem(rowData, index) {
        
        const Colors = this.props.themeInfo.colors;
        const date = DateUtil(rowData.loginTimespan)
        const modifyTime = DateUtil(rowData.modifyTime)
        const deviceName = rowData.deviceName
        const device = rowData.phoneBrand + '  ' + rowData.phoneModel
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}

        return(
            <View style={[styles.itemWrapper,{backgroundColor: Colors.themeBg,marginTop: index ? 0 : 10}]}>
                <View style={styles.item}>
                    <Text style={[styles.titleName,{color: Colors.themeTextLight}]}>最近登录时间：</Text>
                    <Text style={[styles.timeText,{color: Colors.themeText}]}>{date}</Text>
                </View>
                <View style={styles.item}>
                    <Text style={[styles.titleName,{color: Colors.themeTextLight}]}>最后操作时间：</Text>
                    <Text style={[styles.timeText,{color: Colors.themeText}]}>{modifyTime}</Text>
                </View>
                <View style={styles.item}>
                    <Image style={[styles.icon,tintColor]} source={require('../../images/user_manage/account.png')}/>
                    <Text style={[styles.phoneText,{color: Colors.themeTextLight}]}>{deviceName}</Text>
                </View>
                <View style={styles.item}>
                    <Image style={[styles.icon,tintColor]} source={require('../../images/user_manage/phoneBrand.png')}/>
                    <Text style={[styles.phoneText,{color: Colors.themeTextLight}]}>{device}</Text>
                </View>
                {/* {this._renderDeviceController(rowData, index)} */}
                {this._renderStatusView(rowData)}
                <View style={styles.rightBottomWrapper}>
                    {this._renderDeleteBtn(rowData)}
                </View>
            </View>
        )
    }

    renderNoContent(){
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
        return(
            <ListNoContent
				img={Images.noMember} 
				text={'暂无登录设备'}
			/>
        )
    }

    _extraUniqueKey(item, index) {
		return "device_index_" + index;
	}

    renderList(){
        let loginData = this.state.loginData

        if(!loginData){
            return null
        }

        return(
            <FlatList
				style={{flex: 1}}
				contentContainerStyle={{ paddingBottom: 50}}
				data={this.state.loginData}
  				renderItem={({ item, index }) => this.renderRowItem(item, index)}
				scrollIndicatorInsets={{right: 1}}
                initialNumToRender={5}
                ListEmptyComponent={this.renderNoContent()}
                keyExtractor={(item, index) => 'member_record_log'+index}
                refreshControl={
                    <RefreshControl
                        colors={[Colors.themeBGInactive]}
                        tintColor={Colors.themeBGInactive}
                        refreshing={this.state.isRefreshing}
                        onRefresh={this.onRefresh.bind(this)}
                    />
                }
				// onEndReached={()=>{this.requestSmartLog(this.state.currentPage+1)}}
                // onEndReachedThreshold={0.1}
                // ListFooterComponent={Footer}
			/> 
        )
    }
	
	render() {
        const Colors = this.props.themeInfo.colors
		return (
			<View style={{backgroundColor: Colors.themeBaseBg, flex: 1}}>
                {this.renderSectionHeader()}
				{this.renderList()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	itemWrapper:{
        marginBottom: 10,
        paddingVertical: 10,
        paddingHorizontal: 16,
        marginHorizontal: 16,
        borderRadius: 5,
    },
    item:{
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    icon:{
        width: 12,
        height: 12,
        resizeMode: 'contain'
    },
    sectionIcon:{
        width: 40,
        height: 40,
        resizeMode: 'contain',
        borderRadius: 20
    },
    titleName:{
        fontSize: 13
    },
    timeText:{
        fontSize: 14,
        
    },
    phoneText:{
        fontSize: 13,
        marginLeft: 10
    },
    statusWrapper:{
        width: 40,
        height: 20,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    sectionWrapper:{
        paddingLeft: 16,
        paddingVertical: 10,
        borderBottomWidth: 1,
        flexDirection: 'row', 
        alignItems:'center'
    },
    nickName:{
        marginTop: 8, 
        fontSize: 13
    },
    onlineNumber:{
        fontSize: 12,
        marginLeft: 20
    },
    arrow:{
        width: 10,
        height: 10,
        resizeMode: 'contain',
        marginLeft: 5,
        marginRight: 10
    },
    startUseBtn:{
        height: 40,
        width: 80,
        paddingRight: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    useBtn:{
        width: 60,
        height: 30,
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    blackBtnTouch:{
        justifyContent: 'center',
        alignItems: 'center',
        height: 32,
        width: 110,
        borderWidth: 1,
        borderRadius: 16,
        flexDirection: 'row',
        marginRight: 20
    },
    blackmanImg:{
        width: 14,
        height: 14,
        resizeMode: 'contain',
        marginRight: 5
    },
    statusIcon:{
        width: 20,
        height: 20,
        resizeMode: 'contain',
        position: 'absolute',
        right: 16,
        top: 22
    },
    statusLabel:{
        fontSize: 13,
        position: 'absolute',
        right: 16,
        top: 22
    },
    deleteTouch:{
        height: 32,
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'flex-end'
    },
    deleteText:{
        fontSize: 14,
        color: Colors.themBgRed
    },
    rightBottomWrapper:{
        position: 'absolute',
        right: 16,
        bottom: 10
    },
    kickTouch:{
        justifyContent: 'center',
        alignItems: 'center',
        height: 28,
        width: 80,
        borderWidth: 1,
        borderRadius: 14,
        flexDirection: 'row'
    }
})


export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(MemberLoginRecord)
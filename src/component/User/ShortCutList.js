import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    StatusBar,
    FlatList,
    NativeModules,
    NativeEventEmitter,
    Modal
} from 'react-native';
import ActionSheet from 'react-native-actionsheet';
import { connect } from 'react-redux';
import { NotificationKeys, NetUrls, LocalStorageKeys, Colors } from '../../common/Constants';
import ToastManager from "../../common/CustomComponent/ToastManager";
import { postJson,postPicture } from "../../util/ApiRequest";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import {ImagesLight, ImagesDark} from '../../common/Themes';
import { ListNoContent, FooterEnd, FooterLoading, ListLoading } from "../../common/CustomComponent/ListLoading";

const JKRNUtils = NativeModules.JKRNUtils;
const ShortCutManager = Platform.select({
    ios: new NativeEventEmitter(JKRNUtils),
    android: null
})

class ShortCutList extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'Siri快捷指令'}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text: '筛选',onPress:()=>{
                    navigation.state.params.filterModal()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
		super(props);
        const { getParam,setParams } = props.navigation;
        this.state={
            indicators: [{name: '场景', type: 1, filterType: 3}, {name: '设备', type: 2, filterType:7}],
            currentType: 1,
            listData: null,
            loading: true,
            refreshing: false,

            modalVisible: false,
            filterType: 3,    // 3:场景 7:设备
            filterData: null,
            filterIds: null,
            pickerType: 1,  // 1-筛选 2-设备,
            roomselectedData: []
        }

        setParams({
            filterModal: this.filterModal.bind(this)
        })
	}

	componentDidMount() {
		this.shortCutList()
        this.getFilterList()

        //添加快捷指令成功回调
        if(Platform.OS == 'ios'){
            this.shortcutsNoti = ShortCutManager.addListener(NotificationKeys.kRNAddSiriSuccess,(data)=>{
                if(data == 1){
                    ToastManager.show('添加快捷成功')
                }else if(data == 2){
                    ToastManager.show('快捷已更新')
                }else if(data == 3){
                    ToastManager.show('删除快捷成功')
                }
            })
        }
	}

    componentWillUnmount(){
        if(Platform.OS == 'ios'){
            this.shortcutsNoti.remove()
        }
    }

    filterModal(){
        if(!this.state.filterData){
            ToastManager.show('数据加载中，请稍后')
            return
        }
        this.setState({
            pickerData: this.state.filterData,
            modalVisible: true,
            pickerTitle:'筛选'
        })
    }

    // 处理房屋筛选
    handleWithRoomFilter(selectData, selectIndex){
        console.log(selectData+'  '+selectIndex);
        const floorIndex = selectIndex[0]
        const roomIndex = selectIndex[1]
        const selectIds = this.state.filterIds[floorIndex][roomIndex]
        const floorId = selectIds[0]
        const roomId = selectIds[1]
        this.setState({
            modalVisible: false,
            roomselectedData: selectData,
            listData: null,
            loading: true
        },()=>{
            this.shortCutList(floorId, roomId)
        })
    }

    //加载数据
    async shortCutList(floorId, roomId) {
        try {
            let res = await postJson({
                url: NetUrls.siriList,
                params: {
                    type: this.state.currentType,
                    floor: floorId,
                    roomid: roomId
                }
            });
            this.setState({loading: false, refreshing: false})
            if (res.code == 0) {
                this.setState({
                    listData: res.result || []
                })
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            this.setState({loading: false, refreshing: false})
            ToastManager.show('网络错误');
        }
    }

    // 获取筛选列表
    async getFilterList() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.floorAndRoomList,
                params: {
                    type: this.state.filterType,
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                if (data.result && data.result.length > 0) {
                    let result = []
                    let result_ids = []
                    let list = data.result
                    let current_select = []
                    for (const item of list) {
                        let temp = {}
                        let arrs = []
                        let arrs_ids = []
                        for (const child of item.room) {
                            arrs.push(child.roomName)
                            // [楼层id, 房屋id]
                            arrs_ids.push([item.floor, child.roomId])
                            if(child.roomId == this.roomId){
                                current_select = [item.floorText, child.roomName]
                            }
                        }
                        temp[item.floorText] = arrs;
                        result.push(temp)
                        result_ids.push(arrs_ids)
                    }
                    this.setState({
                        filterData: result,
                        filterIds: result_ids,
                        roomselectedData: current_select
                    })
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }
	
	getActionSheet() {
        if(Platform.OS == 'android'){
            return
        }
		return (
			<ActionSheet
				ref={ e => this.actionSheet = e}
				title={'选择需要执行的操作'}
				options={['打开设备', '关闭设备', '取消']}
				cancelButtonIndex={2}
				onPress={(index) => {
					if (index == 0) {
						JKRNUtils.addDeviceOrderToSiriShortCut(this.state.selectId, this.state.selectName, 1)
					} else if (index == 1) {
						JKRNUtils.addDeviceOrderToSiriShortCut(this.state.selectId, this.state.selectName, 0)
					}
				}}
			/>
		)
	}

    onRefresh(){
        this.setState({
            refreshing: true
        })
        this.shortCutList()
    }

    // 获取选择框的 Modal
	getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
	}

    // 弹出 Modal
    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerData,
            pickerTitleText: this.state.pickerTitle,
            selectedValue: this.state.roomselectedData,
            onPickerConfirm: (data,index) => {
                console.log(data,index)
                if(this.state.pickerType == 1){
                    this.handleWithRoomFilter(data,index)
                }else{
                    this.handlePickerSelectData(data,index)
                }
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

    renderHeader(){
        const Colors = this.props.themeInfo.colors
        
        const list = this.state.indicators.map((value, index)=>{

            const lineBackgroud = this.state.currentType == value.type ? Colors.themeButton : Colors.fullyTransparent
            const fontWeight = this.state.currentType == value.type ? 'bold' : 'normal'
            const textColor = this.state.currentType == value.type ? Colors.themeText : Colors.themeTextLight

            return(
                <TouchableOpacity
                    key={'index_'+index}
                    style={styles.indicatorTouch}
                    onPress={()=>{
                        if(this.state.currentType == value.type){
                            return
                        }
                        this.setState({
                            currentType: value.type,
                            listData: null,
                            filterType: value.filterType,
                            roomselectedData: [],
                            filterData: null,
                            loading: true
                        },()=>{
                            this.shortCutList()
                            this.getFilterList()
                        })
                    }}
                >
                    <Text style={{fontSize: 15, color: textColor, fontWeight: fontWeight}}>{value.name}</Text>
                    <View style={{height: 2, width: 35, backgroundColor: lineBackgroud,marginTop: 5}}/>
                </TouchableOpacity>
            )
        })

        return(
            <View style={[styles.header,{backgroundColor: Colors.themeBg, borderBottomColor: Colors.themeBaseBg}]}>
                {list}
            </View>
        )
    }

    renderEmptyList(){
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight

        if(this.state.loading){
            return <ListLoading style={styles.loading}/>
        }else if(this.state.listData && this.state.listData.length <= 0){
            if(this.state.currentType == 1){
                return <ListNoContent img={Images.noScene} text={'暂无可添加Siri快捷的场景'} />
            }else{
                return <ListNoContent img={Images.noDevice} text={'暂无可添加Siri快捷的设备'} />
            }
        }
        return null
    }

    renderItem(rowData, index){
        const Colors = this.props.themeInfo.colors
        const icon = this.props.themeInfo.isDark ? rowData.nightIconOn : rowData.dayIconOn
        const defaultImg = this.props.themeInfo.isDark ? ImagesDark.default : ImagesLight.default

        let roomName = null
        if(rowData.floorText && rowData.roomName){
            roomName = rowData.floorText + ' | '  + rowData.roomName
        }

        return(
            <View style={[styles.item,{backgroundColor: Colors.themeBg, borderBottomColor: Colors.themeBaseBg}]}>
                <Image style={{width: 32, height: 32, resizeMode:'contain'}} defaultSource={defaultImg} source={{uri: icon}}/>
                <View style={{marginLeft: 15, flex: 1}}>
                    <Text style={{color: Colors.themeText, fontWeight: 'bold', fontSize: 16}}>{rowData.name}</Text>
                    {roomName ? <Text style={{color: Colors.themeTextLight, marginTop: 5, fontSize: 12}}>{roomName}</Text> : null}
                </View>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.siriTouch,{borderColor: Colors.themeTextLight}]}
                    onPress={()=>{
                        if(this.state.currentType == 1){
                            JKRNUtils.addSceneToSiriShortCut(rowData.id ,rowData.name)
                        }else{
                            this.setState({
                                selectId: rowData.id,
                                selectName: rowData.name
                            },()=>{
                                this.actionSheet.show()
                            })
                        }
                    }}
                >
                    <Text style={{fontSize: 14, color: Colors.themeTextLight}}>添加到Siri</Text>
                </TouchableOpacity>
            </View>
        )
    }

	renderList() {
		
		return (
			<FlatList
                keyExtractor={(item, index) => index.toString()}
                contentContainerStyle={{
                    paddingBottom: 32,
                    paddingTop: 10
                }}
                data={this.state.listData}
                renderItem={({ item, index }) => this.renderItem(item, index)}
                ListEmptyComponent={() => this.renderEmptyList()}
                initialNumToRender={10}
                // refreshing={this.state.refreshing}
                // onRefresh={() => this.onRefresh()}
            />
		)
	}

	render() {
		const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderHeader()}
                {this.renderList()}
                {this.getActionSheet()}
                {this.getModal()}
            </View>
		)
	}
}

const styles = StyleSheet.create({
	container:{
        flex: 1
    },
    header:{
        height: 44,
        flexDirection: 'row',
        borderBottomWidth: 1
    },
    indicatorTouch:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    loading:{
        alignItems: 'center', 
        justifyContent: 'center', 
        marginTop: '50%',
        paddingVertical: 20
    },
    siriTouch:{
        justifyContent:'center', 
        alignItems:'center',
        height: 30,
        width: 100,
        borderRadius: 15,
        borderWidth: 1
    },
    item:{
        flexDirection: 'row',
        height: 76,
        alignItems:'center', 
        paddingHorizontal: 16,
        borderBottomWidth: 1
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ShortCutList)

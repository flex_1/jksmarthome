import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity,
    Alert,
    Linking
} from 'react-native';
import { DeviceEventEmitter } from "react-native";
import deviceInfo from '../../util/DeviceInfo';
import { connect } from 'react-redux';
import { NotificationKeys,CurrentState, NetUrls, Colors } from '../../common/Constants';
import LocalTokenHandler from '../../util/LocalTokenHandler';
import { postJson,getRequest } from "../../util/ApiRequest";
import SpinnerManager from '../../common/CustomComponent/SpinnerManager';
import ToastManager from "../../common/CustomComponent/ToastManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import AppConfigs,{IOS_APPSTORE_URL,ANDROID_YINYONGBAO_URL} from '../../util/AppConfigs';

const AppleStoreVersionInfo = 'https://itunes.apple.com/cn/lookup?id=1485186693'

class Setting extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'设置'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
		super(props);
        const { getParam } = props.navigation
        this.userData = getParam('userData') || {}
        this.callBack = getParam('callBack')
		this.state = {
            lastVersion: null
        }
    }

    componentDidMount(){
        if(Platform.OS == 'ios'){
            this.checkIOSLastVersion()
        }else{
            this.checkLastVersion()
        }
    }
    
	async logout() {
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.logout,
                timeout: 3
            })
            SpinnerManager.close()
            if (res.code == 0) {
                LocalTokenHandler.remove(() => {
                    DeviceEventEmitter.emit(NotificationKeys.kLogoutNotification)
                })
            } else {
                throw '登出'
            }
        } catch (error) {
            LocalTokenHandler.remove(() => {
                DeviceEventEmitter.emit(NotificationKeys.kLogoutNotification)
            })
        }
    }

    async checkLastVersion() {
        try {
			let data = await getRequest({
				url: NetUrls.appVersion,
			});
			if (data.code == 0) {
                if(data.result && data.result.lastVersion){
                    this.setState({
                        lastVersion: data.result && data.result.lastVersion
                    })
                }
			}
		} catch (error) {
			console.log(JSON.stringify(error));
		}
    }

    async checkIOSLastVersion() {
        try {
			let data = await getRequest({
				url: AppleStoreVersionInfo,
			});
            let versionInfo = data.results && data.results[0]
            if(versionInfo){
                this.setState({
                    lastVersion: versionInfo.version
                })
            }else{
                throw ''
            }
		} catch (error) {
            this.checkLastVersion()
		}
    }

	getItem(title,callBack,text){
        const Colors = this.props.themeInfo.colors

		let extra = <Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
		if(text){
			extra = <Text style={{marginRight: 10,fontSize:15,color:Colors.themeTextLight}}>v{text}</Text>
		}
		return(
			<TouchableOpacity activeOpacity={0.7} style={styles.item} onPress={()=>
				callBack && callBack()
			}>
				<Text style={{fontSize:15,color:Colors.themeText,flex:1}}>{title}</Text>
				{extra}
			</TouchableOpacity>
		)
    }
    
    // 检查更新
    showUpdateAlert(){
        if(!this.state.lastVersion){
            ToastManager.show('正在获取版本信息，请稍后')
            return
        }

        let isNeedUpdate = this.checkAppIsNeedUpdate()

        if(!isNeedUpdate){
            ToastManager.show('当前已是最新版本')
            return
        }

        let tips = 'AppStore已有新版本发布，是否前往AppStore更新版本?'
        if(Platform.OS == 'android'){
            tips = '当前不是最新版本，请前往应用应用市场更新至最新版本。'
        }

        Alert.alert(
            '提示',
            tips,
            [
                { text: '取消', onPress: () => { }, style: 'cancel' },
                { text: '去更新', onPress: () => { 
                    if(Platform.OS == 'android'){
                        Linking.openURL(ANDROID_YINYONGBAO_URL)
                    }else{
                        Linking.openURL(IOS_APPSTORE_URL)
                    }
                }},
            ]
        )
    }

    //判断是否需要更新
    checkAppIsNeedUpdate(){
        if(!this.state.lastVersion){
            return false
        }
        let isNeedUpdate = false
        let currentVersion = deviceInfo.appVersion
        let lastVersion = this.state.lastVersion
        let current_arr = currentVersion.split('.')
        let last_arr = lastVersion.split('.')

        let length = Math.max(current_arr.length, last_arr.length)

        for (let index = 0; index < length; index++) {
            let c = current_arr[index]
            let l = last_arr[index]

            if(c==null && l!=null){
                isNeedUpdate = true
                break
            }else if(c!=null && l==null){
                isNeedUpdate = false
                break
            }else{
                if(parseInt(c) < parseInt(l)){
                    isNeedUpdate = true
                    break
                }else if(parseInt(c) > parseInt(l)){
                    isNeedUpdate = false
                    break
                }
            }
        }
        return isNeedUpdate
    }

    //清除账号缓存
    showCleanUpAlert(){
        Alert.alert(
            '提示',
            '确定要清空登录过的账号记录吗？',
            [
                { text: '取消', onPress: () => { }, style: 'cancel' },
                { text: '清空', onPress: () => { 
                    AppConfigs.cleanAccountNumber()
                    ToastManager.show('清除成功')
                }},
            ]
        )
    }

    renderUpdateIcon(){
        if(!this.checkAppIsNeedUpdate()){
            return null
        }
        return(
            <View style={styles.redDot}/>
        )
    }

	getListView() {
        const {navigate} = this.props.navigation
        const Colors = this.props.themeInfo.colors
        const split = <View style={[styles.line, {backgroundColor:Colors.split}]}/>
        
		return (
			<View style={{ paddingHorizontal:16,marginTop:10,backgroundColor:Colors.themeBg }}>
				{/* {this.getItem('账号管理',()=>{ 
					navigate('UserSetting',{userData: this.userData,callBack:()=>{
                        this.callBack && this.callBack()
                    }})
				})}
                {split} */}
                {/* {this.getItem('消息推送',()=>{ navigate('MessageManage')})}
				{split} */}
				{this.getItem('设置密码',()=>{
                    if(!this.userData){
                        return
                    }
                    if(this.userData.phone?.includes('apple')){
                        navigate('BindPhone',{
                            isChangePhone: true,
                            updateUserInfo: (userData)=>{
                                this.userData = JSON.parse(JSON.stringify(userData))
                            }
                        })
                        return
                    }
                    navigate('ForgetPsw',{
                        title: '设置密码',
                        phone: this.userData.phone,
                        isSettingPass: true
                    })
                })}
				{split}
				{this.getItem('用户协议',()=>{ navigate('Protocol',{title:'用户协议',type:1}) })}
				{split}
				{this.getItem('隐私政策',()=>{ navigate('Protocol',{title:'隐私政策',type:2}) })}
				{split}
                {this.getItem('清空登录历史',()=>{ this.showCleanUpAlert() })}
				{split}
                <TouchableOpacity activeOpacity={0.7} style={styles.item}
                    onPress = {()=>{
                        this.showUpdateAlert()
                    }}
                    onLongPress={()=>{
					    if(CurrentState != 'production'){
						    navigate('DebugPage')
					    }
				    }}
                >
					<Text style={{fontSize:15,color:Colors.themeText,flex:1}}>版本信息</Text>
                    {this.renderUpdateIcon()}
					<Text style={{fontSize:15,color:Colors.themeTextLight}}>{'v'+ deviceInfo.appVersion}</Text>
                    <Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
				</TouchableOpacity>
			</View>
		)
	}

	getButton() {
        const Colors = this.props.themeInfo.colors
		return (
			<View style={{ paddingHorizontal:16,marginTop:50,marginBottom:40 }}>
				<TouchableOpacity
					activeOpacity={0.7}
					style={[styles.btn, {backgroundColor:Colors.themeBg}]}
					onPress={() => { this.logout() }}
				>
					<Text style={{ color:Colors.red,fontSize:15 }}>退出当前账号</Text>
				</TouchableOpacity>
			</View>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors
		return (
			<ScrollView style={{backgroundColor: Colors.themeBaseBg}}>
				{this.getListView()}
				{this.getButton()}
			</ScrollView >
		)
	}
}

const styles = StyleSheet.create({
	item:{
		flexDirection:'row',
		alignItems:'center',
		height:50
	},
	arrow:{
		width:8,
		height:12,
		marginRight:5,
		marginLeft:10
	},
	btn:{
		width:'100%',
		height: 40,
		borderRadius:4,
		justifyContent:'center',
		alignItems:'center'
	},
	line:{
		width:'100%',
		height:1,
    },
    versionTips:{
        marginRight: 10,
        fontSize:13,
    },
    redDot:{
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: Colors.themBgRed,
        marginRight: 10
    }
})


export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Setting)
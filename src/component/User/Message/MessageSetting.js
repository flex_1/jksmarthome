/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    NativeModules,
    Linking,
    Platform,
    Modal,
    AppState
} from 'react-native';
import { connect } from 'react-redux';
import { NetUrls,Colors } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { postJson } from "../../../util/ApiRequest";
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import SwitchButton from '../../../common/CustomComponent/SwitchButton';
import HeaderTitle from '../../CommonPage/Navigation/HeaderTitle';

const JKRNUtils = Platform.select({
    ios: NativeModules.JKRNUtils,
    android: NativeModules.JKCameraAndRNUtils
});

class MessageSetting extends Component {

    static navigationOptions = ({navigation}) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={'消息设置'}/>,
        headerBackground: <HeaderBackground hiddenBorder={false}/>
    })

    constructor(props) {
        super(props);
        const { getParam } = this.props.navigation
        this.callBack = getParam('callBack')

        this.state = {
            isNoticeOpened: getParam('isNoticeOpened') || false,
            appState: AppState.currentState,

            jpushSystematic: 0,
            jpushAlarm: 1,
            isDnd: true,
            dndTimeStart: 23,
            dndTimeEnd: 7,

            selectedValue: [],

            member: 1,
            device: 1,
            scene: 1,
            warnning: 0,
            isNotDisturb: true,
            modalVisible: false,
            pickerData: [],
            beginTime: 23,
            endTime: 7
        }
    }

    componentDidMount() {
        this.requestMessageSettings()
        this.checkNotificationSwitch()
        AppState.addEventListener("change", this._handleAppStateChange);
    }

    componentWillUnmount() {
        AppState.removeEventListener("change", this._handleAppStateChange);
    }

    //加载设置数据
    async requestMessageSettings() {
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.getConfig,
            });
            SpinnerManager.close()
            if (res.code == 0) {
                this.setState({
                    jpushSystematic: res.result?.jpushSystematic,
                    jpushAlarm: res.result?.jpushAlarm,
                    isDnd: res.result?.isDnd,
                    dndTimeStart: res.result?.dndTimeStart,
                    dndTimeEnd: res.result?.dndTimeEnd,
                })
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误');
        }
    }

    //保存设置数据
    async saveMessageSettings(callBack) {
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.setConfig,
                params: {
                    jpushSystematic: this.state.jpushSystematic,
                    jpushAlarm: this.state.jpushAlarm,
                    isDnd: this.state.isDnd ? true: false,
                    dndTimeStart: this.state.dndTimeStart,
                    dndTimeEnd: this.state.dndTimeEnd,
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                callBack && callBack()
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误');
        }
    }

    // 处理App状态
    _handleAppStateChange = nextAppState => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            this.checkNotificationSwitch()
        }
        this.setState({ appState: nextAppState })
    }

    // 监测系统通知开关
    checkNotificationSwitch(){
        // 监测App 推送权限是否开启
        JKRNUtils.getSystemNoticeStatus().then((isOpen) => {
            this.setState({isNoticeOpened: isOpen})
        }).catch((e) => {
            console.log('getSystemNoticeStatus error', e)
        });
    }

    handlePickerSelectData(data,index){
        let startLabel = data[0]
        let endLabel = data[1]

        let beginTime = parseInt(startLabel)
        let endTime = parseInt(endLabel)

        this.setState({
            dndTimeStart: beginTime,
            dndTimeEnd: endTime
        },()=>{
            this.saveMessageSettings()
        })
    }

    renderHeaderTips(){
        if(this.state.isNoticeOpened == true){
            return null
        }

        return(
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={styles.header} 
                onPress={()=>{
                    if(Platform.OS == 'ios'){
                        Linking.openURL('app-settings:')
                    }else{
                        JKRNUtils.openSystemNoticeView();
                    }
                }}
            >   
                <Image style={styles.warning} source={require('../../../images/warning.png')}/>
                <Text style={styles.tipsText}>点此开启消息通知权限，实时接收消息通知</Text>
            </TouchableOpacity>
        )
    }

    renderDisturbBottom(){
        if(!this.state.isDnd){
            return null
        }
        const Colors = this.props.themeInfo.colors

        let beginLabel = this.state.dndTimeStart + ':00'
        let endlabel = this.state.dndTimeEnd + ':00'

        return(
            <TouchableOpacity activeOpacity={0.7} style={styles.disturbBottomTouch} onPress={()=>{
                if(!this.state.pickerData || this.state.pickerData.length<=0){
                    let start = []
                    let end = []
                    for (let index = 0; index < 24; index++) {
                        let time = index + ':00'
                        start.push(time)
                        end.push(time)
                    }
                    this.state.pickerData = [start, end]
                }
                this.setState({
                    pickerData: this.state.pickerData,
                    modalVisible: true,
                    selectedValue: [beginLabel, endlabel]
                })
            }}>
                <Text style={[styles.titleText,{color: Colors.themeText}]}>免打扰时段</Text>
                <Text style={[styles.extraText,{color: Colors.themeTextLight}]}>{beginLabel}-{endlabel}</Text>
                <Image style={{width:16,height:16,marginLeft: 5}} source={require('../../../images/message/rightArrow.png')}/>
            </TouchableOpacity>
        )
    }

    renderArrowItem(title){
        const Colors = this.props.themeInfo.colors

        return(
            <TouchableOpacity style={styles.item} onPress={()=>{
                this.props.navigation.navigate('DeviceMessageManage',{
                    jpushAlarm: this.state.jpushAlarm,
                    callBack: (jpushAlarm)=>{
                        this.setState({
                            jpushAlarm: jpushAlarm
                        },()=>{
                            this.saveMessageSettings()
                        })
                    }
                })
            }}>
                <Text style={[styles.titleText,{color: Colors.themeText}]}>{title}</Text>
                <Image style={{width:16,height:16,marginRight:16}} source={require('../../../images/message/rightArrow.png')}/>
            </TouchableOpacity>
        )
    }

    renderSwitchItem(title, type){
        const Colors = this.props.themeInfo.colors
        
        return(
            <View style={styles.item}>
                <Text style={[styles.titleText,{color: Colors.themeText}]}>{title}</Text>
                <SwitchButton
                    isAsync = {true}
                    value = {this.state[type] == 1}
                    onPress = {()=>{
                        this.state[type] = this.state[type] == 1 ? 0:1
                        this.setState({
                            ...this.state
                        },()=>{
                            this.saveMessageSettings()
                        })
                    }}
                />
            </View>
        )
    }

    renderAccessList(){
        const Colors = this.props.themeInfo.colors
        
        return(
            <View style={[styles.itemWrapper,{backgroundColor: Colors.themeBg}]}>
                {this.renderSwitchItem('系统通知', 'jpushSystematic')}
                <View style={[styles.line,{backgroundColor: Colors.themeBaseBg}]}/>
                {this.renderSwitchItem('设备通知', 'jpushAlarm')}
                <View style={[styles.line,{backgroundColor: Colors.themeBaseBg}]}/>
                {this.renderArrowItem('设备通知管理')}
                <View style={[styles.line,{backgroundColor: Colors.themeBaseBg}]}/>
                {this.renderSwitchItem('消息免打扰', 'isDnd')}
                <View style={[styles.line,{backgroundColor: Colors.themeBaseBg}]}/>
                {this.renderDisturbBottom()}
            </View>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerData,
            pickerTitleText: '请选择免打扰时间段',
            selectedValue: this.state.selectedValue,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false
                })
                this.handlePickerSelectData(data,index);
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                }, () => {
                    window.CustomPicker.hide()
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

    // 免打扰时间段
    renderTimeModal(){
        return(
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.CustomPicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                <ScrollView contentContainerStyle={styles.scrollContent}>
                    {this.renderHeaderTips()}
                    {this.renderAccessList()}
                </ScrollView>
                {this.renderTimeModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    scrollContent:{
        paddingBottom: 100
    },
    header:{
        height:40, 
        marginHorizontal: 16,
        backgroundColor: '#FFFBE8',
        alignItems: 'center',
        paddingHorizontal: 15,
        flexDirection: 'row',
        borderRadius: 8,
        marginTop: 10
    },
    tipsText:{
        fontSize: 12,
        color: '#FF6F3D',
        marginLeft: 7
    },
    warning:{
        width: 14,
        height: 14
    },
    itemWrapper:{
        marginTop: 20,
        borderRadius: 8,
        marginHorizontal: 16
    },
    line:{
        width: '100%',
        height: 1
    },
    item:{
        height:42,
        flexDirection:'row',
        alignItems:'center',
        paddingLeft: 16
    },
    titleText:{
        fontSize: 16,
        fontWeight: '400',
        flex: 1
    },
    disturbBottomTouch:{
        height: 42,
        width: '100%',
        flexDirection:'row', 
        alignItems: 'center',
        paddingHorizontal: 16
    },
    extraText:{
        fontSize: 14,
        fontWeight: '400'
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo
    })
)(MessageSetting)

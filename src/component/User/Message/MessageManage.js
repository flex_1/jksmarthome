/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import {NetUrls} from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import {postJson} from "../../../util/ApiRequest";
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import {ImagesLight, ImagesDark} from '../../../common/Themes';

class MessageManage extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'消息推送'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

    constructor(props) {
        super(props);
        this.state = {
            status: [0,0,0]
        }
    }

    componentDidMount() {
        this.getMessageStatus()
    }

    async getMessageStatus() {
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.getJpushStatus,
                params: {}
            });
            SpinnerManager.close()
            if (res.code == 0) {
                let status = res.result && res.result.jpushStatus
                if(status){
                    this.setState({ 
                        status: status.split('')
                    })
                }
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误');
        }
    }

    async messageSetting(index) {
        let targetStatus = JSON.parse(JSON.stringify(this.state.status))
        targetStatus[index] = targetStatus[index]==1 ? 0:1

        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.jpushStatus,
                params: {
                    status: targetStatus.join('') 
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                this.setState({ 
                    status: targetStatus
                })
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误');
        }
    }

    renderItem(title, value, callBack){
        const Colors = this.props.themeInfo.colors
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
        
        return(
            <View style={styles.item}>
				<Text style={[styles.itemTitle, {color:Colors.themeText}]}>{title}</Text>
                <SwitchButton
                    isAsync = {true}
                    value = {value == 1}
                    onPress = {()=>{
                        callBack()
                    }}
                />
			</View>
        )
    }

    renderMainList(){
        const Colors = this.props.themeInfo.colors
        return(
            <View style={[styles.wrapper, {backgroundColor: Colors.themeBg}]}>
                {this.renderItem('接收成员关系变动消息', this.state.status[0], ()=>{
                    this.messageSetting(0)
                } )}
                <View style={[styles.line, {backgroundColor:Colors.split}]}/>
                {this.renderItem('接收设备定时消息', this.state.status[1], ()=>{
                    this.messageSetting(1)
                } )}
                <View style={[styles.line, {backgroundColor:Colors.split}]}/>
                {this.renderItem('接收场景定时消息', this.state.status[2], ()=>{
                    this.messageSetting(2)
                } )}
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors
        return (
            <View style={{
                backgroundColor: Colors.themeBaseBg,
                flex: 1
            }}>
                {this.renderMainList()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    wrapper:{
        marginTop: 10,
        paddingLeft: 16
    },
    item:{
		flexDirection:'row',
		alignItems:'center',
        height:55
    },
    itemTitle:{
        fontSize:15,
        flex:1
    },
    itemTouch:{
        height: '100%',
        paddingHorizontal: 16,
        justifyContent:'center',
        alignItems: 'center'
    },
    line:{
		width:'100%',
		height:1,
	}
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(MessageManage)

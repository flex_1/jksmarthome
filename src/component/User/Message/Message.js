/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    FlatList,
    NativeModules,
    Linking,
    Platform,
    AppState,
    Modal
} from 'react-native';
import { connect } from 'react-redux';
import { NetUrls,Colors } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { postJson } from "../../../util/ApiRequest";
import DateUtil,{GetTimeLabel} from "../../../util/DateUtil";
import { ListNoContent, ListLoading, FooterEnd, FooterLoading } from "../../../common/CustomComponent/ListLoading";
import {ImagesLight, ImagesDark} from '../../../common/Themes';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';
import HeaderTitle from '../../CommonPage/Navigation/HeaderTitle';
import { updateUserInfo } from '../../../redux/actions/UserInfoAction';

const JKRNUtils = Platform.select({
    ios: NativeModules.JKRNUtils,
    android: NativeModules.JKCameraAndRNUtils
});

class Message extends Component {

    static navigationOptions = ({navigation}) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={'消息中心'}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {icon:require('../../../images/panel/robot/more.png'),onPress:()=>{
                    navigation.navigate('MessageSetting')
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground hiddenBorder={false}/>
    })

    constructor(props) {
        super(props);
        const { getParam } = this.props.navigation
        this.callBack = getParam('callBack')

        this.state = {
            isNoticeOpened: true,
            appState: AppState.currentState,
            loading: true,
            currentPage: 1,
            totalPage: null,
            
            refreshing: false,

            newdeviceList: null,
            olddeviceList: null,
            systemMsgCount: 0,
            systemMsgRemark: '',
            modalVisible: false,
            selectHouseId: null,
            selectHouse: [],
            filterIds: [],
            pickerData: []
        }
    }

    componentDidMount() {
        this.messageList()
        this.checkNotificationSwitch()
        AppState.addEventListener("change", this._handleAppStateChange);
    }

    componentWillUnmount() {
        AppState.removeEventListener("change", this._handleAppStateChange);
    }

    // 处理App状态
    _handleAppStateChange = nextAppState => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            this.checkNotificationSwitch()
        }
        this.setState({ appState: nextAppState })
    }

    // 监测系统通知开关
    checkNotificationSwitch(){
        // 监测App 推送权限是否开启
        JKRNUtils.getSystemNoticeStatus().then((isOpen) => {
            console.log('getSystemNotice', isOpen)
            this.setState({isNoticeOpened: isOpen})
        }).catch((e) => {
            console.log('getSystemNoticeStatus error', e)
        });
    }

    //加载数据
    async messageList() {
        try {
            let res = await postJson({
                url: NetUrls.msgListV3,
                params: {
                    page: this.state.currentPage,
                    houseId: this.state.selectHouseId
                }
            });
            this.setState({loading: false, refreshing: false})
            if (res.code == 0) {
                let olddeviceList = []
                if(this.state.currentPage == 1){
                    olddeviceList = res.result.olddeviceList || []
                }else if(this.state.currentPage > 1){
                    olddeviceList = this.state.olddeviceList.concat(res.result.olddeviceList)
                }
                this.setState({
                    currentPage: res.result?.curPage,
                    totalPage: res.result?.totalPageNum,
                    systemMsgCount: res.result?.systemMsgCount,
                    systemMsgRemark: res.result?.systemMsgRemark,
                    newdeviceList: res.result?.newdeviceList,
                    olddeviceList: olddeviceList

                })
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            this.setState({loading: false, refreshing: false})
            ToastManager.show('网络错误');
        }
    }

    // 获取筛选列表
    async getFilterList() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.houseList,
                params: {
                    type: 6,  // 6代表智能条件选择触发设备
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                if (data.result.listhouse.length > 0) {
                    let result = ['全部房屋']
                    let result_ids = [0]
                    
                    for (const item of data.result.listhouse) {
                        result.push(item.name)
                        result_ids.push(item.id)
                    }
                    this.setState({
                        pickerData: result,
                        filterIds: result_ids,
                        modalVisible: true
                    })
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

    //点击已读 type : 1-今天的消息 2-更早的消息 3-系统已读
    async messageRead(id, index, type) {
        try {
            let res = await postJson({
                url: NetUrls.readMessage,
                params: {
                    id: id,
                    type: type == 3 ? 6 : 4
                }
            });
            if (res.code == 0) {
                if(!res.result.totalUnRead){
                    this.props.updateUserInfo()
                }
                //修改本地数据状态
                if(type == 1){
                    this.state.newdeviceList[index].msgCount = 0
                }else if(type == 2){
                    this.state.olddeviceList[index].msgCount = 0
                }else if(type == 3){
                    this.state.systemMsgRemark = '最近7天无通知'
                    this.state.systemMsgCount = 0
                }

                this.setState({
                    ...this.state
                });
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            ToastManager.show('网络错误，请稍后重试');
        }
    }

    //设备消息 全部已读
    async batchRead() {
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.readAllMessage,
                params: {
                    type: 4,
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                this.callBack && this.callBack()
                if(!res.result.totalUnRead){
                    this.props.updateUserInfo()
                }
                //修改本地数据状态
                this.state.olddeviceList.forEach(item => {
                    item.msgCount = 0;
                });
                this.state.newdeviceList.forEach(item => {
                    item.msgCount = 0;
                });
                this.setState({
                    ...this.state
                });
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误，请稍后重试');
        }
    }
    
    //下拉刷新
    onRefresh() {
        this.state.currentPage = 1
        this.setState({
            refreshing: true
        })
        this.messageList()
    }

    //加载下一页
    onLoadNextPage() {
        if (this.state.currentPage >= this.state.totalPage) {
            return;
        }
        this.state.currentPage += 1
        this.messageList()
    }

    handlePickerData(selectData, selectIndex){
        this.setState({
            modalVisible: false,
            selectHouse: selectData,
            loading: true,
            selectHouseId: this.state.filterIds[selectIndex[0]],
            currentPage: 1,
            totalPage: null,
            newdeviceList: null,
            olddeviceList: null
        },()=>{
            this.messageList()
        })
    }

    renderHeaderTips(){
        if(this.state.isNoticeOpened == true){
            return null
        }

        return(
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={styles.header} 
                onPress={()=>{
                    if(Platform.OS == 'ios'){
                        Linking.openURL('app-settings:')
                    }else{
                        JKRNUtils.openSystemNoticeView();
                    }
                }}
            >   
                <Image style={styles.warning} source={require('../../../images/warning.png')}/>
                <Text style={styles.tipsText}>点此开启消息通知权限，实时接收消息通知</Text>
            </TouchableOpacity>
        )
    }

    renderDefaultItem(){
        const Colors = this.props.themeInfo.colors
        const redDot = this.state.systemMsgCount > 0 ? <View style={styles.redDot}/> : null
        let tips = null
        if(this.state.systemMsgRemark){
            tips = <Text style={[styles.subtitle,{color: Colors.themeTextLight}]}>{this.state.systemMsgRemark}</Text>
        }
    
        return(
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={[styles.defaultItem,{backgroundColor: Colors.themeBg}]}
                onPress={()=>{
                    if(this.state.systemMsgCount > 0){
                        this.messageRead(null,null,3)
                    }
                    const {navigate} = this.props.navigation
                    navigate('MessageDetailList',{
                        title: '系统通知',
                        type: 6
                    })
                }}
            >
                <Image style={styles.itemImg} source={require('../../../images/message/ding.png')}/>
                <View style={styles.titleWrapper}>
                    <Text style={[styles.title,{color: Colors.themeText}]}>系统通知</Text>
                    {tips}
                </View>
                {redDot}
            </TouchableOpacity>
        )
    }

    renderFilterHeader(){
        const Colors = this.props.themeInfo.colors
        let houseText = '全部房屋'
        if(this.state.selectHouseId){
            houseText = this.state.selectHouse.toString()
        }
        let tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}
        
        return(
            <View style={{marginTop: 20,flexDirection:'row',paddingHorizontal: 16,alignItems: 'center'}}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={{flexDirection: 'row',alignItems:'center',paddingVertical:6}} 
                    onPress={()=>{
                        if(!this.state.pickerData || this.state.pickerData.length <= 0){
                            this.getFilterList()
                        }else{
                            this.setState({
                                modalVisible: true
                            })
                        }
                    }}
                >
                    <Text style={{fontSize: 16, fontWeight: '500',color: Colors.themeText}}>{houseText}</Text>
                    <Image style={[{width:16,height:16,marginLeft:5},tintColor]} source={require('../../../images/message/arrow_down.png')}/>
                </TouchableOpacity>
                <View style={{flex: 1}}/>
                <TouchableOpacity style={{flexDirection: 'row',alignItems:'center'}} onPress={()=>{
                    this.batchRead()
                }}>
                    <Image style={[{width:24,height:24},tintColor]} source={require('../../../images/message/message.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    // 设备单元格 type : 1-今天的消息 2-更早的消息
    renderDeviceItem(rowData, index, type){
        const Colors = this.props.themeInfo.colors
        const defaultImg = this.props.themeInfo.isDark ? ImagesDark.default : ImagesLight.default
        
        const redDot = rowData.msgCount > 0 ? <View style={styles.redDot2}/> : null
        const icon = this.props.themeInfo.isDark ? rowData?.nightIcon : rowData?.dayIcon
        const time = GetTimeLabel(rowData.latestTime)

        let roomText = null
        if(rowData.floorText && rowData.roomName){
            roomText = '  |  ' + rowData.floorText + '  |  ' + rowData.roomName
        }

        return (
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={{flexDirection: 'row',flex: 1,alignItems: 'center',paddingHorizontal: 16,height:90}} 
                onPress={()=>{
                    if(rowData.msgCount > 0){
                        this.messageRead(rowData.deviceId, index, type)
                    }
                    const {navigate} = this.props.navigation
                    navigate('MessageDetailList',{
                        title: rowData.deviceName,
                        deviceData: rowData,
                        type: 4
                    })
                }}
            >
                <Image style={styles.itemImg} defaultSource={defaultImg} source={{uri: icon}}/>
                <View style={styles.titleWrapper}>
                    <Text style={[styles.title,{color: Colors.themeText}]}>{rowData.deviceName}</Text>
                    <Text style={[styles.roomText,{color: Colors.themeTextLight}]}>{rowData.houseName}{roomText}</Text>
                    <Text style={[styles.marks,{color: Colors.themeTextLight}]}>{rowData.remark}</Text>
                </View>
                {redDot}
                <Text style={[styles.messageTime,{color: Colors.themeTextLight}]}>{time}</Text>
            </TouchableOpacity>
        )
    }

    // 今天的消息
    renderListHeader(){
        const Colors = this.props.themeInfo.colors
        
        if(!this.state.newdeviceList || this.state.newdeviceList.length <= 0){
            return null
        }

        const todayList = this.state.newdeviceList.map((value, index)=>{
            return (
                <View key={index} style={{flex: 1}}>
                    <View style={{marginHorizontal: 16,height:1,backgroundColor: '#F1F2F6'}}/>
                    {this.renderDeviceItem(value, index, 1)}
                </View>
            )
        })
        
        return(
            <View style={{marginHorizontal: 16,marginBottom:16,borderRadius:8,backgroundColor: Colors.themeBg}}>
                <View style={{paddingVertical: 9,justifyContent:'center',alignItems:'center'}}>
                    <View style={{width:123,height:24,borderRadius:12,backgroundColor: '#D8D9E3',justifyContent:'center',alignItems:'center'}}>
                        <Text style={{fontSize:12,color: Colors.white}}>今天</Text>
                    </View>
                </View>
                {todayList}
            </View>
        )
    }

    // 更早的消息
    renderItem(rowData, index){
        const Colors = this.props.themeInfo.colors
        
        let header = null
        if(index == 0){
            header = (
                <View style={{paddingVertical: 9,justifyContent:'center',alignItems:'center'}}>
                    <View style={{width:123,height:24,borderRadius:12,backgroundColor: '#D8D9E3',justifyContent:'center',alignItems:'center'}}>
                        <Text style={{fontSize:12,color: Colors.white}}>更早</Text>
                    </View>
                </View>
            )
        }

        let borderStyle = {}
        if(index == 0){
            borderStyle = {borderTopLeftRadius: 8, borderTopRightRadius: 8}
        }else if(index == this.state.olddeviceList.length - 1){
            borderStyle = {borderBottomLeftRadius: 8, borderBottomRightRadius: 8}
        }
        
        return(
            <View style={[{marginHorizontal: 16,backgroundColor: Colors.themeBg},borderStyle]}>
                {header}
                <View style={{marginHorizontal: 16,height:1,backgroundColor: '#F1F2F6'}}/>
                {this.renderDeviceItem(rowData, index, 2)}
            </View>
        )
    }

    renderEmptyList(){
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
        if(this.state.loading){
            return <ListLoading style={styles.loading}/>
        }else if(this.state.newdeviceList?.length <= 0){
            return <ListNoContent img={Images.noMessage} text={'暂无消息'} />
        }else{
            return null
        }
    }

    //footer
    renderFooter() {
        if(!this.state.olddeviceList || this.state.olddeviceList.length <= 0){
            return null
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return <FooterEnd/>
        }
        if(this.state.olddeviceList.length > 0){
            return <FooterLoading/>
        }
        return null;
    }

    renderList(){
        return(
            <View style={{flex: 1,marginTop: 16 }}>
                <FlatList
                    keyExtractor={(item, index) => 'message_' + index}
                    contentContainerStyle={{paddingBottom: 32}}
                    data={this.state.olddeviceList}
                    renderItem={({ item, index }) => this.renderItem(item, index)}
                    refreshing={this.state.refreshing}
                    ListHeaderComponent={()=> this.renderListHeader()}
                    onRefresh={() => this.onRefresh()}
                    ListEmptyComponent={() => this.renderEmptyList()}
                    onEndReached={() => this.onLoadNextPage()}
                    onEndReachedThreshold={0.1}
                    ListFooterComponent={() => this.renderFooter()}
                />
            </View>
        )
    }

    // 获取选择框的 Modal
	getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
	}

    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerData,
            pickerTitleText: '房屋筛选',
            selectedValue: this.state.selectHouse,
            onPickerConfirm: (data,index) => {
                this.handlePickerData(data, index)
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }
    
    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderHeaderTips()}
                {this.renderDefaultItem()}
                {this.renderFilterHeader()}
                {this.renderList()}
                {this.getModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    loading:{
        alignItems: 'center', 
        justifyContent: 'center', 
        marginTop: 40,
        paddingVertical: 20
    },
    header:{
        height:40, 
        marginHorizontal: 16,
        backgroundColor: '#FFFBE8',
        alignItems: 'center',
        paddingHorizontal: 15,
        flexDirection: 'row',
        borderRadius: 8,
        marginTop: 10
    },
    tipsText:{
        fontSize: 12,
        color: '#FF6F3D',
        marginLeft: 7
    },
    arrow:{
        width: 7,
        height: 12,
        resizeMode: 'contain',
        marginLeft: 5
    },
    listHeader:{
        paddingBottom: 14
    },
    defaultItem:{
        marginHorizontal: 16,
        height: 56,
        borderRadius: 8,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 17,
        paddingHorizontal: 10
    },
    itemImg:{
        width: 28,
        height: 28,
        resizeMode: 'contain'
    },
    redDot:{
        position: 'absolute',
        width: 6,
        height: 6,
        borderRadius: 3,
        top: 8,
        left: 8,
        backgroundColor: Colors.themBgRed
    },
    redDot2:{
        position: 'absolute',
        width: 6,
        height: 6,
        borderRadius: 3,
        top: 16,
        left: 16,
        backgroundColor: Colors.themBgRed
    },
    itemArrow:{
        width: 12,
        height: 22,
        resizeMode: 'contain',
        marginLeft: 10
    },
    titleWrapper:{
        marginLeft: 16,
        flex: 1
    },
    title:{
        fontSize: 14,
        fontWeight: '400',
    },
    subtitle:{
        fontSize: 12,
        fontWeight: '400',
        marginTop: 5
    },
    item:{
        height: 90, 
        width:'100%',
        marginTop: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 16
    },
    roomText:{
        fontSize: 12,
        fontWeight: '400',
        marginTop: 5
    },
    marks:{
        fontSize: 12,
        fontWeight: '400',
        marginTop: 5
    },
    warning:{
        width: 14,
        height: 14
    },
    messageTime:{
        position: 'absolute',
        top: 16,
        right: 16,
        fontSize: 12,
        fontWeight: '400'
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    }),
    (dispatch) => ({
        updateUserInfo: ()=> dispatch(updateUserInfo())
    })
)(Message)

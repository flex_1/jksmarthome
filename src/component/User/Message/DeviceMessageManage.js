/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    Modal,
    FlatList
} from 'react-native';
import { connect } from 'react-redux';
import {NetUrls, Colors} from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import {postJson} from "../../../util/ApiRequest";
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import {ImagesLight, ImagesDark} from '../../../common/Themes';
import SwitchButton from '../../../common/CustomComponent/SwitchButton';
import HeaderTitle from '../../CommonPage/Navigation/HeaderTitle';
import { ListNoContent, ListLoading, FooterEnd, FooterLoading } from "../../../common/CustomComponent/ListLoading";

class DeviceMessageManage extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={'设备通知管理'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

    constructor(props) {
        super(props);
        const { getParam } = this.props.navigation

        this.callBack = getParam('callBack')
        this.state = {
            selectHouseId: null,
            selectHouse: [],
            modalVisible: false,
            pickerData: [],
            filterIds: [],
            jpushAlarm: getParam('jpushAlarm'),
            deviceList: null,
            loading: true,
            currentPage: 1,
            totalPage: null,
        }
    }

    componentDidMount() {
        this.getMessageDeviceList()
    }

    async getMessageDeviceList() {
        
        try {
            let res = await postJson({
                url: NetUrls.relay1pList,
                params: {
                    houseId: this.state.selectHouseId,
                    page: this.state.currentPage
                }
            });
            
            this.setState({loading: false})
            if (res.code == 0) {
                let deviceList = []
                if(this.state.currentPage == 1){
                    deviceList = res.result.list || []
                }else if(this.state.currentPage > 1){
                    deviceList = this.state.deviceList.concat(res.result.list)
                }
                this.setState({
                    deviceList: deviceList,
                    currentPage: res.result?.curPage,
                    totalPage: res.result?.totalPageNum,
                })
                
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            this.setState({loading: false})
            
            ToastManager.show('网络错误');
        }
    }

    //保存设置数据
    async saveMessageSettings(id, jpushStatus, callBack) {
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.deviceJpushStatus,
                params: {
                    deviceId: id,
                    jpushStatus: jpushStatus
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                callBack && callBack()
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误');
        }
    }

    // 获取筛选列表
    async getFilterList() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.houseList,
                params: {
                    type: 6,  // 6代表智能条件选择触发设备
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                if (data.result.listhouse.length > 0) {
                    let result = ['全部房屋']
                    let result_ids = [0]
                    
                    for (const item of data.result.listhouse) {
                        result.push(item.name)
                        result_ids.push(item.id)
                    }
                    this.setState({
                        pickerData: result,
                        filterIds: result_ids,
                        modalVisible: true
                    })
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

    handlePickerData(selectData, selectIndex){
        this.setState({
            modalVisible: false,
            selectHouse: selectData,
            loading: true,
            selectHouseId: this.state.filterIds[selectIndex[0]],
            deviceList: null,
            loading: true,
            currentPage: 1,
            totalPage: null
        },()=>{
            this.getMessageDeviceList()
        })
    }

     //加载下一页
     onLoadNextPage() {
        if (this.state.currentPage >= this.state.totalPage) {
            return;
        }
        this.state.currentPage += 1
        this.getMessageDeviceList()
    }

    renderFilterHeader(){
        const Colors = this.props.themeInfo.colors
        let houseText = '全部房屋'
        if(this.state.selectHouseId){
            houseText = this.state.selectHouse.toString()
        }
        let tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}
        
        return(
            <View style={{marginTop: 20,flexDirection:'row',paddingHorizontal: 16,alignItems: 'center'}}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={{flexDirection: 'row',alignItems:'center',paddingVertical:6}} 
                    onPress={()=>{
                        if(!this.state.pickerData || this.state.pickerData.length <= 0){
                            this.getFilterList()
                        }else{
                            this.setState({
                                modalVisible: true
                            })
                        }
                    }}
                >
                    <Text style={{fontSize: 16, fontWeight: '500',color: Colors.themeText}}>{houseText}</Text>
                    <Image style={[{width:16,height:16,marginLeft:5},tintColor]} source={require('../../../images/message/arrow_down.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    renderTopSwicth(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={[styles.topItem,{backgroundColor: Colors.themeBg}]}>
                <Text style={{fontSize: 16,fontWeight:'400',flex: 1, color: Colors.themeText}}>允许通知</Text>
                <SwitchButton
                    isAsync = {true}
                    value = {this.state.jpushAlarm == 1}
                    onPress = {()=>{
                        let target = this.state.jpushAlarm ? 0:1
                        this.setState({
                            jpushAlarm: target
                        },()=>{
                            this.callBack && this.callBack(target)
                        })
                    }}
                />
            </View>
        )
    }

    //footer
    renderFooter() {
        if(!this.state.deviceList || this.state.deviceList.length <= 0){
            return null
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return <FooterEnd/>
        }
        if(this.state.deviceList.length > 0){
            return <FooterLoading/>
        }
        return null;
    }

    renderEmptyList(){
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
        if(this.state.loading){
            return <ListLoading style={styles.loading}/>
        }else if(this.state.deviceList?.length <= 0){
            return <ListNoContent img={Images.noDevice} text={'暂无设备'} />
        }else{
            return null
        }
    }

    renderItem(rowData, index){
        const Colors = this.props.themeInfo.colors
        const firstStyle = index == 0 ? {borderTopLeftRadius: 8, borderTopRightRadius: 8} : {}
        const lastStyle = index == (this.state.deviceList.length -1) ? {borderBottomLeftRadius: 8, borderBottomRightRadius: 8} : {}
        const line = index == (this.state.deviceList.length -1) ? null : <View style={[styles.line,{backgroundColor: Colors.themeBaseBg}]}/>

        const defaultImg = this.props.themeInfo.isDark ? ImagesDark.default : ImagesLight.default
        const icon = this.props.themeInfo.isDark ? rowData?.nightImg : rowData?.dayImg

        let roomText = null
        if(rowData.floorText && rowData.roomName){
            roomText = '  |  ' + rowData.floorText + '  |  ' + rowData.roomName
        }

        return(
            <View style={[styles.item,firstStyle,lastStyle,{backgroundColor: Colors.themeBg}]}>
                <Image style={styles.itemImg} defaultSource={defaultImg} source={{uri: icon}}/>
                <View style={{marginLeft: 12, flex: 1}}>
                    <Text style={[styles.name, {color:Colors.themeText}]}>{rowData.deviceName}</Text>
                    <Text style={[styles.roomText,{color: Colors.themeTextLight}]}>{rowData.houseName}{roomText}</Text>
                </View>
                <SwitchButton
                    isAsync = {true}
                    value = {rowData.jpushStatus == 1}
                    onPress = {()=>{
                        let target = rowData.jpushStatus ? 0:1
                        this.saveMessageSettings(rowData.deviceId,target,()=>{
                            let device = this.state.deviceList[index]
                            device['jpushStatus'] = target
                            this.state.deviceList[index] = device
                            this.setState({
                                deviceList: this.state.deviceList
                            })
                        })
                    }}
                />
				{line}
			</View>
        )
    }

    renderDeviceList(){
        if(!this.state.jpushAlarm){
            return null
        }
        return(
            <View style={{marginTop: 25,flex: 1}}>
                <FlatList
                    keyExtractor={(item, index) => 'device_' + index}
                    contentContainerStyle={{paddingBottom: 32}}
                    data={this.state.deviceList}
                    renderItem={({ item, index }) => this.renderItem(item, index)}
                    ListEmptyComponent={() => this.renderEmptyList()}
                    onEndReached={() => this.onLoadNextPage()}
                    onEndReachedThreshold={0.1}
                    ListFooterComponent={() => this.renderFooter()}
                />
            </View>
        )

    }

    // 获取选择框的 Modal
	getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
	}

    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerData,
            pickerTitleText: '房屋筛选',
            selectedValue: this.state.selectHouse,
            onPickerConfirm: (data,index) => {
                this.handlePickerData(data, index)
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

    render() {
        const Colors = this.props.themeInfo.colors
        return (
            <View style={{backgroundColor: Colors.themeBaseBg,flex: 1}}>
                {this.renderFilterHeader()}
                {this.renderTopSwicth()}
                {this.renderDeviceList()}
                {this.getModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    wrapper:{
        marginTop: 10,
        paddingLeft: 16
    },
    itemTitle:{
        fontSize:15,
        flex:1
    },
    itemTouch:{
        height: '100%',
        paddingHorizontal: 16,
        justifyContent:'center',
        alignItems: 'center'
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    topItem:{
        marginTop: 20,
        marginHorizontal: 16,
        height:42,
        flexDirection:'row',
        alignItems:'center',
        paddingLeft: 16,
        borderRadius: 8
    },
    item:{
        marginHorizontal: 16,
        height:56,
		flexDirection:'row',
		alignItems:'center',
        paddingLeft: 16
    },
    line:{
        position: 'absolute',
		height:1,
        bottom: 0,
        right: 0,
        left: 42,
	},
    itemImg:{
        width: 28,
        height: 28,
        resizeMode: 'contain'
    },
    name:{
        fontSize: 16,
        fontWeight: '400'
    },
    roomText:{
        marginTop: 5,
        fontSize: 12,
        fontWeight: '400'
    },
    loading:{
        alignItems: 'center', 
        justifyContent: 'center', 
        marginTop: '30%',
        paddingVertical: 20
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(DeviceMessageManage)

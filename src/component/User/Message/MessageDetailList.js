/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    FlatList,
    SectionList,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import { NetUrls } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { StatusBarHeight, NavigationBarHeight, BottomSafeMargin } from '../../../util/ScreenUtil';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { postJson } from "../../../util/ApiRequest";
import DateUtil,{GetDateLabel} from "../../../util/DateUtil";
import { ListNoContent, FooterEnd, FooterLoading, ListLoading } from "../../../common/CustomComponent/ListLoading";
import {ImagesLight, ImagesDark} from '../../../common/Themes';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import { updateUserInfo } from '../../../redux/actions/UserInfoAction';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderTitle from '../../CommonPage/Navigation/HeaderTitle';

class MessageDetailList extends Component {

    static navigationOptions = ({navigation}) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={navigation.getParam('title')}/>,
        headerBackground: <HeaderBackground hiddenBorder={false}/>
    })

    constructor(props) {
        super(props);
        const { getParam } = this.props.navigation
        this.callBack = getParam('callBack')
        this.type = getParam('type')
        this.deviceData = getParam('deviceData') || {}

        this.deviceId = this.deviceData.deviceId
        this.attributeName = this.deviceData.attributeName
        
        this.state = {
            messageLists: [],
            currentPage: 1,
            totalPage: 0,
            loading: true,

            listItemType: 0,   //0:正常显示列表  1:展示选择框,
            selectIds: [],
            totalMessage: 0,
            allIds: null
        }
    }

    componentDidMount() {
        this.messageList();
    }

    //加载数据
    async messageList() {
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.listLevelV3,
                params: {
                    page: this.state.currentPage,
                    type: this.type,
                    deviceId: this.deviceId
                }
            });
            SpinnerManager.close()
            this.setState({loading: false})
            if (res.code == 0) {
                let messageLists = []
                if(this.state.currentPage == 1){
                    messageLists = res.result.list || []
                }else if(this.state.currentPage > 1){
                    messageLists = this.state.messageLists.concat(res.result.list)
                }
                let allIds = []
                for (const message of messageLists) {
                    for (const m of message.data) {
                        allIds.push(m.id)
                    }
                }
                this.setState({
                    messageLists: messageLists,
                    totalPage: res.result.totalPageNum,
                    allIds: allIds
                })
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            this.setState({loading: false})
            ToastManager.show('网络错误');
        }
    }

    //删除消息
    async deleteMessage() {
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.deleteMessage,
                params: {
                    messageId: this.state.selectIds.toString()
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                this.setState({
                    currentPage: 1,
                    listItemType: 0,
                    allIds: null,
                    selectIds: []
                },()=>{
                    this.messageList()
                })
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误，请稍后重试');
        }
    }

    //点击已读
    async messageRead(id, index) {
        SpinnerManager.show()
        try {
            const { pop } = this.props.navigation;
            let res = await postJson({
                url: NetUrls.singleReadMessage,
                params: {
                    id: id
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                if(!res.result.typeUnRead){
                    this.callBack && this.callBack()
                }
                if(!res.result.totalUnRead){
                    this.props.updateUserInfo()
                }
                //修改本地数据状态
                this.state.messageLists[index].isRead = 1;
                this.setState({
                    messageLists: this.state.messageLists,
                });
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误，请稍后重试');
        }
    }

    //全部已读
    async batchRead() {
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.batchReadMessage,
                params: {
                    type: this.type,
                    deviceId: this.deviceId
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                this.callBack && this.callBack()
                if(!res.result.totalUnRead){
                    this.props.updateUserInfo()
                }
                //修改本地数据状态
                this.state.messageLists.forEach(item => {
                    if(item.isRead == 0) {
                        item.isRead = 1;
                    }
                });
                this.setState({
                    messageLists: this.state.messageLists,
                });
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误，请稍后重试');
        }
    }

    //点击全部已读
    onClickAllRead() {
        let ids = []
        for (const val of this.state.messageLists) {
            if (val.isRead == 0) {
                ids.push(val.id);
            }
        }
        if (ids.length <= 0) {
            return;
        }
        this.batchRead();
    }

    //下拉刷新
    onRefresh() {
        this.state.currentPage = 1
        this.messageList()
    }

    //加载下一页
    onLoadNextPage() {
        if(this.state.listItemType == 1){
            return
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return;
        }
        this.state.currentPage += 1
        this.messageList()
    }

    //未读红点
    renderHotDot(isRead) {
        const Colors = this.props.themeInfo.colors
        if (isRead == 0) {
            return (
                <View style={styles.hotDotContainer}>
                    <View style={[styles.hotDot, {backgroundColor: Colors.red}]} />
                </View>
            );
        }
        return null;
    }

    //空列表
    renderEmptyList() {
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
        if(this.state.loading){
            return <ListLoading style={styles.loading}/>
        }else if(this.state.messageLists?.length <= 0){
            return <ListNoContent img={Images.noMessage} text={'暂无消息'} />
        }
        return null
    }

    //footer
    renderFooter() {
        if(!this.state.messageLists || this.state.messageLists.length <= 0){
            return null
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return <FooterEnd/>
        }
        if(this.state.messageLists.length > 0){
            return <FooterLoading/>
        }
        return null;
    }

    renderTopSelect(){
        if(!this.state.messageLists || this.state.messageLists.length <= 0){
            return null
        }
        const Colors = this.props.themeInfo.colors

        let selectLabel = '选择信息'
        let selectIcon = require('../../../images/message/select.png')
        if(this.state.listItemType == 1){
            selectLabel = '全选'
            if(!this.state.allIds || (this.state.allIds.length != this.state.selectIds.length)){
                selectIcon = require('../../../images/message/unselect.png')
            }else{
                selectIcon = require('../../../images/message/selected.png')
            }
        }

        return(
            <View style={{width:'100%',alignItems:'flex-end'}}>
                <TouchableOpacity style={styles.selectTouch} onPress={()=>{
                    if(this.state.listItemType == 0){
                        this.setState({
                            listItemType: 1
                        })
                    }else{
                        if(!this.state.allIds || (this.state.allIds.length != this.state.selectIds.length)){
                            this.setState({
                                selectIds: JSON.parse(JSON.stringify(this.state.allIds)),
                            })
                        }else{
                            this.setState({
                                selectIds: [],
                            })
                        }
                    }
                }}>
                    <Text style={[styles.selectText,{color: Colors.themeTextLight}]}>{selectLabel}</Text>
                    <Image style={styles.select} source={selectIcon}/>
                </TouchableOpacity>
            </View>
        )
    }

    renderSectionHeader(section){
        const Colors = this.props.themeInfo.colors;
        
        return(
            <View style={[styles.headerWrapper,{backgroundColor: Colors.themeBg}]}>
                <View style={{width:130,height:24,justifyContent:'center',alignItems:'center',borderRadius:12,backgroundColor:'#D8D9E3'}}>
                    <Text style={{fontSize:13,fontWeight:'400',color: Colors.white}}>{GetDateLabel(section.createTime)}</Text>
                </View>
                <View style={styles.line}/>
            </View>
        )
    }

    renderRowItem(rowData, index, section){
        if(!rowData){
            return null
        }

        const Colors = this.props.themeInfo.colors
        let rightIcon = require('../../../images/message/rightArrow.png')
        if(this.state.listItemType == 1){
            if(this.state.selectIds.includes(rowData.id)){
                rightIcon = require('../../../images/message/selected.png')
            }else{
                rightIcon = require('../../../images/message/unselect.png')
            }
        }

        let lastItemStyle = {}
        let firstItemStyle = {}
        if(index == section.data.length - 1){
            lastItemStyle = {marginBottom: 24,paddingBottom:16,borderBottomLeftRadius: 8, borderBottomRightRadius: 8}
        }else if(index == 0){
            firstItemStyle = {paddingTop: 16}
        }
        
        return(
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={[styles.item,lastItemStyle,firstItemStyle,{backgroundColor: Colors.themeBg}]} 
                onPress={()=>{
                    if(this.state.listItemType == 0){
                        this.props.navigation.navigate('MessageDetail',{
                            content: rowData.content,
                            title: '消息详情',
                            time: rowData.createTime
                        })
                    }else{
                        if(this.state.selectIds.includes(rowData.id)){
                            this.state.selectIds.remove(rowData.id)
                        }else{
                            this.state.selectIds.push(rowData.id)
                        }
                        this.setState({
                            ...this.state
                        })
                    }
                }}
            >   
                <Text style={[styles.titleTime,{color: Colors.themeText}]}>{DateUtil(rowData.createTime, 'hh:mm')}</Text>
                <Text numberOfLines={1} style={[styles.title,{color: Colors.themeText}]}>{rowData.title}</Text>
                <Image style={{width:16,height:16}} source={rightIcon}/>
            </TouchableOpacity>
        )
    }

    renderMainListView(){
        return(
            <View style={{flex: 1}}>
                <SectionList
				    style={{flex: 1}}
				    contentContainerStyle={{ paddingBottom: 50}}
				    stickySectionHeadersEnabled={false}
  				    renderItem={({ item, index, section }) => this.renderRowItem(item, index, section)}
  				    renderSectionHeader={({ section }) => this.renderSectionHeader(section)}
				    sections={this.state.messageLists}
				    scrollIndicatorInsets={{right: 1}}
				    initialNumToRender={5}
				    keyExtractor={(item, index) => 'devices_'+index}
                    onEndReached={() => this.onLoadNextPage()}
                    onEndReachedThreshold={0.1}
                    ListEmptyComponent={() => this.renderEmptyList()}
			    />
            </View>
        )
    }

    renderBottomDelete(){
        if(this.state.listItemType == 0){
            return null
        }
        const Colors = this.props.themeInfo.colors
        return(
            <TouchableOpacity style={[styles.bottomWrapper,{backgroundColor: Colors.themeBg,borderTopColor: Colors.themeBaseBg}]} onPress={()=>{
                if(!this.state.selectIds || this.state.selectIds.length <= 0){
                    ToastManager.show('请至少选中一个消息。')
                    return
                }
                this.deleteMessage()
            }}>
                <Text style={{fontSize:16, fontWeight:'400',color: Colors.themeText}}>删除</Text>
            </TouchableOpacity>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors
        return (
            <View style={{backgroundColor: Colors.themeBaseBg,flex: 1}}>
                {this.renderTopSelect()}
                {this.renderMainListView()}
                {this.renderBottomDelete()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    loading:{
        alignItems: 'center', 
        justifyContent: 'center', 
        marginTop: '50%',
        paddingVertical: 20
    },
    hotDotContainer: {
        position: 'absolute',
        height: '100%',
        right: 12,
        alignItems: 'center',
        justifyContent: 'center',
    },
    hotDot: {
        width: 7,
        height: 7,
        borderRadius: 4,
    },
    allRead:{
        fontSize: 15
    },
    select:{
        width: 20,
        height: 20,
        resizeMode: 'contain',
        marginLeft: 8
    },
    selectText:{
        fontSize: 14,
        fontWeight: '400'
    },
    selectTouch:{
        flexDirection:'row',
        alignItems:'center',
        paddingHorizontal:16,
        paddingVertical:15
    },
    headerWrapper:{
        marginHorizontal: 16, 
        height: 42, 
        justifyContent:'center',
        alignItems:'center',
        borderTopRightRadius: 8,
        borderTopLeftRadius: 8
    },
    line:{
        position: 'absolute',
        height: 1,
        left: 16,
        right: 16,
        bottom: 0,
        backgroundColor: '#F1F2F6'
    },
    item:{
        justifyContent: 'center',
        marginHorizontal: 16,
        paddingHorizontal: 16,
        paddingVertical: 8,
        flexDirection: 'row',
        alignItems:'center'
    },
    bottomWrapper:{
        paddingBottom: BottomSafeMargin + 16, 
        paddingTop:16, 
        justifyContent:'center',
        alignItems:'center',
        borderTopWidth: 1,
    },
    titleTime:{
        fontSize: 13, 
        fontWeight:'400',
        lineHeight:17
    },
    title:{
        fontSize: 13, 
        fontWeight:'400',
        lineHeight:17,
        marginLeft: 5,
        flex: 1
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo
    }),
    (dispatch) => ({
        updateUserInfo: ()=> dispatch(updateUserInfo())
    })
)(MessageDetailList)

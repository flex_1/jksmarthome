import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    StatusBar,
    Alert
} from 'react-native';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import { connect } from 'react-redux';
import { NotificationKeys, NetUrls, LocalStorageKeys } from '../../common/Constants';
import ToastManager from "../../common/CustomComponent/ToastManager";
import LocalTokenHandler from '../../util/LocalTokenHandler';
import AsyncStorage from '@react-native-community/async-storage';
import { postJson,postPicture } from "../../util/ApiRequest";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import { updateAppTheme } from '../../redux/actions/ThemeAction';
import SwitchButton from '../../common/CustomComponent/SwitchButton';

class LaboratoryList extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'实验室功能'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
		super(props);
        this.state = {
            floatBtnShow: false
        }
	}

	componentDidMount() {
		
	}
	
    changeDarkTheme(type){
        let isDark = this.props.themeInfo.isDark ? 1:0
        if(isDark == type){
            return
        }
        if(type == 1){
            // 暗黑模式
            this.props.updateAppTheme(true)
            StatusBar.setBarStyle('light-content')
            AsyncStorage.setItem(LocalStorageKeys.kAppThemeIsDark,'1')
        }else{
            // 明亮模式
            this.props.updateAppTheme(false)
            StatusBar.setBarStyle('dark-content')
            AsyncStorage.setItem(LocalStorageKeys.kAppThemeIsDark,'0')
        }
    }
    
	getActionSheet() {
		return (
			<ActionSheet
                title='选择主题模式'
				ref={e => this.actionSheet = e}
				options={['明亮模式', '黑夜模式', '取消']}
				cancelButtonIndex={2}
				onPress={(index) => {
                    if(index == 0){
                        this.changeDarkTheme(0)
                    }else if(index == 1){
                        this.changeDarkTheme(1)
                    }
				}}
			/>
		)
	}

    renderSiriItem(){
        if(Platform.OS == 'android'){
            return null
        }
        const Colors = this.props.themeInfo.colors
        return(
            <>
                <View style={[styles.line, {backgroundColor:Colors.split}]}/>
                <TouchableOpacity activeOpacity={0.7} style={styles.item} onPress={()=>{
                    this.props.navigation.navigate('ShortCutList')
                }}>
					<Text style={{fontSize:15,color:Colors.themeText}}>添加Siri快捷指令</Text>
                    <View style={{flex: 1}}/>
					<Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
				</TouchableOpacity>
            </>
        )
    }
    
    // 问号按钮
    renderQuestionBtn(){
        return(
            <TouchableOpacity style={{height:'100%',paddingHorizontal:12,justifyContent:'center',alignItems:'center'}} onPress={()=>{
                Alert.alert(
                    '说明',
                    '1. 开启后，当应用进入后台时，会在桌面显示一个悬浮按钮，按住按钮说话可实现【语音控制】功能。\r\n'+
                    '2. 使用悬浮框功能前，请先确认开启【允许显示在其他应用上层】和【本地录音】的权限\r\n'+
                    '3. 当小萨管家APP进程被杀死时，悬浮框会消失。',
                    [
                        {text: '知道了', onPress: () => {}, style: 'cancel'}
                    ]
                )
            }}>
                <Image style={{width: 16, height:16}} source={require('../../images/sceneIcon/question.png')}/>
            </TouchableOpacity>
        )
    }

    renderAndroidFloatButton(){
        // if(Platform.OS == 'ios'){
        //     return null
        // }
        const Colors = this.props.themeInfo.colors
        return(
            <>
                <View style={[styles.line, {backgroundColor:Colors.split}]}/>
                <View  style={styles.item}>
					<Text style={{fontSize:15,color:Colors.themeText}}>语音后台悬浮框</Text>
                    {this.renderQuestionBtn()}
                    <View style={{flex: 1}}/>
					<SwitchButton
                        value = {this.state.floatBtnShow}
                        isAsync = {false}
                        onPress = {()=>{
                            let target = this.state.floatBtnShow ? false : true
                            this.setState({
                                floatBtnShow: target
                            })
                        }}
                    />
				</View>
            </>
        )
    }

	//常用功能 Item
    getCustomItem(img, title, toPage) {
		const { navigate } = this.props.navigation
		const Colors = this.props.themeInfo.colors
		
        return(
            <TouchableOpacity activeOpacity={0.7} style={styles.customItemTouch} onPress={()=>{
                navigate(toPage)
            }}>
                <Text style={{flex:1,marginLeft:20,fontSize:15,color:Colors.themeText}}>{title}</Text>
                <Image style={{width:8,height:12,marginRight:5}} source={require('../../images/enterLight.png')}/>
            </TouchableOpacity>
        )
    }

	getListView() {
		const {navigate,pop} = this.props.navigation
		const Colors = this.props.themeInfo.colors
		
		let themeText = this.props.themeInfo.isDark ? '黑夜模式' : '明亮模式'

		return (
			<View style={{marginTop:10, backgroundColor:Colors.themeBg }}>	
				<TouchableOpacity activeOpacity={0.7} style={styles.item} onPress={()=>{
                    this.actionSheet.show()
                }}>
					<Text style={{fontSize:15,color:Colors.themeText}}>主题模式</Text>
                    <View style={{flex: 1}}/>
					<Text style={{color: Colors.themeTextLight}}>{themeText}</Text>
					<Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
				</TouchableOpacity>
                {this.renderSiriItem()}
                {/* {this.renderAndroidFloatButton()} */}
			</View>
		)
	}

	render() {
		const Colors = this.props.themeInfo.colors
		return (
			<ScrollView style={{backgroundColor: Colors.themeBaseBg}}>
				{this.getListView()}
                
				{this.getActionSheet()}
			</ScrollView >
		)
	}
}

const styles = StyleSheet.create({
	item:{
		flexDirection:'row',
		alignItems:'center',
        height:55,
        paddingLeft: 16
    },
	arrow:{
		width:8,
		height:12,
		marginRight:16,
		marginLeft:15
	},
	line:{
		width:'100%',
		height:1,
    },
    titles: {
        fontSize:16,
        fontWeight:'bold',
        margin: 16,
        marginTop: 30
    },
    loginIcon:{
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    thirdText:{
        marginLeft: 10,
        fontSize: 15,
    },
    imgWrapper:{
        width:38,
        height:38,
        borderRadius:19,
        overflow:'hidden'
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    }),
    (dispatch) => ({
        updateAppTheme: (isDark)=> dispatch(updateAppTheme(isDark)),
    })
)(LaboratoryList)

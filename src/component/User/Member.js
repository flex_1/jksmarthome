/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 成员与权限
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    FlatList, 
    Alert,
    RefreshControl
} from 'react-native';
import { connect } from 'react-redux';
import {Colors, NetUrls} from '../../common/Constants';
import ToastManager from "../../common/CustomComponent/ToastManager";
import {BottomSafeMargin} from '../../util/ScreenUtil';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import {postJson} from "../../util/ApiRequest";
import {ListNoContent,ListLoading,ListError } from "../../common/CustomComponent/ListLoading";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import {ImagesLight, ImagesDark} from '../../common/Themes';

class Member extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'家庭成员'}/>,
        headerRight: (
            navigation.getParam('isHouseHolder') ? <HeaderRight buttonArrays={[
                {text: '过户', onPress:()=>{
                    navigation.navigate('TransferWarrning')
                }}
            ]}/> : null
        ),
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation

        this.state = {
            memberList: null,
            loading: true,
            isRefreshing: false,
            isHouseHolder: getParam('isHouseHolder')
        }
    }

    componentDidMount() {
        this.getMemberListInfo();
    }

    async getMemberListInfo() {
        try {
            let res = await postJson({
                url: NetUrls.memberList,
                useToken: true,
                params: {}
            });
            this.setState({
                isRefreshing: false,
                loading: false
            })
            if (res.code == 0) {
                this.setState({
                    memberList: res.result || []
                })
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            this.setState({
                isRefreshing: false,
                loading: false
            })
            ToastManager.show(JSON.stringify(e));
        }
    }

    async requestDelete(id) {
        SpinnerManager.show()
        try {
            let res = await postJson({
                url: NetUrls.deleteMember,
                useToken: true,
                params: {
                    "id": id
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                this.getMemberListInfo()
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(e));
        }
    }

    // 删除成员
    deleteMember(id){
        Alert.alert(
            '提示',
            '确认删除该成员?',
            [
                {
                    text: '取消', onPress: () => {}, style: 'cancel'
                },
                {
                    text: '删除', onPress: () => {
                        this.requestDelete(id)
                    }
                },
            ],
        )
    }

    // 进入设置
    gotoSetting(val){
        const {navigate} = this.props.navigation;
        navigate("MemberSetting", {
            phoneNum: val.phone, 
            memberId: val.id,
            memberType: val.type,
            remark: val.memberRemark,
            isHouseHolder: this.state.isHouseHolder,
            isEnabled: val.isEnabled,
            startTime: val.startTime,
            endTime: val.endTime,
            callBack: () => {
                this.getMemberListInfo()
            },
            deletCallBack: ()=>{
                this.deleteMember(val.id)
            }
        })
    }

    onRefresh(){
        if (this.state.isRefreshing) return
        this.setState({ isRefreshing: true })
        this.getMemberListInfo()
    }

    renderRowItem(item, index){
        const Colors = this.props.themeInfo.colors;
        const userIcon = item.img ? {uri: item.img} : require('../../images/user_manage/memberIcon.png')
        const memberRemark = item.memberRemark || ''
        const host = item.type == 2 ? "普通成员" : "管理员"

        let bgColor = item.isEnabled ? Colors.themeBg : Colors.offlineBg
        let expireText = null
        const currentTime = Date.parse(new Date())
        if(currentTime < item.startTime || currentTime > item.endTime){
            bgColor = Colors.offlineBg
            expireText = <Text style={[styles.expireText,{color: Colors.themeTextLight}]}>已停用</Text>
        }
        
        return(
            <View style={[styles.itemWrapper,{paddingLeft: index%2 ? 8:16, paddingRight: index%2 ? 16:8}]}>
                <View style={[styles.item,{backgroundColor: bgColor}]}>
                    <TouchableOpacity activeOpacity={0.7} style={styles.topTouch} onPress={()=>{
                        this.props.navigation.navigate('MemberLoginRecord',{id: item.id})
                    }}>
                        <Image style={styles.icon} source={userIcon}/>
                        <Text style={[styles.phoneText,{color: Colors.themeText}]}>{item.phone}</Text>
                        <Text style={styles.markText}>{memberRemark}</Text>
                        <View style={styles.labelWrapper}>
                            <Text style={styles.memberText}>{host}</Text>
                            {expireText}
                        </View>
                    </TouchableOpacity>
                    <View style={styles.itemTouch}>
                        <TouchableOpacity style={styles.btn} onPress={()=>{ this.deleteMember(item.id) }}>
                            <Image style={styles.btnImg} source={require('../../images/user_manage/memberDelete.png')}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btn} onPress={()=>{ this.gotoSetting(item) }}>
                            <Image style={styles.btnImg} source={require('../../images/user_manage/memberSetting.png')}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }

    renderNoContent(){
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
        return(
            <ListNoContent
				img={Images.noMember} 
				text={'暂无成员'}
			/>
        )
    }

    // 成员列表
    getMemberList() {
        let memberList = this.state.memberList

        //正在加载
		if (this.state.loading && !memberList) {
			return <ListLoading/>
        }
        // 网络错误
		if (!memberList) {
            return <ListError onPress={()=>{ this.getMemberListInfo() }}/>
        }

        // 正常列表
		return (
            <FlatList
                style={{flex: 1}}
                contentContainerStyle={{paddingBottom: 100}}
                numColumns={2}
                scrollIndicatorInsets={{right: 1}}
                removeClippedSubviews={false}
                data={memberList}
                keyExtractor={(item, index)=> "member_index_" + index}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
                ListEmptyComponent={this.renderNoContent()}
                initialNumToRender={10}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.isRefreshing}
                        onRefresh={this.onRefresh.bind(this)}
                    />
                }
            />
        )
    }

    sureButton() {
        const {navigate} = this.props.navigation;
        const Colors = this.props.themeInfo.colors;

        return (
            <View style={[styles.bottomWrapper,{backgroundColor: Colors.themeBaseBg}]}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.sureTouch} 
                    onPress={() => {
                        navigate('AddMember', {
                            isHouseHolder: this.state.isHouseHolder,
                            callBack: () => { this.getMemberListInfo() }
                        })
                    }}
                >
                    <Text style={styles.addMemText}>添加成员</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors;

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.getMemberList()}
                {this.sureButton()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.themBGLightGray,
        flex: 1
    },
    itemWrapper:{ 
		width:'50%',
        marginTop: 15
    },
    item:{
        width:'100%',
        borderRadius: 10,
        alignItems: 'center',
    },
    icon:{
        marginTop: 10,
        width: 50,
        height: 50,
        resizeMode: 'contain',
        borderRadius: 25
    },
    phoneText:{
        color: Colors.themeTextBlack,
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 10,
    },
    markText:{
        color: Colors.themeTextLightGray,
        fontSize: 14,
        marginTop: 10,
    },
    memberText:{
        color: Colors.tabActiveColor,
        fontSize: 12
    },
    topTouch:{
        alignItems:'center',
        width:'100%'
    },
    itemTouch:{
        marginTop: 10,
        flexDirection: 'row',
        justifyContent:'space-between',
        width: '100%'
    },
    btn:{
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    btnImg:{
        width: 24,
        height: 24,
        resizeMode: 'contain'
    },
    sureTouch:{
        width: '90%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.tabActiveColor,
        borderRadius: 4
    },
    addMemText:{
        justifyContent: 'center',
        alignItems: 'center',
        color: Colors.white
    },
    bottomWrapper:{
        width: '100%', 
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
        paddingBottom: BottomSafeMargin + 10,
        paddingTop: 10
    },
    expireText:{
        fontSize: 12,
        marginLeft: 10
    },
    labelWrapper:{
        flexDirection: 'row', 
        alignItems: 'center',
        marginTop: 15
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Member)

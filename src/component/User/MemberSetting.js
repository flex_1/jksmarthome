/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 成员设置
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ImageBackground,
    TextInput,
    Button, 
    ScrollView, 
    Switch, 
    Alert,
    DeviceEventEmitter
} from 'react-native';
import {Colors, NotificationKeys, NetUrls} from '../../common/Constants';
import {postJson} from '../../util/ApiRequest';
import {StatusBarHeight,BottomSafeMargin} from '../../util/ScreenUtil';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import ActionSheet from 'react-native-actionsheet';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import DateUtil from '../../util/DateUtil';

class MemberSetting extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'成员设置'}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text: '保存', onPress:()=>{
                    navigation.getParam('saveBtn')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground/>
    })
    
    constructor(props) {
        super(props)
        const {getParam, setParams} = props.navigation;

        this.phoneNum = getParam('phoneNum')
        this.memberId = getParam('memberId')
        this.callBack = getParam('callBack')
        this.deletCallBack = getParam('deletCallBack')
        this.startTime = getParam('startTime') || ''
        this.endTime = getParam('endTime') || ''
        
        this.state = {
            memberType: getParam('memberType') || 1,
            remark: getParam('remark'),
            isEnabled: getParam('isEnabled'),
            startTime: this.startTime && DateUtil(this.startTime,'yyyy-MM-dd'),
            endTime: this.endTime && DateUtil(this.endTime,'yyyy-MM-dd')
        }

        setParams({
            saveBtn: ()=>{ this.createAndUpdate() }
        })
    }

    componentDidMount() {
        
    }

    async createAndUpdate() {
        SpinnerManager.show()
        let params = {}
        if(this.state.isEnabled){
            params.startTime = this.state.startTime
            params.endTime = this.state.endTime
        }
        try {
            const {pop} = this.props.navigation;
            let res = await postJson({
                url: NetUrls.createAndUpdate,
                useToken: true,
                params: {
                    id: this.memberId,
                    phone: this.phoneNum,
                    type: this.state.memberType,
                    memberRemark: this.state.remark,
                    isEnabled: this.state.isEnabled,
                    ...params
                }
            });
            SpinnerManager.close()
            if (res.code == 0) {
                //回调刷新
                this.callBack && this.callBack()
                pop()
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(e));
        }
    }

    getActionSheet() {
        return (
            <ActionSheet
                ref={e => this.actionSheet = e}
                options={['普通成员', '管理员', '取消']}
                cancelButtonIndex={2}
                onPress={(index) => {
                    if (index == 0) {
                        this.setState({
                            memberType: 2//"普通成员"
                        })
                    } else if (index == 1) {
                        this.setState({
                            memberType: 1//"管理员"
                        })
                    }
                }}
            />
        )
    }

    // 手机号
    renderPhoneItem() {
        const Colors = this.props.themeInfo.colors;
        return (
            <View style={[styles.customItemTouch,{backgroundColor: Colors.themeBg}]} >
                <Text style={[styles.itemTitle,{color: Colors.themeText}]}>成员手机号</Text>
                <Text style={[styles.phoneText,{color: Colors.themeText}]}>{this.phoneNum}</Text>
            </View>
        )
    }

    // 成员备注
    renderRemarkItem() {
        const Colors = this.props.themeInfo.colors;
        return (
            <View style={[styles.customItemTouch,{backgroundColor: Colors.themeBg}]} >
                <Text style={[styles.itemTitle,{color: Colors.themeText}]}>备注名</Text>
                <TextInput
                    defaultValue={this.state.remark}
                    style={[styles.phoneText,{color: Colors.themeText}]}
                    placeholderTextColor={Colors.themeInactive}
                    placeholder="请输入备注名(可选)"
                    keyboardType={'default'}
                    maxLength={8}
                    onChangeText={(text) => {
                        this.state.remark = text
                    }}
                />
            </View>
        )
    }

    // 成员身份
    getCustomItem(title, toPage) {
        const Colors = this.props.themeInfo.colors;
        const {navigate} = this.props.navigation;
        let memberType = toPage ? '' : (this.state.memberType == 1 ? "管理员" : "普通成员")

        return (
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={[styles.customItemTouch,{backgroundColor: Colors.themeBg}]} 
                onPress={() => {
                    if (toPage) {
                        navigate(toPage, {
                            memberId: this.memberId,
                            phoneNum: this.phoneNum,
                            memberType: this.state.memberType,
                            isEnabled: this.state.isEnabled
                        })
                    } else {
                        //弹出选择框
                        this.actionSheet.show()
                    }
                }}
            >
                <Text style={{flex: 1, marginLeft: 20, fontSize: 15, color: Colors.themeText}}>{title}</Text>
                <View style={{flexDirection: 'row', alignItems: "center"}}>
                    <Text style={{fontSize: 15, marginRight: 10, color: Colors.themeText}}>{memberType}</Text>
                    <Image style={{width: 8, height: 12, marginRight: 5}} source={require('../../images/enterLight.png')}/>
                </View>
            </TouchableOpacity>
        )
    }

    // 生效时间
    renderEffectTime(){
        const Colors = this.props.themeInfo.colors;
        const {navigate} = this.props.navigation;
        let extraText = '永久'
        let start = this.state.startTime
        let end = this.state.endTime

        if(!this.state.isEnabled){
            extraText = '已停用'
        }else{
            extraText = start + ' 至 ' + end
        }
        
        return (
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={[styles.customItemTouch,{backgroundColor: Colors.themeBg}]} 
                onPress={() => {
                    navigate('EffectTime',{
                        beginDate: start, 
                        endDate: end,
                        isEnabled: this.state.isEnabled,
                        isShowEffectSwitch: true,
                        callBack: (beginDate, endDate, isEnabled)=>{
                            this.setState({
                                startTime: beginDate,
                                endTime: endDate,
                                isEnabled: isEnabled
                            })
                        }
                    })
                }}
            >
                <Text style={{flex: 1, marginLeft: 20, fontSize: 15, color: Colors.themeText}}>生效日期</Text>
                <View style={{flexDirection: 'row', alignItems: "center"}}>
                    <Text style={{fontSize: 15, marginRight: 10, color: Colors.themeText}}>{extraText}</Text>
                    <Image style={{width: 8, height: 12, marginRight: 5}} source={require('../../images/enterLight.png')}/>
                </View>
            </TouchableOpacity>
        )
    }

    // 删除按钮
    getDeleteButton() {
        const {pop} = this.props.navigation;
        return (
            <View style={styles.bottomBtnWrapper}>
                <TouchableOpacity activeOpacity={0.7} style={styles.deleteTouch} onPress={() => {
                    Alert.alert(
                        '提示',
                        '确认删除该成员?',
                        [
                            {
                                text: '取消', style: 'cancel'
                            },
                            {
                                text: '删除', onPress: () => {
                                    // 删除成员
                                    this.deletCallBack && this.deletCallBack()
                                    pop()
                                }
                            },
                        ],
                    )
                }}>
                    <Text style={{fontSize: 15, color: Colors.white}}>删除成员</Text>
                </TouchableOpacity>
            </View>
        )
    }

    getPermissonSelect(){
        if(this.state.memberType == 1){
            return null
        }
        const Colors = this.props.themeInfo.colors;
        return(
            <>
                {this.getCustomItem("房间权限", "RoomPermission")}
                <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                {this.getCustomItem("设备权限", "DevicePermission")}
            </>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors;

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                <ScrollView style={{paddingTop: 10}}>
                    {this.renderPhoneItem()}
                    <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                    {this.renderRemarkItem()}
                    <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                    {this.getCustomItem("成员类型", null)}
                    <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                    {this.renderEffectTime()}
                    <View style={styles.split}/>
                    {this.getPermissonSelect()}
                    {this.getActionSheet()}
                </ScrollView>
                {this.getDeleteButton()}
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 16,
		paddingLeft: 10,
	},
	navText: {
		color: Colors.tabActiveColor,
		fontSize: 15
	},
    customItemTouch: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 50,
        paddingRight: 20
    },
    line:{
        height:1,
        width: '100%'
    },
    split: {
        height: 20,
        width: '100%'
    },
    deleteTouch: {
        width: '100%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.themBgRed,
        borderRadius: 4
    },
    bottomBtnWrapper: {
        position: 'absolute',
        left: 0,
        bottom: BottomSafeMargin,
        width: '100%',
        paddingHorizontal: 16,
        paddingVertical: 10
    },
    phoneText:{
        fontSize: 16, 
        marginRight: 10, 
        textAlign: 'right',
        flex: 1
    },
    itemTitle:{
        marginLeft: 20,
        fontSize: 16
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(MemberSetting)

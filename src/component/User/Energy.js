import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Dimensions 
} from 'react-native';
import { Colors, NetUrls, NotificationKeys } from '../../common/Constants';
import HeaderLeftView from '../../common/CustomComponent/HeaderLeftView';
import ToastManager from "../../common/CustomComponent/ToastManager";
import { DeviceEventEmitter } from "react-native";
import LocalTokenHandler from '../../util/LocalTokenHandler';
import { postJson } from "../../util/ApiRequest";
import DateUtil from "../../util/DateUtil";
import {StatusBarHeight} from "../../util/ScreenUtil";

class Energy extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeftView navigation={navigation} text={'能源'} />,
        headerRight: (
            <TouchableOpacity style={styles.navTouch} onPress={() => {
                navigation.navigate('PriceSetting')
            }}>
                <Text style={styles.navRText}>电价设置</Text>
            </TouchableOpacity>
        )
    })

    constructor(props) {
        super(props);
        const { getParam } = props.navigation
        this.state = {
            indicators: ['日', '周', '月', '年'],
            select: '日',
            data: {}
        };
    }

    componentDidMount() {
        this.requestEnergy(1)
    }

    //网络请求
    async requestEnergy(type) {
        try {
            let res = await postJson({
                url: NetUrls.energy,
                params: {
                    type: type
                }
            });
            if (res.code == 0) {
                this.setState({
                    data: res.result || {}
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    getHeaderSegment() {
        let list = this.state.indicators.map((val, index) => {
            let bgStyle = Colors.white
            let tetxStyle = Colors.themeBGInactive
            if (this.state.select == val) {
                bgStyle = Colors.themeBG
                tetxStyle = Colors.white
            }
            return (
                <View style={styles.segmentWrapper} key={'energy_index_' + index}>
                    <TouchableOpacity style={[styles.segmentTouch, { backgroundColor: bgStyle }]} onPress={() => {
                        this.setState({
                            select: val
                        })
                        //能源
                        this.requestEnergy(index + 1)
                    }}>
                        <Text style={{ color: tetxStyle, fontSize: 13 }}>{val}</Text>
                    </TouchableOpacity>
                </View>
            )
        })
        return (
            <View style={{ marginTop:10,paddingHorizontal:12,flexDirection:'row' }}>
                {list}
            </View>
        )
    }

    getEletricDetaiView() {
        let quantityIcon = null
        let priceIcon = null

        if(this.state.data.quantity){
            quantityIcon = <Image style={styles.sameIcon} source={require('../../images/user_manage/same.png')} />
        }
        if(this.state.data.price){
            priceIcon = <Image style={styles.upIcon} source={require('../../images/user_manage/up.png')} />
        }

        return (
            <View style={styles.headerContain}>
                <View style={styles.cardWrapper}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontSize: 30, fontWeight: 'bold' }}>{this.state.data.quantity || '0'}</Text>
                        <Text style={styles.unit}>kWh</Text>
                    </View>
                    {quantityIcon}
                    <Text style={styles.electTips}>本{this.state.select}用电量</Text>
                </View>
                <View style={styles.cardWrapper}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontSize: 15, color: Colors.themeTextBlack, marginTop: 10 }}>￥</Text>
                        <Text style={styles.price}>{this.state.data.price || '0'}</Text>
                    </View>
                    {priceIcon}
                    <Text style={styles.electTips}>总电费</Text>
                </View>
            </View>
        )
    }

    // 长线条
    getDetailLineWithLength(name, value, length, lineImg, index) {
        if(!name){
            name = ''
        }
        return (
            <View style={{ marginTop: 15, width: '100%' }} key={'line_list_' + index}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ fontSize: 11, color: Colors.themeTextLightGray }}>{name}</Text>
                    {/*<Text style={{fontSize: 11, color: Colors.themeTextLightGray}}>{value}</Text>*/}
                </View>
                <View style={{ flexDirection: 'row', width: '92%' }}>
                    <Image style={{ width: length, height: 6, borderRadius: 3, marginTop: 5, }} source={lineImg} />
                    <Text style={{ fontSize: 11, color: Colors.themeTextLightGray, marginLeft: 5 }}>{value}</Text>
                </View>
            </View>
        )
    }

    // 获取用电量的模块
    getElectricitySection(name, data, lineImg) {
        let baseQuantity = 100000

        let charts = null
        // 如果数据 为空
        if (!data || data.length <= 0) {

            charts = (
                <View style={{ paddingVertical:5, alignItems: 'center',marginTop:30}}>
                    <Text style={{color:Colors.themeBGInactive }}>暂无{name}用电数据</Text>
                </View>
            )
        } else {
            // 显示 用电量
            charts = data.map((val, index) => {
                let percent = 0
                if (index == 0) {
                    percent = '100%'
                    baseQuantity = val.quantity
                } else {
                    percent = (val.quantity / baseQuantity * 100) + "%"
                }
                return (
                    this.getDetailLineWithLength(val.floor, val.quantity, percent, lineImg, index)
                )
            })
        }

        return (
            <View style={{ marginTop: 30 }}>
                <Text style={styles.electUseTitle}>本{this.state.select}{name}用电排名</Text>
                {charts}
            </View>
        )
    }


    getChartList() {
        return (
            <View style={{ paddingHorizontal: 16, paddingBottom: 50, marginTop: 10 }}>
                {this.getElectricitySection('楼层', this.state.data.floors, require('../../images/user_manage/chart_line_floor.png'))}
                {this.getElectricitySection('房间', this.state.data.rooms, require('../../images/user_manage/chart_line_room.png'))}
                {this.getElectricitySection('电器', this.state.data.devices, require('../../images/user_manage/chart_line_device.png'))}
            </View>
        )
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                {this.getHeaderSegment()}
                {this.getEletricDetaiView()}
                {this.getChartList()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.themBGLightGray
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 16,
    },
    navRText: {
        color: Colors.tabActiveColor,
        fontSize: 15
    },
    headerContain:{ 
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20,
        paddingHorizontal: 16
    },
    segmentWrapper: {
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    segmentTouch: {
        height: 30,
        width: '100%',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardWrapper: {
        width: '48%',
        height: 120,
        backgroundColor: Colors.white,
        borderRadius: 5,
        paddingVertical: 10,
        paddingHorizontal: 12
    },
    sameIcon: {
        width: 20,
        height: 2.5,
        resizeMode: 'contain',
        position: 'absolute',
        right: 10,
        top: 15
    },
    upIcon: {
        width: 13,
        height: 18,
        resizeMode: 'contain',
        position: 'absolute',
        right: 10,
        top: 15
    },
    electTips: {
        color: Colors.themeBGInactive,
        fontSize: 15,
        marginTop: 35
    },
    price: {
        fontSize: 30,
        fontWeight: 'bold',
        color: Colors.themeTextBlack,
        marginLeft: 5
    },
    unit: {
        fontSize: 15,
        color: Colors.themeBGInactive,
        marginLeft: 5,
        marginTop: 10
    },
    electUseTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.themeTextBlack
    }
})


export default Energy

import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Dimensions,
    Modal,
    TextInput,
    DeviceEventEmitter
} from 'react-native';
import { Colors, NetUrls, NotificationKeys } from '../../common/Constants';
import HeaderLeftView from '../../common/CustomComponent/HeaderLeftView';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import LocalTokenHandler from '../../util/LocalTokenHandler';
import { postJson } from "../../util/ApiRequest";
import DateUtil from "../../util/DateUtil";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {BottomSafeMargin, StatusBarHeight} from '../../util/ScreenUtil';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';

//let china_area_json = require('../../resources/china_area.json')
const china_area_json_new = require('../../resources/china_area_new.json')
const dismissKeyboard = require('dismissKeyboard');

// 输入框 上移距离
const ExtraScrollHeight = 120

class ApplyforDebug extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'调试申请'}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text: '申请记录', onPress:()=>{
                    navigation.navigate('ApplyDebugRecord')
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const { getParam } = props.navigation
        this.userData = getParam('userData') || {}

        this.state = {
            userPhone: this.userData.phone,
            modalVisible: false,
            debugUserModal: false,
            cityData: null,
            timeData: null,
            citySelectData: [],
            timeSelectData: [],
            addressText: null,
            remarkText: null,
            pickerType: 1, // 1:日期  2:地址
            warnningType: 0,  // 0:无警告  1:详细地址  2:问题描述,
            debugUsers: null,
            selectDebugUser: null,
            selectDebugUserId: null
        };
    }

    componentDidMount() {
        this.prepareForCityData()
        this.prepareForTimeData()
        this.requsetAddress()
        if(!this.state.userPhone){
            this.requetUserInfo()
        }
    }

    componentWillUnmount(){
        
    }

    // 准备城市 数据
    prepareForCityData(){
        // let data = []
        // for (const province of china_area_json) {
        //     let cityArrs = []
        //     for (const city of province.dics) {
        //         let areaArrs = []
        //         for (const area of city.dics) {
        //             let areaName = area.label
        //             areaArrs.push(areaName)
        //         }
        //         let cityTemp = {}
        //         cityTemp[city.label] = areaArrs
        //         cityArrs.push(cityTemp)
        //     }
        //     let provinceTemp = {}
        //     provinceTemp[province.label] = cityArrs
        //     data.push(provinceTemp)
        // }


        let data = []
        for (const province of china_area_json_new) {
            let cityArrs = []
            for (const city of province.cities.city) {
                let areaArrs = []
                for (const area of city.areas.area) {
                    let areaName = area.ssqname
                    areaArrs.push(areaName)
                }
                let cityTemp = {}
                cityTemp[city.ssqname] = areaArrs
                cityArrs.push(cityTemp)
            }
            let provinceTemp = {}
            provinceTemp[province.ssqname] = cityArrs
            data.push(provinceTemp)
        }
        this.setState({
            cityData: data
        })
    }

    // 准备日期 数据
    prepareForTimeData(){
        let dd=new Date();
        let dateArr=[];
        let weekday=["周日","周一","周二","周三","周四","周五","周六"];
        for(let i=0;i<7;i++){
            dd.setDate(dd.getDate()+1);
            let myddy=dd.getDay();
            let myweek = '(' + weekday[myddy] + ')'
            dateArr.push((dd.getMonth()+1)+'月'+dd.getDate()+'日')
        }
        let timeArr = ['9:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00']
        let timeData = [dateArr,timeArr]
        this.setState({
            timeData: timeData
        })
    }

    async requsetSubmit(){
        if(!this.state.timeSelectData || this.state.timeSelectData.length<=0 ){
            ToastManager.show('请选择服务日期')
            return
        }
        if(!this.state.citySelectData || this.state.citySelectData.length<=0){
            ToastManager.show('请选择服务地址')
            return
        }
        if(!this.state.addressText){
            this.setState({warnningType: 1})
            return
        }
        if(!this.state.remarkText){
            this.setState({warnningType: 2})
            return
        }

        let cityName = this.state.citySelectData.toString()
        cityName = cityName.replace(/,/g,'')

        SpinnerManager.show()
        try {
            const { pop, replace } = this.props.navigation;
            let data = await postJson({
                url: NetUrls.debugRequest,
                params: {
                    phone: this.userData.phone,
                    address: cityName + this.state.addressText,
                    time: null,
                    timeString: this.state.timeSelectData[0]+' '+this.state.timeSelectData[1],
                    memo: this.state.remarkText,
                    debugUserId: this.state.selectDebugUserId
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                replace('ApplyDebugRecord')
                ToastManager.show('提交申请成功!');
            } else {
                ToastManager.show(data.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误');
        }
    }

    async requsetAddress(){
        SpinnerManager.show()
        try {
            const { pop, replace } = this.props.navigation;
            let data = await postJson({
                url: NetUrls.getAddress,
                params: {
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
               this.setState({
                    citySelectData: data.result?.city || [],
                    addressText: data.result?.address || '',
                    debugUsers: data.result?.debugUsers || []
               })
            } else {
                ToastManager.show(data.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误');
        }
    }

    // 获取用户信息
    async requetUserInfo() {
        try {
            let data = await postJson({
                url: NetUrls.homePage,
                params: {}
            });
            if (data.code == 0) {
                this.setState({
                    userPhone: data.result?.phone
                })
            } else {
                ToastManager.show(data.msg);
            }
        } catch (e) {
            ToastManager.show('网络错误');
        }
    }
    
    getModal() {
        if(!this.state.cityData){
            return null
        }
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    renderListModal(){
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="fade"
                transparent={true}
                visible={this.state.debugUserModal}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTopTouch} onPress={() => {
                    this.setState({
                        debugUserModal: false
                    })
                }}>
                </TouchableOpacity>
                <View style={styles.listWrapper}>
                    <View style={styles.headerWrapper}>
                        <Text style={styles.modalTitle}>选择调试员</Text>
                        <TouchableOpacity style={styles.closeTouch} onPress={()=>{
                            this.setState({
                                debugUserModal: false
                            })
                        }}>
                            <Text style={{color: Colors.tabActiveColor, fontSize: 16}}>关闭</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.clearTouch} onPress={()=>{
                            this.setState({
                                debugUserModal: false,
                                selectDebugUser: null,
                                selectDebugUserId: null
                            })
                        }}>
                            <Text style={{color: Colors.themBgRed, fontSize: 15}}>清除选择</Text>
                        </TouchableOpacity>
                    </View>
                    {this.renderDebugUserList()}
                </View>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        let pickerData = this.state.pickerType == 1 ? this.state.timeData : this.state.cityData
        window.DoubulePicker.init({
            pickerData: pickerData,
            pickerConfirmBtnText: '确定  ',
            pickerCancelBtnText: '',
            pickerTitleText: '',
            pickerCancelBtnColor: [255, 255, 255, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 18,
            pickerRowHeight:40,
            selectedValue: this.state.pickerType ==1 ? this.state.timeSelectData : this.state.citySelectData,
            onPickerConfirm: data => {
                if(this.state.pickerType == 1){
                    this.setState({
                        modalVisible: false,
                        timeSelectData: data
                    })
                }else{
                    this.setState({
                        modalVisible: false,
                        citySelectData: data
                    })
                }
                console.log(data);
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })
            },
            onPickerSelect: data => {
                
            }
        });
        window.DoubulePicker.show()
    }

    // 获取城市区域
    getPickerView(type){
        const Colors = this.props.themeInfo.colors;

        let areaText = type == 1 ? '点击选择上门服务日期(必填)':'点击选择城市区域(必填)'
        let textStyle = {fontSize:15,color:Colors.themeInactive}
        if(this.state.timeSelectData.length && (type == 1) ){
            areaText = this.state.timeSelectData[0] + ' ' + this.state.timeSelectData[1]
            textStyle.color = Colors.themeText
            if(Platform.OS == 'android'){
                textStyle.height = '100%'
                textStyle.textAlignVertical = 'center'
            }
        }
        if(this.state.citySelectData.length && (type == 2) ){
            areaText = this.state.citySelectData.toString()
            textStyle.color = Colors.themeText
            if(Platform.OS == 'android'){
                textStyle.height = '100%'
                textStyle.textAlignVertical = 'center'
            }
        }
        return(
            <TouchableOpacity activeOpacity={0.8} style={{flex:1,paddingVertical:2,justifyContent:'center'}} onPress={()=>{
                dismissKeyboard()
                this.setState({
                    pickerType: type,
                    modalVisible: true
                })
            }}>
                <Text numberOfLines={2} style={textStyle}>{areaText}</Text>
            </TouchableOpacity>
        )
    }

    // 调试人员按钮
    renderDebugUser(){
        const Colors = this.props.themeInfo.colors;
        let textStyle = {fontSize:15,color:Colors.themeInactive}

        let itemText = '点击选择调试人员(非必填)'
        if(this.state.selectDebugUser){
            itemText = this.state.selectDebugUser
            textStyle.color = Colors.themeText
            if(Platform.OS == 'android'){
                textStyle.height = '100%'
                textStyle.textAlignVertical = 'center'
            }
        }

        return(
            <TouchableOpacity style={{flex:1,paddingVertical:2,justifyContent:'center'}} onPress={()=>{
                if(!this.state.debugUsers){
                    ToastManager.show('数据加载中，请稍后。')
                    return
                }
                if(this.state.debugUsers.length <= 0){
                    ToastManager.show('暂无调试人员。')
                    return
                }
                this.setState({
                    debugUserModal: true
                })
            }}>
                <Text numberOfLines={2} style={textStyle}>{itemText}</Text>
            </TouchableOpacity>
        )
    }

    renderDebugUserList(){
        if(!this.state.debugUsers){
            return null
        }
        const reg = /^(\d{3})\d{4}(\d{4})$/;
        let list = this.state.debugUsers.map((value, index)=>{

            let tel = value.mobilePhone.replace(reg, "$1****$2");
            let selectIcon = null
            if(this.state.selectDebugUserId == value.id){
                selectIcon = <Image style={styles.selectIcon} source={require('../../images/select.png')}/>
            }

            return(
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    key={'debug_'+index} 
                    style={styles.debugItem} 
                    onPress={()=>{
                        this.setState({
                            debugUserModal: false,
                            selectDebugUser: value.realName,
                            selectDebugUserId: value.id
                        })
                    }}
                >
                    <Text style={styles.nameText}>{value.realName}</Text>
                    <Text style={styles.phoneText}>{tel}</Text>
                    {selectIcon}
                </TouchableOpacity>
            )
        })
        return(
            <ScrollView contentContainerStyle={{paddingBottom: BottomSafeMargin + 20}}>
                {list}
            </ScrollView>
        )
    }

    getAddressInputs(){
        const Colors = this.props.themeInfo.colors;
        return(
            <View style={{padding: 16}}>
                <View style={[styles.wrapper,{backgroundColor: Colors.themeBg}]}>
                    <TouchableOpacity activeOpacity={1} style={styles.selectWrapper} onPress={()=>{
                        if(!this.state.userPhone){
                            this.requetUserInfo()
                        }
                    }}>
                        <Text style={[styles.titleText,{color:Colors.themeText}]}>申请人</Text>
                        <Text style={{fontSize:15,color:Colors.themeText}}>{this.state.userPhone}</Text>
                    </TouchableOpacity>
                    <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                    <View style={styles.selectWrapper}>
                        <Text style={[styles.titleText,{color:Colors.themeText}]}>服务日期</Text>
                        {this.getPickerView(1)}
                    </View>
                    <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                    <View style={styles.selectWrapper}>
                        <Text style={[styles.titleText,{color:Colors.themeText}]}>选择调试员</Text>
                        {this.renderDebugUser()}
                    </View>
                    <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                    <View style={[styles.selectWrapper,{height:55}]}>
                        <Text style={[styles.titleText,{color:Colors.themeText}]}>服务地址</Text>
                        {this.getPickerView(2)}
                    </View>
                    <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                    <View style={styles.selectWrapper}>
                        <TextInput
						    style={[styles.input,{color: Colors.themeText}]}
						    placeholder={'请输入详细地址'}
						    placeholderTextColor={Colors.themeInactive}
						    defaultValue={this.state.addressText}
                            onFocus={()=>{
                                this.setState({
                                    warnningType: 0
                                })
                            }}
						    onChangeText={(text) => {
							    this.state.addressText = text
						    }}
						    returnKeyType={'done'}
					    />
                    </View>
                </View>
                {this.state.warnningType == 1 ? <Text style={styles.warrningText}>详细地址为必填项</Text>:null}
            </View>
        )
    }

    getRemarksInput(){
        const Colors = this.props.themeInfo.colors;
        return(
            <View style={{marginTop:5,paddingVertical:5,paddingHorizontal: 16}}>
                <View style={[styles.inputWrapper,{backgroundColor: Colors.themeBg}]}>
                    <TextInput
						style={[styles.remarkInput,{color: Colors.themeText}]}
						placeholder={'请简单叙述遇到的问题(200字以内)'}
						placeholderTextColor={Colors.themeInactive}
						multiline={true}
                        blurOnSubmit={false}
                        autoFocus={false}
                        returnKeyLabel={'换行'}
                        returnKeyType={'default'}
                        maxLength={200}
                        underlineColorAndroid="transparent"
                        onFocus={()=>{
                            this.setState({
                                warnningType: 0
                            })
                        }}
						onChangeText={(text) => {
							this.state.remarkText = text
						}}
					/>
                </View>
                {this.state.warnningType == 2 ? <Text style={styles.warrningText}>问题描述为必填项</Text>:null}
            </View>
        )
    }

    getSubmitBtn(){
        return(
            <View style={{padding: 16,marginTop:10}}>
                <TouchableOpacity activeOpacity={0.8} style={styles.btnTouch} onPress={()=>{
                    dismissKeyboard()
                    this.requsetSubmit()
                }}>
                    <Text style={{color:Colors.white}}>提交申请</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors;

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                <KeyboardAwareScrollView
                    enableOnAndroid={true}
                    extraScrollHeight={ExtraScrollHeight}                   
                    style={styles.keyboardWrapper}
                    keyboardShouldPersistTaps="handled"
                >
                    {this.getAddressInputs()}
                    {this.getRemarksInput()}
                    {this.getSubmitBtn()}
                </KeyboardAwareScrollView>
                {this.getModal()}
                {this.renderListModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    keyboardWrapper: {
        flex: 1,
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 16,
    },
    navRText: {
        color: Colors.tabActiveColor,
        fontSize: 15
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    wrapper:{
        width: '100%',
        borderRadius: 5,
        paddingHorizontal: 16,
        paddingVertical: 5
    },
    inputWrapper:{
        width: '100%',
        borderRadius: 5,
        paddingVertical: 5
    },
    line:{
        width: '100%',
        height: 1
    },
    titleText:{
        width: '30%',
        fontSize:15
    },
    selectWrapper:{
        height:50,
        flexDirection: 'row',
        alignItems:'center'
    },
    input: {
		flex: 1,
		height: 40,
		marginLeft: 5,
        fontSize:15
    },
    remarkInput:{
		width:'100%',
		height:80,
		paddingHorizontal:16,
      	textAlignVertical:'top',
		fontSize:14,
      	borderRadius:4,
    },
    btnTouch:{
        width:'100%',
        height:40,
        backgroundColor:Colors.tabActiveColor,
        borderRadius: 4,
        justifyContent:'center',
        alignItems:'center'
    },
    warrningText:{
        marginTop: 10, 
        fontSize: 14, 
        color: Colors.themBgRed, 
        marginLeft: 5
    },
    modalTopTouch:{
        flex: 3,
        backgroundColor: Colors.translucent
    },
    listWrapper:{
        flex: 7,
        backgroundColor: Colors.themBGLightGray
    },
    closeTouch:{
        position:'absolute',
        left: 0,
        top:0,
        height:'100%',
        paddingHorizontal:20,
        justifyContent:'center',
        alignItems: 'center'
    },
    clearTouch:{
        position:'absolute',
        right: 0,
        top:0,
        height:'100%',
        paddingHorizontal:20,
        justifyContent:'center',
        alignItems: 'center'
    },
    headerWrapper:{
        height: 48,
        width:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    modalTitle:{
        color: Colors.themeTextBlack, 
        fontSize: 18, 
        fontWeight:'bold'
    },
    debugItem:{
        marginHorizontal: 16,
        backgroundColor: Colors.white,
        marginTop: 15,
        paddingHorizontal: 20,
        height: 60,
        borderRadius: 4,
        justifyContent: 'center'
    },
    nameText:{
        fontSize: 16,
        color: Colors.themeTextBlack
    },
    phoneText:{
        fontSize: 14, 
        color: Colors.themeTextLightGray,
        marginTop: 8
    },
    selectIcon:{
        position: 'absolute',
        width: 20,
        height: 20,
        right: 20,
        top: 20,
        resizeMode: 'contain'
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ApplyforDebug)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component, useMemo } from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    Linking, 
    Alert,
    DeviceEventEmitter,
    Platform,
    StatusBar,
    RefreshControl,
    NativeModules
} from 'react-native';
import { connect } from 'react-redux';
import { NetUrls, NotificationKeys, LocalStorageKeys,Colors } from '../../common/Constants';
import ToastManager from "../../common/CustomComponent/ToastManager";
import { StatusBarHeight, NavigationBarHeight } from '../../util/ScreenUtil';
import { postJson } from "../../util/ApiRequest";
import ActionSheet from 'react-native-actionsheet';
import { updateAppTheme } from '../../redux/actions/ThemeAction';
import { updateUserInfo } from '../../redux/actions/UserInfoAction';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';

const JKUMengShare = Platform.select({
    ios: NativeModules.UMShareModule,
    android: NativeModules.UMShareModule
})

class User extends Component {

    static navigationOptions = {
        header: null,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    };

    constructor(props) {
        super(props);
        this.state = {
            isHouseHolder: 0, //1 是户主
            notifiNumber: 0,
            memberType: null,
            isRefreshing: false
        }
    }

    componentDidMount() {

    }

    componentWillUnmount(){
        
    }

    async requetUserInfo() {
        try {
            let data = await postJson({
                url: NetUrls.homePage,
                params: {}
            });
            this.setState({isRefreshing: false})
            if (data.code == 0) {
                let userInfo = data.result || {}
                this.props.updateUserInfo(userInfo)
            } else {
                ToastManager.show(data.msg);
            }
        } catch (e) {
            this.setState({isRefreshing: false})
            ToastManager.show('网络错误');
        }
    }

    call = phone => {
        const url = `tel:${phone}`;
        Linking.canOpenURL(url)
            .then(supported => {
                if (!supported) {
                    return Alert.alert('提示', `您的设备不支持该功能，请手动拨打 ${phone}`, [
                        { text: '确定' }
                    ]);
                }
                return Linking.openURL(url);
            })
            .catch(err => Toast.info(`出错了：${err}`, 1.5));
    }

    callMerchant = () => {
        let servicePhone = this.props.userInfo.servicePhone
        if(!servicePhone){
            Alert.alert('提示', 
                '暂未获取到客服电话，请稍后再试', 
                [{ text: '确定' }]
            )
            return
        }
        this.call(servicePhone);
    }

    onRefresh(){
        if (this.state.isRefreshing) return
        this.setState({ isRefreshing: true })
        this.requetUserInfo()
    }

    bindWechat(isBindWechat){
        const { navigate } = this.props.navigation
        navigate('WechatBind',{userData: this.props.userInfo})
        return
        
        if(isBindWechat === null){
            ToastManager.show('数据加载中，请稍后')
            return
        }
        if(isBindWechat == 1){
            const { navigate } = this.props.navigation
            navigate('WechatBind',{userData: this.props.userInfo})
            return
        }
        // type: 2-微信
        JKUMengShare.auth(2,(code,res,msg)=>{
            if(code == 200){
                
            }else{
                ToastManager.show(msg)
            }
        })
    }

    // 我的页面头部
    getUserHeader() {
        const { navigate } = this.props.navigation
        const Colors = this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}

        let notifiBadge = null
        if (this.props.userInfo.unreadMessage){
            notifiBadge = (
                <View style={[styles.badge, {backgroundColor: Colors.red}]}>
                    {/* <Text style={{color: Colors.white, fontSize: 10}}>{this.state.notifiNumber}</Text> */}
                </View>
            )
        }

        return (
            <View style={{backgroundColor: Colors.themeBg}}>
                <View style={styles.status} />
                <View style={styles.nav}>
                    <Text style={{ fontSize: 24, fontWeight: 'bold', color: Colors.themeText, flex: 1 }}>我的</Text>
                    <TouchableOpacity style={styles.topBottomWrapper} onPress={() => {
                        navigate('Setting',{userData: this.props.userInfo,callBack:()=>{
                            this.props.updateUserInfo()
                        }})
                    }}>
                        <Image style={[styles.settingIcon,tintColor]} source={require('../../images/user_manage/setting.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.topBottomWrapper} onPress={() => {
                        navigate('Message',{callBack:()=>{
                            this.props.updateUserInfo()
                        }})
                    }}>
                        <Image style={[styles.settingIcon,tintColor]} source={require('../../images/notification.png')} />
                        {notifiBadge}
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    
    // 用户卡片
    getUserCard() {
        const {navigate} = this.props.navigation
        const Colors = this.props.themeInfo.colors
        const isHouseHolder = this.props.userInfo.isHouseHolder

        let hostText = null
        if (isHouseHolder == 1) {
            hostText = '户主'
        }else{
            hostText = this.props.userInfo.memberType == 1 ? '管理员':'普通成员'
        }

        let userName = this.props.userInfo.phone || ''
        if(this.props.userInfo.username){
            userName = this.props.userInfo.username
        }

        let userIcon = require('../../images/header_pic.png')
        if(this.props.userInfo.photo){
            userIcon = { uri: this.props.userInfo.photo }
        }

        return (
            <View style={[styles.userHeader, {backgroundColor: Colors.themeBg}]}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.userIconTouch, {backgroundColor:Colors.themeBaseBg}]} 
                    onPress={()=>{
                        navigate('UserSetting',{userData: this.props.userInfo,callBack:()=>{
                            this.props.updateUserInfo()
                        }})
                    }}
                >
                    <Image style={styles.userIcon} source={userIcon} />
                </TouchableOpacity >
                <TouchableOpacity activeOpacity={0.7} style={styles.cardBottom} onPress={()=>{
                    navigate('UserSetting',{userData: this.props.userInfo,callBack:()=>{
                        this.props.updateUserInfo()
                    }})
                }}>
                    <View style={[styles.hostIcon, isHouseHolder ? {backgroundColor: Colors.yellow}:{backgroundColor:Colors.themeButton} ]}>
                        <Text style={[styles.holderText, {color: Colors.themeBg}]}>{hostText}</Text>
                    </View>
                    <Text style={[styles.userName,{color: Colors.themeText}]}>{userName+'  '} </Text>
                </TouchableOpacity>
            </View>
        )
    }

    //绑定手机 提示
    getBindPhoneTips(){
        const Colors = this.props.themeInfo.colors
        const {navigate} = this.props.navigation

        if(!this.props.userInfo || !this.props.userInfo.phone){
            return null
        }
        if(!this.props.userInfo.phone.includes('apple')){
            return null
        }

        return(
            <TouchableOpacity
                activeOpacity={0.7}
                style={[styles.bindTipsWrapper,{backgroundColor:Colors.yellow}]}
                onPress={()=>{
                    navigate('BindPhone',{isChangePhone: true})
                }}
            >
                <Text style={styles.tipsText} numberOfLines={2}>您的账号暂未绑定手机，为了方便下次登，去绑定。</Text>
                <Image style={styles.tipsRightArrow} source={require('../../images/enterLight.png')} />
            </TouchableOpacity>
        )
    }

    //常用功能 Item
    getCustomItem(img, title, toPage, callBack, extra) {
        const { navigate } = this.props.navigation
        const Colors = this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}

        let extraText = null
        if(extra){
            extraText = <Text style={[styles.extraText, {color: Colors.themeTextLight}]}>{extra}</Text>
        }

        return (
            <TouchableOpacity activeOpacity={0.7} style={styles.customItemTouch} onPress={() => {
                if(callBack){
                    callBack()
                }else{
                    navigate(toPage)
                }
            }}>
                <Image style={[{ width: 20, height: 20, resizeMode: 'contain' },tintColor]} source={img} />
                <Text style={[styles.itemText, {color: Colors.themeText}]}>{title}</Text>
                {extraText}
                <Image style={{ width: 8, height: 12, marginRight: 5 }} source={require('../../images/enterLight.png')} />
            </TouchableOpacity>
        )
    }

    //常用功能列表
    getCustom() {
        const Colors = this.props.themeInfo.colors
        const {navigate} = this.props.navigation

        const isHouseHolder = this.props.userInfo?.isHouseHolder
        const memberType = this.props.userInfo?.memberType
        const showDeviceManage = isHouseHolder || (memberType == 1)
        const isBindWechat = this.props.userInfo?.bindWechat
        
        const split = <View style={[styles.split, {backgroundColor:Colors.themeBaseBg}]}/>
        const line = <View style={[styles.line, {backgroundColor:Colors.split}]}/>

        if(showDeviceManage){
            return(
                <View style={{ marginTop: 10,backgroundColor: Colors.themeBg }}>
                    {this.getCustomItem(require('../../images/user_manage/devices.png'), '设备管理', 'DeviceManage')}
                    {line}
                    {this.getCustomItem(require('../../images/user_manage/members.png'), '成员管理', '', ()=>{
                        navigate('Member',{isHouseHolder: isHouseHolder})
                    })}
                    {line}
                    {this.getCustomItem(require('../../images/user_manage/timeLine.png'), '定时管理', '', ()=>{
                        navigate('TimingManager')
                    })}
                    {line}
                    {this.getCustomItem(require('../../images/user_manage/layout.png'), '房屋布线', '',()=>{ 
                        navigate('HouseLayout',{id: this.props.userInfo.houseId}) } 
                    )}
                    {line}
                    {this.getCustomItem(require('../../images/user_manage/enviroment.png'), '环境监测', '',()=>{ 
                        navigate('EnvironmentObservation', { houseId: this.props.userInfo.houseId, title: '环境监测' }) } 
                    )}
                    {split}
                    {this.getCustomItem(require('../../images/user_manage/laboratory.png'), '实验室功能','',()=>{
                        navigate('LaboratoryList')}
                    )}
                    {line}
                    {this.getCustomItem(require('../../images/user_manage/tcp.png'), 'TCP模式', '',()=>{ 
                        navigate('TCPSetting') } 
                    )}
                    {/* {line}
                    {this.getCustomItem(require('../../images/user_manage/tcp.png'), 'UDP模式', '',()=>{ 
                        // navigate('UDP')
                        navigate('WiFiPassword',{
                            connectWifiType: 2   // 2. AP配网
                        }) 
                    } )} */}
                    {split}
                    {this.getCustomItem(require('../../images/user_manage/hotline.png'), '400热线', '', ()=>{ 
                        this.callMerchant() } 
                    )}
                    {line}
                    {this.getCustomItem(require('../../images/user_manage/debug.png'), '调试申请', '',()=>{
                        if(this.props.userInfo.phone && this.props.userInfo.phone.includes('apple')){
                            navigate('BindPhone',{isChangePhone: true})
                            return
                        }
                        navigate('ApplyforDebug',{userData: this.props.userInfo}) } 
                    )}
                    {line}
                    {/* {this.getCustomItem(require('../../images/user_manage/question.png'), '常见问题', '',()=>{ 
                        navigate('Questions',{userData: this.props.userInfo, title: '常见问题'}) } 
                    )}
                    {line} */}
                    {this.getCustomItem(require('../../images/user_manage/aboutus.png'), '关于我们', '',()=>{ 
                        navigate('AboutUs',{userData: this.props.userInfo, title: '关于我们'}) } 
                    )}
                    {split}
                    {this.getCustomItem(require('../../images/user_manage/wechat.png'), '微信绑定', '',()=>{ 
                        this.bindWechat(isBindWechat)
                    })}
                </View>
            )
        }else{
            return(
                <View style={{ marginTop: 10,backgroundColor: Colors.themeBg }}>
                    {this.getCustomItem(require('../../images/user_manage/timeLine.png'), '定时管理', '', ()=>{
                        navigate('TimingManager')
                    })}
                    {line}
                    {this.getCustomItem(require('../../images/user_manage/enviroment.png'), '环境监测', '',()=>{ 
                        navigate('EnvironmentObservation', { houseId: this.props.userInfo.houseId, title: '环境监测' }) } 
                    )}
                    {split}
                    {this.getCustomItem(require('../../images/user_manage/laboratory.png'), '实验室功能','',()=>{
                        navigate('LaboratoryList')}
                    )}
                    {line}
                    {this.getCustomItem(require('../../images/user_manage/tcp.png'), 'TCP模式', '',()=>{ 
                        navigate('TCPSetting') 
                    } )}
                    {/* {line}
                    {this.getCustomItem(require('../../images/user_manage/tcp.png'), 'UDP模式', '',()=>{ 
                        // navigate('UDP') 
                        navigate('WiFiPassword',{
                            connectWifiType: 2   // 2. AP配网
                        }) 
                    } )} */}
                    {split}
                    {this.getCustomItem(require('../../images/user_manage/hotline.png'), '400热线', '', ()=>{ 
                        this.callMerchant() } 
                    )}
                    {line}
                    {this.getCustomItem(require('../../images/user_manage/debug.png'), '调试申请', '',()=>{
                        if(this.props.userInfo.phone && this.props.userInfo.phone.includes('apple')){
                            navigate('BindPhone',{isChangePhone: true})
                            return
                        }
                        navigate('ApplyforDebug',{userData: this.props.userInfo}) } 
                    )}
                    {line}
                    {/* {this.getCustomItem(require('../../images/user_manage/question.png'), '常见问题', '',()=>{ 
                        navigate('Questions',{userData: this.props.userInfo, title: '常见问题'}) }  
                    )}
                    {line} */}
                    {this.getCustomItem(require('../../images/user_manage/aboutus.png'), '关于我们', '',()=>{ 
                        navigate('AboutUs',{userData: this.props.userInfo, title: '关于我们'}) }
                    )}
                    {split}
                    {this.getCustomItem(require('../../images/user_manage/wechat.png'), '微信绑定', '',()=>{ 
                        this.bindWechat(isBindWechat)
                    })}
                </View>
            )
        }
    }

    render() {
        const Colors = this.props.themeInfo.colors
        return (
            <View style={{backgroundColor: Colors.themeBg, flex: 1}}>
                {this.getUserHeader()}
                <ScrollView 
                    style={{backgroundColor: Colors.themeBaseBg}} 
                    contentContainerStyle={styles.scrollContent}
                    refreshControl={
                        <RefreshControl
                            colors={[Colors.activeIndiColor]}
                            tintColor={Colors.activeIndiColor}
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                >
                    {this.getUserCard()}
                    {this.getBindPhoneTips()}
                    {this.getCustom()}
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    scrollContent:{
        paddingBottom: 50,   
    },
    status: {
        height: StatusBarHeight,
        width: '100%',
    },
    userHeader:{ 
        paddingTop: 10,
        paddingBottom: 20,
        alignItems: 'center',
        width:'100%',
    },
    nav: {
        height: NavigationBarHeight,
        width: '100%',
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 16
    },
    hostIcon: {
        marginRight: 14,
        paddingHorizontal: 12,
        height: 24,
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    customItemTouch: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
        marginVertical:5,
        paddingHorizontal: 20,
    },
    badge: {
        width: 8,
        height: 8,
        position: 'absolute',
        top: Platform.select({
            ios: 10,
            android: 15
        }),
        right: 14,
        borderRadius: 4
    },
    myhouseTouch:{
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'space-between',
        paddingHorizontal: 10,
        height:30,
        borderRadius:15,
        borderWidth: 1,
        marginRight:20,
        minWidth:80
    },
    myhouseText:{ 
        fontSize: 14,
        marginRight: 10
    },
    settingIcon:{ 
        width: 22, 
        height: 22, 
        resizeMode: 'contain' 
    },
    userIconTouch:{
        width: 80, 
        height: 80,
        borderRadius: 40,
        overflow:'hidden',
    },
    holderText:{ 
        fontSize: 14,
    },
    line:{
        width:'100%',
        height:1,
        marginLeft: 50
    },
    split:{
        width:'100%',
        height:10,
    },
    itemText: {
        flex: 1, 
        marginLeft: 15, 
        fontSize: 15, 
        fontWeight:'bold',
    },
    topBottomWrapper:{ 
        paddingHorizontal: 16, 
        marginLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height:'100%' 
    },
    extraText:{
        fontSize: 13,
        marginRight: 15
    },
    bindTipsWrapper:{
        width:'100%',
        height:40,
        alignItems:'center',
        paddingHorizontal:16,
        flexDirection: 'row'
    },
    tipsRightArrow:{ 
        width: 8, 
        height: 12, 
        marginRight: 5,
        tintColor: Colors.themeTextBlack
    },
    tipsText:{
        color: Colors.themeTextBlack, 
        fontSize: 13,
        lineHeight: 15,
        marginRight: 15,
        flex: 1
    },
    userIcon:{ 
        width: 80, 
        height: 80,
        resizeMode:'contain' 
    },
    cardBottom:{ 
        flexDirection: 'row', 
        alignItems: 'center', 
        marginTop: 14
    },
    userName:{
        fontSize: 18, 
        fontWeight: '500',
    }
});


export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
        userInfo: state.userInfo
    }),
    (dispatch) => ({
        updateAppTheme: (isDark)=> dispatch(updateAppTheme(isDark)),
        updateUserInfo: (userInfo)=> dispatch(updateUserInfo(userInfo))
    })
)(User)

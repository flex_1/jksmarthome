/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    br
} from 'react-native';
import {Colors} from '../../common/Constants';
import {StatusBarHeight} from '../../util/ScreenUtil';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import { connect } from 'react-redux';

class TransferWarrning extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'过户须知'}/>,
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props)
    }

    onContent() {
        const Colors = this.props.themeInfo.colors;
        return (
            <View style={{ paddingLeft: 30,width:'100%'}}>
                <Image style={styles.imageStyle} source={require('../../images/user_manage/transfer_icon.png')}/>
                <Text style={[styles.title,{color: Colors.themeText}]}>过户你的房产到其他翰萨科技账号</Text>
                <Text style={[styles.textStyle,{color: Colors.themeButton}]}>此功能不可逆，请确保：</Text>
                <Text style={[styles.textStyle,{color: Colors.themeTextLight}]}>1 接收方手机号为11位手机号码</Text>
                <Text style={[styles.textStyle,{color: Colors.themeTextLight}]}>2 账号为翰萨科技的有效账号</Text>
                <Text style={[styles.textStyle,{color: Colors.themeTextLight}]}>3 接收方账号不可与过户方账号为同一个</Text>
            </View>
        )
    }

    onSureBtn() {
        const {navigate} = this.props.navigation
        return (
            <View style={{width: '100%', alignItems: 'center'}}>
                <TouchableOpacity activeOpacity={0.7} style={styles.addMemberStyle} onPress={() => {
                    navigate('Transfer');
                }}>
                    <Text style={{fontSize: 15, color: Colors.white}}>过户房产到其他账号</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors;
        return (
            <ScrollView style={{backgroundColor: Colors.themeBaseBg}}>
                {this.onContent()}
                {this.onSureBtn()}
            </ScrollView>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    imageStyle: {
        alignSelf: 'center',
        width: '70%',
        resizeMode: 'contain'
    },
    title:{
        fontSize: 17,
        color: Colors.themeTextBlack,
        marginBottom: 10
    },
    textStyle: {
        fontSize: 16,
        marginTop: 10
    },
    addMemberStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 44,
        width: '90%',
        marginTop: 30,
        backgroundColor: Colors.tabActiveColor,
        borderRadius: 4,
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(TransferWarrning)

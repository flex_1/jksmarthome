/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity
} from 'react-native';
import { WebView } from 'react-native-webview';
import { NetUrls,Colors } from '../../common/Constants';
import {StatusBarHeight,BottomSafeMargin} from '../../util/ScreenUtil';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';

class Questions extends Component {
    
    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} onClick={()=>{ navigation.getParam('goBackWeb')() }}/>,
        headerTitle: <HeaderTitle title={navigation.getParam('title')}/>,
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const {getParam,setParams,goBack} = this.props.navigation

        this.userData = getParam('userData') || {}
        this.state = {
            canGoback: false
        }

        setParams({
            goBackWeb: ()=>{
                if(this.state.canGoback){
                    this.questionWebview && this.questionWebview.goBack()
                }else{
                    goBack()
                }
            }
        });
    }

    componentDidMount() {
        
    }

    componentWillUnmount() {
        
    }

    onNavigationStateChange = (navState)=>{
        this.setState({
            canGoback: navState.canGoBack
        })
    }

    getWebview(){
        const url = NetUrls.questions + '?id=' + this.userData.phone
        
        return(
            <WebView
                ref={e => this.questionWebview = e}
                source={{ uri: url }}
                style={styles.container}
                containerStyle={styles.scorllContent}
                startInLoadingState={true}
                showsVerticalScrollIndicator={false}
                allowsInlineMediaPlayback={true}
                onNavigationStateChange={this.onNavigationStateChange}
            />
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.getWebview()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scorllContent:{
        marginTop: 10,
        paddingHorizontal: 16,
        paddingBottom: BottomSafeMargin
    },
    navTouch:{ 
        paddingLeft: 15,
        paddingRight:20,
        flexDirection:'row', 
        alignItems: 'center',
        height:'100%' 
    },
    navImg:{ 
        width: 10, 
        height: 18,
        resizeMode:'contain' 
    }
});

export default Questions

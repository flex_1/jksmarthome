import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Text,
	View,
	ScrollView,
	Image,
	TouchableOpacity,
	Alert,
	TextInput,
	DeviceEventEmitter,
    Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import { Colors, NetUrls } from '../../common/Constants';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import { postJson } from '../../util/ApiRequest';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';

class PriceSetting extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'电价设置'}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text: '保存', onPress:()=>{
                    navigation.getParam('saveBtnClick')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground/>
    })

	constructor(props) {
		super(props);
		const { getParam,setParams } = props.navigation
        this.callBack = getParam('callBack')
        this.deviceData = getParam('deviceData') || {}

		this.state = {
			select: 0,
			maxPrice: null,
			minPrice: null,
			max_beginTime: '07:00',
			max_endTime: '23:00',
			min_beginTime: '23:00',
			min_endTime: '07:00',
		};

        setParams({
            saveBtnClick: this.requestPriceSetting.bind(this)
        })
	}

	componentDidMount() {
		this.requestPrice()
	}

	componentWillUnmount() {
		
	}

	// 获取电价
	async requestPrice() {
		SpinnerManager.show()
        try {
            const { pop } = this.props.navigation;
            let data = await postJson({
				url: NetUrls.getElectricPrice,
                params: {
					id: this.deviceData?.id
				}
			});
			SpinnerManager.close()
            if (data.code == 0) {
				this.setState({
					minPrice: data?.result?.minPrice?.toString(),
					maxPrice: data?.result?.maxPrice?.toString(),
				})
            } else {
                ToastManager.show(data.msg);
            }
        } catch (e) {
			SpinnerManager.close()
            ToastManager.show('网络错误');
        }
    }

	// 设置电价
	async requestPriceSetting() {
        Keyboard.dismiss()
		if(!this.state.maxPrice){
			ToastManager.show('请输入峰值电价');
			return
		}
		if(!this.state.minPrice){
			ToastManager.show('请输入谷值电价');
			return
		}
		SpinnerManager.show()
        try {
            const { pop } = this.props.navigation;
            let data = await postJson({
                url: NetUrls.elecPriceSetting,
                params: {
                    id: this.deviceData?.id,
					maxPrice: parseFloat(this.state.maxPrice),
					minPrice: parseFloat(this.state.minPrice)
				}
			});
			SpinnerManager.close()
            if (data.code == 0) {
				ToastManager.show('电价设置成功');
				pop()
            } else {
                ToastManager.show(data.msg);
            }
        } catch (e) {
			SpinnerManager.close()
            ToastManager.show('网络错误');
        }
    }

    renderTimes(isHigh){
        const Colors = this.props.themeInfo.colors;
        let beginTime = isHigh ? this.state.max_beginTime : this.state.max_endTime
        let endTime = isHigh ? this.state.min_beginTime : this.state.min_endTime

        return(
            <View style={styles.timeWrapper}>
				<View style={styles.timeTouch}>
					<Text style={[styles.timeNumber,{color: Colors.themeText}]}>{beginTime}</Text>
					<Text style={[styles.timeTitle,{color: Colors.themeTextLight}]}>开始时间</Text>
				</View>
				<View style={styles.lineVertical} />
				<View style={styles.timeTouch}>
					<Text style={[styles.timeNumber,{color: Colors.themeText}]}>{endTime}</Text>
					<Text style={[styles.timeTitle,{color: Colors.themeTextLight}]}>结束时间</Text>
				</View>
			</View>
        )
    }

	getFluctuatePrice() {
        const Colors = this.props.themeInfo.colors;

		return (
			<View style={{ marginTop: 10, paddingVertical: 5, paddingHorizontal: 16 }}>
				<View style={[styles.sectionWrapper,{backgroundColor: Colors.themeBg}]}>
					<View style={{ flexDirection: 'row', alignItems: 'center' }}>
						<Text style={{ fontSize: 16, color: Colors.themeText, fontWeight: 'bold' }}>峰值电价</Text>
					</View>
					<View style={styles.fluctWrapper}>
						<Text style={{ fontSize: 15, color: Colors.themeText}}>电价（￥/ kWh）</Text>
						<TextInput
							style={[styles.fluctInput,this.state.maxPrice?{fontSize:18,fontWeight:'bold'}:{fontSize:15,fontWeight:'normal'}]}
							placeholder={'请输入电价'}
							placeholderTextColor={Colors.themeInactive}
                            maxLength={5}
							keyboardType={'numeric'}
							defaultValue={this.state.maxPrice}
							onChangeText={(text) => {
								this.setState({
									maxPrice: text
								})
							}}
							returnKeyType={'done'}
						/>
						<Text>元</Text>
					</View>
					<View style={styles.line} />
					{this.renderTimes(1)}
				</View>

				<View style={[styles.sectionWrapper,{marginTop:30,backgroundColor: Colors.themeBg}]}>
					<View style={{ flexDirection: 'row', alignItems: 'center'}}>
						<Text style={{ fontSize: 16, color: Colors.themeText, fontWeight: 'bold' }}>谷值电价</Text>
					</View>
					<View style={styles.fluctWrapper}>
						<Text style={{ fontSize: 15, color: Colors.themeText}}>电价（￥/ kWh）</Text>
						<TextInput
							style={[styles.fluctInput,this.state.minPrice?{fontSize:18,fontWeight:'bold'}:{fontWeight:'normal'}]}
							placeholder={'请输入电价'}
							placeholderTextColor={Colors.themeBGInactive}
                            maxLength={5}
							keyboardType={'numeric'}
							defaultValue={this.state.minPrice}
							onChangeText={(text) => {
								this.setState({
									minPrice: text
								})
							}}
							returnKeyType={'done'}
						/>
						<Text>元</Text>
					</View>
					<View style={styles.line} />
					{this.renderTimes(0)}
				</View>
			</View>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				<KeyboardAwareScrollView behavior="padding" style={styles.keyboardWrapper} contentContainerStyle={styles.contentStyle}>
					{this.getFluctuatePrice()}
				</KeyboardAwareScrollView>
			</View >
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex:1
	},
	keyboardWrapper:{
		flex:1
	},
	contentStyle:{
		paddingBottom:60
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 16,
	},
	navRText: {
		color: Colors.tabActiveColor,
		fontSize: 15
	},
	hederWrapper: {
		paddingHorizontal: 16,
		paddingBottom:10,
		marginTop: 10,
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	segementTouch: {
		width: '48%',
		height: 35,
		borderRadius: 4,
		backgroundColor: Colors.white,
		justifyContent: 'center',
		alignItems: 'center'
	},
	segementText: {
		fontSize: 14,
		color: Colors.themeTextBlack
	},
	inputWrapper: {
		width: '100%',
		height: 55,
		backgroundColor: Colors.white,
		borderRadius: 5,
		alignItems: 'center',
		flexDirection: 'row'
	},
	sectionWrapper: {
		width: '100%',
		borderRadius: 5,
		paddingVertical: 10,
		paddingHorizontal: 16
	},
	fluctWrapper: {
		width: '100%',
		marginTop: 20,
		alignItems: 'center',
		flexDirection: 'row',
	},
	fluctInput:{
		flex:1,
		marginLeft: 5,
		textAlign: 'right',
		paddingRight: 10,
		color: Colors.tabActiveColor,
		fontSize: 15,
		paddingVertical:10,
		fontWeight: 'normal',
		lineHeight: 20
	},
	line: {
		width: '100%',
		height: 1,
		marginTop: 10,
		backgroundColor: Colors.splitLightGray
	},
	timeWrapper: {
		marginTop: 15,
		flexDirection: 'row',
		alignItems: 'center',
		flex: 1
	},
	lineVertical: {
		width: 1,
		height: 20,
		backgroundColor: Colors.splitLightGray
	},
	timeTouch: {
		flex: 1,
		height: '100%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	bottomBtn:{
		backgroundColor:Colors.tabActiveColor,
		height:40,
		justifyContent:'center',
		alignItems:'center',
		borderRadius:4
	},
	timeNumber:{
		fontSize:15
	},
	timeTitle:{
		marginTop: 5,
		fontSize: 12
	}
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(PriceSetting)

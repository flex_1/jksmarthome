import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity,
    DeviceEventEmitter,
    FlatList
} from 'react-native';
import { connect } from 'react-redux';
import { NotificationKeys,CurrentState, NetUrls, LocalStorageKeys } from '../../common/Constants';
import { postJson } from "../../util/ApiRequest";
import SpinnerManager from '../../common/CustomComponent/SpinnerManager';
import ToastManager from "../../common/CustomComponent/ToastManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import DateUtil from '../../util/DateUtil';
import deviceInfo from '../../util/DeviceInfo';

class AccountLoginRecord extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={'登录设备管理'}/>,
        headerBackground: <HeaderBackground/>
    })

	constructor(props) {
		super(props);
        const { getParam } = props.navigation

        this.deviceId = deviceInfo.uniqueId
        this.state={
            loginData: null
        }
    }

    componentDidMount(){
        this.requestDeviceInfo()
    }

    //获取
    async requestDeviceInfo() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.loginRecord,
				params: {
					
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
				this.setState({
                    loginData: data.result || []
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    renderStatusView(rowData,index){
        const Colors = this.props.themeInfo.colors;

        if(this.deviceId == rowData.phoneId){
            return (
                <View style={[styles.statusWrapper,{backgroundColor: Colors.themeButton}]}>
                    <Text style={{fontSize: 12, color: Colors.white}}>本机</Text>
                </View>
            )
        }else if(rowData.isOnline){
            return (
                <Text style={{fontSize: 13,color: Colors.lightGreen}}>在线</Text>
            )
        }else{
            return (
                <Text style={{fontSize: 13,color: Colors.themeTextLight}}>离线</Text>
            )
        }
    }

    renderRowItem(rowData, index) {
        if(index != 0  && this.deviceId == rowData.phoneId){
            return null
        }
        const Colors = this.props.themeInfo.colors;
        const date = DateUtil(rowData.loginTimespan)
        const modifyTime = DateUtil(rowData.modifyTime)
        const deviceName = rowData.deviceName
        const device = rowData.phoneBrand + '  ' + rowData.phoneModel
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}

        return(
            <View style={[styles.itemWrapper,{backgroundColor: Colors.themeBg}]}>
                <View style={styles.item}>
                    <Text style={[styles.titleName,{color: Colors.themeTextLight}]}>最近登录时间：</Text>
                    <Text style={[styles.timeText,{color: Colors.themeText}]}>{date}</Text>
                    <View style={{flex: 1}}/>
                    {this.renderStatusView(rowData,index)}
                </View>
                <View style={styles.item}>
                    <Text style={[styles.titleName,{color: Colors.themeTextLight}]}>最后操作时间：</Text>
                    <Text style={[styles.timeText,{color: Colors.themeText}]}>{modifyTime}</Text>
                </View>
                <View style={styles.item}>
                    <Image style={[styles.icon,tintColor]} source={require('../../images/user_manage/account.png')}/>
                    <Text style={[styles.phoneText,{color: Colors.themeTextLight}]}>{deviceName}</Text>
                </View>
                <View style={styles.item}>
                    <Image style={[styles.icon,tintColor]} source={require('../../images/user_manage/phoneBrand.png')}/>
                    <Text style={[styles.phoneText,{color: Colors.themeTextLight}]}>{device}</Text>
                </View>
            </View>
        )
    }

    _extraUniqueKey(item, index) {
		return "device_index_" + index;
	}

    renderList(){
        let loginData = this.state.loginData

        if(!loginData){
            return null
        }

        return(
            <FlatList
                scrollIndicatorInsets={{right: 1}}
                contentContainerStyle={{paddingBottom: 50}}
                data={loginData}
                keyExtractor={this._extraUniqueKey}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
                initialNumToRender={10}
            /> 
        )
    }
	
	render() {
        const Colors = this.props.themeInfo.colors
		return (
			<View style={{backgroundColor: Colors.themeBaseBg, flex: 1}}>
				{this.renderList()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	itemWrapper:{
        marginTop: 15,
        padding: 16,
        marginHorizontal: 16,
        borderRadius: 5,
    },
    item:{
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    icon:{
        width: 12,
        height: 12,
        resizeMode: 'contain'
    },
    titleName:{
        fontSize: 13
    },
    timeText:{
        fontSize: 14,
        
    },
    phoneText:{
        fontSize: 13,
        marginLeft: 10
    },
    statusWrapper:{
        width: 40,
        height: 20,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }
})


export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(AccountLoginRecord)
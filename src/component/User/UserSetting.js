import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    Alert
} from 'react-native';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import { connect } from 'react-redux';
import { NotificationKeys,NetUrls } from '../../common/Constants';
import ToastManager from "../../common/CustomComponent/ToastManager";
import LocalTokenHandler from '../../util/LocalTokenHandler';
import { postJson,postPicture } from "../../util/ApiRequest";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';

class UserSetting extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'账号管理'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
		super(props);
        const { getParam } = props.navigation
        this.userData = getParam('userData') || {}
		this.callBack = getParam('callBack')
		
		this.state = {
			avatarSource: {uri: this.userData.photo},
            modalVisible:false,
            userName: this.userData.username,
            bindQQ: 0,
            bindWechat: 0
		};
	}

	componentDidMount() {
		this.requestUserInfo()
	}
	
	async requestUserInfo() {
        try {
            let data = await postJson({
                url: NetUrls.homePage,
                params: {}
            });
            if (data.code == 0) {
                this.setState({
                    userData : data.result || {},
					avatarSource: {uri: data.result.photo},
                    userName: data.result && data.result.username,
                    bindQQ: data.result && data.result.bindQQ,
                    bindWechat: data.result && data.result.bindWechat,
                    bindApple: data.result && data.result.bindApple
                })
            } else {
                ToastManager.show(data.msg);
            }
        } catch (e) {
            ToastManager.show('网络错误');
        }
    }
    
    //修改用户头像
	async requestChangeUserThumb(imageData) {
		try {
			let data = await postPicture({
				url: NetUrls.updateUserThumb,
				filePath: imageData.path,
				fileName: imageData.filename || 'userIcon.jpg',
				params: {
                }
            })
			if (data && data.code == 0) {
                ToastManager.show('用户头像已更新')
                this.callBack && this.callBack()
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			console.log(JSON.stringify(error));
        }
    }
    
    //修改用户昵称
    async requestChangeUserNick(name) {
		Keyboard.dismiss()
		if (!name) {
			ToastManager.show('请输入用户名')
			return
		}
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.updateUserNick,
				params: {
					name: name
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
				this.setState({
                    userName: name
                })
                this.callBack && this.callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }
    
    //解绑第三方账号
    async requestUnbind(type) {
		Keyboard.dismiss()
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.thirdUnbind,
				params: {
					type: type
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
				this.requestUserInfo()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
	}

	getActionSheet() {
		return (
			<ActionSheet
				ref={e => this.actionSheet = e}
				options={['从相册中选择', '拍一张', '取消']}
				cancelButtonIndex={2}
				onPress={(index) => {
					const { navigate } = this.props.navigation
					if (index == 0) {
						this.pickImage()
					} else if (index == 1) {
						this.pickImagewithCamera()
					}
				}}
			/>
		)
	}

	pickImage(){
		ImagePicker.openPicker({
			width: 100,
			height: 100,
			cropping: true,
			mediaType: 'photo',
			compressImageQuality: 1,
			cropperCircleOverlay: true
		}).then(image => {
			console.log(image);
			this.setState({
				avatarSource: {uri: image.path}
            })
            this.requestChangeUserThumb(image)
		}).catch(err=>{
			console.log(err);
		})
	}

	pickImagewithCamera(){
		ImagePicker.openCamera({
			width: 100,
			height: 100,
			cropping: true,
			mediaType: 'photo',
			compressImageQuality: 1,
			cropperCircleOverlay: true
		}).then(image => {
			console.log(image);
			this.setState({
				avatarSource: {uri: image.path}
            })
            this.requestChangeUserThumb(image)
		}).catch(err=>{
			console.log(err);
		})
    }
    
    unbindThirdPlatform(type){
        // type: 2-微信   0-QQ聊天
        let platform = ''
        if(type == 0){
            platform = 'QQ'
        }else if(type == 2){
            platform = '微信'
        }else if(type == 3){
            platform = 'Apple'
        }
        Alert.alert(
            '提示',
            '确定解除与'+platform+'账号的绑定?',
            [
                { text: '取消', onPress: () => { }, style: 'cancel' },
                { text: '解绑', onPress: () => {
                    this.requestUnbind(type)
                } },
            ]
        )
    }

	//常用功能 Item
    getCustomItem(img, title, toPage) {
		const { navigate } = this.props.navigation
		const Colors = this.props.themeInfo.colors
		
        return(
            <TouchableOpacity activeOpacity={0.7} style={styles.customItemTouch} onPress={()=>{
                navigate(toPage)
            }}>
                <Text style={{flex:1,marginLeft:20,fontSize:15,color:Colors.themeText}}>{title}</Text>
                <Image style={{width:8,height:12,marginRight:5}} source={require('../../images/enterLight.png')}/>
            </TouchableOpacity>
        )
    }

    // 第三方登录模块
    getThirdPlatformView(){
		const Colors = this.props.themeInfo.colors
		
        let bindWechat = this.state.bindWechat
        let bindQQ = this.state.bindQQ
        let bindApple = this.state.bindApple
        if(!bindWechat && !bindQQ && !bindApple){
            return
        }
        return(
            <>
                <Text style={[styles.titles, {color:Colors.themeText}]}>第三方账号</Text>
                {bindWechat ? <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.platformItem,{backgroundColor: Colors.themeBg,borderBottomColor: Colors.split}]}
                    onPress={()=>{
                        // type: 2-微信   0-QQ聊天  3-Apple
                        this.unbindThirdPlatform(2)
                    }}
                >
                    <Image style={styles.loginIcon} source={require('../../images/login/wechat.png')}/>
                    <Text style={[styles.thirdText, {color: Colors.themeText}]}>微信</Text>
                    <View style={{flex: 1}}/>
					<Text style={{color:Colors.themeText}}>解绑</Text>
                    <Image style={[styles.arrow,{marginLeft: 5}]} source={require('../../images/enterLight.png')}/>
				</TouchableOpacity> : null}
                
                {bindQQ ? <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.platformItem,{backgroundColor: Colors.themeBg,borderBottomColor: Colors.split}]}
                    onPress={()=>{
                        // type: 2-微信   0-QQ聊天  3-Apple
                        this.unbindThirdPlatform(0)
                    }}
                >
                    <Image style={styles.loginIcon} source={require('../../images/login/QQ.png')}/>
                    <Text style={[styles.thirdText, {color: Colors.themeText}]}>QQ</Text>
                    <View style={{flex: 1}}/>
					<Text style={{color:Colors.themeText}}>解绑</Text>
                    <Image style={[styles.arrow,{marginLeft: 5}]} source={require('../../images/enterLight.png')}/>
				</TouchableOpacity> : null }
                
                {bindApple ? <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.platformItem,{backgroundColor: Colors.themeBg,borderBottomColor: Colors.split}]}
                    onPress={()=>{
                        // type: 2-微信   0-QQ聊天  3-Apple
                        this.unbindThirdPlatform(3)
                    }}
                >
                    <Image style={styles.loginIcon} source={require('../../images/login/apple.png')}/>
                    <Text style={[styles.thirdText, {color: Colors.themeText}]}>Apple</Text>
                    <View style={{flex: 1}}/>
					<Text style={{color:Colors.themeText}}>解绑</Text>
                    <Image style={[styles.arrow,{marginLeft: 5}]} source={require('../../images/enterLight.png')}/>
				</TouchableOpacity> : null }
            </>
        )
    }

	getListView() {
		const {navigate,pop} = this.props.navigation
		const Colors = this.props.themeInfo.colors
		
		let userIcon = require('../../images/header_pic.png');
        if(this.state.avatarSource.uri){
            userIcon = this.state.avatarSource
		}
		return (
			<View style={{marginTop:10, backgroundColor:Colors.themeBg }}>
				<TouchableOpacity activeOpacity={0.7} style={styles.item} onPress={()=>{
                    this.actionSheet.show()
                }}>
					<Text style={{fontSize:15,color:Colors.themeText}}>个人头像</Text>
                    <View style={{flex: 1}}/>
					<View style={[styles.imgWrapper,{backgroundColor:Colors.themeBaseBg}]}>
						<Image style={{ width: 38, height: 38,resizeMode:'contain' }} source={userIcon} />
					</View>
					<Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
				</TouchableOpacity>
                <View style={[styles.line, {backgroundColor:Colors.split}]}/>			
				<TouchableOpacity activeOpacity={0.7} style={styles.item} onPress={()=>{
                    navigate('ReName',{
                        title:'修改名称',
                        name: this.state.userName,
                        callBack:(name)=>{
                            pop()
                            this.requestChangeUserNick(name)
                        }}
                    )
                }}>
					<Text style={{fontSize:15,color:Colors.themeText}}>用户名</Text>
                    <View style={{flex: 1}}/>
					<Text style={{color: Colors.themeTextLight}}>{this.state.userName}</Text>
					<Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
				</TouchableOpacity>
                <View style={[styles.line, {backgroundColor:Colors.split}]}/>
                <TouchableOpacity activeOpacity={0.7} style={styles.item} onPress={()=>{
                    navigate('AccountLoginRecord')
                }}>
					<Text style={{fontSize:15,color:Colors.themeText}}>登录设备管理</Text>
                    <View style={{flex: 1}}/>
					<Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
				</TouchableOpacity>
                <View style={[styles.line, {backgroundColor:Colors.split}]}/>
				<View style={styles.item}>
					<Text style={{fontSize:15,color:Colors.themeText}}>账号</Text>
                    <View style={{flex: 1}}/>
					<Text style={{color:Colors.themeTextLight,marginRight:5}}>{this.userData.phone}</Text>
				</View>
                <View style={[styles.line, {backgroundColor:Colors.split}]}/>
				<TouchableOpacity activeOpacity={0.7} style={styles.item} onPress={()=>{
                    navigate('CloseAccount')
                }}>
					<Text style={{fontSize:15,color:Colors.themeText}}>注销账号</Text>
                    <View style={{flex: 1}}/>
					<Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
                    </TouchableOpacity>
			</View>
		)
	}

	render() {
		const Colors = this.props.themeInfo.colors
		return (
			<ScrollView style={{backgroundColor: Colors.themeBaseBg}}>
				{this.getListView()}
                {this.getThirdPlatformView()}
				{this.getActionSheet()}
			</ScrollView >
		)
	}
}

const styles = StyleSheet.create({
	item:{
		flexDirection:'row',
		alignItems:'center',
        height:50,
        paddingHorizontal: 16
    },
    platformItem: {
        flexDirection:'row',
		alignItems:'center',
        height:50,
        paddingHorizontal: 16,
        borderBottomWidth: 1
    },
	arrow:{
		width:8,
		height:12,
		marginRight:5,
		marginLeft:15
	},
	line:{
		width:'100%',
		height:1,
    },
    titles: {
        fontSize:16,
        fontWeight:'bold',
        margin: 16,
        marginTop: 30
    },
    loginIcon:{
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    thirdText:{
        marginLeft: 10,
        fontSize: 15,
    },
    imgWrapper:{
        width:38,
        height:38,
        borderRadius:19,
        overflow:'hidden'
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(UserSetting)

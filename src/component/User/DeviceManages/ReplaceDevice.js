/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	DeviceEventEmitter,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
    TextInput,
    Dimensions,
	Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import { NotificationKeys, NetUrls, Colors } from '../../../common/Constants';
import { postJson } from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';
import { BottomSafeMargin } from '../../../util/ScreenUtil';

const screenH = Dimensions.get('window').height
const screenW = Dimensions.get('window').width

class ReplaceDevice extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={'设备更换'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.getwayData = getParam('getwayData') || {}
		
		this.state = {
			isNextStep: false,
            serialNo: ''
		}
	}

	componentDidMount() {
		
	}

	componentWillUnmount() {
		
	}

    // 获取面板信息
	async requestPanelInfo() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.getWifiPanelGateway,
				params: {	
					id: this.getwayData.id,
                    serialNumber: this.state.serialNo
				}
			});
			SpinnerManager.close()
            ToastManager.show(data.msg)
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	// 获取网关信息
	async requestPutSerialNumber() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.updateSerialNumber,
				params: {	
					id: this.getwayData.id,
                    serialNumber: this.state.serialNo
				}
			});
			SpinnerManager.close()
            ToastManager.show(data.msg)
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    renderMainImg(){
        return(
            <View style={styles.imgWrapper}>
                <Image style={styles.mainImg} source={require('../../../images/485/replaceDevice.png')}/>
            </View>
        )
    }

    renderReplaceTips(){
        if(this.state.isNextStep){
            return null
        }
        const Colors = this.props.themeInfo.colors;

        return(
            <View style={styles.tipsWrapper}>
                <Text style={[styles.tipsText,{color: Colors.themeTextLight}]}>请长按新设备上方高亮按钮3秒，以绑定设备</Text>
            </View>
        )
    }

    renderReplaceInput(){
        if(!this.state.isNextStep){
            return null
        }
        const Colors = this.props.themeInfo.colors;

        return(
            <View style={styles.tipsWrapper}>
                <Text style={[styles.tipsText,{color: Colors.themeTextLight}]}>设备更换完成，我们已经将原设备的功能完整的复制到了新的设备中</Text>
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={[styles.input,{color: Colors.themeText}]}
                        placeholder="请输入设备序列号"
                        clearButtonMode={'while-editing'}
                        placeholderTextColor={Colors.themeInactive}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                            this.state.serialNo = text
                        }} 
                    />
                </View>
            </View>
        )
    }

    renderBottomBtn(){
        const btnLabel = this.state.isNextStep ? '确认更换' : '下一步'

        return(
            <View style={styles.bottomWrapper}>
                <TouchableOpacity style={styles.bottomBtn} onPress={()=>{
                    if(this.state.isNextStep){
                        if(!this.state.serialNo){
                            ToastManager.show('设备序列号不能为空')
                            return
                        }
                        this.requestPutSerialNumber()
                    }else{
                        this.setState({
                            isNextStep: true
                        })
                    }
                }}>
                    <Text style={styles.btnText}>{btnLabel}</Text>
                </TouchableOpacity>
            </View>
        )
    }

	render() {
		const Colors = this.props.themeInfo.colors;

		return (
			<TouchableOpacity activeOpacity={1} style={{backgroundColor: Colors.themeBg, flex: 1}} onPress={()=>{
                Keyboard.dismiss()
            }}>
				{this.renderMainImg()}
                {this.renderReplaceTips()}
                {this.renderReplaceInput()}
                {this.renderBottomBtn()}
			</TouchableOpacity>
		)
	}
}

const styles = StyleSheet.create({
	imgWrapper:{
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    },
    mainImg:{
        width: screenW * 0.4,
        height: screenW * 0.4,
        resizeMode:'contain',
        marginTop: 20
    },
    tipsWrapper:{
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: '15%'
    },
    tipsText:{
        textAlign: 'center',
        fontSize: 15,
        lineHeight: 20
    },
    btnTouch:{
        marginTop: 20,
        height:44,
        width: 200,
        borderRadius:22,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: Colors.newBtnBlueBg
    },
    bottomWrapper:{
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: BottomSafeMargin + screenH * 0.1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    bottomBtn:{
        justifyContent: 'center',
        alignItems: 'center',
        width: screenW * 0.5,
        height: 44,
        borderRadius: 22,
        backgroundColor: Colors.newBtnBlueBg
    },
    btnText:{
        fontSize: 18,
        color: Colors.white
    },
    input:{
        width: '100%',
        height: '100%',
        paddingHorizontal: 10,
        fontSize: 16,
        fontWeight: '500'
    },
    inputWrapper:{
        marginTop: 20,
        width: '100%',
        height: 44,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#C8C9CC'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ReplaceDevice)

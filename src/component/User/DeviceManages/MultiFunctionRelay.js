import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity,
    RefreshControl,
    DeviceEventEmitter,
    SwipeableFlatList
} from 'react-native';
import Slider from "react-native-slider";
import { connect } from 'react-redux';
import {Colors, NetUrls, NotificationKeys,NetParams} from '../../../common/Constants';
import {postJson} from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';
import {ImagesDark, ImagesLight} from '../../../common/Themes';
import ActionSheet from 'react-native-actionsheet';
import {ListLoading, ListError} from "../../../common/CustomComponent/ListLoading";

//侧滑最大距离
const maxSwipeDistance = 180
//侧滑按钮个数
const countSwiper = 2

class MultiFunctionRelay extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text: '设置', onPress:()=>{
                    navigation.getParam('settingBtn')()
                }},
                {icon:require('../../../images/add.png'),onPress:()=>{
                    navigation.getParam('addBtn')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

    constructor(props) {
        super(props);
        const {getParam, setParams} = props.navigation
        this.getwayData = getParam('getwayData')
        this.callBack = getParam('callBack')

        this.state = {
            loading: true,
            isRefreshing: false,
            listData: null,
            selectType: 0,
            typeList: [{type:0, value: '全部'}]
        }

        setParams({
            settingBtn: this.settingBtnClick.bind(this),
            addBtn: this.addDeviceBtnClick.bind(this)
        })
    }

    componentDidMount() {
        this.requestFunctionTypes()
        this.requestList()
        this.deviceStatusNoti = DeviceEventEmitter.addListener(NotificationKeys.kDeviceStatusNotification,()=>{
            this.requestFunctionTypes()
			this.requestList()
		})
    }

    componentWillUnmount(){
        this.deviceStatusNoti.remove()
    }

    // 设置按钮点击
    settingBtnClick(){
        const {navigate, setParams} = this.props.navigation
        navigate('GatewaySetting',{
            title: this.getwayData.name + '设置',
            name: this.getwayData.name,
            getwayData: this.getwayData,
            isMultiGateway: true,
            callBack: (data)=>{
                if(data && data.name){
                    setParams({title: data.name})
                }
                this.callBack && this.callBack()
            }
        })
    }

    //添加按钮点击
    addDeviceBtnClick(){
        this.actionSheet.show()
    }

    // 设备类型
    async requestFunctionTypes() {
        try {
            let data = await postJson({
                url: NetUrls.multiFunctionTypes,
                params: {
                    deviceId: this.getwayData && this.getwayData.id
                }
            });
            
            if (data.code == 0) {
                let types = [{type:0, value: '全部'}]
                if(data.result && data.result.length >=0 ){
                    types = types.concat(data.result)
                }
                this.setState({
                    typeList: types
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    // 请求列表
    async requestList() {
        try {
            let data = await postJson({
                url: NetUrls.multiFunctionDevice,
                params: {
                    deviceId: this.getwayData && this.getwayData.id,
                    type: this.state.selectType
                }
            });
            this.setState({loading: false,isRefreshing:false})
            if (data.code == 0) {
                this.setState({
                    listData: data.result || []
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({loading: false,isRefreshing:false})
            ToastManager.show('网络错误')
        }
    }

    // 设备 开关
	async changeDevieceStatus(id,targetStatus,call){
		try {
			let data = await postJson({
				url: NetUrls.deviceUpdate,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: {
					status: targetStatus,
					id: id
				}
			});
			if (data.code == 0) {
				call && call()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    //更改备注
    async updateRemark(params){
        params = params || {}
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.saveDevice,
				params: {
					...params
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
                this.requestList()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
	}

    //移除
    async deleteDevice(params){
        params = params || {}
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.removeMultiDevice,
				params: {
					...params
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
                this.requestList()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
	}

    gotoAddDevice(type){
        if(!type){
            type = this.state.typeList[this.state.selectType] || {}
        }
        if(type.type == 0){
            this.addDeviceBtnClick()
            return
        }
        if(parseInt(type.max) - parseInt(type.count) <= 0){
            ToastManager.show('该设备已达最大值。')
            return
        }
        this.props.navigation.navigate('AddMultiFunctionDevice',{
            title: type.value,
            type: type.type,
            getwayData: this.getwayData,
            maxCount: parseInt(type.max) - parseInt(type.count),
            callBack: ()=>{
                this.requestFunctionTypes()
                this.requestList()
            }
        })
    }

    onRefresh(){
        if (this.state.isRefreshing) return
		this.setState({ isRefreshing: true })
        this.requestList()
    }

    renderHeader(){
        const Colors = this.props.themeInfo.colors

        let typeList = this.state.typeList.map((val, index) => {
			let  underLine = null
            let activeText = {color:Colors.themeTextLight}
            
			if(this.state.selectType == val.type){
				underLine = <View style={styles.underLine} />
				activeText = {color:Colors.themeText,fontWeight:'bold'}
			}

			return (
				<TouchableOpacity key={index} activeOpacity={0.7} style={styles.typeTouch} onPress={()=>{
                    this.closeSwiper()
                    if(this.state.selectType == val.type){
                        return
                    }
					this.setState({
						selectType: val.type,
                        loading: true,
                        listData: null
                    },()=>{
                        this.requestList()
                        // this.refs.scene_list.getScene()
                        // this.refs.scene_list.closeSwiper()
                    })
				}}>
					<Text style={[styles.typeText,activeText]}>{val.value}</Text>
					{underLine}
				</TouchableOpacity>
			)
		})

        return(
            <View style={[styles.sectionHead,{backgroundColor: Colors.themeBg,borderBottomColor: Colors.themeBaseBg}]}>
                <ScrollView
					horizontal={true}
					showsHorizontalScrollIndicator={false}
                    style={styles.headScroll}
					contentContainerStyle={styles.headScrollContainer}
				>
					{typeList}
				</ScrollView>
            </View>
        )
    }

    renderActionSheet() {
        let options = []
        for (const type of this.state.typeList) {
            if(type.type != 0){
                options.push(type.value)  
            } 
        }
        options.push('取消')

		return (
			<ActionSheet
                title='选择设备类型'
				ref={ e => this.actionSheet = e}
				options={options}
				cancelButtonIndex={options.length - 1}
				onPress={(index) => {
                    this.closeSwiper()
                    if(index == options.length - 1){
                        return
                    }
                    let type = this.state.typeList[index + 1] || {}
                    this.gotoAddDevice(type)
				}}
			/>
		)
	}

    //获取开关类型Item
    renderSwitchControl(rowData, index){
        const Colors = this.props.themeInfo.colors

        // 设备状态
        let activeTouch = {}
        let activeText = <Text style={{fontSize:14,color:Colors.themeTextLight}}>开启</Text>
        if(rowData.value > 0){
            activeTouch = {backgroundColor:Colors.themeButton,borderColor:Colors.themeButton}
            activeText = <Text style={{fontSize:14,color:Colors.white}}>已开启</Text>
        }

        return(
            <TouchableOpacity style={styles.switchBtnTouch} onPress={()=>{
                this.closeSwiper()
                if(rowData.islock){
                    ToastManager.show('当前设备被锁定')
                    return
                }
                let target = rowData.value ? 0 : 1
                this.changeDevieceStatus(rowData.id,target,()=>{
                    this.state.listData[index].value = target
                    this.setState({ ...this.state })
                })
            }}>
                <View style={[styles.switchBtn, {borderColor:Colors.borderLight}, activeTouch]}>
                    {activeText}
                </View>
            </TouchableOpacity>
        )
    }

    //获取滑竿类型的Item
    renderSliderControl(rowData, index){
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
        const switchIcon = rowData.value > 0 ? Images.switchOn : Images.switchOff

        return(
            <TouchableOpacity style={styles.switchTouch} onPress={()=>{
                this.closeSwiper()
                let target = rowData.value ? 0 : 1
                this.changeDevieceStatus(rowData.id, target,()=>{
                    this.state.listData[index].value = target
                    this.setState({ ...this.state })
                })
            }}>
                <Image style={{width:50,height:30,resizeMode:'contain'}} source={switchIcon}/>
            </TouchableOpacity>
        )
    }

    //获取静态类型的Item
    renderStaticItem(rowData, index){
        const Colors = this.props.themeInfo.colors
        
        if(!rowData.statusText){
            return null
        }

        return (
            <View style={[styles.statusTextWrapper, {borderColor:Colors.borderLight}]}>
                <Text style={{fontSize: 14, color:Colors.themeTextLight}}>{rowData.statusText}</Text>
            </View>
        )
    }

    // 控制模块
    renderControl(rowData, index){
        if(rowData.type == 1){
            return this.renderSwitchControl(rowData, index)
        }else if(rowData.type == 2){
            return this.renderSliderControl(rowData, index)
        }else{
            return this.renderStaticItem(rowData, index)
        }
    }

    //滑竿
    renderSliderView(rowData, index){
        const Colors = this.props.themeInfo.colors

        if(rowData.value <= 0){
            return null
        }
        if(rowData.type != 2){
            return null
        }

        return (
            <View style={{height: 50, justifyContent:'center', alignItems:'center'}}>
                <Slider
                    style={{width: '90%'}}
                    minimumValue={0}
                    maximumValue={100}
                    value={rowData.value}
                    onSlidingComplete={(value)=>{
                        this.changeDevieceStatus(rowData.id,Math.floor(value),()=>{})
                    }}
                    minimumTrackTintColor={Colors.themeButton}
                    maximumTrackTintColor={Colors.trackBg}
                    thumbTintColor={Colors.themeButton}
                    trackStyle={{height:6,borderRadius: 3}}
                    thumbStyle={{width:20,height:20,borderRadius:10}}
                />
            </View>
        )
    }

    renderItem(rowData, index){
        const Colors = this.props.themeInfo.colors
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
        const deviceIcon = this.props.themeInfo.isDark ? rowData.nightIcon : rowData.dayIcon

        // 楼层房间信息
        let floorView = null
        if(rowData.floorText && rowData.roomName){
            floorView = <Text style={{fontSize:14,color:Colors.themeTextLight,marginTop:5}}>{rowData.floorText + '  ' + rowData.roomName}</Text>
        }

        // 设备是否 锁定
        let lockIcon = null
        if(rowData.islock){
            lockIcon = <Image style={styles.lockIcon} source={require('../../../images/deviceIcon/lock.png')}/>
        }

        // 按钮点击事件
        let buttonClick = ()=>{
            const {navigate} = this.props.navigation
            // 进入二级页面
            navigate('DeviceSetting', {
                deviceData: {deviceId:rowData.id},
                isFromManager:true
            })
        }

        return(
            <TouchableOpacity
                activeOpacity={1}
                key={'second_mange_list_'+index} 
                style={[styles.sliderItem, {backgroundColor:Colors.themeBg}]}
                onPress={()=>{
                    this.closeSwiper()
                    buttonClick()
                }}
            >
                <View style={styles.sliderTouch}>
                    <Image style={styles.icon} defaultSource={Images.default} source={{uri: deviceIcon}}/>
                    <View style={{marginLeft:20,flex:1}}>
                        <Text style={{fontSize:15,color:Colors.themeText,fontWeight:'bold'}}>{rowData.name}</Text>
                        {floorView}
                    </View>
                    {lockIcon}
                    {this.renderControl(rowData,index)}
                </View>
                {this.renderSliderView(rowData, index)}
                <View style={[styles.splitLine,{backgroundColor:Colors.borderLight}]}/>
                <View style={styles.bottomTouch}>
                    <Text style={{fontSize: 14, color: Colors.themeTextLight}}>备注</Text> 
                    <Text style={[styles.numText,{color: Colors.themeText}]}>{rowData.multiRemark}</Text>
                    <View style={{flex: 1}}/>
                    <Image style={styles.arrow} source={require('../../../images/enterLight.png')}/>
                </View>
            </TouchableOpacity>
        )
    }

    renderNoContent(){
        const Colors = this.props.themeInfo.colors;

        return(
            <View style={{alignItems: 'center', marginTop: '50%', paddingHorizontal: 20}}>
                <Text style={{color: Colors.themeTextLight}}>暂无设备</Text>
                <TouchableOpacity style={[styles.moreTouch,{marginTop:20}]} onPress={()=>{
                    this.gotoAddDevice()
				}}>
					<Text style={{fontSize:14,color:Colors.themeTextLight}}>去添加</Text>
					<Image style={styles.arrow} source={require('../../../images/enterLight.png')}/>
				</TouchableOpacity>
            </View>
        )
    }

    getQuickActions(rowData, index) {
		return (
			<View style={styles.quickAContent}>
                <TouchableOpacity
                    style={[styles.quick,styles.quickCenter]}
                    activeOpacity={0.7} 
                    onPress={()=>{
                        this.closeSwiper()
                        this.props.navigation.navigate('ReName',{
                            title: '修改备注',
                            name: rowData.multiRemark,
                            maxLength: 16,
                            isAllowEmpty: true,
                            callBack:(name)=>{
                                this.updateRemark({id: rowData.id,multiRemark: name})
                                this.props.navigation.pop()
                            }
                        })
                    }}
                >
					<Text style={styles.quickText}>修改备注</Text>
				</TouchableOpacity>
                <TouchableOpacity
                    style={styles.quick}
                    activeOpacity={0.7} 
                    onPress={()=>{
                        this.closeSwiper()
                        this.deleteDevice({id: rowData.id})
                    }}
                >
					<Text style={styles.quickText}>移除</Text>
				</TouchableOpacity>
			</View>
		)
    }

    _extraUniqueKey(item, index) {
		return "multi_index_" + index;
	}

    //关闭侧滑栏
	closeSwiper() {
		this.swiperFlatlist && this.swiperFlatlist.setState({ openRowKey: null })
	}

    //获取总列表
    renderListView() {
        if(this.state.loading && !this.state.listData){
            return <ListLoading/>
        }else if(!this.state.listData){
            return <ListError/>
        }

        return(
            <SwipeableFlatList
                ref={e => this.swiperFlatlist = e}
                contentContainerStyle={{ paddingBottom: 50 }}
                scrollIndicatorInsets={{right: 1}}
                removeClippedSubviews={false}
                data={this.state.listData}
                keyExtractor={this._extraUniqueKey}
                renderItem={({ item, index }) => this.renderItem(item, index)}
                renderQuickActions={({ item, index }) => this.getQuickActions(item, index)}
                maxSwipeDistance={maxSwipeDistance}
                bounceFirstRowOnMount={false}
                initialNumToRender={5}
                ListEmptyComponent= {this.renderNoContent()}
                refreshControl={
                    <RefreshControl
                        colors={[Colors.themeBGInactive]}
                        tintColor={Colors.themeBGInactive}
                        refreshing={this.state.isRefreshing}
                        onRefresh={this.onRefresh.bind(this)}
                    />
                }
            />
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors;

        return (
            <View style={{flex: 1, backgroundColor: Colors.themeBaseBg}}>
                {this.renderHeader()}
                {this.renderListView()}
                {this.renderActionSheet()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    scrollContent:{
        paddingBottom: 50,
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 16,
    },
    icon:{
        width: 40, 
        height: 40, 
        marginLeft: 16, 
        resizeMode:'contain'
    },
    inputWrapper:{
		width: '100%',
		height: 65,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:15
    },
    statusTextWrapper:{
		height:30,
		borderRadius:15,
		borderWidth:1,
		justifyContent:'center',
		alignItems:'center',
		marginRight:10,
		paddingHorizontal: 15,
    },
    switchBtn:{
        width:90,
		height:34,
		borderRadius:17,
		borderWidth:1,
		justifyContent:'center',
		alignItems:'center'
    },
    switchBtnTouch:{
        paddingHorizontal: 10,
        height:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    lockIcon:{ 
        width: 16, 
        height: 20, 
        resizeMode: 'contain', 
        marginRight: 15 
    },
    switchTouch:{
        height:'100%',
        paddingHorizontal: 16,
        justifyContent:'center'
    },
    sliderItem:{
		borderRadius:5,
		marginTop:15,
        marginHorizontal: 16
    },
    sliderTouch: {
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        height: 65
    },
    underLine:{
        position:'absolute',
        bottom: 0,
        width: 30,
        height: 3,
        backgroundColor: Colors.tabActiveColor,
    },
    sectionHead: {
		width: '100%',
		height: 40,
        flexDirection:'row',
        alignItems:'center',
        borderBottomWidth: 1
	},
    headScroll: {
		height: '100%',
	},
	headScrollContainer: {
		alignItems: 'center',
		paddingLeft: 10,
		paddingRight:20
	},
	typeTouch: {
		alignItems: 'center',
        justifyContent: 'center',
		paddingHorizontal: 15,
		marginRight: 10,
        height: '100%'
	},
    typeText:{
        fontSize: 15
    },
    arrow:{
        width: 7,
        height: 13,
        resizeMode: 'contain'
    },
    splitLine:{
        height:1,
        marginTop: 5,
        marginHorizontal: 16
    },
    bottomTouch:{
        paddingHorizontal:16,
        flexDirection:'row',
        alignItems:'center',
        height:40
    },
    numText:{
        fontSize: 14,
        marginLeft: 15
    },
    moreTouch:{
		borderColor:Colors.borderLightGray,
		borderWidth:1,
		width:120,
		height:30,
		borderRadius:15,
		justifyContent:'center',
		alignItems:'center',
		flexDirection:'row'
	},
	arrow:{
		width:6,
		height:11,
		marginLeft:10,
		resizeMode:'contain'
    },
    //侧滑菜单的样式
	quickAContent: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginRight: 16,
		marginTop: 15,
		overflow: 'hidden',
        paddingVertical: 0.5
	},
    quick: {
		alignItems: 'center',
		justifyContent: 'center',
		width: maxSwipeDistance / countSwiper,
        backgroundColor: Colors.themBgRed,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10
	},
    quickCenter: {
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,
        backgroundColor: Colors.tabActiveColor,
    },
    quickText:{
        fontSize: 14,
        color: Colors.white
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(MultiFunctionRelay)

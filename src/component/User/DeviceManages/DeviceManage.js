import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    DeviceEventEmitter,
    NativeModules,
    Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import {SwipeAction} from '@ant-design/react-native';
import {Colors, NetUrls, NotificationKeys} from '../../../common/Constants';
import {postJson} from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import {ListNoContent} from "../../../common/CustomComponent/ListLoading";
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';
import {ImagesLight, ImagesDark, ColorsLight, ColorsDark} from '../../../common/Themes';
import PopUp from '../../../common/CustomComponent/PopUp';

const JKUMengShare = Platform.select({
    ios: NativeModules.UMShareModule,
    android: NativeModules.JKUMAndShareUtils
});

class DeviceManage extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'设备管理'}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {icon:require('../../../images/add.png'),onPress:()=>{
                    navigation.getParam('addBtnClick')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const {getParam,setParams} = props.navigation
        this.state = {
            loading: true,
            expandStatus: [],
            ctrData: null,
            popVisible: false
        };

        setParams({
            addBtnClick: ()=>{ this.setState({popVisible: true}) },
            dismissPop: ()=>{ this.setState({popVisible: false}) }
        })
    }

    componentDidMount() {
        this.getControllerInfo()
        this.deviceUpdateNoti = DeviceEventEmitter.addListener(NotificationKeys.kDeviceStatusNotification, ()=>{
            this.getControllerInfo()
        })
    }

    componentWillUnmount(){
        this.deviceUpdateNoti.remove()
    }

    async getControllerInfo() {
        this.setState({loading: true})
        try {
            let data = await postJson({
                url: NetUrls.controllerList,
                params: {}
            });
            this.setState({loading: false})
            if (data.code == 0) {
                this.setState({
                    ctrData: data.result || [],
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({loading: false})
            ToastManager.show('网络错误')
        }
    }

    async addMultiGateway(){
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.createMultiFunction,
                params: {}
            });
            SpinnerManager.close()
            if (data.code == 0) {
                ToastManager.show('多功能网关创建成功!')
                this.getControllerInfo()
                this.props.navigation.navigate('MultiFunctionRelay', {
                    getwayData: data.result,
                    title: data.result.name,
                    callBack: () => {this.getControllerInfo()}
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({loading: false})
            ToastManager.show('网络错误')
        }
    }

    // controllerUpdateName
    async requestSaveName(params){
        SpinnerManager.show()
        try {
			let data = await postJson({
				url: NetUrls.controllerUpdateName,
				params: params
			});
            SpinnerManager.close()
			if (data.code == 0) {
				this.getControllerInfo()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    // 处理一级Item 的展开状态
    expandItem(devicesData, index) {
        if (!devicesData || devicesData.length <= 0 || !devicesData[0]) {
            ToastManager.show('该控制器暂未连接任何设备')
            return
        }
        let expandStatus = JSON.parse(JSON.stringify(this.state.expandStatus))
        expandStatus[index] = !expandStatus[index]
        this.setState({
            expandStatus: expandStatus
        })
    }

    // 当 Item 是未展开状态时 (可以左滑) (一级Item)
    getSwipableItem(val, index) {
        const {navigate} = this.props.navigation
        const Colors = this.props.themeInfo.colors
        
        let controller = val || {}
        const right = [
            {
                text: '重命名',
                onPress: () => {
                    const { navigate,pop } = this.props.navigation
                    navigate('ReName',{title:'控制器重命名', callBack:(name)=>{
                        pop()
                        this.requestSaveName({id:val.id, name:name})
                    }})
                },
                style: {backgroundColor: Colors.themeButton, color: Colors.themeBg},
            },
        ];
        return (
            <View key={'swiperItem_' + index} style={styles.itemWrapper}>
                <View style={[styles.swiper, {height: 60}]}>
                    <SwipeAction
                        autoClose
                        style={{backgroundColor: Colors.themeBg, borderRadius: 5}}
                        right={right}
                        // disabled={true}
                    >
                        <TouchableOpacity
                            activeOpacity={0.7}
                            style={{height: '100%', paddingRight: 15, flexDirection: 'row', alignItems: 'center'}}
                            onPress={() => {
                                this.expandItem(controller.devices, index)
                            }}
                        >
                            <Text style={{fontSize: 18,marginLeft: 30,flex: 1,color: Colors.themeText}}>{controller.name}</Text>
                            <Image style={{width: 13, height: 8}} source={require('../../../images/controller/arrow_down.png')}/>
                        </TouchableOpacity>
                    </SwipeAction>
                </View>
            </View>
        )
    }

    // 当 Item 是展开状态时 (一级Item)
    getExpandItem(val, index) {
        const Colors = this.props.themeInfo.colors
        
        let devices = val.devices || []
        let subCount = devices.length
        let subItem = devices.map((val, index) => {
            return (
                this.getSubItem(val, index, index == subCount - 1)
            )
        })
        let item = (
            <View key={'expandItem_' + index} style={styles.itemWrapper}>
                <View style={styles.swiper}>
                    <View style={{backgroundColor: Colors.themeBg, borderTopLeftRadius: 5, borderTopRightRadius: 5}}>
                        <TouchableOpacity
                            activeOpacity={0.7}
                            style={{height: 60, paddingRight: 15, flexDirection: 'row', alignItems: 'center'}}
                            onPress={() => {
                                this.expandItem(devices, index)
                            }}
                        >
                            <Text style={{fontSize: 18,marginLeft: 30,flex: 1,color: Colors.themeText}}>{val.name}</Text>
                            <Image style={{width: 13, height: 8}} source={require('../../../images/controller/arrow_up.png')}/>
                        </TouchableOpacity>
                    </View>
                    {subItem}
                </View>
            </View>
        )
        return item
    }

    //当 Item 被展开时的 子item (二级 Item)
    getSubItem(val, index, isBottom) {
        const {navigate} = this.props.navigation
        const Colors = this.props.themeInfo.colors
        
        let device = val || {}
        let cusStyle = null
        let icon = this.props.themeInfo.isDark ? val.nightIcon : val.dayIcon
        if (isBottom) {
            cusStyle = {paddingBottom: 5, borderBottomLeftRadius: 5, borderBottomRightRadius: 5}
        }

        return (
            <View key={index} style={{backgroundColor: Colors.themeBg, ...cusStyle}}>
                <View style={[styles.line, {backgroundColor: Colors.split}]}/>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={{height: 60, paddingRight: 15, flexDirection: 'row', alignItems: 'center'}}
                    onPress={() => {
                        //被展开的子 Item 点击事件
                        if (!device) {
                            return
                        }
                        if(device.h5Url){
                            navigate('BleConfigWebPage',{
                                url: device.h5Url
                            })
                            return
                        }
                        if(device.attributeTypename == 'multi-function4-gateway'){
                            // 多功能继电器
                            navigate('MultiFunctionRelay', {
                                getwayData: val,
                                title: val.name,
                                callBack: () => {this.getControllerInfo()}
                            })
                        }else if(device.panelType == 'wifi_panel_led'){
                            navigate('WifiPanelButtonList', {
                                getwayData: val,
                                title: val.name,
                                callBack: () => {this.getControllerInfo()}
                            })
                        }else if(device.attributeTypename.indexOf('instruction-') == 0){
                            navigate('ProtocolControlList',{
                                getwayData: val,
                                title: val.name,
                                attributeType: device.attributeTypename
                            })
                        }else{
                            // 进入二级页面
                            navigate('GroupManageList', {
                                getwayData: val,
                                title: val.name,
                                attributeType: device.attributeTypename,
                                callBack: () => {this.getControllerInfo()}
                            })
                            // attributeTypename: 'zigbee-gateway',
                        }
                    }}
                >
                    <Image style={styles.getwayIcon} source={{uri: icon}}/>
                    <View style={{marginLeft: 20, flex:1}}>
                        <Text style={{fontSize: 15, color: Colors.themeText}}>{device.name}</Text>
                        {device.gatewayNumber ? <Text style={{fontSize: 12, marginTop: 5,color: Colors.themeTextLight}}>{device.gatewayNumber}</Text> : null}
                    </View>
                    <Image style={styles.rightArrow} source={require('../../../images/enterLight.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    // 获取每个Item（一级 Item）
    getItem(val, index) {
        let item = this.getSwipableItem(val, index)
        if (this.state.expandStatus[index]) {
            item = this.getExpandItem(val, index)
        }
        return item
    }

    //获取总列表
    renderList() {
        const Colors = this.props.themeInfo.colors
        
        if(this.state.loading && !this.state.ctrData){
            return (
                <View style={styles.loading}>
                    <Text style={{color: Colors.themeTextLight}}>加载中...</Text>
                </View>
            )
        }
        if(!this.state.ctrData){
            return (
                <View style={styles.loading}>
                    <Text style={{color: Colors.themeTextLight}}>网络错误，请稍后重试</Text>
                </View>
            )
        }
        if(this.state.ctrData.length <= 0) {
            const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
            return (
                <ListNoContent
					img={Images.noController} 
					text={'暂无控制器'}
				/>
            )
        }
        for (const key in this.state.ctrData) {
            this.state.expandStatus.push(true)
        }
        let list = this.state.ctrData.map((val, index) => {
            return this.getItem(val, index)
        })

        return (
            <ScrollView 
                contentContainerStyle={styles.scrollContent} 
                scrollIndicatorInsets={{right: 1}}
            >
                {list}
            </ScrollView>
        )
    }

    renderPopModal(){
        if(!this.state.popVisible){
            return null
        }
        const {navigate} = this.props.navigation
        const Colors = this.props.themeInfo.isDark ? ColorsLight : ColorsDark

        return (
            <PopUp
                frame={{top: 0, right: 10, width: 140}}
                triangleRight={15}
                dismiss={()=>{ this.setState({popVisible: false}) }}
                bgColor={Colors.themeBg}
                textColor={Colors.themeText}
                borderColor={Colors.split}
                buttonArrays={[
                    {icon: require('../../../images/wifi.png'),title: 'WIFI配网',onPress:()=>{
                        this.setState({popVisible: false})
                        navigate('WiFiDeviceCategory')
                        // JKUMengShare.openMinProgram()
                    }},
                    {icon: require('../../../images/qrScan.png'),title: '扫码添加',onPress:()=>{
                        navigate('AddDeviceQRCode')
                        this.setState({popVisible: false})
                    }},
                    // {icon: require('../../../images/add.png'),title: '多功能网关',onPress:()=>{
                    //     this.setState({popVisible: false})
                    //     this.addMultiGateway()
                    // }},
                    {icon: require('../../../images/find.png'),title: '发现设备',onPress:()=>{
                        this.setState({popVisible: false})
                        navigate('FindDevices')
                    }},
                ]}
            />
        )   
    }

    render() {
        const Colors = this.props.themeInfo.colors
        
        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderList()}
                {this.renderPopModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    scrollContent:{
        paddingBottom: 100
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 10,
        paddingLeft: 10,
        marginRight: 5
    },
    navImg: {
        width: 18,
        height: 18,
        resizeMode:'contain'
    },
    swiper: {
        width: '100%',
        borderRadius: 5
    },
    itemWrapper: {
        width: '100%',
        alignItems: 'center',
        marginTop: 20,
        paddingLeft: 15,
        paddingRight: 15
    },
    line: {
        width: '90%',
        marginLeft: '5%',
        height: 1,
    },
    getwayIcon:{
        width: 40, 
        height: 40, 
        marginLeft: 30, 
        resizeMode:'contain'
    },
    rightArrow:{
        width: 7, 
        height: 13, 
        marginRight:10,
        resizeMode:'contain'
    },
    loading:{
        alignItems: 'center', 
        justifyContent:'center', 
        flex: 1
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(DeviceManage)

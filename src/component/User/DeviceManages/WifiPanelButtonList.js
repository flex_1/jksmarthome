import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity,
    RefreshControl,
    DeviceEventEmitter,
    SwipeableFlatList
} from 'react-native';
import { connect } from 'react-redux';
import {GetdeviceRouter, NetUrls, NotificationKeys,NetParams,Colors} from '../../../common/Constants';
import {postJson} from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';

//侧滑最大距离
const maxSwipeDistance = 100
//侧滑按钮个数
const countSwiper = 1

// wifi面板 按键
class WifiPanelButtonList extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                navigation.getParam('showSetting') ? {text: '设置', onPress:()=>{
                    navigation.getParam('settingBtn')()
                }} : null
            ]}/>
        ),
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const {getParam, setParams} = props.navigation
        this.getwayData = getParam('getwayData')
        this.callBack = getParam('callBack')
        this.attributeType = getParam('attributeType')
        this.isZigbeePanel = this.attributeType == 'zigbee-panel'

        this.state = {
            loading: true,
            isRefreshing: false,
            listData: null
        }

        setParams({
            settingBtn: this.settingBtnClick.bind(this)
        })
    }

    componentDidMount() {
        this.requestList()
        this.deviceStatusNoti = DeviceEventEmitter.addListener(NotificationKeys.kDeviceStatusNotification,()=>{
			this.requestList()
		})
    }

    componentWillUnmount(){
        this.deviceStatusNoti.remove()
    }

    // 设置按钮点击
    settingBtnClick(){
        const {navigate,setParams} = this.props.navigation
        navigate('PanelSetting',{
            deviceId: this.getwayData?.id,
            callBack: (data)=>{
                this.callBack && this.callBack()
                this.requestList()
            }
        })
    }

    // 请求列表
    async requestList() {
        try {
            let data = await postJson({
                url: NetUrls.setmanageSecond,
                params: {
                    deviceId: this.getwayData?.id
                }
            });
            this.setState({loading: false,isRefreshing:false})
            if (data.code == 0) {
                this.setState({
                    listData: data.result || []
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({loading: false,isRefreshing:false})
            ToastManager.show('网络错误')
        }
    }

    // 修改名称
    async requestSettingName(deviceId, name) {
        SpinnerManager.show()
        try {
			let data = await postJson({
				url: NetUrls.saveDevice,
				params: {
					id: deviceId,
					name: name
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                this.requestList()
                this.callBack && this.callBack()
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    //关闭侧滑栏
    closeSwiper() {
        this.btnList && this.btnList.setState({ openRowKey: null })
    }

    renderLedICon(index){
        // let ledImg = <Image style={styles.icon} defaultSource={Images.default} source={{uri: deviceIcon}}/>
        // if(val.ledRgbw){
        //     ledImg = <View style={[styles.icon,{backgroundColor: val.ledRgbw}]}/>
        // }
        const bg1 = this.props.themeInfo.isDark ? '#FFFFFF33' : '#B4B4B41F'
        const bg2 = this.props.themeInfo.isDark ? '#000' : '#fff'
        const TextColor = this.props.themeInfo.isDark ? '#848484' : '#969799'

        return(
            <View style={[styles.iconWrapper,{backgroundColor: bg1}]}>
                <View style={[styles.iconSubWrapper,{backgroundColor: bg2}]}>
                    <Text style={[styles.iconIndex,{color: TextColor}]}>{index}</Text>
                </View>
            </View>
        )
    }
    
    //获取静态类型的Item
    renderRowItem(val,index){
        const {pop, navigate} = this.props.navigation
        const Colors = this.props.themeInfo.colors

        // 绑定的设备信息
        let bindMsgView = null
        if(val.bindingMsg){
            bindMsgView = <Text style={{fontSize:13,marginTop:5,color:Colors.themeTextLight}}>{val.bindingMsg}</Text>
        }

        //设备的楼层信息
        let roomMsgView = null
        if(val.roomMsg){
            roomMsgView = <Text style={{fontSize:12,marginTop:5,color:Colors.themeTextLight}}>{val.roomMsg}</Text>
        }

        return(
            <View
                key={'second_mange_list_'+index} 
                style={[styles.inputWrapper, {backgroundColor:Colors.themeBg}]}
            >
                {this.renderLedICon(index+1)}
                <TouchableOpacity activeOpacity={0.7} style={{marginLeft:20,flex:1}} onPress={()=>{
                    navigate('ReName',{
                        title:'重命名',
                        name: val.name,
                        callBack:(name)=>{
                            pop()
                            this.requestSettingName(val.deviceId, name)
                        }
                    })
                }}>
                    <Text style={{fontSize:15,color:Colors.themeText,fontWeight:'500'}}>{val.name}</Text>
                    {bindMsgView}
                    {roomMsgView}
                </TouchableOpacity>
                {val.bindingButtonType == 1 ? null : <TouchableOpacity style={styles.bindTouch} onPress={()=>{
                    if(this.isZigbeePanel){
                        navigate('ZigbeePanelBindDevice',{
                            buttonId: val.deviceId,
                            title: val.name,
                            callBack: ()=>{
                                this.callBack && this.callBack()
                                this.requestList()
                            }
                        })
                    }else{
                        navigate('WifiButtonBind',{
                            buttonId: val.deviceId,
                            bindingDeviceId: val.bindingDeviceId,
                            bindingSceneId: val.bindingSceneId,
                            bindingDeviceExcuteKey: val.bindingDeviceExcuteKey,
                            bindingDeviceExcuteKeyText: val.bindingDeviceExcuteKeyText,
                            title: val.name,
                            callBack: ()=>{
                                this.callBack && this.callBack()
                                this.requestList()
                            }
                        })
                    }
                }}>
                    <Text style={{fontSize: 14,color: Colors.themeTextLight}}>绑定</Text>
                    <Image style={styles.rightArrow} source={require('../../../images/enterLight.png')}/>
                </TouchableOpacity>}
            </View>
        )
    }

    getQuickActions(rowData, index){
        if(rowData.bindingButtonType != 1){
            return null
        }
        if(!this.isZigbeePanel){
            return null
        }
        const {navigate} = this.props.navigation

        return (
            <View style={styles.quickAContent}>
                <TouchableOpacity
                    activeOpacity={0.7} 
                    style={styles.quick} 
                    onPress={()=>{
					    this.closeSwiper()
					    navigate('Setting_Timing',{
                            isJuniorUser: this.props.isJuniorUser,
                            deviceData: {
                                id: rowData.deviceId
                            }
                        })
				    }}
                >
					<Image style={{width:40,height:40}} source={require('../../../images/deviceIcon/time.png')}/>
				</TouchableOpacity>
            </View>
        )
    }

    //获取总列表
    getListView() {
        const Colors = this.props.themeInfo.colors;
    
        if(this.state.loading && !this.state.listData){
            return (
                <View style={{alignItems: 'center', marginTop: '50%'}}>
                    <Text style={{color: Colors.themeTextLight}}>加载中...</Text>
                </View>
            )
        }
        if(!this.state.listData){
            return (
                <View style={{alignItems: 'center', marginTop: '50%'}}>
                    <Text style={{color: Colors.themeTextLight}}>网络错误，请稍后重试</Text>
                </View>
            )
        }
        if(this.state.listData.length <= 0) {
            return (
                <View style={{alignItems: 'center', marginTop: '50%'}}>
                    <Text style={{color: Colors.themeTextLight}}>暂无设备</Text>
                </View>
            )
        }

        return (
            <SwipeableFlatList
                ref={e => this.btnList = e}
                contentContainerStyle={styles.contentStyle}
                scrollIndicatorInsets={{right: 1}}
                removeClippedSubviews={false}
                data={this.state.listData}
                keyExtractor={(item, index)=> 'index_'+index}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
                initialNumToRender={10}
                renderQuickActions={({ item, index }) => this.getQuickActions(item,index)}//创建侧滑菜单
                maxSwipeDistance={maxSwipeDistance}
                bounceFirstRowOnMount={false}
                refreshControl={
                    <RefreshControl
                        colors={[Colors.activeIndiColor]}
                        tintColor={Colors.activeIndiColor}
						refreshing={this.state.isRefreshing}
						onRefresh={() => {
							if (this.state.isRefreshing) return
							this.setState({ isRefreshing: true })
							this.requestList()
						}}
					/>
                }
            />
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors;

        return (
            <View style={{flex: 1,backgroundColor: Colors.themeBaseBg}}>
                {this.getListView()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    contentStyle:{
        paddingBottom: 50,
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 16,
    },
    icon:{
        width: 40, 
        height: 40, 
        marginLeft: 16, 
        resizeMode:'contain',
        borderRadius: 20
    },
    inputWrapper:{
		height: 80,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:15,
        marginHorizontal: 16
    },
    statusTextWrapper:{
		height:30,
		borderRadius:15,
		borderWidth:1,
		justifyContent:'center',
		alignItems:'center',
		marginRight:10,
		paddingHorizontal: 15,
    },
    switchBtn:{
        paddingHorizontal: 10,
        height:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    switchBtnTouch:{
		width:90,
		height:34,
		borderRadius:17,
		borderWidth:1,
		justifyContent:'center',
		alignItems:'center'
    },
    lockIcon:{ 
        width: 16, 
        height: 20, 
        resizeMode: 'contain', 
        marginRight: 15 
    },
    switchTouch:{
        height:'100%',
        paddingHorizontal: 16,
        justifyContent:'center'
    },
    sliderItem:{
        width: '100%',
		borderRadius:5,
		marginTop:15
    },
    sliderTouch: {
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        height: 65
    },
    bindTouch:{
        height: '100%',
        paddingHorizontal:20,
        justifyContent:'center',
        alignItems:'center',
        marginLeft: 10,
        flexDirection: 'row'
    },
    rightArrow:{
        width: 7, 
        height: 13, 
        marginLeft:5,
        resizeMode:'contain'
    },
    iconWrapper:{
        width: 36, 
        height:36, 
        borderRadius: 18, 
        marginLeft: 16,
        justifyContent:'center',
        alignItems:'center',
    },
    iconSubWrapper:{
        width: 22, 
        height:22, 
        borderRadius: 11,
        justifyContent:'center',
        alignItems:'center',
    },
    iconIndex:{
        fontSize:14, 
        fontWeight: '400'
    },
    //侧滑菜单的样式
    // quickAContent: {
    //     flex: 1,
    //     flexDirection: 'row',
    //     justifyContent: 'flex-end',
    //     marginRight: '5%',
    //     marginTop: 15,
    //     overflow: 'hidden',
    //     borderTopRightRadius: 5,
    //     borderBottomRightRadius: 5,
    //     paddingVertical: 1
    // },
    // quick: {
    //     backgroundColor: Colors.white,
    //     flex: 1,
    //     alignItems: 'center',
    //     justifyContent: 'center',
    //     width: maxSwipeDistance / countSwiper,
    // },
    quickAContent: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginRight: 16,
		marginTop: 12,
		overflow: 'hidden',
	},
	quick: {
		alignItems: 'center',
		justifyContent: 'center',
		width: maxSwipeDistance / countSwiper,
	},
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(WifiPanelButtonList)

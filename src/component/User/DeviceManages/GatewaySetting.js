/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	DeviceEventEmitter,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
    Alert,
    ScrollView,
    Dimensions,
	Modal
} from 'react-native';
import { connect } from 'react-redux';
import { NotificationKeys, NetUrls, Colors } from '../../../common/Constants';
import { postJson } from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { StatusBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import ActionSheet from 'react-native-actionsheet';

const screenW = Dimensions.get('screen').width;

class GatewaySetting extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'设置'}/>,
        headerBackground: <HeaderBackground/>
    })

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.getwayData = getParam('getwayData') || {}
		this.callBack = getParam('callBack')
        this.isMultiGateway = getParam('isMultiGateway')
		
		this.state = {
			name: this.getwayData.name,
			getwayData: {},
			modalVisible: false,
			netamode: '',
			netamodeStatus: null,
			addr485: '',
			activeTime: '',
			pickerTitle: '',
			pickerDataAdr: this.getPickerData(1),
			pickerDataTime: this.getPickerData(3),
			selectData: [],
			pickerType: 1,  //1:总线地址  3:待机时间,
            isHiddenConfigWIFI: 1,   //1隐藏  0显示
            hasNewVersion: false,
            isShowUpgrade: false,
            showReplaceDevice: true
		}
	}

	componentDidMount() {
		this.requestGetwayInfo()
		// console.log(this.getwayData);
	}

	componentWillUnmount() {
		
	}

	getPickerData(type){
		let pickerData = []
		let count = type == 1 ? 128 : 255
		for(let i=0; i<=count; i++){
			pickerData.push(i)
		}
		return pickerData
	}

	// 获取网关信息
	async requestGetwayInfo() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.getwayDevice,
				params: {	
					id: this.getwayData.id,
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
				this.setState({
					getwayData: data.result || {},
					netamode: data.result?.netamode,
					netamodeStatus: data.result?.netamodeStatus,
					addr485: data.result?.addr485,
					activeTime: data.result?.activeTime,
                    isHiddenConfigWIFI: data.result?.isHiddenConfigWIFI,
                    hasNewVersion: data.result?.hasNewVersion,
                    isShowUpgrade: data.result?.isShowUpgrade,
                    // showReplaceDevice: data.result?.showReplaceDevice
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}
	
	// 修改网关
	async updateDevice(name) {
		const { pop } = this.props.navigation
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.saveGateWay,
				params: {	
					id: this.getwayData.id,
					name: name,
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
				this.requestGetwayInfo()
				this.callBack && this.callBack({name: name})
				ToastManager.show('更改成功')
				pop()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	// 修改Wifi面板设置
	async updateWifiPanel(params, callBack) {
		params =  params || {}
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.updatePanel,
				params: {
					...params,
					id: this.getwayData.id
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
				callBack && callBack()
				ToastManager.show('设置成功')
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	//删除网关
    async deleteGetway() {
		const {popToTop,pop,navigate} = this.props.navigation
		
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.delGateway,
                params: {
                    id: this.getwayData?.id
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.callBack && this.callBack()
                
		        DeviceEventEmitter.emit(NotificationKeys.kRoomInfoNotification)
                ToastManager.show('网关解绑成功')
                pop(2)
            } else if(data.code == 1000){
                navigate('UnbindGateway',{
                    title: '解绑网关',
                    type: 3,
                    deviceData: data.result,
                    ids: [this.getwayData?.id]
                })
            } else{
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

    //设置网关图片
    async saveGatewayImage(imgId){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.gatewayPicture,
				params: {
                    contentId: imgId,
                    id: this.getwayData.id
                }
			});
            SpinnerManager.close()
			if (data.code == 0) {
				this.requestGetwayInfo()
                DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
	}

	handlePickerSelectData(data,index){
		if(this.state.pickerType == 1){
			this.updateWifiPanel({type: 1, addr485: data[0]}, ()=>{
				this.setState({
					addr485: data[0]
				})
			})
		}else if(this.state.pickerType == 3){
			this.updateWifiPanel({type: 3, activeTime: data[0]}, ()=>{
				this.setState({
					activeTime: data[0]
				})
			})
		}
	}

	renderWifiIcon(){
		const Colors = this.props.themeInfo.colors;
		const {rssi,gatewayType} = this.state.getwayData

		if(gatewayType != 2){
			return null
		}

		let wifiIcon = null
		if(rssi == 1){
			wifiIcon = require('../../../images/connectWifi/wifi_1.png')
		}else if(rssi == 2){
			wifiIcon = require('../../../images/connectWifi/wifi_2.png')
		}else if(rssi == 3){
			wifiIcon = require('../../../images/connectWifi/wifi_3.png')
		}else{
			return null
		}

		return(
			<View style={{flexDirection: 'row',alignItems: 'center',marginTop: 10}}>
				<Text style={{color: Colors.themeText, fontSize: 14, fontWeight:'400'}}>网络信号</Text>
				<Image style={{width: 24,height: 24,marginLeft: 10}} source={wifiIcon}/>
			</View>
		)
	}
					
	getDetailInfos() {
		const Colors = this.props.themeInfo.colors;
		const {name,producerCode,producerName,serialNumber,mac,nightIcon,dayIcon} = this.state.getwayData
        const {navigate} = this.props.navigation
        const icon = this.props.themeInfo.isDark ? nightIcon : dayIcon
        let gatewayNum = null
        try {
            gatewayNum = serialNumber.split('_')[1]
        } catch (error) {
            
        }

		return(
			<View style={styles.imgTouch}>
				<TouchableOpacity activeOpacity={0.7} style={{justifyContent: 'center',alignItems:'center'}} onPress={()=>{
					navigate('ControllerThumb',{callBack: (data)=>{
						this.saveGatewayImage(data.id)
					}})
				}}>
					<Image style={[styles.img,{backgroundColor:Colors.themeBg}]} source={{uri:icon}}/>
					<Text style={{marginTop:20,marginBottom: 10,fontSize:18,fontWeight:'bold',color: Colors.themeText}}>{name}</Text>
				</TouchableOpacity>
				{producerCode ? <Text style={{marginTop:5,fontSize:14,color:Colors.themeTextLight}}>产品型号: {producerCode}</Text> : null}
				{serialNumber ? <Text style={{marginTop:5,fontSize:14,color:Colors.themeTextLight}}>序列号: {serialNumber}</Text> : null}
                {gatewayNum ? <Text style={{marginTop:5,fontSize:14,color:Colors.themeTextLight}}>网关编号: {gatewayNum}</Text> : null}
				{producerName ? <Text style={{marginTop:5,fontSize:14,color:Colors.themeTextLight}}>厂商: {producerName}</Text> : null}
				{mac ? <Text style={{marginTop:5,fontSize:15,color:Colors.themeTextLight}}>MAC: {mac}</Text> : null}
				{this.renderWifiIcon()}
			</View>
		)
	}

    rednerFuctionSwitch(){
        if(this.isMultiGateway){
            return null
        }

        const {list,name,statusDesc} = this.state.getwayData
        const Colors = this.props.themeInfo.colors;
        const {navigate} = this.props.navigation

        return(
            <>
                <TouchableOpacity activeOpacity={0.7} style={styles.settingTouch} onPress={()=>{
					navigate('ControllerSetting_StartUsing',{
                        listData:list,
                        name:name,
                        getwayId: this.getwayData.id,
                        callBack:()=>{
						    this.requestGetwayInfo()
						    DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
						    DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
						    DeviceEventEmitter.emit(NotificationKeys.kSmartListChangeNotification)
					    }
                    })
				}}>
					<View style={{width: 6, height: 6,borderRadius:3,backgroundColor:Colors.themeButton}}/>
					<Text style={{marginLeft: 15,flex: 1,fontSize:15, color: Colors.themeText}}>功能停启用</Text>
                    {/* <Text numberOfLines={1} style={[styles.extraText, {color: Colors.themeTextLight}]}>{statusDesc}</Text> */}
					<Image style={{width: 7, height: 13, resizeMode:'contain'}} source={require('../../../images/enterLight.png')}/>
				</TouchableOpacity>
            </>
        )
    }

    _renderUpgradeItem(){
        if(!this.state.isShowUpgrade){
            return null
        }
		const Colors = this.props.themeInfo.colors;
        const hasNewVersion = this.state.hasNewVersion
        const {navigate} = this.props.navigation

		return(
            <>
            <View style={[styles.split, {backgroundColor: Colors.split}]}/>
			<TouchableOpacity activeOpacity={0.7} style={styles.settingTouch} onPress={()=>{
				navigate('FirmwareUpgrade',{getwayData: this.getwayData})
			}}>
				<View style={{width: 6, height: 6,borderRadius:3,backgroundColor:Colors.themeButton}}/>
				<Text style={{marginLeft: 15,flex: 1,fontSize:15, color: Colors.themeText}}>固件升级</Text>
                {hasNewVersion ? <View style={styles.redDot}/> : null}
				{hasNewVersion ? <Text numberOfLines={1} style={[styles.extraText, {color: Colors.themeTextLight}]}>有新固件</Text> : null}
				<Image style={{width: 7, height: 13, resizeMode:'contain'}} source={require('../../../images/enterLight.png')}/>
			</TouchableOpacity>
            </>
		)
	}

    _renderSettingItem(title, callBack, extraText){
		const Colors = this.props.themeInfo.colors;

		return(
			<TouchableOpacity activeOpacity={0.7} style={styles.settingTouch} onPress={()=>{
				callBack && callBack()
			}}>
				<View style={{width: 6, height: 6,borderRadius:3,backgroundColor:Colors.themeButton}}/>
				<Text style={{marginLeft: 15,flex: 1,fontSize:15, color: Colors.themeText}}>{title}</Text>
				{extraText == null ? null : <Text numberOfLines={1} style={[styles.extraText, {color: Colors.themeTextLight}]}>{extraText}</Text>}
				<Image style={{width: 7, height: 13, resizeMode:'contain'}} source={require('../../../images/enterLight.png')}/>
			</TouchableOpacity>
		)
	}

	// wifi面板特殊选项
	renderWifiPanelItems(){
		const {gatewayType} = this.state.getwayData
        const {showReplaceDevice} = this.state
        const {navigate} = this.props.navigation
		const Colors = this.props.themeInfo.colors;

		if(gatewayType != 2){
			return null
		}
		return(
			<>
                <View style={[styles.split, {backgroundColor: Colors.split}]}/>
				{this._renderSettingItem('模式切换',()=>{
					this.actionSheet.show()
				}, this.state.netamode)}
				<View style={[styles.split, {backgroundColor: Colors.split}]}/>
				{this._renderSettingItem('总线地址',()=>{
					if(this.state.netamodeStatus == 0){
						ToastManager.show('当前模式不支持修改总线地址。')
						return
					}
					this.setState({
						modalVisible: true,
						pickerTitle: '总线地址',
						selectData: [this.state.addr485],
						pickerType: 1   // 总线地址
					})
				}, this.state.addr485)}
				<View style={[styles.split, {backgroundColor: Colors.split}]}/>
				{this._renderSettingItem('待机时间',()=>{
					this.setState({
						modalVisible: true,
						pickerTitle: '待机时间(秒)',
						selectData: [this.state.activeTime],
						pickerType: 3   // 待机时间
					})
				}, this.state.activeTime+'秒')}
                {showReplaceDevice ? <View style={[styles.split, {backgroundColor: Colors.split}]}/> : null}
                {showReplaceDevice ? this._renderSettingItem('设备更换',()=>{
					navigate('ReplaceDevice',{
                        getwayData: this.getwayData
                    })
				}, '') : null}
				{this._renderUpgradeItem()}
			</>
		)

	}

	getSettings(){
		const {navigate} = this.props.navigation
		const Colors = this.props.themeInfo.colors;
        const {gatewayType, panelLocation, name} = this.state.getwayData

		const nameTitle = gatewayType == 2 ? '产品名称' : '网关名称'
		const locationText = gatewayType == 2 ? panelLocation : null
        
		return(
			<View style={{marginTop:20,width:'100%',paddingHorizontal: 16}}>
				<View style={[styles.settingWrapper, {backgroundColor: Colors.themeBg}]}>
					{this._renderSettingItem(nameTitle,()=>{
						navigate('ReName',{title:'修改名称',name: name,callBack:(name)=>{
							this.updateDevice(name)
						} })
					}, name)}
					<View style={[styles.split, {backgroundColor: Colors.split}]}/>
					{this._renderSettingItem('位置',()=>{
						if(gatewayType == 2){
							navigate('Seting_Room',{
                                excuteNetwork: false,
                                callBack:(data)=>{
							        this.updateWifiPanel({type: 2, roomId: data.id}, ()=>{
										this.requestGetwayInfo()
									})
                                }
                            })
						}else{
							navigate('ControllerSetting_Location',{getwayData: this.getwayData, callBack:()=>{
								this.requestGetwayInfo()
							}})
						}
					}, locationText)}
					<View style={[styles.split, {backgroundColor: Colors.split}]}/>
					{this.rednerFuctionSwitch()}
					{this.renderWifiPanelItems()}
                    {this.state.isHiddenConfigWIFI ? null : <View style={[styles.split, {backgroundColor: Colors.split}]}/>  }
					{this.state.isHiddenConfigWIFI ? null : this._renderSettingItem('配网',()=>{
						navigate('NetworkSetting',{parentId: this.getwayData?.id})
					})}
				</View>
			</View>
		)
	}
	

	getDeleteButton() {
		const Colors = this.props.themeInfo.colors;

        return (
            <View style={styles.bottomBtnWrapper}>
                <TouchableOpacity
                    style={[styles.deleteTouch, {backgroundColor: Colors.themeBg}]}
                    onPress={() => {
                        Alert.alert(
                            '提示',
                            '确认解绑? 解绑后，该网关下的设备都会被解绑。',
                            [ {text: '取消', onPress: () => {}, style: 'cancel'},
                            { text: '解绑', onPress: () => {this.deleteGetway()} }]
                        )
                    }}
                >
                    <Text style={{color: Colors.red, fontSize: 15}}>解除绑定</Text>
                </TouchableOpacity>
            </View>
        )
    }

	// 获取选择框的 Modal
	getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showNumberPicker()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
	}

	// 弹出 模式切换 Modal
    showNumberPicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerType == 1 ? this.state.pickerDataAdr : this.state.pickerDataTime,
            pickerTitleText: this.state.pickerTitle,
            selectedValue: this.state.selectData,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false
                },()=>{
					this.handlePickerSelectData(data,index);
				})
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                }, () => {
                    window.CustomPicker.hide()
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

	getActionSheet() {
		return (
			<ActionSheet
                title='模式切换'
				ref={e => this.actionSheet = e}
				options={['本地模式','网络模式','总线模式', '取消']}
				cancelButtonIndex={3}
				onPress={(index) => {
                    if(index != 3){
						let label = '本地模式'
						if(index == 1) label = '网络模式'
						if(index == 2) label = '总线模式'
						this.updateWifiPanel({type: 4, status: index}, ()=>{
							this.setState({
								netamode: label,
								netamodeStatus: index
							})
						})
					}
				}}
			/>
		)
	}

	render() {
		const Colors = this.props.themeInfo.colors;

		return (
			<View style={{backgroundColor: Colors.themeBaseBg, flex: 1}}>
				<ScrollView
               	 	contentContainerStyle={styles.scrollContain}
            	>
					{this.getDetailInfos()}
					{this.getSettings()}
					{this.getDeleteButton()}
				</ScrollView>
				{this.getModal()}
				{this.getActionSheet()}
			</View>
            
		)
	}
}

const styles = StyleSheet.create({
	settingWrapper: {
		paddingVertical:5,
		paddingHorizontal: 16,
		width: '100%',
		borderRadius: 5
	},
	deleteTouch: {
        width: '100%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
		borderRadius: 4
    },
    bottomBtnWrapper: {
        marginTop: 20,
        width: '100%',
        paddingHorizontal: 16,
        paddingVertical: 10
	},
	split:{
		width: '100%', 
		height: 1, 
	},
	settingTouch:{
		width:'100%',
		height:50,
		flexDirection: 'row',
		alignItems:'center'
    },
    extraText:{
        fontSize: 14,
        marginRight: 5,
        textAlign: 'right'
    },
    scrollContain:{
        paddingBottom: BottomSafeMargin + 20
    },
    imgTouch:{
        marginTop: 20,
        justifyContent:'center',
        alignItems:'center',
        width:'100%'
    },
    img:{
        width:screenW*0.4,
        height:screenW*0.4,
        resizeMode:'contain'
    },
	modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    redDot:{
        backgroundColor: Colors.themBgRed,
        width: 8,
        height: 8,
        borderRadius: 4,
        marginRight: 5

    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(GatewaySetting)

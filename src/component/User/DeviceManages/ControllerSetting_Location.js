/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
    Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import { Colors,NetUrls } from '../../../common/Constants';
import { postPicture,postJson } from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { StatusBarHeight } from '../../../util/ScreenUtil';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';

class ControllerSetting_Location extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'位置'}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text: '保存', onPress:()=>{
                    navigation.getParam('saveBtnClick')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground/>
    })

	constructor(props) {
		super(props);
		const {getParam,setParams} = this.props.navigation
		this.callBack = getParam('callBack')
		this.getwayData = getParam('getwayData') || {}

		this.state = {
			avatarSource: null,
			describe: ''
		}

		setParams({
			saveBtnClick: ()=>{
				this.requestLoadLocation()
			}
		})
	}

	componentDidMount() {
		this.requestLocationInfo()
	}

	//获取网关位置信息
	async requestLocationInfo() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.getwayLocation,
				params: {	
					id: this.getwayData.id,
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
				this.setState({
					getwayData: data.result || {},
					avatarSource: (data.result && data.result.img && {uri: data.result.img} ) || null ,
					describe: (data.result && data.result.describe) || ''
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}


	// 上传位置信息
	async requestLoadLocation() {
        Keyboard.dismiss()
        if (!this.state.describe) {
            ToastManager.show('请输入位置信息')
            return
		}
		const {pop} = this.props.navigation
        SpinnerManager.show()
        try {
            let data = await postPicture({
                url: NetUrls.controllerLocation,
                filePath: this.state.avatarSource && this.state.avatarSource.uri,
                fileName: this.getwayData.name + 'location.jpg',
                params: {
                    id: this.getwayData.id,
                    describe: this.state.describe,
                }
            })
            SpinnerManager.close()
            if (data && data.code == 0) {
				ToastManager.show('信息已提交')
				this.callBack && this.callBack()
                pop()
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

	getActionSheet() {
		return (
			<ActionSheet
				ref={e => this.actionSheet = e}
				title={'上传或拍照'}
				options={['从相册中选择', '拍一张', '取消']}
				cancelButtonIndex={2}
				onPress={(index) => {
					const { navigate } = this.props.navigation
					if (index == 0) {
						this.pickImage()
					} else if (index == 1) {
						this.pickImagewithCamera()
					}
				}}
			/>
		)
	}

	pickImage(){
		ImagePicker.openPicker({
			width: 200,
			height: 200,
			cropping: true,
			mediaType: 'photo',
			compressImageQuality: 1,
		}).then(image => {
			console.log(image);
			this.setState({
				avatarSource: {uri: image.path}
			})
		}).catch(err=>{
			console.log(err);
		})
	}

	pickImagewithCamera(){
		ImagePicker.openCamera({
			width: 200,
			height: 200,
			cropping: true,
			mediaType: 'photo',
			compressImageQuality: 1,
		}).then(image => {
			this.setState({
				avatarSource: {uri: image.path}
			})
			console.log(image);
		}).catch(err=>{
			console.log(err);
		})
	}

	getPicture(){
		const Colors = this.props.themeInfo.colors;

		let imgView = <Image style={{width:40,height:35,resizeMode:'contain'}} source={require('../../../images/user_manage/camera.png')}/>
		
		if(this.state.avatarSource){
			imgView = <Image style={{width:'100%',height:'100%',resizeMode:'contain'}} source={this.state.avatarSource}/>
		}
		return(
			<View style={{marginTop:20,paddingHorizontal:16}}>
				<TouchableOpacity activeOpacity={0.7} style={[styles.picTouch, {backgroundColor:Colors.themeBg}]} onPress={()=>{
					this.actionSheet.show()
				}}>
					{imgView}
				</TouchableOpacity>
			</View>
		)
	}

	getInput(){
		const Colors = this.props.themeInfo.colors;
		return(
			<View style={{marginTop:20,paddingHorizontal:16}}>
				<TextInput
					style={[styles.input, {backgroundColor:Colors.themeBg, color:Colors.themeText}]}
					value={this.state.describe}
                    multiline={true}
                    blurOnSubmit={false}
                    autoFocus={false}
                    placeholderTextColor={Colors.themeInactive}
                    returnKeyLabel={'换行'}
                    returnKeyType={'default'}
                    underlineColorAndroid="transparent"
                    placeholder={"请在此输入有关电器的位置描述.."}
                    onChangeText={(text)=>{
						this.setState({
							describe: text
						})
                    }}
				/>
				<Text style={{marginTop:20,fontSize:14,color:Colors.themeText}}>请拍摄照片记录电器位置信息，以便日后维修、维护。</Text>
			</View>
		)
	}

	render() {
		const Colors = this.props.themeInfo.colors;

		return (
			<ScrollView style={{backgroundColor: Colors.themeBaseBg}}>
				{this.getPicture()}
				{this.getInput()}
				{this.getActionSheet()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 16,
	},
	picTouch:{
		width:120,
		height:120,
		justifyContent:'center',
		alignItems:'center',
		borderRadius:4
	},
	input:{
		width:'100%',
		height:115,
		paddingTop:10,
		paddingBottom:10,
		paddingHorizontal:16,
      	textAlignVertical:'top',
		fontSize:14,
      	borderRadius:4,
	}
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ControllerSetting_Location)

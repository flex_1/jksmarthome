/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Animated,
	Dimensions,
    Modal,
    Keyboard,
    Alert,
    BackHandler
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import {ColorsDark, ColorsLight} from '../../../common/Themes';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';
import HeaderTitle from '../../CommonPage/Navigation/HeaderTitle';
import PCtrPanel from '../../CommonPage/ProtocolControlPanel';
import ProtocolConfig from './ProtocolConfig';

const PCPanelStyleList = [  {id: 1, name: '样式1', componet:PCtrPanel.PCtrPanel1},
                            {id: 2, name: '样式2', componet:PCtrPanel.PCtrPanel2},
                            {id: 3, name: '样式3', componet:PCtrPanel.PCtrPanel3},
                            {id: 4, name: '样式4', componet:PCtrPanel.PCtrPanel4}
                        ]

class ProtocolControlSetting extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={navigation.getParam('title')}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
		super(props);
		const { getParam,setParams } = props.navigation;
        
        this.deviceId = getParam('deviceId')
        this.brand = getParam('brand')   // 设备类型
        this.gatewayId = getParam('gatewayId')

        this.state = {
            currentStyleId: 1,
            currentComponet: PCtrPanel.PCtrPanel1,
            showFunctionModal: false,
            currentBtnParams: {},
            currentBtnIndex: 0,
            isCustomBtn: false,
            buttonList: null,
            customButtonList: null,
            baudRateList: [],
            deviceType: 0,
            isDeleting: false
        }
    }

	componentDidMount() {
        this.requestButtonInfo({deviceId: this.deviceId})
	}

	componentWillUnmount(){

    }

    // 请求列表
    async requestButtonInfo(params) {
        SpinnerManager.show()
        try {
            let data = await postJson({ 
                url: NetUrls.initStyleInstructionProtocol,
                params: {
                    ...params
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                let currentComponet = PCtrPanel.PCtrPanelCustom
                if(this.brand != 'OTHER'){
                    const styleIndex = data.result?.style - 1
                    if(styleIndex < 0 || styleIndex > 3){
                        ToastManager.show('样式加载出错。')
                    }
                    currentComponet = PCPanelStyleList[styleIndex]['componet']
                }
                this.setState({
                    buttonList : data.result?.buttonList || [],
                    customButtonList: data.result?.customButtonList || [],
                    currentStyleId: data.result?.style,
                    baudRateList: data.result?.baudDate,
                    currentComponet: currentComponet,
                    deviceType: data.result?.typeId
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    // 小保存按钮
    async requestSaveProtocolButton(params, callBack) {
        params = params || {}
        if(this.brand  == 'OTHER'){
            params.style = 0
        }else{
            params.style = this.state.currentStyleId
        }
        SpinnerManager.show()
        try {
            let data = await postJson({ 
                url: NetUrls.saveInstructionProtocolButton,
                params: {
                    deviceId: this.deviceId,
                    ...params
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                callBack && callBack(data.result?.button)
                ToastManager.show('保存成功')
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    // 添加按钮
    async requestAddProtocolButton(callBack) {
        let params = params || {}
        if(this.brand  == 'OTHER'){
            params.style = 0
        }else{
            params.style = this.state.currentStyleId
        }
        SpinnerManager.show()
        try {
            let data = await postJson({ 
                url: NetUrls.addProtocolButton,
                params: {
                    deviceId: this.deviceId,
                    gatewayId: this.gatewayId,
                    ...params
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                callBack && callBack(data.result?.button)
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    // 删除按钮
    async requestDeleteProtocolButton(params, callBack) {
        params = params || {}
        if(this.brand  == 'OTHER'){
            params.style = 0
        }else{
            params.style = this.state.currentStyleId
        }
        SpinnerManager.show()
        try {
            let data = await postJson({ 
                url: NetUrls.deleteProtocolButton,
                params: {
                    deviceId: this.deviceId,
                    ...params
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                callBack && callBack()
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    // 按钮点击
    panelBtnClick(index, isCustomBtn){
        // 删除状态下按钮
        if(this.state.isDeleting && isCustomBtn){
            let btnData = this.state.customButtonList[index] || {}
            let params = {buttonId: btnData.buttonId}
            this.requestDeleteProtocolButton(params, ()=>{
                this.state.customButtonList.splice(index, 1)
                this.setState({
                    customButtonList: this.state.customButtonList
                })
            })
            return
        }

        // 普通状态下点击按钮
        if(!isCustomBtn && !this.state.buttonList){
            ToastManager.show('数据加载中，请稍后。')
            return
        }else if(isCustomBtn && !this.state.customButtonList){
            ToastManager.show('数据加载中，请稍后。')
            return
        }
        let btnParams = {}
        if(isCustomBtn){
            btnParams = this.state.customButtonList[index]
        }else{
            btnParams = this.state.buttonList[index]
        }
        this.setState({
            showFunctionModal: true,
            currentBtnIndex: index,
            currentBtnParams: btnParams,
            isCustomBtn: isCustomBtn
        })
    }

    //按钮设置 保存按钮点击事件
    panelBtnCompleteClick(params){
        let btnParams = {}
        if(this.state.isCustomBtn){
            btnParams = this.state.customButtonList[this.state.currentBtnIndex] || {}
        }else{
            btnParams = this.state.buttonList[this.state.currentBtnIndex] || {}
        }

        params = params || {}
        btnParams.instructionProtocolStyleButtonId = params.instructionProtocolStyleButtonId
        btnParams.name = params.name
        btnParams.hard = params.hard
        btnParams.baud = params.baud
        btnParams.parity = params.parity
        btnParams.stop = params.stop
        btnParams.cmdType = params.cmdType
        btnParams.cmd = params.cmd
        
        this.requestSaveProtocolButton(btnParams, (cusParams)=>{
            if(this.state.isCustomBtn){
                this.state.customButtonList[this.state.currentBtnIndex] = cusParams?.buttonId ? cusParams : btnParams
            }else{
                this.state.buttonList[this.state.currentBtnIndex] = cusParams?.buttonId ? cusParams : btnParams
            }
            this.setState({
                buttonList: this.state.buttonList,
                showFunctionModal: false
            })
        })
    }

    //添加按钮点击事件
    addBtnClick(){
        this.requestAddProtocolButton((button)=>{
            if(button){
                this.state.customButtonList.push(button)
            }
            this.setState({
                customButtonList: this.state.customButtonList
            })
        })
    }

    //删除按钮点击事件
    deleteBtnClick(){
        this.setState({
            isDeleting: !this.state.isDeleting
        })
    }

    renderTopSelection(){
        // 其他（自定义类型设备）
        if(this.brand  == 'OTHER'){
            return null
        }
        const Colors = this.props.themeInfo.colors;

        let list = PCPanelStyleList.map((value, index)=>{
            const icon = value.id == this.state.currentStyleId ? require('../../../images/ProtocolControl/selected.png')
            : require('../../../images/ProtocolControl/unselected.png')

            return(
                <TouchableOpacity key={'item_'+index} style={styles.item} onPress={()=>{
                    if(value.id != this.state.currentStyleId){
                        this.requestButtonInfo({style: value.id, deviceId: this.deviceId})
                    }
                }}>
                    <Image style={styles.selectIcon} source={icon}/>
                    <Text style={[styles.name,{color: Colors.themeText}]}>{value.name}</Text>
                </TouchableOpacity>
            )
        })

        return(
            <View style={styles.selectWrapper}>
                {list}
            </View>
        )
    }

    renderMainController(){
        if(!this.state.buttonList){
            return null
        }
        const Panel = this.state.currentComponet
        if(!Panel){
            return null
        }

        return(
            <Panel 
                isSetting={true}
                isDeleting={this.state.isDeleting}
                customButtonList={this.state.customButtonList}
                btnClick={(index, isCustomBtn)=>{
                    this.panelBtnClick(index, isCustomBtn)
                }}
                addBtnClick={()=>{
                    this.addBtnClick()
                }}
                deleteBtnClick={()=>{
                    this.deleteBtnClick()
                }}
            />
        )
    }

    renderModal(){
        if(!this.state.showFunctionModal){
            return null
        }

        let btnParams = this.state.currentBtnParams || {}
        let parityBitText = btnParams.parity == 0 ? '无校验' : (btnParams.parity == 2 ? '偶校验' : '奇校验')

        return(
            <View style={styles.modalWrapper}>
                <View style={styles.topWrapper}/>
                <View style={styles.bottomWrapper}>
                    <ProtocolConfig
                        configType={3}
                        deviceId={this.deviceId}
                        btnNumber={btnParams.btn}
                        buttonId={btnParams.buttonId}
                        isCustomBtn={this.state.isCustomBtn}
                        instructionProtocolStyleButtonId={btnParams.instructionProtocolStyleButtonId}
                        deviceType={this.state.deviceType}
                        btnName={btnParams.name}
                        connectType={btnParams.hard}
                        baudRate={btnParams.baud}
                        baudRateList={this.state.baudRateList}
                        parityBit={btnParams.parity}
                        parityBitText={parityBitText}
                        stopBit={btnParams.stop}
                        sendType={btnParams.cmdType}
                        sendText={btnParams.cmd}
                        cancelBtnClick={()=>{
                            this.setState({
                                showFunctionModal: false
                            })
                        }}
                        completeClick={(params)=>{
                            this.panelBtnCompleteClick(params)
                        }}
                    />
                </View>
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors;

		return (
			<View style={[styles.wrapper,{backgroundColor: Colors.themeBaseBg}]}>
				<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
                    {this.renderTopSelection()}
                    {this.renderMainController()}
                </View>
                {this.renderModal()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	wrapper: {
        flex: 1
    },
    main:{
        marginTop: 6,
        flex: 1,
        alignItems: 'center'
    },
    mianCtrWrapper:{
        marginTop: 15, 
        flex: 1,
        width: '100%',
        alignItems: 'center'
    },
    selectWrapper:{
        flexDirection: 'row',
        marginTop: 20,
        flexWrap:'wrap',
        paddingLeft: 20,
        paddingRight: 10
    },
    item:{
        flexDirection: 'row',
        alignItems:'center',
        paddingHorizontal:5,
        paddingVertical: 6,
        marginRight: 8
    },
    selectIcon:{
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    name:{
        fontSize: 16,
        fontWeight: '400',
        marginLeft: 6,
    },
    modalWrapper:{
        position: 'absolute',
        width: '100%',
        height: '100%',
        left: 0,
        top: 0
    },
    topWrapper:{
        backgroundColor: Colors.translucent,
        flex: 3
    },
    bottomWrapper:{
        flex: 7,
        backgroundColor: Colors.white
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ProtocolControlSetting)



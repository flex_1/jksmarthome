/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
} from 'react-native';

import {Colors} from '../../../common/Constants';
import HeaderLeftView from '../../../common/CustomComponent/HeaderLeftView';
import { StatusBarHeight } from '../../../util/ScreenUtil';

class ControllerSetting_Mac extends Component {

    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: <HeaderLeftView navigation={navigation} text={'MAC'}/>,
			headerRight: <View/>
        }
    };

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation
        this.callBack = getParam('callBack')
        this.mMac = getParam('mac')
        this.state = {}
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    getMacView() {
        return (
            <View style={{flex: 1}}>
                <View style={{marginTop: 30, paddingHorizontal: 16}}>
                    <Text style={{fontSize: 18, color: Colors.themeTextBlack, fontWeight: 'bold'}}>Mac地址</Text>
                    <Text style={{marginTop: 20, color: Colors.themeTextBlack, fontSize: 15}}>{this.mMac}</Text>
                </View>
            </View>
        )
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                {this.getMacView()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        flex: 1
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 15,
        paddingLeft: 10,
        marginRight: 5
    },
    navText: {
        color: Colors.tabActiveColor,
        fontSize: 15
    },
    itemWrapper: {
        width: '100%',
        paddingVertical: 5,
        backgroundColor: Colors.white,
        borderRadius: 4,
        marginTop: 15,
        //阴影四连
        shadowOffset: {width: 0, height: 2},
        shadowColor: '#070F26',
        shadowOpacity: 0.15,
        shadowRadius: 4,
        elevation: 5
    },
    item: {
        height: 54,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    }
});

export default ControllerSetting_Mac

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Animated,
	Dimensions,
    Modal,
    Keyboard,
    Alert,
    ImageBackground
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class ProtocolConfig extends Component {

	constructor(props) {
		super(props);
        
        this.configType = props.configType   //1-新建协议器  2-修改协议器  3-修改按钮
        this.gatewayId = props.gatewayId
        this.deviceId = props.deviceId
        this.isCustomBtn = props.isCustomBtn
        this.btnNumber = props.btnNumber   //按钮设置专属
        this.buttonId = props.buttonId

        this.state = {
            modalVisible: false,
            pickerData: [],
            pickerTitle: '',
            selectValue: [],
            pickerType: 1,

            typeNameList: props.typeNameList,
            typeIdList: props.typeIdList,
            deviceType: props.deviceType,
            deviceTypeText: props.deviceTypeText,
            deviceAdrress: props.deviceAdrress,
            connectType: props.connectType,
            baudRate: props.baudRate,
            baudRateList: props.baudRateList,
            parityBit: props.parityBit,   //0-无校验  2-偶校验  3-奇校验
            parityBitText: props.parityBitText,
            stopBit: props.stopBit,
            sendType: props.sendType,
            sendText: props.sendText,
            btnName: props.btnName,  // 自定义按钮时，有该属性
            adrressList: props.adrressList,
            instructionProtocolStyleButtonId: props.instructionProtocolStyleButtonId
        }
    }

	componentDidMount() {
        
	}

	componentWillUnmount(){
        
    }

    // 测试发送 addInstructionProtocol
    async requestSendTest() {
        let params = {}
        if(this.configType == 3){
            params.test = 0
            params.btn = this.btnNumber
            params.gatewayId = this.deviceId
            params.buttonId = this.buttonId
            // params.deviceId = this.deviceId
        }else{
            params.test = 1
            params.btn = 1
            params.gatewayId = this.gatewayId
        }
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.testSendCmd,
                params: {
                    instructionProtocolStyleButtonId: this.state.instructionProtocolStyleButtonId,
                    address: this.state.deviceAdrress,
                    typeId: this.state.deviceType,
                    hard: this.state.connectType,
                    baud: this.state.baudRate,
                    parity: this.state.parityBit,
                    stop: this.state.stopBit,
                    cmdType: this.state.sendType,
                    cmd: this.state.sendText,
                    ...params
                }
            });
            SpinnerManager.close()
            ToastManager.show(data.msg)
        } catch (error) {
            this.setState({loading: false,isRefreshing:false})
            ToastManager.show('网络错误')
        }
    }

    // 判断字符是否是16进制字符
    is16HexCha(cha){
        if((cha >='0' && cha <='9') || (cha >='A' && cha <='F') || (cha >='a' && cha <='f') || (cha == ' ')){
            return true
        }
        return false
    }

    handleTextForHex(text){
        let newText = ''
        if(text){
            for (const cha of text) {
                if(this.is16HexCha(cha)){
                    newText = newText + cha.toUpperCase()
                }
            }
        }
        this.setState({
            sendText: newText
        })
    }

    // type: 1-设备种类 2-设备地址 3-波特率 4-校验位 5-停止位
    onItemClick(title, subTitle, type){
        let pickerData = []
        if(type == 1){
            pickerData = JSON.parse(JSON.stringify(this.state.typeNameList))
        }else if(type == 2){
            pickerData = JSON.parse(JSON.stringify(this.state.adrressList))
        }else if(type == 3){
            pickerData = this.state.baudRateList
        }else if(type == 4){
            pickerData = ['无校验','偶校验','奇校验']
        }else if(type == 5){
            pickerData = [1,2,3,4]
        }
        this.setState({
            pickerTitle: title,
            pickerType: type,
            pickerData: pickerData,
            modalVisible: true,
            selectValue: [subTitle]
        })
    }

    // type: 1-设备种类 2-设备地址 3-波特率 4-校验位 5-停止位
    pickerConfirmClick(data, index){
        if(!data || data.length <= 0){
            return
        }
        const type = this.state.pickerType
        if(type == 1){
            this.setState({
                deviceTypeText: data[0],
                deviceType: this.state.typeIdList[index[0]]
            })
        }else if(type == 2){
            this.setState({
                deviceAdrress: data[0]
            })
        }else if(type == 3){
            this.setState({
                baudRate: data[0]
            })
        }else if(type == 4){
            let parityArr = [0,2,3]
            this.setState({
                parityBitText: data[0],
                parityBit: parityArr[index[0]]
            })
        }else if(type == 5){
            this.setState({
                stopBit: data[0]
            })
        }
    }

    renderNameInputItem(){
        if(this.configType != 3 || !this.isCustomBtn){
            return null
        }

        const Colors = this.props.themeInfo.colors;
        return(
            <>
                <View style={[styles.itemWrapper,{height: 70}]}>
                    <Text style={[styles.nameTitle,{color: Colors.themeText}]}>按键名称</Text>
                    <View style={[styles.nameInputWrapper,{marginLeft: 15}]}>
                        <TextInput
                            style={[styles.input,{textAlign: 'right',color: Colors.themeText}]}
                            value={this.state.btnName}
                            returnKeyType={'done'}
                            placeholder={'修改名称'}
                            enablesReturnKeyAutomatically={true}
                            maxLength={4}
                            onChangeText={(text) => {
                                this.setState({
                                    btnName: text
                                })
                            }}
                        />
                    </View>
                </View>
                <View style={[styles.line,{backgroundColor: Colors.themeBaseBg}]}/>
            </>
        )
    }

    // type: 1-连接方式  2-发送方式 
    renderSelectItem(title,type){
        const Colors = this.props.themeInfo.colors;
        const selectType = type == 1 ? this.state.connectType : this.state.sendType
        const selections = type == 1 ? [{name: 'RS485',value:0}, {name: 'RS232',value:1}, {name: 'RSTTL',value:2}]
            : [{name: '字符串',value:0}, {name: '16进制',value:1}, {name: '字符串+回车',value:2}]

        return(
            <View style={styles.itemWrapper}>
                <Text style={[styles.title,{color: Colors.themeText}]}>{title}</Text>
                {selections.map((value, index)=>{
                    const icon = selectType == value.value ? require('../../../images/ProtocolControl/selected.png')
                        : require('../../../images/ProtocolControl/unselected.png')
                    
                    return(
                        <TouchableOpacity key={index} style={styles.selectWrapper} onPress={()=>{
                            if(type == 1){
                                this.setState({connectType: value.value})
                            }else{
                                lastType = this.state.sendType
                                nextType = value.value
                                if(nextType == 1  && (lastType == 0 || lastType == 2)){
                                    this.setState({
                                        sendText: ''
                                    })
                                }else if((nextType == 0 || nextType == 2) && lastType == 1){
                                    this.setState({
                                        sendText: ''
                                    })
                                }
                                this.setState({sendType: value.value})
                            }
                        }}>
                            <Image style={styles.selectIcon} source={icon}/>
                            <Text style={[styles.name,{color: Colors.themeText}]}>{value.name}</Text>
                        </TouchableOpacity>
                    )
                })}
            </View>
        )
    }

    renderArrowItem(title, subTitle, type){
        let arrowIcon = <Image style={styles.arrow} source={require('../../../images/ProtocolControl/arrow.png')}/>
        const Colors = this.props.themeInfo.colors;

        if(this.configType == 3 && (type == 1 || type == 2)){
            return null
        }else if(this.configType == 2 && type == 1){
            arrowIcon = null
        }else if(type == 2){
            arrowIcon = null
        }
        
        return(
            <>
                <TouchableOpacity activeOpacity={0.7} style={styles.itemWrapper} onPress={()=>{
                    if(this.configType == 2 && type == 1){
                        return null
                    }else if(type == 2){
                        return null
                    }
                    this.onItemClick(title, subTitle, type)
                }}>
                    <Text style={[styles.title,{color: Colors.themeText}]}>{title}</Text>
                    <Text style={[styles.name,{color: Colors.themeTextLight}]}>{subTitle}</Text>
                    {arrowIcon}
                </TouchableOpacity>
                <View style={[styles.line,{backgroundColor: Colors.themeBaseBg}]}/>
            </>
        )
    }

    renderInputItem(){
        const Colors = this.props.themeInfo.colors;
        
        return(
            <View style={[styles.itemWrapper,{height: 70}]}>
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={[styles.input,{color: Colors.themeText}]}
                        value={this.state.sendText}
                        returnKeyType={'send'}
                        enablesReturnKeyAutomatically={true}
                        placeholder={'测试码'}
                        onChangeText={(text) => {
                            if(this.state.sendType != 1){
                                this.setState({
                                    sendText: text
                                })
                            }else{
                                this.handleTextForHex(text)
                            }
                        }}
                        onSubmitEditing={()=>{
                            this.requestSendTest()
                        }}
                    />
                </View>
                <TouchableOpacity style={styles.sendTouch} onPress={()=>{
                    if(!this.state.sendText){
                        ToastManager.show('请输入发送数据')
                        return
                    }
                    this.requestSendTest()
                }}>
                    <Text style={styles.sendText}>测试发送</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderMainConfig(){
        const Colors = this.props.themeInfo.colors;

        return(
            <View style={{marginTop: 5,flex: 1}}>
                {this.renderNameInputItem()}
                {this.renderArrowItem('设备种类', this.state.deviceTypeText, 1)}
                {this.renderArrowItem('设备地址', this.state.deviceAdrress, 2)}
                {this.renderSelectItem('连接方式',1)}
                <View style={[styles.line,{backgroundColor: Colors.themeBaseBg}]}/>
                {this.renderArrowItem('波特率', this.state.baudRate, 3)}
                {this.renderArrowItem('校验位', this.state.parityBitText, 4)}
                {this.renderArrowItem('停止位', this.state.stopBit, 5)}
                {this.renderSelectItem('发送方式',2)}
                <View style={[styles.line,{backgroundColor: Colors.themeBaseBg}]}/>
                {this.renderInputItem()}
            </View>
        )
    }

    renderCancelNextBtn(){
        const Colors = this.props.themeInfo.colors;

        return(
            <View style={styles.bottomTouchWrapper}>
                <TouchableOpacity style={styles.cancelTouch} onPress={()=>{
                    this.props.cancelBtnClick()
                }}>
                    <Text style={[styles.btnText,{color: Colors.themeText}]}>返回</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.saveTouch} onPress={()=>{
                    if(this.isCustomBtn && !this.state.btnName){
                        ToastManager.show('按钮名不能为空')
                        return
                    }
                    let params = {
                        instructionProtocolStyleButtonId: this.state.instructionProtocolStyleButtonId,
                        name: this.state.btnName,
                        hard: this.state.connectType,
                        baud: this.state.baudRate,
                        parity: this.state.parityBit,
                        stop: this.state.stopBit,
                        cmdType: this.state.sendType,
                        cmd: this.state.sendText
                    }
                    this.props.completeClick(params)
                }}>
                    <Text style={[styles.btnText,{color: Colors.white}]}>保存</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderNextButton(){
        if(this.configType == 3){
            return this.renderCancelNextBtn()
        }

        const btnText = this.configType == 2 ? '保存' : '下一步'
        return(
            <TouchableOpacity style={styles.sendButton} onPress={()=>{
                let params = {
                    address: this.state.deviceAdrress,
                    typeId: this.state.deviceType,
                    hard: this.state.connectType,
                    baud: this.state.baudRate,
                    parity: this.state.parityBit,
                    stop: this.state.stopBit,
                    cmdType: this.state.sendType,
                    cmd: this.state.sendText
                }
                this.props.completeClick(params)
            }}>
                <Text style={styles.nextText}>{btnText}</Text>
            </TouchableOpacity>
        )
    }

    getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerData,
            pickerTitleText: this.state.pickerTitle,
            selectedValue: this.state.selectValue,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false,
                    selectValue: data
                },()=>{
                    this.pickerConfirmClick(data, index)
                })
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

	render() {
        const Colors = this.props.themeInfo.colors;

		return (
			<View style={[styles.wrapper,{backgroundColor: Colors.themeBg}]}>
                <KeyboardAwareScrollView contentContainerStyle={styles.content} extraScrollHeight={20} extraHeight={300}>
                    {this.renderMainConfig()}
                    {this.renderNextButton()}
                </KeyboardAwareScrollView>
                {this.getModal()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	wrapper: {
        flex: 1
    },
    content:{
        paddingBottom: BottomSafeMargin + 20
    },
    title:{
        fontSize: 16,
        fontWeight: '400',
        flex: 1
    },
    nameTitle:{
        fontSize: 16,
        fontWeight: '400',
    },
    selectWrapper:{
        flexDirection: 'row',
        alignItems: 'center',
        height: '100%',
        marginLeft: 10
    },
    selectIcon:{
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    name:{
        fontSize: 14,
        fontWeight: '400',
        marginLeft: 5
    },
    itemWrapper:{
        height: 54,
        flexDirection: 'row',
        alignItems:'center',
        paddingHorizontal: 16
    },
    line:{
        height: 1,
        width:'100%'
    },
    arrow:{
        marginLeft: 5,
        width: 16,
        height: 16
    },
    sendTouch:{
        justifyContent:'center',
        alignItems:'center',
        width:100,
        height: 44,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#0075F9',
        marginLeft: 8
    },
    sendText:{
       fontSize: 16,
       color: '#008CFF',
       fontWeight: '400'
    },
    nameInputWrapper:{
        height: 44,
        flex: 1,
        paddingHorizontal: 10
    },
    inputWrapper:{
        backgroundColor: '#ADADAD1F',
        borderRadius: 4,
        height: 44,
        flex: 1,
        paddingHorizontal: 10
    },
    input:{
        height: '100%'
    },
    sendButton:{
        marginTop: 20,
        marginHorizontal: 16,
        height: 44,
        borderRadius: 22,
        backgroundColor: '#008CFF',
        justifyContent: 'center',
        alignItems: 'center'
    },
    nextText:{
        fontSize: 16,
        fontWeight: '400',
        color: Colors.white
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    cancelTouch:{
        height: 40,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#DCDEE0',
        flex: 1
    },
    saveTouch:{
        flex: 1,
        height: 40,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#008CFF',
        marginLeft: 12
    },
    bottomTouchWrapper:{
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20,
        paddingHorizontal: 16
    },
    btnText:{
        fontSize: 16,
        fontWeight: '400'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ProtocolConfig)



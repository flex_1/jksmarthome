/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	StyleSheet,
	Image,
	Text,
	View,
	FlatList
} from 'react-native';
import { connect } from 'react-redux';
import { NetUrls, Colors } from '../../../common/Constants';
import { postJson } from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../../CommonPage/Navigation/HeaderTitle';
import DateUtil from '../../../util/DateUtil';

class FirmwareUpgradeLog extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={'升级日志'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.getwayData = getParam('getwayData') || {}
		
		this.state = {
			logList: []
		}
	}

	componentDidMount() {
		this.requestGetwayInfo()
	}

	componentWillUnmount() {
		
	}

	// 获取网关信息
	async requestGetwayInfo() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.upgradePanelLog,
				params: {	
					id: this.getwayData.id
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
				this.setState({
					logList: data.result?.list || []
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    renderRowItem(rowData, index){
        const Colors = this.props.themeInfo.colors;
        let statusColor = rowData.status == 3 ? Colors.red : Colors.newBtnBlueBg
        let statusText = ''
        if(rowData.status == 1){
            statusText = '已提交'
        }else if(rowData.status == 2){
            statusText = '升级成功'
        }else if(rowData.status == 3){
            statusText = '升级失败'
        }

        return(
            <View style={styles.item}>
                <View style={{flexDirection: 'row',alignItems: 'center'}}>
                    <Text style={{fontSize: 15,fontWeight:'400',flex: 1,color: Colors.themeText}}>升级前版本: {rowData.currentVersion}</Text>
                    <Text style={{fontSize: 14,fontWeight:'400',color: Colors.themeTextLight}}>{DateUtil(rowData.createTime)}</Text>
                </View>
                <View style={{flexDirection: 'row',alignItems: 'center',marginTop: 8}}>
                    <Text style={{fontSize: 15,fontWeight:'400',flex: 1,color: Colors.themeText}}>升级后版本: {rowData.newVersion}</Text>
                    <Text style={{fontSize: 15,fontWeight:'400',color: statusColor}}>{statusText}</Text>
                </View>
                <View style={[styles.split,{backgroundColor: Colors.themeBaseBg}]}/>
            </View>
        )
    }

    renderLogListView(){
        const Colors = this.props.themeInfo.colors;
        if(!this.state.logList){
            return null
        }
        if(this.state.logList.length <= 1){
            return (
                <View style={styles.noContentWrapper}>
                    <Text style={{fontSize: 15, color: Colors.themeTextLight}}>暂无升级日志</Text>
                </View>
            )
        }
        return(
            <View style={{flex:1}}>
                <FlatList
                    scrollIndicatorInsets={{right: 1}}
				    contentContainerStyle={{paddingBottom: 50}}
				    data={this.state.logList}
				    keyExtractor={(item, index) => 'log_'+index}
				    renderItem={({ item, index }) => this.renderRowItem(item, index)}
				    initialNumToRender={10}
			    />
		    </View>
        )
    }

	render() {
		const Colors = this.props.themeInfo.colors;

		return (
			<View style={{backgroundColor: Colors.themeBg, flex: 1}}>
				{this.renderLogListView()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
    item:{
        paddingHorizontal: 16,
        paddingVertical: 16
    },
	split:{
        position:'absolute',
        height:1,
        right: 0,
        top: 0,
        left: 0
    },
    noContentWrapper:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(FirmwareUpgradeLog)

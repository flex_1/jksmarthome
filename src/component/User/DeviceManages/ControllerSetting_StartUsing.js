/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	DeviceEventEmitter,
} from 'react-native';
import { connect } from 'react-redux';
import { Colors,NetUrls } from '../../../common/Constants';
import HeaderLeftView from '../../../common/CustomComponent/HeaderLeftView';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { StatusBarHeight } from '../../../util/ScreenUtil';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';

class ControllerSetting_StartUsing extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'功能停启用'}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text: '确定', onPress:()=>{
                    navigation.getParam('sureBtnClick')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground/>
    })

	constructor(props) {
		super(props);
		const { getParam, setParams } = this.props.navigation
		this.callBack = getParam('callBack')
		this.listData = getParam('listData') || []
		this.name = getParam('name')
		this.getwayId = getParam('getwayId')
		this.allIds = []

		this.state = {
			activeDeviceIds: [],
            unactiveDeviceIds: []
        }
        
        setParams({
            sureBtnClick: this.requestControllerFunction.bind(this)
        })
	}

	componentDidMount() {
		this.getDefaultIds()
	}

	componentWillUnmount() {
		
	}

    getDefaultIds(){
		let activeDeviceIds = []
        let unactiveDeviceIds = []
		for (const val of this.listData) {
			this.allIds.push(val.id)

            if(val.isStop){
                unactiveDeviceIds.push(val.id)
            }else{
                activeDeviceIds.push(val.id)
            }
			// if(val.houseId){
			// 	activeDeviceIds.push(val.id)
			// }else{
            //     unactiveDeviceIds.push(val.id)
            // }
		}
		this.setState({
            activeDeviceIds: activeDeviceIds,
            unactiveDeviceIds: unactiveDeviceIds
        })
	}

	//控制器 控制 功能停启用
	async requestControllerFunction() {
		const {pop, navigate} = this.props.navigation
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.getwayFunction,
				params: {
					parentId: this.getwayId,
					deviceIds: this.state.activeDeviceIds.toString(),
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.callBack && this.callBack()
				pop()
				ToastManager.show('设置成功')
			}else if(data.code == 1000){
                // 如果该设备在场景或智能下，需要去确认
				navigate('UnbindGateway',{
                    title: '停用设备', 
                    type: 2, 
                    deviceData: data.result,
                    ids: this.state.unactiveDeviceIds,
                    callBack: this.callBack
                })
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	getList() {
		if(!this.listData || this.listData.length<=0){
			return null
		}
        const Colors = this.props.themeInfo.colors;
        
		let devices = this.listData.map((val, index) => {

			let selectView = <View style={[styles.selectedView, {borderColor: Colors.borderLight}]} />
            let idIndex = this.state.activeDeviceIds.indexOf(val.id)
            let icon = this.props.themeInfo.isDark ? val.nightIcon : val.dayIcon
			
			if(idIndex >= 0){
				selectView = <Image style={{width:18,height:18}} source={require('../../../images/select.png')}/>
			}
			if(val.parentId == 0){
				selectView = null
			}

			return (
				<View key={'device_stting_item_' + index} style={{ alignItems: 'center', paddingHorizontal: 16 }}>
					<View style={{ width: '100%', height: 1, backgroundColor: Colors.split }} />
					<TouchableOpacity activeOpacity={0.8} style={styles.item} onPress={() => {
						if(idIndex == -1){
							this.state.activeDeviceIds.push(val.id)
                            this.state.unactiveDeviceIds.remove(val.id)
						}else{
							this.state.activeDeviceIds.remove(val.id)
                            this.state.unactiveDeviceIds.push(val.id)
						}
						this.setState({
							activeDeviceIds: this.state.activeDeviceIds,
                            unactiveDeviceIds: this.state.unactiveDeviceIds
						})
					}}>
						
						<Image style={{ width: 30, height: 30,resizeMode:'contain' }} source={{uri: icon}} />
						<Text style={{ marginLeft: 15, fontSize: 15, color: Colors.themeText, flex: 1 }}>{val.name}</Text>
						{selectView}
					</TouchableOpacity>
				</View>
			)
		})
		
		// 全选按钮
		let allSelectImg = require('../../../images/rectangle_unselect.png')
		if(this.state.activeDeviceIds.length == this.listData.length){
			allSelectImg = require('../../../images/rectangle_select.png')
		}
		return (
			<View style={{ paddingHorizontal: 16, paddingVertical: 10 }}>
				<View style={[styles.itemWrapper, {backgroundColor: Colors.themeBg}]}>
					<View style={styles.item}>
						<Text style={[styles.getwayName, {color: Colors.themeText}]}>{this.name}</Text>
						<TouchableOpacity activeOpacity={0.7} style={styles.selecTouch} onPress={()=>{
							if(this.state.activeDeviceIds.length == this.listData.length){
								// 全不选
								this.setState({
                                    activeDeviceIds: [],
                                    unactiveDeviceIds: JSON.parse(JSON.stringify(this.allIds))
                                })
							}else{
								// 全选
								this.setState({
                                    activeDeviceIds: JSON.parse(JSON.stringify(this.allIds)),
                                    unactiveDeviceIds: []
                                })
							}
						}}>
							<Image style={{width:18,height:18,resizeMode:'contain'}} source={allSelectImg}/>
							<Text style={{marginLeft: 10, color:Colors.themeText}}>全选</Text>
						</TouchableOpacity>
					</View>
					{devices}
				</View>
			</View>
		)
	}

	render() {
		const Colors = this.props.themeInfo.colors;

		return (
            <ScrollView 
                style={{backgroundColor: Colors.themeBaseBg}} 
                contentContainerStyle={styles.scrollContent}
            >
				{this.getList()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	scrollContent:{
		paddingBottom: 50
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight: 5
	},
	itemWrapper: {
		width: '100%',
		paddingVertical: 5,
		borderRadius: 4,
		marginTop: 15
	},
	item: {
		height: 54,
		width: '100%',
		flexDirection: 'row',
		alignItems: 'center'
	},
	selectedView:{ 
		width: 18, 
		height: 18, 
		borderWidth: 1, 
		borderRadius: 9 
	},
	selecTouch:{
		paddingHorizontal:16,
		flexDirection: 'row',
		alignItems:'center',
		justifyContent:'center'
	},
	getwayName:{ 
		marginLeft: 16, 
		fontSize: 15, 
		flex: 1,
		fontWeight:'bold' 
	}
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ControllerSetting_StartUsing)

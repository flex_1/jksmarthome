import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity,
    RefreshControl,
    DeviceEventEmitter,
    ImageBackground,
    FlatList
} from 'react-native';
import Slider from "react-native-slider";
import { connect } from 'react-redux';
import {GetdeviceRouter, NetUrls, NotificationKeys,NetParams, Colors} from '../../../common/Constants';
import {BottomSafeMargin} from '../../../util/ScreenUtil';
import {postJson} from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';
import {ImagesDark, ImagesLight} from '../../../common/Themes';
import SwitchButton from '../../../common/CustomComponent/SwitchButton';

// 网关下设备列表
class GroupManageList extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text: '设置', onPress:()=>{
                    navigation.getParam('settingBtn')()
                }},
                navigation.getParam('attributeType') == 'zigbee-gateway' ? 
                {icon:require('../../../images/add.png'),onPress:()=>{ navigation.getParam('addBtnClick')()}} : null
            ]}/>
        ),
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const {getParam, setParams} = props.navigation
        this.getwayData = getParam('getwayData')
        this.callBack = getParam('callBack')

        this.state = {
            loading: true,
            isRefreshing: false,
            listData: null
        }

        setParams({
            settingBtn: this.settingBtnClick.bind(this),
            addBtnClick: this.addBtnClick.bind(this)
        })
    }

    componentDidMount() {
        this.requestList()
        this.deviceStatusNoti = DeviceEventEmitter.addListener(NotificationKeys.kDeviceStatusNotification,()=>{
			this.requestList()
		})
    }

    componentWillUnmount(){
        this.deviceStatusNoti.remove()
    }

    // 设置按钮点击
    settingBtnClick(){
        const {navigate,setParams} = this.props.navigation
        navigate('GatewaySetting',{
            title: this.getwayData.name + '设置',
            name: this.getwayData.name,
            getwayData: this.getwayData,
            callBack: (data)=>{
                if(data && data.name){
                    setParams({title: data.name})
                }
                this.callBack && this.callBack()
            }
        })
    }

    addBtnClick(){
        const {navigate} = this.props.navigation
        navigate('ZigbeeConfigWifi',{
            getwayId: this.getwayData?.id,
            callBack: ()=>{
                this.requestList()
            }
        })
    }

    // 请求列表
    async requestList() {
        try {
            let data = await postJson({
                url: NetUrls.setmanageSecond,
                params: {
                    deviceId: this.getwayData?.id
                }
            });
            this.setState({loading: false,isRefreshing:false})
            if (data.code == 0) {
                this.setState({
                    listData: data.result || []
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({loading: false,isRefreshing:false})
            ToastManager.show('网络错误')
        }
    }

    // 设备 开关
	async changeDevieceStatus(showSpinner,id,targetStatus,call){
        if(showSpinner){
            SpinnerManager.show()
        }
		try {
			let data = await postJson({
				url: NetUrls.deviceUpdate,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: {
					status: targetStatus,
					id: id
				}
			});
            SpinnerManager.close()
			if (data.code == 0) {
				call && call()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    // 设备 控制
	async requestDeviceControl(id, status,call){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				params: {
                    deviceId: id,
                    status: status
				}
			});
            SpinnerManager.close()
			if (data.code == 0) {
				call && call()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    
    //获取静态类型的Item
    getStaticItem(val,index,floorView, lockIcon, buttonClick){
        const Colors = this.props.themeInfo.colors
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
        const deviceIcon = this.props.themeInfo.isDark ? val.nightIcon : val.dayIcon
        
        // 设备状态
        let statusTextView = null
        if(val.statusText){
            statusTextView = (
                <View style={styles.statusTextWrapper}>
                    <Text style={{fontSize: 14, color:Colors.themeTextLight}}>{val.statusText}</Text>
                </View>
            )
        }

        return(
            <TouchableOpacity activeOpacity={0.7} key={'second_mange_list_'+index} 
                style={[styles.inputWrapper, {backgroundColor:Colors.themeBg}]} onPress={()=>{
                // 进入二级页面
                buttonClick()
            }}>
                <Image style={styles.icon} defaultSource={Images.default} source={{uri: deviceIcon}}/>
                <View style={{marginLeft:20,flex:1}}>
                    <Text style={{fontSize:15,color:Colors.themeText}}>{val.name}</Text>
                    {floorView}
                </View>
                {lockIcon}
                {statusTextView}
            </TouchableOpacity>
        )
    }

    //获取开关类型Item
    getSwitchItem(val, index, floorView, lockIcon, buttonClick){
        const Colors = this.props.themeInfo.colors
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
        const deviceIcon = this.props.themeInfo.isDark ? val.nightIcon : val.dayIcon

        // 设备状态
        let activeTouch = {}
        let activeText = <Text style={{fontSize:14,color:Colors.themeTextLight}}>开启</Text>
        if(val.value > 0){
            activeTouch = {backgroundColor:Colors.themeButton,borderColor:Colors.themeButton}
            activeText = <Text style={{fontSize:14,color:Colors.white}}>已开启</Text>
        }

        return(
            <TouchableOpacity activeOpacity={0.7} key={'second_mange_list_'+index} 
                style={[styles.inputWrapper, {backgroundColor:Colors.themeBg}]} onPress={()=>{
                // 进入二级页面
                buttonClick()
            }}>
                <Image style={styles.icon} defaultSource={Images.default} source={{uri: deviceIcon}}/>
                <View style={{marginLeft:20,flex:1}}>
                    <Text style={{fontSize:15,color:Colors.themeText}}>{val.name}</Text>
                    {floorView}
                </View>
                {lockIcon}
                <SwitchButton
                    value = {val.value > 0}
                    isAsync = {true}
                    onPress = {()=>{
                        if(val.islock){
                            ToastManager.show('当前设备被锁定')
                            return
                        }
                        let target = val.value ? 0 : 1
                        this.changeDevieceStatus(true, val.deviceId,target,()=>{
                            this.state.listData[index].value = target
                            this.setState({ ...this.state })
                        })
                    }}
                />
            </TouchableOpacity>
        )
    }

    //获取开关+滑竿类型的Item
    getSwitchSliderItem(val,index,floorView, lockIcon, buttonClick){
        const Colors = this.props.themeInfo.colors
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
        const deviceIcon = this.props.themeInfo.isDark ? val.nightIcon : val.dayIcon

        //滑竿
        let sliderView = null
        if(val.value > 0){
            sliderView = (
                <View style={{height: 50, justifyContent:'center', alignItems:'center'}}>
                    <Slider
					    style={{width: '90%'}}
					    minimumValue={0}
                        maximumValue={100}
          			    value={val.value}
					    onSlidingComplete={(value)=>{
                            this.changeDevieceStatus(false, val.deviceId,Math.floor(value),()=>{})
                        }}
					    minimumTrackTintColor={Colors.themeButton}
					    maximumTrackTintColor={Colors.trackBg}
					    thumbTintColor={Colors.themeButton}
					    trackStyle={{height:6,borderRadius: 3}}
                        thumbStyle={{width:20,height:20,borderRadius:10}}
        		    />
                </View>
            )
        }

        return(
            <View key={'second_mange_list_'+index} style={[styles.sliderItem, {backgroundColor:Colors.themeBg}]}>
                <TouchableOpacity activeOpacity={0.7} style={styles.sliderTouch} onPress={()=>{
                    buttonClick()
                }}>
                    <Image style={styles.icon} defaultSource={Images.default} source={{uri: deviceIcon}}/>
                    <View style={{marginLeft:20,flex:1}}>
                        <Text style={{fontSize:15,color:Colors.themeText}}>{val.name}</Text>
                        {floorView}
                    </View>
                    {lockIcon}
                    <SwitchButton
                        isAsync = {lockIcon != null}
                        value = {val.value > 0}
                        onPress = {()=>{
                            let target = val.value ? 0 : 1
                            this.changeDevieceStatus(false, val.deviceId, target,()=>{
                                this.state.listData[index].value = target
                                this.setState({ ...this.state })
                            })
                        }}
                    />
                </TouchableOpacity>
                {sliderView}
            </View>
        )
    }

    //获取滑竿类型的Item(窗帘)
    getSliderItem(val,index,floorView, lockIcon, buttonClick){
        const Colors = this.props.themeInfo.colors
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
        const deviceIcon = this.props.themeInfo.isDark ? val.nightIcon : val.dayIcon

        return(
            <View key={'second_mange_list_'+index} style={[styles.sliderItem, {backgroundColor:Colors.themeBg}]}>
                <TouchableOpacity activeOpacity={0.7} style={styles.sliderTouch} onPress={()=>{
                    buttonClick()
                }}>
                    <Image style={styles.icon} defaultSource={Images.default} source={{uri: deviceIcon}}/>
                    <View style={{marginLeft:20,flex:1}}>
                        <Text style={{fontSize:15,color:Colors.themeText}}>{val.name}</Text>
                        {floorView}
                    </View>
                    {lockIcon}
                </TouchableOpacity>
                <View style={{height: 55, justifyContent:'center', alignItems:'center'}}>
                    <View style={{width: '90%',flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                        <Text style={{fontSize:12,color:Colors.themeTextLight}}>全开</Text>
                        <Text style={{fontSize:12,color:Colors.themeTextLight}}>全关</Text>
                    </View>
                    <Slider
					    style={{width: '90%'}}
					    minimumValue={0}
                        maximumValue={100}
          			    value={val.value}
                        disabled={val.islock ? true:false}
                        onSlidingStart={()=>{
                            if(val.islock){
                                ToastManager.show('设备已上锁，暂无法控制。')
                            }
                        }}
					    onSlidingComplete={(value)=>{
                            if(val.islock){
                                return
                            }
                            this.changeDevieceStatus(false, val.deviceId,Math.floor(value),()=>{})
                        }}
					    minimumTrackTintColor={Colors.themeButton}
					    maximumTrackTintColor={Colors.trackBg}
					    thumbTintColor={Colors.themeButton}
					    trackStyle={{height:6,borderRadius: 3}}
                        thumbStyle={{width:20,height:20,borderRadius:10}}
        		    />
                </View>
            </View>
        )
    }

    //获按钮类型的Item(射频窗帘)
    getRFBtnItem(val,index,floorView, lockIcon, buttonClick){
        const Colors = this.props.themeInfo.colors
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
        const deviceIcon = this.props.themeInfo.isDark ? val.nightIcon : val.dayIcon

        const openIcon = val.type == 5 ? require('../../../images/ProtocolControl/curtain_out.png') 
        : require('../../../images/ProtocolControl/curtain_up.png')  
        const shutIcon = val.type == 5 ? require('../../../images/ProtocolControl/curtain_in.png') 
        :require('../../../images/ProtocolControl/curtain_down.png') 

        return(
            <View key={'second_mange_list_'+index} style={[styles.sliderItem, {backgroundColor:Colors.themeBg}]}>
                <TouchableOpacity activeOpacity={0.7} style={styles.sliderTouch} onPress={()=>{
                    buttonClick()
                }}>
                    <Image style={styles.icon} defaultSource={Images.default} source={{uri: deviceIcon}}/>
                    <View style={{marginLeft:20,flex:1}}>
                        <Text style={{fontSize:15,color:Colors.themeText}}>{val.name}</Text>
                        {floorView}
                    </View>
                    {lockIcon}
                </TouchableOpacity>
                <View style={styles.ctrWrapper}>
                    <TouchableOpacity onPress={()=>{ this.requestDeviceControl(val.deviceId, 1) }}>
                        <ImageBackground style={styles.btnBg} source={require('../../../images/ProtocolControl/btn5.png')}>
                            <Image style={{width:18,height:18}} source={openIcon}/>
                        </ImageBackground>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{ this.requestDeviceControl(val.deviceId, 3) }}>
                        <ImageBackground style={styles.btnBg} source={require('../../../images/ProtocolControl/btn5.png')}>
                            <Image style={{width:18,height:18}} source={require('../../../images/ProtocolControl/curtain_pause.png')}/>
                        </ImageBackground>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{ this.requestDeviceControl(val.deviceId, 2) }}>
                        <ImageBackground style={styles.btnBg} source={require('../../../images/ProtocolControl/btn5.png')}>
                            <Image style={{width:18,height:18}} source={shutIcon}/>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    _devicesExtraUniqueKey(item, index){
		return "devices_index_" + index;
	}

    renderNoContent(){
        const Colors = this.props.themeInfo.colors;

        if(this.state.loading && !this.state.listData){
            return (
                <View style={styles.loading}>
                    <Text style={{color: Colors.themeTextLight}}>加载中...</Text>
                </View>
            )
        }
        if(!this.state.listData){
            return (
                <View style={styles.loading}>
                    <Text style={{color: Colors.themeTextLight}}>网络错误，请稍后重试</Text>
                </View>
            )
        }
        if(this.state.listData.length <= 0) {
            return (
                <View style={styles.loading}>
                    <Text style={{marginTop: 10,color: Colors.themeTextLight}}>暂无设备</Text>
                </View>
            )
        }
    }

    renderRowItem(rowData, index){
        const Colors = this.props.themeInfo.colors;

        // 楼层房间信息
        let floorView = null
        if(rowData.floorText && rowData.roomName){
            floorView = <Text style={{fontSize:14,marginTop:5,color:Colors.themeTextLight}}>{rowData.floorText + '  ' + rowData.roomName}</Text>
        }

        // 设备是否 锁定
        let lockIcon = null
        if(rowData.islock){
            lockIcon = <Image style={styles.lockIcon} source={require('../../../images/deviceIcon/lock.png')}/>
        }

        // 按钮点击事件
        let buttonClick = ()=>{
            const {navigate,push} = this.props.navigation
            const routerName = GetdeviceRouter(rowData.attributeTypeName)

            if(!routerName || routerName == 'Camera'){
                push('DeviceSetting',{deviceData:rowData, isFromManager: true})
            }else{
                push(routerName,{title:rowData.name, deviceData:rowData})
            }
            // 进入二级页面
            // navigate('DeviceSetting', {
            //     deviceData: {deviceId:val.id},
            //     isFromManager:true
            // })
        }

        if(rowData.type == 1){
            return this.getSwitchItem(rowData, index, floorView, lockIcon, buttonClick)
        }else if(rowData.type == 2){
            return this.getSwitchSliderItem(rowData, index, floorView, lockIcon, buttonClick)
        }else if(rowData.type == 3){
            return this.getSliderItem(rowData, index, floorView, lockIcon, buttonClick)
        }else if(rowData.type == 4 || rowData.type == 5){
            return this.getRFBtnItem(rowData, index, floorView, lockIcon, buttonClick)
        }else{
            return this.getStaticItem(rowData, index, floorView, lockIcon, buttonClick)
        }
    }

    renderList(){
        const Colors = this.props.themeInfo.colors;

        return(
            <FlatList
                ref={e => this.swiperflatlist = e}
                contentContainerStyle={styles.contentStyle}
                scrollIndicatorInsets={{right: 1}}
                removeClippedSubviews={false}
                data={this.state.listData}
                keyExtractor={this._devicesExtraUniqueKey}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
                initialNumToRender={10}
                ListEmptyComponent = {this.renderNoContent()}
                refreshControl={
					<RefreshControl
                        colors={[Colors.activeIndiColor]}
                        tintColor={Colors.activeIndiColor}
						refreshing={this.state.isRefreshing}
						onRefresh={() => {
							if (this.state.isRefreshing) return
							this.setState({ isRefreshing: true })
							this.requestList()
						}}
					/>
				}
            />
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors;

        return (
            <View style={{flex: 1, backgroundColor: Colors.themeBaseBg}}>
                {this.renderList()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    scrollContent:{
        paddingBottom: 50,
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 16,
    },
    icon:{
        width: 40, 
        height: 40, 
        marginLeft: 16, 
        resizeMode:'contain'
    },
    inputWrapper:{
		
		height: 65,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:15,
        marginHorizontal: 16
    },
    statusTextWrapper:{
		height:30,
		justifyContent:'center',
		alignItems:'center',
		marginRight:10,
		paddingHorizontal: 15,
    },
    switchBtn:{
        paddingHorizontal: 10,
        height:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    switchBtnTouch:{
		width:90,
		height:34,
		borderRadius:17,
		borderWidth:1,
		justifyContent:'center',
		alignItems:'center'
    },
    lockIcon:{ 
        width: 16, 
        height: 20, 
        resizeMode: 'contain', 
        marginRight: 15 
    },
    switchTouch:{
        height:'100%',
        paddingHorizontal: 16,
        justifyContent:'center'
    },
    sliderItem:{
        width: '100%',
		borderRadius:5,
		marginTop:15
    },
    sliderTouch: {
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        height: 60
    },
    btnBg:{
        width:72, 
        height:30,  
        resizeMode:'contain',
        justifyContent: 'center',
        alignItems: 'center'
    },
    ctrWrapper:{
        height: 50, 
        flexDirection:'row',
        justifyContent:'space-between', 
        alignItems:'center',
        paddingHorizontal: 20
    },
    loading:{
        alignItems: 'center',
        marginTop: '50%'
    },
    contentStyle:{
        paddingBottom: BottomSafeMargin + 20
    },
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(GroupManageList)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import { 
	Platform, 
	StyleSheet, 
	Image, 
	Text, 
	View, 
	TouchableOpacity, 
	ScrollView, 
} from 'react-native';
import { connect } from 'react-redux';
import { Colors, NetUrls } from '../../../common/Constants';
import { postJson } from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../../CommonPage/Navigation/HeaderTitle';
import {ImagesDark, ImagesLight} from '../../../common/Themes';

class ControllerThumb extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeft navigation={navigation}/>,
			headerTitle: <HeaderTitle title={'选择图标'}/>,
            headerBackground: <HeaderBackground hiddenBorder={true}/>
		}
	}

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.callBack = getParam('callBack')
		
		this.state = {
			thumbs: []
		}
	}

	componentDidMount() {
		this.getThumbList()
	}

	componentWillUnmount() {

	}

	async getThumbList(params){
		try {
			let data = await postJson({
				url: NetUrls.getControllerPicture,
				params: params
			});
			if (data.code == 0) {
				this.setState({
					thumbs : data.result || []
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
	}

	getThumbs(){
		const {pop} = this.props.navigation
		const Colors = this.props.themeInfo.colors;
        const defaultImg = this.props.themeInfo.isDark ? ImagesDark.default : ImagesLight.default
        
		let thumbsList = this.state.thumbs.map((val,index)=>{
            let imgUrl = this.props.themeInfo.isDark ? val.nightImg : val.dayImg
            
			return(
				<View style={styles.thumbWrapper} key={index}>
					<TouchableOpacity style={styles.thumbTouch} key={index} onPress={()=>{
						this.callBack && this.callBack(val)
						pop()
					}}>
						<View style={styles.imgWrapper}>
							<Image style={styles.thumbImg} defaultSource={defaultImg} source={{uri:imgUrl}}/>
						</View>
						<Text style={[styles.thumbName,{color: Colors.themeText}]}>{val.name}</Text>
					</TouchableOpacity>
				</View>
			)
		})

		return(
			<View style={{flexDirection:'row',flexWrap:'wrap',}}>
				{thumbsList}
			</View>
		)
	}

	render() {
		const Colors = this.props.themeInfo.colors;

		return (
			<ScrollView style={{backgroundColor: Colors.themeBg}} contentContainerStyle={styles.contentStyle}>
				{this.getThumbs()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	contentStyle:{
		paddingBottom:50
	},
	thumbTouch: {
		justifyContent:'center',
		alignItems:'center',
	},
	imgWrapper:{
		justifyContent:'center',
		alignItems:'center',
		width: 100,
		height: 100,
		borderRadius: 5
    },
    thumbWrapper:{
        width:'33%',
        marginTop:10,
        justifyContent:'center',
        alignItems:'center'
    },
    thumbName:{
        textAlign:'center',
        fontSize:14,
        marginTop:10
    },
    thumbImg:{
        width:'60%',
        height:'60%',
        resizeMode: 'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ControllerThumb)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	Text,
    TouchableOpacity,
    ScrollView,
    Image
} from 'react-native';
import { Colors, NetUrls} from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../../CommonPage/Navigation/HeaderTitle';
import ProtocolConfig from './ProtocolConfig';


class ProtocolControlCreat extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={navigation.getParam('title')}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
		super(props);
		const { getParam,setParams } = props.navigation;
        
        this.gatewayId = getParam('gatewayId')
        this.deviceId = getParam('deviceId')
        this.callBack = getParam('callBack')
        this.attributeType = getParam('attributeType')

        this.state = {
            modalVisible: false,
            pickerData: [],
            pickerTitle: '',
            selectValue: [],
            pickerType: 1,
            
            typeList: [],
            typeNameList: [],
            typeIdList: [],
            deviceType: '',
            deviceTypeText: '',
            adrressList: [],
            deviceAdrress: 1,
            connectType: 0,
            baudRate: '',
            baudRateList: [],
            parityBit: 0,   //0-无校验  2-偶校验  3-奇校验
            parityBitText: '无校验',
            stopBit: 1,
            sendType: 0,
            sendText: '',
        }
    }

	componentDidMount() {
        if(this.attributeType == 'instruction-protocol-gateway'){
            this.requestProtocolType()
        }else{
            this.requestCommonProtocolType()
        }
	}

	componentWillUnmount(){
        
    }

    //通用协议器种类
    async requestCommonProtocolType(){
        SpinnerManager.show()
        try {
            let data = await postJson({ 
                url: NetUrls.commonProtocolType,
                params: {
                    gatewayId: this.gatewayId,
                    id: this.deviceId
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.setState({
                    typeList: data.result?.typeList || []
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    // 请求列表
    async requestProtocolType() {
        SpinnerManager.show()
        try {
            let data = await postJson({ 
                url: NetUrls.instructionProtocolType,
                params: {
                    gatewayId: this.gatewayId,
                    id: this.deviceId
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                let typeList = data.result?.typeList || []
                let typeNameList = []
                let typeIdList = []
                for (const type of typeList) {
                    typeNameList.push(type.typeName)
                    typeIdList.push(type.typeId)
                }
                if(data.result?.deviceInfo){
                    let deviceInfo = JSON.parse(data.result.deviceInfo)
                    let typeIdIndex = typeIdList.indexOf(deviceInfo.instructionProtocolTypeId)
                    this.setState({
                        deviceAdrress: deviceInfo.address,
                        connectType: deviceInfo.hard,
                        baudRate: deviceInfo.baud,
                        parityBit: deviceInfo.parity,
                        parityBitText: deviceInfo.parity == 0 ? '无校验' : (deviceInfo.parity == 2 ? '偶校验' : '奇校验'),
                        stopBit: deviceInfo.stop,
                        sendType: deviceInfo.cmdType,
                        deviceType: typeIdList[typeIdIndex],
                        deviceTypeText: typeNameList[typeIdIndex],
                        sendText: deviceInfo.cmd
                    })
                }else{
                    this.setState({
                        baudRate: data.result?.baudDate[0],
                        deviceTypeText: typeNameList[0],
                        deviceAdrress: data.result?.addressDate[0],
                        deviceType: typeIdList[0]
                    })
                }
                this.setState({
                    typeList: typeList,
                    typeNameList: typeNameList,
                    typeIdList: typeIdList,
                    baudRateList: data.result?.baudDate,
                    adrressList: data.result?.addressDate
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    // 创建协议器
    async requestCreatProtocol(params) {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.addInstructionProtocol,
                params: {
                    deviceId: this.deviceId,
                    gatewayId: this.gatewayId,
                    ...params
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                const {navigate,pop} = this.props.navigation
                this.callBack && this.callBack()
                pop()
                if(!this.deviceId){
                    navigate('ProtocolControlSetting',{
                        deviceId: data.result?.id,
                        title: data.result?.name,
                        typeId: this.state.deviceType,
                        gatewayId: this.gatewayId,
                        brand: data.result?.brand,
                    })
                }
            }else{
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({loading: false,isRefreshing:false})
            ToastManager.show('网络错误')
        }
    }

    // 创建协议器(红外)
    async requestCreatCommonProtocol(params) {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.addCommonProtocol,
                params: {
                    gatewayId: this.gatewayId,
                    ...params
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                const {navigate,pop} = this.props.navigation
                this.callBack && this.callBack()
                pop()
                navigate('RIStudySetting',{
                    deviceId: data.result?.id,
                    title: data.result?.name,
                    typeId: params.typeId,
                    gatewayId: this.gatewayId,
                    brand: data.result?.brand,
                })
            }else{
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({loading: false,isRefreshing:false})
            ToastManager.show('网络错误')
        }
    }

    renderMianList(){
        if(!this.state.typeIdList || this.state.typeIdList.length<=0){
            return null
        }
        if(this.attributeType != 'instruction-protocol-gateway'){
            return null
        }

        return(
            <ProtocolConfig
                gatewayId={this.gatewayId}
                configType={this.deviceId ? 2:1}
                typeIdList={this.state.typeIdList}
                typeNameList={this.state.typeNameList}
                deviceType={this.state.deviceType}
                deviceTypeText={this.state.deviceTypeText}
                deviceAdrress={this.state.deviceAdrress}
                connectType={this.state.connectType}
                baudRate={this.state.baudRate}
                baudRateList={this.state.baudRateList}
                adrressList={this.state.adrressList}
                parityBit={this.state.parityBit}
                parityBitText={this.state.parityBitText}
                stopBit={this.state.stopBit}
                sendType={this.state.sendType}
                sendText={this.state.sendText}
                completeClick={(params)=>{
                    this.requestCreatProtocol(params)
                }}
            />
        )
    }

    renderTypeList(){
        if(this.attributeType == 'instruction-protocol-gateway'){
            return null
        }
        const Colors = this.props.themeInfo.colors;

        let list = this.state.typeList.map((value, index)=>{
            return(
                <TouchableOpacity key={index} activeOpacity={0.7} style={styles.item} onPress={()=>{
                    this.requestCreatCommonProtocol({typeId: value.typeId})
                }}>
                    <Text style={styles.name}>{value.typeName}</Text>
                    <Image style={styles.arrowImg} source={require('../../../images/ProtocolControl/arrow.png')}/>
                </TouchableOpacity>
            )
        })

        return(
            <View style={styles.typeListWrapper}>
                <Text style={[styles.mainTitle,{color: Colors.themeText}]}>选择类型</Text>
                <ScrollView style={styles.list}>
                    {list}
                </ScrollView>
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors;

		return (
            <View style={[styles.wrapper,{backgroundColor: Colors.themeBg}]}>
                {this.renderMianList()}
                {this.renderTypeList()}
            </View>
        )
	}
}

const styles = StyleSheet.create({
	wrapper:{
        flex: 1
    },
    typeListWrapper:{
        width: '100%',
        flex: 1,
        alignItems: 'center',
        marginTop: 20
    },
    mainTitle:{
        fontSize: 16,
        fontWeight: '500'
    },
    item:{
        flexDirection: 'row',
        height: 50,
        alignItems: 'center',
        paddingHorizontal: 22,
        width: '100%'
    },
    name:{
        fontSize: 16,
        fontWeight: '400',
        flex: 1
    },
    list:{
        marginTop: 20,
        width: '100%'
    },
    arrowImg:{
        width: 18,
        height: 18
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ProtocolControlCreat)



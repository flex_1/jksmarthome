import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    Alert,
    DeviceEventEmitter
} from 'react-native';
import {Colors, NetUrls, NotificationKeys} from '../../../common/Constants';
import HeaderLeftView from '../../../common/CustomComponent/HeaderLeftView';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import {postJson} from '../../../util/ApiRequest';
import { BottomSafeMargin,StatusBarHeight } from '../../../util/ScreenUtil';


class ControllerSetting extends Component {

    static navigationOptions = ({navigation}) => ({
        headerLeft: <HeaderLeftView navigation={navigation} text={'控制器设置'}/>,
    })

    constructor(props) {
        super(props);
        const {getParam} = props.navigation
        this.controlId = getParam('controlId')
        this.callBack = getParam('callBack')
        this.state = {
            controlData: {}
        };
    }

    componentDidMount() {
        this.getContolSetting()
    }

    // 获取控制器 状态
    async getContolSetting() {
        try {
            let data = await postJson({
                url: NetUrls.controllerSetting,
                params: {
                    id: this.controlId
                }
            });
            if (data.code == 0) {
                this.setState({
                    controlData: data.result || {}
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    //删除控制器
    async deleteControl() {
        const {popToTop} = this.props.navigation
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.deleteContoller,
                params: {
                    id: this.controlId
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                // this.callBack && this.callBack()
		        DeviceEventEmitter.emit(NotificationKeys.kRoomInfoNotification)
                ToastManager.show('解绑成功')
                popToTop()
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

    getHeaderSection() {
        return (
            <View key={'device_stting_item_header'}
                  style={{alignItems: 'center', paddingHorizontal: 16, flexDirection: 'row', height: 54,}}>
                <Image style={{width: 25, height: 28, resizeMode: 'contain'}}
                       source={require('../../../images/controller/controller_icon.png')}/>
                <View style={{flex: 1}}/>
                <Text style={{fontSize: 15, color: Colors.themeTextBlack}}>{this.state.controlData.name}</Text>
            </View>
        )
    }

    getSection(itemsData) {
        let count = itemsData.length
        let items = itemsData.map((val, index) => {
            let arrow = null
            let splitLine = <View style={{width: '100%', height: 1, backgroundColor: Colors.splitLightGray}}/>
            if (val.showArrow) {
                arrow = <Image style={{width: 10, height: 16}} source={require('../../../images/enter_light.png')}/>
            }
            if (index == 0 || count <= 1) {
                splitLine = null
            }
            if (index == 0 && val.isHeader) {
                return this.getHeaderSection()
            }
            return (
                <View key={'device_stting_item_' + index} style={{alignItems: 'center', paddingHorizontal: 16}}>
                    {splitLine}
                    <TouchableOpacity activeOpacity={0.8} style={styles.item} onPress={() => {
                        val.action && val.action()
                    }}>
                        <Image style={{width: 6, height: 6}} source={val.icon}/>
                        <Text style={{
                            marginLeft: 15,
                            fontSize: 15,
                            color: Colors.themeTextBlack,
                            flex: 1
                        }}>{val.name}</Text>
                        <Text style={{marginRight: 10, fontSize: 15, color: Colors.themeBGInactive}}>{val.extra}</Text>
                        {arrow}
                    </TouchableOpacity>
                </View>
            )
        })
        return (
            <View style={[{height: count * 55 + 10}, styles.itemWrapper]}>
                {items}
            </View>
        )
    }


    getDeleteButton() {
        let {name} = this.state.controlData
        return (
            <View style={styles.bottomBtnWrapper}>
                <TouchableOpacity
                    style={styles.deleteTouch}
                    onPress={() => {
                        Alert.alert(
                            '提示',
                            '确认解绑控制器 : ' + name + ' ? 解绑控制器后，控制器所连接的设备都会被解绑。',
                            [
                                {
                                    text: '取消', onPress: () => {
                                    }, style: 'cancel'
                                },
                                {
                                    text: '解绑', onPress: () => {
                                        this.deleteControl()
                                    }
                                },
                            ]
                        )
                    }}
                >
                    <Text style={{color: Colors.white, fontSize: 15}}>解绑控制器</Text>
                </TouchableOpacity>
            </View>
        )
    }

    getSettingList() {
        const {navigate} = this.props.navigation
        let {name, location, mac, producerName, status, devices} = this.state.controlData

        return (
            <View style={{width: '100%', paddingHorizontal: 16, paddingVertical: 10}}>
                {this.getSection([
                    {isHeader: true},
                    {
                        icon: require('../../../images/controller/dot.png'),
                        name: '生产商',
                        extra: producerName,
                        showArrow: true,
                        action: () => {
                            navigate('ControllerSetting_Producer',{name:name,producerName: producerName})
                        }
                    },
                    {
                        icon: require('../../../images/controller/dot.png'),
                        name: '位置',
                        extra: location,
                        showArrow: true,
                        action: () => {
                            navigate('ControllerSetting_Location',{controlData:this.state.controlData,callBack:()=>{
                                this.getContolSetting()
                            }})
                        }
                    },
                    {
                        icon: require('../../../images/controller/dot.png'),
                        name: '功能停启用',
                        extra: null,
                        showArrow: true,
                        action: () => {
                            navigate('ControllerSetting_StartUsing',{controlData:this.state.controlData,callBack:()=>{
                                this.getContolSetting()
                            }})
                        }
                    },
                    {
                        icon: require('../../../images/controller/dot.png'),
                        name: '固件升级',
                        extra: '',
                        showArrow: true,
                        action: () => {
                            navigate('ControllerSetting_HardwareUpdate',{controlData:this.state.controlData})
                        }
                    }
                ])}
                {this.getSection([
                    {
                        icon: require('../../../images/controller/dot.png'),
                        name: 'MAC',
                        extra: mac,
                        showArrow: true,
                        action: () => {
                            navigate('ControllerSetting_Mac',{mac:mac})
                        }
                    },
                ])}
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView contentContainerStyle={styles.scrollContent}>
                    {this.getSettingList()}
                </ScrollView>
                {this.getDeleteButton()}
            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        flex: 1,
    },
    scrollContent:{
        paddingBottom: 50
    },
    btn: {
        width: '100%',
        height: 44,
        borderRadius: 22,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: Colors.themeBG
    },
    itemWrapper: {
        width: '100%',
        paddingVertical: 5,
        backgroundColor: Colors.white,
        borderRadius: 4,
        marginTop: 15,
        //阴影四连
        shadowOffset: {width: 0, height: 2},
        shadowColor: '#070F26',
        shadowOpacity: 0.15,
        shadowRadius: 4,
        elevation: 5
    },
    item: {
        height: 54,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    bottomBtnWrapper: {
        position: 'absolute',
        left: 0,
        bottom: BottomSafeMargin,
        width: '100%',
        paddingHorizontal: 16,
        paddingVertical: 10
    },
    deleteTouch: {
        width: '100%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.themBgRed,
        borderRadius: 4
    },
})

export default ControllerSetting

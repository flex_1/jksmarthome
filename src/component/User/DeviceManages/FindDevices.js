/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    DeviceEventEmitter,
    Keyboard,
    Image,
    SectionList,
    Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import {Colors,NetUrls,NotificationKeys} from '../../../common/Constants';
import { postJson } from '../../../util/ApiRequest';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';

const screenH = Dimensions.get('window').height;

class FindDevices extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'发现设备'}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text: '全选', onPress:()=>{
                    navigation.getParam('selectAllBtnClick')()
                }},
                {text: '添加', onPress:()=>{
                    navigation.getParam('addBtnClick')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const {getParam, setParams} = this.props.navigation
        
        this.state = {
            deviceListData: null,
            selectDevices: [],
            allDeviceIds: []
        }

        setParams({
            addBtnClick: this.requestAddDevice.bind(this),
            selectAllBtnClick: this.selectAllBtnClick.bind(this)
        })
    }

    componentDidMount() {
        this.requestFindDeviceList()
    }

    componentWillUnmount() {
        
    }

    //获取默认的账号密码
    async requestFindDeviceList() {
        try {
			let data = await postJson({
				url: NetUrls.discoverDevice,
				params: {	
				}
			});
			if (data.code == 0) {
				let dataList = data.result || []
                let sectionListData = []
                let allDeviceIds = []

                for (const gateway of dataList) {
                    let item = {}
                    item.data = gateway.list || []
                    item.name = gateway.name
                    item.dayIcon = gateway.dayIcon
                    item.nightIcon = gateway.nightIcon
                    item.id = gateway.id

                    sectionListData.push(item)
                    allDeviceIds.push(gateway.id)

                    if(gateway.list?.length > 0){
                        for (const device of gateway.list) {
                            allDeviceIds.push(device.id)
                        }
                    }
                }
                this.setState({
                    deviceListData: sectionListData,
                    allDeviceIds: allDeviceIds
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 添加设备
	async requestAddDevice() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.discoverDeviceBind,
				params: {	
                    idList: this.state.selectDevices
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
                this.props.navigation.pop()
                DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
				ToastManager.show('设备添加成功')
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    selectAllBtnClick(){
        let allDeviceIds = this.state.allDeviceIds
        let selectDevices = this.state.selectDevices

        if(!allDeviceIds){
            ToastManager.show('数据加载中，请稍后')
            return
        }
        if(allDeviceIds.length == 0){
            return
        }
        if(allDeviceIds.length == selectDevices.length){
            this.setState({
                selectDevices: []
            })
        }else{
            this.setState({
                selectDevices: JSON.parse(JSON.stringify(this.state.allDeviceIds))
            })
        }
    }

    renderSectionHeader(section){
        const Colors = this.props.themeInfo.colors;
        const idIndex = this.state.selectDevices.indexOf(section.id)
        const selectImg = idIndex >= 0 ? require('../../../images/select.png')
        : require('../../../images/connectWifi/unselected.png')
        const icon = this.props.themeInfo.isDark ? section.nightIcon : section.dayIcon
        
        return(
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={[styles.sectionHeaderWrapper,{backgroundColor: Colors.themeBg}]}
                onPress={()=>{
                    let sectionAllIds = [section.id]
                    for (const device of section.data) {
                        sectionAllIds.push(device.id)
                        if(this.state.selectDevices.includes(device.id)){
                            this.state.selectDevices.remove(device.id)
                        }
                    }
                    if(this.state.selectDevices.includes(section.id)){
                        this.state.selectDevices.remove(section.id)
                    }else{
                        this.state.selectDevices = this.state.selectDevices.concat(sectionAllIds)
                    }
                    this.setState({
                        selectDevices: this.state.selectDevices
                    })
                }}
            >
                <Image style={styles.gatewayIcon} source={{uri: icon}}/>
                <Text style={[styles.gatewayText,{color: Colors.themeText}]}>{section.name}</Text>
                <View style={{flex: 1}}/>
                <Image style={styles.selectIcon} source={selectImg}/>
                <View style={[styles.split,{backgroundColor: Colors.split}]} />
            </TouchableOpacity>
        )
    }

    renderRowItem(rowData, index, section){
        if(!rowData){
            return null
        }
        const Colors = this.props.themeInfo.colors;
        const idIndex = this.state.selectDevices.indexOf(rowData.id)
        const selectImg = idIndex >= 0 ? require('../../../images/select.png')
        : require('../../../images/connectWifi/unselected.png')
        const icon = this.props.themeInfo.isDark ? rowData.nightIcon : rowData.dayIcon

        return(
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={[styles.item,{backgroundColor: Colors.themeBaseBg}]} 
                onPress={()=>{
                    if(!this.state.selectDevices.includes(section.id)){
                        ToastManager.show('请先选中网关')
                        return
                    }
                    if(this.state.selectDevices.includes(rowData.id)){
                        this.state.selectDevices.remove(rowData.id)
                    }else{
                        this.state.selectDevices.push(rowData.id)
                    }
                    this.setState({
                        selectDevices: this.state.selectDevices
                    })

                }}
            >
                <Image source={{uri: icon}} style={[styles.icon]}/>
                <Text style={[styles.itemTitle,{color: Colors.themeText}]}>{rowData.name}</Text>
                <Image style={styles.selectIcon} source={selectImg}/>
                <View style={[styles.split,{backgroundColor: Colors.split}]} />
            </TouchableOpacity>
        )
    }

    renderMainListView(){
        const Colors = this.props.themeInfo.colors;
        
        if(!this.state.deviceListData){
            return null
        }
        
		if(this.state.deviceListData.length <= 0){
			return (
                <View style={styles.noContent}>
                    <Text style={{color: Colors.themeTextLight, fontSize: 15}}>暂无设备</Text>
                </View>
            )
		}

        return(
			<SectionList
				style={{flex: 1}}
				contentContainerStyle={{ paddingBottom: 50}}
				stickySectionHeadersEnabled={true}
  				renderItem={({ item, index, section }) => this.renderRowItem(item, index, section)}
  				renderSectionHeader={({ section }) => this.renderSectionHeader(section)}
				sections={this.state.deviceListData}
				scrollIndicatorInsets={{right: 1}}
				initialNumToRender={5}
				keyExtractor={(item, index) => 'devices_'+index}
			/>
		)
    }
    
    render() {
        const Colors = this.props.themeInfo.colors
        return (
            <View style={{backgroundColor: Colors.themeBaseBg, flex: 1}}>
                {this.renderMainListView()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    noContent:{
        justifyContent:'center',
        alignItems:'center',
        marginTop: screenH * 0.35
    },
    sectionHeaderWrapper:{
        height: 60, 
        width:'100%', 
        paddingLeft:16,
        paddingRight: 30,
        alignItems:'center', 
        flexDirection:'row'
    },
    selectIcon:{
        width:18,
        height:18,
        resizeMode: 'contain'
    },
    split:{
        position: 'absolute',
        right: 0, 
        height: 1, 
        left: 0,
        bottom: 0
    },
    gatewayText:{
        fontSize: 16, 
        fontWeight:'bold',
        marginLeft: 8
    },
    gatewayIcon:{
        width: 40, 
        height: 40, 
        resizeMode:'contain'
    },
    icon:{
        width:30,
        height: 30,
        resizeMode:'contain',
        marginLeft: 16,
        marginRight: 15
    },
    itemTitle:{
		fontSize: 15,
		flex: 1 
    },
    item: {
		height: 55,
		width: '100%',
		flexDirection: 'row',
		alignItems: 'center',
        paddingLeft: 32,
        paddingRight: 30
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(FindDevices)

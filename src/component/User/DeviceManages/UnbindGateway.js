/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    DeviceEventEmitter,
    FlatList,
    Image
} from 'react-native';
import { connect } from 'react-redux';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import {Colors,NetUrls,NotificationKeys} from '../../../common/Constants';
import { postJson } from '../../../util/ApiRequest';
import { BottomSafeMargin } from '../../../util/ScreenUtil';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';

class UnbindGateway extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation
        
        this.type = getParam('type')   //  1.单个设备停用  2.多个设备停启用  3.网关解绑
        this.isFromManager = getParam('isFromManager')
        this.deviceData = getParam('deviceData')
        this.ids = getParam('ids')
        this.callBack = getParam('callBack')
        this.state = {
            isConfirm: false,
            deviceData: [],
            foldList: []
        }
    }

    componentDidMount() {
        
    }

    componentWillUnmount() {
        
    }

    // 修改网关
	async confirmUnBindDevice() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.confirmUnBindDevice,
				params: {	
                    ids: this.ids
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
				this.handleUnbindSuccess()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 解绑成功后的处理
    handleUnbindSuccess(){
        const {pop} = this.props.navigation
        
        DeviceEventEmitter.emit(NotificationKeys.kSmartListChangeNotification)
        //  1.单个设备停用  2.多个设备停启用  3.网关解绑
        if(this.type == 1){
            DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
			if(this.isFromManager){
				pop(2)
			}else{
				pop(3)
			}
            ToastManager.show('设备已停用')
        }else if(this.type == 2){
            this.callBack && this.callBack()
            pop(2)
            ToastManager.show('所选设备已停用')
        }else if(this.type == 3){
            DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
            DeviceEventEmitter.emit(NotificationKeys.kRoomInfoNotification)
            pop(3)
            ToastManager.show('网关解绑成功')
        }
    }

    
    renderTips(){
        const Colors = this.props.themeInfo.colors
        const count = this.deviceData?.length || 0

        let warnningText = ''
        if(this.type == 1){
            warnningText = '警告: 该设备关联有场景或智能,停用后该设备将会从智能或场景中移除。'
        }else if(this.type == 2){
            warnningText = '警告: 您所选的设备中有' + count + '个设备关联有场景或智能,停用后这些设备将会从智能或场景中移除。'
        }else if(this.type == 3){
            warnningText = '警告: 该网关下有' + count + '个设备关联有场景或智能,解绑后这些设备将会从智能或场景中移除。'
        }

        return(
            <Text style={[styles.topTips,{color: Colors.red}]}>{warnningText}</Text>
        )
    }

    _renderSmartList(smartList, deviceId){
        if(!smartList || smartList.length <= 0){
            return null
        }
        if(this.state.foldList.includes(deviceId)){
            return null
        }
        const Colors = this.props.themeInfo.colors

        const list = smartList.map((value, index)=>{
            
            return (
                <View key={'scene_index_'+index} style={[styles.item,{backgroundColor: Colors.themeBaseBg}]}>
                    <Text style={{fontSize: 14, color: Colors.themeText}}>{value.name}</Text>
                </View>
            )
        })
        return(
            <View style={{marginTop: 15}}>
                <Text style={{fontSize: 16, fontWeight:'bold',color: Colors.themeButton}}>所在智能:</Text>
                <View style={styles.itemWrapper}>
                    {list}
                </View>
            </View>
        )
    }

    _renderSceneList(sceneList, deviceId){
        if(!sceneList || sceneList.length <= 0){
            return null
        }
        if(this.state.foldList.includes(deviceId)){
            return null
        }
        const Colors = this.props.themeInfo.colors

        const list = sceneList.map((value, index)=>{
            let floorView = null
            if(value.floorText && value.roomName){
                floorView = <Text style={{fontSize: 13,marginTop: 5 ,color: Colors.themeTextLight}}>{value.floorText + ' ' + value.roomName}</Text>
            }
            return (
                <View key={'smart_index_'+index} style={[styles.item,{backgroundColor: Colors.themeBaseBg}]}>
                    <Text style={{fontSize: 14, color: Colors.themeText}}>{value.name}</Text>
                    {floorView}
                </View>
            )
        })
        return(
            <View style={{marginTop: 15}}>
                <Text style={{fontSize: 16, fontWeight:'bold',color: Colors.themeButton}}>所在场景:</Text>
                <View style={styles.itemWrapper}>
                    {list}
                </View>
            </View>
        )
    }

    renderRowItem(rowData, index){
        const Colors = this.props.themeInfo.colors
        let floorView = null
        if(rowData.floorText && rowData.roomName){
            floorView = <Text style={{fontSize: 14,marginTop: 5 ,color: Colors.themeTextLight}}>{rowData.floorText + ' ' + rowData.roomName}</Text>
        }
        let arrowImg = require('../../../images/controller/arrow_up.png')
        if(this.state.foldList.includes(rowData.deviceId)){
            arrowImg = require('../../../images/controller/arrow_down.png')
        }
        return(
            <View style={[styles.rowWrapper,{backgroundColor: Colors.themeBg}]}>
                <TouchableOpacity activeOpacity={0.7} style={{flexDirection: 'row',alignItems: 'center'}} onPress={()=>{
                    if(this.state.foldList.includes(rowData.deviceId)){
                        this.state.foldList.remove(rowData.deviceId)
                    }else{
                        this.state.foldList.push(rowData.deviceId)
                    }
                    this.setState({
                        foldList: this.state.foldList
                    })
                }}>
                    <View style={{flex: 1}}>
                        <Text style={{fontSize: 18, fontWeight:'bold',color: Colors.themeText}}>{rowData.deviceName}</Text>
                        {floorView}
                    </View>
                    <Image style={styles.arrowImg} source={arrowImg}/>
                </TouchableOpacity>
                {this._renderSceneList(rowData.scene, rowData.deviceId)}
                {this._renderSmartList(rowData.intelligent, rowData.deviceId)}
            </View>
        )
    }

    renderMainView(){
        if(!this.deviceData){
            return null
        }
        return(
            <FlatList 
                style={{marginTop:10, flex: 1}}
			    data={this.deviceData}
			    removeClippedSubviews={false}
			    keyExtractor={(item, index)=> 'device_index_'+index}
			    renderItem={({ item, index }) => this.renderRowItem(item, index)}
			    initialNumToRender={5}
		    />
        )
    }
    
    renderBottomView(){
        const Colors = this.props.themeInfo.colors
        const selectImg = this.state.isConfirm ? require('../../../images/select.png') :
        require('../../../images/connectWifi/unselected.png')
        let btnBg = this.state.isConfirm ? Colors.themeButton : Colors.unselectBg
        const stopText = this.type == 3 ? '解绑' : '停用'

        return(
            <View style={styles.bottomWrapper}>
                <TouchableOpacity activeOpacity={0.7} style={styles.tipsTouchWrapper} onPress={()=>{
                    this.setState({
                        isConfirm: !this.state.isConfirm
                    })
                }}>
                    <Image style={styles.selectedImg} source={selectImg}/>
                    <Text style={[styles.bottomTipsText,{color: Colors.themeText}]}>已确认上述操作</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.7} style={[styles.bottomBtn,{backgroundColor: btnBg}]} onPress={()=>{
                    if(!this.state.isConfirm){
                        return
                    }
                    this.confirmUnBindDevice()
                }}>
                    <Text style={[styles.bottomBtnText,{color: Colors.white}]}>{stopText}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors
        return (
            <View style={{backgroundColor: Colors.themeBaseBg, flex: 1}}>
                {this.renderTips()}
                {this.renderMainView()}
                {this.renderBottomView()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    selectedImg:{
        width: 16,
        height:16,
        resizeMode: 'contain'
    },
    bottomTipsText:{
        marginLeft: 7, 
        fontSize: 14
    },
    bottomBtn:{
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        borderRadius: 22,
        marginTop: 10,
        width: '80%'
    },
    bottomBtnText:{
        fontSize: 18
    },
    tipsTouchWrapper:{
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10
    },
    bottomWrapper:{
        paddingBottom: BottomSafeMargin + 20,
        alignItems:'center'
    },
    topTips:{
        marginHorizontal: 16, 
        fontSize: 14, 
        marginTop: 10
    },
    item:{
        width: '48%',
        height: 50, 
        marginBottom: 12, 
        paddingHorizontal: 5,
        borderRadius: 5, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    itemWrapper:{
        flexDirection: 'row',
        marginTop: 10, 
        flexWrap:'wrap', 
        justifyContent: 'space-between'
    },
    rowWrapper:{
        marginHorizontal: 16, 
        marginTop: 10, 
        padding: 16,
        borderRadius: 5
    },
    arrowImg:{
        width: 13, 
        height: 8,
        marginRight: 5,
        resizeMode: 'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(UnbindGateway)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	DeviceEventEmitter,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
    Alert,
    ScrollView,
    Dimensions,
	Modal
} from 'react-native';
import { connect } from 'react-redux';
import { NotificationKeys, NetUrls, Colors } from '../../../common/Constants';
import { postJson } from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';
import { BottomSafeMargin } from '../../../util/ScreenUtil';

const screenH = Dimensions.get('window').height
const screenW = Dimensions.get('window').width

class FirmwareUpgrade extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {icon:require('../../../images/panel/robot/more.png'), onPress:()=>{
                    navigation.navigate('FirmwareUpgradeLog',{
                        getwayData: navigation.getParam('getwayData')
                    })
                }}
            ]}/>
        ),
        headerTitle: <HeaderTitle title={'固件升级'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.getwayData = getParam('getwayData') || {}
		
		this.state = {
			hasNewVersion: false,
            img: '',
            name: '',
            currentVersion: '',
            newVersion: '',
            isUpgrading: false
		}
	}

	componentDidMount() {
		this.requestGetwayInfo()
		
	}

	componentWillUnmount() {
		
	}

	// 获取网关信息
	async requestGetwayInfo() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.upgradePanelInfo,
				params: {	
					id: this.getwayData.id
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
				this.setState({
					hasNewVersion: data.result?.hasNewVersion,
                    img: data.result?.img,
                    name: data.result?.name,
                    currentVersion: data.result?.currentVersion,
                    newVersion: data.result?.newVersion
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    async requestUpgrade() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.upgradePanel,
				params: {	
					id: this.getwayData.id,
                    currentVersion: this.state.currentVersion,
                    newVersion: this.state.newVersion
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
				this.setState({
                    isUpgrading: true
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    renderMainImg(){
        const Colors = this.props.themeInfo.colors;

        return(
            <View style={styles.imgWrapper}>
                <Text style={{fontSize:14, fontWeight: '400', color: Colors.themeText}}>{this.state.name}</Text>
                {this.state.img ? <Image style={styles.mainImg} source={{uri: this.state.img}}/> : null}
            </View>
        )
    }
    
    renderUpgradeInfo(){
        if(!this.state.hasNewVersion){
            return(
                <View style={{marginTop: 20, justifyContent: 'center',alignItems: 'center'}}>
                    <Text style={{fontSize: 14, fontWeight: '400',color: Colors.newBtnBlueBg,marginTop: 10}}>当前已是最新版本</Text>
                    <Text style={{fontSize: 14, fontWeight: '400',color: Colors.newBtnBlueBg,marginTop: 10}}>当前版本：{this.state.currentVersion}</Text>
                </View>
            )
        }else if(this.state.isUpgrading){
            return(
                <View style={{marginTop: 20, justifyContent: 'center',alignItems: 'center'}}>
                    <Text style={{fontSize: 20, fontWeight: '400',color: Colors.newBtnBlueBg,marginTop: 10}}>升级中</Text>
                    <Text style={{fontSize: 16, fontWeight: '400',color: Colors.newBtnBlueBg,marginTop: 10}}>请勿断开面板电源或网络</Text>
                </View>
            )
        }else{
            return(
                <View style={{marginTop: 20, justifyContent: 'center',alignItems: 'center'}}>
                    <Text style={{fontSize: 16, fontWeight: '400',color: Colors.newBtnBlueBg,marginTop: 10}}>发现新版本: {this.state.newVersion}</Text>
                    <TouchableOpacity style={styles.btnTouch} onPress={()=>{
                        this.requestUpgrade()
                    }}>
                        <Text style={{fontSize: 16, fontWeight: '500',color: Colors.white}}>立即升级</Text>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    renderCurrentVersion(){
        if(!this.state.hasNewVersion){
            return null
        }
        const Colors = this.props.themeInfo.colors;
        return(
            <View style={{position: 'absolute',width:'100%',left:0,bottom: BottomSafeMargin + screenH * 0.1,justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:14,fontWeight:'400',color: Colors.themeTextLight}}>当前版本: {this.state.currentVersion}</Text>
            </View>
        )
    }

	render() {
		const Colors = this.props.themeInfo.colors;

		return (
			<View style={{backgroundColor: Colors.themeBg, flex: 1}}>
				{this.renderMainImg()}
                {this.renderUpgradeInfo()}
                {this.renderCurrentVersion()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	imgWrapper:{
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: screenH * 0.12
    },
    mainImg:{
        width: screenW * 0.4,
        height: screenW * 0.4,
        resizeMode:'contain',
        marginTop: 20
    },
    btnTouch:{
        marginTop: 20,
        height:44,
        width: 200,
        borderRadius:22,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: Colors.newBtnBlueBg
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(FirmwareUpgrade)

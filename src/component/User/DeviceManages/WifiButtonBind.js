/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Animated,
	Dimensions,
    Modal,
    Keyboard,
    Alert
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import DateUtil from '../../../util/DateUtil';
import {ColorsDark, ColorsLight} from '../../../common/Themes';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight'
import {FooterLoading,FooterEnd } from "../../../common/CustomComponent/ListLoading";

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const keybordHeight = 180

class WifiButtonBind extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text: '保存', onPress:()=>{
                    navigation.getParam('saveBtnClick')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
		super(props);
		const { getParam,setParams } = props.navigation;
        
        this.buttonId = getParam('buttonId')
        this.bindingDeviceId = getParam('bindingDeviceId')
        this.bindingSceneId = getParam('bindingSceneId')
        this.bindingDeviceExcuteKey = getParam('bindingDeviceExcuteKey')
        this.bindingDeviceExcuteKeyText = getParam('bindingDeviceExcuteKeyText')
        this.callBack = getParam('callBack')

        this.state = {
            currentSelect: 1,  //1：设备  2：场景,
            loading: false,
            sceneList: null,
            deviceList: null,
            currentPage: 0,
            totalPage: 0,
            modalVisible: false,
            roomselectedData: [],
            pickerData: [],
            allRoomIds: [],
            currentRoomId: null,
            showSearch: false,
            searchText: '',
            translateValue:new Animated.ValueXY({x:0,y:0}),
            panelId: null,

            selectItem: this.getSelectItem(),
            modalType: 1,  // 1.房屋筛选  2.设备开关状态
            statusPickerData: [],
            selectStatusData: [],
            selectDeviceId: null,
            selectDeviceIndex: null
        }

        setParams({
            saveBtnClick: this.saveBtnClick.bind(this)
        })
    }

    getSelectItem(){
        if(this.bindingDeviceId){
            return(
                {type: 1, id: this.bindingDeviceId, statusId: this.bindingDeviceExcuteKey, statusText: this.bindingDeviceExcuteKeyText}
            )
        }else if(this.bindingSceneId){
            return(
                {type: 2, id: this.bindingSceneId}
            )
        }else{
            return {}
        }
    }
    
	componentDidMount() {
        this.requestBindList()
        this.requestRoomList()
	}

	componentWillUnmount(){
        
    }

    saveBtnClick(){
        const selectItem = this.state.selectItem

        if(!selectItem.id){
            ToastManager.show('请选择一个场景或设备')
            return
        }

        let params = {panelButtonId: this.buttonId}
        if(selectItem.type == 1){
            params.deviceId = selectItem.id
            params.key = selectItem.statusId
            params.text = selectItem.statusText
        }else{
            params.sceneId = selectItem.id
        }

        if(this.bindingDeviceId || this.bindingSceneId){
            Alert.alert(
                '提示',
                '此按键已绑定过，是否覆盖？',
                [
                    { text: '取消', onPress: () => { }, style: 'cancel'},
                    { text: '是', onPress: () => { this.requestBindButton(params) }},
                ]
            ) 
        }else{
            this.requestBindButton(params)
        }
    }

    async requestBindList(params) {
        params = params || {}
        params.roomId = this.state.currentRoomId
        params.name = this.state.searchText
        params.panelButtonId = this.buttonId
        this.setState({loading:true})
        try {
			let data = await postJson({
				url: NetUrls.getPanelBindingList,
				params: {
                    ...params,
                    type: this.state.currentSelect
                }
			});
			this.setState({
				loading: false
			})
			if (data.code == 0) {
                let list = []
                if(params.page && params.page > 1){
                    if(this.state.currentSelect == 1){
                        list = this.state.deviceList.concat(data.result.list)
                    }else{
                        list = this.state.sceneList.concat(data.result.list)
                    }
                }else{
                    list = data.result?.list || []
                }
                if(this.state.currentSelect == 1){
                    this.setState({
                        deviceList: list
                    })
                }else{
                    this.setState({
                        sceneList: list
                    })
                }
                this.setState({
                    currentPage: data.result?.curPage,
                    totalPage: data.result?.totalPageNum
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({ 
				loading: false
			})
			ToastManager.show('网络错误');
		}
    }

    // 获取房间 列表
	async requestRoomList() {
		try {
			let data = await postJson({
				url: NetUrls.roomList,
				params: {
                    type: 2
				}
			})
			if (data.code == 0) {
				const floorData = data.result || []
                let pickerData = [{'全部楼层':['全部房间']}]
                let allRoomIds = [[0]]
                for (const floor of floorData) {
                    let f_name = {}
                    let r_names = []
                    let r_ids = []
                    for (const room of floor.listRoom) {
                        r_names.push(room.name)
                        r_ids.push(room.id)
                    }
                    f_name[floor.floorText] = r_names
                    allRoomIds.push(r_ids)
                    pickerData.push(f_name)
                }
                this.setState({
                    pickerData: pickerData,
                    allRoomIds: allRoomIds
                })
			} else {
				ToastManager.show(data.msg);
			}
		} catch (e) {
			ToastManager.show('网络错误');
		}
	}

    // 按键绑定
    async requestBindButton(params) {
        SpinnerManager.show()
        try {
			let data = await postJson({
				url: NetUrls.bindingPanel,
				params: params
			})
            SpinnerManager.close()
			if (data.code == 0) {
                ToastManager.show('绑定成功');
                this.callBack && this.callBack()
                this.props.navigation.pop()
            } else {
				ToastManager.show(data.msg);
			}
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误');
        }
    }

    search(){
        Keyboard.dismiss()
        this.setState({
            sceneList: null,
            deviceList: null,
            currentPage: 0,
            totalPage: 0
        },()=>{
            this.requestBindList()
        })
    }

    onLoadNextPage(){
        if(this.state.loading){
            return
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return
        }
        this.requestBindList({page: this.state.currentPage+1})
    }

    renderNoContent(){
        if(this.state.loading){
            return (
                <View style={styles.noContentWrapper}>
                    <Text style={styles.noContentText}>正在加载...</Text>
                </View>
            )
        }else{
            let nocontentText = this.state.currentSelect == 1 ? '暂无设备' : '暂无场景'
            return (
                <View style={styles.noContentWrapper}>
                    <Text style={styles.noContentText}>{nocontentText}</Text>
                </View>
            )
        } 
    }

    renderBindIcon(isBinding){
        if(!isBinding){
            return null
        }
        return(
            <View style={styles.bindIcon}>
                <Text style={styles.bindIconText}>已绑定</Text>
            </View>
        )
    }

    _renderDeviceItem(rowData, index, roomView){
        const Colors = this.props.themeInfo.colors
        const icon = this.props.themeInfo.isDark ? rowData.nightIcon : rowData.dayIcon
        let selectImg = require('../../../images/connectWifi/unselected.png')
        if(this.state.selectItem.type == 1 && this.state.selectItem.id == rowData.id){
            selectImg = require('../../../images/connectWifi/selected.png')
        }
        
        let statusText = rowData.statusText
        let statusId = rowData.statusId

        let executeText = rowData.executeText
        let executeKey = rowData.executeKey
        let executeTextArr = null
        let executeKeyArr = null
        let arrowIcon = null
        if(rowData.executeKey.includes(',')){
            // 多属性
            executeTextArr = rowData.executeText.split(',')
            executeKeyArr = rowData.executeKey.split(',')
            executeText = executeTextArr[0]
            executeKey = executeKeyArr[0]
            arrowIcon = <Image style={styles.arrowImg} source={require('../../../images/controller/arrow_down.png')}/>
        }

        if(statusId == null){
            if(this.bindingDeviceId == rowData.id){
                statusText = this.bindingDeviceExcuteKeyText
                statusId = this.bindingDeviceExcuteKey
            }else{
                statusText = executeText
                statusId = executeKey
            }
        }

        return(
            <View style={[styles.deviceItem,{backgroundColor: Colors.themeBg}]}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.touchWrapper,{flex: 1}]} 
                    onPress={()=>{
                        this.setState({
                            selectItem: {type: 1, id: rowData.id, statusId: statusId, statusText: statusText}
                        })
                    }}
                >
                    <Image style={styles.selectedImg} source={selectImg}/>
                    <Image style={[styles.icon,{marginLeft: 10}]} source={{uri: icon}}/>
                    <View style={styles.labelWrapper}>
                        <View style={styles.nameWrapper}>
                            <Text style={[styles.name,{color: Colors.themeText}]}>{rowData.name}</Text>
                            {this.renderBindIcon(rowData.isBinding)}
                        </View>
                        {roomView}
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.touchWrapper,{paddingLeft: 20}]} 
                    onPress={()=>{
                        if(executeKeyArr == null){
                            return
                        }
                        this.setState({
                            modalType: 2,
                            modalVisible: true,
                            selectDeviceId: rowData.id,
                            selectDeviceIndex: index,
                            selectStatusData: [statusText],
                            statusPickerData: executeTextArr,
                            selectKeys: executeKeyArr
                        })
                    }}
                >
                    <Text style={[styles.statusText,{color: rowData.type == 1 ? Colors.themeText : Colors.themeTextLight }]}>{statusText}</Text>
                    {arrowIcon}
                </TouchableOpacity>
            </View>
        )
    }

    _renderSceneItem(rowData, index, roomView){
        const Colors = this.props.themeInfo.colors
        const icon = this.props.themeInfo.isDark ? rowData.nightIcon : rowData.dayIcon
        let selectImg = require('../../../images/connectWifi/unselected.png')
        if(this.state.selectItem.type == 2 && this.state.selectItem.id == rowData.id){
            selectImg = require('../../../images/connectWifi/selected.png')
        }

        return(
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={[styles.item,{backgroundColor: Colors.themeBg}]} 
                onPress={()=>{
                    this.setState({
                        selectItem: {type: 2, id: rowData.id}
                    })
                }}
            >
                <Image style={styles.selectedImg} source={selectImg}/>
                <Image style={[styles.icon,{marginLeft: 10}]} source={{uri: icon}}/>
                <View style={[styles.labelWrapper,{flex: 1}]}>
                    <View style={styles.nameWrapper}>
                        <Text style={[styles.name,{color: Colors.themeText}]}>{rowData.name}</Text>
                        {this.renderBindIcon(rowData.isBinding)}
                    </View>
                    {roomView}
                </View>
                <Text style={[styles.statusText,{color: rowData.type == 1 ? Colors.themeText : Colors.themeTextLight }]}>执行</Text>
            </TouchableOpacity>
        )
    }

    renderRowItem(rowData, index){
        const Colors = this.props.themeInfo.colors
        let roomView = null
        if(rowData.floorText && rowData.roomName){
            roomView = <Text style={[styles.roomName,{color: Colors.themeTextLight}]}>{rowData.floorText + ' ' + rowData.roomName}</Text>
        }

        if(this.state.currentSelect == 1){
            return this._renderDeviceItem(rowData, index, roomView)
        }else{
            return this._renderSceneItem(rowData, index, roomView)
        }
    }

    //footer
    renderFooter() {
        const listData = this.state.currentSelect == 1 ? this.state.deviceList : this.state.sceneList

        if(!listData || listData.length <= 0){
            return null
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return <FooterEnd/>
        }
        return <FooterLoading/>
    }

    renderList(){
        const listData = this.state.currentSelect == 1 ? this.state.deviceList : this.state.sceneList

        return(
            <FlatList
                ref={e => this.flatlist = e}
                contentContainerStyle={styles.contentStyle}
                scrollIndicatorInsets={{right: 1}}
                removeClippedSubviews={false}
                data={listData}
                keyExtractor={(item, index)=> 'list_item_'+index}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
                initialNumToRender={10}
                ListEmptyComponent = {this.renderNoContent()}
                onEndReached={() => this.onLoadNextPage()}
                onEndReachedThreshold={0.1}
                ListFooterComponent={() => this.renderFooter()}
                keyboardDismissMode={'on-drag'}
                keyboardShouldPersistTaps={'handled'}
            />
        )
    }

    renderSearchInput(){
        if(!this.state.showSearch){
            return null
        }
        const Colors = this.props.themeInfo.colors

        return(
            <View style={[styles.searchWrapper,{backgroundColor: Colors.themeBg,borderBottomColor: Colors.themeBaseBg}]}>
                <View style={styles.inputWrapper}>
                    <TextInput
                        ref={e => this.keyBord = e}
                        style={styles.input}
                        value={this.state.searchText}
                        placeholder={'请输入关键字'}
                        placeholderTextColor={Colors.themeBGInactive}
                        returnKeyType={'search'}
                        onChangeText={(text) => {
                            this.setState({
                                searchText : text
                            })
                        }}
                        onSubmitEditing={()=>{
                            Keyboard.dismiss()
                            this.search()
                        }}
                    />
                    <TouchableOpacity activeOpacity={0.7} style={styles.clearBtn} onPress={()=>{
                        this.setState({
                            searchText: ''
                        })
                    }}>
                        <Image style={styles.clearImg} source={require('../../../images/panel/panelBind/clear.png')}/>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity activeOpacity={0.7} style={styles.searchBtn} onPress={()=>{
                    this.search()
                }}>
                    <Text style={[styles.searchText,{color: Colors.themeText}]}>搜索</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderBottomControlView(){
        const Colors = this.props.themeInfo.colors
        const underLine = <View style={styles.underLine}/>
        const sel_style = {fontSize: 14,fontWeight:'500',color: Colors.themeText}
        const unsel_style = {fontSize: 14, fontWeight:'400',color: Colors.themeTextLight}
        const filter_style = this.state.currentRoomId ? {tintColor: Colors.themeButton} : {}
        
        return(
            <View style={{flex: 1}}>
                <View style={[styles.headWraper,{backgroundColor: Colors.themeBg,borderBottomColor: Colors.themeBaseBg}]}>
                    <TouchableOpacity style={styles.headerBtn} onPress={()=>{
                        this.setState({
                            currentSelect: 1,
                            currentPage: 0,
                            totalPage: 0,
                            sceneList: null
                        },()=>{
                            this.requestBindList()
                        })
                    }}>
                        <Text style={this.state.currentSelect == 1 ? sel_style : unsel_style}>设备</Text>
                        {this.state.currentSelect == 1 ? underLine : null}
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.headerBtn,{marginLeft: 30}]} onPress={()=>{
                        this.setState({
                            currentSelect: 2,
                            currentPage: 0,
                            totalPage: 0,
                            deviceList: null 
                        },()=>{
                            this.requestBindList()
                        })
                    }}>
                        <Text style={this.state.currentSelect == 2 ? sel_style : unsel_style}>场景</Text>
                        {this.state.currentSelect == 2 ? underLine : null}
                    </TouchableOpacity>
                    <View style={{flex: 1}}/>
                    <TouchableOpacity style={styles.headerBtn} onPress={()=>{
                        this.setState({
                            modalVisible: true,
                            modalType: 1
                        })
                    }}>
                        <Image style={[styles.filterIcon,filter_style]} source={require('../../../images/panel/panelBind/filter.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.headerBtn]} onPress={()=>{
                        this.setState({
                            searchText: '',
                            showSearch: !this.state.showSearch
                        },()=>{
                            if(!this.state.showSearch){
                                this.search()
                            }
                        })
                    }}>
                        <Image style={styles.filterIcon} source={require('../../../images/panel/panelBind/search.png')}/>
                    </TouchableOpacity>
                </View>
                {this.renderSearchInput()}
                <View style={{flex: 1}}>
                    {this.renderList()}
                </View>
            </View>
        )
    }

    // 获取选择框的 Modal
	getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    if(this.state.modalType == 1){
                        this.showDoublePicker()
                    }else{
                        this.showDoubleStstusPicker()
                    }
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
	}

    handlePickerSelectData(data, index){
        const i_0 = index[0]
        const i_1 = index[1]
        const roomId = this.state.allRoomIds[i_0][i_1]
        const roomselectedData = [data[0], data[1]]
        this.setState({
            roomselectedData: roomselectedData,
            sceneList: null,
            deviceList: null,
            currentPage: 0,
            totalPage: 0,
            currentRoomId: roomId
        },()=>{
            this.requestBindList()
        })
    }

    handleStatus(data, index){
        let rowData = this.state.deviceList[this.state.selectDeviceIndex]
        rowData.statusText = data[0]
        rowData.statusId = this.state.selectKeys[index[0]]
        this.state.deviceList[this.state.selectDeviceIndex] = rowData

        let selectItem = this.state.selectItem
        if(selectItem.type == 1 && selectItem.id == this.state.selectDeviceId){
            this.state.selectItem.statusId = this.state.selectKeys[index[0]]
            this.state.selectItem.statusText = data[0]
        }

        this.setState({
            deviceList: this.state.deviceList,
            selectItem: this.state.selectItem
        })
    }
	
	// 弹出 Modal
    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerData,
            pickerTitleText: '房间筛选',
            selectedValue: this.state.roomselectedData,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false
                })
                this.handlePickerSelectData(data,index);
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                }, () => {
                    window.CustomPicker.hide()
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

    // 设备状态
    showDoubleStstusPicker(){
        window.CustomPicker.init({
            pickerData: this.state.statusPickerData,
            pickerTitleText: '设备状态',
            selectedValue: this.state.selectStatusData,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false
                })
                this.handleStatus(data,index);
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                }, () => {
                    window.CustomPicker.hide()
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

	render() {
        const Colors = this.props.themeInfo.colors;

		return (
			<View style={[styles.main,{backgroundColor: Colors.themeBaseBg}]}>
				{this.renderBottomControlView()}
                {this.getModal()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1
    },
    headWraper:{
        height: 44, 
        width:'100%',
        paddingLeft: 16,
        flexDirection: 'row',
        borderBottomWidth: 1
    },
    headerBtn:{
        paddingHorizontal: 16,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    underLine:{
        position: 'absolute',
        height: 3,
        borderRadius: 1.5,
        width: 40,
        backgroundColor: Colors.tabActiveColor,
        bottom: 0,
        alignSelf: 'center'
    },
    contentStyle:{
        paddingBottom: BottomSafeMargin + 10
    },
    deviceItem:{
        height: 54, 
        marginHorizontal: 16,
        borderRadius: 5,
        flexDirection: "row",
        alignItems: 'center',
        marginTop: 10,
        backgroundColor: Colors.white
    },
    item:{
        height: 60, 
        marginHorizontal: 16,
        paddingHorizontal: 16,
        borderRadius: 5,
        flexDirection: "row",
        alignItems: 'center',
        marginTop: 10,
        backgroundColor: Colors.white
    },
    icon:{
        width: 24,
        height: 24,
        resizeMode: 'contain'
    },
    name:{
        fontSize: 14,
        fontWeight: '400'
    },
    selectedImg:{
        width: 16,
        height:16,
        resizeMode: 'contain'
    },
    filterIcon:{
        width: 16,
        height: 17,
        resizeMode: 'contain'
    },
    roomName:{
        fontSize: 13,
        marginTop: 5
    },
    labelWrapper:{
        marginLeft: 10,
    },
    nameWrapper :{
        flexDirection:'row',
        alignItems: 'center'
    },
    topWrapper:{
        height: screenH * 0.4, 
        justifyContent:'center', 
        alignItems: 'center'
    },
    topImg:{
        width:'100%', 
        height: screenH * 0.4, 
        resizeMode:'contain'
    },
    noContentWrapper:{ 
        marginTop: screenH*0.1, 
        justifyContent:'center',
        alignItems: 'center'
    },
    noContentText:{
        fontSize: 14, 
        color: Colors.themeTextLightGray
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    searchWrapper:{
        height: 60, 
        width:'100%',
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 16
    },
    inputWrapper:{
        backgroundColor:Colors.themBGLightGray,
        height: 36,
        borderRadius: 18,
		flex:1,
		alignItems:'center',
		flexDirection:'row',
        paddingLeft: 10,
        // paddingVertical: Platform.select({
        //     android: 0,
        //     ios: 10
        // })
    },
    input:{
        flex:1,
        paddingLeft:10,
        fontSize: 14,
        height: '100%'
    },
    searchBtn:{
        height:'100%',
        paddingHorizontal: 10, 
        marginLeft: 5,
        justifyContent: 'center',
        alignItems:'center'
    },
    searchText:{
        fontSize: 16, 
        fontWeight: '400', 
        color: Colors.themeTextBlack
    },
    clearBtn:{
        height:'100%',
        paddingHorizontal: 16,
        justifyContent:'center',
        alignItems:'center'
    },
    clearImg:{
        width:16,
        height:16,
        resizeMode: 'contain'
    },
    arrowImg:{
        width: 13, 
        height: 8,
        marginRight: 5,
        resizeMode: 'contain',
        marginLeft: 8,
        tintColor: Colors.themeTextLightGray
    },
    touchWrapper:{
        paddingHorizontal: 16, 
        height: '100%',
        flexDirection: 'row',
        alignItems:'center'
    },
    statusText:{
        fontSize: 14,
        color: Colors.themeTextLightGray
    },
    bindIcon:{
        backgroundColor: '#008CFF', 
        marginLeft: 8,
        justifyContent: 'center',
        alignItems:'center',
        paddingHorizontal:6,
        paddingVertical: 3,
        borderRadius: 4
    },
    bindIconText:{
        fontSize: 11, 
        color: Colors.white
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(WifiButtonBind)



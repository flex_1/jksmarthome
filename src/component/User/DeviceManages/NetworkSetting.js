/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    DeviceEventEmitter,
    Keyboard,
    Image
} from 'react-native';
import { connect } from 'react-redux';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import {Colors,NetUrls} from '../../../common/Constants';
import { postJson } from '../../../util/ApiRequest';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';

class NetworkSetting extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'配网'}/>,
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation
        this.callBack = getParam('callBack')
        this.parentId = getParam('parentId')
        
        this.state = {
            hiddenPass: true,
            name: '',
            pass: ''
        }
    }

    componentDidMount() {
        this.getDefaultPass()
    }

    componentWillUnmount() {
        
    }

    //获取默认的账号密码
    async getDefaultPass() {
        try {
			let data = await postJson({
				url: NetUrls.getWifi,
				params: {	
                    parentId: this.parentId
				}
			});
			if (data.code == 0) {
				this.setState({
                    name: data.result && data.result.ssid,
                    pass: data.result && data.result.pwd
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 修改网关
	async submitNetwork() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.downWIFI,
				params: {	
                    ssid: this.state.name,
                    pwd: this.state.pass,
                    parentId: this.parentId
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
				this.handleResult()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }
    
    handleResult(){
        const { pop } = this.props.navigation
        SpinnerManager.show('配网中...')
        setTimeout(() => {
            SpinnerManager.close()
            ToastManager.show('配网成功')
            pop()
        }, 5*1000);
    }

    submit(){
        Keyboard.dismiss()
        if(!this.state.name){
			ToastManager.show('名称不能为空')
			return
        }
        if(!this.state.pass){
			ToastManager.show('密码不能为空')
			return
        }
        this.submitNetwork()
    }

    renderNameInput() {
        const Colors = this.props.themeInfo.colors
        return (
            <View style={[styles.inputWrapper,{backgroundColor:Colors.themeBg}]}>
                <TextInput
                    style={[styles.input,{color:Colors.themeText}]}
                    defaultValue={this.state.name}
                    underlineColorAndroid='transparent'
                    clearButtonMode={'while-editing'}
                    returnKeyType={'done'}
                    placeholderTextColor={Colors.themeInactive}
                    placeholder={'请输入Wifi名称'}
                    onChangeText={(text)=>{
                        this.state.name = text
                    }}
                />
            </View>
        )
    }

    renderPassInput(){
        const Colors = this.props.themeInfo.colors
        const hideIcon = this.state.hiddenPass ? require('../../../images/user_manage/pass_hidden.png')
        : require('../../../images/user_manage/pass_show.png')
        
        return (
            <View style={[styles.paasInputWrapper,{backgroundColor:Colors.themeBg}]}>
                <TextInput
                    style={[styles.passInput,{color:Colors.themeText}]}
                    defaultValue={this.state.pass}
                    underlineColorAndroid='transparent'
                    placeholderTextColor={Colors.themeInactive}
                    returnKeyType={'done'}
                    placeholder={'请输入Wifi密码'}
                    secureTextEntry={this.state.hiddenPass}
                    onChangeText={(text)=>{
                        this.state.pass = text
                    }}
                />
                <TouchableOpacity style={styles.hiddenBtn} onPress={()=>{
                    this.setState({ hiddenPass: !this.state.hiddenPass})
                }}>
                    <Image style={styles.hideImg} source={hideIcon}/>
                </TouchableOpacity>
            </View>
        )
    }

    renderSureBtn(){
        const Colors = this.props.themeInfo.colors
        return(
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={[styles.sureBtn,{backgroundColor: Colors.themeBg}]}
                onPress={()=>{ this.submit() }}
            >
                <Text style={styles.sureText}>提交</Text>
            </TouchableOpacity>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors
        return (
            <View style={{backgroundColor: Colors.themeBaseBg, flex: 1}}>
                {this.renderNameInput()}
                {this.renderPassInput()}
                {this.renderSureBtn()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 15,
        paddingLeft: 10,
        marginRight: 5
    },
    inputWrapper:{
        marginTop:10,
        paddingRight: 22
    },
    input: {
        width:'100%',
        height:50,
        paddingHorizontal: 20,
        marginRight: 20
    },
    paasInputWrapper:{
        marginTop:10,
        paddingRight: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    passInput:{
        flex: 1,
        height:50,
        paddingHorizontal: 20
    },
    hiddenBtn:{
        marginLeft: 10,
        height: 50,
        paddingHorizontal: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    hideImg:{
       width: 22,
       height: 22,
       resizeMode: 'contain' 
    },
    sureBtn:{
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
        height: 44
    },
    sureText:{
        color: Colors.tabActiveColor,
        fontSize: 16
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(NetworkSetting)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	TextInput,
	DeviceEventEmitter,
	ImageBackground,
	Switch
} from 'react-native';

import { Colors,NetUrls } from '../../../common/Constants';
import HeaderLeftView from '../../../common/CustomComponent/HeaderLeftView';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { StatusBarHeight } from '../../../util/ScreenUtil';

class ControllerSetting_HardwareUpdate extends Component {

	static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeftView navigation={navigation} text={'固件升级'} />
		}
	};

	constructor(props) {
		super(props);
		const { getParam } = this.props.navigation
		this.callBack = getParam('callBack')
		this.controlData = getParam('controlData') || {}

		this.state = {
			currentVersion: '1.0',
			newestVersion: '1.0'
		}
	}

	componentDidMount() {
		this.requestControllerVersion()
	}

	componentWillUnmount() {

	}

	//获取控制器 版本信息
	async requestControllerVersion() {
		try {
			let data = await postJson({
				url: NetUrls.controllerVersion,
				params: {
					
				}
			});
			if (data && data.code == 0) {
				this.setState({
					currentVersion: data.result && data.result.versions1,
					newestVersion: data.result && data.result.versions2
				})
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

	getHardwareInfo(){
		return(
			<View style={{width:'100%',flex:1}}>
				<View style={{marginTop:20,paddingHorizontal:16}}>
					<Text style={{marginTop:10,color:Colors.themeTextBlack,fontSize:15}}>当前固件版本：{this.state.currentVersion}</Text>
					<Text style={{marginTop:30,color:Colors.themeTextBlack,fontSize:15}}>最新固件版本：{this.state.newestVersion}</Text>
				</View>
			</View>
		)
	}

	getButton(){
		let btn = (
			<View style={{...styles.btnTouch,backgroundColor:Colors.themeBGInactive}}>
				<Text style={{color:Colors.white}}>当前已是最新版本</Text>
			</View>
		)
		if(this.state.currentVersion != this.state.newestVersion){
			btn = (
				<TouchableOpacity activeOpacity={0.9} style={styles.btnTouch} onPress={()=>{

				}}>
					<Text style={{color:Colors.white}}>升级</Text>
				</TouchableOpacity>
			)
		}
		return(
			<View style={{marginTop:40,paddingHorizontal:16,paddingVertical:10}}>
				{btn}
			</View>
		)
	}

	render() {
		return (
			<ScrollView style={styles.container}>
				{this.getHardwareInfo()}
				{this.getButton()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.white,
		flex: 1
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight: 5
	},
	navText: {
		color: Colors.tabActiveColor,
		fontSize: 15
	},
	btnTouch:{
		height:40,
		width:'100%',
		backgroundColor:Colors.tabActiveColor,
		borderRadius:4,
		justifyContent:'center',
		alignItems:'center'
	}
});

export default ControllerSetting_HardwareUpdate

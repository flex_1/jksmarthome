/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    View,
    BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import { WebView } from 'react-native-webview';
import { BottomSafeMargin } from '../../../util/ScreenUtil';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../../CommonPage/Navigation/HeaderTitle';

class BleConfigWebPage extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} onClick={()=>{ navigation.getParam('goBackWeb')() }}/>,
        headerBackground: <HeaderBackground/>,
        headerTitle: <HeaderTitle title={navigation.getParam('title')}/>,
    })

    constructor(props) {
        super(props);
        const {getParam,setParams} = this.props.navigation

        this.callBack = getParam('callBack')
        
        this.state = {
            url: getParam('url'),
            backLock: false,
            canGoback: false
        }

        setParams({
            goBackWeb: this.goBackBtnClick.bind(this)
        });
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress',this.onBackButtonPressAndroid);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
    }

    // 安卓物理返回按钮
    onBackButtonPressAndroid = ()=>{
        if(this.state.canGoback){
            this.goBackBtnClick()
            return true
        }
		return false
	}

    goBackBtnClick(){
        const {goBack} = this.props.navigation

        if(this.state.canGoback){
            this.webview?.goBack()
        }else{
            goBack()
        }
    }

    onNavigationStateChange(navState) {
        


        console.log("navState:  " + JSON.stringify(navState));
        // if(navState.url.indexOf("about:blank")!=-1){
        //     if(!this.state.backLock){
        //         this.state.backLock = true
        //         this.props.navigation.goBack()
        //     }
        //     this.callBack && this.callBack()
        // }
        if(navState.url.indexOf("/h5Index")!=-1){
            this.setState({
                canGoback: false
            })
        }else{
            this.setState({
                canGoback: true
            })
        }
        if(!navState.title){
            return
        }
        if(navState.title.includes('www')){
            return
        }
        if(navState.title.includes('http')){
            return
        }
        const {setParams} = this.props.navigation
        setParams({
            title: navState.title
        })
    }

    getWebview(){
        return(
            <WebView
                style={styles.container}
                ref={e => this.webview = e}
                // source={{ uri: this.state.url }}
                source={{ uri: this.state.url, method: 'GET', headers: { 'Cache-Control':'no-cache'} }}
                incognito={true}
                mixedContentMode='always'
                onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                onMessage={(e) => {
                    console.log(e.nativeEvent.data)
                }}
            />
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.getWebview()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: BottomSafeMargin
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(BleConfigWebPage)

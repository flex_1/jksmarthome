/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Animated,
	Dimensions,
    Modal,
    Keyboard,
    Alert,
    BackHandler
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';
import HeaderTitle from '../../CommonPage/Navigation/HeaderTitle';
import PCtrPanel from '../../CommonPage/ProtocolControlPanel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AnimateButton from '../../../common/CustomComponent/AnimateButton';

const screenW = Dimensions.get('screen').width;
const PCPanelStyleList = [  {id: 1, name: '样式1', componet:PCtrPanel.PCtrPanel1},
                            {id: 2, name: '样式2', componet:PCtrPanel.PCtrPanel2},
                            {id: 3, name: '样式3', componet:PCtrPanel.PCtrPanel3},
                            {id: 4, name: '样式4', componet:PCtrPanel.PCtrPanel4}
                        ]

class RIStudySetting extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={navigation.getParam('title')}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
		super(props);
		const { getParam,setParams } = props.navigation;
        
        this.deviceId = getParam('deviceId')
        this.brand = getParam('brand')   // 设备类型
        this.gatewayId = getParam('gatewayId')

        this.state = {
            currentStyleId: 1,
            currentComponet: PCtrPanel.PCtrPanel1,

            showFunctionModal: false,
            isCustomBtn: false,
            buttonList: null,
            customButtonList: null,
            currentBtnParams: {},
            currentBtnIndex: 0,
            btnName: '',
            centerLabel: '点击绑定',
            bindingStatus: 0,   //0-未开始 1-正在绑定  2-绑定成功  3-绑定失败
            bindSeriaNO: '',
            currentBtnIsConfig: false,
            isDeleting: false
        }
    }
    
	componentDidMount() {
        this.requestButtonInfo({deviceId: this.deviceId})
	}

	componentWillUnmount(){
        
    }

    // 请求列表
    async requestButtonInfo(params) {
        SpinnerManager.show()
        try {
            let data = await postJson({ 
                url: NetUrls.initStyleCommonProtocol,
                params: {
                    ...params
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                let currentComponet = PCtrPanel.PCtrPanelCustom
                if(this.brand != 'OTHER'){
                    const styleIndex = data.result?.style - 1
                    if(styleIndex < 0 || styleIndex > 3){
                        ToastManager.show('样式加载出错。')
                    }
                    currentComponet = PCPanelStyleList[styleIndex]['componet']
                }
                this.setState({
                    buttonList : data.result?.buttonList || [],
                    customButtonList: data.result?.customButtonList || [],
                    currentStyleId: data.result?.style,
                    currentComponet: currentComponet
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    // 添加按钮
    async requestAddProtocolButton(callBack) {
        let params = params || {}
        if(this.brand  == 'OTHER'){
            params.style = 0
        }else{
            params.style = this.state.currentStyleId
        }
        SpinnerManager.show()
        try {
            let data = await postJson({ 
                url: NetUrls.addCommonProtocolButton,
                params: {
                    deviceId: this.deviceId,
                    gatewayId: this.gatewayId,
                    ...params
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                callBack && callBack(data.result?.button)
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    // 修改按钮
    async requestUpdateButton(params, callBack) {
        params = params || {}
        SpinnerManager.show()
        try {
            let data = await postJson({ 
                url: NetUrls.updateCommonProtocolButton,
                params: {
                    ...params
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                callBack && callBack()
                ToastManager.show('保存成功')
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    // 删除按钮
    async requestDeleteProtocolButton(params, callBack) {
        params = params || {}
        SpinnerManager.show()
        try {
            let data = await postJson({ 
                url: NetUrls.deleteCommonProtocolButton,
                params: {
                    deviceId: this.deviceId,
                    ...params
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                callBack && callBack()
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    // 开始绑定按钮
    async requestBindCommonProtocolButton(buttonId) {
        try {
            let data = await postJson({ 
                url: NetUrls.bindCommonProtocolButton,
                params: {
                    deviceId: this.deviceId,
                    buttonId: buttonId
                }
            });
            this.animateButton.stopAnimation()
            if (data.code == 0) {
                this.setState({
                    bindingStatus: 2,
                    currentBtnIsConfig: true,
                    showFunctionModal: false
                })
                this.panelBtnComplete({isConfig: 1})
                ToastManager.show('配置成功。')
            } else {
                this.setState({
                    bindingStatus: 3
                })
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.animateButton.stopAnimation()
            this.setState({
                bindingStatus: 3
            })
            ToastManager.show('网络错误')
        }
    }

    panelBtnClick(index, isCustomBtn){
        // 删除状态下按钮
        if(this.state.isDeleting && isCustomBtn){
            let btnData = this.state.customButtonList[index] || {}
            let params = {buttonId: btnData.buttonId}
            this.requestDeleteProtocolButton(params, ()=>{
                this.state.customButtonList.splice(index, 1)
                this.setState({
                    customButtonList: this.state.customButtonList
                })
            })
            return
        }

        if(!isCustomBtn && !this.state.buttonList){
            ToastManager.show('数据加载中，请稍后。')
            return
        }else if(isCustomBtn && !this.state.customButtonList){
            ToastManager.show('数据加载中，请稍后。')
            return
        }
        let btnParams = {}
        if(isCustomBtn){
            btnParams = this.state.customButtonList[index]
        }else{
            btnParams = this.state.buttonList[index]
        }
        this.setState({
            showFunctionModal: true,
            currentBtnIndex: index,
            currentBtnParams: btnParams,
            isCustomBtn: isCustomBtn,
            currentBtnIsConfig: btnParams?.isConfig,
            btnName: btnParams?.name
        })
    }

    // 按钮配置成功操作
    panelBtnComplete(params){
        let btnParams = {}
        if(this.state.isCustomBtn){
            btnParams = this.state.customButtonList[this.state.currentBtnIndex] || {}
        }else{
            btnParams = this.state.buttonList[this.state.currentBtnIndex] || {}
        }

        params = params || {}
        if(params.isConfig){
            btnParams.isConfig = params.isConfig
        }else if(params.name){
            btnParams.name = params.name
        }
        
        if(this.state.isCustomBtn){
            this.state.customButtonList[this.state.currentBtnIndex] = btnParams
        }else{
            this.state.buttonList[this.state.currentBtnIndex] = btnParams
        }
        this.setState({
            buttonList: this.state.buttonList,
            customButtonList: this.state.customButtonList
        })
    }

    //添加按钮点击事件
    addBtnClick(){
        this.requestAddProtocolButton((button)=>{
            if(button){
                this.state.customButtonList.push(button)
            }
            this.setState({
                customButtonList: this.state.customButtonList
            })
        })
    }

    //删除按钮点击事件
    deleteBtnClick(){
        this.setState({
            isDeleting: !this.state.isDeleting
        })
    }

    renderTopSelection(){
        // 其他（自定义类型设备）
        if(this.brand  == 'OTHER'){
            return null
        }
        const Colors = this.props.themeInfo.colors;

        let list = PCPanelStyleList.map((value, index)=>{
            const icon = value.id == this.state.currentStyleId ? require('../../../images/ProtocolControl/selected.png')
            : require('../../../images/ProtocolControl/unselected.png')

            return(
                <TouchableOpacity key={'item_'+index} style={styles.item} onPress={()=>{
                    if(value.id != this.state.currentStyleId){
                        this.requestButtonInfo({style: value.id, deviceId: this.deviceId})
                    }
                }}>
                    <Image style={styles.selectIcon} source={icon}/>
                    <Text style={[styles.name,{color: Colors.themeText}]}>{value.name}</Text>
                </TouchableOpacity>
            )
        })

        return(
            <View style={styles.selectWrapper}>
                {list}
            </View>
        )
    }

    renderMainController(){
        if(!this.state.buttonList){
            return null
        }
        const Panel = this.state.currentComponet
        if(!Panel){
            return null
        }

        return(
            <Panel 
                isSetting={true}
                isDeleting={this.state.isDeleting}
                customButtonList={this.state.customButtonList}
                btnClick={(index, isCustomBtn)=>{
                    this.panelBtnClick(index, isCustomBtn)
                }}
                addBtnClick={()=>{
                    this.addBtnClick()
                }}
                deleteBtnClick={()=>{
                    this.deleteBtnClick()
                }}
            />
        )
    }

    renderNameInputItem(){
        if(!this.state.isCustomBtn){
            return null
        }
        return(
            <View style={[styles.itemWrapper,{height: 70}]}>
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={[styles.input,{color: Colors.themeTextBlack}]}
                        maxLength={4}
                        value={this.state.btnName}
                        returnKeyType={'send'}
                        enablesReturnKeyAutomatically={true}
                        placeholder={'修改名称'}
                        onChangeText={(text) => {
                            this.setState({
                                btnName: text
                            })
                        }}
                    />
                </View>
                <TouchableOpacity style={styles.saveTouch} onPress={()=>{
                    if(!this.state.btnName){
                        ToastManager.show('名称不能为空')
                        return
                    }
                    let btnParams = this.state.currentBtnParams || {}
                    this.requestUpdateButton({buttonId: btnParams.buttonId, name: this.state.btnName}, ()=>{
                        this.panelBtnComplete({name: this.state.btnName})
                    })
                }}>
                    <Text style={styles.sendText}>保存</Text>
                </TouchableOpacity>
            </View>
        )
    }

    // center animation button
    showAniButton() {
        let centerLabel = '开始绑定'
        if(this.state.bindingStatus == 1){
            centerLabel = '正在绑定...'
        }
        let btnParams = this.state.currentBtnParams || {}
        return(
            <AnimateButton
                initialPosition={{ top: 0, left: screenW * 0.5 }}
                rippleColor={'#008CFF'} 
                style = {{ width:'100%',alignItems:'center'}}
                ref={e => this.animateButton = e}
                centerLabel = {centerLabel}
                onClick={()=>{
                    if(this.state.bindingStatus == 1){
                        return
                    }
                    this.requestBindCommonProtocolButton(btnParams.buttonId)
                    this.setState({
                        bindingStatus: 1
                    },()=>{
                        this.animateButton.startAnimation();
                    })
                }}
            />
        )
    }

    renderBindingView(){
        return(
            <View style={{justifyContent: 'center',alignItems: 'center',height: screenW * 0.5,width:'100%',marginTop:20}}>
                {this.showAniButton()}
            </View>
        )
    }

    renderStatusText(){
        let label = ''
        let subLabel = ''
        if(this.state.currentBtnIsConfig){
            label = '该按键已绑定'
        }else{
            label = '该按键未绑定'
        }
        if(this.state.bindingStatus == 0){
            if(this.state.currentBtnIsConfig){
                subLabel = '点击进行重新绑定'
            }else{
                subLabel = '点击开始绑定'
            }
        }else if(this.state.bindingStatus == 1){
            subLabel = '正在绑定...'
        }else if(this.state.bindingStatus == 2){
            subLabel = '绑定成功!点击可重新绑定'
        }else if(this.state.bindingStatus == 3){
            subLabel = '绑定失败!点击可重新绑定'
        }

        return(
            <View style={{width: '100%',marginTop: 20,justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize: 16, fontWeight: '500', color: '#008CFF'}}>{label}</Text>
                <Text style={{marginTop: 15,fontSize: 14, fontWeight: '400', color: Colors.themeTextLightGray}}>{subLabel}</Text>
            </View>
        )
    }

    renderCancelBtn(){
        return(
            <View style={styles.bottomTouchWrapper}>
                <TouchableOpacity style={styles.cancelTouch} onPress={()=>{
                    this.animateButton.stopAnimation()
                    this.interval && clearInterval(this.interval);
                    this.setState({
                        showFunctionModal: false,
                        bindingStatus: 0
                    })
                }}>
                    <Text style={styles.btnText}>取消</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderModal(){
        if(!this.state.showFunctionModal){
            return null
        }

        return(
            <View style={styles.modalWrapper}>
                <TouchableOpacity activeOpacity={1} style={styles.topWrapper} onPress={()=>{
                    if(this.state.bindingStatus == 1){
                        return
                    }
                    this.setState({
                        showFunctionModal: false
                    })
                }}/>
                <View style={styles.bottomWrapper}>
                    <KeyboardAwareScrollView contentContainerStyle={styles.content} extraScrollHeight={20} extraHeight={300}>
                        {this.renderNameInputItem()}
                        {this.renderBindingView()}
                        {this.renderStatusText()}
                        {this.renderCancelBtn()}
                    </KeyboardAwareScrollView>
                </View>
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors;

		return (
			<View style={[styles.wrapper,{backgroundColor: Colors.themeBaseBg}]}>
				<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
                    {this.renderTopSelection()}
                    {this.renderMainController()}
                </View>
                {this.renderModal()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	wrapper: {
        flex: 1
    },
    main:{
        marginTop: 6,
        flex: 1,
        alignItems: 'center'
    },
    mianCtrWrapper:{
        marginTop: 15, 
        flex: 1,
        width: '100%',
        alignItems: 'center'
    },
    selectWrapper:{
        flexDirection: 'row',
        marginTop: 20,
        flexWrap:'wrap',
        paddingLeft: 20,
        paddingRight: 10
    },
    item:{
        flexDirection: 'row',
        alignItems:'center',
        paddingHorizontal:5,
        paddingVertical: 6,
        marginRight: 8
    },
    selectIcon:{
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    name:{
        fontSize: 16,
        fontWeight: '400',
        marginLeft: 6,
    },
    modalWrapper:{
        position: 'absolute',
        width: '100%',
        height: '100%',
        left: 0,
        top: 0
    },
    topWrapper:{
        backgroundColor: Colors.translucent,
        flex: 3
    },
    bottomWrapper:{
        flex: 7,
        backgroundColor: Colors.white
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    itemWrapper:{
        height: 54,
        flexDirection: 'row',
        alignItems:'center',
        paddingHorizontal: 16,
        marginTop: 10
    },
    content:{
        paddingBottom: BottomSafeMargin + 20
    },
    inputWrapper:{
        backgroundColor: '#ADADAD1F',
        borderRadius: 4,
        height: 44,
        flex: 1,
        paddingHorizontal: 10
    },
    input:{
        height: '100%'
    },
    saveTouch:{
        justifyContent:'center',
        alignItems:'center',
        width:100,
        height: 44,
        backgroundColor: '#008CFF',
        marginLeft: 8,
        borderRadius: 4
    },
    sendText:{
        fontSize: 16,
        color: '#fff',
        fontWeight: '500'
    },
    bottomTouchWrapper:{
        marginTop: 20,
        paddingHorizontal: 20,
        width: '100%'
    },
    cancelTouch:{
        height: 44,
        borderRadius: 22,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#DCDEE0'
    },
    btnText:{
        fontSize: 16,
        fontWeight: '400',
        color: Colors.themeTextBlack
    },
});
export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(RIStudySetting)



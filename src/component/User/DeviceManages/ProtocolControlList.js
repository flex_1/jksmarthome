import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity,
    RefreshControl,
    DeviceEventEmitter,
    SwipeableFlatList
} from 'react-native';
import Slider from "react-native-slider";
import { connect } from 'react-redux';
import {GetdeviceRouter, NetUrls, NotificationKeys,Colors} from '../../../common/Constants';
import {postJson} from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';
import { BottomSafeMargin } from '../../../util/ScreenUtil';

//侧滑最大距离
const maxSwipeDistance = 120
//侧滑按钮个数
const countSwiper = 2

class ProtocolControlList extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text: '设置', onPress:()=>{
                    navigation.getParam('settingBtn')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const {getParam, setParams} = props.navigation
        this.getwayData = getParam('getwayData')
        this.callBack = getParam('callBack')
        this.attributeType = getParam('attributeType')

        this.state = {
            loading: true,
            isRefreshing: false,
            listData: null
        }

        setParams({
            settingBtn: this.settingBtnClick.bind(this)
        })
    }

    componentDidMount() {
        this.requestList()
        this.deviceStatusNoti = DeviceEventEmitter.addListener(NotificationKeys.kDeviceStatusNotification,()=>{
			this.requestList()
		})
    }

    componentWillUnmount(){
        this.deviceStatusNoti.remove()
    }

    // 设置按钮点击
    settingBtnClick(){
        const {navigate,setParams} = this.props.navigation
        navigate('GatewaySetting',{
            title: this.getwayData.name + '设置',
            name: this.getwayData.name,
            getwayData: this.getwayData,
            callBack: (data)=>{
                if(data && data.name){
                    setParams({title: data.name})
                }
                this.callBack && this.callBack()
            }
        })
    }

    // 请求列表
    async requestList() {
        try {
            let data = await postJson({
                url: NetUrls.setmanageSecond,
                params: {
                    deviceId: this.getwayData?.id
                }
            });
            this.setState({loading: false,isRefreshing:false})
            if (data.code == 0) {
                this.setState({
                    listData: data.result || []
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({loading: false,isRefreshing:false})
            ToastManager.show('网络错误')
        }
    }

    // 删除设备
    async deleteProtocol(id) {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.deleteInstructionProtocol,
                params: {
                    id: id
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.requestList()
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    //关闭侧滑栏
	closeSwiper(){
		this.swiperflatlist?.setState({openRowKey:null})
	}

    _devicesExtraUniqueKey(item, index){
		return "devices_index_" + index;
	}

    renderAddBtn(){
        if(this.state.listData?.length >= 4){
            return null
        }
        
        return(
            <TouchableOpacity style={{marginTop: 20,marginHorizontal:16, justifyContent: 'center',alignItems:'center'}} onPress={()=>{
                this.props.navigation.navigate('ProtocolControlCreat',{
                    title: '添加协议器',
                    gatewayId: this.getwayData?.id,
                    attributeType: this.attributeType,
                    callBack: ()=>{
                        this.requestList()
                    }
                })
            }}>
                <Image style={{width: 32,height:32,resizeMode:'contain'}} source={require('../../../images/ProtocolControl/add.png')}/>
            </TouchableOpacity>
        )
    }

    getQuickActions(rowData, index){
        const {navigate} = this.props.navigation

		return (
			<View style={styles.quickAContent}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.quick}  
                    onPress={()=>{
					    this.closeSwiper()
					    this.deleteProtocol(rowData.deviceId)
				    }}
                >
					<Image style={{width:40,height:40}} source={require('../../../images/deviceIcon/delete.png')}/>
				</TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.7} 
                    style={styles.quick} 
                    onPress={()=>{
					    this.closeSwiper()
                        if(this.attributeType == 'instruction-protocol-gateway'){
                            navigate('ProtocolControlCreat',{
                                title: rowData.name + '设置',
                                gatewayId: this.getwayData?.id,
                                deviceId: rowData.deviceId,
                                attributeType: this.attributeType,
                                callBack: ()=>{
                                    this.requestList()
                                }
                            })
                        }else{
                            navigate('RIStudySetting',{
                                deviceId: rowData.deviceId,
                                title: rowData.name,
                                gatewayId: this.getwayData?.id,
                                brand: rowData.brand,
                                attributeType: this.attributeType,
                                callBack: ()=>{
                                    this.requestList()
                                }
                            })
                        }
				    }}
                >
					<Image style={{width:40,height:40}} source={require('../../../images/deviceIcon/setting.png')}/>
				</TouchableOpacity>
			</View>
		)
    }

    renderNoContent(){
        const Colors = this.props.themeInfo.colors;

        if(this.state.loading && !this.state.listData){
            return (
                <View style={styles.loading}>
                    <Text style={{color: Colors.themeTextLight}}>加载中...</Text>
                </View>
            )
        }
        if(!this.state.listData){
            return (
                <View style={styles.loading}>
                    <Text style={{color: Colors.themeTextLight}}>网络错误，请稍后重试</Text>
                </View>
            )
        }
        if(this.state.listData.length <= 0) {
            return (
                <View style={styles.loading}>
                    {this.renderAddBtn()}
                    <Text style={{marginTop: 10,color: Colors.themeTextLight}}>暂无设备，去添加</Text>
                </View>
            )
        }
    }

    renderRowItem(rowData, index){
        const Colors = this.props.themeInfo.colors;
        const deviceIcon = this.props.themeInfo.isDark ? { uri: rowData.nightIcon } : { uri: rowData.dayIcon }
        const {navigate} = this.props.navigation
        // 楼层房间信息
        let floorView = null
        if(rowData.floorText && rowData.roomName){
            floorView = <Text style={{fontSize:14,marginTop:5,color:Colors.themeTextLight}}>{rowData.floorText + '  ' + rowData.roomName}</Text>
        }
        
        return(
            <View style={styles.itemWrapper}>
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={[styles.itemTouch,{backgroundColor: Colors.themeBg}]} 
                    onPress={() => {
                        this.closeSwiper()
                        if(this.attributeType == 'instruction-protocol-gateway'){
                            navigate('ProtocolController',{
                                deviceData: rowData,
                                brand: rowData.brand,
                                title: rowData.name
                            })
                        }else{
                            navigate('RIStudyController',{
                                deviceData: rowData,
                                brand: rowData.brand,
                                title: rowData.name
                            })
                        }
                    }}
                >
                    <Image style={styles.img} source={deviceIcon}/>
                    <View style={{marginLeft: 15,flex:1}}>
                        <Text style={[styles.name,{color: Colors.themeText}]}>{rowData.name}</Text>
                        {floorView}
                    </View>
                    <TouchableOpacity style={styles.settingTouch} onPress={()=>{
                        this.closeSwiper()
                        if(this.attributeType == 'instruction-protocol-gateway'){
                            navigate('ProtocolControlSetting',{
                                deviceId: rowData.deviceId,
                                title: rowData.name,
                                gatewayId: this.getwayData?.id,
                                brand: rowData.brand,
                                callBack: ()=>{
                                    this.requestList()
                                }
                            })
                        }else{
                            navigate('RIStudySetting',{
                                deviceId: rowData.deviceId,
                                title: rowData.name,
                                gatewayId: this.getwayData?.id,
                                brand: rowData.brand,
                                callBack: ()=>{
                                    this.requestList()
                                }
                            })
                        }
                    }}>
                        <Image style={{width: 18, height: 19, resizeMode:'contain'}} source={require('../../../images/ProtocolControl/setting.png')}/>
                    </TouchableOpacity>
                </TouchableOpacity>
            </View>
        )
    }

    renderFooter(){
        if(this.state.loading && !this.state.listData){
            return null
        }
        if(!this.state.listData || this.state.listData.length <= 0){
            return null
        }
        return this.renderAddBtn()
    }

    renderList(){
        return(
            <SwipeableFlatList
                ref={e => this.swiperflatlist = e}
                contentContainerStyle={styles.contentStyle}
                scrollIndicatorInsets={{right: 1}}
                removeClippedSubviews={false}
                data={this.state.listData}
                keyExtractor={this._devicesExtraUniqueKey}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
                initialNumToRender={10}
                ListEmptyComponent = {this.renderNoContent()}
                renderQuickActions={({ item, index }) => this.getQuickActions(item,index)}//创建侧滑菜单
                maxSwipeDistance={maxSwipeDistance}
                bounceFirstRowOnMount={false}
                ListFooterComponent={() => this.renderFooter()}
            />
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors;

        return (
            <View style={{flex: 1,backgroundColor: Colors.themeBaseBg}}>
                {this.renderList()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    scrollContent:{
        paddingBottom: 50,
    },
    
    icon:{
        width: 40, 
        height: 40, 
        marginLeft: 16, 
        resizeMode:'contain',
        borderRadius: 20
    },
    inputWrapper:{
		width: '100%',
		height: 80,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:15
    },
    statusTextWrapper:{
		height:30,
		borderRadius:15,
		borderWidth:1,
		justifyContent:'center',
		alignItems:'center',
		marginRight:10,
		paddingHorizontal: 15,
    },
    switchBtn:{
        paddingHorizontal: 10,
        height:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    switchBtnTouch:{
		width:90,
		height:34,
		borderRadius:17,
		borderWidth:1,
		justifyContent:'center',
		alignItems:'center'
    },
    
    switchTouch:{
        height:'100%',
        paddingHorizontal: 16,
        justifyContent:'center'
    },
    sliderItem:{
        width: '100%',
		borderRadius:5,
		marginTop:15
    },
    sliderTouch: {
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        height: 65
    },
    bindTouch:{
        height: '100%',
        paddingHorizontal:20,
        justifyContent:'center',
        alignItems:'center',
        marginLeft: 10,
        flexDirection: 'row'
    },
    rightArrow:{
        width: 7, 
        height: 13, 
        marginLeft:5,
        resizeMode:'contain'
    },
    iconWrapper:{
        width: 36, 
        height:36, 
        borderRadius: 18, 
        marginLeft: 16,
        justifyContent:'center',
        alignItems:'center',
    },
    iconSubWrapper:{
        width: 22, 
        height:22, 
        borderRadius: 11,
        justifyContent:'center',
        alignItems:'center',
    },
    iconIndex:{
        fontSize:14, 
        fontWeight: '400'
    },
    contentStyle:{
        paddingBottom: BottomSafeMargin + 10
    },

    itemWrapper:{ 
		width: '100%', 
        marginTop: 15,
        justifyContent: 'center', 
        alignItems: 'center',
        paddingHorizontal: 16
    },
    itemTouch: {
		flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 5,
        width: '100%',
        height: 60,
        paddingLeft: 16
    },
    img:{
        width: 40,
        height: 40,
        resizeMode: 'contain'
    },
    name:{
        fontSize: 16,
        fontWeight: '400'
    },
    settingTouch:{
        paddingHorizontal: 16, 
        height:'100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    loading:{
        alignItems: 'center',
        marginTop: '50%'
    },
    //侧滑菜单的样式
	quickAContent: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginRight: 16,
		marginTop: 12,
		overflow: 'hidden',
	},
	quick: {
		alignItems: 'center',
		justifyContent: 'center',
		width: maxSwipeDistance / countSwiper,
	},
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ProtocolControlList)

import React, {Component} from 'react';
import {
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity,
    RefreshControl,
    Modal,
    FlatList,
    TextInput,
    Keyboard,
    Animated,
    Easing
} from 'react-native';
import { connect } from 'react-redux';
import {Colors, NetUrls, NotificationKeys,NetParams} from '../../../common/Constants';
import {postJson} from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';
import {ImagesDark, ImagesLight} from '../../../common/Themes';


class AddMultiFunctionDevice extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text: '保存', onPress:()=>{
                    navigation.getParam('saveBtn')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

    constructor(props) {
        super(props);
        const {getParam, setParams} = props.navigation
        this.getwayData = getParam('getwayData')
        this.type = getParam('type')
        this.callBack = getParam('callBack')
        this.maxCount = getParam('maxCount')

        this.state = {
            loading: true,
            isRefreshing: false,
            listData: null,
            selectType: 0,
        
            modalVisible: false,
            inputString: '',
            keyboardHeight: -60,
            leftCount: this.maxCount,

            compositeAnim: new Animated.Value(0),
            selectId: null,
            selectName: '',
            selectedIds: [],
            idWithRemark: {}
        }

        setParams({
            saveBtn: this.saveBtnClick.bind(this)
        })
    }

    componentDidMount() {
        this.requestList()

        let KeyboardShowEventName = 'keyboardWillShow'
        let KeyboardHideEventName = 'keyboardWillHide'
        if(Platform.OS == 'android'){
            KeyboardShowEventName = 'keyboardDidShow'
            KeyboardHideEventName = 'keyboardDidHide'
        }
        this.keyboardWillShowListener = Keyboard.addListener(KeyboardShowEventName,(e)=>{
            this._keyboardDidShow(e)
        })
        this.keyboardWillHideListener = Keyboard.addListener(KeyboardHideEventName,(e)=>{
            this._keyboardDidHide(e)
        })
    }

    componentWillUnmount(){
        this.keyboardWillShowListener.remove()
    }

    // 键盘弹出
    _keyboardDidShow(e){
        
        let keyboardHeight = e.endCoordinates.height
        // this.setState({
        //     keyboardHeight: Platform.OS == 'ios' ? keyboardHeight : 0,
        // })

        Animated.timing(this.state.compositeAnim, {
            toValue: keyboardHeight,
            easing: Easing.keyboard,
            duration: 250
        }).start()
        
    }
    // 键盘隐藏
    _keyboardDidHide(e){
        Animated.timing(this.state.compositeAnim, {
            toValue: 0,
            easing: Easing.keyboard,
            duration: 250
        }).start()
    }

    // 设置按钮点击
    saveBtnClick(){
        if(!this.state.listData || this.state.listData.length<=0){
            ToastManager.show('设备加载中')
            return
        }
        let list = []
        for (const id of this.state.selectedIds) {
            let remark = this.state.idWithRemark[id]
            list.push({deviceId: id, remark: remark})
        }
        if(list.length <= 0){
            ToastManager.show('请至少添加一个设备。')
            return
        }
        this.requestAddDevice(list)
    }

    // 请求列表
    async requestList(params) {
        params = params || {}
        try {
            let data = await postJson({
                url: NetUrls.multiFunctionList,
                params: {
                    type: this.type,
                    ...params
                }
            });
            this.setState({loading: false,isRefreshing:false})
            if (data.code == 0) {
                this.setState({
                    listData: data.result || []
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({loading: false,isRefreshing:false})
            ToastManager.show('网络错误')
        }
    }

    // 保存
	async requestAddDevice(list){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.addMultiFunction,
				params: {
					gatewayId: this.getwayData && this.getwayData.id,
                    list: list
				}
			});
            SpinnerManager.close()
			if (data.code == 0) {
				this.callBack && this.callBack()
                this.props.navigation.pop()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    selectBtnClick(rowData){
        if(this.state.leftCount <= 0){
            ToastManager.show('添加设备已达最大值。')
            return
        }
        let idIndex = this.state.selectedIds.indexOf(rowData.id)
        if(idIndex == -1){
            this.state.selectedIds.push(rowData.id)
        }else{
            this.state.selectedIds.remove(rowData.id)
        }
        this.setState({
            selectedIds: this.state.selectedIds,
            leftCount: (this.maxCount - this.state.selectedIds.length) || 0
        })
    }

    submitClick(){
        let id_key = this.state.selectId
        this.state.idWithRemark[id_key] = this.state.inputString
        this.setState({
            modalVisible: false,
            idWithRemark: this.state.idWithRemark
        })
    }

    renderKeyboard(){
        const Colors = this.props.themeInfo.colors;
        let bottom = Platform.OS == 'ios' ? this.state.compositeAnim : 0

        return(
            <Animated.View style={{backgroundColor:'#fff',zIndex:100,width:'100%',position:'absolute',padding:10,bottom:bottom}}>
                <Text style={styles.remarkTitle}>{this.state.selectName} 添加备注:</Text>
                <View style={styles.inputWraper}>
                    <TextInput
                        ref={e => this.remarkInput = e}
                        value={this.state.inputString}
                        style={styles.input}
                        blurOnSubmit={false}
                        returnKeyLabel={'完成'}
                        returnKeyType={'done'}
                        underlineColorAndroid="transparent"
                        maxLength={16}
                        onChangeText={(text)=>{
                            this.setState({inputString:text})
                        }}
                        onSubmitEditing={()=>{
                            this.submitClick()
                        }}
                    />
                </View>
            </Animated.View>
        )
    }

    renderRemarkModal(){
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                statusBarTranslucent={false}
                onShow={() => {
                    this.remarkInput.focus()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity
                    style={styles.modalTouchable}
                    activeOpacity={1}
                    onPress={() => {
                        this.setState({modalVisible: false})
                    }}
                >
                    {this.renderKeyboard()}
                </TouchableOpacity>
            </Modal>
        )
    }

    renderSearchHeader(){
        const {pop} = this.props.navigation
        const Colors = this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}

		return(
            <View style={[styles.topWrapper,{backgroundColor:Colors.themeBg}]}>
                
                <View style={styles.searchWrapper}>
                    <Image style={styles.searchIcon} source={require('../../../images/search_gray.png')}/>
                    <TextInput
                        style={{flex:1,paddingLeft:10}}
                        placeholder={'请输入关键字'}
                        placeholderTextColor={Colors.themeBGInactive}
                        returnKeyType={'search'}
                        clearButtonMode={'while-editing'}
                        maxLength={16}
                        onChangeText={(text) => {
                            this.state.searchText = text
                        }}
                        onSubmitEditing={()=>{
                            this.requestList({remark: this.state.searchText})
                        }}
                    />
                </View>
                
            </View>
		)
	}

    renderTips(){
        const Colors = this.props.themeInfo.colors;
        let leftCount = this.state.leftCount
        
        return(
            <View style={[styles.tipsWrapper,{backgroundColor: Colors.themeBg,borderBottomColor: Colors.themeBaseBg}]}>
                <Text style={{fontSize:13,color: Colors.themeTextLight}}>您还可以添加
                    <Text style={{fontSize:14,color: Colors.themeButton}}>{leftCount}</Text>
                路设备</Text>
            </View>
        )
    }

    renderRemark(rowData, index){
        const Colors = this.props.themeInfo.colors;
        let remark = this.state.idWithRemark[rowData.id]
        let remarkText = remark || '点击添加备注'
        let remarkColor = remark ? Colors.themeText : Colors.themeTextLight

        return (
            <View style={styles.bottomTouch}>
                <Text style={{fontSize: 14, color: Colors.themeTextLight}}>备注</Text> 
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.remarkTouch,{borderColor: Colors.borderLight}]}
                    onPress={()=>{
                        this.setState({
                            modalVisible: true,
                            selectId: rowData.id,
                            selectName: rowData.name,
                            inputString: remark
                        })
                    }}
                >
                    <Text style={{fontSize: 13, color: remarkColor}}>{remarkText}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderRowItem(rowData, index){
        const Colors = this.props.themeInfo.colors;
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
        const deviceIcon = this.props.themeInfo.isDark ? rowData.nightIcon : rowData.dayIcon
        let idIndex = this.state.selectedIds.indexOf(rowData.id)
        const selectImg = idIndex == -1 ? require('../../../images/unselect_green.png')
        : require('../../../images/select_green.png')

        let floorText = null
        if(rowData.floorText && rowData.roomName){
            floorText = <Text style={[styles.floorText,{color: Colors.themeTextLight}]}>{rowData.floorText+'  '+rowData.roomName}</Text>
        }
         
        return(
            <TouchableOpacity
                key={'second_mange_list_'+index} 
                activeOpacity={1}
                style={[styles.sliderItem, {backgroundColor:Colors.themeBg}]}
                onPress={()=>{
                    this.selectBtnClick(rowData)
                }}
            >
                <View style={styles.sliderTouch}>
                    <Image style={styles.icon} defaultSource={Images.default} source={{uri: deviceIcon}}/>
                    <View style={styles.titleWrapper}>
                        <Text style={[styles.name,{color:Colors.themeText}]}>{rowData.name}</Text>
                        {floorText}
                    </View>
                    <View style={{flex: 1}}/>
                    <Image style={styles.selectImg} source={selectImg}/>
                </View>
                <View style={[styles.splitLine,{backgroundColor:Colors.borderLight}]}/>
                {this.renderRemark(rowData, index)}
            </TouchableOpacity>
        )
    }

    _extraUniqueKey(item, index) {
		return "add_multi_index_" + index;
	}

    //获取总列表
    renderListView() {
        const Colors = this.props.themeInfo.colors;
        
        if(this.state.loading && !this.state.listData){
            return (
                <View style={{alignItems: 'center', marginTop: 100}}>
                    <Text style={{color: Colors.themeTextLight}}>加载中...</Text>
                </View>
            )
        }else if(!this.state.listData){
            return (
                <View style={{alignItems: 'center', marginTop: 100}}>
                    <Text style={{color: Colors.themeTextLight}}>网络错误，请稍后重试</Text>
                </View>
            )
        }else if(this.state.listData.length <= 0) {
            return (
                <View style={{alignItems: 'center', marginTop: 100}}>
                    <Text style={{color: Colors.themeTextLight}}>暂无可添加设备</Text>
                </View>
            )
        }

        return(
            <FlatList
                ref={e => this.flatlist = e}
                contentContainerStyle={{ paddingBottom: 50 }}
                scrollIndicatorInsets={{right: 1}}
                removeClippedSubviews={false}
                data={this.state.listData}
                keyExtractor={this._extraUniqueKey}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
                initialNumToRender={5}
                keyboardDismissMode={'on-drag'}
            />
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors;

        return (
            <View style={{flex: 1, backgroundColor: Colors.themeBaseBg}}>
                {this.renderSearchHeader()}
                {this.renderTips()}
                {this.renderListView()}
                {this.renderRemarkModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    scrollContent:{
        paddingBottom: 50,
    },
    sliderItem:{
		borderRadius:10,
		marginTop:15,
        marginHorizontal: 16
    },
    sliderTouch: {
        flexDirection:'row',
        alignItems:'center',
        paddingHorizontal: 16,
        height: 65
    },
    underLine:{
        position:'absolute',
        bottom: 0,
        width: 30,
        height: 3,
        backgroundColor: Colors.tabActiveColor,
    },
    icon:{
        width: 40, 
        height: 40, 
        
        resizeMode:'contain'
    },
    splitLine:{
        height:1,
        marginHorizontal: 16
    },
    bottomTouch:{
        paddingHorizontal:16,
        flexDirection:'row',
        alignItems:'center',
        height: 45
    },
    titleWrapper:{
        marginLeft:15,
    },
    name:{
        fontSize:16,
        fontWeight:'bold'
    },
    selectImg:{
        width:24,
        height:24,
        resizeMode:'contain',
        marginRight: 5
    },
    remarkTouch:{
        minWidth: 120,
        height: 26,
        borderRadius: 2,
        borderWidth: 1,
        marginLeft: 10,
        justifyContent: 'center',
        paddingHorizontal: 5
    },
    inputWraper:{
        backgroundColor:Colors.white,
        borderColor:Colors.borderLightGray,
        borderWidth:1,
        borderRadius:4,
    },
    input: {
        height:40,
        paddingHorizontal:10,
        fontSize:14,
        color:'#666'
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.shallowTrans
    },
    floorText:{
        marginTop:5, 
        fontSize: 13
    },
    remarkTitle:{
        fontSize: 13,
        marginBottom: 10,
        color: Colors.themeTextLightGray
    },
    topWrapper:{
		width:'100%',
		alignItems:'center',
        flexDirection:'row',
        paddingVertical: 5,
        paddingHorizontal: 10
	},
	searchWrapper:{
		backgroundColor:Colors.themBGLightGray,
		flex:1,
		borderRadius: 4,
		alignItems:'center',
		flexDirection:'row',
        paddingHorizontal: 10,
        paddingVertical: Platform.select({
            android: 0,
            ios: 10
        })
	},
    backIcon:{
        width:12,
        height:20,
        resizeMode:'contain'
    },
    searchIcon:{
        width:15,
        height:15,
        resizeMode:'contain'
    },
    tipsWrapper:{
        paddingHorizontal:16,
        paddingVertical:5,
        borderBottomWidth: 1
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(AddMultiFunctionDevice)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	TextInput,
	DeviceEventEmitter,
	ImageBackground,
	Switch
} from 'react-native';

import { Colors } from '../../../common/Constants';
import HeaderLeftView from '../../../common/CustomComponent/HeaderLeftView';
import { StatusBarHeight } from '../../../util/ScreenUtil';

class ControllerSetting_Producer extends Component {

	static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeftView navigation={navigation} text={'生产商'}/>,
			headerRight: <View/>
		}
	};

	constructor(props) {
		super(props);
		const {getParam} = this.props.navigation
		this.producerName = getParam('producerName')
		this.name = getParam('name')
		this.state = {
			
		}
	}

	componentDidMount() {

	}

	componentWillUnmount(){

	}

	getControllerDetail(){
		return(
			<View style={{width:'100%',flex:1}}>
				
				<View style={{marginTop:50,paddingHorizontal:16}}>
					<Text style={{fontSize:18,color:Colors.themeTextBlack,fontWeight:'bold'}}>{this.name}</Text>
					<Text style={{marginTop:33,color:Colors.themeBGInactive,fontSize:15}}>生产商：{this.producerName}</Text>
				</View>
			</View>
		)
	}

	render() {
		return (
			<ScrollView style={styles.container}>
				{this.getControllerDetail()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.white,
		flex:1
	},
	inputWrapper:{
		width: '100%',
		height: 60,
		backgroundColor:Colors.white,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:15,
		//阴影四连
		shadowOffset: { width: 0, height: 2 },
		shadowColor: '#070F26',
		shadowOpacity: 0.15,
		shadowRadius: 5,
		elevation: 5
	},
	bottomWrapper:{
		width: '100%',
		height: 165,
		backgroundColor:Colors.white,
		borderRadius:5,
		marginTop:15,
		padding:15,
		//阴影四连
		shadowOffset: { width: 0, height: 2 },
		shadowColor: '#070F26',
		shadowOpacity: 0.15,
		shadowRadius: 5,
		elevation: 5
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight:5
	},
	resetTouch:{
		justifyContent:'center',
		alignItems:'center',
		width:140,
		height:44,
		backgroundColor:Colors.tabActiveColor,
		borderRadius:4
	},
	navText:{
		color:Colors.tabActiveColor,
		fontSize:15
	},
});

export default ControllerSetting_Producer

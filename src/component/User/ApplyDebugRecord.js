import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Dimensions,
    Modal,
    SwipeableFlatList,
    FlatList
} from 'react-native';
import { Colors, NetUrls, NotificationKeys } from '../../common/Constants';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import { postJson } from "../../util/ApiRequest";
import {ListNoContent} from "../../common/CustomComponent/ListLoading";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import { connect } from 'react-redux';
import {ImagesLight, ImagesDark} from '../../common/Themes';

class ApplyDebugRecord extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'调试申请记录'}/>,
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const { getParam } = props.navigation
        this.userData = getParam('userData') || {}

        this.state = {
            recordData: null,
            loading: true
        };
    }

    componentDidMount() {
        this.requsetRecord()
    }

    async requsetRecord(){
        try {
            const { pop } = this.props.navigation;
            let data = await postJson({
                url: NetUrls.debugRecordList,
                params: {
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.setState({
                    recordData: data.result || []
                })
            } else {
                ToastManager.show(data.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误');
        }
    }

    getItem(icon,title,detail){
        const Colors = this.props.themeInfo.colors;
        return(
            <View style={{flexDirection: 'row',paddingVertical:5,alignItems:'flex-start'}}>
                <View style={{flexDirection: 'row',alignItems:'center',width:80}}>
                    <Image style={{width:14,height:14,resizeMode:'contain'}} source={icon}/>
                    <Text style={{marginLeft: 5,fontSize:14,color:Colors.themeTextLight,lineHeight:18,}}>{title}</Text>
                </View>
                <Text style={{marginTop:1,marginLeft: 10,flex:1,fontSize:14,color:Colors.themeText,lineHeight:18,lineHeight:18}}>{detail}</Text>
            </View>
        )
    }

    // 和后台约定好的
    getStatusText(status){
        switch (status) {
            case 10:
                return '待处理';
                
            case 20:
                return '待委派';
                    
            case 30:
                return '待完成';      
                  
            case 40:
                return '已完成';
                
            case 50:
                return '已关闭';
                
            default:
                return '待处理';
        }
    }

    renderRowItem(rowData, index) {
        const Colors = this.props.themeInfo.colors;

        let statusText = this.getStatusText(rowData.status)
        let statusTextColor = (rowData.status == 40 || rowData.status == 50) ? Colors.themeText: Colors.red
        let serviceDate = rowData.serviceDateString
        //serviceDate = DateUtil(serviceDate)
        let serviceAddress = rowData.serviceAddress
        let remark = rowData.memo || '无'

		return (
			<View style={{paddingHorizontal: 16,marginTop:15}}>
                <View style={[styles.wrapper,{backgroundColor: Colors.themeBg}]}>
                    <Text style={{color:statusTextColor,fontSize:18,fontWeight:'bold',marginBottom: 10}}>{statusText}</Text>
                    {this.getItem(require('../../images/user_manage/date.png'),'服务日期',serviceDate)}
                    {this.getItem(require('../../images/user_manage/address.png'),'服务地址',serviceAddress)}
                    {this.getItem(require('../../images/user_manage/remark.png'), '备注',remark)}
                </View>              
            </View>
		)
	}

    _extraUniqueKey(item, index) {
		return "debug_record_index_" + index;
	}

    //房间列表
	getRecordList(){
		let recordData = this.state.recordData

		//正在加载
		if (this.state.loading && !recordData) {
			return (
				<View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
					<Text style={{ color: Colors.themeTextLightGray }}>正在加载...</Text>
				</View>
			)
		}
		// 网络错误
		if (!recordData) {
			return (
				<View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
					<TouchableOpacity
						onPress={() => {
							this.requsetRecord()
						}}
					>
						<Text style={{ color: Colors.tabActiveColor, fontSize: 15 }}>网络错误,刷新重试</Text>
					</TouchableOpacity>
				</View>
			)
		}
		// 暂无记录
		if (recordData && recordData.length <= 0) {
            const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
			return (
                <ListNoContent 
                    img={Images.noRecord}
                    text={'暂无申请记录'}
                />
			)
		}

		// 正常列表
		if (recordData.length > 0) {
			return (
				<View style={{flex:1}}>
					<FlatList
                        scrollIndicatorInsets={{right: 1}}
						contentContainerStyle={{paddingBottom: 50}}
						data={recordData}
						keyExtractor={this._extraUniqueKey}
						renderItem={({ item, index }) => this.renderRowItem(item, index)}
						initialNumToRender={10}
					/>
				</View>
			)
		}
	}
    
    render() {
        const Colors = this.props.themeInfo.colors;
        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.getRecordList()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    wrapper:{
        width: '100%',
        borderRadius: 5,
        padding: 16
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ApplyDebugRecord)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	SectionList
} from 'react-native';
import { connect } from 'react-redux';
import { Colors,NetUrls } from '../../common/Constants';
import ToastManager from "../../common/CustomComponent/ToastManager";
import { postJson } from '../../util/ApiRequest';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
 
class RoomPermission extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'房间权限'}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text: '全选', onPress:()=>{
                    navigation.getParam('selectAllBtnClick')()
                }},
                {text: '保存', onPress:()=>{
                    navigation.getParam('saveBtnClick')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground/>
    })

	constructor(props) {
		super(props);
		const { getParam,setParams } = this.props.navigation
		
        this.memberId = getParam('memberId')
        this.phoneNum = getParam('phoneNum')
        this.memberType = getParam('memberType')
        this.isEnabled = getParam('isEnabled')

		this.state = {
			roomListDatas: null,
            activeRoomIds: [],
            allRoomIds: null
        }
        
        setParams({
            saveBtnClick: ()=>{ this.requestSavePermission() },
            selectAllBtnClick: ()=>{this.selectAllBtnClick()}
        })
	}

	componentDidMount() {
		this.requestList()
	}

	componentWillUnmount() {
		
	}

    selectAllBtnClick(){
        let allRoomIds = this.state.allRoomIds
        let activeRoomIds = this.state.activeRoomIds

        if(!allRoomIds){
            ToastManager.show('数据加载中，请稍后')
            return
        }
        if(allRoomIds.length == 0){
            return
        }
        if(allRoomIds.length == activeRoomIds.length){
            this.setState({
                activeRoomIds: []
            })
        }else{
            this.setState({
                activeRoomIds: JSON.parse(JSON.stringify(this.state.allRoomIds))
            })
        }
    }

	//获取房屋列表
	async requestList() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.roomList,
				params: {
					accountId: this.memberId,
                    type: 1
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                let dataList = data.result || []
                let sectionListData = []
                let activeRoomIds = []
                let allRoomIds = []

                for (const floorData of dataList) {
                    let item = {}
                    item.data = floorData.listRoom
                    item.name = floorData.floorText

                    if(floorData.listRoom && floorData.listRoom.length>0){
                        sectionListData.push(item)
                        for (const room of floorData.listRoom) {
                            allRoomIds.push(room.id)
                            if(room.member){
                                activeRoomIds.push(room.id)
                            }
                        }
                    }
                }
                this.setState({
                    roomListDatas: sectionListData,
                    activeRoomIds: activeRoomIds,
                    allRoomIds: allRoomIds
                })
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	// 保存 设备 权限
	async requestSavePermission() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.createAndUpdate,
				params: {
                    id: this.memberId,
                    phone: this.phoneNum,
                    type: this.memberType,
					rooms: this.state.activeRoomIds.toString(),
                    isEnabled : this.isEnabled
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                const {pop} = this.props.navigation
                ToastManager.show('设置成功')
				pop()
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }
    
    renderSectionHeader(section){
        const Colors = this.props.themeInfo.colors;

        return(
            <View style={[styles.sectionHeaderWrapper,{backgroundColor: Colors.themeBaseBg}]}>
                <Text style={{color: Colors.themeText, fontSize: 16, fontWeight:'bold'}}>{section.name}</Text>
                <Text style={{color: Colors.themeTextLight, fontSize: 12}}>  ({section.data.length}间房间)</Text>
                <View style={{flex: 1}}/>
                <TouchableOpacity activeOpacity={0.7} style={styles.sectionTouch} onPress={()=>{
                    let sectionSelectedIds = []
                    let sectionAllIds = []
                    for (const device of section.data) {
                        sectionAllIds.push(device.id)
                        if(this.state.activeRoomIds.includes(device.id)){
                            this.state.activeRoomIds.remove(device.id)
                            sectionSelectedIds.push(device.id)
                        }
                    }
                    if(sectionSelectedIds.length == 0 || (sectionSelectedIds.length != sectionAllIds.length)){
                        this.state.activeRoomIds = this.state.activeRoomIds.concat(sectionAllIds)
                    }
                    this.setState({
                        activeRoomIds: this.state.activeRoomIds
                    })
                }}>
                    <Text style={{fontSize: 15, color: Colors.themeText}}>全选</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderRowItem(rowData, index, section){
        const icon = this.props.themeInfo.isDark ? rowData.nightImg : rowData.dayImg
        const Colors = this.props.themeInfo.colors;
        const tintColor = this.props.themeInfo.isDark ? {tintColor:Colors.themeText} : {}
        
		const idIndex = this.state.activeRoomIds.indexOf(rowData.id)
        const selectView = idIndex >= 0 ? <Image style={styles.selectIcon} source={require('../../images/select.png')}/>
        : <View style={styles.selectedView} />

        return(
            <View style={{backgroundColor: Colors.themeBg}}>
                <TouchableOpacity activeOpacity={0.7} style={styles.item} onPress={()=>{
                    // 三级Item
					if(idIndex == -1){
						this.state.activeRoomIds.push(rowData.id)
					}else{
						this.state.activeRoomIds.remove(rowData.id)
					}
					this.setState({
						activeRoomIds: this.state.activeRoomIds
					})
                }}>
                    <Image source={{uri: icon}} style={[styles.icon,tintColor]}/>
                    <Text style={[styles.itemTitle,{color: Colors.themeText}]}>{rowData.name}</Text>
                    {selectView}
                </TouchableOpacity>
                <View style={[styles.split,{backgroundColor: Colors.split}]} />
            </View>
        )
    }

    getMainListView(){
        const Colors = this.props.themeInfo.colors;
        
        if(!this.state.roomListDatas){
            return null
        }
        
		if(this.state.roomListDatas.length<=0){
			return (
                <View style={styles.noContent}>
                    <Text style={{color: Colors.themeTextLight, fontSize: 15}}>暂无设备</Text>
                </View>
            )
		}

        return(
			<SectionList
				style={{flex: 1}}
				contentContainerStyle={{ paddingBottom: 50}}
				stickySectionHeadersEnabled={true}
  				renderItem={({ item, index, section }) => this.renderRowItem(item, index, section)}
  				renderSectionHeader={({ section }) => this.renderSectionHeader(section)}
				sections={this.state.roomListDatas}
				scrollIndicatorInsets={{right: 1}}
				initialNumToRender={5}
				keyExtractor={(item, index) => 'rooms_'+index}
			/>
		)
    }

	render() {
        const Colors = this.props.themeInfo.colors;

		return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getMainListView()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	selectedView:{ 
		width: 18, 
		height: 18, 
		borderColor: Colors.borderLightGray, 
		borderWidth: 1, 
		borderRadius: 9 
	},
	itemTitle:{
		fontSize: 15,
		flex: 1 
    },
    icon:{
        width:30,
        height: 30,
        resizeMode:'contain',
        marginLeft: 16,
        marginRight: 15
    },
    item: {
		height: 54,
		width: '100%',
		flexDirection: 'row',
		alignItems: 'center',
        paddingHorizontal: 32
    },
    sectionHeaderWrapper:{
        height: 55, 
        width:'100%', 
        paddingLeft:16, 
        alignItems:'center', 
        flexDirection:'row'
    },
    sectionTouch:{
        height:'100%', 
        paddingHorizontal: 20, 
        justifyContent:'center', 
        alignItems:'center'
    },
    selectIcon:{
        width:18,
        height:18,
        resizeMode: 'contain'
    },
    split:{
        width: '100%', 
        height: 1, 
    },
    noContent:{
        justifyContent:'center',
        alignItems:'center',
        marginTop: '40%'
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(RoomPermission)

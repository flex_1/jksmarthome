/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ImageBackground,
    TextInput,
    Button, 
    ScrollView, 
    Switch,
    DeviceEventEmitter
} from 'react-native';
import {Colors, NotificationKeys, NetUrls} from '../../common/Constants';
import {postJson} from '../../util/ApiRequest';
import {StatusBarHeight, NavigationBarHeight} from '../../util/ScreenUtil';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import ActionSheet from "react-native-actionsheet";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import { connect } from 'react-redux';
import DateUtil from '../../util/DateUtil';

let codeTime = 120; //倒计时初始值
const dismissKeyboard = require('dismissKeyboard');

class AddMember extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'添加成员'}/>,
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props)
        const {getParam} = this.props.navigation
        this.callBack = getParam('callBack')

        this.state = {
            memberType: 2,
            phone: '',
            code: '',
            remark: '',
            timerCount: codeTime,
            timerTitle: '获取验证码',
            isHouseHolder: getParam('isHouseHolder'),
            startTime: DateUtil(Date.parse(new Date()),'yyyy-MM-dd'),
            endTime: null,
            isEnabled: true
        };
    }

    componentWillUnmount() {
        this.interval && clearInterval(this.interval);
    }

    async createAndUpdate() {
        dismissKeyboard()
        if (!this.checkPhone()) return
        if (!this.state.phone) {
            ToastManager.show('请输入手机号')
            return
        }
        if (!this.state.code) {
            ToastManager.show('请输入验证码')
            return
        }
        SpinnerManager.show()
        try {
            const {pop} = this.props.navigation;
            let res = await postJson({
                url: NetUrls.createAndUpdate,
                useToken: true,
                params: {
                    phone: this.state.phone,
                    type: this.state.memberType,
                    code: this.state.code,
                    memberRemark: this.state.remark,
                    startTime: this.state.startTime,
                    endTime: this.state.endTime,
                    isEnabled: this.state.isEnabled
                }
            });
            SpinnerManager.close()
            console.log(res);
            if (res.code == 0) {
                //回调刷新
                this.callBack && this.callBack()
                pop()
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(e));
        }
    }

    async getMsg() {
        dismissKeyboard()
        if (!this.checkPhone()) return
        if (!this.state.phone) {
            ToastManager.show('请输入手机号')
            return
        }
        SpinnerManager.show()
        try {
            const {pop} = this.props.navigation;
            let res = await postJson({
                url: NetUrls.getMsg,
                useToken: false,
                params: {
                    mobile: this.state.phone,
                    type: 2,//1.找回密码 2-邀请成员 3-过户
                }
            });
            SpinnerManager.close()
            console.log(res);
            if (res.code == 0) {
                this.onTimes();
                ToastManager.show("获取验证码成功");
            } else {
                ToastManager.show(res.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(e));
        }
    }

    //号码校验
    checkPhone() {
        var phone = this.state.phone;
        if (!phone) {
            ToastManager.show('请输入手机号码')
            return false;
        }
        if (!(/^1[3456789]\d{9}$/.test(phone))) {
            ToastManager.show('请输入正确的号码')
            return false;
        }
        return true
    }

    /**
     * 倒计时
     */
    onTimes() {
        this.interval = setInterval(() => {
            var timer = this.state.timerCount - 1;
            if (timer === 0) {
                this.interval && clearInterval(this.interval);
                this.setState({timerCount: codeTime, timerTitle: '重新获取'})
            } else {
                this.setState({timerCount: timer,})
            }
        }, 1000)
    }

    getActionSheet() {
        return (
            <ActionSheet
                ref={e => this.actionSheet = e}
                options={['普通成员', '管理员', '取消']}
                cancelButtonIndex={2}
                onPress={(index) => {
                    if (index == 0) {
                        this.setState({
                            memberType: 2//"普通成员"
                        })
                    } else if (index == 1) {
                        this.setState({
                            memberType: 1//"管理员"
                        })
                    }
                }}
            />
        )
    }

    // 去空格
    trim(str) {
        if(!str){
            return ''
        }
        str = str.replace(/\s/g,"");
        str = str.replace(/-/g,"");
        str = str.replace(/——/g,"");
	    return str
    }

    // 成员手机号
    renderPhoneItem() {
        const Colors = this.props.themeInfo.colors;
        return (
            <View style={[styles.customItemTouch,{backgroundColor: Colors.themeBg}]}>
                <Text style={[styles.itemTitle,{color: Colors.themeText}]}>成员手机号</Text>
                <TextInput
                    value={this.state.phone}
                    style={[styles.phoneText,{color: Colors.themeText}]}
                    placeholder="请输入成员手机号"
                    placeholderTextColor={Colors.themeInactive}
                    keyboardType={'numeric'}
                    maxLength={20}
                    secureTextEntry={false}
                    onChangeText={(text) => {
                        this.setState({
                            phone: this.trim(text)
                        })
                    }}
                />
            </View>
        )
    }

    // 成员备注
    renderRemarkItem() {
        const Colors = this.props.themeInfo.colors;
        return (
            <View style={[styles.customItemTouch,{backgroundColor: Colors.themeBg}]}>
                <Text style={[styles.itemTitle,{color: Colors.themeText}]}>备注名</Text>
                <TextInput
                    style={[styles.phoneText,{color: Colors.themeText}]}
                    placeholder="请输入备注名(可选)"
                    placeholderTextColor={Colors.themeInactive}
                    keyboardType={'default'}
                    maxLength={8}
                    onChangeText={(text) => {
                        this.state.remark = text
                    }}
                />
            </View>
        )
    }

    // 验证码
    renderInputCode() {
        const Colors = this.props.themeInfo.colors;
        return (
            <View style={[styles.customItemTouch,{backgroundColor: Colors.themeBg}]}>
                <TextInput
                    style={[styles.input,{color: Colors.themeText}]}
                    placeholder="请输入验证码"
                    placeholderTextColor={Colors.themeInactive}
                    clearButtonMode={'while-editing'}
                    maxLength={6}
                    keyboardType={'numeric'}
                    onChangeText={(text) => {
                        this.state.code = text
                    }}
                />
                <TouchableOpacity style={{marginLeft: 40}} onPress={() => {
                    if (this.state.timerCount == codeTime) {
                        this.getMsg()
                    }
                }}>
                    <Text style={{color: Colors.themeButton, fontSize: 15}}>
                        {this.state.timerCount == codeTime ? this.state.timerTitle : this.state.timerCount}
                    </Text>
                 </TouchableOpacity>
            </View>
        )
    }

    // 成员关系
    getCustomItem() {
        const Colors = this.props.themeInfo.colors;

        return (
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={[styles.customItemTouch,{backgroundColor: Colors.themeBg}]} 
                onPress={() => {
                    if(this.state.isHouseHolder){
                        this.actionSheet.show()
                    }else{
                        ToastManager.show('管理员身份只能添加普通成员')
                    }
                }}
            >
                <Text style={{flex: 1,fontSize: 16, color: Colors.themeText}}>{'成员类型'}</Text>
                <View style={{flexDirection: 'row', alignItems: "center"}}>
                    <Text style={{fontSize: 15, marginRight: 10, color: Colors.themeText}}>{this.state.memberType == 1 ? "管理员" : "普通成员"}</Text>
                    <Image style={{width: 8, height: 12, marginRight: 5}} source={require('../../images/enterLight.png')}/>
                </View>
            </TouchableOpacity>
        )
    }

    // 生效时间
    renderEffectTime(){
        const Colors = this.props.themeInfo.colors;
        const {navigate} = this.props.navigation;
        let extraText = '长期有效'
        let start = this.state.startTime
        let end = this.state.endTime

        if(!this.state.isEnabled){
            extraText = '已停用'
        }else if(start && end){
            extraText = start + ' 至 ' + end
        }
        
        return (
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={[styles.customItemTouch,{backgroundColor: Colors.themeBg}]} 
                onPress={() => {
                    navigate('EffectTime',{
                        beginDate: start, 
                        endDate: end,
                        isEnabled: this.state.isEnabled,
                        isShowEffectSwitch: true,
                        callBack: (beginDate, endDate, isEnabled)=>{
                            this.setState({
                                startTime: beginDate,
                                endTime: endDate,
                                isEnabled: isEnabled
                            })
                        }
                    })
                }}
            >
                <Text style={{flex: 1,fontSize: 15, color: Colors.themeText}}>生效日期</Text>
                <View style={{flexDirection: 'row', alignItems: "center"}}>
                    <Text style={{fontSize: 15, marginRight: 10, color: Colors.themeText}}>{extraText}</Text>
                    <Image style={{width: 8, height: 12, marginRight: 5}} source={require('../../images/enterLight.png')}/>
                </View>
            </TouchableOpacity>
        )
    }

    onAddMemberBtn() {
        return (
            <TouchableOpacity activeOpacity={0.7} style={styles.addMemberStyle} onPress={() => {
                this.createAndUpdate()
            }}>
                <Text style={{fontSize: 15, color: Colors.white}}>添加成员</Text>
            </TouchableOpacity>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors;

        return (
            <ScrollView 
                keyboardShouldPersistTaps="handled"
                style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}
            >
                {this.renderPhoneItem()}
                <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                {this.renderRemarkItem()}
                <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                {this.renderInputCode()}
                <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                {this.getCustomItem()}
                <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                {this.renderEffectTime()}
                {this.onAddMemberBtn()}
                {this.getActionSheet()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 10
    },
    customItemTouch: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 50,
        paddingHorizontal: 20
    },
    addMemberStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        marginHorizontal: 16,
        marginTop: 40,
        borderRadius: 4,
        backgroundColor: Colors.tabActiveColor,
    },
    input: {
        flex: 1,
        fontSize: 16
    },
    phoneText:{
        fontSize: 16,
        textAlign: 'right',
        flex: 1
    },
    itemTitle:{
        fontSize: 16
    },
    line:{
        height:1,
        width: '100%'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(AddMember)

import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    Alert,
    DeviceEventEmitter
} from 'react-native';
import { connect } from 'react-redux';
import { NotificationKeys,NetUrls } from '../../common/Constants';
import ToastManager from "../../common/CustomComponent/ToastManager";
import LocalTokenHandler from '../../util/LocalTokenHandler';
import { postJson } from "../../util/ApiRequest";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';

class CloseAccount extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'注销账号'}/>,
        headerBackground: <HeaderBackground/>
    })

	constructor(props) {
		super(props);
        const { getParam } = props.navigation
        
		this.state = {
			isChecked: false
		};
	}

	componentDidMount() {
		
	}
	
	async closeAccount() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.cancellation,
                params: {}
            });
            SpinnerManager.close()
            if (data.code == 0) {
                ToastManager.show('账号已注销')
                LocalTokenHandler.remove(() => {
                    DeviceEventEmitter.emit(NotificationKeys.kLogoutNotification)
                })
            } else {
                Alert.alert(
                    '提示',
                    data.msg,
                    [
                        { text: '知道了', onPress: () => { }, style: 'cancel' },
                    ]
                )
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误');
        }
    }

    renderTitle(){
        const Colors = this.props.themeInfo.colors
        
        return <Text style={[styles.title,{color: Colors.themeText}]}>重要提醒</Text>
    }

    renderContent(){
        const Colors = this.props.themeInfo.colors
        const isDark = this.props.themeInfo.isDark
        const checkIcon = this.state.isChecked ? require('../../images/check_blue.png')
        : require('../../images/uncheck_blue.png')
        const bgColor = this.state.isChecked ? Colors.red : (isDark ? Colors.tabInactive : Colors.lightGray)
        const textColor = this.state.isChecked ? Colors.white : (isDark ? Colors.themeBg : Colors.themeTextLight)

        return(
            <View style={styles.content}>
                <Text style={[styles.contentText,{color: Colors.themeText}]}>
                    {'      '}注销小萨管家账号是不可恢复的操作，操作之前，请确认与小萨管家相关的所有服务均已进行妥善处理。
                </Text>
                <Text style={[styles.contentText,{color: Colors.themeText}]}>
                    {'      '}请谨记: 注销小萨账号，你将无法再使用本账号或找回你添加或绑定的任何内容或信息（即使你使用相同的手机号码再次注册并使用小萨管家）。
                </Text>
                <View style={styles.confirmWrapper}>
                    <TouchableOpacity style={styles.checkTouch} onPress={()=>{
                        this.setState({isChecked: !this.state.isChecked})
                    }}>
                        <Image style={styles.checkIcon} source={checkIcon}/>
                    </TouchableOpacity>
                    <Text style={[styles.checkText,{color:Colors.themeText}]}>我已阅读并知晓。</Text>
                </View>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.closeBtn,{backgroundColor: bgColor}]}
                    onPress={()=>{
                        if(!this.state.isChecked){
                            return
                        }
                        this.closeAccount()
                    }}
                >
                    <Text style={[styles.closeText,{color: textColor}]}>注销账号</Text>
                </TouchableOpacity>
            </View>
        )
    }

	render() {
		const Colors = this.props.themeInfo.colors

		return (
            <View style={[styles.container,{backgroundColor: Colors.themeBg}]}>
                <ScrollView>
				    {this.renderTitle()}
                    {this.renderContent()}
			    </ScrollView>
            </View>
			
		)
	}
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
	title:{
        textAlign: 'center',
        fontSize:18,
        fontWeight: 'bold',
        marginTop: 20
    },
    content:{
        paddingHorizontal: 20,
        marginTop: 20
    },
    contentText:{
        fontSize: 16,
        lineHeight: 24
    },
    confirmWrapper:{
        marginTop: 30,
        flexDirection: 'row',
        alignItems: 'center'
    },
    checkTouch:{
        padding: 10,
    },
    checkIcon:{
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
    checkText:{
        fontSize:15
    },
    closeBtn:{
        marginHorizontal: 10,
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        backgroundColor: 'red'
    },
    closeText:{
        fontSize: 16,
        fontWeight: '500'
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(CloseAccount)

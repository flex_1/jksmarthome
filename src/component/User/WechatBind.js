/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import { NetUrls,Colors } from '../../common/Constants';
import {StatusBarHeight,BottomSafeMargin} from '../../util/ScreenUtil';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import ToastManager from "../../common/CustomComponent/ToastManager";
import { postJson } from "../../util/ApiRequest";
import { connect } from 'react-redux';
import ActionSheet from 'react-native-actionsheet';
import savePicture from '../../util/DownloadImage';

const screenW =  Dimensions.get('window').width;

class WechatBind extends Component {
    
    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={'微信通知'}/>,
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation

        this.userData = getParam('userData') || {}
        
        this.state={
            qrUrl: null,
            remark: ''
        }
        
    }

    componentDidMount() {
        this.requetQRCode()
    }

    componentWillUnmount() {
        
    }

    async requetQRCode() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.getWechatServiceAccount,
                params: {

                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.setState({
                    qrUrl: data.result?.url,
                    remark: data.result?.remark
                })
            } else {
                ToastManager.show(data.msg);
            }
        } catch (e) {
            this.setState({isRefreshing: false})
            ToastManager.show('网络错误');
        }
    }

    savePic(uri){
        savePicture(this.state.qrUrl)
    }

    renderQRCode(){
        if(!this.state.qrUrl){
            return null
        }

        return(
            <View style={{marginTop: 40,justifyContent:'center',alignItems: 'center'}}>
                <TouchableOpacity activeOpacity={1} onLongPress={()=>{
                    this.actionSheet.show()
                }}>
                    <Image style={{width: screenW*0.6, height:screenW*0.6}} source={{uri: this.state.qrUrl}}/>
                </TouchableOpacity>
            </View>
        )
    }

    renderRemark(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={styles.qrcodeWrapper}>
                <Text style={[styles.remarkText,{color: Colors.themeTextLight}]}>{this.state.remark}</Text>
            </View>
        )
    }

    getDeviceActionSheet() {
		return (
			<ActionSheet
				ref={e => this.actionSheet = e}
				options={['保存图片','取消']}
				cancelButtonIndex={1}
				onPress={(index) => {
					if (index == 0) {
						this.savePic()
					}
				}}
			/>
		)
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]} >
                {this.renderQRCode()}
                {this.renderRemark()}
                {this.getDeviceActionSheet()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    qrcodeWrapper:{
        marginTop: 30,
        justifyContent:'center',
        alignItems: 'center',
        paddingHorizontal: screenW*0.2
    },
    remarkText:{
        fontSize:14, 
        fontWeight: '400', 
        textAlign: 'center',
        lineHeight:24
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(WechatBind)

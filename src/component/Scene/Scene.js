/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    NativeModules,
    NativeEventEmitter,
    Alert
} from 'react-native';
import {Colors, NotificationKeys, NetUrls} from '../../common/Constants';
import {postJson} from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import SceneList from './SceneList';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import {ColorsLight, ColorsDark} from '../../common/Themes';
import PopUp from '../../common/CustomComponent/PopUp';
import FloorScrollTab from '../../common/CustomComponent/FloorScrollTab';
import {HSTips} from '../../common/Strings';
 
const JKRNUtils = NativeModules.JKRNUtils;
const ShortCutManager = Platform.select({
    ios: new NativeEventEmitter(JKRNUtils),
    android: null
})

class Scene extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'场景'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {icon:require('../../images/add.png'),onPress:()=>{
                        navigation.getParam('addScene')()
                        // navigation.getParam('dismissPop')()
                        // navigation.navigate('AddScene', {title: '添加场景'})
                    }},
                    {icon:require('../../images/more.png'),onPress:()=>{
                        navigation.getParam('addBtnClick')()
                    }}
                ]}/>
            ),
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
	}

    constructor(props) {
        super(props);
        const { getParam,setParams } = props.navigation;

        this.shortCutsSceneId = getParam('shortCutsSceneId')
        this.shortCutsSceneName = getParam('shortCutsSceneName')

        this.state = {
            currentFloor: 0,
            currentIndex: 0,
            floors: [{ floor: 0 }],
            numColumns: getParam('numColumns') || 1,
            popVisible: false
        }

        setParams({
            addScene: this.addSceneClick.bind(this),
            addBtnClick: ()=>{ this.setState({popVisible: true}) },
            dismissPop: ()=>{ this.setState({popVisible: false}) }
        })
    }

    addSceneClick(){
        this.setState({popVisible: false})
        if(this.props.userInfo.memberType == 2){
            Alert.alert(
                '提示',
                HSTips.limitsCreatScene,
                [
                    {text: '知道了', onPress: () => {}, style: 'cancel'}
                ]
            )
        }else{
            this.props.navigation.navigate('AddScene', {title: '添加场景'})
        }
    }

    componentDidMount() {
        this.getSceneFloor()
        this.sceneFloorNoti = DeviceEventEmitter.addListener(NotificationKeys.kSceneFloorNotification,()=>{
            this.setState({
                currentFloor: 0,
                currentIndex: 0
            },()=>{
                this.getSceneFloor()
                this.scene_list.getScene()
            })
        })
        // iOS Siri快捷指令
        if(this.shortCutsSceneId){
            this.excuteScene(this.shortCutsSceneId, this.shortCutsSceneName)
        }
        if(Platform.OS == 'ios'){
            this.shortcutsNoti = ShortCutManager.addListener(NotificationKeys.kRNShortCutsNotification,(data)=>{
                if(data.type == 'excuteScene'){
                    this.excuteScene(data.sceneId, data.sceneName)
                }
            })
        }
    }

    componentWillUnmount() {
        this.sceneFloorNoti.remove()
        if(Platform.OS == 'ios'){
            this.shortcutsNoti.remove()
        }
    }

    // 获取所有场景的楼层
    async getSceneFloor(){
		try {
			let data = await postJson({
				url: NetUrls.sceneFloor,
				params: {
				}
			});
			if (data.code == 0) {
                let floors = [{ floor: 0, floorText: '整屋' }]
                if(data.result && data.result.length > 0){
                    floors = floors.concat(data.result)
                }
                this.setState({
                    floors: floors,
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }
    
    //更改场景状态
    async excuteScene(id, name){
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.updateSceneStatus,
				params: {
					id: id,
					status: 1,
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				ToastManager.show(name + ' 执行成功')
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误');
		}
    }

    //头部向导
	getHeaderView() {
        let switchThumbImg = this.state.numColumns == 1 ? require('../../images/homeBG/list.png') : 
            require('../../images/homeBG/table.png')
        const Colors = this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}

		return (
			<View style={[styles.sectionHead,{backgroundColor: Colors.themeBg,borderBottomColor: Colors.themeBaseBg}]}>
				<FloorScrollTab
                    floors = {this.state.floors}
                    currentFloor = {this.state.currentFloor}
                    currentIndex = {this.state.currentIndex}
                    isDark = {this.props.themeInfo.isDark}
                    onClick = {(floor, index)=>{
                        this.setState({
                            currentFloor: floor,
                            currentIndex: index
                        },()=>{
                            this.scene_list.reloadList()
                        })
                    }}
                />
                <View style={styles.split}/>
                <TouchableOpacity style={styles.thumbnailTouch} onPress={()=>{
                    let target = this.state.numColumns%2 + 1
                    this.setState({ numColumns: target })
                }}>
                    <Image style={[styles.thumbImg,tintColor]} source={switchThumbImg}/>
                 </TouchableOpacity>
			</View>
		)
    }
    
    renderModal(){
        if(!this.state.popVisible){
            return null
        }
        const {navigate} = this.props.navigation
        const Colors = this.props.themeInfo.isDark ? ColorsLight : ColorsDark


        let searchBtn = {icon: require('../../images/search.png'),title: '搜索场景',onPress:()=>{
            navigate('SearchPage',{searchType: 'Scene'})
            this.setState({popVisible: false})
        }}
        let logBtn = {icon: require('../../images/log.png'),title: '场景日志',onPress:()=>{
            navigate('Logs',{title:'场景日志',type:'scene'})
            this.setState({popVisible: false})
        }}
        let recommandBtn = {icon: require('../../images/recommend.png'),title: '场景推荐',onPress:()=>{
            navigate('SceneRecommend')
            this.setState({popVisible: false})
        }}

        let buttonArrays = [searchBtn,logBtn,recommandBtn]
        if(this.props.userInfo.memberType == 2){
            buttonArrays = [searchBtn,logBtn,]
        }

        return (
            <PopUp
                frame={{top: 0, right: 10, width: 140}}
                triangleRight={15}
                dismiss={()=>{ this.setState({popVisible: false}) }}
                bgColor={Colors.themeBg}
                textColor={Colors.themeText}
                borderColor={Colors.split}
                buttonArrays={buttonArrays}
            />
        )   
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.getHeaderView()}
                <SceneList 
                    ref={e => this.scene_list = e} 
                    navigation={this.props.navigation} 
                    floor={this.state.currentFloor}
                    numColumns={this.state.numColumns}
                    isDark={this.props.themeInfo.isDark}
                    isJuniorUser={this.props.userInfo.memberType == 2}
                />
                {this.renderModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingHorizontal: 16,
        marginLeft: 5
    },
    navImg: {
        width: 20,
        height: 20
    },
    sectionHead: {
		width: '100%',
		height: 40,
        flexDirection:'row',
        alignItems:'center',
        borderBottomWidth: 1
	},
	headScroll: {
		height: '100%',
	},
	headScrollContainer: {
		alignItems: 'center',
		paddingLeft: 10,
		paddingRight:20
	},
	floorTouch: {
		alignItems: 'center',
		paddingHorizontal: 10,
		marginRight: 10
	},
	dot: {
		width: 4,
		height: 4,
		backgroundColor: Colors.themeTextBlue,
		borderRadius: 2,
		marginTop: 5
	},
	floorText: {
		fontSize: 15,
		fontWeight: 'bold',
		color: Colors.themeBGInactive
    },
    searchNavImg:{
		width: 20,
		height: 20,
		resizeMode:'contain'
    },
    recommendNavImg:{
        width: 22,
        height: 22,
        resizeMode: 'contain'
    },
    logIcon:{
        width: 17,
        height: 19,
        resizeMode: 'contain'
    },
    thumbnailTouch:{  
        height:'100%',
        alignItems: 'center',
        paddingHorizontal: 16,
        justifyContent:'center'
    },
    split:{
        marginLeft: 10,
        width: 1,
        height:'50%',
        backgroundColor:Colors.themBGLightGray
    },
    thumbImg:{
        width:16,
        height:16,
        resizeMode:'contain'
    },
    modalWrapper:{
        position: 'absolute',
        width:'100%',
        height:'100%',
        top: 0,
        left: 0
    },
    expandWrapper:{
        paddingVertical: 5,
        width: 120,
        borderRadius: 4
    },
    expandItem:{
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    expandText:{
        marginLeft: 10,
        fontSize: 14
    },
    expandImg:{
        width: 20,
        height: 20,
        resizeMode: 'contain'  
    },
    arrow:{
        alignSelf: 'flex-end',
        marginRight: 15,
        width:0,
        height:0,
        borderStyle:'solid',
        borderWidth: 5,
        borderTopColor: 'transparent', //下箭头颜色
        borderLeftColor: 'transparent', //右箭头颜色
        borderRightColor: 'transparent' //左箭头颜色
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
        userInfo: state.userInfo
    })
)(Scene)

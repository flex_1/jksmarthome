/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    FlatList,
    Alert,
    RefreshControl,
    SwipeableFlatList,
    Dimensions,
    ImageBackground,
    Modal
} from 'react-native';
import {Colors, NotificationKeys, NetUrls} from '../../common/Constants';
import {StatusBarHeight} from '../../util/ScreenUtil';
import {postJson} from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';

const screenW = Dimensions.get('screen').width;
const bgWidth = screenW
const bgHeight = bgWidth * (1050*1.0/750)

//侧滑最大距离
const maxSwipeDistance = 90
//侧滑按钮个数
const countSwiper = 1

class SceneRecommendDetail extends Component {

    static navigationOptions = {
		header: null,
        headerBackground: <HeaderBackground/>
	}

    constructor(props) {
        super(props);
        const { getParam } = props.navigation;
        this.id = getParam('id')
        
        this.state = {
            roomId: 0,
            roomName: '全屋',
            name: '',
            bigImg: '',
            bigImgText: '',
            icon: '',
            attributes: null,
            isShowNavigtionBg: false,

            modalVisible: false,
            pickerData: null,
            pickerTitle: '',
            filterData: null,
            dictData:[],
            selectData: [],
            deviceId: null,
            currentRowIndex: null
        }
    }

    componentDidMount() {
        this.getRecommendDetail()
    }

    componentWillUnmount() {
        
    }

    // 获取推荐场景 列表
    async getRecommendDetail(){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.rmdSceneDetail,
				params: {
                    id: this.id
				}
            });
            SpinnerManager.close()
			if (data.code == 0) {
                let res = data.result || {}
                this.setState({
                    name: res.name,
                    bigImg: res.bigImg,
                    bigImgText: res.bigImgText,
                    icon: res.icon,
                    attributes: res.attributes
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    //添加设备
    async addDevice(ids){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.rmdSceneAddDevice,
				params: {
                    deviceIds: ids
				}
            });
            SpinnerManager.close()
			if (data.code == 0) {
                if(data.result && data.result.length>0){
                    this.setState({
                        attributes: this.state.attributes.concat(data.result)
                    })
                }
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    // 创建场景
    async createScene(){
        if(this.state.attributes.length <= 0){
            ToastManager.show('至少选择一个设备')
            return
        }
        const {pop} = this.props.navigation
        SpinnerManager.show()
        let devices = []
        for (const item of this.state.attributes) {
            if(item.devices){
                let device = item.devices
                let deviceStatus = {}
                if(device.extraStatus){
                    // 设备支持多个模式
                    deviceStatus =  device.extraStatus
                }else{
                    // 设备只支持开关
                    let status = device.optionValue + ',' + device.delay
                    deviceStatus[device.executeKey] = status
                }
                let param = {deviceId: device.deviceId,deviceStatus:JSON.stringify(deviceStatus)}
                devices.push(param)
            }
        }
		try {
			let data = await postJson({
				url: NetUrls.addRmdSceneDetail,
				params: {
                    name: this.state.name,
                    devices: devices
				}
            });
            SpinnerManager.close()
			if (data.code == 0) {
                DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
                pop(2)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    // 获取字典列表
    async getDictList(item) {
        try {
            let data = await postJson({
                url: NetUrls.getDelayDictionary,
                params: {
                    type: 4
                }
            });
            if (data.code == 0) {
                if (data.result && data.result.delayList
                    && data.result.delayList.length > 0) {
                    let list = data.result.delayList;
                    this.setState({
                        result:data.result,
                        deviceId:item.deviceId,
                    })
                    this.getConditionList(item.deviceId,item.name,item.icon,list);
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    async getConditionList(deviceId,deviceName,deviceIcon,list){
        SpinnerManager.show();
        try {
            let data = await postJson({
                url: NetUrls.getExecuteByDeviceId,
                params: {
                    deviceId:deviceId
                }
            });
            SpinnerManager.close();
            if (data.code == 0) {
                if (data.result && data.result.length>0) {
                    this.setState({
                        childData:data.result,
                        childValues:data.result[0].value
                    })
                    // this.popSelectPicker(data.result,deviceName,list);
                    this.dealAndJump(data.result,deviceId,deviceName,deviceIcon,list);
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close();
            ToastManager.show('网络错误')
        }
    }

    // 弹出选择框
    popSelectPicker(data,deviceName,list){
        let arr1 = []
        let arr2 = []
        let dlist = data[0].value;
        for (const item of list) {
            arr1.push(item.text)
        }
        for (const child of dlist) {
            arr2.push(child.text)
        }
        this.setState({
            pickerData: [arr1,arr2],
            modalVisible: true,
            pickerTitle: deviceName,
        })
    }

    //响应或者跳转
    dealAndJump(data,deviceId,deviceName,deviceIcon,list){
        const {navigate} = this.props.navigation

        if(data.length <= 1){
            //灯泡 电视  仅打开关闭
            this.popSelectPicker(data,deviceName,list)
        }else{
            //空调  新风 多项 跳转新页面
            let currentDevice = this.state.attributes[this.state.currentRowIndex].devices
            let executeJson = null

            if(currentDevice.extraStatus){
                // 设备支持多个模式
                executeJson =  JSON.stringify(currentDevice.extraStatus) 
            }else{
                // 设备 只有开关
                let status = currentDevice.optionValue + ',' + currentDevice.delay
                let deviceStatus = {}
                deviceStatus[currentDevice.executeKey] = status
                executeJson = JSON.stringify(deviceStatus)
            }
            navigate('SenceDeviceList',{
                title:deviceName,
                deviceId:deviceId,
                deviceIcon:deviceIcon,
                type:4,
                list:data,
                timeList:list,
                selectItem: {executeJson: executeJson},
                rmdSceneCallBack: (data) => {
                    let extraStatus = {}
                    for (const sta of data) {
                        let status = sta.position[0] + ',' + sta.position[1]
                        extraStatus[sta.parttern] = status
                        if(sta.index == 0){
                            currentDevice.delay = sta.position[1]
                            currentDevice.delayString = sta.data[0]
                            currentDevice.optionValue = sta.position[1]
                            currentDevice.optionText = sta.data[1]
                        }
                    }
                    currentDevice.extraStatus = extraStatus
                    this.state.attributes[this.state.currentRowIndex].devices = currentDevice
                    this.setState({
                        ...this.state
                    })
                }}
            )
        }
    }

    getModal() {
        if(!this.state.pickerData){
            return null
        }
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        window.DoubulePicker.init({
            pickerData: this.state.pickerData,
            pickerConfirmBtnText: '确定',
            pickerCancelBtnText: '取消',
            pickerTitleText: this.state.pickerTitle,
            pickerCancelBtnColor: [38, 80, 215, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: this.state.selectData,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false,
                    selectData: data
                })
                this.pickerConfirm(data,index)
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })
            }
        });
        window.DoubulePicker.show()
    }

    //确认按钮
    pickerConfirm(selectData,selectIndex){
        let selectValue = this.state.childValues[selectIndex[1]].id
        let delay = this.state.result.delayList[selectIndex[0]].id
        let itemData = this.state.attributes[this.state.currentRowIndex]

        itemData.devices.delay = delay
        itemData.devices.delayString = selectData[0]
        itemData.devices.optionValue = selectValue
        itemData.devices.optionText = selectData[1]

        this.state.attributes[this.state.currentRowIndex] = itemData
        this.setState({
            ...this.state
        })
    }

    getTopNavigation(){
        const {getParam,pop} = this.props.navigation
        let naviBg = Colors.transparency
        let backColor = Colors.white
        if(this.state.isShowNavigtionBg){
            naviBg = Colors.white
            backColor = Colors.black
        }
        return(
            <View style={[styles.fixTop,{backgroundColor: naviBg}]}>
                <View style={styles.status}/>
                <View style={styles.navi}>
                    <TouchableOpacity style={styles.naviTouch} onPress={()=>{
                        pop()
                    }}>
                        <Image style={{ width: 10, height: 18 }} source={require('../../images/back_white.png')}/>
                        <Text style={[styles.title,{color: backColor}]}>{getParam('title')}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    getSceneBgView(){
        const {getParam,pop} = this.props.navigation
        let bigImg = this.state.bigImg ? {uri: this.state.bigImg} : require('../../images/sceneIcon/sceneBg.jpg')
        return(
            <View>
                <ImageBackground 
                    style={styles.topBg} 
                    source={bigImg}
                >
                    <View style={[styles.navi,{marginTop: StatusBarHeight}]}>
                        <TouchableOpacity style={styles.naviTouch} onPress={()=>{
                            pop()
                        }}>
                            <Image style={{ width: 10, height: 18 }} source={require('../../images/back_white.png')}/>
                            <Text style={[styles.title,{color: Colors.white}]}>{getParam('title')}</Text>
                        </TouchableOpacity>
                        <View style={{flex:1}}/>
                    </View>
                    <Text style={styles.tipsTitle}>{this.state.name}启动</Text>
                    <Text style={styles.tipsContent}>{this.state.bigImgText}</Text>
                </ImageBackground>
            </View>
        )
    }

    //关闭侧滑栏
	closeSwiper() {
        this.swiperFlatlist?.setState({ openRowKey: null })
    }

    // 获取侧滑设置
    getQuickActions(rowData, index){
        return (
			<View style={styles.quickAContent}>
				<TouchableOpacity activeOpacity={0.7} onPress={() => {
					this.closeSwiper()
					Alert.alert(
						'提示',
						'确认删除?',
						[
							{ text: '取消', onPress: () => { }, style: 'cancel' },
							{ text: '删除', onPress: () => {
                                this.state.attributes.splice(index,1)
                                this.setState({
                                    ...this.state
                                })
                            }},
						]
					)
				}}>
					<View style={[styles.quick, { backgroundColor: Colors.themBgRed }]}>
						<Text style={{ color: Colors.white, fontSize: 14 }}>删除</Text>
					</View>
				</TouchableOpacity>
			</View>
		)
    }

    renderRowItem(rowData, index){
        let deviceName = rowData.attributeTypeText
        let floorText = null
        let statusText = null
        if(rowData.devices){
            let {floor,roomName,name,delayString,optionText} = rowData.devices
            deviceName = name
            if(floor && roomName){
                floorText = <Text style={{marginTop:10,fontSize:12,color:Colors.themeTextLightGray}}>{floor}F {roomName}</Text>
            }
            statusText = delayString + ' ' + optionText
        }
        return(
            <View style={styles.itemWrapper}>
                <TouchableOpacity activeOpacity={1} style={styles.itemTouch} onPress={()=>{
                    this.closeSwiper()
                    if(rowData.devices){
                        this.getDictList(rowData.devices)
                        this.setState({
                            currentRowIndex: index,
                            selectData: [rowData.devices.delayString,rowData.devices.optionText]
                        })
                    }
                }}>
                    <Image style={{width:50,height:50,resizeMode:'contain'}} source={{uri: rowData.attributeTypeIcon}}/>
                    <View style={{marginLeft: 10,flex:1}}>
                        <Text style={{fontSize:14,color:Colors.themeTextBlack}}>{deviceName}</Text>
                        {floorText}
                    </View>
                    <Text style={{fontSize:15,color:Colors.themeTextBlack}}>{statusText}</Text>
                </TouchableOpacity>
                <View style={styles.split}/>
            </View>
        )
    }

    _extraUniqueKey(item, index) {
		return "device_index_" + index;
    }

    getDeviceListView(){
        let attributes = this.state.attributes
        if(!attributes || attributes.length<=0){
            return null
        }
        return (
            <SwipeableFlatList
                ref={e => this.swiperFlatlist = e}
                showsVerticalScrollIndicator={false}
				contentContainerStyle={{ paddingBottom: 10 }}
				removeClippedSubviews={false}
				data={attributes}
				keyExtractor={this._extraUniqueKey}
				renderItem={({ item, index }) => this.renderRowItem(item, index)}
				renderQuickActions={({ item, index }) => this.getQuickActions(item, index)}
				maxSwipeDistance={maxSwipeDistance}
				bounceFirstRowOnMount={false}
				initialNumToRender={5}
			/>
        )
    }

    getDeviceContent(){
        return(
            <View style={styles.contentWrapper}>
                <View style={styles.topSection}>
                    {this.state.icon ? <Image style={styles.topIcon} source={{uri: this.state.icon}}/> : null}
                    <Text style={styles.topTitle}>{this.state.name}</Text>
                    <View style={{flex:1}}/>
                    <Text style={styles.topRoom}>{this.state.roomName}</Text>
                </View>
                {this.getDeviceListView()}
            </View>
        )
    }

    getBottomView(){
        const {navigate} = this.props.navigation
        return(
            <View>
                <TouchableOpacity style={styles.addBtnTouch} onPress={()=>{
                    this.closeSwiper()
                    navigate('SelectDevice',{
                        type: 4,
                        rmdSceneCallBack:(data)=>{
                            let ids = data.toString()
                            this.addDevice(ids)
                        }
                    })
                }}>
                    <Text style={{color:Colors.tabActiveColor,fontSize:15}}>+添加设备</Text>
                </TouchableOpacity>
                <View style={styles.createBtnWrapper}>
                    <TouchableOpacity activeOpacity={0.8} style={styles.createBtnTouch} onPress={()=>{
                        this.closeSwiper()
                        this.createScene()
                    }}>
                        <Text style={{color:Colors.white,fontSize:15}}>创建场景</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    
    render() {
        return (
            <View style={styles.container}>
                <ScrollView
                    contentContainerStyle={{paddingBottom: 60}}
                    scrollIndicatorInsets={{right: 1}}
                >
                    {this.getSceneBgView()}
                    {this.getDeviceContent()}
                    {this.getBottomView()}
                </ScrollView>
                {this.getModal()}
            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white
    },
    fixTop:{
        position: 'absolute',
        zIndex: 1000,
        top: 0,
        left: 0,
        width: '100%'
    },
    status:{
        height: StatusBarHeight
    },
    navi: {
        height:50,
        flexDirection: 'row',
        alignItems:'center'
    },
    naviTouch:{
        flexDirection: 'row',
        alignItems:'center',
        height: '100%',
        paddingLeft: 16
    },
    topBg:{
        // paddingTop: StatusBarHeight + 50,
        width:bgWidth,
        height:bgHeight,
        backgroundColor:Colors.themBGLightGray
    },
    title:{
        fontSize: 24,
        marginLeft: 10,
        fontWeight: 'bold'
    },
    tipsTitle:{
        fontSize: 18,
        marginTop: 20,
        color: Colors.white,
        fontWeight: 'bold',
        marginLeft: 16
    },
    tipsContent:{
        fontSize: 15,
        color: Colors.white,
        marginTop: 10,
        marginLeft: 16,
        marginRight: 16,
        lineHeight: 24
    },
    contentWrapper:{
        backgroundColor: Colors.white,
        marginTop: -20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    topSection:{
        height: 50,
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: Colors.borderLightGray
    },
    topIcon:{
        width:18,
        height:18,
        resizeMode:'contain',
        marginLeft: 16
    },
    topTitle:{
        marginLeft:15,
        fontSize: 18,
        color: Colors.themeTextBlack,
        fontWeight: 'bold'
    },
    topRoom:{
        fontSize: 16,
        color: Colors.themeTextBlack,
        fontWeight: 'bold',
        marginRight:10
    },
    topRightTouch:{
        paddingRight:16,
        paddingLeft:10,
        flexDirection: 'row',
        alignItems: 'center',
    },
    //侧滑菜单的样式
	quickAContent: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginTop: 0,
		overflow: 'hidden',
	},
	quick: {
		backgroundColor: Colors.white,
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
        width: maxSwipeDistance / countSwiper,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
    },
    itemWrapper:{
        width:'100%',
        height:60,
        backgroundColor:Colors.white,
        paddingHorizontal: 16
    },
    split:{
        width:'100%',
        height:1,
        backgroundColor: Colors.splitLightGray
    },
    itemTouch:{
        flex:1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    addBtnTouch:{
        marginTop: 10,
        height:30,
        justifyContent:'center',
        alignItems:'center'
    },
    createBtnWrapper:{
        marginTop:20,
        paddingHorizontal:16
    },
    createBtnTouch:{
        justifyContent:'center',
        alignItems:'center',
        height:40,
        borderRadius:20,
        backgroundColor: Colors.tabActiveColor
    }, 
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    }
});

export default SceneRecommendDetail

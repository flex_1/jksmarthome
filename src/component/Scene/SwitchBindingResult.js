/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	DeviceEventEmitter,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Alert,
	TextInput,
	SafeAreaView,
	Dimensions
} from 'react-native';
import { Colors, NotificationKeys, NetUrls } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeftView from '../../common/CustomComponent/HeaderLeftView';
import {BottomSafeMargin,StatusBarHeight} from '../../util/ScreenUtil';
import { connect } from 'react-redux';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';

class SwitchBindingResult extends Component {
    
    static navigationOptions = ({ navigation }) => {
		return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'配对状态'}/>,
            headerBackground: <HeaderBackground/>
		}
	}

	constructor(props) {
		super(props);
        const { getParam } = props.navigation;
        
        this.suspendingText = getParam('suspendingText')
		this.sceneId = getParam('sceneId')
        this.deviceParams = getParam('deviceParams')
        this.bindSeriaNO = getParam('bindSeriaNO')
        
        this.state={
            statusCode : getParam('statusCode')  // 0-失败   1-成功  3-等待确认(强制覆盖)
        }
	}
    
    //强制绑定
    async requestBinding() {
        let params = {}
        if (this.sceneId) {
            params.sceneId = this.sceneId
        } else if (this.deviceParams && this.deviceParams.deviceId) {
            params = this.deviceParams
        } else {
            ToastManager.show('参数有误，请稍后重试')
            return
        }
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.switchBiding,
                params: {
                    ...params,
                    mustPrefix: this.bindSeriaNO,
                    must: 0  // must:0 代表强制绑定
                }
            });
            SpinnerManager.close()
            if (data && data.code == 0) {
                DeviceEventEmitter.emit(NotificationKeys.kSwitchSuccessBidingNotification)
                this.setState({ statusCode: 1 })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show(JSON.stringify(error))
        }
    }

	getMainView(){
        const Colors = this.props.themeInfo.colors
        let img = null
        let tips = null

        if(this.state.statusCode == 3){
            // 等待确认
            tips = this.suspendingText
        }else if(this.state.statusCode == 1){
            // 成功
            tips = '开关绑定成功'
            img = <Image style={{width:90,height:90,resizeMode:'contain'}} source={require('../../images/sceneIcon/sucess.png')}/>
        }else{
            // 失败
            tips = '开关绑定失败'
            img = <Image style={{width:90,height:90,resizeMode:'contain'}} source={require('../../images/sceneIcon/failed.png')}/>
        }

		return(
			<View style={{width:'100%',height:'100%',alignItems:'center',marginTop:'40%'}}>
				{img}
				<Text style={{marginTop:20,color:Colors.themeText,fontSize:18}}>{tips}</Text>
			</View>
		)
	}

	getButtons(){
        const {replace,pop} = this.props.navigation
        const Colors = this.props.themeInfo.colors

		if(this.state.statusCode == 1){
            // 成功
			return(
				<View style={styles.bottomWrapper}>
					<TouchableOpacity activeOpacity={0.7} style={styles.backTouch} onPress={()=>{
						pop()	
					}}>
						<Text style={{fontSize:18,color:Colors.white}}>返回</Text>
					</TouchableOpacity>
				</View>
			)
		}else if(this.state.statusCode == 0){
            // 失败
			return(
				<View style={styles.bottomWrapper}>
					<TouchableOpacity activeOpacity={0.7} style={styles.backTouch} onPress={()=>{
						let deviceParams = null
						if(this.deviceParams && this.deviceParams.deviceId){
							deviceParams = JSON.parse(JSON.stringify(this.deviceParams))
						}
						replace('SwitchBinding',{sceneId: this.sceneId, deviceParams: deviceParams})
					}}>
						<Text style={{fontSize:18,color:Colors.white}}>再试一次</Text>
					</TouchableOpacity>
					<TouchableOpacity activeOpacity={0.7} style={styles.cancel} onPress={()=>{
						pop()	
					}}>
						<Text style={{fontSize:18,color:Colors.themeTextLight}}>返回</Text>
					</TouchableOpacity>
				</View>
			)
		}else if(this.state.statusCode == 3){
            // 等待确认(强制覆盖)
            return(
                <View style={styles.bottomWrapper}>
					<TouchableOpacity activeOpacity={0.7} style={styles.backTouch} onPress={()=>{
						this.requestBinding()
					}}>
						<Text style={{fontSize:18,color:Colors.white}}>确定</Text>
					</TouchableOpacity>
					<TouchableOpacity activeOpacity={0.7} style={styles.cancel} onPress={()=>{
						pop()	
					}}>
						<Text style={{fontSize:18,color:Colors.themeTextLight}}>取消</Text>
					</TouchableOpacity>
				</View>
            )
        }else{
            return null
        }
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getMainView()}
				{this.getButtons()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.white,
		flex: 1,
	},
	scrollContainStyle: {
		paddingBottom: BottomSafeMargin + 60
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight: 5
	},
	navText: {
		color: Colors.themeBG,
		fontSize: 15
	},
	backTouch:{
		width:'80%',
		height:44,
		borderRadius:4,
		justifyContent:'center',
        alignItems:'center',
        backgroundColor:Colors.switchBindBg
	},
	cancel:{
		width:'80%',
		height:44,
		justifyContent:'center',
		alignItems:'center',
		marginTop:10
	},
	bottomWrapper:{
		width:'100%',
		alignItems:'center',
		position: 'absolute',
		bottom: BottomSafeMargin + 80
	}
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SwitchBindingResult)

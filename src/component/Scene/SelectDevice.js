/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    FlatList,
    Alert,
    Modal,
    TextInput,
    Keyboard,
    Dimensions,
    UIManager,
    findNodeHandle
} from 'react-native';
import { Colors, NetUrls, NotificationKeys, NetParams } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import { StatusBarHeight } from '../../util/ScreenUtil';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import ToastManager from "../../common/CustomComponent/ToastManager";
import {ListNoContent,ListLoading,ListError } from "../../common/CustomComponent/ListLoading";
import { connect } from 'react-redux';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';

class SelectDevice extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'添加设备'}/>,
			headerRight: (
				<HeaderRight buttonArrays={[
                    {text:'保存',onPress:()=>{
                        navigation.getParam('saveBtnClick')()
                    }}
                ]}/>
			),
            headerBackground: <HeaderBackground hiddenBorder={true}/>
		}
	}

    constructor(props) {
        super(props);
        const { getParam,setParams } = props.navigation;
        this.roomId = getParam('roomId')

        this.floor = getParam('floor')
        this.floorText = getParam('floorText')

        this.roomName = getParam('roomName')
        this.callBack = getParam('callBack')
        this.rmdSceneCallBack = getParam('rmdSceneCallBack')
        this.sceneId = getParam('sceneId')

        // 判断是否是为场景添加设备
        this.selectedIds = getParam('selectedIds') || []

        this.state = {
            deviceList: null,
            selectedIds: [],
            searchText: '',

            showFilterModalType: 0,
            allFloorData: [],
            allRoomData: [],
            selectedRoom: this.getDefaultRoom(),
            selectedFloor: getParam('floor') || 0,
            selectedFloorText: getParam('floorText') || '全部楼层',

            sectionStatus: {}
        }

        setParams({
            saveBtnClick: this.confirm.bind(this)
        })
    }

    getDefaultRoom(){
        if(this.roomId && this.roomName){
            return { roomId: this.roomId, roomName: this.roomName }
        }else{
            return { roomId: 0, roomName: '全部房间' }
        }
    }

    componentDidMount() {
        this.requestDeviceList()
        this.getFilterList()
    }

    componentWillUnmount() {

    }

    confirm() {
        if (this.state.selectedIds.length <= 0) {
            ToastManager.show('请选择您要添加的设备');
            return
        }
        if (this.rmdSceneCallBack) {
            const { pop } = this.props.navigation
            this.rmdSceneCallBack(this.state.selectedIds)
            pop()
        } else {
            let deviceIds = this.state.selectedIds.join(",");
            this.addDeviceList(deviceIds, this.sceneId);
        }
    }

    // 获取楼层信息
    async getFilterList() {
        SpinnerManager.show();
        try {
            let data = await postJson({
                url: NetUrls.floorAndRoomList,
                params: {
                    type: 4,  //3:场景  4:设备
                }
            });
            SpinnerManager.close();
            if (data.code == 0) {
                if (data.result && data.result.length > 0) {
                    this.setState({
                        allFloorData: data.result
                    })
                    for (const roomData of data.result) {
                        if(roomData.floor == this.floor){
                            this.setState({
                                allRoomData: roomData.room
                            })
                            break;
                        }
                    }
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close();
            ToastManager.show('网络错误')
        }
    }

    // 获取设备列表
    async requestDeviceList() {
        let params = {}
        if (this.state.searchText) {
            params.deviceName = this.state.searchText
        }
        // if(this.roomId){
        //     params.roomid = this.roomId
        // }
        if(this.state.selectedRoom && this.state.selectedRoom.roomId){
            params.roomid = this.state.selectedRoom.roomId
        }
        if(this.state.selectedFloor){
            params.floor = this.state.selectedFloor
        }
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.sceneSelectDeviceList,
                params: params
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.setState({
                    deviceList: data.result || []
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

    // 设备 开关
	async changeDevieceStatus(id,targetStatus,call){
		try {
			let data = await postJson({
				url: NetUrls.deviceUpdate,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: {
					status: targetStatus,
					id: id
				}
			});
			if (data.code == 0) {
				call && call()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    // 批量添加设备
    async addDeviceList(deviceIds, senceId) {
        const { pop } = this.props.navigation
        SpinnerManager.show();
        try {
            let data = await postJson({
                url: NetUrls.addDeviceList,
                params: {
                    deviceIds: deviceIds,//设备ids
                    sceneId: senceId,//场景id
                }
            });
            SpinnerManager.close();
            if (data.code == 0) {
                ToastManager.show("选择成功")
                this.callBack && this.callBack()
                pop()
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close();
            ToastManager.show('网络错误')
        }
    }

    // 搜索框
    getTextInputHeader() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.topWrapper,{backgroundColor: Colors.themeBg}]}>    
                <View style={styles.searchWrapper}>
                    <Image style={{ width: 15, height: 15, resizeMode: 'contain' }} source={require('../../images/search_gray.png')} />
                    <TextInput
                        style={{ flex: 1, paddingLeft: 10 }}
                        value={this.state.searchText}
                        placeholder={'请输入关键字'}
                        placeholderTextColor={Colors.themeBGInactive}
                        returnKeyType={'search'}
                        clearButtonMode={'while-editing'}
                        maxLength={16}
                        onChangeText={(text) => {
                            this.setState({ searchText: text })
                        }}
                        onBlur={()=>{
                            this.requestDeviceList()
                        }}
                        onSubmitEditing={() => {
                            
                        }}
                    />
                </View>  
            </View>
        )
    }

    // 筛选栏
    getFilterHeader(){
        const Colors = this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}

        let floorText = this.state.selectedFloorText
        let floorArrow = this.state.showFilterModalType == 1 ? require('../../images/sceneIcon/arrow_up.png') 
            : require('../../images/sceneIcon/arrow_down.png')
        let roomText = this.state.selectedRoom.roomName || '全部房间'
        let roomArrow = this.state.showFilterModalType == 2 ? require('../../images/sceneIcon/arrow_up.png') 
            : require('../../images/sceneIcon/arrow_down.png')
        let numeber = this.state.selectedIds.length

        return(
            <View style={[styles.filterHeader,{backgroundColor: Colors.themeBg,borderBottomColor: Colors.themeBaseBg}]}>
                <TouchableOpacity style={styles.filterBtn} onPress={()=>{
                    Keyboard.dismiss()
                    this.setState({ showFilterModalType: 1 })
                }}>
                    <Text style={{fontSize:15,color:Colors.themeText}}>{floorText}</Text>
                    <Image style={[styles.arrowDown,tintColor]} source={floorArrow}/>
                </TouchableOpacity>
                <TouchableOpacity style={styles.filterBtn} onPress={()=>{
                    Keyboard.dismiss()
                    this.setState({ showFilterModalType: 2 })
                }}>
                    <Text style={{fontSize:15,color:Colors.themeText}}>{roomText}</Text>
                    <Image style={[styles.arrowDown,tintColor]} source={roomArrow}/>
                </TouchableOpacity>
                <View style={{flex:1}}/>
                <Text style={{fontSize:12,color: Colors.themeTextLight}}>
                    已选<Text style={{color:Colors.themeButton}}>{numeber}</Text>个设备
                </Text>
            </View>
        )
    }

    // 筛选框
    getFilterModal(){
        const Colors = this.props.themeInfo.colors

        if(!this.state.showFilterModalType){
            return null
        }
        let list = null
        if(this.state.showFilterModalType == 1){
            // 显示楼层信息
            list = this.state.allFloorData.map((value,index)=>{
                let bg = value.floor == this.state.selectedFloor ? Colors.themeButton : Colors.themeTextLight
                return(
                    <TouchableOpacity 
                        key={'floor_index'+index} 
                        style={styles.filterBtnTouch} 
                        onPress={()=>{
                            this.setState({
                                selectedFloor: value.floor,
                                selectedFloorText: value.floorText,
                                allRoomData: value.room,
                                selectedRoom: { roomId: 0, roomName: '全部房间' },
                                showFilterModalType: 0
                            },()=>{
                                this.requestDeviceList()
                            })
                        }}
                    >
                        <Text style={{color:bg,fontSize:14}}>{value.floorText}</Text>
                    </TouchableOpacity>
                )
            })
        }else if(this.state.showFilterModalType == 2){
            // 显示房屋信息
            list = this.state.allRoomData.map((value,index)=>{
                let bg = value.roomId == this.state.selectedRoom.roomId ? Colors.themeButton : Colors.themeTextLight
                return(
                    <TouchableOpacity 
                        key={'room_index'+index} 
                        style={styles.filterBtnTouch} 
                        onPress={()=>{
                            this.setState({
                                selectedRoom: value,
                                showFilterModalType: 0
                            },()=>{
                                this.requestDeviceList()
                            })
                        }}
                    >
                        <Text style={{color:bg,fontSize:14}}>{value.roomName}</Text>
                    </TouchableOpacity>
                )
            })
        }

        return(
            <View style={styles.filterWrapper}>
                <View style={[styles.filterContent,{backgroundColor: Colors.themeBg}]}>
                    {list}
                </View>
                <TouchableOpacity style={{flex:1 ,width:'100%'}} onPress={()=>{
                    this.setState({showFilterModalType: 0})
                }}>
                </TouchableOpacity>
            </View>
        )
    }

    _renderItemList(item, index){
        const Colors = this.props.themeInfo.colors
        let itemData = item.devices
        if(!itemData || itemData.length<=0){
            return null
        }
        let list = itemData.map((val,i)=>{
            // 楼层信息
            let floorText = null
            if(val.floorText && val.roomName){
                floorText = <Text style={{marginTop:10,fontSize:12,color:Colors.themeTextLight}}>{val.floorText + ' ' + val.roomName}</Text>
            }
            // 有没有被选择
            let selectIcon = require('../../images/sceneIcon/dot_unselect.png')
            let tintColor = {}
            if(this.state.selectedIds.includes(val.deviceId)){
                selectIcon = require('../../images/sceneIcon/dot_select.png')
                tintColor = {tintColor: Colors.themeButton}
            }
            // 状态 信息
            let statusView = null
            if(val.type == 1){
                let img = val.value ? require('../../images/sceneIcon/device_on.png') 
                    : require('../../images/sceneIcon/device_off.png')
                
                statusView = (
                    <TouchableOpacity style={styles.switchIconTouch} onPress={()=>{
                        // 开关控制
                        let target = val.value ? 0:1
                        this.changeDevieceStatus(val.deviceId,target,()=>{
                            itemData[i]['value'] = target
                            this.state.deviceList[index]['devices'] = itemData
                            this.setState({
                                ...this.state
                            })
                            // this.requestDeviceList()
                        })
                    }}>
                        <Image style={styles.switchIcon} source={img}/>
                    </TouchableOpacity>
                )
            }else{
                statusView = val.statusText ? <Text style={styles.itemStatus}>{val.statusText}</Text> : null
            }
            // 设备图标
            let deviceIcon = this.props.themeInfo.isDark ? val.nightImg : val.dayImg
            return(
                <TouchableOpacity
                    key={index + 'device_item' + i}
                    activeOpacity={0.7} 
                    style={[styles.itemTouch,{backgroundColor:Colors.themeBg}]} 
                    onPress={()=>{
                        // Item 点击事件
                        if(this.state.selectedIds.includes(val.deviceId)){
                            this.state.selectedIds.remove(val.deviceId)
                        }else{
                            this.state.selectedIds.push(val.deviceId)
                        }
                        this.setState({selectedIds: this.state.selectedIds})
                    }}
                >
                    <Text style={{fontSize:16,color:Colors.themeText,fontWeight:'bold'}}>{val.deviceName}</Text>
                    {floorText}
                    {statusView}
                    <Image style={[styles.dotSelect,tintColor]} source={selectIcon}/>
                    <Image style={styles.itemImg} source={{uri: deviceIcon}}/>
                </TouchableOpacity>
            )
        })

        return list
    }

    renderRowItem(item, index){
        const Colors = this.props.themeInfo.colors
        let itemData = item.devices

        let sectionStatus = this.state.sectionStatus
        let floor = item.floor || 0
        let onColor = sectionStatus[floor+'on'] ? Colors.themeText : Colors.themeTextLight
        let offColor = sectionStatus[floor+'off'] ? Colors.themeText : Colors.themeTextLight
        let allColor = sectionStatus[floor+'all'] ? Colors.themeText : Colors.themeTextLight

        return(
            <View style={{paddingHorizontal: 16,marginBottom:15}}>
                <View style={{flexDirection:'row',alignItems:'center',paddingVertical:5}}>
                    <Text style={{fontSize:18,color:Colors.themeText,fontWeight:'bold'}}>{item.floorText}</Text>
                    <View style={{flex:1}}/>
                    <TouchableOpacity style={{paddingLeft:15,marginLeft:10,}} onPress={()=>{
                        // ON 按钮点击事件
                        if(sectionStatus[floor+'on']){
                            // 全消
                            sectionStatus[floor+'on'] = false
                            for (const device of itemData) {
                                if(this.state.selectedIds.includes(device.deviceId) && device.value ){
                                    this.state.selectedIds.remove(device.deviceId)
                                }
                            }
                        }else{
                            // 全选
                            sectionStatus[floor+'on'] = true
                            for (const device of itemData) {
                                if(!this.state.selectedIds.includes(device.deviceId) && device.value ){
                                    this.state.selectedIds.push(device.deviceId)
                                }
                            }
                        }
                        this.setState({...this.state})
                    }}>
                        <Text style={{fontSize:15,color:onColor}}>ON</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{paddingLeft:15,marginLeft:10}} onPress={()=>{
                        // OFF 按钮点击事件
                        if(sectionStatus[floor+'off']){
                            // 全消
                            sectionStatus[floor+'off'] = false
                            for (const device of itemData) {
                                if(this.state.selectedIds.includes(device.deviceId) && device.value==0){
                                    this.state.selectedIds.remove(device.deviceId)
                                }
                            }
                        }else{
                            // 全选
                            sectionStatus[floor+'off'] = true
                            for (const device of itemData) {
                                if(!this.state.selectedIds.includes(device.deviceId) && device.value==0 ){
                                    this.state.selectedIds.push(device.deviceId)
                                }
                            }
                        }
                        this.setState({...this.state})
                    }}>
                        <Text style={{fontSize:15,color:offColor}}>OFF</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.allSelect} onPress={()=>{
                        // 全选按钮点击事件
                        if(sectionStatus[floor+'all']){
                            // 全消
                            sectionStatus[floor+'all'] = false
                            for (const device of itemData) {
                                if(this.state.selectedIds.includes(device.deviceId)){
                                    this.state.selectedIds.remove(device.deviceId)
                                }
                            }
                        }else{
                            // 全选
                            sectionStatus[floor+'all'] = true
                            for (const device of itemData) {
                                if(!this.state.selectedIds.includes(device.deviceId)){
                                    this.state.selectedIds.push(device.deviceId)
                                }
                            }
                        }
                        this.setState({...this.state})
                    }}>
                        <Text style={{fontSize:15,color:allColor,marginLeft:5}}>全选</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row',flexWrap:'wrap',justifyContent:'space-between'}}>
                    {this._renderItemList(item, index)}
                </View>
            </View>
        )
    }

    getDeviceSectionList(){
        let data = this.state.deviceList

		if(!data){
            return <ListLoading />
		}
		
		return(
			<FlatList
				style={{paddingTop:10,flex: 1}}
                contentContainerStyle={{ paddingBottom: 50}}
  				renderItem={({ item, index }) => this.renderRowItem(item, index)}
  				data={data}
				scrollIndicatorInsets={{right: 1}}
                initialNumToRender={5}
                keyboardDismissMode={'on-drag'}
                keyExtractor={(item, index) => 'smart_log'+index}
                ListEmptyComponent={<ListNoContent text={'暂无设备'}/>}
			/>
		)
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.getTextInputHeader()}
                {this.getFilterHeader()}
                {/* {this.getHeaderSection()} */}
                {this.getDeviceSectionList()}
                {this.getFilterModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        flex: 1
    },
    backBtn: {
        paddingHorizontal: 16,
        paddingRight: 20,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    status: {
        width: '100%',
        height: StatusBarHeight
    },
    navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight: 5
    },
    navText: {
		color: Colors.tabActiveColor,
		fontSize: 15
	},
    topWrapper: {
        height: 50,
        width: '100%',
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 16
    },
    searchWrapper: {
        backgroundColor: Colors.themBGLightGray,
        flex: 1,
        borderRadius: 4,
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingVertical: Platform.select({
            android: 0,
            ios: 10
        })
    },
    cancelBtn: {
        paddingRight: 16,
        paddingLeft: 16,
        marginLeft: 5,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    floorText: {
        marginTop: 10,
        fontSize: 12,
        color: Colors.themeTextLightGray
    },
    filterHeader:{
        height:50,
        flexDirection:'row',
        alignItems:'center',
        paddingHorizontal: 10,
        borderBottomWidth: 1
    },
    filterWrapper:{
        backgroundColor:Colors.translucent,
        left: 0,
        width:'100%',
        height: '100%',
        position: 'absolute',
        top: 100
    },
    filterBtn:{
        paddingHorizontal: 10,
        height: '100%',
        justifyContent: 'center',
        alignItems:'center',
        marginRight: 10,
        flexDirection: 'row'
    },
    filterContent:{
        paddingVertical:20,
        paddingHorizontal:10,
        flexDirection:'row',
        flexWrap:'wrap'
    },
    filterBtnTouch:{
        paddingHorizontal: 12,
        paddingVertical:8,
        marginRight:10,
        marginTop: 5
    },
    arrowDown:{
        width:8,
        height:5,
        resizeMode:'contain',
        marginLeft:5
    },
    itemTouch:{
        width:'48%',
        height:100,
        marginTop:10,
        borderRadius: 4,
        padding: 10,
    },
    dotSelect:{
        position: 'absolute',
        width:15,
        height:15,
        top:10,
        right:15
    },
    itemImg:{
        position: 'absolute',
        width: 45,
        height: 45,
        resizeMode: 'contain',
        bottom: 10,
        right: 10,
    },
    itemStatus:{
        position: 'absolute',
        bottom: 10,
        left: 10,
        color: Colors.tabActiveColor,
        fontSize: 14
    },
    allSelect:{
        paddingLeft:10,
        marginLeft:10,
        flexDirection:'row',
        alignItems:'center'
    },
    switchIconTouch:{
        position: 'absolute',
        left:0,
        bottom:0,
        paddingVertical: 40,
        paddingRight: 40,
        paddingLeft: 10,
        paddingBottom: 10
    },
    switchIcon:{
        width:22,
        height:22,
        resizeMode:'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SelectDevice)

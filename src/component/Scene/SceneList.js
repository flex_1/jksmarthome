/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	DeviceEventEmitter,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	FlatList,
	Alert,
	RefreshControl,
	SwipeableFlatList,
	Dimensions,
	ImageBackground
} from 'react-native';
import { Colors, NotificationKeys, NetUrls, NetParams } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import {ListNoContent,FooterEnd,ListLoading,ListError,FooterLoading} from "../../common/CustomComponent/ListLoading";
import SceneListitem from "../CommonPage/ListItems/SceneListItem";
import {ImagesLight, ImagesDark} from '../../common/Themes';
import { BottomSafeMargin } from '../../util/ScreenUtil';

//侧滑最大距离
const maxSwipeDistance = 180
const maxSwipeDistanceShort = 80
//侧滑按钮个数
const countSwiper = 3

class SceneList extends Component {

	constructor(props) {
		super(props);
		this.state = {
			loading: true,
			isRefreshing: false,
			scenes: null,
            currentPage: 0,
            totalPage: 0
		}
        this.initialBreakFn()
	}

	componentDidMount() {
		this.getScene()
		this.sceneInfoNoti = DeviceEventEmitter.addListener(NotificationKeys.kSceneInfoNotification, () => {
			this.getScene()
		})
	}

	componentWillUnmount() {
		this.sceneInfoNoti.remove()
	}

    // 用于打断网络的函数
    initialBreakFn(){
        break_fn = null
        this.break_promise = new Promise(function(resolve,reject){
            break_fn = ()=>{
                reject({ 'code': NetParams.networkBreak })
            }
        })
    }

    // 用于切换列表
    reloadList(){
        break_fn && break_fn()
        this.initialBreakFn()

        this.closeSwiper()
        this.setState({
            loading: true,
            scenes: null
        },()=>{
            this.getScene()
        })
    }

	// 获取场景列表
	async getScene(params) {
		params = params || {}
		if(this.props.floor) {
			params.floor = this.props.floor
		}
		if(this.props.sceneName) {
			params.sceneName = this.props.sceneName
		}
		try {
			let data = await postJson({
				url: NetUrls.sceneList,
                break_promise: this.break_promise,
				params: {
					...params,
					roomId: this.props.roomId
				}
			});
			this.setState({
				loading: false,
                isRefreshing: false
			})
			if (data.code == 0) {
                let sceneList = []
                if(params.page && params.page > 1){
                    sceneList = this.state.scenes.concat(data.result.list)
                }else{
                    sceneList = data.result?.list || []
                }
				this.setState({
					scenes: sceneList,
                    currentPage: data.result?.curPage,
                    totalPage: data.result?.totalPageNum
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({ 
				isRefreshing: false,
				loading: false
			})
            if(error.code == NetParams.networkBreak){
                return
            }
			ToastManager.show('网络错误');
		}
	}

	// 删除场景
	async deleteScene(sceneId) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.delScene,
				params: {
					id: sceneId
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				// DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
				DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
				DeviceEventEmitter.emit(NotificationKeys.kSceneFloorNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误');
		}
	}

	// 场景置顶
	async setTopScene(sceneId,target) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.setTopScene,
				params: {
					id: sceneId,
					istop: target,
					type: 1
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				let text = target ? '置顶成功' : '取消置顶'
				ToastManager.show(text);
				DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误');
		}
    }
    
    // 复制场景
    async copyScene(sceneId){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.copyScene,
				params: {
					sceneId: sceneId,
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				ToastManager.show('场景复制成功');
				DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误');
		}
    }


	_extraUniqueKey(item, index) {
		return "roomInfo_index_" + index;
	}

	//关闭侧滑栏
	closeSwiper() {
		this.swiperFlatlist && this.swiperFlatlist.setState({ openRowKey: null })
	}

    //加载下一页
    onLoadNextPage() {
        if(this.state.loading){
            return
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return
        }
        this.getScene({page: this.state.currentPage+1})
    }

	// 渲染每一行
	renderRowItem(rowData, index) {
		return(
			<SceneListitem
                numColumns={this.props.numColumns}
				rowData={rowData} 
				index={index} 
				navigation={this.props.navigation}
                isJuniorUser={this.props.isJuniorUser}
				callBack={()=>{
					this.closeSwiper()
                }}
                collectClick={()=>{
                    DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
                }}
                isDark={this.props.isDark}
			/>
		)
    }

    //普通成员
    getActionsForJunior(rowData){
        const { navigate } = this.props.navigation

        return(
            <View style={styles.quickAContent}>
                <TouchableOpacity
                    style={styles.quick2}
                    activeOpacity={0.7} 
                    onPress={() => {
					    this.closeSwiper()
					    navigate('SceneSetting', { 
                            title: '场景设置',
                            sceneId: rowData.id, 
                            roomId: rowData.roomId 
                        })
				    }}
                >
					<Image style={{width:40,height:40}} source={require('../../images/deviceIcon/setting.png')}/>
				</TouchableOpacity>
            </View>
        )
    }
    
    //侧滑菜单渲染
	getQuickActions(rowData, index) {
        if(this.props.numColumns != 1){
            return null
        }
        if(this.props.isJuniorUser){
            return this.getActionsForJunior(rowData)
        }
        const { navigate } = this.props.navigation

		return (
			<View style={styles.quickAContent}>
                <TouchableOpacity
                    style={styles.quick}
                    activeOpacity={0.7} 
                    onPress={()=>{
					    this.copyScene(rowData.id)
					    this.closeSwiper()
                    }}
                >
					<Image style={{width:40,height:40}} source={require('../../images/deviceIcon/copy.png')}/>
				</TouchableOpacity>
                <TouchableOpacity
                    style={styles.quick}
                    activeOpacity={0.7} 
                    onPress={() => {
					    this.closeSwiper()
                        Alert.alert(
                            '提示',
                            '确认删除该场景?',
                            [
                                { text: '取消', onPress: () => { }, style: 'cancel' },
                                { text: '删除', onPress: () => { this.deleteScene(rowData.id) } },
                            ]
                        )
				    }}
                >
					<Image style={{width:40,height:40}} source={require('../../images/deviceIcon/delete.png')}/>
				</TouchableOpacity>
                <TouchableOpacity
                    style={styles.quick}
                    activeOpacity={0.7} 
                    onPress={() => {
					    this.closeSwiper()
					    navigate('SceneSetting', { 
                            title: '场景设置',
                            sceneId: rowData.id, 
                            roomId: rowData.roomId 
                        })
				    }}
                >
					<Image style={{width:40,height:40}} source={require('../../images/deviceIcon/setting.png')}/>
				</TouchableOpacity>
			</View>
		)
    }

    //footer
    renderFooter() {
        if(!this.state.scenes || this.state.scenes.length <= 0){
            return null
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return <FooterEnd/>
        }
        return <FooterLoading/>
    }

    // 显示添加设备按钮 还是 暂无文字
    renderSubContent(){
        if(this.props.isJuniorUser){
            return (
                <Text style={{fontSize: 14, color: Colors.themeTextLightGray, marginTop: 10}}>暂无场景</Text>
            )
        }else{
            return (
                <TouchableOpacity style={[styles.moreTouch,{marginTop:5}]} onPress={()=>{
                    const {navigate} = this.props.navigation
                    navigate('AddScene', {title: '添加场景',roomId: this.props.roomId})
                }}>
                    <Text style={{fontSize:14,color:Colors.themeTextLightGray}}>自定义场景</Text>
                    <Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
                </TouchableOpacity>
            )
        }
    }
    
    renderNoContent(){
        if(this.props.isSearchList){
            // 如果是搜索结果
            return(
                <View style={{alignItems:'center',marginTop:50}}>
                    <Text style={{color:Colors.themeTextLightGray,fontSize:13}}>暂无搜索结果</Text>
                </View>
            )
        }else{
            const Images = this.props.isDark ? ImagesDark : ImagesLight
            return (
                <View style={{alignItems:'center'}}>
                    <ListNoContent
                        style={{alignItems: 'center', marginTop: '8%'}}
                        img={Images.noScene}     
                    />
                    {this.renderSubContent()}
                </View>
            )
        }
    }

	//场景列表
	getSceneList() {
		let list = this.state.scenes

		//正在加载
		if (this.state.loading && !list) {
			return <ListLoading/>
		}
		// 网络错误
		if (!list) {
            return <ListError onPress={()=>{ this.getScene() }}/>
        }

        const maxDistance = this.props.isJuniorUser ? maxSwipeDistanceShort : maxSwipeDistance

		return (
            <SwipeableFlatList
                numColumns={this.props.numColumns}
                key={this.props.numColumns}
                ref={e => this.swiperFlatlist = e}
                contentContainerStyle={styles.contentStyle}
                scrollIndicatorInsets={{right: 1}}
                removeClippedSubviews={false}
                data={list}
                keyExtractor={this._extraUniqueKey}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
                renderQuickActions={({ item, index }) => this.getQuickActions(item, index)}
                ListEmptyComponent = {this.renderNoContent()}
                maxSwipeDistance={maxDistance}
                bounceFirstRowOnMount={false}
                onEndReached={() => this.onLoadNextPage()}
                onEndReachedThreshold={0.1}
                ListFooterComponent={() => this.renderFooter()}
                initialNumToRender={10}
                refreshControl={
                    <RefreshControl
                        colors={[Colors.themeBGInactive]}
                        tintColor={Colors.themeBGInactive}
                        refreshing={this.state.isRefreshing}
                        onRefresh={() => {
                            if (this.state.isRefreshing) return
                            this.setState({ isRefreshing: true })
                            this.getScene()
                        }}
                    />
                }
            />
        )
	}

	render() {
		return this.getSceneList()
	}
}

const styles = StyleSheet.create({
    contentStyle:{
        paddingBottom: BottomSafeMargin + 10
    },
	noSecens: {
		justifyContent: 'center',
		alignItems: 'center',
		flex: 1
	},
	//侧滑菜单的样式
	quickAContent: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginRight: 16,
		marginTop: 12,
		overflow: 'hidden',
	},
    quick: {
		alignItems: 'center',
		justifyContent: 'center',
		width: maxSwipeDistance / countSwiper,
	},
    quick2:{
        alignItems: 'center',
		justifyContent: 'center',
		width: maxSwipeDistanceShort,
    },
	moreTouch:{
		borderColor:Colors.borderLightGray,
		borderWidth:1,
		width:120,
		height:30,
		borderRadius:15,
		justifyContent:'center',
		alignItems:'center',
		flexDirection:'row'
	},
	arrow:{
		width:6,
		height:11,
		marginLeft:10,
		resizeMode:'contain'
	}
});

export default SceneList

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    Alert,
    TextInput,
    SafeAreaView,
    Dimensions,
    Animated,
    Easing,
} from 'react-native';
import { Colors, NotificationKeys, NetUrls, NetParams } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import AnimateButton from '../../common/CustomComponent/AnimateButton';

const screenW = Dimensions.get('screen').width;
const TimerCountDefault = 20

class SwitchBinding extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'开关绑定'}/>,
            headerBackground: <HeaderBackground/>
		}
	}

    constructor(props) {
        super(props);
        const { getParam } = props.navigation;
        this.sceneId = getParam('sceneId')
        this.deviceParams = getParam('deviceParams')
        
        this.state = {
            bindSeriaNO: '',
            isOnBinding: false,
            timerCount: TimerCountDefault
        }
    }

    componentDidMount() {
        
    }

    componentWillUnmount() {
        this.interval && clearInterval(this.interval);
    }

    //请求绑定
    async requestBinding() {
        let params = {}
        if (this.sceneId) {
            params.sceneId = this.sceneId
        } else if (this.deviceParams && this.deviceParams.deviceId) {
            params = this.deviceParams
        } else {
            ToastManager.show('参数有误，请稍后重试')
            return
        }
        SpinnerManager.show()
        this.setState({isOnBinding: true})
        try {
            let data = await postJson({
                url: NetUrls.switchBiding,
                params: {
                    must: 1,   //正常绑定
                    ...params
                }
            });
            SpinnerManager.close()
            if (data && data.code == 0) {
                this.setState({
                    bindSeriaNO: data.result || '',
                    timerCount: TimerCountDefault
                })
                this.startCount()
                this.animateButton.startAnimation();
            } else {
                this.setState({ isOnBinding: false })
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({ isOnBinding: false })
            ToastManager.show(JSON.stringify(error))
        }
    }

    // 查询绑定状态
    async requestBindStatus() {
        try {
            let data = await postJson({
                url: NetUrls.bindingResult,
                timeout: 1.8, // 2秒内无结果 为网络超时
                params: {
                    result: this.state.bindSeriaNO
                }
            });
            SpinnerManager.close()
            if (data && data.code == 0) {
                const { replace } = this.props.navigation
                let result = data.result
                if (result.status == 0) {
                    // 失败
                    this.bindingFail(0)

                } else if (result.status == 1) {
                    // 成功
                    DeviceEventEmitter.emit(NotificationKeys.kSwitchSuccessBidingNotification)
                    replace('SwitchBindingResult', { statusCode: 1 })

                } else if (result.status == 3){
                    // 询问是否 需要覆盖
                    this.bindingFail(3, result.info, this.state.bindSeriaNO)

                }else {
                    // 继续
                }
            } else {
                let error = new CommonException(data.msg, data.code)
                throw error
            }
        } catch (error) {
            if (error.code == NetParams.networkTimeout) {
                // 网络 1.8 秒内未完成
                return
            }
            this.bindingFail(0)
            ToastManager.show('网络错误，请稍后重试')
        }
    }

    bindingFail(statusCode, suspendingText, bindSeriaNO) {
        const { replace } = this.props.navigation
        let deviceParams = null
        if (this.deviceParams && this.deviceParams.deviceId) {
            deviceParams = JSON.parse(JSON.stringify(this.deviceParams))
        }
        replace('SwitchBindingResult', { 
            statusCode: statusCode, 
            sceneId: this.sceneId, 
            deviceParams: deviceParams,
            suspendingText: suspendingText,
            bindSeriaNO: bindSeriaNO
        })
    }

    startCount() {
        const { replace } = this.props.navigation

        this.interval = setInterval(() => {
            let timerCount = this.state.timerCount - 1;
            if (timerCount <= 0) {
                this.interval && clearInterval(this.interval);
                this.bindingFail(0)
            } else {
                this.setState({ timerCount: timerCount })
                if (timerCount % 2 == 0) {
                    this.requestBindStatus()
                }
            }
        }, 1000)
    }

    getBindingBtn() {
        if (this.state.isOnBinding && this.state.timerCount) {
            return (
                this.showAniButton(this.state.timerCount + 's ')
            )
        } else if(!this.state.isOnBinding){
            return (
                this.showAniButton('点击开始')
            )
        }else{
            return (
                this.showAniButton('正在绑定...')
            ) 
        }
    }

    showAniButton(textVal) {
        return(
            <AnimateButton
                style = {{marginTop: '5%',width:'100%'}}
                ref={e => this.animateButton = e}
                centerLabel = {textVal}
                rippleColor={Colors.switchBindBg}
                onClick={()=>{
                    if(this.state.isOnBinding){
                        return
                    }
                    this.requestBinding()
                }}
            />
        )
    }

    getMainView() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={styles.mainWrapper}>
                <Image source={require('../../images/sceneIcon/swtichBinding.png')} style={styles.mianImg} />
                <View style={{ paddingHorizontal: 20 }}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.themeText }}>请执行需要绑定的开关</Text>
                    <Text style={{ marginTop: 20, fontSize: 14, lineHeight: 16, color: Colors.themeTextLight }}>点击下方开始按钮后，在倒计时内单击需绑定的面板开关。</Text>
                    <Text style={{ marginTop: 20, fontSize: 12, color: Colors.red }}>注：无源无线开关与主机距离不可过远，否则会绑定失败！</Text>
                </View>
                {this.getBindingBtn()}
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.getMainView()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        flex: 1,
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 15,
        paddingLeft: 10,
        marginRight: 5
    },
    navText: {
        color: Colors.themeBG,
        fontSize: 15
    },
    mainWrapper: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    mianImg: {
        width: screenW * 0.5,
        height: screenW * 0.5,
        resizeMode: 'contain',
        marginTop: '15%'
    },
    startBtn: {
        width: 120,
        height: 120,
        marginTop: 60,
        borderRadius: 60,
        borderColor: Colors.switchBindBg,
        borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    spreadCircle: {
        borderRadius: 999,
        position: 'absolute',
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SwitchBinding)

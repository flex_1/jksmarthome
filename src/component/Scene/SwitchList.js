/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 设备组合
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    Alert,
    Dimensions,
    Modal,
    SwipeableFlatList
} from 'react-native';
import { connect } from 'react-redux';
import { Colors, NotificationKeys, NetUrls } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import DateUtil from '../../util/DateUtil';
import NoContentButton from '../../common/CustomComponent/NoContentButton';

const screenW = Dimensions.get('window').width;
const screenH = Dimensions.get('screen').height;

//侧滑最大距离
const maxSwipeDistance = 180
//侧滑按钮个数
const countSwiper = 2

class SwitchList extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeft navigation={navigation}/>,
			headerTitle: <HeaderTitle title={'开关列表'}/>,
			headerRight: (
				<HeaderRight buttonArrays={[
                    {text:'添加功能',onPress:()=>{
                        navigation.getParam('addDevice')()
                    }}
                ]}/>
			),
            headerBackground: <HeaderBackground/>
		}
	}

    constructor(props) {
        super(props);
        const { getParam,setParams } = props.navigation;
        this.deviceData = getParam('deviceData')
        this.sceneId = getParam('sceneId')

        this.state = {
            modalVisible: false,
            // 设备 组合
            list: null,

            // 供 picker 用的所有 时间与状态选项
            times: [0,1,2,3,4,5],
            status: [],

            currentIndex: 0,

            pickerData: null,
            optionData: null,
            pickerTitle: '选择功能',
        }

        setParams({
            addDevice: this.addDeviceBtn.bind(this)
        })
    }

    addDeviceBtn(){
        this.closeSwiper()
        if(this.sceneId){
            const {navigate} = this.props.navigation
            navigate('SwitchBinding',{sceneId: this.sceneId, deviceParams: null })
        }else{
            this.getDictList()
        }
    }

    componentDidMount() {
        this.getDeviceDetail()
        this.switchBindingNoti = DeviceEventEmitter.addListener(NotificationKeys.kSwitchSuccessBidingNotification, ()=>{
			this.getDeviceDetail()
		})
    }

    componentWillUnmount() {
        this.switchBindingNoti.remove()
    }

    // 获取设备绑定列表
    async getDeviceDetail() {
        SpinnerManager.show()
        let params = {}
        if(this.deviceData && this.deviceData.id){
            params.deviceId = this.deviceData.id
        }
        if(this.sceneId){
            params.sceneId = this.sceneId
        }
        try {
            let data = await postJson({
                url: NetUrls.bindingList,
                params: params
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.setState({
                    list: data.result || [],
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

    // 解绑
    async deleteDevice(id) {
        SpinnerManager.show('正在解绑...')
        try {
            let data = await postJson({
                url: NetUrls.unbinding,
                params: {
                    id: id
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.getDeviceDetail()
                ToastManager.show('解绑成功')
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

    // 更新
    async updateSwitch(id) {
        SpinnerManager.show('正在更新...')
        try {
            let data = await postJson({
                url: NetUrls.bindingUpdate,
                params: {
                    id: id
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.getDeviceDetail()
                ToastManager.show('更新完成')
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

    // 获取字典列表
    async getDictList() {
        try {
            let data = await postJson({
                url: NetUrls.bindingOptions,
                params: {
                    deviceId: this.deviceData.id,
                }
            });
            if (data.code == 0) {
                let list = data.result

                if (list && list.length > 0) {
                    let pickerData = []
                    for (const item of list) {
                        pickerData.push(item.text)
                    }
                    this.setState({
                        pickerData: pickerData,
                        optionData : list,
                        modalVisible: true
                    })
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    // Picker 确定按钮 点击处理
    handlePickerSelectData(selectData,selectIndex){

        const {navigate,pop} = this.props.navigation
        let param = {}

        param.deviceId = this.deviceData.id
        param.id = this.state.optionData[selectIndex[0]].id
        param.text = this.state.optionData[selectIndex[0]].text
        param.key = this.state.optionData[selectIndex[0]].key

        // param.valueText = this.state.result[selectIndex[0]].list[selectIndex[1]].valueText
        // param.value = this.state.result[selectIndex[0]].list[selectIndex[1]].value

        navigate('SwitchBinding',{sceneId: null, deviceParams: param })
        this.setState({
            modalVisible: false
        })
    }

    //关闭侧滑栏
    closeSwiper() {
        this.sceneCombineList && this.sceneCombineList.setState({ openRowKey: null })
    }

    //侧滑菜单渲染
    getQuickActions(rowData, index) {
        return (
            <View style={styles.quickAContent}>
                <TouchableOpacity
                    style={[styles.quick, { backgroundColor: Colors.tabActiveColor }]}
                    activeOpacity={0.7} 
                    onPress={() => {
                        this.closeSwiper()
                        this.updateSwitch(rowData.id)
                    }}
                >
                    <Text style={{ color: Colors.white, fontSize: 14 }}>更新</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.quick, { backgroundColor: Colors.themBgRed }]}
                    activeOpacity={0.7} 
                    onPress={() => {
                        this.closeSwiper()
                        Alert.alert(
                            '提示',
                            '确定解绑?',
                            [
                                { text: '取消', onPress: () => { }, style: 'cancel' },
                                { text: '解绑', onPress: () => { this.deleteDevice(rowData.id) } },
                            ]
                        )
                    }}
                >
                    <Text style={{ color: Colors.white, fontSize: 14 }}>解绑</Text>
                </TouchableOpacity>
            </View>
        )
    }

    _extraUniqueKey(item, index) {
        return "scene_devices_index_" + index;
    }

    // 渲染每一行
    renderRowItem(rowData, index) {
        const Colors = this.props.themeInfo.colors;
        const icon = this.props.themeInfo.isDark ? rowData.nightImg : rowData.dayImg

        // 楼层
		let roomInfoView = null
		if(rowData.floorText && rowData.roomName){
            roomName = rowData.floorText + '  ' + rowData.roomName
            roomInfoView = (
                <Text style={{fontSize: 12, color: Colors.themeTextLight,marginTop:5}}>{roomName}</Text>
            )
        }
        
        // 功能
        let extraText = null
        if(rowData.stext){
            extraText = <Text style={{ fontSize: 15, color: Colors.themeButton }}>{rowData.stext}</Text>
        }

        //序列号
        let svalueText = null
        if(rowData.svalueText){
            svalueText = <Text style={{ fontSize: 14, marginLeft: 10, color: Colors.themeTextLight }}>{rowData.svalueText}</Text>
        }

        //时间
        let mounth = rowData.prefix.substring(4,6)
        let date = rowData.prefix.substring(6,8)
        let hour = rowData.prefix.substring(8,10)
        let minute = rowData.prefix.substring(10,12)
        let second = rowData.prefix.substring(12,14)
        let dateText = mounth + '-' + date + ' ' + hour + ':' + minute + ':' + second
        
        return (
            <View style={styles.itemWrapper}>
                <View style={[styles.item,{backgroundColor: Colors.themeBg}]}>
                    <Image source={{ uri: icon }} style={styles.deviceIcon} />
                    <View style={{flex: 1,marginLeft: 15}}>
                        <Text style={{fontSize: 15, color: Colors.themeText}}>{rowData.name}</Text>
                        {roomInfoView}
                    </View>
                    <View style={{alignItems: 'flex-end'}}>
                        <View style={{flexDirection:'row', alignItems:'center',marginBottom: 10}}>
                            {extraText}
                            {svalueText}
                        </View>
                        <Text style={{ fontSize: 14, color: Colors.themeTextLight }}>{dateText}</Text>
                    </View>
                </View>
            </View>
        )
    }

    getCenterView() {
        if (!this.state.list || !Array.isArray(this.state.list)) {
            return null
        }
        if (this.state.list.length <= 0) {
            return(
                <View style={styles.noContentWrapper}>
                    <Text style={styles.noContentText}>暂无设备绑定</Text>
                    <NoContentButton
                        text={'添加功能'}
                        onPress={()=>{
                            this.addDeviceBtn()
                        }}
                    />
				</View>
            )
        }
        return (
            <View style={{ flex: 1 }}>
                <SwipeableFlatList
                    ref={e => this.sceneCombineList = e}
                    contentContainerStyle={{ paddingBottom: 50 }}
                    scrollIndicatorInsets={{right: 1}}
                    maxSwipeDistance={maxSwipeDistance}
                    bounceFirstRowOnMount={false}
                    data={this.state.list}
                    keyExtractor={this._extraUniqueKey}
                    renderItem={({ item, index }) => this.renderRowItem(item, index)}
                    renderQuickActions={({ item, index }) => this.getQuickActions(item, index)}
                />
            </View>
        )
    }

    getModal() {
        if(!this.state.pickerData){
            return null
        }
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        window.DoubulePicker.init({
            pickerData: this.state.pickerData,
            pickerConfirmBtnText: '确定',
            pickerCancelBtnText: '取消',
            pickerTitleText: this.state.pickerTitle,
            pickerCancelBtnColor: [38, 80, 215, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: this.state.selectData,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false,
                    selectData: data
                })
                this.handlePickerSelectData(data,index);
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })
            },
            onPickerSelect: data => {

            }
        });
        window.DoubulePicker.show()
    }

    render() {
        const Colors = this.props.themeInfo.colors;

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.getCenterView()}
                {this.getModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    deviceIcon: {
        width: 40,
        height: 40,
        resizeMode: 'contain'
    },
    itemWrapper:{ 
        justifyContent: 'center', 
        alignItems: 'center', 
        width: '100%', 
        marginTop: 15
    },
    item: {
        width: '90%',
        height: 60,
        borderRadius: 5,
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 16
    },
    modalStyle: {
        flex: 1
    },
    modalBackground: {
        width: '100%',
        height: '100%',
        backgroundColor: Colors.translucent
    },
    rowChooseBtn: {
        flexDirection: 'row',
        width: 100,
        height: 30,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
    },
    //侧滑菜单的样式
    quickAContent: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginRight: '5%',
        marginTop: 15,
        overflow: 'hidden',
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        paddingVertical: 1
    },
    quick: {
        alignItems: 'center',
        justifyContent: 'center',
        width: maxSwipeDistance / countSwiper,
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    noContentWrapper:{
        marginTop: screenH * 0.3,
        alignItems: 'center', 
        justifyContent: 'center'
    },
    noContentText:{
        color: Colors.themeTextLightGray,
        fontSize: 14,
        marginBottom: 20
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SwitchList)

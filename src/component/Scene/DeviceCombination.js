/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 设备组合
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    FlatList,
    Alert,
    Dimensions,
    Modal,
    TextInput,
    SwipeableFlatList
} from 'react-native';
import { Colors, NotificationKeys, NetUrls } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import NoContentButton from '../../common/CustomComponent/NoContentButton';

const screenW = Dimensions.get('window').width;
const screenH = Dimensions.get('window').height;

//侧滑最大距离
const maxSwipeDistance = 180
//侧滑按钮个数
const countSwiper = 2

class DeviceCombination extends Component {

    static navigationOptions = ({ navigation }) => {
        let rightBtn = null
        if(!navigation.getParam('isJuniorUser')){
            rightBtn = (
                <HeaderRight buttonArrays={[
                    {text:'添加设备',onPress:()=>{
                        navigation.getParam('addDeviceBtnClick')()
                    }}
                ]}/>
            )
        }
		return {
			headerLeft: <HeaderLeft navigation={navigation} backTitle={'设备组合'}/>,
			headerRight: rightBtn,
            headerBackground: <HeaderBackground/>
		}
	}

    constructor(props) {
        super(props);
        const { getParam,setParams } = props.navigation;
        this.roomId = getParam('roomId')
        this.floor = getParam('floor')
        this.floorText = getParam('floorText')
        this.roomName = getParam('roomName')
        this.sceneId = getParam('sceneId')
        this.callBack = getParam('callBack')
        
        this.state = {
            modalVisible: false,
            // 设备 组合
            selectedIds: [],
            list: null,

            pickerData: null,
            pickerTitle: '',
            delayList: [],
            resultList: [],
            selectedRowData: {},
            excuteData: [],
            currentSelectedValue: []
        }

        setParams({
            addDeviceBtnClick: this.addDeviceBtnClick.bind(this)
        })
    }

    componentDidMount() {
        this.getSceneDetail()
    }

    componentWillUnmount() {
        
    }

    addDeviceBtnClick(){
        const { navigate } = this.props.navigation
        navigate('SelectDevice', {
            roomId: this.roomId,
            floor: this.floor,
            floorText: this.floorText,
            roomName: this.roomName,
            sceneId: this.sceneId,
            type:4,
            isScene: true,
            //selectedIds: this.state.selectedIds,
            callBack: () => {
                this.getSceneDetail()
                this.callBack && this.callBack()
                DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
            }
        })
    }

    // 获取设备组合列表
    async getSceneDetail() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.getSceneDetail,
                params: {
                    id: this.sceneId,
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                let selectedIds = []
                if (data.result && data.result.length > 0) {
                    for (const iterator of data.result) {
                        selectedIds.push(iterator.deviceId)
                    }
                }
                this.setState({
                    list: data.result || [],
                    selectedIds: selectedIds
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

    // 更改场景组合
    async updateSceneDetail(selectedStatus) {
        this.setState({
            modalVisible: !this.state.modalVisible
        })
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.addSceneDetail,
                params: {
                    id: this.state.selectedRowData.id,
                    sceneId: this.sceneId,
                    deviceId: this.state.selectedRowData.deviceId,
                    status: selectedStatus
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.getSceneDetail()
                ToastManager.show('设置成功')
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

    // 删除 场景组合
    async deleteSceneDetail(id) {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.delSceneDetail,
                params: {
                    id: id
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.getSceneDetail()
                this.callBack && this.callBack()
                ToastManager.show('删除成功')
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

    // 获取延时 选项
    async getDictList() {
        if(this.state.delayList && this.state.delayList.length>0){
            this.getConditionList()
            return
        }

        SpinnerManager.show();
        try {
            let data = await postJson({
                url: NetUrls.getDelayDictionary,
                params: {
                    type:4
                }
            });
            SpinnerManager.close();
            if (data.code == 0 && data.result) {
                this.setState({
                    delayList: data.result.delayList || [],
                },()=>{
                    this.getConditionList()
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close();
            ToastManager.show('网络错误')
        }
    }

    // 获取操作选项
    async getConditionList(){
        SpinnerManager.show();
        try {
            let data = await postJson({
                url: NetUrls.getExecuteByDeviceId,
                params: {
                    deviceId: this.state.selectedRowData.deviceId
                }
            });
            SpinnerManager.close();
            if (data.code == 0) {
                if (data.result && data.result.length>0) {
                    this.setState({
                        excuteData: data.result,
                        resultList: data.result[0].value
                    },()=>{
                        this.dealAndJump()
                    })
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close();
            ToastManager.show('网络错误')
        }
    }

    // 复制设备
    async copyDevice(id) {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.sceneCopyDevice,
                params: {
                    id: id
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.getSceneDetail()
                this.callBack && this.callBack()
                ToastManager.show('设备复制成功')
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

    //响应或者跳转
    dealAndJump(){
        const {navigate} = this.props.navigation

        let dlist = this.state.delayList
        let rlist = this.state.resultList
        // 设备 执行的 数据
        let excuteData = this.state.excuteData
        const { deviceName,deviceId,icon } = this.state.selectedRowData

        if(this.state.excuteData.length <= 1){
            let s_title = []
            let s_value = []
            for (const item of dlist) {
                s_title.push(item.text)
            }
            for (const item of rlist) {
                s_value.push(item.text)
            }
            this.setState({
                pickerData: [s_title,s_value],
                modalVisible: true,
                pickerTitle: deviceName,
            })  
        }else{
            navigate('SenceDeviceList',{
                sceneId: this.sceneId,
                title: deviceName,
                deviceId: deviceId,
                deviceIcon: icon,
                type: 4,
                list: excuteData,
                selectItem: this.state.selectedRowData,
                timeList: dlist,
                callBack: () => {
                    this.getSceneDetail()
                    this.callBack && this.callBack()
                    DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
                }
            })
        }
    }

    // Picker 确定按钮 点击处理
    handlePickerSelectData(selectData,selectIndex){
        let executeKey = this.state.excuteData[0].executeKey ||  'status'

        let delayList = this.state.delayList || {}
        let delay = delayList[selectIndex[0]].id

        let resultList = this.state.resultList || {}
        let selectValue = resultList[selectIndex[1]].id

        let data = {}
        data[executeKey] = selectValue + ',' + delay

        this.updateSceneDetail( JSON.stringify(data) );
        this.setState({ modalVisible: false})
    }

    //关闭侧滑栏
    closeSwiper() {
        this.sceneCombineFlatlist && this.sceneCombineFlatlist.setState({ openRowKey: null })
    }

    //侧滑菜单渲染
    getQuickActions(rowData, index) {
        if(this.props.userInfo.memberType == 2){
            return null
        }
        return (
            <View style={styles.quickAContent}>
                <TouchableOpacity activeOpacity={0.7} onPress={() => {
                    this.closeSwiper()
                    Alert.alert(
                        '提示',
                        '确定移除该设备?',
                        [
                            { text: '取消', onPress: () => { }, style: 'cancel' },
                            { text: '删除', onPress: () => { this.deleteSceneDetail(rowData.id) } },
                        ]
                    )
                }}>
                    <View style={[styles.quick, { backgroundColor: Colors.themBgRed }]}>
                        <Text style={{ color: Colors.white, fontSize: 14 }}>删除</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.7} onPress={() => {
                    this.closeSwiper()
                    this.copyDevice(rowData.id)
                }}>
                    <View style={[styles.quick, { backgroundColor: Colors.themPurpe }]}>
                        <Text style={{ color: Colors.white, fontSize: 14 }}>复制</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    _extraUniqueKey(item, index) {
        return "scene_devices_index_" + index;
    }

    _renderSettingBtn(rowData){
        const isJuniorUser = this.props.userInfo.memberType == 2

        let settingArrow = <Image style={styles.downArrow} source={require('../../images/sceneIcon/sence_down_icon.png')} />
        if(rowData.attributeCount > 1){
            settingArrow = <Image style={styles.rightArrow} source={require('../../images/sceneIcon/sence_right_icon.png')} />
        }else if(isJuniorUser){
            settingArrow = null
        }
        return(
            <TouchableOpacity activeOpacity={0.7} style={styles.rowChooseBtn} onPress={() => {
                if(isJuniorUser && rowData.attributeCount <= 1){
                    return
                }
                this.closeSwiper()
                let s_value = rowData.conditionValue || ''
                let s_title = (rowData.conditionTitle || '') + (rowData.conditionUnit || '')
                this.setState({ 
                    selectedRowData: JSON.parse( JSON.stringify(rowData) ),
                    currentSelectedValue: [s_value,s_title],
                },()=>{
                    this.getDictList()
                })
            }}>
                <Text style={{ fontSize: 12, color: Colors.white }}>
                    {rowData.conditionValue + "  " + rowData.conditionTitle}{rowData.conditionUnit?rowData.conditionUnit:''}
                </Text>
                {settingArrow}
            </TouchableOpacity>
        )
    }

    // 渲染每一行
    renderRowItem(rowData, index) {
        const Colors = this.props.themeInfo.colors
        const icon = this.props.themeInfo.isDark ? rowData.nightImg : rowData.dayImg

        // 楼层
		let roomInfoView = null
		if(rowData.floorText && rowData.roomName){
            roomName = rowData.floorText + '  ' + rowData.roomName
            roomInfoView = (
                <Text style={{fontSize: 12, color: Colors.themeTextLight, marginTop:5}}>{roomName}</Text>
            )
        }
        
        return (
            <View style={styles.itemWrapper}>
                <View style={[styles.item,{backgroundColor: Colors.themeBg}]}>
                    <Image source={{ uri: icon }} style={styles.deviceIcon} />
                    <View style={{flex: 1,marginLeft: 15}}>
                        <Text style={{fontSize: 15, color: Colors.themeText}}>{rowData.deviceName}</Text>
                        {roomInfoView}
                    </View>
                    {this._renderSettingBtn(rowData)}
                </View>
            </View>
        )
    }

    renderNoContent(){
        const isJuniorUser = this.props.userInfo.memberType == 2
        return(
            <View style={styles.noContentWrapper}>
                <Text style={styles.noContentText}>暂无设备组合</Text>
                {isJuniorUser ? null :
                    <NoContentButton
                        text={'添加设备'}
                        onPress={()=>{
                            this.addDeviceBtnClick()
                        }}
                    />
                }
            </View>
        )
    }

    getCenterView() {
        if (!this.state.list || !Array.isArray(this.state.list)) {
            return null
        }
        return (
            <SwipeableFlatList
                style={{flex: 1}}
                ref={e => this.sceneCombineFlatlist = e}
                contentContainerStyle={{ paddingBottom: 50 }}
                scrollIndicatorInsets={{right: 1}}
                renderQuickActions={({ item, index }) => this.getQuickActions(item, index)}//创建侧滑菜单
                maxSwipeDistance={maxSwipeDistance}
                bounceFirstRowOnMount={false}
                data={this.state.list}
                ListEmptyComponent={this.renderNoContent()}
                keyExtractor={this._extraUniqueKey}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
            />
        )
    }

    getModal() {
        if(!this.state.pickerData){
            return null
        }
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.CustomPicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerData,
            pickerTitleText: this.state.pickerTitle,
            selectedValue: this.state.currentSelectedValue,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false,
                    selectData: data
                })
                this.handlePickerSelectData(data,index);
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                }, () => {
                    window.CustomPicker.hide()
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.getCenterView()}
                {this.getModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.themBGLightGray,
        flex: 1,
    },
    navTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        marginLeft: 15,
        textAlignVertical: 'center',
        color: Colors.themeTextBlack,
        flex: 1
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 16,
        paddingLeft: 10,
    },
    navText: {
        color: Colors.tabActiveColor,
        fontSize: 15
    },
    deviceIcon: {
        width: 40,
        height: 40,
        marginLeft: 8,
        resizeMode: 'contain'
    },
    itemWrapper:{ 
        justifyContent: 'center', 
        alignItems: 'center', 
        width: '100%', 
        marginTop: 15
    },
    item: {
        width: '90%',
        height: 70,
        borderRadius: 5,
        alignItems: 'center',
        flexDirection: 'row'
    },
    noContentWrapper:{
        marginTop: screenH * 0.3,
        alignItems: 'center', 
        justifyContent: 'center'
    },
    noContentText:{
        color: Colors.themeTextLightGray,
        fontSize: 14,
        marginBottom: 20
    },
    modalStyle: {
        flex: 1
    },
    modalBackground: {
        width: '100%',
        height: '100%',
        backgroundColor: Colors.translucent
    },
    rowChooseBtn: {
        flexDirection: 'row',
        backgroundColor: Colors.tabActiveColor,
        width: 120,
        height: 30,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10,
    },
    modalHeader: {
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        height: 40,
    },
    modalConfirmBtn: {
        justifyContent: 'center',
        alignItems: 'flex-end',
        fontSize: 16,
        paddingRight: 15,
        paddingTop: 11
    },
    //侧滑菜单的样式
    quickAContent: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginRight: '5%',
        marginTop: 15,
        overflow: 'hidden',
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        paddingVertical: 1
    },
    quick: {
        backgroundColor: Colors.white,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: maxSwipeDistance / countSwiper,
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    downArrow:{
        marginLeft:6,
        width:8,
        height:4.5,
        resizeMode:'contain'
    },
    rightArrow:{
        marginLeft:6,
        width:4.5,
        height:8,
        resizeMode:'contain'
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
        userInfo: state.userInfo
    })
)(DeviceCombination)

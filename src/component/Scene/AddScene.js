/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	DeviceEventEmitter,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Alert,
	TextInput,
	SafeAreaView,
	Keyboard
} from 'react-native';
import { Colors, NotificationKeys, NetUrls } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';

class AddScene extends Component {

	static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeft navigation={navigation}/>,
			headerTitle: <HeaderTitle title={navigation.getParam('title')}/>,
			headerRight: (
				<HeaderRight buttonArrays={[
                    {text:'保存',onPress:()=>{
                        navigation.getParam('saveBtnClick')()
                    }}
                ]}/>
			),
            headerBackground: <HeaderBackground/>
		}
	}

	constructor(props) {
		super(props);
		const { getParam,setParams } = props.navigation;
		this.sceneData = getParam('sceneData')
		this.roomId = getParam('roomId')

		this.state = {
			text: null,
			thumbs: [],
			selectThumb: null,
			autoName: null,
			selectThumbId: 0
        }
        
        setParams({
            saveBtnClick: ()=>{ this.save() }
        })
        
	}

	componentDidMount() {
		this.getThumbList()
	}

	componentWillUnmount() {
		
	}

	// 获取场景 图标
	async getThumbList(){
		try {
			let data = await postJson({
				url: NetUrls.getScenePicture,		
			});
			if (data.code == 0) {
				this.setState({
					thumbs : data.result || []
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
	}

	// 保存按钮点击
	async save() {
		Keyboard.dismiss()
		// 添加 或 设置场景接口
		const {pop,replace} = this.props.navigation
		let name = this.state.text || this.state.autoName
		if(!name){
			ToastManager.show('场景名不能为空')
			return
		}
		if(!this.state.selectThumb){
			ToastManager.show('请选择场景图标')
			return
		}
		let params = {}
		if(this.roomId){
			params.roomId = this.roomId
		}
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.addScene,
				params: {
					...params,
					name: name,
					groupName: this.state.selectThumb.groupName,
					imgId: this.state.selectThumbId,
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                replace('SceneSetting', { title: '场景设置', sceneId: data.result, roomId:this.roomId })
                DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
				DeviceEventEmitter.emit(NotificationKeys.kSceneFloorNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	// 获取场景 图标
	getThumbs() {
        const Colors = this.props.themeInfo.colors
        
		let thumbsList = this.state.thumbs.map((val,index)=>{
            let tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}
            let textColor = Colors.themeText
            let iconBg = {}

			if(this.state.selectThumb && this.state.selectThumb.groupName == val.groupName){				
				iconBg = {backgroundColor:Colors.themeButton,borderRadius: 4}
                textColor = Colors.white
                tintColor = {tintColor: Colors.white}
			}
			return(
				<View style={styles.btnWrapper} key={index}>
					<TouchableOpacity style={[styles.btnTouch,iconBg]} key={index} onPress={()=>{
						this.setState({
							selectThumb: val,
							autoName: val.groupName,
							selectThumbId: val.idIconOn
						})
					}}>
						<Image style={[styles.icon,tintColor]} source={{uri: val.dayUrlIconOn}} resizeMode={'contain'}/>
						<Text style={[styles.iconName,{color:textColor}]}>{val.groupName}</Text>
					</TouchableOpacity>
				</View>
			)
		})
		return (
            <View style={{flexDirection:'row',flexWrap:'wrap',marginTop:10,paddingHorizontal:16}}>
				{thumbsList}
			</View>
		)
    }
    
    getInput() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.inputWrapper,{backgroundColor: Colors.themeBg}]}>
				<Text style={{ marginLeft: 20, fontSize: 15, color: Colors.themeText }}>场景名称</Text>
				<TextInput
					style={[styles.input,{color: Colors.themeText}]}
					defaultValue={this.state.text || this.state.autoName}
					placeholder={'请输入场景名称(不超过8个字)'}
					placeholderTextColor={Colors.themeInactive}
					onChangeText={(text) => {
						this.setState({
							text: text,
							autoName: text
						})
					}}
					returnKeyType={'done'}
				/>
			</View>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
            <KeyboardAwareScrollView 
                style={[styles.container,{backgroundColor: Colors.themeBaseBg}]} 
                contentContainerStyle={styles.scrollContainStyle}
            >
				{this.getThumbs()}
                {this.getInput()}
			</KeyboardAwareScrollView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	scrollContainStyle: {
		paddingBottom: 60
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight: 5
	},
	navText: {
		color: Colors.tabActiveColor,
		fontSize: 15
	},
	inputWrapper: {
		marginHorizontal: '5%',
		height: 55,
		
		borderRadius: 5,
		alignItems: 'center',
        flexDirection: 'row',
        marginTop: 30
	},
	floorTouch: {
		flexDirection: 'row',
		justifyContent: 'flex-end',
		alignItems: 'center',
		height: '90%',
		flex: 1,
		paddingRight: 10
	},
	addThumbTouch: {
		width: 84,
		height: 84,
		borderRadius: 4,
		alignItems: 'center',
		justifyContent: 'center'
	},
	input: {
		flex: 1,
		height: 40,
		marginLeft: 5,
		marginRight: 10,
		textAlign: 'right',
		paddingRight: 10
    },
    btnWrapper:{
        width:'25%',
        justifyContent:'center',
        alignItems:'center',
        paddingTop:5,
        paddingBottom:5
    },
    btnTouch:{
        justifyContent:'center',
        alignItems:'center',
        padding:10
    },
    icon:{
        width:30,
        height:30,
        resizeMode: 'contain'
    },
    iconName:{
        textAlign:'center',
        fontSize:14,
        marginTop:10
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(AddScene)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    FlatList,
    Alert,
    Modal
} from 'react-native';
import { Colors, NetUrls } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import {ListNoContent} from "../../common/CustomComponent/ListLoading";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import {ImagesLight, ImagesDark} from '../../common/Themes';
import SmartListItem from "../CommonPage/ListItems/SmartListItem";

class SenceChooseSmart extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={navigation.getParam('title')}/>,
            headerRight: (
				<HeaderRight buttonArrays={[
                    {text:'添加',onPress:()=>{
                        navigation.getParam('addBtnClick')()
                    }},
                ]}/>
			),
            headerBackground: <HeaderBackground hiddenBorder={true}/>
		}
	}

    constructor(props) {
        super(props);
        const { getParam,setParams } = props.navigation;
        this.callBack = getParam('callBack')
        this.sceneId = getParam('sceneId')

        this.state = {
            smartListData: null,
            selectedSmart: []
        }

        setParams({
            addBtnClick: this.addBtnClick.bind(this)
        })
    }

    componentDidMount() {
        this.requestSamrtList()
    }

    componentWillUnmount() {
        
    }

    addBtnClick(){
        if(this.state.selectedSmart.length <= 0){
            ToastManager.show('至少选择一条智能')
            return
        }
        this.requestAddSmart()
    }

    // 添加设备到场景
    async requestSamrtList(){
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.allSmartForScene,
                params: {
                    id: this.sceneId
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.setState({
                    smartListData: data.result || [] 
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

    // 添加智能到场景
    async requestAddSmart(){
        const { pop } = this.props.navigation
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.addSmartToScene,
                params: {
                    id: this.sceneId,
                    intelligentId: this.state.selectedSmart
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                pop()
                this.callBack && this.callBack()
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

    _extraConditionUniqueKey(item, index) {
        return "smart_index_" + index;
    }

    renderRowItem(rowData,index) {
        let isSelected = this.state.selectedSmart.includes(rowData.intelligentId)

        return(
            <SmartListItem 
                rowData={rowData} 
                index={index} 
                isSmartChoose={true}
                isSelected={isSelected}
                callBack={(id)=>{
                    if(this.state.selectedSmart.includes(id)){
                        this.state.selectedSmart.remove(id)
                    }else{
                        this.state.selectedSmart.push(id)
                    }
                    this.setState({
                        selectedSmart: this.state.selectedSmart
                    })
                }}
                isDark={this.props.themeInfo.isDark}
            />
        )
    }

    renderSmartList(){

        let list = this.state.smartListData;

        // 暂无场景
        if (list && list.length <= 0) {
            const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
            return (
                <ListNoContent img={Images.noSmart} text={'暂无可添加的智能'}/>
            )
        }

        return(
            <FlatList
                contentContainerStyle={{ paddingBottom: 50 }}
                style={styles.list}
                data={list}
                keyExtractor={this._extraConditionUniqueKey}
                renderItem={({item,index}) => this.renderRowItem(item,index)}
            />
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]} >
                {this.renderSmartList()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    list:{
        marginTop: 10
    },
    addTouch:{
        marginTop:20,
        marginBottom:20,
        height: 44,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderWidth: 1,
        borderColor: Colors.tabActiveColor,
        marginHorizontal: 16,
    },
    modalTouchable:{
        width: '100%',
        height: '100%',
        backgroundColor: Colors.translucent
    },
    itemTouch:{
        paddingHorizontal: 16,
        height:55,
        alignItems:'center',
        flexDirection: 'row',
        borderBottomWidth: 1
    },
    itemText:{
        marginLeft: 20,
        fontSize: 16,
        flex:1
    },
    itemDesc:{
        position:'absolute',
        right:30,
        marginLeft: 20,
        fontSize: 16
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SenceChooseSmart)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    SwipeableFlatList,
    Alert,
    Modal,
    Dimensions
} from 'react-native';
import { Colors, NetUrls } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import {ListNoContent} from "../../common/CustomComponent/ListLoading";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import SmartListItem from "../CommonPage/ListItems/SmartListItem";
import NoContentButton from '../../common/CustomComponent/NoContentButton';

//侧滑最大距离
const maxSwipeDistance = 90
//侧滑按钮个数
const countSwiper = 1
const screenH = Dimensions.get('screen').height;

class SenceSmartList extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={navigation.getParam('title')}/>,
            headerRight: (
				<HeaderRight buttonArrays={[
                    {text:'添加智能',onPress:()=>{
                        navigation.getParam('addBtnClick')()
                    }},
                ]}/>
			),
            headerBackground: <HeaderBackground hiddenBorder={true}/>
		}
	}

    constructor(props) {
        super(props);
        const { getParam,setParams } = props.navigation;
        this.callBack = getParam('callBack')
        this.sceneId = getParam('sceneId')

        this.state = {
            smartListData: null,
        }

        setParams({
            addBtnClick: this.addBtnClick.bind(this)
        })
    }

    componentDidMount() {
        this.requestSamrtList()
    }

    componentWillUnmount() {
        
    }

    addBtnClick(){
        const { navigate } = this.props.navigation

        navigate('SenceChooseSmart',{
            title: '添加智能',
            sceneId: this.sceneId,
            callBack: () => {
                this.requestSamrtList()
                this.callBack && this.callBack()
            }
        })
    }

    async requestSamrtList(){
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.sceneSmartList,
                params: {
                    id: this.sceneId,
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.setState({
                    smartListData: data.result || []
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

    // 停启用智能
    async requestUpdateSmart(id, status, callBack){
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.updateSceneIntelligent,
                params: {
                    id: id,
                    status: status
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                callBack && callBack()
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

    // 删除智能
    async requestDeleteSmart(id){
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.delSceneIntelligent,
                params: {
                    id: id
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.requestSamrtList()
                this.callBack && this.callBack()
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

    _extraConditionUniqueKey(item, index) {
        return "smart_index_" + index;
    }

    // 获取侧滑设置
    getQuickActions(rowData, index){
        return (
			<View style={styles.quickAContent}>
				<TouchableOpacity activeOpacity={0.7} onPress={() => {
					this.closeSwiper()
					this.requestDeleteSmart(rowData.id)
				}}>
					<View style={[styles.quick, { backgroundColor: Colors.themBgRed }]}>
						<Text style={{ color: Colors.white, fontSize: 14 }}>删除</Text>
					</View>
				</TouchableOpacity>
			</View>
		)
    }

    //关闭侧滑栏
	closeSwiper() {
        this.swiperFlatlist && this.swiperFlatlist.setState({ openRowKey: null })
    }

    renderRowItem(rowData,index) {
        return(
            <SmartListItem 
                rowData={rowData} 
                index={index}
                isStartAndStop={true}
                callBack={(status)=>{
                    this.requestUpdateSmart(rowData.id,status,()=>{
                        let newData = JSON.parse(JSON.stringify(rowData))
                        newData.status = status
                        this.state.smartListData[index] = newData
                        this.setState({
                            smartListData: this.state.smartListData
                        })
                    })
                }}
                closeSwiper={()=>{ this.closeSwiper() }}
                isDark={this.props.themeInfo.isDark}
            />
        )
    }

    renderNoContent(){
        return(
            <View style={styles.noContentWrapper}>
                <Text style={styles.noContentText}>暂无智能</Text>
                <NoContentButton
                    text={'添加智能'}
                    onPress={()=>{
                        this.addBtnClick()
                    }}
                />
            </View>
        )
    }

    renderSmartList(){
        if (!this.state.smartListData) {
            return null
        }
        return(
            <SwipeableFlatList
                ref={e => this.swiperFlatlist = e}
                contentContainerStyle={{ paddingBottom: 50 }}
                data={this.state.smartListData}
                keyExtractor={this._extraConditionUniqueKey}
                renderItem={({item,index}) => this.renderRowItem(item,index)}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator = {false}
                renderQuickActions={({ item, index }) => this.getQuickActions(item, index)}
                maxSwipeDistance={maxSwipeDistance}
				bounceFirstRowOnMount={false}
                ListEmptyComponent = {this.renderNoContent()}
            />
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]} >
                {this.renderSmartList()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    addTouch:{
        marginTop:20,
        marginBottom:20,
        height: 44,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderWidth: 1,
        borderColor: Colors.tabActiveColor,
        marginHorizontal: 16,
    },
    modalTouchable:{
        width: '100%',
        height: '100%',
        backgroundColor: Colors.translucent
    },
    itemTouch:{
        paddingHorizontal: 16,
        height:55,
        alignItems:'center',
        flexDirection: 'row',
        borderBottomWidth: 1
    },
    itemText:{
        marginLeft: 20,
        fontSize: 16,
        flex:1
    },
    itemDesc:{
        position:'absolute',
        right:30,
        marginLeft: 20,
        fontSize: 16
    },
    //侧滑菜单的样式
	quickAContent: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginRight: 16,
		marginTop: 12,
		overflow: 'hidden',
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
        paddingVertical: 0.5
	},
	quick: {
        flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
        width: maxSwipeDistance / countSwiper,
	},
    noContentWrapper:{
        marginTop: screenH * 0.3,
        alignItems: 'center', 
        justifyContent: 'center'
    },
    noContentText:{
        color: Colors.themeTextLightGray,
        fontSize: 14,
        marginBottom: 20
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SenceSmartList)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	DeviceEventEmitter,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Alert,
	TextInput,
    NativeModules,
    NativeEventEmitter
} from 'react-native';
import { connect } from 'react-redux';
import { Colors, NotificationKeys, NetUrls } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import {BottomSafeMargin} from '../../util/ScreenUtil';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import {ImagesLight, ImagesDark} from '../../common/Themes';
import DraggableManager from '../../common/CustomComponent/DraggableManager';
import {HSTips} from '../../common/Strings';
import SwitchButton from '../../common/CustomComponent/SwitchButton';

const JKRNUtils = NativeModules.JKRNUtils;
const ShortCutManager = Platform.select({
    ios: new NativeEventEmitter(JKRNUtils),
    android: null
})

class SceneSetting extends Component {

    static navigationOptions = ({ navigation }) => {
        let collectIcon = navigation.getParam('isCollect') ? require('../../images/collect/collect_blue.png')
            : require('../../images/collect/collect_border.png')

		return {
			headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
			headerRight: (
				<HeaderRight buttonArrays={[
                    {icon:require('../../images/share.png'),noImgTint:true,onPress:()=>{
                        navigation.getParam('shareBtnClick')()
                    }},
                    {icon:collectIcon, tintColor: {}, onPress:()=>{
                        navigation.getParam('collectBtnClick')()
                    }}
                ]}/>
			),
            headerBackground: <HeaderBackground/>
		}
	}

	constructor(props) {
		super(props);
		const { getParam,setParams } = props.navigation;
		this.sceneId = getParam('sceneId')
		this.roomId = getParam('roomId')
		this.callBack = getParam('callBack')

		this.state = {
            thumb: null,
            thumb_dark: null,
			sceneName: '',
			timerCount: 0,
			
            switchbinding: 0,
			isCollect: getParam('isCollect'),
            thumbId: 0,
            floor: null,
            roomName: null,
            istop: false,
            mistakeLock: false,
            shareInfo: null,
            intelligentCount: 0
		}

		setParams({
			shareBtnClick: ()=>{
				this.share()
            },
            collectBtnClick: ()=>{
                this.collect()
            }
		})
	}

	componentDidMount() {
		this.switchBindingNoti = DeviceEventEmitter.addListener(NotificationKeys.kSwitchSuccessBidingNotification, ()=>{
			this.requestSceneInfo()
		})
		if(this.sceneId){
			this.requestSceneInfo()
        }
        //添加快捷指令成功回调
        if(Platform.OS == 'ios'){
            this.shortcutsNoti = ShortCutManager.addListener(NotificationKeys.kRNAddSiriSuccess,(data)=>{
                if(data == 1){
                    ToastManager.show('添加快捷成功')
                }else if(data == 2){
                    ToastManager.show('快捷已更新')
                }else if(data == 3){
                    ToastManager.show('删除快捷成功')
                }
            })
        }
	}

	componentWillUnmount() {
        this.switchBindingNoti.remove()
        if(Platform.OS == 'ios'){
            this.shortcutsNoti.remove()
        }
    }
    
	//获取场景信息
	async requestSceneInfo(){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.sceneInfo,
				params: {
					id :this.sceneId
				}
            });
            SpinnerManager.close()
			if (data && data.code == 0) {
                let result = data.result || {}
				this.setState({
                    isCollect: result.isCollect,
					sceneName: result.name,
					timerCount: result.timerCount,
					deviceCount: result.deviceCount,
                    labelCount: result.labelCount,
                    thumb: {uri: result.dayIconOn},
                    thumb_dark: {uri: result.nightIconOn},
                    switchbinding: result.switchbinding || 0,
                    intelligentCount: result.intelligentCount || 0,
                    floor: result.floorValue,
                    floorText: result.floorText,
                    roomName: result.roomName,
                    istop: result.istop,
                    mistakeLock: result.mistakeLock,
                    shareInfo: result.shareInfo
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	// 保存按钮点击
	async save(params, callBack) {
        params = params || {}
		if(this.sceneId){
			params.id = this.sceneId
		}
		if(this.roomId){
			params.roomId = this.roomId
		}
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.addScene,
				params: params
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                if(params.imgId || params.name){
                    DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
                }
                callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	// 删除场景
	async deleteScene() {
		const {pop} = this.props.navigation
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.delScene,
				params: {
					id: this.sceneId
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
				DeviceEventEmitter.emit(NotificationKeys.kSceneFloorNotification)
				pop()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }
    
    //收藏场景
    async collect(){
        const { setParams } = this.props.navigation;
        let target = !this.state.isCollect
        try {
			let data = await postJson({
				url: NetUrls.collect,
				params: {
                    ids: this.sceneId,
                    type: 2,
                    isAdd: target
                }
            });
			if (data.code == 0) {
                let tips = target ? '收藏成功' : '取消收藏'
                this.state.isCollect = target
                setParams({isCollect: target})
                ToastManager.show(tips)
                DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    async setTopScene(target) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.setTopScene,
				params: {
					id: this.sceneId,
					istop: target,
					type: 1
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                this.setState({
                    istop: target
                })
				DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误');
		}
    }

    share(){
        const { navigate } = this.props.navigation
        let thumbImg = this.props.themeInfo.isDark ? this.state.thumb_dark : this.state.thumb

        navigate('Setting_Share',{
            title: '场景分享',
            id: this.sceneId,
            sceneData: {sceneId: this.sceneId, icon: thumbImg, name: this.state.sceneName},
            shareData: this.state.shareInfo,
            shareType: 2,   // 1设备 2场景
        })
    }

	// 获取场景 图标
	getThumb() {
		const { navigate } = this.props.navigation
		const Colors = this.props.themeInfo.colors;

		let thumbImg = require('../../images/add_themeTextBlue.png')
		if(this.state.thumb){
			thumbImg = this.props.themeInfo.isDark ? this.state.thumb_dark : this.state.thumb
        }
        
		return (
			<View style={{ marginTop: 10, alignItems: 'center' }}>
				<TouchableOpacity
					style={[styles.addThumbTouch,{backgroundColor : Colors.themeBg}]}
					onPress={() => {
                        if(this.props.userInfo.memberType == 2){
                            return
                        }
						navigate('SelectThumb', { thumbClass: 2, callBack: (data) => {
                            this.save({groupName: data.groupName,imgId: data.idIconOn}, ()=>{
                                this.setState({ 
                                    thumb: {uri:data.dayUrlIconOn},
                                    thumb_dark: {uri:data.nightUrlIconOn}
                                })
                            })
							ToastManager.show('图标更新成功')
						} })
					}}
				>
					<Image style={styles.iconImg} source={thumbImg} />
				</TouchableOpacity>
			</View>
		)
	}

    // 场景名称
	getInput() {
        const { navigate, pop } = this.props.navigation
        const Colors = this.props.themeInfo.colors
        const isJuniorUser = this.props.userInfo.memberType == 2
        
		return (
            <TouchableOpacity
                activeOpacity={0.7}
                style={[styles.inputWrapper, {backgroundColor: Colors.themeBg}]} 
                onPress={()=>{
                    if(isJuniorUser){
                        return
                    }
                    navigate('ReName',{title:'修改名称',name:this.state.sceneName, callBack:(name)=>{
                        this.save({name: name}, ()=>{
                            this.setState({
                                sceneName: name
                            })
                            ToastManager.show('名称修改成功')
                            pop()
                        })
                    }})
                }}
            >
				<Text style={[styles.enterTitle, {color: Colors.themeText}]}>场景名称</Text>
                <Text style={{ fontSize: 15, color: Colors.themeTextLight }}>{this.state.sceneName}</Text>
				{isJuniorUser ? null : <Image style={styles.enterArrow} source={require('../../images/enterLight.png')} />}
			</TouchableOpacity>
		)
    }

    // 设置别名
	getOtherName() {
        const { navigate, pop } = this.props.navigation
        const Colors = this.props.themeInfo.colors;
        
		return (
            <TouchableOpacity
                activeOpacity={0.7}
                style={[styles.inputWrapper, {backgroundColor: Colors.themeBg}]} 
                onPress={()=>{
                    navigate('SettingOtherName',{
                        title: '场景别名',
                        id: this.sceneId,
                        type: 2,
                        callBack:()=>{
                            this.requestSceneInfo()
                        }}
                    )
                }}
            >
                {/* <Image style={styles.itemIcon} source={require('../../images/sceneIcon/other_name.png')}/> */}
				<Text style={[styles.enterTitle, {color: Colors.themeText}]}>场景别名</Text>
                <Text style={{ fontSize: 15, color: Colors.themeTextLight }}>{this.state.labelCount}个</Text>
				<Image style={styles.enterArrow} source={require('../../images/enterLight.png')} />
			</TouchableOpacity>
		)
    }
    
    // 设备组合
    getDevices(){
        const { navigate } = this.props.navigation
        const Colors = this.props.themeInfo.colors;
        
        return(
            <TouchableOpacity
                activeOpacity={0.7}
                style={[styles.inputWrapper, { backgroundColor: Colors.themeBg}]} 
                onPress={()=>{
                    navigate('DeviceCombination',{
                        roomId: this.roomId,
                        sceneId: this.sceneId,
                        floor: this.state.floor,
                        floorText: this.state.floorText,
                        roomName: this.state.roomName,
                        isJuniorUser: this.props.userInfo.memberType == 2,
                        callBack:()=>{
                            this.requestSceneInfo()
                        }}
                    )
                }}
            >
				<Text style={[styles.enterTitle, {color: Colors.themeText}]}>设备组合</Text>
				<Text style={{ fontSize: 15, color: Colors.themeTextLight }}>{this.state.deviceCount}个</Text>
				<Image style={styles.enterArrow} source={require('../../images/enterLight.png')} />
			</TouchableOpacity>
        )
    }

    // 智能停启用
    getSmart(){
        const { navigate } = this.props.navigation
        const Colors = this.props.themeInfo.colors;
        
        return(
            <TouchableOpacity
                activeOpacity={0.7}
                style={[styles.inputWrapper, { backgroundColor: Colors.themeBg}]} 
                onPress={()=>{
                    navigate('SenceSmartList',{
                        title: '停启用智能',
                        sceneId: this.sceneId,
                        callBack:()=>{
                            this.requestSceneInfo()
                        }
                    })
                }}
            >
				<Text style={[styles.enterTitle, {color: Colors.themeText}]}>停启用智能</Text>
				<Text style={{ fontSize: 15, color: Colors.themeTextLight }}>{this.state.intelligentCount}个</Text>
				<Image style={styles.enterArrow} source={require('../../images/enterLight.png')} />
			</TouchableOpacity>
        )
    }

    // 定时
	getTiming() {
		const { navigate } = this.props.navigation
		const Colors = this.props.themeInfo.colors;
        
		return (
            <TouchableOpacity
                activeOpacity={0.7}
                style={[styles.inputWrapper, {backgroundColor: Colors.themeBg}]} 
                onPress={()=>{
                    navigate('Setting_Timing',{
                        sceneId: this.sceneId,
                        isJuniorUser: this.props.userInfo.memberType == 2,
                        callBack: ()=>{
                            this.requestSceneInfo()
                        }}
                    )
                }}
            >
				<Text style={[styles.enterTitle, {color: Colors.themeText}]}>定时</Text>
                <Text style={{ fontSize: 15, color: Colors.themeTextLight }}>{this.state.timerCount}个</Text>
				<Image style={styles.enterArrow} source={require('../../images/enterLight.png')} />
			</TouchableOpacity>
		)
    }

    renderQuestionBtn(){
        return(
            <TouchableOpacity style={{height:'100%',paddingHorizontal:12,justifyContent:'center',alignItems:'center'}} onPress={()=>{
                Alert.alert(
                    '说明',
                    '防误触开关作用于手动执行该场景时，是否需要出现弹框来确认执行。',
                    [
                        {text: '知道了', onPress: () => {}, style: 'cancel'}
                    ]
                )
            }}>
                <Image style={{width: 16, height:16}} source={require('../../images/sceneIcon/question.png')}/>
            </TouchableOpacity>
        )
    }

    // 误触锁
    getMistakeLock(){
        const Colors = this.props.themeInfo.colors;

        return(
            <View style={[styles.inputWrapper, { backgroundColor: Colors.themeBg, paddingRight: 0}]}>
				<Text style={{ fontSize: 15, color: Colors.themeText }}>防误触</Text>
                {this.renderQuestionBtn()}
                <View style={{flex: 1}}/>
                <SwitchButton
                    value = {this.state.mistakeLock}
                    isAsync = {true}
                    onPress = {()=>{
                        let target = this.state.mistakeLock ? false : true
                        this.save({mistakeLock: target}, ()=>{
                            this.setState({
                                mistakeLock: target
                            })
                            DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
                        })
                    }}
                />
			</View>
        )
    }
    
    // 置顶
    getSetingTop(){
        const Colors = this.props.themeInfo.colors;
        
        return(
            <View style={[styles.inputWrapper, { backgroundColor: Colors.themeBg, paddingRight: 0}]}>
				<Text style={{ fontSize: 15, color: Colors.themeText }}>场景置顶</Text>
                <View style={{flex: 1}}/>
                <SwitchButton
                    value = {this.state.istop}
                    isAsync = {true}
                    onPress = {()=>{
                        let target = this.state.istop ? 0 : 1
					    this.setTopScene(target)
                    }}
                />
			</View>
        )
    }
    
    // 日志
	getLog() {
		if (!this.sceneId) {
			return null
		}
		const { navigate } = this.props.navigation
		const Colors = this.props.themeInfo.colors;
        
		return (
            <TouchableOpacity
                activeOpacity={0.7}
                style={[styles.inputWrapper, {backgroundColor: Colors.themeBg,paddingRight: 16}]} 
                onPress={()=>{
                    navigate('Setting_Log',{sceneId: this.sceneId })
                }}
            >
				<Text style={[styles.enterTitle, {color: Colors.themeText}]}>日志</Text>
				<Image style={styles.enterArrow} source={require('../../images/enterLight.png')} />
			</TouchableOpacity>
		)
    }

    // 开关绑定
    getSwitchBinding(){
        const { navigate } = this.props.navigation
		const Colors = this.props.themeInfo.colors;
        
        return(
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={[styles.inputWrapper, { backgroundColor: Colors.themeBg,paddingRight:16 }]} 
                onPress={()=>{
                    navigate('SwitchList',{sceneId: this.sceneId, deviceParams: null})
                }}
            >
                <Text style={[styles.enterTitle, {color: Colors.themeText}]}>开关绑定</Text>
                <Text style={{ fontSize: 15, color: Colors.themeTextLight }}>{this.state.switchbinding}个</Text>
                <Image style={styles.enterArrow} source={require('../../images/enterLight.png')} />
            </TouchableOpacity>
        )
    }
    
    // Siri
    getAddSiriBtn(){
        if(Platform.OS == 'android' || !this.sceneId || !this.state.deviceCount){
            return
		}
		const Colors = this.props.themeInfo.colors;
        
        return (
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={[styles.inputWrapper, { backgroundColor: Colors.themeBg,paddingRight:16 }]} 
                onPress={()=>{
                    JKRNUtils.addSceneToSiriShortCut(this.sceneId,this.state.sceneName)
                }}
            >
                <Text style={[styles.enterTitle, {color: Colors.themeText}]}>添加Siri快捷</Text>
                <Image style={styles.enterArrow} source={require('../../images/enterLight.png')} />
            </TouchableOpacity>
		)
    }

	getBottomBtn() {
		const Colors = this.props.themeInfo.colors;

		if (this.sceneId) {
			return (
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.bottomBtnWrapper, { backgroundColor: Colors.themeBg,paddingRight:16 }]} 
                    onPress={()=>{
                        Alert.alert(
                            '提示',
                            '确认删除该场景?',
                            [
                                {text: '取消', onPress: () => {}, style: 'cancel'},
                                {text: '删除', onPress: () => { this.deleteScene() }},
                            ]
                        )
                    }}
                >
                    <Text style={{ color: Colors.red,fontSize:15 }}>删除</Text>
                </TouchableOpacity>
			)
		}
    }

    renderSceneSettings() {
		if(!this.sceneId){
            return null
        }
        const isJuniorUser = this.props.userInfo.memberType == 2
		return (
			<View style={{ paddingVertical: 5, paddingHorizontal: 16 }}>
                {this.getInput()}
                {this.getDevices()}
                {this.getTiming()}
				{isJuniorUser ? null : this.getSwitchBinding()}
                {isJuniorUser ? null : this.getSmart()}
                {isJuniorUser ? null : this.getOtherName()}
                {this.getMistakeLock()}
				{this.getSetingTop()}
				{this.getLog()}
                {this.getAddSiriBtn()}
                {isJuniorUser ? null : this.getBottomBtn()}
			</View>
		)
    }
    
	render() {
		const Colors = this.props.themeInfo.colors;
		
		return (
            <ScrollView 
                style={{backgroundColor: Colors.themeBaseBg}} 
                contentContainerStyle={styles.scrollContainStyle}
            >
				{this.getThumb()}
				{this.renderSceneSettings()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	scrollContainStyle: {
		paddingBottom: BottomSafeMargin + 20
    },
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight: 5
	},
    navImg: {
        width: 22,
        height: 22
    },
	inputWrapper: {
		width: '100%',
		height: 55,
		borderRadius: 5,
		alignItems: 'center',
        flexDirection: 'row',
        marginTop: 10,
        paddingHorizontal: 16
	},
	floorTouch: {
		flexDirection: 'row',
		justifyContent: 'flex-end',
		alignItems: 'center',
		height: '90%',
		flex: 1,
		paddingRight: 10
	},
	deleteTouch: {
		width: '100%',
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 4
	},
	bottomBtnWrapper: {
        marginTop: 50,
        justifyContent:'center',
        alignItems: 'center',
        height: 45,
        borderRadius: 4
	},
	addThumbTouch: {
		width: 84,
		height: 84,
		borderRadius: 4,
		alignItems: 'center',
		justifyContent: 'center'
	},
	input: {
		flex: 1,
		height: 40,
		marginLeft: 5,
		marginRight: 10,
		textAlign: 'right',
		paddingRight: 10
	},
	enterTitle:{ 
		fontSize: 15, 
		flex:1, 
	},
	enterArrow:{ 
		marginLeft: 10, 
		width: 7, 
		height: 13, 
		marginRight: 10 
    },
    iconImg:{ 
        width: '60%', 
        height: '60%', 
        resizeMode: 'contain' 
    },
    itemIcon:{
        width: 20,
        height:20,
        resizeMode: 'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
        userInfo: state.userInfo
    })
)(SceneSetting)
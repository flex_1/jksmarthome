/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StatusBar,
    View,
    DeviceEventEmitter,
    Modal,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    ScrollView,
    Alert,
    NativeModules
} from 'react-native';
import { postJson, } from '../../util/ApiRequest';
import { NetUrls, LocalStorageKeys } from '../../common/Constants';
import { Colors } from '../../common/Constants';
import { connect } from 'react-redux';
import { StatusBarHeight, NavigationBarHeight, BottomSafeMargin } from '../../util/ScreenUtil';
import {ActivityIndicator} from '@ant-design/react-native';
import Toast from "react-native-easy-toast";
import SwitchButton from '../../common/CustomComponent/SwitchButton';

class VoiceProblemFeedbackModal extends Component {

    constructor(props) {
        super(props);

        
        
        this.state = {
            
        }
    }

    componentWillMount(){
        
    }

    componentDidMount() {
        
        
    }

    componentWillUnmount() {
        
    }

    render() {

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                <TouchableOpacity style={{flex: 4,backgroundColor:'rgba(0,0,0,0.6)'}}>

                </TouchableOpacity>
                <View style={{flex: 6,backgroundColor: '#fff'}}>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
    }
})

export default VoiceProblemFeedbackModal

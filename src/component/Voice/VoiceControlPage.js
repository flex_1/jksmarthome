/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StatusBar,
    View,
    DeviceEventEmitter,
    Modal,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    Dimensions,
    BackHandler,
    ImageBackground,
    ScrollView,
    Keyboard,
    TextInput,
    NativeModules
} from 'react-native';
import { postJson,postPicture } from '../../util/ApiRequest';
import { NetUrls, LocalStorageKeys } from '../../common/Constants';
import { Colors } from '../../common/Constants';
import ToastManager from '../../common/CustomComponent/ToastManager';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import { StatusBarHeight, NavigationBarHeight, BottomSafeMargin } from '../../util/ScreenUtil';
import { AudioRecorder, AudioUtils } from 'react-native-audio';
import Sound from 'react-native-sound';
import LocalTokenHandler from '../../util/LocalTokenHandler';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import { connect } from 'react-redux';
import VoiceSettingModal from './VoiceSettingModal';
import VoiceQuestion from './VoiceQuestion';
import VoiceProblemFeedbackModal from './VoiceProblemFeedbackModal';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height
const bottomControlHeight = 100

// 录音 路径
let audioPath = AudioUtils.DocumentDirectoryPath + '/test3.aac';
// const JKVoiceUtils = NativeModules.JKVoiceUtils;

class VoiceControlPage extends Component {

    static navigationOptions = {
		header: null,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    }
    
    constructor(props) {
        super(props);
        this.isAnimateView = props.isAnimateView || false

        this.state = {
            voiceData: [],
            userId: null,

            currentTime: 0,//
            duration: 0,//总时长
            recording: false,//是否录音
            
            stoppedRecording: false,
            finished: false,
            play: false,
            audioPath: AudioUtils.DocumentDirectoryPath + this.getAacName(),//生成的录音
            hasPermission: true,

            showKeybord:false,
            keyboardHeight:-500,
            inputString: '',

            floor: null,
            roomNames: [],
            roomIds: [],
            isBroadcast: 0,
            isWakeUp: 0,
            isPlaying: false,
            
            settingModal: false,
            questionModal: false,
            problemModal: false
        }
    }

    componentWillMount(){
        // if(!this.isAnimateView && !this.props.themeInfo.isDark){
        //     StatusBar.setBarStyle('light-content')
        // }
        // 获取用户id
        this.getUserId()

        // 获取 楼层 房间 信息
        // this.requestCurrentRooms()
        // this.requestSpeechSetting()

        // 关闭语音唤醒与语音识别
        // JKVoiceUtils.stopWake()
        // JKVoiceUtils.stopRecongize()
        this.voiceAnimateLifeSetting()
    }

    componentDidMount() {
        // this.audioInit()
    }

    componentWillUnmount() {
        AudioRecorder && AudioRecorder.removeListeners()
        // if(this.state.isWakeUp){
        //     JKVoiceUtils.settingUp()
        // }
        // if(!this.isAnimateView && !this.props.themeInfo.isDark){
        //     StatusBar.setBarStyle('dark-content')
        // }
        this.whoosh && this.whoosh.release()
        this.voicePageOpened && this.voicePageOpened.remove()
        this.voicePageClosed && this.voicePageClosed.remove()
    }

    voiceAnimateLifeSetting(){
        this.voicePageOpened = DeviceEventEmitter.addListener('kVoiceAnimateViewOpened',()=>{
            // 页面被打开
            // 关闭语音唤醒与语音识别(小萨语音助手相关) 
            // JKVoiceUtils.stopWake()
            // JKVoiceUtils.stopRecongize()

            this.requestCurrentRooms()
            this.requestSpeechSetting()

            this.audioInit()
        })
        this.voicePageClosed = DeviceEventEmitter.addListener('kVoiceAnimateViewClosed',()=>{
            //页面被关闭
            // (小萨语音助手相关) 
            // if(this.state.isWakeUp){
            //     JKVoiceUtils.settingUp()
            // }
            this.setState({
                voiceData: [],
                isPlaying: false
            })
            AudioRecorder.removeListeners()
            this.whoosh && this.whoosh.release()
        })
    }

    // 初始化 录音器
    audioInit(){
        AudioRecorder.requestAuthorization().then((isAuthorised) => {
            this.setState({ hasPermission: isAuthorised });

            if (!isAuthorised) return;

            this.prepareRecordingPath();

            AudioRecorder.onProgress = (data) => {
                this.setState({ currentTime: Math.floor(data.currentTime) });
            };

            AudioRecorder.onFinished = (data) => {
                console.log("data" + JSON.stringify(data));
                this.setState({ 
                    finished: true
                })
                this.uploadVoice(data.audioFileURL)
            };
        });
    }

    async getUserId(){
        let tokens = await LocalTokenHandler.get()
        this.setState({
            userId: tokens.id
        })
    }

    // 获取控制房间
    async requestCurrentRooms() {
		try {
			let data = await postJson({
				url: NetUrls.speechControllArea,
				params: {}
			});
			
			if (data && data.code == 0) {
                let roomIds = []
                let roomNames = []
                if(data.result.areaIds){
                    roomIds = data.result.areaIds.split(',')
                }
                if(data.result.areaNames){
                    roomNames = data.result.areaNames.split(',')
                }
                this.setState({
                    floor: data.result.floorName,
                    floorText: data.result.floorText,
                    roomNames: roomNames,
                    roomIds: roomIds
                })
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 语音设置
    async requestSpeechSetting() {
		try {
			let data = await postJson({
				url: NetUrls.getSpeechSetting,
				params: {}
			});
			if (data && data.code == 0) {
                this.setState({
                    isBroadcast: data.result.isSpeechBroadcast,
                    isWakeUp: data.result.isWakeUp
                })
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 上传语音
	async uploadVoice(path) {
        let params = {}
        if(this.state.inputString){
            params.text = this.state.inputString
        }
        if(!this.state.userId){
            ToastManager.show('获取用户id失败')
            return
        }
        if(this.state.floor){
            params.floor = this.state.floor
        }
        if(this.state.roomNames && this.state.roomNames.length>0){
            params.rooms = this.state.roomNames.toString()
        }
        SpinnerManager.show()
		try {
			let data = await postPicture({
                url: NetUrls.uploadVoice,
                timeout: 30,    // 30秒如果没有响应 则显示超时
				filePath: path,
				fileName: 'voice.aac',
				params: {
                    ...params,
                    broadcastSwitch: this.state.isBroadcast,
                    accountId: this.state.userId,
                }
            })
            SpinnerManager.close()
			if (data && data.code == 0) {
                let res = data.result
                let voiceData = JSON.parse(JSON.stringify(this.state.voiceData))
                voiceData.push(res)
                
                // 播报语音
                if(res.broadcast_url){
                    this._play(res.broadcast_url)
                }
                this.setState({
                    voiceData: voiceData,
                    inputString: ''
                },()=>{
                    setTimeout(() => {
                        this.content_scroll.scrollToEnd()
                    }, 200);

                    // 识别意图
                    setTimeout(()=>{
                        this.recognizeIntent(res.intent)
                    }, 500)
                })
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error));
		}
	}

    // 键盘弹出
    _keyboardDidShow(e){
        let keyboardHeight = e.endCoordinates.height
        this.setState({
            keyboardHeight: keyboardHeight
        })
    }
    // 键盘隐藏
    _keyboardDidHide(e){
        
    }

    // 录音准备
    prepareRecordingPath() {
        AudioRecorder.prepareRecordingAtPath(this.state.audioPath, {
            SampleRate: 22050,
            Channels: 1,
            AudioQuality: "Medium",
            AudioEncoding: "aac",
            AudioEncodingBitRate: 32000
        });
    }

    // 开始录音
    async _record() {
        if (!this.state.hasPermission) {
            ToastManager.show("无法录音，请授予权限");
            return;
        }
        if(this.state.recording){
            ToastManager.show("正在录音中");
            return;
        }
        if (this.state.stoppedRecording) {
            this.prepareRecordingPath(this.state.audioPath);
        }
        this.setState({ recording: true, play: false });
        try {
            await AudioRecorder.startRecording()
        } catch (error) {
            ToastManager.show('录音错误')
        }
    }

    // 停止 录音
    async _stop() {
        this.setState({
            currentTime: 0,
            stoppedRecording: true,
            finished: true,
            recording: false,
            // duration: this.sound.getDuration()
        });
        try {
            await AudioRecorder.stopRecording();
        } catch (error) {
            ToastManager.show('录音错误，或录音时间过短')
        }
    }

    // 播放录音
    async _play(voiceUrl) {
        let whoosh = new Sound(voiceUrl, '', (err) => {
            this.setState({isPlaying: true})
            if (err) {
                ToastManager.show('播放失败')
                return
            }
            whoosh.play(success => {
                if (success) {
                    console.log('success - 播放成功')
                } else {
                    ToastManager.show('播放失败')
                    return
                }
                whoosh.release();
                this.setState({isPlaying: false})
            })
        })
        this.whoosh = whoosh
    }

    // 识别意图
    recognizeIntent(intent){
        if(!intent) return

        // const {pop, navigate} = this.props.navigation

        if(intent == 'CHANGE_ROOM'){  
            this.requestCurrentRooms()
        }
        // else if(intent == 'DEVICE_PAGE'){
        //     pop()
        //     navigate('Device',{title:'全部设备'})
        // }else if(intent == 'SCENE_PAGE'){
        //     pop()
        //     navigate('Scene',{title:'全部场景'})
        // }else if(intent == 'DEVICE_MANAGE'){
        //     pop()
        //     navigate('DeviceManage')
        // }else if(intent == 'MEMBER_MANAGE'){
        //     pop()
        //     navigate('MemberLoginRecord')
        // }
    }

    sendMessage(){
        if(!this.state.inputString){
            ToastManager.show('指令不能为空')
            return
        }
        Keyboard.dismiss()
        this.setState({showKeybord: false})
        // 释放录音
        this.whoosh && this.whoosh.release()
        this.setState({isPlaying: false})
        this.uploadVoice()
    }

    gotoSetting(){
        if(this.isAnimateView){
            this.setState({
                settingModal: true
            })
            return
        }
        const {navigate} = this.props.navigation
        navigate('VoiceSetting',{
            floor: this.state.floor,
            floorText: this.state.floorText,
            roomNames: this.state.roomNames,
            roomIds: this.state.roomIds,
            isBroadcast: this.state.isBroadcast,
            callBack: ()=>{ this.requestCurrentRooms() },
            broadCastCallBack: (isSpeechBroadcast, isWakeUp)=>{ 
                this.setState({
                    isBroadcast: isSpeechBroadcast,
                    isWakeUp: isWakeUp
                })
            }
        })
    }

    // 获取录音文件名
    getAacName() {
        return "/test.aac";
    }

    // 获取 头部
    getHeaderView() {
        return (
            <View style={styles.header}>
                <View style={styles.status}/>
                <View style={styles.nav}>
                    <TouchableOpacity style={styles.backTouch} onPress={() => {
                        if(this.isAnimateView){
                            this.props.close()
                        }else{
                            const {pop} = this.props.navigation
                            pop()
                        }
                    }}>
                        <Image style={{ width: 10, height: 18 }} source={require('../../images/back_white.png')} />
                        <Text style={styles.navText}>语音控制</Text>
                    </TouchableOpacity>
                    <View style={{flex: 1}}/>
                    
                    <TouchableOpacity style={styles.playButton} onPress={() => {
                        this.gotoSetting()
                    }}>
                        <Text style={styles.settingText}>设置</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    // 当前房屋
    renderTipsHeader(){
        let areaText = '整屋'
        if(this.state.floor){
            areaText = this.state.floorText
        }
        if(this.state.roomNames && this.state.roomNames.length > 0){
            areaText += '（ ' + this.state.roomNames + ' ）'
        }
        return(
            <View style={styles.tipsWrapper}>
                <Text style={styles.tipsText}>按住话筒说话，说完后松开即可!</Text>
                <TouchableOpacity activeOpacity={0.7} style={styles.areaTextTouch} onPress={()=>{
                    this.gotoSetting()
                }}>
                    <Text numberOfLines={2} style={styles.areaText}>当前控制区域:  {areaText}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    _getProblemButton(){
        return(
            <TouchableOpacity onPress={()=>{
                this.setState({
                    problemModal: true
                })
            }}>
                <Text style={{color: '#fff',fontSize: 16}}>问题反馈</Text>
            </TouchableOpacity>
        )
    }

    _getStopButton(index){
        if(!this.state.isBroadcast){
            return null
        }
        if(this.state.voiceData.length - 1 != index){
            return null
        }
        if(!this.state.isPlaying){
            return null
        }

        return(
            <TouchableOpacity style={styles.stopTouch} onPress={()=>{
                this.whoosh && this.whoosh.release()
                this.setState({isPlaying: false})
            }}>
                <Image style={styles.stopImg} source={require('../../images/voice/stop.png')}/>
                <Text style={styles.stopText}>停止播放</Text>
            </TouchableOpacity>
        )
    }

    // 获取内容
    getContentView() {
        let list = this.state.voiceData.map((val, index) => {
            let line = <View style={{height:1,backgroundColor: Colors.themeTextBlack,width:'100%',marginBottom:20}}/>
            if(index == 0){
                line = null
            }
            return (
                <View key={'voice_list_' + index} style={{ marginBottom: 30 }}>
                    {/* {line} */}
                    <TouchableOpacity
                        style={{paddingHorizontal: 16}}
                        activeOpacity={0.7} 
                        onPress={()=>{
                            this.setState({
                                showKeybord: true,
                                inputString: val.input
                            })
                        }}
                    >
                        <Text style={styles.question}>{val.input}</Text>
                        <View style={{flexDirection:'row',marginTop: 6, alignItems:'center'}}>
                            <Text style={styles.clickText}>轻点以编辑</Text>
                            <Image style={styles.clickImg} source={require('../../images/voice/arrow.png')}/>
                        </View>
                    </TouchableOpacity>
                    <Text style={styles.answer}>{val.say}</Text>
                    {this._getStopButton(index)}
                    {/* {this._getProblemButton(val)} */}
                </View>
            )
        })
        return (
            <ScrollView
                ref={e => this.content_scroll = e}
                style={styles.scroll} 
                contentContainerStyle={styles.scrollContent}
            >
                {list}
            </ScrollView>
        )
    }

    // 获取底部按钮
    getBottomControl() {
        return (
            <View style={styles.bottomWrapper}>
                <TouchableOpacity style={{ padding: 15 }} onPress={() => {
                    this.setState({
                        showKeybord: true,
                        inputString: ''
                    })
                }}>
                    <Image style={{ height: 24, width: 24, resizeMode:'contain' }} source={require('../../images/voice/keybord.png')} />
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={{ marginHorizontal: '15%',padding:10 }}
                    onPress={() => {
                        this.whoosh && this.whoosh.release() 
                        this.setState({isPlaying: false})

                        if (!this.state.hasPermission) {
                            ToastManager.show("无法录音，请授予权限");
                            return;
                        }
                    }}
                    onPressIn={() => {
                        this.whoosh && this.whoosh.release()
                        this.setState({isPlaying: false})

                        if (!this.state.hasPermission) {
                            return;
                        }
                        this._record()
                    }}
                    onPressOut={() => {
                        if (!this.state.hasPermission) {
                            return;
                        }
                        this._stop()
                    }}
                >
                    <Image style={{ height: 70, width: 70}}  source={require('../../images/bottom_tab/center.png')} />
                </TouchableOpacity>
                <TouchableOpacity style={{ padding: 15 }} onPress={() => {
                    if(this.isAnimateView){
                        this.setState({questionModal: true})
                    }else{
                        const {navigate} = this.props.navigation
                        navigate('VoiceQuestion')
                    }
                }}>
                    <Image style={{ height: 24, width: 24,resizeMode:'contain' }} source={require('../../images/voice/question.png')} />
                </TouchableOpacity>
            </View>
        )
    }

    // 输入框
    getKeybordView(){
        if(!this.state.showKeybord){
            return null
        }
        return(
            <TouchableOpacity activeOpacity={1} style={styles.newBgWrapper} onPress={()=>{
                this.setState({
                    showKeybord: false
                })
                Keyboard.dismiss()
            }}>
                <Text style={styles.newInputTips}>您可以通过编辑文字来发送命令</Text>
                <View style={styles.newInputWrapper}>
                    <TextInput
                        value={this.state.inputString}
                        style={styles.input}
                        blurOnSubmit={false}
                        autoFocus={true}
                        returnKeyLabel={'发送'}
                        returnKeyType={'send'}
                        underlineColorAndroid="transparent"
                        onChangeText={(text)=>{
                            this.setState({inputString:text})
                        }}
                        onSubmitEditing={()=>{
                            this.sendMessage()
                        }}
                    />
                </View>
            </TouchableOpacity>
        )
    }

    renderCenterAnimation(){
        if(!this.state.recording){
            return null
        }
        return(
            <View style={styles.centerAnimate}>
                <Image style={styles.animateGif} source={require('../../images/voice/voice.gif')}/>
                <Text style={styles.centerAnimateText}>正在聆听...</Text>
            </View>
        )
    }

    // 设置页面
    renderSettingModal(){
        return(
            <Modal
                style={{ flex: 1 }}
                animationType="slide"
                transparent={true}
                visible={this.state.settingModal}
                onShow={() => {
                    StatusBar.setBarStyle('dark-content')
                }}
                onRequestClose={() => {
                    
                }}
            >
                <VoiceSettingModal
                    floor={this.state.floor}
                    roomNames={this.state.roomNames}
                    roomIds={this.state.roomIds}
                    isBroadcast={this.state.isBroadcast}
                    broadCastCallBack={(isSpeechBroadcast, isWakeUp)=>{
                        this.setState({
                            isBroadcast: isSpeechBroadcast,
                            isWakeUp: isWakeUp
                        })
                    }}
                    callBack={()=>{ this.requestCurrentRooms() }} 
                    close={()=>{
                        StatusBar.setBarStyle('light-content')
                        this.setState({settingModal: false})
                    }}
                />
            </Modal>
        )
    }

    // 帮助页面
    renderQuestionModal(){
        return(
            <Modal
                style={{ flex: 1 }}
                animationType="fade"
                transparent={true}
                visible={this.state.questionModal}
                onShow={() => {
                    
                }}
                onRequestClose={() => {

                }}
            >
                <VoiceQuestion 
                    isAnimateView={this.isAnimateView} 
                    close={()=>{
                        this.setState({questionModal: false})
                    }}
                />
            </Modal>
        )
    }

    renderProblemFeedbackModal(){
        return(
            <Modal
                style={{ flex: 1 }}
                animationType="slider"
                transparent={true}
                visible={this.state.problemModal}
                onShow={() => {
                    
                }}
                onRequestClose={() => {

                }}
            >
                <VoiceProblemFeedbackModal 
                    close={()=>{
                        this.setState({questionModal: false})
                    }}
                />
            </Modal>
        )
    }

    render() {
        return (
            <ImageBackground style={styles.container} source={require('../../images/voice/voiceBg.png')}>
                {this.getHeaderView()}
                {this.renderTipsHeader()}
                {this.getContentView()}
                {this.getBottomControl()}
                {this.getKeybordView()}
                {this.renderCenterAnimation()}
                {this.renderSettingModal()}
                {this.renderQuestionModal()}
                {/* {this.renderProblemFeedbackModal()} */}
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: 'rgba(35, 16, 47, 1)',
    },
    header: {
        width: '100%',
        height: StatusBarHeight + NavigationBarHeight
    },
    status: {
        height: StatusBarHeight,
        width: '100%',
    },
    nav: {
        height: NavigationBarHeight,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    navText:{ 
        marginLeft: 15, 
        fontSize: 24, 
        color: Colors.white, 
        fontWeight: 'bold' 
    },
    backTouch: {
        height: '100%',
        marginLeft: 16,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    scroll:{
        marginTop:10,
        flex:1,
    },
    scrollContent: {
        paddingBottom: 0
    },
    bottomWrapper: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        height: BottomSafeMargin + bottomControlHeight,
        paddingBottom: BottomSafeMargin
    },
    question: {
        fontSize: 16,
        color: Colors.white,
        fontWeight: 'bold'
    },
    clickText:{
        fontSize: 12,
        color: Colors.themeTextLightGray
    },
    clickImg:{
        width: 14,
        height: 14,
        resizeMode:'contain',
        marginLeft: 5
    },
    answer: {
        marginTop: 10,
        fontSize: 14,
        color: Colors.white,
        lineHeight: 20,
        marginHorizontal: 16
    },
    settingText:{
        color: Colors.white,
        fontSize: 16,
        fontWeight: 'bold'
    },
    playButton:{ 
        paddingHorizontal: 20,
        justifyContent:'center',
        alignItems:'center',
        height: '100%'
    },
    inputWraper:{
        backgroundColor:Colors.white,
        borderColor:Colors.borderLightGray,
        borderWidth:1,
        borderRadius:4,
    },
    input: {
        height:50,
        paddingHorizontal:10,
        color:Colors.themeTextBlack,
        fontSize: 18,
    },
    centerAnimate:{
        position: 'absolute', 
        top: 0, 
        left: 0, 
        width: screenW, 
        height: screenH, 
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.8)',
        borderRadius: 4
    },
    animateGif:{ 
        height: screenW/2 , 
        width: screenW/2
    },
    centerAnimateText:{
        fontSize: 16,
        color: Colors.white,
        marginTop: 10,
        fontWeight: 'bold'
    },
    tipsWrapper:{
        paddingHorizontal: 16,
        paddingTop: 10,
        paddingBottom: 15
    },
    tipsText:{
        color: Colors.white,
        fontSize: 16,
        fontWeight: 'bold'
    },
    areaTextTouch:{
        marginTop: 5,
        paddingVertical: 5
    },
    areaText: {
        color: Colors.themeTextBlue,
        fontSize: 14,
        lineHeight: 20
    },
    newBgWrapper:{
        height:'100%', 
        width:screenW,
        alignItems:'center',
        position:'absolute',
        zIndex:10,
        backgroundColor:'rgba(0,0,0,0.8)'
    },
    newInputWrapper:{
        marginTop:30,
        backgroundColor: 'rgba(255,255,255,1)',                       
        borderRadius:4,
        width:'90%'
    },
    newInputTips:{
        color: 'white',
        marginTop: StatusBarHeight + NavigationBarHeight+ 80
    },
    quesInput:{
        backgroundColor: Colors.white,
        zIndex:100,
        width:'100%',
        position:'absolute',
        padding:10
    },
    quesWrapper:{
        height:'100%', 
        width:screenW,
        position:'absolute',
        zIndex:10
    },
    stopTouch:{
        alignItems:'center',
        paddingVertical:10,
        marginTop: 5, 
        flexDirection:'row',
        paddingHorizontal: 16
    },
    stopImg:{
        width: 20, 
        height:20, 
        resizeMode:'contain'
    },
    stopText:{
        color: Colors.themBgRed,
        fontSize:14,
        marginLeft: 5
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(VoiceControlPage) 

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StatusBar,
    View,
    DeviceEventEmitter,
    Modal,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    Dimensions,
    BackHandler,
    ImageBackground,
    ScrollView
} from 'react-native';
import { postJson, } from '../../util/ApiRequest';
import { NetUrls, NotificationKeys } from '../../common/Constants';
import { Colors } from '../../common/Constants';
import { StatusBarHeight, NavigationBarHeight } from '../../util/ScreenUtil';
import { TextInput } from 'react-native-gesture-handler';


class VoiceQuestion extends Component {

    static navigationOptions = {
		header: null,
    }
    
    constructor(props) {
        super(props);
        this.isAnimateView = props.isAnimateView || false
        this.close = props.close

        this.state = {
            pageType: 1, // 1您可以这样问我  2详情
            pageDetailTitle: '',
            questionData: [
                { icon: require('../../images/deviceIcon/device_2.png'), title: '电视',content:'打开1楼客厅电视' },
                { icon: require('../../images/deviceIcon/air_condition.png'), title: '空调',content:'打开2楼卧室空调' },
                { icon: require('../../images/deviceIcon/device_1.png'), title: '窗帘',content:'打开3楼客厅窗帘' },
            ],
            detailData:[
                '打开所有',
                '打开1楼客厅',
                '关闭所有',
                '关闭1楼客厅',
                '打开2楼卧室和大厅',
            ]
        }
    }

    // 获取 头部
    getHeaderView() {
        const statusView = Platform.OS == 'ios' ? <View style={styles.status}/> : null

        return (
            <View style={styles.header}>
                {statusView}
                <View style={styles.nav}>
                    <TouchableOpacity style={styles.backTouch} onPress={() => {
                        if(this.isAnimateView){
                            this.close && this.close()
                        }else{
                            const {pop} = this.props.navigation
                            pop()
                        }
                    }}>
                        <Image style={{ width: 10, height: 18 }} source={require('../../images/back_white.png')} />
                        <Text style={{ marginLeft: 15, fontSize: 24, color: Colors.white, fontWeight: 'bold' }}>返回</Text>
                    </TouchableOpacity>
                    
                </View>
            </View>
        )
    }

    // 获取内容
    getContentView() {
        if(this.state.pageType != 1){
            return null
        }
        let list = this.state.questionData.map((val, index) => {
            return (
                <TouchableOpacity 
                    key={'voice_list_' + index} 
                    style={{ marginTop: 30,flexDirection:'row',alignItems:'center' }}
                    onPress={()=>{
                        this.setState({
                            pageDetailTitle: val.title,
                            pageType: 2
                        })
                    }}
                >
                    <Image style={{width:36,height:30,resizeMode:'contain'}} source={val.icon}/>
                    <View style={{marginLeft: 15,justifyContent:'center',flex:1}}>
                        <Text style={styles.title}>{val.title}</Text>
                        <Text style={styles.subtitile}>{val.content}</Text>
                    </View>
                    <Image style={{width:7,height:13,resizeMode:'contain'}} source={require('../../images/enter_white.png')}/>
                </TouchableOpacity>
            )
        })

        return (
            <ScrollView contentContainerStyle={styles.scrollContent}>
                <Text style={{fontSize:24,color:Colors.white,fontWeight:'bold',marginTop:10 }}>您可以这样问我：</Text>
                {list}
                {/* <Text style={styles.tipsText}>注：此页面是通过手动录音来进行语音控制,暂不支持语音唤醒小萨功能。</Text> */}
            </ScrollView>
        )
    }

    getDetailContentView(){
        if(this.state.pageType != 2){
            return null
        }
        let list = this.state.detailData.map((val,index)=>{
            return(
                <View key={'detail_list_' + index} style={{marginBottom: 15}}>
                    <Text style={styles.details}>{val+this.state.pageDetailTitle}</Text>
                </View>
            )
        })
        return(
            <ScrollView contentContainerStyle={styles.scrollContent}>
                <Text style={{fontSize:24,color:Colors.white,fontWeight:'bold',marginTop:10 }}>{this.state.pageDetailTitle}</Text>
                <View style={styles.line}/>
                {list}
            </ScrollView>
        )
    }

    render() {
        return (
            <ImageBackground style={styles.container} source={require('../../images/voice/voiceBg.png')}>
                {this.getHeaderView()}
                {this.getContentView()}
                {this.getDetailContentView()}
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#191837'
    },
    header: {
        width: '100%'
    },
    status: {
        height: StatusBarHeight,
        width: '100%',
    },
    nav: {
        height: NavigationBarHeight,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    backTouch: {
        height: '100%',
        marginLeft: 16,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    scrollContent: {
        paddingBottom: 50,
        paddingHorizontal: 16,
        paddingTop: 10
    },
    title: {
        fontSize: 15,
        fontWeight: 'bold',
        color: Colors.white
    },
    subtitile: {
        marginTop: 5,
        fontSize: 13,
        color: Colors.white,
        opacity: 0.6
    },
    playButton:{ 
        borderRadius: 15,
        width:60,
        height:30,
        backgroundColor:Colors.themeBG,
        justifyContent:'center',
        alignItems:'center',
        marginRight:20
    },
    line:{
        height:1,
        width:'100%',
        backgroundColor:Colors.white,
        opacity: 0.4,
        marginVertical: 20,
    },
    details:{
        fontSize: 15,
        color: Colors.white,
        opacity: 0.6
    },
    tipsText:{
        fontSize: 16,
        color: Colors.themeTipsYellow,
        marginTop: 50,
        lineHeight: 22
    }
})

export default VoiceQuestion

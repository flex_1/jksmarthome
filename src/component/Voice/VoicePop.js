/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    View,
    DeviceEventEmitter,
    Modal,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    Dimensions,
    BackHandler,
    ImageBackground,
    ScrollView,
    NativeModules,
    NativeEventEmitter,
    PermissionsAndroid
} from 'react-native';
import { postJson } from '../../util/ApiRequest';
import { NetUrls, NetParams, NotificationKeys} from '../../common/Constants';
import { Colors } from '../../common/Constants';
import { connect } from 'react-redux';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import AsyncStorage from '@react-native-community/async-storage';
import LocalTokenHandler from '../../util/LocalTokenHandler';
import Sound from 'react-native-sound';

const JKVoiceUtils = NativeModules.JKVoiceUtils;
const VoiceManager = new NativeEventEmitter(JKVoiceUtils)

//语音唤醒的 通知字段
const kVocieWakeUpNotification = 'VocieWakeUpNotification'
//语音识别的 文字
const kVocieSendTextNotification = 'VocieSendTextNotification'
//错误提示
const kVocieErrorTextNotification = 'VocieErrorTextNotification'

// 弹框关闭时间
const shutDownVoicePopTime = 3 * 1000

//提示框的Key
const PromptBeenShownKey = 'VoicePromptHaveBeenShown'


class VoicePop extends Component {

    constructor(props) {
        super(props);
        
        this.voicePlayer = null
        this.helloPlayer = null
        this.state = {
            showText: '',
            status: 0, //0-关闭 1-正在听 2-识别中 3-正在分析(识别完成) 4-小萨在说 5-连续识别中
            userId: 0,
            isBroadcast: false,
            helloSays: null,
            // showPrompt: false,
            isPlaying: false
        }
    }

    componentWillMount(){
        // this.getPromptInfo()
    }

    componentDidMount() {
        this.getUserId()
        this.requestSpeechSetting(true)

        // 语音实时状态反馈
        this.voiceListener = VoiceManager.addListener(kVocieWakeUpNotification,(status)=>{
            if( (this.state.status == 1 || this.state.status == 2 || this.state.status == 5) && status == 1){
                return
            }
            if(status == 1){
                this.voicePlayer && this.voicePlayer.release()
                this.helloPlayer && this.helloPlayer.release()

                if(this.state.status){
                    this.setState({
                        showText: ''
                    })
                }
                // 可以播报 "我在！"
                if(this.state.isBroadcast){
                    this.sayHello()
                }else{
                    JKVoiceUtils.startRecord()
                }
            }
            this.setState({
                status: status
            })
            // if(this.state.showPrompt && status==1){
            //     this.setState({showPrompt: false})
            //     AsyncStorage.setItem(PromptBeenShownKey, "1")
            // }
        })

        // 语音解析结果
        this.recognizeListener = VoiceManager.addListener(kVocieSendTextNotification,(data)=>{            
            let message = {}
            if(Platform.OS == 'android'){
                message = {message: '', isFinished: false}
                try {
                    let wordsDict = JSON.parse(data)
                    message = {message: wordsDict.results_recognition, isFinished: wordsDict.isFinished}
                } catch (error) {
                    message = {message: '数据解析有误，请稍后重试', isFinished: false}
                }
            }else{
                message = data
            }
            this.setState({
                showText: message.message
            },()=>{
                if(message.isFinished){
                    this.uploadVoice()
                }
            })
        })
        // 错误信息
        this.errorListener = VoiceManager.addListener(kVocieErrorTextNotification,(content)=>{
            // ToastManager.show(content)
        })
        this.voiceSettingListener = DeviceEventEmitter.addListener('VoiceSettingChanges',()=>{
            this.shutdownVoicePop()
            this.requestSpeechSetting(false)
        })
    }

    componentWillUnmount() {
        JKVoiceUtils.stopRecongize()
        JKVoiceUtils.stopWake()

        this.voiceListener && this.voiceListener.remove()
        this.recognizeListener && this.recognizeListener.remove()
        this.errorListener && this.errorListener.remove()
        
        this.voiceSettingListener.remove()

        this.voicePlayer && this.voicePlayer.release()
        this.helloPlayer && this.helloPlayer.release()
    }

    async getUserId(){
        let tokens = await LocalTokenHandler.get()
        this.setState({
            userId: tokens.id
        })
    }

    // 新功能提示框
    // async getPromptInfo(){
    //     let promptInfo = await AsyncStorage.getItem(PromptBeenShownKey)
    //     if(promptInfo != "1"){
    //         this.setState({
    //             showPrompt: true
    //         })
    //     }
    // }

    // 语音设置
    async requestSpeechSetting(isInitial) {
		try {
			let data = await postJson({
				url: NetUrls.getSpeechSetting,
				params: {}
			});
			if (data && data.code == 0) {
                this.setState({
                    isBroadcast: data.result.isSpeechBroadcast,
                    helloSays: data.result.broadcastPath
                })
                if(data.result.isWakeUp && isInitial){
                    if(Platform.OS == 'android'){
                        PermissionsAndroid.request(
                            PermissionsAndroid.PERMISSIONS.RECORD_AUDIO
                        ).then(result => {
                            if (result == PermissionsAndroid.RESULTS.GRANTED || result == true){
                                JKVoiceUtils.settingUp()
                            }  
                        })
                    }else{
                        JKVoiceUtils.settingUp()
                    }
                }
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络超时')
		}
    }

    async uploadVoice() {
		try {
			let data = await postJson({
                url: NetUrls.jsonSpeechControl,
                timeout: NetParams.networkUserReactTimeoutTime,
				params: {
                    text: this.state.showText.toString(),
                    accountId: this.state.userId
				}
			});
			if (data && data.code == 0) {
                if(this.state.status == 0){
                    return
                }
                // { input: '关闭射灯。', say: '没有找到可用设备！', intent: 'DEV_ACTION' }
                let res = data.result
                this.setState({
                    status: 4,
                    showText: res.say
                },()=>{
                    if(res.broadcast_url){
                        this.playVoice(res.broadcast_url)
                    }else{
                        this.setState({status: 5})
                        JKVoiceUtils.startRecord()
                    }
                })
			}else{
                this.shutdownVoicePop()
                ToastManager.show(data.msg)
			}
		} catch (error) {
            this.shutdownVoicePop()
            ToastManager.show('网络状态不佳。')
		}
    }

    //生成从minNum到maxNum的随机数
    randomNum(minNum,maxNum){ 
        switch(arguments.length){ 
            case 1: 
                return parseInt(Math.random()*minNum+1,10); 
            break; 
            case 2: 
                return parseInt(Math.random()*(maxNum-minNum+1)+minNum,10); 
            break; 
            default: 
                return 0; 
            break; 
        } 
    }

    //小萨回答 我在!
    async sayHello(){
        if(!this.state.helloSays || this.state.helloSays.length <= 0){
            return
        }
        let length = this.state.helloSays.length
        let index = this.randomNum(0, length-1)
        let helloUrl = this.state.helloSays[index]

        let helloPlayer = new Sound(helloUrl, '', (err) => {
            if (err) {
                JKVoiceUtils.startRecord()
                helloPlayer.release();
                return
            }
            Sound.setCategory('Playback')
            helloPlayer.play(success => {
                Sound.setCategory('PlayAndRecord')
                JKVoiceUtils.startRecord()
                helloPlayer.release();
            })
        })
        this.helloPlayer = helloPlayer
    }

    // 播放录音
    async playVoice(voiceUrl) {
        this.voicePlayer && this.voicePlayer.release()

        let voicePlayer = new Sound(voiceUrl, '', (err) => {
            if (err) {
                this.setState({
                    status: 5
                })
                JKVoiceUtils.startRecord()
                return
            }
            Sound.setCategory('PlayAndRecord')
            voicePlayer.play(success => {
                // this.shutdownVoicePop()
                // Sound.setCategory('PlayAndRecord')
                JKVoiceUtils.startRecord()
                this.setState({
                    status: 5
                })
                voicePlayer.release();
            })
        })
        this.voicePlayer = voicePlayer
    }

    // 现已隐藏
    renderPrompt(){
        return(
            <View style={styles.prompt}>
                <Text style={{color: Colors.newTheme,fontSize: 18,fontWeight:'bold',textAlign:'center'}}>新功能提示</Text>
                <ScrollView style={{paddingHorizontal: 16,marginTop:10}}>
                    <Text style={styles.promptProduceText}>小萨管家新增 语音唤醒 功能:</Text>
                    <Text style={styles.promptProduceText}>1.当您对着手机说:“小萨小萨”，界面会弹出语音卡片。</Text>
                    <Text style={styles.promptProduceText}>2.说出您想要的操作，小萨会自动响应。</Text>
                    <Text style={styles.promptWarnText}>特别提醒: </Text>
                    <Text style={styles.promptWarnText}>1.请确保您已开启麦克风权限。</Text>
                    <Text style={styles.promptWarnText}>2.请确保网络畅通。</Text>
                    <Text style={styles.promptWarnText}>3.请确保语音唤醒功能开启。</Text>
                    <Text style={styles.promptWarnText}>4.如果你想关闭此功能，可以点击正下方话筒按钮，然后点击进入设置页面。</Text>
                </ScrollView>
                <View style={{flexDirection:'row',paddingVertical:5,justifyContent:'space-between',paddingHorizontal:16}}>
                    <TouchableOpacity style={styles.promptBtn} onPress={()=>{
                        AsyncStorage.setItem(PromptBeenShownKey, "1")
                        this.setState({
                            showPrompt: false
                        })
                        DeviceEventEmitter.emit(NotificationKeys.kEnterVoiceSetting)
                    }}>
                        <Text style={{color: Colors.white, fontSize: 16, fontWeight:'bold'}}>去设置</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.promptBtn} onPress={()=>{
                        AsyncStorage.setItem(PromptBeenShownKey, "1")
                        this.setState({
                            showPrompt: false
                        })
                    }}>
                        <Text style={{color: Colors.white, fontSize: 16, fontWeight:'bold'}}>知道啦</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    // 关闭对话框
    shutdownVoicePop(){
        JKVoiceUtils.pauseRecongize()
        this.helloPlayer && this.helloPlayer.release()
        this.voicePlayer && this.voicePlayer.release()
        this.setState({ status: 0 })
        if(this.state.isBroadcast){
            Sound.setCategory('PlayAndRecord')
        }     
    }

    renderTop(){
        let statusText = '小萨正在听'
        if(this.state.status == 3){
            statusText = '分析中...'
        }else if(this.state.status == 4){
            statusText = '小萨的答复'
        }
        return(
            <View style={{flexDirection:'row',alignItems:'center',paddingLeft: 16, height: 25}}>
                <Image style={{height:20,width:20,resizeMode:'contain'}} source={require('../../images/bottom_tab/xiaosa.png')}/>
                <Text style={{fontSize: 14, color: Colors.white, marginLeft: 10}}>{statusText}</Text>
                <View style={{flex: 1}}/>
                <TouchableOpacity style={{paddingLeft: 16, paddingRight: 10,height:'100%',justifyContent:'center'}} onPress={()=>{
                    DeviceEventEmitter.emit(NotificationKeys.kEnterVoiceSetting)
                }}>
                    <Text style={{fontSize: 14, color: Colors.white}}>设置</Text>
                </TouchableOpacity>
                <View style={{height: 10,width:1, backgroundColor: Colors.white}}/>
                <TouchableOpacity style={{paddingLeft: 10, paddingRight: 16,height:'100%',justifyContent:'center'}} onPress={()=>{
                    this.shutdownVoicePop()
                }}>
                    <Image style={{width: 18,height: 18}} source={require('../../images/close_w.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    renderContent(){
        if(this.state.status == 1){
            return(
                <View style={styles.contentWrapper}>
                    <Text style={styles.contentTips}>你可以说</Text>
                    <Text style={styles.content}>“打开卧室空调”</Text>
                </View>
            )
        }
        return (
            <View style={styles.contentWrapper}>
                <Text numberOfLines={4} style={styles.content}>{this.state.showText}</Text>
            </View>
        )
    }

    render() {
        // if(this.state.showPrompt){
        //     return(
        //         <View style={styles.promptWrapper}>
        //             {this.renderPrompt()}
        //         </View>
        //     )
        // }

        if(!this.state.status || this.state.status == 0){
            return null
        }
        
        return (
            <View style={styles.container}>
                {this.renderTop()}
                {this.renderContent()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    promptWrapper:{
        position:'absolute',
        top: 0,
        left: 0,
        width:'100%',
        height:'100%',
        backgroundColor: Colors.translucent,
        justifyContent: 'center',
        alignItems: 'center'
    },
    prompt:{
        backgroundColor: Colors.white, 
        width:'80%',
        height:'50%',
        borderRadius:8,
        paddingVertical:15
    },
    promptBtn:{
        width:'48%',
        height:45,
        backgroundColor:Colors.newTheme,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:5
    },
    promptProduceText:{
        color: Colors.newTheme,
        fontSize: 15,
        fontWeight:'bold',
        marginTop:10
    },
    promptWarnText:{
        color: Colors.themeTextBlack,
        fontSize: 15,
        fontWeight:'bold',
        marginTop:10
    },
    container: {
        position: 'absolute',
        flex:1,
        bottom: 100,
        left: 20,
        right: 20,
        borderRadius: 5,
        backgroundColor: Colors.newTheme,
        paddingTop: 5,
        paddingBottom: 16
    },
    contentWrapper:{
        marginTop: 10,
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal: 20
    },
    contentTips:{
        fontSize: 14,
        color: Colors.white,
    },
    content:{
        fontSize: 18,
        lineHeight: 22,
        fontWeight: 'bold',
        color: Colors.white,
        marginTop: 5
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(VoicePop)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StatusBar,
    View,
    DeviceEventEmitter,
    Modal,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    ScrollView,
    Alert,
    NativeModules,
    BackHandler
} from 'react-native';
import { postJson, } from '../../util/ApiRequest';
import { NetUrls, LocalStorageKeys } from '../../common/Constants';
import { Colors } from '../../common/Constants';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";

const JKVoiceUtils = NativeModules.JKVoiceUtils;

class VoiceSetting extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} onClick={()=>{
            if(navigation.getParam('backBtnClick')){
                navigation.getParam('backBtnClick')()
            }else{
                navigation.goBack()
            }
        }}/>,
        headerTitle: <HeaderTitle title={'语音设置'}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text:'保存', onPress:()=>{
                    navigation.getParam('saveBtn')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })
    
    constructor(props) {
        super(props);
        const { getParam,setParams } = this.props.navigation
        
        this.fromVoicePop = getParam('fromVoicePop')
        this.callBack = getParam('callBack')
        this.broadCastCallBack = getParam('broadCastCallBack')
        this.state = {
            floorList: [{floor: null}],
            roomList: null,
            floor: '',
            
            roomNames: [],
            roomIds: [],
            
            selectIndex: null,
            showSelector: false,
            isBroadcast: true,
            isWakeUp: true,

            isBackToRoot: false
        }

        setParams({
            saveBtn: this.requestSaveRoomSetting.bind(this),
            backBtnClick: this.backBtnClick.bind(this)
        })
    }

    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", ()=>{
            this.backBtnClick()
            return true
        });
    }

    componentDidMount() {
        this.requestSpeechSetting()
        this.requestCurrentRooms()
        if(!this.props.themeInfo.isDark && !this.fromVoicePop){
            StatusBar.setBarStyle('dark-content')
        }
    }

    componentWillUnmount() {
        if(!this.props.themeInfo.isDark && !this.fromVoicePop && !this.state.isBackToRoot){
            StatusBar.setBarStyle('light-content')
        }
    }

    // 返回按钮点击事件
    backBtnClick(){
        const {pop, popToTop} = this.props.navigation
        if(this.state.isBackToRoot){
            popToTop()
        }else{
            pop()
        }
    }

    // 语音设置
    async requestSpeechSetting() {
		try {
			let data = await postJson({
				url: NetUrls.getSpeechSetting,
				params: {}
			});
			if (data && data.code == 0) {
                this.setState({
                    isBroadcast: data.result.isSpeechBroadcast,
                    isWakeUp: data.result.isWakeUp
                })
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 获取控制房间
    async requestCurrentRooms() {
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.speechControllArea,
				params: {}
			});
			
			if (data && data.code == 0) {
                let roomIds = []
                let roomNames = []
                if(data.result.areaIds){
                    roomIds = data.result.areaIds.split(',')
                }
                if(data.result.areaNames){
                    roomNames = data.result.areaNames.split(',')
                }
                this.setState({
                    floor: data.result.floorName,
                    roomNames: roomNames,
                    roomIds: roomIds
                },()=>{
                    this.requestRoomList()
                })
			}else{
                SpinnerManager.close()
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 获取房间 列表
    async requestRoomList() {
		try {
			let data = await postJson({
				url: NetUrls.roomList,
				params: {
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                let dataList =  data.result || []
                dataList.unshift({floor: null})

                let rooms = null
                for (const data of dataList) {
                    if(data.floor != null && data.floor == this.state.floor ){
                        rooms = data.rooms
                    }
                }
                this.setState({
                    floorList: dataList,
                    roomList: rooms
                })
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 保存房间设置
    async requestSaveRoomSetting() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.saveVoiceRooms,
				params: {
                    speechRoomIds: this.state.roomIds.toString(),
                    floor: this.state.floor
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                const { pop } = this.props.navigation
                this.callBack && this.callBack()
                pop()
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 开启关闭 语音功能
    async requestBroadcastSwitch() {
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.setBroadcast,
				params: {
                    isSpeechBroadcast: this.state.isBroadcast,
                    isWakeUp: this.state.isWakeUp
				}
            });
            SpinnerManager.close()
			if (data && data.code == 0) {
                this.broadCastCallBack && this.broadCastCallBack(this.state.isBroadcast, this.state.isWakeUp)
                DeviceEventEmitter.emit('VoiceSettingChanges')
                if(this.state.isWakeUp){
                    JKVoiceUtils.settingUp()
                    this.state.isBackToRoot = true
                }else{
                    JKVoiceUtils.stopWake()
                    JKVoiceUtils.stopRecongize()
                    this.state.isBackToRoot = false
                }
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }
    
    renderExpand(){
        if(!this.state.showSelector){
            return null
        }
        
        const Colors = this.props.themeInfo.colors
        let floors = this.state.floorList.map((value, index)=>{
            
            let touchStyle = value.floor == this.state.floor ? {borderWidth: 1, borderColor: Colors.themeButton} : {}
            let textColor =  value.floor == this.state.floor ?  Colors.themeButton : Colors.themeText
            let checkIcon =  value.floor == this.state.floor ?  <Image style={styles.checkIcon} source={require('../../images/voice/check.png')}/> : null

            let floorText = value.floor == null ? '整屋' : value.floor + '楼'

            return (
                <View key={index} style={styles.floorsWrapper}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={[styles.floorBtn,{backgroundColor:Colors.themeBaseBg},touchStyle]} 
                        onPress={()=>{
                            console.log('sss  ' + this.state.roomIds.toString());
                            this.setState({
                                floor: value.floor,
                                selectIndex: index,
                                showSelector: false,
                                roomList: value.rooms,
                                roomIds: [],
                                roomNames: []
                            })
                        }}
                    >
                        <Text style={{fontSize: 14,color: textColor}}>{floorText}</Text>
                        {checkIcon}
                    </TouchableOpacity>
                </View>
            )
        })
        return(
            <View style={[styles.btnWrapper,{backgroundColor: Colors.themeBg}]}>
                {floors}
            </View>
        )
    }

    // 语音答复
    renderBroadSwitch(){
        const Colors = this.props.themeInfo.colors
        let icon = this.state.isBroadcast ? require('../../images/switch_on.png')
        : require('../../images/switch_off.png')

        return(
            <View style={[styles.itemWrapper,{backgroundColor: Colors.themeBg}]}>
                <View style={styles.switchWrapper}>
                    <Text style={[styles.title,{color: Colors.themeText}]}>语音答复</Text>
                    <View style={{flex: 1}}/>
                    <TouchableOpacity style={styles.switchTouch} onPress={()=>{
                        let target = this.state.isBroadcast ? 0:1
                        this.setState({
                            isBroadcast: target
                        },()=>{
                            this.requestBroadcastSwitch()
                        })
                    }}>
                        <Image style={styles.switchImg} source={icon}/>
                    </TouchableOpacity>
                </View>
                <Text style={[styles.subDetail,{color: Colors.themeTextLight}]}>小萨收到语音指令后，是否用语音的形式播报给您的答复。</Text>
            </View>
        )
    }

    renderQuestionBtn(){
        return(
            <TouchableOpacity style={styles.questionBtn} onPress={()=>{
                Alert.alert(
                    '怎样使用语音唤醒',
                    '1.开启麦克风权限，并打开语音唤醒开关\r\n2.直接对着手机说:“小萨小萨”或"你好小萨"或"小萨管家"\r\n3.语音卡片弹出后，说出您想要的操作',
                    [
                        {text: '知道了', onPress: () => {}, style: 'cancel'}
                    ]
                )
            }}>
                <Image style={{width: 16, height:16}} source={require('../../images/sceneIcon/question.png')}/>
            </TouchableOpacity>
        )
    }

    // 语音唤醒
    renderWakeUpSwitch(){
        const Colors = this.props.themeInfo.colors
        let icon = this.state.isWakeUp ? require('../../images/switch_on.png')
        : require('../../images/switch_off.png')

        return(
            <View style={[styles.itemWrapper,{backgroundColor: Colors.themeBg}]}>
                <View style={styles.switchWrapper}>
                    <Text style={[styles.title,{color: Colors.themeText}]}>语音唤醒功能</Text>
                    {this.renderQuestionBtn()}
                    <View style={{flex: 1}}/>
                    <TouchableOpacity style={styles.switchTouch} onPress={()=>{
                        let target = this.state.isWakeUp ? 0:1
                        this.setState({
                            isWakeUp: target
                        },()=>{
                            this.requestBroadcastSwitch()
                        })
                    }}>
                        <Image style={styles.switchImg} source={icon}/>
                    </TouchableOpacity>
                </View>
                <Text style={[styles.subDetail,{color: Colors.themeTextLight}]}>是否随时响应“小萨小萨”等语音交互命令。</Text>
            </View>
        )
    }

    renderFloorSelector(){
        const Colors = this.props.themeInfo.colors

        let floor = this.state.floor ? this.state.floor + '楼' : '整屋' 
        
        return(
            <View style={styles.titleWrapper}>
                <Text style={[styles.title,{color: Colors.themeText}]}>请选择楼层</Text>
                <TouchableOpacity
                    activeOpacity={0.7} 
                    style={[styles.floorTouch,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        this.setState({
                            showSelector: !this.state.showSelector
                        })
                    }}
                >
                    <Image style={styles.floorImg} source={require('../../images/voice/floor.png')}/>
                    <Text style={[styles.floorText,{color: Colors.themeText}]}>{floor}</Text>
                    <View style={{flex: 1}}/>
                    <Image style={[styles.arrowDown,{tintColor: Colors.themeTextLight}]} source={require('../../images/down.png')}/>
                </TouchableOpacity>
                {this.renderExpand()}
            </View>
        )
    }

    renderRooms(){
        if(!this.state.roomList){
            return null
        }
        const Colors = this.props.themeInfo.colors
        let rooms = this.state.roomList.map((value,index)=>{
            let s_index = this.state.roomIds.indexOf(value.id)

            let touchStyle = s_index == -1 ? {} : {borderWidth: 1, borderColor: Colors.themeButton}
            let textColor =  s_index == -1 ? Colors.themeText : Colors.themeButton
            let checkIcon =  s_index == -1 ? null : <Image style={styles.checkIcon} source={require('../../images/voice/check.png')}/>
            
            return(
                <View key={index} style={styles.floorsWrapper}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={[styles.floorBtn,{backgroundColor: Colors.themeBg},touchStyle]} 
                        onPress={()=>{
                            if(s_index == -1){
                                this.state.roomIds.push(value.id)
                                this.state.roomNames.push(value.name)
                            }else{
                                this.state.roomIds.splice(s_index, 1)
                                this.state.roomNames.splice(s_index, 1)
                            }
                            this.setState({
                                ...this.state
                            })
                        }}
                    >
                        <Text style={{fontSize: 13,color: textColor,textAlign:'center'}}>{value.name}</Text>
                        {checkIcon}
                    </TouchableOpacity>
                </View>
            )
        })
        return(
            <View style={[styles.btnWrapper,{paddingHorizontal: 0, flex: 1}]}>
                {rooms}
            </View>  
        )
    }

    renderRoomSelector(){
        if(this.state.showSelector){
            return null
        }
        if(!this.state.roomList || !this.state.roomList.length){
            return null
        }
        const Colors = this.props.themeInfo.colors
        return(
            <View style={[styles.titleWrapper,{flex: 1}]}>
                <Text style={[styles.title,{color: Colors.themeText}]}>请选择房间</Text>
                {this.renderRooms()}
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors
        return (
            <ScrollView style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderBroadSwitch()}
                {this.renderWakeUpSwitch()}
                {this.renderFloorSelector()}
                {this.renderRoomSelector()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
    },
    title:{
        fontSize: 18,
        fontWeight: 'bold',
    },
    itemWrapper:{
        paddingLeft: 16,
        marginTop: 10,
        paddingVertical: 10
    },
    switchWrapper:{
        flexDirection: 'row',
        alignItems: 'center'
    },
    titleWrapper:{
        marginTop: 20,
        marginBottom: 10,
        paddingHorizontal: 16
    },
    floorTouch:{
        marginTop: 15,
        width: '100%',
        height: 40,
        borderRadius: 4,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 16
    },
    floorImg:{
        width: 26,
        height: 19,
        resizeMode: 'contain'
    },
    floorText:{
        fontSize: 16,
        marginLeft: 15
    },
    arrowDown:{
       width: 14,
       height: 7,
       resizeMode: 'contain' 
    },
    expandWrapper:{
        marginTop: 5,
        width: '100%',
        height: 1
    },
    floorsWrapper:{
        width: '33%',
        height: 50, 
        justifyContent:'center',
        alignItems:'center',
        padding: 5
    },
    floorBtn:{
        width:'100%',
        height:'100%',
        justifyContent:'center',
        alignItems:'center',
        borderRadius: 4,
        overflow: 'hidden'
    },
    btnWrapper:{
        width:'100%',
        flexDirection:'row',
        flexWrap:'wrap',
        marginTop: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    checkIcon: {
        position: 'absolute',
        top: -2,
        right: -2,
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    switchTouch:{
        paddingHorizontal: 20,
        height: '100%',
        alignItems: 'center'
    },
    switchImg:{
        width:50,
        height:40,
        resizeMode:'contain'
    },
    subDetail:{
        fontSize: 13,
        marginTop: 5
    },
    questionBtn:{
        paddingHorizontal:15,
        height: 40,
        justifyContent:'center',
        alignItems:'center'
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(VoiceSetting)

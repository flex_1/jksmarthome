/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Dimensions,
    Animated,
    Easing,
    StatusBar,
    BackHandler
} from 'react-native';
import VoiceControlPage from './VoiceControlPage';

const {width, height} = Dimensions.get('window');

export default class VoiceAnimateView extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            leftMargin: new Animated.Value(width),
        }

        this.openAnimated = Animated.timing(
            this.state.leftMargin,
            {
                toValue: 0,
                duration: 250,   //动画时长
                easing: Easing.inOut(Easing.ease),
            }
        )

        this.closeAnimated = Animated.timing(
            this.state.leftMargin,
            {
                toValue: width,
                duration: 250,   //动画时长
                easing: Easing.inOut(Easing.ease),
            }
        )
    }

    // 开始动画
    _startAnimated() {
        this.openAnimated.start(() => {
            if(!this.props.isDark){
                StatusBar.setBarStyle('light-content')
            }
            
            if (Platform.OS === 'android'){
                BackHandler.addEventListener('hardwareBackPress', this.onBackAndroid);
            }
            DeviceEventEmitter.emit('kVoiceAnimateViewOpened')
        })
    }

    // 关闭动画
    _closeAnimated() {
        this.closeAnimated.start(() => {
            if(!this.props.isDark){
                StatusBar.setBarStyle('dark-content')
            }
            
            if (Platform.OS === 'android'){
                BackHandler.removeEventListener('hardwareBackPress', this.onBackAndroid);
            }
            DeviceEventEmitter.emit('kVoiceAnimateViewClosed')
        })
    }

    // 处理安卓返回按钮
    onBackAndroid = () => {
        this._closeAnimated()
        return true
    }

    render() {
        return (
            <Animated.View style={[styles.container,{left: this.state.leftMargin}]}>
                <VoiceControlPage
                    isAnimateView={true}
                    close = {()=>{
                        this._closeAnimated()
                    }}
                />
            </Animated.View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        position:'absolute',
        top: 0,
        height: '100%', 
        width: '100%',
        zIndex: 100
    },
})


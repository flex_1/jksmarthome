/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StatusBar,
    View,
    DeviceEventEmitter,
    Modal,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    ScrollView,
    Alert,
    NativeModules
} from 'react-native';
import { postJson, } from '../../util/ApiRequest';
import { NetUrls, LocalStorageKeys } from '../../common/Constants';
import { Colors } from '../../common/Constants';
import { connect } from 'react-redux';
import { StatusBarHeight, NavigationBarHeight, BottomSafeMargin } from '../../util/ScreenUtil';
import {ActivityIndicator} from '@ant-design/react-native';
import Toast from "react-native-easy-toast";
import SwitchButton from '../../common/CustomComponent/SwitchButton';

const JKVoiceUtils = NativeModules.JKVoiceUtils;

class VoiceSettingModal extends Component {

    constructor(props) {
        super(props);

        this.fromVoicePop = props.fromVoicePop
        this.callBack = props.callBack
        this.broadCastCallBack = props.broadCastCallBack
        this.close = props.close
        
        this.state = {
            floorList: [{floor: null}],
            roomList: null,
            floor: props.floor,
            floorText: props.floorText || '',
            
            roomNames: [],
            roomIds: [],
            
            selectIndex: null,
            showSelector: false,
            isBroadcast: true,
            isWakeUp: true,

            isBackToRoot: false,
            spinner: true
        }
    }

    componentWillMount(){
        
    }

    componentDidMount() {
        this.requestSpeechSetting()
        this.requestCurrentRooms()
        
    }

    componentWillUnmount() {
        
    }

    // 语音设置
    async requestSpeechSetting() {
		try {
			let data = await postJson({
				url: NetUrls.getSpeechSetting,
				params: {}
			});
			if (data && data.code == 0) {
                this.setState({
                    isBroadcast: data.result.isSpeechBroadcast,
                    isWakeUp: data.result.isWakeUp
                })
			}else{
                this.showToast(data.msg)
			}
		} catch (error) {
			this.showToast(JSON.stringify(error))
		}
    }

    // 获取控制房间
    async requestCurrentRooms() {
        this.showSpinner(true)
		try {
			let data = await postJson({
				url: NetUrls.speechControllArea,
				params: {}
			});
			
			if (data && data.code == 0) {
                let roomIds = []
                let roomNames = []
                if(data.result.areaIds){
                    roomIds = data.result.areaIds.split(',')
                }
                if(data.result.areaNames){
                    roomNames = data.result.areaNames.split(',')
                }
                this.setState({
                    floor: data.result.floorName,
                    floorText: data.result.floorText || '整屋',
                    roomNames: roomNames,
                    roomIds: roomIds
                },()=>{
                    this.requestRoomList()
                })
			}else{
                this.showSpinner(false)
				this.showToast(data.msg)
			}
		} catch (error) {
            this.showSpinner(false)
			this.showToast(JSON.stringify(error))
		}
    }

    // 获取房间 列表
    async requestRoomList() {
		try {
			let data = await postJson({
				url: NetUrls.roomList,
				params: {
                    type: 3
				}
			});
			this.showSpinner(false)
			if (data && data.code == 0) {
                let dataList =  data.result || []
                dataList.unshift({floor: null, floorText:'整屋',listRoom:[]})

                let rooms = null
                for (const floors of dataList) {
                    if(floors.floor != null && floors.floor == this.state.floor ){
                        rooms = floors.listRoom
                        break;
                    }
                }
                this.setState({
                    floorList: dataList,
                    roomList: rooms || []
                })
			}else{
				this.showToast(data.msg)
			}
		} catch (error) {
			this.showSpinner(false)
			this.showToast(JSON.stringify(error))
		}
    }

    // 保存房间设置
    async requestSaveRoomSetting() {
		this.showSpinner(true)
		try {
			let data = await postJson({
				url: NetUrls.saveVoiceRooms,
				params: {
                    speechRoomIds: this.state.roomIds.toString(),
                    floor: this.state.floor
				}
			});
			this.showSpinner(false)
			if (data && data.code == 0) {
                this.callBack && this.callBack()
                this.close && this.close()
			}else{
				this.showToast(data.msg)
			}
		} catch (error) {
			this.showSpinner(false)
			this.showToast(JSON.stringify(error))
		}
    }

    // 开启关闭 语音功能
    async requestBroadcastSwitch() {
        this.showSpinner(true)
		try {
			let data = await postJson({
				url: NetUrls.setBroadcast,
				params: {
                    isSpeechBroadcast: this.state.isBroadcast,
                    isWakeUp: this.state.isWakeUp
				}
            });
            this.showSpinner(false)
			if (data && data.code == 0) {
                this.broadCastCallBack && this.broadCastCallBack(this.state.isBroadcast, this.state.isWakeUp)
                DeviceEventEmitter.emit('VoiceSettingChanges')
                // (小萨语音助手相关) 
                // if(this.state.isWakeUp){
                //     JKVoiceUtils.settingUp()
                //     this.state.isBackToRoot = true
                // }else{
                //     JKVoiceUtils.stopWake()
                //     JKVoiceUtils.stopRecongize()
                //     this.state.isBackToRoot = false
                // }
			}else{
				this.showToast(data.msg)
			}
		} catch (error) {
            this.showSpinner(false)
			this.showToast(JSON.stringify(error))
		}
    }

    showSpinner(show){
        this.setState({spinner: show})
    }

    showToast(text){
        this.toast.show(text)
    }

    // 获取 头部
    getHeaderView() {
        const Colors = this.props.themeInfo.colors
        const statusView = Platform.OS == 'ios' ? <View style={styles.status}/> : null

        return (
            <View style={[styles.header,{backgroundColor: Colors.themeBg}]}>
                {statusView}
                <View style={styles.nav}>
                    <TouchableOpacity style={styles.backTouch} onPress={() => {
                        this.close && this.close()
                    }}>
                        <Image style={[styles.backImg,{tintColor: Colors.themeText}]} source={require('../../images/back_white.png')} />
                        <Text style={[styles.navText,{color: Colors.themeText}]}>语音设置</Text>
                    </TouchableOpacity>
                    <View style={{flex: 1}}/>
                    <TouchableOpacity style={styles.playButton} onPress={() => {
                        this.requestSaveRoomSetting()
                    }}>
                        <Text style={[styles.settingText,{color: Colors.themeText}]}>保存</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    
    // 语音答复
    renderBroadSwitch(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={[styles.itemWrapper,{backgroundColor: Colors.themeBg}]}>
                <View style={styles.switchWrapper}>
                    <Text style={[styles.title,{color: Colors.themeText}]}>语音答复</Text>
                    <View style={{flex: 1}}/>
                    <SwitchButton
                        style = {styles.switchTouch} 
                        value = {this.state.isBroadcast}
                        onPress = {()=>{
                            let target = this.state.isBroadcast ? 0:1
                            this.setState({
                                isBroadcast: target
                            },()=>{
                                this.requestBroadcastSwitch()
                            })
                        }}
                    />
                </View>
                <Text style={[styles.subDetail,{color: Colors.themeTextLight}]}>小萨收到语音指令后，是否用语音的形式播报给您的答复。</Text>
            </View>
        )
    }

    renderQuestionBtn(){
        return(
            <TouchableOpacity style={styles.questionBtn} onPress={()=>{
                Alert.alert(
                    '怎样使用语音唤醒',
                    '1.开启麦克风权限，并打开语音唤醒开关\r\n2.直接对着手机说:“小萨小萨”或"你好小萨"或"小萨管家"\r\n3.语音卡片弹出后，说出您想要的操作',
                    [
                        {text: '知道了', onPress: () => {}, style: 'cancel'}
                    ]
                )
            }}>
                <Image style={{width: 16, height:16}} source={require('../../images/sceneIcon/question.png')}/>
            </TouchableOpacity>
        )
    }

    // 语音唤醒
    renderWakeUpSwitch(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={[styles.itemWrapper,{backgroundColor: Colors.themeBg}]}>
                <View style={styles.switchWrapper}>
                    <Text style={[styles.title,{color: Colors.themeText}]}>语音唤醒功能</Text>
                    {this.renderQuestionBtn()}
                    <View style={{flex: 1}}/>
                    <SwitchButton
                        style = {styles.switchTouch} 
                        value = {this.state.isWakeUp}
                        onPress = {()=>{
                            let target = this.state.isWakeUp ? 0:1
                            this.setState({
                                isWakeUp: target
                            },()=>{
                                this.requestBroadcastSwitch()
                            })
                        }}
                    />
                </View>
                <Text style={[styles.subDetail,{color: Colors.themeTextLight}]}>是否随时响应“小萨小萨”等语音交互命令。</Text>
            </View>
        )
    }

    // 所有楼层 展开显示
    renderExpand(){
        if(!this.state.showSelector){
            return null
        }
        
        const Colors = this.props.themeInfo.colors
        let floors = this.state.floorList.map((value, index)=>{
            
            let touchStyle = value.floor == this.state.floor ? {borderWidth: 1, borderColor: Colors.themeButton} : {}
            let textColor =  value.floor == this.state.floor ?  Colors.themeButton : Colors.themeText
            let checkIcon =  value.floor == this.state.floor ?  <Image style={styles.checkIcon} source={require('../../images/voice/check.png')}/> : null

            let floorText = value.floor == null ? '整屋' : value.floorText

            return (
                <View key={index} style={styles.floorsWrapper}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={[styles.floorBtn,{backgroundColor:Colors.themeBaseBg},touchStyle]} 
                        onPress={()=>{
                            this.setState({
                                floor: value.floor,
                                floorText: value.floorText,
                                selectIndex: index,
                                showSelector: false,
                                roomList: value.listRoom,
                                roomIds: [],
                                roomNames: []
                            })
                        }}
                    >
                        <Text numberOfLines={2} style={{fontSize: 13,color: textColor,textAlign:'center'}}>{floorText}</Text>
                        {checkIcon}
                    </TouchableOpacity>
                </View>
            )
        })
        return(
            <View style={[styles.btnWrapper,{backgroundColor: Colors.themeBg}]}>
                {floors}
            </View>
        )
    }

    // 渲染楼层
    renderFloorSelector(){
        const Colors = this.props.themeInfo.colors
        
        return(
            <View style={styles.titleWrapper}>
                <Text style={[styles.title,{color: Colors.themeText}]}>请选择楼层</Text>
                <TouchableOpacity
                    activeOpacity={0.7} 
                    style={[styles.floorTouch,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        this.setState({
                            showSelector: !this.state.showSelector
                        })
                    }}
                >
                    <Image style={styles.floorImg} source={require('../../images/voice/floor.png')}/>
                    <Text style={[styles.floorText,{color: Colors.themeText}]}>{this.state.floorText }</Text>
                    <View style={{flex: 1}}/>
                    <Image style={[styles.arrowDown,{tintColor: Colors.themeTextLight}]} source={require('../../images/down.png')}/>
                </TouchableOpacity>
                {this.renderExpand()}
            </View>
        )
    }

    renderRooms(){
        if(!this.state.roomList){
            return null
        }
        const Colors = this.props.themeInfo.colors
        let rooms = this.state.roomList.map((value,index)=>{
            let s_index = this.state.roomIds.indexOf(value.id)

            let touchStyle = s_index == -1 ? {} : {borderWidth: 1, borderColor: Colors.themeButton}
            let textColor =  s_index == -1 ? Colors.themeText : Colors.themeButton
            let checkIcon =  s_index == -1 ? null : <Image style={styles.checkIcon} source={require('../../images/voice/check.png')}/>
            let fontW =  s_index == -1 ? 'normal' : 'bold'
            
            return(
                <View key={index} style={styles.floorsWrapper}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={[styles.floorBtn,{backgroundColor: Colors.themeBg},touchStyle]} 
                        onPress={()=>{
                            if(s_index == -1){
                                this.state.roomIds.push(value.id)
                                this.state.roomNames.push(value.name)
                            }else{
                                this.state.roomIds.splice(s_index, 1)
                                this.state.roomNames.splice(s_index, 1)
                            }
                            this.setState({
                                ...this.state
                            })
                        }}
                    >
                        <Text numberOfLines={2} style={{fontSize: 13,textAlign:'center',fontWeight: fontW,color: textColor}}>{value.name}</Text>
                        {checkIcon}
                    </TouchableOpacity>
                </View>
            )
        })
        return(
            <View style={[styles.btnWrapper,{paddingHorizontal: 0, flex: 1}]}>
                {rooms}
            </View>  
        )
    }

    renderRoomSelector(){
        if(this.state.showSelector){
            return null
        }
        if(!this.state.roomList || !this.state.roomList.length){
            return null
        }
        const Colors = this.props.themeInfo.colors
        return(
            <View style={[styles.titleWrapper,{flex: 1}]}>
                <Text style={[styles.title,{color: Colors.themeText}]}>请选择房间</Text>
                {this.renderRooms()}
            </View>
        )
    }

    renderSpinner(){
        return(
            <ActivityIndicator
                animating = {this.state.spinner}
                size="small"
                toast
                color={'#fff'}
            />
        )
    }

    renderToast(){
        return(
            <Toast ref={e => this.toast = e}/>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.getHeaderView()}
                <ScrollView contentContainerStyle={styles.scrollContainer}>
                    {this.renderBroadSwitch()}
                    {/* {this.renderWakeUpSwitch()} */}
                    {this.renderFloorSelector()}
                    {this.renderRoomSelector()}
                </ScrollView>
                {this.renderSpinner()}
                {this.renderToast()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
    },
    scrollContainer:{
		paddingBottom: 50
	},
    header: {
        width: '100%'
    },
    status: {
        height: StatusBarHeight,
        width: '100%',
    },
    nav: {
        height: NavigationBarHeight,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    navText:{ 
        marginLeft: 15, 
        fontSize: 24,
        fontWeight: 'bold' 
    },
    backTouch: {
        height: '100%',
        marginLeft: 16,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    backImg:{
        width: 10, 
        height: 18,
        resizeMode:'contain',
    },
    settingText:{
        color: Colors.white,
        fontSize: 16
    },
    playButton:{ 
        paddingHorizontal: 20,
        justifyContent:'center',
        alignItems:'center',
        height: '100%'
    },
    title:{
        fontSize: 18,
        fontWeight: 'bold',
    },
    itemWrapper:{
        paddingLeft: 16,
        marginTop: 10,
        paddingVertical: 10
    },
    switchWrapper:{
        flexDirection: 'row',
        alignItems: 'center'
    },
    titleWrapper:{
        marginTop: 20,
        marginBottom: 10,
        paddingHorizontal: 16
    },
    floorTouch:{
        marginTop: 15,
        width: '100%',
        height: 40,
        borderRadius: 4,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 16
    },
    floorImg:{
        width: 26,
        height: 19,
        resizeMode: 'contain'
    },
    floorText:{
        fontSize: 16,
        marginLeft: 15
    },
    arrowDown:{
       width: 14,
       height: 7,
       resizeMode: 'contain' 
    },
    expandWrapper:{
        marginTop: 5,
        width: '100%',
        height: 1
    },
    floorsWrapper:{
        width: '33%',
        height: 50, 
        justifyContent:'center',
        alignItems:'center',
        padding: 5
    },
    floorBtn:{
        width:'100%',
        height:'100%',
        justifyContent:'center',
        alignItems:'center',
        borderRadius: 4,
        overflow: 'hidden',
        paddingHorizontal: 5
    },
    btnWrapper:{
        width:'100%',
        flexDirection:'row',
        flexWrap:'wrap',
        marginTop: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    checkIcon: {
        position: 'absolute',
        top: -2,
        right: -2,
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    switchTouch:{
        paddingHorizontal: 20,
        height: '100%',
        alignItems: 'center'
    },
    switchImg:{
        width:50,
        height:40,
        resizeMode:'contain'
    },
    subDetail:{
        fontSize: 13,
        marginTop: 5
    },
    questionBtn:{
        paddingHorizontal:15,
        height: 40,
        justifyContent:'center',
        alignItems:'center'
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(VoiceSettingModal)

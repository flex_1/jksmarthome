

import {
    Platform,
    NativeEventEmitter,
    NativeModules,
    DeviceEventEmitter,
} from 'react-native';
import { AudioRecorder, AudioUtils } from 'react-native-audio';
import Sound from 'react-native-sound';
import { postJson,postPicture } from '../../util/ApiRequest';
import { NetUrls, LocalStorageKeys } from '../../common/Constants';
import LocalTokenHandler from '../../util/LocalTokenHandler';
import ToastManager from '../../common/CustomComponent/ToastManager';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";


const FloatButton = Platform.OS == 'android' ? NativeModules.FloatButtonModule : null
const FloatButtonEmitter = Platform.OS == 'android' ? new NativeEventEmitter(FloatButton) : null


export default class VoiceForAndroidBackGround {

    constructor(props) {
        
        this.state = {
            userId: null,

            currentTime: 0,//
            duration: 0,//总时长
            recording: false,//是否录音
            
            stoppedRecording: false,
            finished: false,
            play: false,
            audioPath: AudioUtils.DocumentDirectoryPath +  "/test.aac",//生成的录音
            hasPermission: true,
            isPlaying: false
        }
    }

    // 监听事件
    prepareForRecord(){
        this.getUserId()
        this.audioInit()
        FloatButton?.showFloat();

        this.eventListener = FloatButtonEmitter?.addListener('FloatButtonEmitter', (event) => {
            console.log('悬浮按钮点击事件', event);
            if (event?.action == 'down') {
                this.buttonPress()
            } else if (event?.action == 'up') {
                this.buttonRelease()
            }
            // event?.permission == "true"
        });
    }

    // 开始录音
    buttonPress(){
        console.log('开始录音');
        this.whoosh?.release()
        this.state.isPlaying = false
        if (!this.state.hasPermission) {
            return;
        }
        this._record()
    }

    // 结束录音
    buttonRelease(){
        console.log('结束录音');
        if (!this.state.hasPermission) {
            return;
        }
        this._stop()
    }

    deallocRecord(){
        this.eventListener.remove()
    }

    async getUserId(){
        let tokens = await LocalTokenHandler.get()
        this.state.userId = tokens.id
    }

    // 上传语音
	async uploadRecord(path) {
        let params = {}
        
        if(!this.state.userId){
            // ToastManager.show('获取用户id失败')
            return
        }
        // SpinnerManager.show()
		try {
			let data = await postPicture({
                url: NetUrls.uploadVoice,
                timeout: 30,    // 30秒如果没有响应 则显示超时
				filePath: path,
				fileName: 'voice.aac',
				params: {
                    ...params,
                    broadcastSwitch: 1,
                    accountId: this.state.userId,
                }
            })
            // SpinnerManager.close()
			if (data && data.code == 0) {
                let res = data.result
                
                // 播报语音
                if(res.broadcast_url){
                    this._play(res.broadcast_url)
                }
                
			}else{
				// ToastManager.show(data.msg)
			}
		} catch (error) {
            // SpinnerManager.close()
			// ToastManager.show(JSON.stringify(error));
		}
	}

    // 初始化 录音器
    audioInit(){
        AudioRecorder.requestAuthorization().then((isAuthorised) => {

            this.state.hasPermission = isAuthorised

            if (!isAuthorised) return;

            this.prepareRecordingPath();

            AudioRecorder.onProgress = (data) => {
                this.state.currentTime =  Math.floor(data.currentTime)
            };

            AudioRecorder.onFinished = (data) => {
                console.log("data" + JSON.stringify(data));
                this.state.finished =  true
                this.uploadRecord(data.audioFileURL)
            };
        });
    }

    // 录音准备
    prepareRecordingPath() {
        AudioRecorder.prepareRecordingAtPath(this.state.audioPath, {
            SampleRate: 22050,
            Channels: 1,
            AudioQuality: "Medium",
            AudioEncoding: "aac",
            AudioEncodingBitRate: 32000
        });
    }

    // 开始录音
    async _record() {
        if (!this.state.hasPermission) {
            // ToastManager.show("无法录音，请授予权限");
            return;
        }
        if(this.state.recording){
            // ToastManager.show("正在录音中");
            return;
        }
        if (this.state.stoppedRecording) {
            this.prepareRecordingPath(this.state.audioPath);
        }
        this.state.recording = true
        this.state.play = false
        
        try {
            await AudioRecorder.startRecording()
        } catch (error) {
            // ToastManager.show('录音错误')
        }
    }

    // 停止 录音
    async _stop() {
        this.state.currentTime = 0
        this.state.stoppedRecording = true
        this.state.finished = true
        this.state.recording = false
        try {
            await AudioRecorder.stopRecording();
        } catch (error) {
            // ToastManager.show('录音错误，或录音时间过短')
        }
    }

    // 播放录音
    async _play(voiceUrl) {
        // ToastManager.show('播放中')
        let whoosh = new Sound(voiceUrl, '', (err) => {
            this.state.isPlaying = true
            if (err) {
                // ToastManager.show('播放失败')
                return
            }
            whoosh.play(success => {
                if (success) {
                    console.log('success - 播放成功')
                } else {
                    // ToastManager.show('播放失败')
                    return
                }
                whoosh.release();
                this.state.isPlaying = false
            })
        })
        this.whoosh = whoosh
    }
}


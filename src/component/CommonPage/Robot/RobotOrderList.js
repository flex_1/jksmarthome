/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    DeviceEventEmitter,
    Image,
    Dimensions,
    RefreshControl,
    FlatList
} from 'react-native';
import { connect } from 'react-redux';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from '../../../common/CustomComponent/SpinnerManager';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderTitle from '../Navigation/HeaderTitle';
import HeaderBackground from '../Navigation/HeaderBackground';
import { Colors,NetUrls } from '../../../common/Constants';
import {FooterLoading,FooterEnd } from "../../../common/CustomComponent/ListLoading";
import { postJson } from '../../../util/ApiRequest';
import DateUtil from '../../../util/DateUtil';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class RobotOrderList extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} disableNight={true}/>,
        headerTitle: <HeaderTitle title={'订单记录'} disableNight={true}/>,
        headerBackground: <HeaderBackground hiddenBorder={true} disableNight={true}/>
    })

    constructor(props) {
        super(props);
        const {getParam, setParams} = this.props.navigation
        
        this.state = {
            currentIndex: 0,
            orderListData: null,
            currentPage: 0,
            totalPage: 0,
            loading: true,
            isRefreshing: false
        }
    }

    componentDidMount(){
        this.requestOrderList()
    }

    // 根据 楼层获取房间列表
	async requestOrderList(params) {
        params = params || {}
        try {
			let data = await postJson({
				url: NetUrls.robotTaskList,
				params: {
                    ...params,
                    type: this.state.currentIndex + 1
                }
			});
			this.setState({
				loading: false,
                isRefreshing: false
			})
			if (data.code == 0) {
                let orderList = []
                if(params.pageNum > 1){
                    orderList = this.state.orderListData.concat(data.result.list)
                }else{
                    orderList = data.result?.list || []
                }
				this.setState({
					orderListData: orderList,
                    currentPage: data.result?.pageNum,
                    totalPage: data.result?.totalPageNum
				})
			} else {
				ToastManager.show(data.msg);
			}
		} catch (e) {
			this.setState({
				loading: false,
                isRefreshing: false
			})
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(e));
		}
    }

    getDateString(str) {
        if (new Date(str).toDateString() === new Date().toDateString()) {
            return '今日' + '  ' + DateUtil(str, 'hh:mm')
        } else if (new Date(str) < new Date()){
            //之前
            return DateUtil(str, 'yyyy-MM-dd  hh:mm') 
        }
    }

    onLoadNextPage(){
        if(this.state.loading){
            return
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return
        }
        this.requestOrderList({pageNum: this.state.currentPage+1})
    }

    onRefresh(){
        this.setState({
            currentPage: 0,
            totalPage: 0,
            loading: true,
            isRefreshing: true
        },()=>{
            this.requestOrderList()
        })
    }

    renderSegment(){
        const topSelects = ['今日订单','待完成','全部订单']

        return(
            <View style={styles.segWrapper}>
                {topSelects.map((value, index) => 
                    <TouchableOpacity
                        key={'seg_'+index}
                        activeOpacity={0.7}
                        style={index == this.state.currentIndex ? styles.segItem_select : styles.segItem}
                        onPress={()=>{
                            this.setState({
                                currentIndex : index,
                                orderListData: null,
                                currentPage: 0,
                                totalPage: 0,
                                loading: true
                            },()=>{
                                this.requestOrderList()
                            })
                        }}
                    >
                        <Text style={index == this.state.currentIndex ? styles.segText_select : styles.segText}>{value}</Text>
                    </TouchableOpacity>
                )}
            </View>
        )
    }

    renderDash(){
        return(
            <View style={{marginLeft: 2}}>
                <View style={[styles.dash,{marginTop: 0}]}/>
                <View style={styles.dash}/>
                <View style={styles.dash}/>
                <View style={styles.dash}/>
            </View>
        )
    }

    renderRowItem(rowData, index){
        const {id,createTime, fromOpenCode, fromStationName, fromStationFloor, fromStationRoom, 
            toStationName, toStationFloor, toStationRoom, 
            taskStatusDesc} = rowData

        const fromStationText = (fromStationFloor ? fromStationFloor + '  ' : '') + 
        (fromStationRoom ? fromStationRoom + '  ' : '') + fromStationName
        const toStationText = (toStationFloor ? toStationFloor + '  ' : '') + 
        (toStationRoom ? toStationRoom + '  ' : '') + toStationName

        return(
            <TouchableOpacity activeOpacity={0.7} style={styles.item} onPress={()=>{
                this.props.navigation.navigate('RobotOrderDetail',{
                    orderId: id,
                    orderDetail: rowData,
                    callBack: ()=>{
                        this.requestOrderList()
                    }
                })
            }}>
                <View style={{flexDirection: 'row',alignItems:'center'}}>
                    <Text style={{fontSize: 14,color: '#242424',fontWeight: '500'}}>开箱码</Text>
                    <Text style={{fontSize: 20,color: '#F97B03',fontWeight: '500',marginLeft: 5}}>{fromOpenCode}</Text>
                </View>
                <View style={{flexDirection: 'row',alignItems:'center',marginTop:8}}>
                    <Image style={{width:10,height:10}} source={require('../../../images/panel/robot/time.png')}/>
                    <Text style={{marginLeft: 5, fontSize: 12, color: '#616264'}}>{this.getDateString(createTime)}</Text>
                </View>
                <View style={styles.siteWrapper}>
                    <View style={{width:6,height:6,borderRadius:3,backgroundColor:'#1983FC'}}/>
                    <Text style={{marginLeft: 5,fontSize: 12,color: '#616264'}}>{fromStationText}</Text>
                </View>
                {this.renderDash()}
                <View style={[styles.siteWrapper,{marginTop: 0}]}>
                    <View style={{width:6,height:6,borderRadius:3,backgroundColor:'#616264'}}/>
                    <Text style={{marginLeft: 5,fontSize: 12,color: '#616264'}}>{toStationText}</Text>
                </View>
                <Text style={styles.statusText}>{taskStatusDesc}</Text>
            </TouchableOpacity>
        )
    }

    //footer
    renderFooter() {
        if(!this.state.orderListData || this.state.orderListData.length <= 0){
            return null
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return <FooterEnd/>
        }
        return <FooterLoading/>
    }

    renderNoContent(){
        let tips = ''
        if (!this.state.orderListData) {
			tips = '正在加载...'
		}else if(this.state.orderListData.length <= 0){
            tips = '暂无订单数据'
        }
        return(
            <View style={{alignItems: 'center',justifyContent:'center',marginTop: screenH*0.3}}>
                <Text style={{fontSize: 14, color: Colors.themeTextLightGray}}>{tips}</Text>
            </View>
        )
    }

    renderOrderList(){
        let orderListData = this.state.orderListData

        return(
            <View style={styles.listWrapper}>
                <FlatList
                    contentContainerStyle={{paddingBottom: 20}}
                    scrollIndicatorInsets={{right: 1}}
                    removeClippedSubviews={false}
                    data={orderListData}
                    keyExtractor={(item, index)=> "roomInfo_index_" + index}
                    renderItem={({ item, index }) => this.renderRowItem(item, index)}
                    initialNumToRender={10}
                    onEndReached={() => this.onLoadNextPage()}
                    onEndReachedThreshold={0.1}
                    ListFooterComponent={() => this.renderFooter()}
                    ListEmptyComponent = {this.renderNoContent()}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                />
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={styles.container}>
                {this.renderSegment()}
                {this.renderOrderList()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#F5F6F7',
        flex: 1
    },
    segWrapper:{
        marginTop: 12,
        marginHorizontal: screenW * 0.15,
        height: 32,
        borderRadius: 16,
        backgroundColor: Colors.white,
        flexDirection: 'row'
    },
    segItem:{
        flex: 1,
        backgroundColor: Colors.white,
        borderRadius: 16,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    segItem_select:{
        flex: 1,
        backgroundColor: '#1983FC',
        borderRadius: 16,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    segText:{
        fontSize: 14,
        color: '#242424'
    },
    segText_select:{
        fontSize: 14,
        color: Colors.white
    },
    listWrapper:{
        flex: 1,
        marginTop: 12
    },
    item:{
        marginHorizontal: 12,
        backgroundColor: Colors.white, 
        height: 124, 
        marginTop: 12,
        borderRadius: 12,
        padding: 12
    },
    siteWrapper:{
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10
    },
    statusText:{
        position: 'absolute',
        top: 16,
        right: 12,
        fontSize: 14,
        color: '#1983FC'
    },
    dash:{
        width:1, 
        height:2,
        backgroundColor: '#BABCBE',
        marginTop:1
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo
    })
)(RobotOrderList)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	StyleSheet,
	Image,
	Text,
	View,
	ScrollView,
	Dimensions,
	ImageBackground,
} from 'react-native';
import { postJson } from '../../../util/ApiRequest';
import { NetUrls, Colors } from '../../../common/Constants';
import ToastManager from '../../../common/CustomComponent/ToastManager';
import SingleChoiceDialog from './SingleChoiceDialog';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import { connect } from 'react-redux';
import { deviceWidth } from '../../../util/ScreenUtil';

const screenW = Dimensions.get('window').width;
const screenH = Dimensions.get('window').height;
const homeCardH = (screenW - 32) * 0.5 // 32为两边padding值，0.5为卡片设计高度比



//侧滑最大距离
const maxSwipeDistance = 160
//侧滑按钮个数
const countSwiper = 2

class RobotLocation extends Component {

	static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeft navigation={navigation} backTitle={'实时状态'} />,
			headerBackground: <HeaderBackground />
		}
	}

	constructor(props) {
		super(props);
		const { getParam, setParams } = this.props.navigation
		this.refreshAllInfos = getParam('refreshAllInfos')




		this.state = {
			defaultHouse: 0,
			houseList: null,
			stationInfoList: [],
			loading: false,
			imageBottom: 0,
			imageLeft: 0,
			imageUrl: '',
			imageHeight: screenH / 2,
			imageWidth: screenW,
			imageWidthRatio: 1,
			originWidth: screenW,
			originHeight: screenH / 2,
			isVisible: false,
			isSelected: {}, // 默认选中第一个，可随意更改
			languageArr: [],
			robotCurrentArea: {},
			robotCurrentInfo: {}

		}
		this.interval = setInterval(() => {
			this.getRobotCurrentArea()
		}, 3000);

	}

	componentDidMount() {
		this.getRobotList()
	}

	componentWillUnmount() {
		this.interval && clearInterval(this.interval);
		this.setState = (state, callback) => {
			return;
		}
	}
	showModal = () => {
		this.setState({
			isVisible: true
		})

	}
	doSelected = (option) => {
		this.setState({
			isSelected: option
		})

	}

	add = () => {


		let imageWidth = this.state.imageWidth * 1.1;

		let imageHeight = this.state.imageHeight * 1.1;
		let imageWidthRatio = this.state.originHeight / imageHeight;

		this.setState({
			imageWidth: imageWidth,
			imageHeight: imageHeight,
			imageWidthRatio: imageWidthRatio,

			imageBottom: Math.abs(this.state.robotCurrentArea.y) / imageWidthRatio,
			imageLeft: Math.abs(this.state.robotCurrentArea.x) / imageWidthRatio
		})
		
		
		
	
	}
	minus = () => {

		if (this.state.imageHeight < screenH / 2) {
			return
		}
		let imageWidth = this.state.imageWidth * 0.9;

		let imageHeight = this.state.imageHeight * 0.9;

		let imageWidthRatio = this.state.originHeight / imageHeight;

		this.setState({
			imageWidth: imageWidth,
			imageHeight: imageHeight,
			imageWidthRatio: imageWidthRatio,
			imageBottom: Math.abs(this.state.robotCurrentArea.y) / imageWidthRatio,
			imageLeft: Math.abs(this.state.robotCurrentArea.x) / imageWidthRatio
		})
	}
	render() {
		const Colors = this.props.themeInfo.colors

		return (

			<View>

				<View style={[styles.operator]}>
					<Text style={[styles.navTitle, { padding: 10 }]} onPress={() => this.minus()}>-</Text>
					<Text style={[styles.navTitle, { padding: 10 }]} onPress={() => this.add()}>+</Text>

				</View>
				<ScrollView style={[{ width: deviceWidth, height: screenH / 2 }]} ref={(view) => {
                    this.myScrollView = view;
                }}
>
					<ScrollView horizontal="true" style={[{ width: deviceWidth, height: this.state.imageHeight }]} ref={(view) => {
						this.myHScrollView = view;
					}}
					>


						<ImageBackground style={[{ width: this.state.imageWidth, height: this.state.imageHeight, overflow: 'visible', resizeMode: 'contain' }]}

							source={{ uri: this.state.imageUrl }}
						>
							<Image ref={(view) => {
						this.myImage = view;
					}} resizeMode='contain' style={[{
								width: 50, height: 50, position: 'absolute',
								bottom: this.state.imageBottom,
								left: this.state.imageLeft
							}]} source={require('../../../images/location/robot.png')}></Image>

							{
								this.state.stationInfoList.map((item, idx) => {
									return (
										<View key={idx} style={[{
											flexDirection: 'column', position: 'absolute',
											bottom: Math.abs(item.y) / this.state.imageWidthRatio,
											left: Math.abs(item.x) / this.state.imageWidthRatio
										}]}   >
											<View style={styles.myButton} />
											<Text style={[{ fontSize: 10 }]}   >{item.stationName}</Text>
										</View>

									)
								})
							}

						</ImageBackground>

					</ScrollView>
				</ScrollView>

				<View style={[styles.myBackGround]} >
					<View style={styles.titleLayout}>
						<Text onPress={() => this.showModal()} >{this.state.isSelected.robotId}</Text>
						<View style={styles.redDot} />
					</View>

					<View style={styles.contentLayout}>
						<View style={[styles.titleLayout]}>
							<Image style={[styles.imageTitle]} source={require('../../../images/location/serving.png')}></Image>
							<Text  >{this.state.robotCurrentInfo.robotStatus == 'Service' ? "服务中" : this.state.robotCurrentInfo.robotStatus == 'Shutdown' ? '关机' : this.state.robotCurrentInfo.robotStatus == 'Maintenance' ? '维护中' : this.state.robotCurrentInfo.robotStatus == 'Broken' ? '故障' : this.state.robotCurrentInfo.robotStatus}</Text>
						</View>

						<View style={[styles.titleLayout]}>
							<Image style={[styles.imageTitle]} source={require('../../../images/location/battery.png')}></Image>
							<Text>{Math.abs(this.state.robotCurrentInfo.battery) + '%'}</Text>
						</View>
					</View>

					<View style={styles.contentLayout}>
						<View style={[styles.titleLayout]}>
							<Image style={[styles.imageTitle]} source={require('../../../images/location/inservice.png')}></Image>
							<Text  >{this.state.robotCurrentInfo.serverStatus == 'Service' ? "在线" : this.state.robotCurrentInfo.serverStatus}</Text>
						</View>

						<View style={[styles.titleLayout]}>
							<Image style={[styles.imageTitle]} source={require('../../../images/location/dingdan.png')}></Image>
							<Text>{this.state.robotCurrentInfo.locationOrderInfoList && this.state.robotCurrentInfo.locationOrderInfoList.length > 0 ? '有' : '无'}</Text>
						</View>
					</View>
				</View>
				<SingleChoiceDialog
					title='请选择机器人'
					isVisible={this.state.isVisible}
					isSelected={this.state.isSelected}
					dataSource={this.state.languageArr}
					onConfirm={(option) => { this.doSelected(option) }}
					onCancel={() => { this.setState({ isVisible: false }) }}
				/>
			</View>
		)
	}

	async getRobotList() {
		try {
			let data = await postJson({
				url: NetUrls.getRobotList,
				params: {
					sourceType: 1
				}
			});
			this.setState({ isRefreshing: false })
			if (data.code == 0) {

				this.setState({
					languageArr: data.result.data,
					isSelected: data.result.data[0]

				})
				//获取单个机器人位置
				this.getRobotCurrentArea()

			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({ isRefreshing: false })
			ToastManager.show(JSON.stringify(error))
		}
	}

	async getRobotCurrentArea() {
		try {
			let data = await postJson({
				url: NetUrls.getRobotCurrentArea,
				params: {
					robotId: this.state.isSelected.robotId,
					sourceType: 1
				}
			});
			this.setState({ isRefreshing: false })
			if (data.code == 0) {
				const area = data.result.robotCurrentArea.data;
				if (this.state.robotCurrentArea.mapId === area.mapId) {
					this.setState({
						robotCurrentArea: area,
						robotCurrentInfo: data.result.robotCurrentInfo.data,
						imageBottom: Math.abs(area.y) / this.state.imageWidthRatio,
						imageLeft: Math.abs(area.x) / this.state.imageWidthRatio
					})

					if(this.state.imageLeft > screenW-10){
						this.myHScrollView.scrollTo({ x: this.state.imageLeft-screenW/2, y: 0, animated: true });

					}
					if(this.state.imageBottom > screenH/2){
						this.myScrollView.scrollTo({ x: 0, y: this.state.imageHeight - this.state.imageBottom - screenH/4, animated: true });

					}
				

				} else {
					this.setState({
						robotCurrentArea: data.result.robotCurrentArea.data,
						robotCurrentInfo: data.result.robotCurrentInfo.data
					})
					this.getRobotMapByMapId()
				}


			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({ isRefreshing: false })
			ToastManager.show(JSON.stringify(error))
		}
	}

	async getRobotMapByMapId() {
		try {
			let data = await postJson({
				url: NetUrls.getRobotMapByMapId,
				params: {
					ids: this.state.robotCurrentArea.mapId,
					sourceType: 1
				}
			});
			this.setState({ isRefreshing: false })
			if (data.code == 0) {

				let f = data.result.data

				if (f.imageUrl === this.state.imageUrl) {
					return
				}

				this.setState({ imageUrl: f.imageUrl })
				this.forceUpdate()



				Image.getSize(f.imageUrl, (width, height) => {
					let imageWidthRatio = height / this.state.imageHeight;
					let imageWidth = width / imageWidthRatio;
					let imageHeight = this.state.imageHeight;
					if (imageWidth < screenW) {
						imageWidth = screenW;
						imageHeight = height / (width / screenW)
						imageWidthRatio = height / imageHeight;

					}

					this.setState({
						originWidth: width,
						originHeight: height,
						imageWidth: imageWidth,
						imageHeight: imageHeight,
						imageWidthRatio: imageWidthRatio,
						stationInfoList: f.stationInfoList,
						imageBottom: Math.abs(this.state.robotCurrentArea.y) / imageWidthRatio,
						imageLeft: Math.abs(this.state.robotCurrentArea.x) / imageWidthRatio
					})
				});

			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({ isRefreshing: false })
			ToastManager.show(JSON.stringify(error))
		}
	}

}

const styles = StyleSheet.create({
	rowDirection: {
		flexDirection: 'row',
	},
	operator: {
		flexDirection: 'row',
		marginLeft: 30,
		marginRight: 30,

		justifyContent: 'space-around'
	},
	imageTitle: {
		width: 20,
		height: 20,
		resizeMode: 'contain',
		marginRight: 5,
		alignSelf: 'flex-start'
	},
	titleLayout: {
		flexDirection: 'row',
		marginLeft: 10,
		marginRight: 10,

		alignItems: "center"

	},
	contentLayout: {

		flexDirection: 'row',
		marginLeft: 10,
		marginRight: 50,
		marginTop: 10,
		justifyContent: 'space-around'

	},
	container: {
		backgroundColor: Colors.newTheme,
		flexDirection: 'row',
		flex: 1
	},
	containerPostion: {
		flexDirection: 'row',
		justifyContent: 'flex-start'
	},
	contentStyle: {
		paddingBottom: 30
	},
	navTitle: {
		fontSize: 24,
		fontWeight: 'bold',
		marginLeft: 15,
		textAlignVertical: 'center',
		color: Colors.themeTextBlack,
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight: 5
	},
	navImg: {
		width: 18,
		height: 18
	},
	homeCardTouch: {
		paddingHorizontal: 16,
		width: '100%',
		marginTop: 20,
		borderRadius: 5
	},
	homeCardBg: {
		width: '100%',
		minHeight: homeCardH,
		resizeMode: 'contain',
		paddingVertical: 16
	},
	homeCardImage: {
		borderRadius: 5,
		backgroundColor: Colors.white
	},
	unit: {
		fontSize: 12,
		color: Colors.white,
	},
	//侧滑菜单的样式
	quickAContent: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginRight: 18,
		marginTop: 20,
		overflow: 'hidden',
		borderTopRightRadius: 5,
		borderBottomRightRadius: 5,
		paddingVertical: 1,
	},
	quick: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		width: maxSwipeDistance / countSwiper,
	},
	quick2: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		width: maxSwipeDistance,
	},
	imageBg: {
		width: '100%',
		resizeMode: 'contain',

		aspectRatio: 0.6 // Your aspect ratio
		// width: this.screenW,
		// height: this.imageHeight,
		// alignSelf: "center",
	}
	,
	myButton: {

		height: 4,
		width: 4,  //The Width must be the same as the height
		borderRadius: 8, //Then Make the Border Radius twice the size of width or Height   
		backgroundColor: 'rgb(0, 0, 0)',

	},
	redDot: {

		height: 4,
		width: 4,  //The Width must be the same as the height
		borderRadius: 8, //Then Make the Border Radius twice the size of width or Height   
		backgroundColor: '#f00',

	},
	myBackGround: {
		backgroundColor: '#EEE',
		borderRadius: 8,
		marginLeft: 15,
		marginRight: 15,
		marginTop: 15,
		paddingVertical: 10
	}
});

export default connect(
	(state) => ({
		themeInfo: state.themeInfo,
	})
)(RobotLocation)

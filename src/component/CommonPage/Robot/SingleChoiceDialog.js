import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Modal,
    ScrollView
} from 'react-native';
import PropTypes from 'prop-types';

export default class SingleChoiceDialog extends Component {
    constructor(props) {
        super(props);
    }
    static propTypes = {
        title: PropTypes.string // Dialog标题
        , isSelected: PropTypes.object // 默认选中项
        , isVisible: PropTypes.bool // 是否显示Dialog
        , dataSource: PropTypes.array // 数据源
        , onConfirm: PropTypes.func.isRequired // 确认回调
        , onCancel: PropTypes.func.isRequired // 取消回调
    };

    static defaultProps = {
        title: '请选择' // 默认题目
        , isSelected:  {} // 默认
        , isVisible: true // 默认关闭组件
        , dataSource: [] // 默认数据源为空数组
    }
    render() {
        return (
            <Modal
                animationType={'slide'}
                transparent={true}
                visible={this.props.isVisible}
                onRequestClose={() => { this.onCancel() }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>
                    <View style={{ backgroundColor: '#fff', borderRadius: 10, height: 350 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#d5d5d5', height: 35 }}>
                            <Text style={{ fontSize: 15, color: '#009AD6' }}>{this.props.title}</Text>
                        </View>
                        <ScrollView style={{ marginTop: 10 }}>
                            {this.renderOptionsList()}
                        </ScrollView>

                        <TouchableOpacity onPress={() => { this.onCancel() }}>
                            <View style={{ marginTop: 15, marginRight: 10, width: 200, marginLeft: 10, marginBottom: 15, backgroundColor: '#009AD6', height: 35, justifyContent: 'center', borderRadius: 1 }}>
                                <Text style={{ textAlign: 'center', color: 'white', fontSize: 15 }}>取消</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

            </Modal>
        );
    }

    renderOptionsList() {
        return this.props.dataSource.map(option => {
            return (
                <TouchableOpacity onPress={() => { this.optionSelected(option) }} key={option.robotId}>
                    <Text style={{
                        width: 200, textAlign: 'center', fontSize: 18, marginTop: 5, alignSelf: 'center',
                        color: this.props.isSelected.robotId === option.robotId ? '#009ad6' : null
                    }}>
                        {option.robotId}
                    </Text>
                </TouchableOpacity>
            )
        })
    }

    onCancel() {
        this.props.onCancel();
    }

    optionSelected(option) {
        // 通过回调传递数据
        this.props.onConfirm(option);
        // 关闭Dialog对话框
        this.onCancel();
    }
}
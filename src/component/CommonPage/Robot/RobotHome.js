/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    DeviceEventEmitter,
    Keyboard,
    Image,
    Dimensions,
    ScrollView,
    RefreshControl
} from 'react-native';
import { connect } from 'react-redux';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderTitle from '../Navigation/HeaderTitle';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderRight from '../Navigation/HeaderRight';
import { Colors,NetUrls } from '../../../common/Constants';
import { BottomSafeMargin,StatusBarHeight,NavigationBarHeight } from '../../../util/ScreenUtil';
import { postJson } from '../../../util/ApiRequest';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const kRobotHomeInfoChanged = 'kRobotHomeInfoChangedNoti'

class RobotHome extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} disableNight={true}/>,
        headerTitle: <HeaderTitle title={navigation.getParam('title')} disableNight={true}/>,
        headerRight: (
            <HeaderRight disableNight={true} buttonArrays={[
                {icon:require('../../../images/panel/robot/more.png'), onPress:()=>{
                    navigation.navigate('RobotOrderList')
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground hiddenBorder={true} disableNight={true}/>
    })

    constructor(props) {
        super(props);
        
        this.state = {
            isFree: true,
            taskInfo: null,
            isRefreshing: false
        }
    }

    componentDidMount(){
        this.requestRobotInfo()
        this.noti = DeviceEventEmitter.addListener(kRobotHomeInfoChanged, ()=>{
            this.requestRobotInfo()
        })
    }

    componentWillUnmount(){
        this.noti.remove()
    }

    async requestRobotInfo(){
		try {
			let data = await postJson({
				url: NetUrls.robotPage,
				params: {
                    
                }
			});
            this.setState({isRefreshing: false})
			if (data.code == 0) {
				this.setState({
                    isFree: data.result?.isFree,
                    taskInfo: data.result?.task
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            this.setState({isRefreshing: false})
			ToastManager.show(JSON.stringify(error))
		}
    }

    onRefresh(){
        this.setState({
            isRefreshing: true
        },()=>{
            this.requestRobotInfo()
        })
    }

    renderHeadMessage(){
        if(!this.state.taskInfo){
            return null
        }
        const {id, fromOpenCode, fromStationName, fromStationFloor, fromStationRoom, 
            toStationName, toStationFloor, toStationRoom, 
            taskStatusDesc} = this.state.taskInfo

        const fromStationText = (fromStationFloor ? fromStationFloor + '  ' : '') + 
        (fromStationRoom ? fromStationRoom + '  ' : '') + fromStationName
        const toStationText = (toStationFloor ? toStationFloor + '  ' : '') + 
        (toStationRoom ? toStationRoom + '  ' : '') + toStationName
        
        
        return(
            <TouchableOpacity activeOpacity={0.7} style={styles.headerWrapper} onPress={()=>{
                this.props.navigation.navigate('RobotOrderDetail',{
                    orderId: id,
                    orderDetail: this.state.taskInfo
                })
            }}>
                <View style={styles.titleWrapper}>
                    <Text style={{fontSize:16, fontWeight:'500', color: '#242424'}}>开箱码</Text>
                    <Text style={{flex: 1,marginLeft: 5,fontSize: 24,fontWeight:'500',color: '#F97B03'}}>{fromOpenCode}</Text>
                    <Text style={{fontSize:14,color:'#1983FC'}}>{'查看更多>>'}</Text>
                </View>
                <View style={styles.siteWrapper}>
                    <View style={{width:6,height:6,borderRadius:3,backgroundColor:'#1983FC'}}/>
                    <Text style={{marginLeft: 5,fontSize: 14,color: '#616264'}}>{fromStationText}</Text>
                </View>
                {this.renderDash()}
                <View style={[styles.siteWrapper,{marginTop: 0}]}>
                    <View style={{width:6,height:6,borderRadius:3,backgroundColor:'#616264'}}/>
                    <Text style={{marginLeft: 5,fontSize: 14,color: '#616264'}}>{toStationText}</Text>
                </View>
                <Text style={styles.waitingText}>{taskStatusDesc}</Text>
            </TouchableOpacity>
        )
    }

    renderDash(){
        return(
            <View style={{marginLeft: 2}}>
                <View style={[styles.dash,{marginTop: 0}]}/>
                <View style={styles.dash}/>
                <View style={styles.dash}/>
                <View style={styles.dash}/>
            </View>
        )
    }

    renderMainImage(){
        let desc = this.state.isFree ? '当前有空闲机器人' : '当前没有空闲机器人'
        let color = this.state.isFree ? '#1983FC' : '#616264'

        return(
            <View style={styles.mainImgWrapper}>
                <Image style={styles.mainImg} source={require('../../../images/panel/robot/robot.png')}/>
                <TouchableOpacity onPress={()=>{
                    this.props.navigation.navigate('RobotLocation')
                }}>
                    <Text style={[styles.freeText,{color: color}]}>{desc}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderBottomButton(){
        return(
            <View style={{paddingBottom:BottomSafeMargin + 20}}>
                <TouchableOpacity activeOpacity={0.7} style={styles.bottomTouch} onPress={()=>{
                    this.props.navigation.navigate('RobotCall')
                }}>
                    <Text style={styles.bottomTitle}>召唤</Text>
                    <Text style={styles.bottomSubTitle}>召唤机器人取送</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={styles.container}>
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                >
                    <View style={styles.contentWrapper}>
                        {this.renderHeadMessage()}
                        {this.renderMainImage()}
                    </View>
                </ScrollView>
                {this.renderBottomButton()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#F5F6F7',
        flex: 1
    },
    contentWrapper:{
        height: screenH - StatusBarHeight - NavigationBarHeight - BottomSafeMargin - 20 - 60
    },
    headerWrapper:{
        marginHorizontal: 12,
        marginTop: 12,
        padding: 12,
        backgroundColor: Colors.white,
        height: 110,
        borderRadius: 12
    },
    titleWrapper:{
        flexDirection: 'row',
        alignItems: 'center'
    },
    siteWrapper:{
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10
    },
    waitingText:{
        fontSize:14,
        color: '#1983FC',
        position: 'absolute',
        bottom: 15,
        right: 12
    },
    mainImgWrapper:{
        justifyContent:'center',
        alignItems:'center',
        flex : 1
    },
    mainImg:{
        width: screenW * 0.5, 
        height: screenW*0.5*1.6, 
        resizeMode:'contain'
    },
    freeText:{
        fontSize: 14,
        marginTop: 20,
        textAlign: 'center'
    },
    bottomTouch:{
        marginHorizontal: 16,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        backgroundColor: '#1983FC'
    },
    bottomTitle:{
        color: Colors.white,
        fontSize: 20,
        fontWeight: '500'
    },
    bottomSubTitle:{
        color: Colors.white,
        fontSize: 12,
        marginTop: 5
    },
    dash:{
        width:1, 
        height:2,
        backgroundColor: '#BABCBE',
        marginTop:1
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo
    })
)(RobotHome)

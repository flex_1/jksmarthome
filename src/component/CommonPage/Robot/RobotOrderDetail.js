/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    DeviceEventEmitter,
    Keyboard,
    Image,
    Dimensions,
    ScrollView,
    Modal,
    RefreshControl
} from 'react-native';
import { connect } from 'react-redux';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from '../../../common/CustomComponent/SpinnerManager';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderTitle from '../Navigation/HeaderTitle';
import HeaderBackground from '../Navigation/HeaderBackground';
import { Colors,NetUrls } from '../../../common/Constants';
import { postJson } from '../../../util/ApiRequest';
import DateUtil from '../../../util/DateUtil';

const kRobotHomeInfoChanged = 'kRobotHomeInfoChangedNoti'

class RobotOrderDetail extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} disableNight={true}/>,
        headerTitle: <HeaderTitle title={'订单详情'} disableNight={true}/>,
        headerBackground: <HeaderBackground hiddenBorder={true} disableNight={true}/>
    })

    constructor(props) {
        super(props);
        const {getParam, setParams} = this.props.navigation
        
        this.callBack = getParam('callBack')
        this.orderId = getParam('orderId')
        this.state = {
            orderDetail: getParam('orderDetail') || {},
            canCancel: true,
            isRefreshing: false
        }
    }

    componentDidMount(){
        this.requestOrderDetail()
    }

    async requestOrderDetail(){
		try {
			let data = await postJson({
				url: NetUrls.robotTaskDetail,
				params: {
                    id: this.orderId
                }
			});
            this.setState({isRefreshing: false})
			if (data.code == 0) {
				this.setState({
                    orderDetail: data.result,
                    canCancel: data.result?.canCancel
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            this.setState({isRefreshing: false})
			ToastManager.show(JSON.stringify(error))
		}
    }

    async requestCancelOrder(){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.robotTaskCancel,
				params: {
                    id: this.orderId
                }
			});
			if (data.code == 0) {
                setTimeout(() => {
                    SpinnerManager.close()
                    ToastManager.show('取消成功')
                    this.requestOrderDetail()
                    this.callBack && this.callBack()
                    DeviceEventEmitter.emit(kRobotHomeInfoChanged)
                }, 1000);
			} else {
                SpinnerManager.close()
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    onRefresh(){
        this.requestOrderDetail()
    }

    renderCancelBtn(){
        if(!this.state.canCancel){
            return null
        }
        return(
            <TouchableOpacity activeOpacity={0.7} style={styles.cancelTouch} onPress={()=>{
                this.requestCancelOrder()
            }}>
                <Text style={styles.cancelText}>取消召唤</Text>
            </TouchableOpacity>
        )
    }

    renderHeaderMessage(){
        const {taskStatusDesc, fromOpenCode} = this.state.orderDetail
        return(
            <View style={styles.headerWrapper}>
                <Text style={styles.statusText}>{taskStatusDesc}</Text>
                <View style={styles.subWrapper}>
                    <Text style={styles.openTitle}>开箱码</Text>
                    <Text style={styles.openCode}>{fromOpenCode}</Text>
                    {this.renderCancelBtn()}
                </View>
            </View>
        )
    }

    renderInfoItem(title, value, subLine){
        const sublineText = subLine ? <Text style={{fontSize: 12, color: '#878787',marginTop: 4}}>{subLine}</Text> : null
        
        return(
            <View style={{flexDirection:'row',marginBottom: 10}}>
                <Text style={{width: 60,fontSize: 12, color: '#878787',lineHeight: 17}}>{title}</Text>
                <View>
                    <Text style={{fontSize: 12, color: '#878787',lineHeight: 17}}>{value}</Text>
                    {sublineText}
                </View>
            </View>
        )
    }

    renderDetailItem(){
        const {fromStationName, fromStationFloor, fromStationRoom, toStationName, toStationFloor, toStationRoom} = this.state.orderDetail
        const {fromWaitTime,toWaitTime,outId,createTime} = this.state.orderDetail
        const fromStationText = (fromStationFloor ? fromStationFloor + '  ' : '') + 
        (fromStationRoom ? fromStationRoom + '  ' : '') + fromStationName
        const toStationText = (toStationFloor ? toStationFloor + '  ' : '') + 
        (toStationRoom ? toStationRoom + '  ' : '') + toStationName
        const stayTimeFrom = '取物 — ' + fromWaitTime + 'min'
        const stayTimeTo = '送物 — ' + toWaitTime + 'min'
        const date = DateUtil(createTime, 'yyyy-MM-dd hh:mm:ss') 

        return(
            <View style={styles.detailItem}>
                <View style={{flex: 1}}>
                    {this.renderInfoItem('取物地点:',fromStationText)}
                    {this.renderInfoItem('送物地点:',toStationText)}
                    {this.renderInfoItem('停留时间:',stayTimeFrom, stayTimeTo)}
                    {this.renderInfoItem('订单编号:',outId)}
                    {this.renderInfoItem('下单时间:',date)}
                </View>
                <Image style={{width:77,height:125,resizeMode:'contain'}} source={require('../../../images/panel/robot/robot_s.png')}/>
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={styles.container}>
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                >
                    {this.renderHeaderMessage()}
                    {this.renderDetailItem()}
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#F5F6F7',
        flex: 1
    },
    headerWrapper:{
        marginHorizontal: 12,
        marginTop: 12,
        paddingHorizontal: 12,
        paddingTop: 16,
        backgroundColor: Colors.white,
        height: 96,
        borderRadius: 12
    },
    statusText:{
        color: '#1983FC',
        fontSize: 22,
        fontWeight: '500'
    },
    subWrapper:{
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10
    },
    cancelTouch:{
        justifyContent: 'center',
        alignItems: 'center',
        width: 96,
        height: 28,
        borderRadius: 14,
        backgroundColor: '#1983FC'
    },
    cancelText:{
        fontSize: 14,
        color: Colors.white
    },
    openTitle:{
        color: '#242424',
        fontSize: 16,
        fontWeight: '500'
    },
    openCode:{
        color: '#F97B03',
        fontSize: 24,
        marginLeft: 5,
        fontWeight: '500',
        flex: 1
    },
    detailItem:{
        margin: 12,
        backgroundColor: Colors.white,
        paddingTop: 12,
        paddingHorizontal: 14,
        borderRadius: 12,
        flexDirection: 'row',
        alignItems: 'center'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo
    })
)(RobotOrderDetail)

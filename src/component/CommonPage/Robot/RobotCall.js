/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    DeviceEventEmitter,
    Keyboard,
    Image,
    Dimensions,
    ScrollView,
    Modal
} from 'react-native';
import { connect } from 'react-redux';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from '../../../common/CustomComponent/SpinnerManager';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderTitle from '../Navigation/HeaderTitle';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderRight from '../Navigation/HeaderRight';
import { Colors,NetUrls } from '../../../common/Constants';
import { BottomSafeMargin } from '../../../util/ScreenUtil';
import SwitchButton from '../../../common/CustomComponent/SwitchButton';
import { postJson } from '../../../util/ApiRequest';

const timesArr = ['1min','2min','3min','4min','5min','6min','7min','8min','9min','10min']
const kRobotHomeInfoChanged = 'kRobotHomeInfoChangedNoti'

class RobotCall extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} disableNight={true}/>,
        headerTitle: <HeaderTitle title={'召唤信息'} disableNight={true}/>,
        headerBackground: <HeaderBackground hiddenBorder={true} disableNight={true}/>
    })

    constructor(props) {
        super(props);
        const {getParam, setParams} = this.props.navigation
        
        this.state = {
            codeSwitch: true,
            verifiCodeText: '8888',
            modalVisible: false,
            pickerTitle: '',
            pickerData: [],
            selectedValue: [],

            pickerType: 1,  // 1:取货地址  2:送货地址  3:取货停留  4:送货停留

            pickUpAddress: '',
            pickUpTime: 5,
            pickUpAddressId: null,
            deliverAddress: '',
            deliverTime: 5,
            deliverAddressId: null,

            addressList: null,
            address_Arr: [],
            addressIds_Arr: [],
            time_Arr: timesArr
        }
    }

    componentDidMount(){
        this.requestRobotCallInfo()
    }

    async requestRobotCallInfo(){
		try {
			let data = await postJson({
				url: NetUrls.getRobotStation,
				params: {
                }
			});
			if (data.code == 0) {
                this.formatStationData(data.result)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 创建订单
    async requestCreateRobotTask(params){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.createRobotTask,
				params: params
			});
            
			if (data.code == 0) {
                setTimeout(() => {
                    SpinnerManager.close()
                    DeviceEventEmitter.emit(kRobotHomeInfoChanged)
                    ToastManager.show('召唤成功')
                    this.props.navigation.replace('RobotOrderDetail',{
                        orderId: data.result.taskId
                    })
                }, 1000);
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    //处理站点数据
    formatStationData(stationData){
        let dataArray = []
        let idArray = []
        for (const floors of stationData) {
            let secondArray = []
            let secondIdArray = []
            for (const rooms of floors.roomList) {
                let threeArray = []
                let threeIdArray = []
                for (const station of rooms.stationList) {
                    threeArray.push(station.stationName)
                    threeIdArray.push({floor:floors.floor, room:rooms.room, stationName:station.stationName, stationId:station.stationId})
                }
                let three = {}
                three[rooms.room] = threeArray
                secondIdArray.push(threeIdArray)
                secondArray.push(three)
            }
            let second = {}
            second[floors.floor] = secondArray
            idArray.push(secondIdArray)
            dataArray.push(second)
        }
        this.setState({
            address_Arr: dataArray,
            addressIds_Arr: idArray
        })
    }

    handlePickerSelectData(data, index){
        if(this.state.pickerType <= 2){
            let address = data[2]
            let addressId = this.state.addressIds_Arr[index[0]][index[1]][index[2]]
            if(this.state.pickerType == 1){
                this.setState({
                    pickUpAddress: address,
                    pickUpAddressId: addressId,
                })
            }else{
                this.setState({
                    deliverAddress: address,
                    deliverAddressId: addressId
                })
            }
        }else{
            let timeStr = data[0]
            let time = timeStr.replace('min','')
            if(this.state.pickerType == 3){
                this.setState({
                    pickUpTime: time
                })
            }else{
                this.setState({
                    deliverTime: time
                })
            }
        }
    }

    // 弹出 Modal
    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerData,
            pickerTitleText: this.state.pickerTitle,
            selectedValue: this.state.selectedValue,
            wheelFlex: [1,2,3],
            pickerFontSize: 14,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false
                })
                this.handlePickerSelectData(data,index);
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                }, () => {
                    window.CustomPicker.hide()
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

    // 点击召唤 按钮
    submitBtnClick(){
        let params = {
            "fromStationId": "",
            "fromWaitTime": 0,

            "toStationId": "",
            "toWaitTime": 0,

            "toNotVerify": true,
            "toOpenCode": "",
        }
        if(!this.state.pickUpAddressId){
            ToastManager.show('请选择取物站点')
            return
        }
        if(!this.state.deliverAddressId){
            ToastManager.show('请选择送物站点')
            return
        }
        if(this.state.pickUpAddressId.stationId == this.state.deliverAddressId.stationId){
            ToastManager.show('取物站点不能与送物站点相同')
            return
        }
        if(!this.state.codeSwitch){
            if(!this.state.verifiCodeText){
                ToastManager.show('请输入取件验证码')
                return
            }else if(this.state.verifiCodeText.length < 4){
                ToastManager.show('请输入4位取件验证码')
                return
            }
        }
        params.fromStationId = this.state.pickUpAddressId.stationId
        params.fromWaitTime = this.state.pickUpTime
        params.toStationId = this.state.deliverAddressId.stationId
        params.toWaitTime = this.state.deliverTime
        params.toNotVerify = this.state.codeSwitch
        if(!this.state.codeSwitch){
            params.toOpenCode = this.state.verifiCodeText
        }
        this.requestCreateRobotTask(params)
    }

    renderPickUpItem(){
        let pickUpAddress = this.state.pickUpAddress ? this.state.pickUpAddress : '请选择取物点'

        return(
            <View style={styles.item}>
                <Text style={styles.itemTitle}>请选择取物站点并设置停留时间</Text>
                <View style={styles.subWrapper}>
                    <TouchableOpacity activeOpacity={0.7} style={[styles.selectWrapper,{flex: 7}]} onPress={()=>{
                        let selectedValue = []
                        let adrData = this.state.pickUpAddressId
                        if(adrData){
                            selectedValue = [adrData.floor,adrData.room,adrData.stationName]
                        }
                        this.setState({
                            modalVisible: true,
                            pickerType: 1,
                            pickerData: this.state.address_Arr,
                            selectedValue: selectedValue,
                            pickerTitle: '取物站点'
                        })
                    }}>
                        <Text style={styles.siteText}>{pickUpAddress}</Text>
                        <Image style={styles.arrow} source={require('../../../images/panel/robot/arrow.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} style={[styles.selectWrapper,styles.timeWrapper]} onPress={()=>{
                        this.setState({
                            modalVisible: true,
                            pickerType: 3,
                            pickerData: this.state.time_Arr,
                            selectedValue: [this.state.pickUpTime + 'min'],
                            pickerTitle: '取物站点停留时间'
                        })
                    }}>
                        <Text style={styles.siteText}>{this.state.pickUpTime}min</Text>
                        <Image style={styles.arrow} source={require('../../../images/panel/robot/arrow.png')}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    renderDeliverItem(){
        let deliverAddress = this.state.deliverAddress ? this.state.deliverAddress : '请选择送物点'

        return(
            <View style={[styles.item,{marginTop: 8}]}>
                <Text style={styles.itemTitle}>请选择送物站点并设置停留时间</Text>
                <View style={styles.subWrapper}>
                    <TouchableOpacity activeOpacity={0.7} style={[styles.selectWrapper,{flex: 7}]} onPress={()=>{
                        let selectedValue = []
                        let adrData = this.state.deliverAddressId
                        if(adrData){
                            selectedValue = [adrData.floor,adrData.room,adrData.stationName]
                        }
                        this.setState({
                            modalVisible: true,
                            pickerType: 2,
                            pickerData: this.state.address_Arr,
                            selectedValue: selectedValue,
                            pickerTitle: '送物站点'
                        })
                    }}>
                        <Text style={styles.siteText}>{deliverAddress}</Text>
                        <Image style={styles.arrow} source={require('../../../images/panel/robot/arrow.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.7} style={[styles.selectWrapper,styles.timeWrapper]} onPress={()=>{
                        this.setState({
                            modalVisible: true,
                            pickerType: 4,
                            pickerData: this.state.time_Arr,
                            selectedValue: [this.state.deliverTime + 'min'],
                            pickerTitle: '送物站点停留时间'
                        })
                    }}>
                        <Text style={styles.siteText}>{this.state.deliverTime}min</Text>
                        <Image style={styles.arrow} source={require('../../../images/panel/robot/arrow.png')}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    renderSwicthButton(){
        return(
            <SwitchButton
                style={{width: 50, height: 30}}
                value = {this.state.codeSwitch}
                onPress = {()=>{
                    this.setState({
                        codeSwitch: !this.state.codeSwitch
                    })
                }}
            />
        )
    }

    renderVerifiCodeView(){
        if(this.state.codeSwitch){
            return null
        }
        return(
            <TouchableOpacity onPress={()=>{ this._input.focus() }} activeOpacity={1} underlayColor='transparent'>
                <View style={styles.passItemWrapper} >
                    <TextInput
                        value={this.state.verifiCodeText}
                        style={styles.hiddenTextinput}
                        ref={(c) => this._input = c}
                        maxLength={4}    
                        keyboardType="number-pad"
                        onChangeText={(text)=>{
                            this.setState({
                                verifiCodeText: text
                            })
                        }}
                    />
                    <View style={styles.unitItem}>
                        <Text style={styles.passText}>{this.state.verifiCodeText.substr(0,1)}</Text>
                    </View>
                    <View style={styles.unitItem}>
                        <Text style={styles.passText}>{this.state.verifiCodeText.substr(1,1)}</Text>
                    </View>
                    <View style={styles.unitItem}>
                        <Text style={styles.passText}>{this.state.verifiCodeText.substr(2,1)}</Text>
                    </View>
                    <View style={styles.unitItem}>
                        <Text style={styles.passText}>{this.state.verifiCodeText.substr(3,1)}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    renderVerificationCodeItem(){
        return(
            <View style={[styles.item,{marginTop: 8}]}>
                <Text style={styles.itemTitle}>无验证码取件</Text>
                <View style={styles.subWrapper}>
                    <Text style={styles.verifiCode}>开启后，取件时无需输入验证码即可开箱取物</Text>
                    {this.renderSwicthButton()}
                </View>
                {this.renderVerifiCodeView()}
            </View>
        )
    }

    renderBottomButton(){
        return(
            <View style={styles.bottomWrapper}>
                <TouchableOpacity activeOpacity={0.7} style={styles.bottomTouch} onPress={()=>{
                    this.submitBtnClick()
                }}>
                    <Text style={styles.bottomTitle}>召唤</Text>
                </TouchableOpacity>
            </View>
        )
    }

    // 获取选择框的 Modal
	getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
	}

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={styles.container}>
                <ScrollView>
                    {this.renderPickUpItem()}
                    {this.renderDeliverItem()}
                    {this.renderVerificationCodeItem()}
                </ScrollView>
                {this.renderBottomButton()}
                {this.getModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#F5F6F7',
        flex: 1
    },
    item:{
        width: '100%',
        marginTop: 12,
        backgroundColor: Colors.white,
        paddingHorizontal:24,
        paddingVertical:16
    },
    selectWrapper:{
        flex: 7,
        height: 40,
        borderRadius: 8,
        borderColor: '#EBEDF0',
        borderWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 8,
        paddingRight: 4
    },
    timeWrapper:{
        flex: 2,
        marginLeft: 12
    },
    siteText:{
        fontSize: 14,
        color: '#878787',
        flex: 1
    },
    arrow:{
        width: 18,
        height: 10,
        resizeMode: 'contain'
    },
    itemTitle:{
        fontSize: 14, 
        color: '#242424'
    },
    subWrapper:{
        marginTop: 10, 
        flexDirection:'row',
        alignItems: 'center'
    },
    verifiCode:{
        color: '#BFBFBF',
        fontSize: 12,
        flex: 1
    },
    bottomWrapper:{
        paddingBottom:BottomSafeMargin + 20
    },
    bottomTouch:{
        marginHorizontal: 16,
        height: 48,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 24,
        backgroundColor: '#1983FC'
    },
    bottomTitle:{
        color: Colors.white,
        fontSize: 20,
        fontWeight: '500'
    },
    passItemWrapper:{
        width: '100%',
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    unitItem:{
        width: 48, 
        height:48, 
        borderRadius: 8, 
        borderWidth: 1, 
        borderColor: '#EBEDF0',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 8
    },
    passText:{
        fontSize: 28,
        fontWeight: '500',
        color: '#323233'
    },
    hiddenTextinput:{
        height:48,
        zIndex:99,
        position:'absolute',
        width: 48 * 4 + 8 * 3,
        opacity:0
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo
    })
)(RobotCall)

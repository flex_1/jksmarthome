/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    DeviceEventEmitter,
    Keyboard,
    Image,
    Dimensions,
    ScrollView,
    RefreshControl,
    Modal,
    Alert,
    BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from '../../../common/CustomComponent/SpinnerManager';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderTitle from '../Navigation/HeaderTitle';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderRight from '../Navigation/HeaderRight';
import { Colors,NetUrls } from '../../../common/Constants';
import { BottomSafeMargin,StatusBarHeight,NavigationBarHeight } from '../../../util/ScreenUtil';
import { postJson } from '../../../util/ApiRequest';
import Slider from "react-native-slider";
import WebView from 'react-native-webview';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class PanelSetting extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} disableNight={true} onClick={()=>{
            navigation.getParam('goBack') && navigation.getParam('goBack')()
        }}/>,
        headerTitle: <HeaderTitle title={'面板设置'} disableNight={true}/>,
        headerBackground: <HeaderBackground disableNight={true} hiddenBorder={true}/>,
        gesturesEnabled: false
    })

    constructor(props) {
        super(props);
        const { getParam,setParams } = props.navigation;

        this.deviceId = getParam('deviceId')
        this.state = {
            rgbs: [
                {name: 'R', bg: '#FF0000'},
                {name: 'G', bg: '#00DF3E'},
                {name: 'B', bg: '#008CFF'}
            ],
            name: '',
            colorModal: false,
            selectIndex: 0,
            rgbData: [
                [0,0,0],
                [0,0,0],
                [0,0,0],
                [0,0,0]
            ],
            isChanged: false
        }

        setParams({
            goBack: this.goBack.bind(this)
        })
    }

    componentDidMount(){
        this.requestPanelData()
        BackHandler.addEventListener('hardwareBackPress',this.onBackButtonPressAndroid);
    }

    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
    }

    // 安卓物理返回按钮
    onBackButtonPressAndroid = ()=>{
        if(this.state.isChanged){
            this.goBack()
            return true
        }
		return false
	}

    goBack(){
        const { pop } = this.props.navigation

        if(this.state.isChanged){
            Alert.alert(
                '提示',
                '您有灯光设置未保存，是保存后退出？',
                [
                    { text: '直接退出', onPress: () => { pop() }},
                    { text: '保存后退出', onPress: () => { this.requestSaveLight(true) }},
                    { text: '取消', onPress: () => { }, style: 'cancel'},
                ]
            ) 
        }else{
            pop()
        }
    }

    // 获取背光灯信息
    async requestPanelData(){
		try {
			let data = await postJson({
				url: NetUrls.getWifiPanelRGBW,
				params: {
                    deviceId: this.deviceId
                }
			});
			if (data.code == 0) {
                let lightData = JSON.parse(data.result) || {}
                const rgbw_0 = lightData.rgbw_0.split(',')
                const rgbw_1 = lightData.rgbw_1.split(',')
                const rgbw_2 = lightData.rgbw_2.split(',')
                const rgbw_3 = lightData.rgbw_3.split(',')
                this.setState({
                    rgbData: [rgbw_0,rgbw_1,rgbw_2,rgbw_3]
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    // 保存面板
    async requestSaveLight(isSave){
        if(isSave){
            SpinnerManager.show()
        }
		try {
            const rgbw_0 = this.state.rgbData[0].toString()
            const rgbw_1 = this.state.rgbData[1].toString()
            const rgbw_2 = this.state.rgbData[2].toString()
            const rgbw_3 = this.state.rgbData[3].toString()

            let params = {}
            if(isSave){
                params.rgbw_0 = rgbw_0
                params.rgbw_1 = rgbw_1
                params.rgbw_2 = rgbw_2
                params.rgbw_3 = rgbw_3
            }else{
                params.rgbw_debug = this.state.rgbData[this.state.selectIndex].toString()
            }
            
			let data = await postJson({
				url: NetUrls.setWifiPanelRGBW,
				params: {
                    deviceId: this.deviceId,
                    isSave: isSave,
                    ...params
                }
			});
            SpinnerManager.close()
			if (data.code == 0) {
                this.state.isChanged = true
                if(isSave){
                    ToastManager.show('完成设置')
                    this.props.navigation.pop()
                }
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    onMessage(colorValue){
        this.setState({
            colorModal: false
        },()=>{
            if(colorValue && colorValue.includes('#')){
                let colorRGB = colorChange.hexToRgb(colorValue)
                console.log(colorRGB);

                let newRgb = [colorRGB.r, colorRGB.g, colorRGB.b]
                this.state.rgbData[this.state.selectIndex] = newRgb
                this.setState({
                    rgbData : this.state.rgbData
                },()=>{
                    this._complete()
                })
            }
        })
    }

    _onChange(value, index){
        let rgb = this.state.rgbData[this.state.selectIndex]
        rgb[index] = value
        this.state.rgbData[this.state.selectIndex] = rgb
        this.setState({
            rgbData : this.state.rgbData
        })
    }

    _complete =()=>{
        this.requestSaveLight(false)
    }

    renderNameItem(){
        return(
            <View style={styles.nameItem}>
                <Text style={styles.itemTitle}>按键名称</Text>
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={styles.input}
                        value={this.state.searchText}
                        placeholder={'请输入按键名称'}
                        placeholderTextColor={Colors.themeBGInactive}
                        returnKeyType={'done'}
                        clearButtonMode={'while-editing'}
                        maxLength={16}
                        onChangeText={(text) => {
                            this.setState({
                                searchText : text
                            })
                        }}
                    />
                </View>
                <Text style={styles.subTips}>为该设备命名以方便您更好的区分与定位您的设备，比如玄关面板，主卧面板等</Text>
            </View>
        )
    }

    renderLights(){
        const lightsData = [{name: '灯开'},{name: '灯关'},{name: '待机开'},{name: '待机关'}]

        const lights = lightsData.map((value, index)=>{
            const bg = index == this.state.selectIndex ? '#C8C9CC' : Colors.white
            const textColor = index == this.state.selectIndex ? Colors.white : '#969799'

            return(
                <View key={index} style={styles.btnTouch}>
                    <TouchableOpacity style={[styles.lightBtn,{backgroundColor: bg}]} onPress={()=>{
                        this.setState({
                            selectIndex: index
                        })
                    }}>
                        <Text style={{color: textColor, fontSize: 14}}>{value.name}</Text>
                    </TouchableOpacity>
                </View>
            )
        })

        return(
            <View style={styles.lightWrapper}>
                {lights}
            </View>
        )
    }

    renderSliders(){
        const sliders = this.state.rgbs.map((value, index)=>{
            
            const rgb = this.state.rgbData[this.state.selectIndex]
            let rgbValue = parseInt(rgb[index]) || 0

            return(
                <View key={'rgbs_'+index} style={styles.sliderWrapper}>
                    <Text style={{fontSize:16,color: Colors.themeTextBlack}}>{value.name}</Text>
                    <Slider
					    style={styles.slider}
					    minimumValue={0}
					    maximumValue={255}
					    step={1}
          			    value={rgbValue}
					    onValueChange={(e)=> this._onChange(e, index)}
                        onSlidingComplete={this._complete}
					    minimumTrackTintColor={value.bg}
					    maximumTrackTintColor={'#DDDDDD'}
					    thumbTintColor={'#FFFFFF'}
					    trackStyle={{height:6,borderRadius: 3}}
					    thumbStyle={styles.thumbStyle}
        		    />
                    <View style={styles.valueWrapper}>
                        <Text style={{fontSize:14, color: '#969799'}}>{rgbValue}</Text>
                    </View>
                </View>
            )
        })
        return(
            <View style={{marginTop: 16}}>
                {sliders}
            </View>
        )
    }

    renderColorPanelBtn(){
        return(
            <View style={{width: '100%',marginTop: 20,justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity style={{width: 120, height: 44, justifyContent:'center',alignItems:'center'}} onPress={()=>{
                    this.setState({
                        colorModal: true
                    })
                }}>
                    <Text style={{fontSize: 15, color: '#4A75F5', fontWeight:'500'}}>调色盘选色</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderLightPanelItem(){
        return(
            <View style={styles.panelItem}>
                <Text style={[styles.itemTitle,{marginLeft: 16}]}>面板灯光设置</Text>
                {this.renderLights()}
                {this.renderSliders()}
                {this.renderColorPanelBtn()}
                {/* <Text style={styles.bottomTips}>您可以为面板设置不同状态下的灯光展示，待机开将在设备无操作10S后进入，如果要关闭灯光，您可将RGB都设置为0。</Text> */}
                {this.renderBottomBtn()}
            </View>
        )
    }

    renderBottomBtn(){
        return(
            <View style={styles.bottom}>
                <TouchableOpacity activeOpacity={0.7} style={styles.bottomTouch} onPress={()=>{
                    this.requestSaveLight(true)
                }}>
                    <Text style={{fontSize: 18, color: Colors.white}}>完成设置</Text>
                </TouchableOpacity>
            </View>
        )
    }

    _renderColorPanel(){
        return(
            <View style={{flex: 1}}>
                <WebView
                    startInLoadingState={true}
                    scrollEnabled = {false}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    source={{uri: NetUrls.colorPanel}}
                    originWhitelist={['*']}
                    scalesPageToFit={false}
                    onMessage={event => this.onMessage(event.nativeEvent.data) }
                />
			</View>
        )
    }

    renderColorModal(){
        return(
            <Modal 
                transparent={true}
                visible={this.state.colorModal}
                animationType={'none'}
            >
                <TouchableOpacity
                    activeOpacity={1}
                    style={{flex: 4,backgroundColor: Colors.translucent}}
                    onPress={()=>{
                        this.setState({colorModal: false})
                    }}
                />
                <View style={styles.panelBg}>
                    {this._renderColorPanel()}
                </View>
            </Modal>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {/* {this.renderNameItem()} */}
                {this.renderLightPanelItem()}
                {this.renderColorModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#F6F7F8',
        flex: 1
    },
    nameItem:{
        marginTop: 16,
        width: '100%',
        paddingHorizontal: 16,
        paddingVertical: 13,
        backgroundColor: Colors.white
    },
    input:{
        flex:1,
        paddingHorizontal:12,
        fontSize: 14,
        color: Colors.themeTextBlack
    },
    inputWrapper:{
        width: '100%',
        height: 40,
        backgroundColor: '#F6F6F8',
        borderRadius: 4,
        marginTop: 10
    },
    itemTitle:{
        fontSize: 16,
        color: Colors.themeTextBlack
    },
    subTips:{
        marginTop: 8,
        fontSize: 12,
        color: Colors.themeTextLightGray,
        lineHeight: 17
    },
    panelItem:{
        marginTop: 16,
        width: '100%',
        paddingTop: 13,
        backgroundColor: Colors.white,
        flex: 1
    },
    lightWrapper:{
        marginTop: 12,
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
        paddingHorizontal: 14
    },
    btnTouch:{
        width: '25%', 
        height: 32, 
        paddingHorizontal: 2,
        marginBottom: 10
    },
    lightBtn:{
        width: '100%', 
        height:'100%',
        backgroundColor: '#C8C9CC',
        justifyContent:'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#C8C9CC',
        borderRadius: 16
    },
    slider:{
        flex: 1,
        marginHorizontal:  16
    },
    sliderWrapper:{
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 16
    },
    valueWrapper:{
        width: 48, 
        height: 24, 
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F6F6F8',
        borderRadius: 4
    },
    thumbStyle:{
        width:16,
        height:16,
        borderRadius:8,
        borderWidth:0.5,
        borderColor:'#008CFF'
    },
    bottomTips:{
        marginTop: 20,
        fontSize: 12,
        color: '#969799',
        lineHeight: 17,
        marginHorizontal: 16
    },
    bottom:{
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    bottomTouch:{
        width: 176,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        backgroundColor: '#4A75F5'
    },
    panelBg:{
        backgroundColor:Colors.white,
        flex: 6,
        paddingBottom: BottomSafeMargin + 10
    }
});

export default PanelSetting

const colorChange = {
    rgbToHex: function (val) {  //RGB(A)颜色转换为HEX十六进制的颜色值
        var r, g, b, a,
            regRgba = /rgba?\((\d{1,3}),(\d{1,3}),(\d{1,3})(,([.\d]+))?\)/,    //判断rgb颜色值格式的正则表达式，如rgba(255,20,10,.54)
            rsa = val.replace(/\s+/g, '').match(regRgba);
        if (!!rsa) {
            r = parseInt(rsa[1]).toString(16);
            r = r.length == 1 ? '0' + r : r;
            g = (+rsa[2]).toString(16);
            g = g.length == 1 ? '0' + g : g;
            b = (+rsa[3]).toString(16);
            b = b.length == 1 ? '0' + b : b;
            a = (+(rsa[5] ? rsa[5] : 1)) * 100;
            return { hex: '#' + r + g + b, r: parseInt(r, 16), g: parseInt(g, 16), b: parseInt(b, 16), alpha: Math.ceil(a) };
        } else {
            return { hex: '无效', alpha: 100 };
        }
    },
    hexToRgb: function (val) {   //HEX十六进制颜色值转换为RGB(A)颜色值
        // 16进制颜色值的正则
        var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
        // 把颜色值变成小写
        var color = val.toLowerCase();
        var result = '';
        if (reg.test(color)) {
            // 如果只有三位的值，需变成六位，如：#fff => #ffffff
            if (color.length === 4) {
                var colorNew = "#";
                for (var i = 1; i < 4; i += 1) {
                    colorNew += color.slice(i, i + 1).concat(color.slice(i, i + 1));
                }
                color = colorNew;
            }
            // 处理六位的颜色值，转为RGB
            var colorChange = [];
            for (var i = 1; i < 7; i += 2) {
                colorChange.push(parseInt("0x" + color.slice(i, i + 2)));
            }
            result = "rgb(" + colorChange.join(",") + ")";
            return { rgb: result, r: colorChange[0], g: colorChange[1], b: colorChange[2] };
        } else {
            result = '无效';
            return { rgb: result };
        }

    }
};

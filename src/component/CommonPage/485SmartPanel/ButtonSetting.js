/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    DeviceEventEmitter,
    Keyboard,
    Image,
    Dimensions,
    ScrollView,
    RefreshControl
} from 'react-native';
import { connect } from 'react-redux';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderTitle from '../Navigation/HeaderTitle';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderRight from '../Navigation/HeaderRight';
import { Colors,NetUrls } from '../../../common/Constants';
import { BottomSafeMargin,StatusBarHeight,NavigationBarHeight } from '../../../util/ScreenUtil';
import { postJson } from '../../../util/ApiRequest';
import style from '../../../third/echarts/style';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class ButtonSetting extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} disableNight={true}/>,
        headerTitle: <HeaderTitle title={navigation.getParam('title')} disableNight={true}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

    constructor(props) {
        super(props);
        
        this.state = {
            
        }
    }

    componentDidMount(){
        
    }

    componentWillUnmount(){
        
    }

    renderMainImg(){
        return(
            <View style={{marginTop: 48, justifyContent: 'center',alignItems: 'center'}}>
                <Image style={{width: screenW*0.6,height: screenW*0.6}} source={require('../../../images/485/485.jpg')}/>
            </View>
        )
    }

    renderButtons(){
        const { navigate } = this.props.navigation
        const listData = ['左一','左二','左三','左四','右一','右二','右三','右四']

        const list = listData.map((value, index)=>{
            return(
                <TouchableOpacity key={index} activeOpacity={0.7} style={styles.item} onPress={()=>{
                    if(index == 0){
                        navigate('PanelSetting')
                    }else if(index == 1){
                        navigate('ButtonSettingDetail',{title: value})
                    }
                }}>
                    <Text style={styles.name}>{value}</Text>
                    <Image style={styles.rightArrow} source={require('../../../images/enterLight.png')}/>
                </TouchableOpacity>
            )
        })

        return(
            <View style={styles.bottomWrapper}>
                <ScrollView style={{marginTop: 15}} contentContainerStyle={{paddingBottom: 34}}>
                    {list}
                </ScrollView>
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderMainImg()}
                {this.renderButtons()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#008CFF1A',
        flex: 1
    },
    item:{
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 24,
        height: 48,
        borderBottomColor: '#0000001A',
        borderBottomWidth: 1
    },
    name:{
        fontSize: 16, 
        color: Colors.themeTextBlack,
        flex: 1
    },
    rightArrow:{
        width: 8,
        height: 14,
        resizeMode: 'contain'
    },
    bottomWrapper:{
        marginTop: 70,
        backgroundColor: Colors.white,
        flex: 1,
        borderTopLeftRadius:24,
        borderTopRightRadius:24,
        shadowOffset: { width: 0, height: 1 },
        shadowColor: '#070F26',
        shadowOpacity: 0.15,
        shadowRadius: screenW * 0.1,
        elevation: 5
    }
});

export default ButtonSetting

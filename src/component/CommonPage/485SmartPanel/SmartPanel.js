/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    DeviceEventEmitter,
    Keyboard,
    Image,
    Dimensions,
    ScrollView,
    RefreshControl,
    ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderTitle from '../Navigation/HeaderTitle';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderRight from '../Navigation/HeaderRight';
import { Colors,NetUrls } from '../../../common/Constants';
import { BottomSafeMargin,StatusBarHeight,NavigationBarHeight } from '../../../util/ScreenUtil';
import { postJson } from '../../../util/ApiRequest';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class SmartPanel extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} disableNight={true}/>,
        headerTitle: <HeaderTitle title={navigation.getParam('title')} disableNight={true}/>,
        headerRight: (
            <HeaderRight disableNight={true} buttonArrays={[
                {icon:require('../../../images/panel/robot/more.png'), onPress:()=>{
                    navigation.getParam('settingBtn')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

    constructor(props) {
        super(props);
        const { getParam,setParams } = props.navigation;

        this.deviceData = getParam('deviceData') || {}
        this.attributeType = this.deviceData.attributeTypeName
        this.isZigbeePanel = this.attributeType == 'zigbee-panel'
        
        this.state = {
            ledList: [],
            gatewayId: null
        }

        setParams({
            settingBtn: this.settingBtnClick.bind(this)
        })
    }

    componentDidMount(){
        if(this.isZigbeePanel){
            this.requestZigbeeButtonList()
        }else{
            this.requestDeviceStatus()
        }
    }

    componentWillUnmount(){
        
    }

    settingBtnClick(){
        const { navigate,push,setParams } = this.props.navigation;

        // navigate('GatewaySetting',{
        //     title: this.deviceData.name + '设置',
        //     name: this.deviceData.deviceName,
        //     getwayData: {id: this.state.gatewayId, name: this.deviceData.deviceName},
        //     callBack: (data)=>{
        //         if(data && data.name){
        //             setParams({title: data.name})
        //         }
        //         this.requestDeviceStatus()
        //     }
        // })

        push('DeviceSetting', {
            deviceData: this.deviceData,
            nameCallBack: (name)=>{
                setParams({title: name})
            }
        })
    }

    // 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				this.setState({
                    ledList: data.result?.ledList || [],
                    gatewayId: data.result?.gatewayId
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

    // 获取按键列表
    async requestZigbeeButtonList(){
        try {
			let data = await postJson({
				url: NetUrls.getCurrentZigbeePanelButtonList,
				params: {
                    id: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				this.setState({
                    ledList: data.result?.ledList,
                    gatewayId: data.result?.gatewayId
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    async panelBtnClick(id){
		try {
			let data = await postJson({
				url: NetUrls.panelLedOperation,
				params: {
                    id: id
                }
			});
			if (data.code == 0) {
				ToastManager.show('指令发送成功')
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

    // 控制
    async zigbeeControl(id){
		try {
			let data = await postJson({
                url: NetUrls.controlCommon,
				params: {
                    id: id
                }
			});
			if (data.code == 0) {
				ToastManager.show('指令发送成功')
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    renderMainPanels(){
        let ledList = this.state.ledList.map((value, index)=>{
            const jsc = index%2 == 0 ? {} : {justifyContent: 'flex-end'}
            const bgImg = index%2 == 0 ? require('../../../images/485/bgL.png') : require('../../../images/485/bgR.png')
            const paddingH = index%2 == 0 ? {paddingLeft: 10} : {paddingRight: 10}
            const dots = <Image style={{width:12,height:12,resizeMode:'contain',marginHorizontal: 5}} source={require('../../../images/485/blackDot.png')}/>
            const leftIcon = index%2 == 0 ? dots : null
            const rightIcon = index%2 == 0 ? null : dots

            let splitLine = index%2 == 0 ? <View style={{height:2,marginLeft:5,marginRight:0,backgroundColor: '#4D4D4D'}}/>
            : <View style={{height:2,marginLeft:0,marginRight:5,backgroundColor: '#4D4D4D'}}/>
            if(index > this.state.ledList.length - 3){
                splitLine = null
            }

            return(
                <TouchableOpacity
                    key={'keyboard_'+index} 
                    style={{height:72,width:'50%'}}
                    onPress={()=>{
                        if(this.isZigbeePanel){
                            this.zigbeeControl(value.id)
                        }else{
                            this.panelBtnClick(value.id)
                        }
                    }}
                >
                    <ImageBackground source={bgImg} style={[{width:'100%',height:70,alignItems:'center',flexDirection:'row'},paddingH,jsc]}>
                        {leftIcon}
                        <Text style={{fontSize: 14, fontWeight:'400',color: Colors.themeTextBlack}}>{value.name}</Text>
                        {rightIcon}
                    </ImageBackground>
                    {splitLine}
                </TouchableOpacity>
            )
        })
        return(
            <View style={styles.panelWrapper}>
                <View style={styles.panel}>
                    {ledList}
                </View>
            </View>
        )
    }

    renderButtons(){
        const {navigate} = this.props.navigation

        return(
            <View style={styles.btnWrapper}>
                {/* <TouchableOpacity style={styles.button}>
                    <Text style={styles.btnText}>定时</Text>
                </TouchableOpacity> */}
                <TouchableOpacity style={[styles.button,{marginTop: 16, backgroundColor: '#008CFF'}]} onPress={()=>{
                    if(!this.state.gatewayId){
                        ToastManager.show('数据加载中,请稍后')
                        return
                    }
                    navigate('WifiPanelButtonList',{
                        getwayData: {id: this.state.gatewayId, name: this.deviceData.deviceName},
                        title: this.deviceData.deviceName,
                        showSetting: !this.isZigbeePanel,
                        attributeType: this.attributeType,
                        callBack: () => {
                            if(this.isZigbeePanel){
                                this.requestZigbeeButtonList()
                            }else{
                                this.requestDeviceStatus()
                            }
                        }
                    })
                }}>
                    <Image style={styles.btnSetting} source={require('../../../images/485/btnSettings.png')}/>
                    <Text style={[styles.btnText,{color: Colors.white}]}>按键设置</Text>
                    <Image style={styles.arrow} source={require('../../../images/enter_white.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderMainPanels()}
                {this.renderButtons()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#FFFFFF',
        flex: 1
    },
    panelWrapper:{
        marginTop: screenH * 0.1,
        justifyContent: 'center',
        alignItems:'center'
    },
    panel:{
        flexDirection: 'row',
        flexWrap:'wrap',
        width:'80%'
    },
    btnWrapper:{
        position: 'absolute',
        bottom: 0,
        left:0,
        right:0,
        width: '100%',
        justifyContent: 'center',
        alignItems:'center',
        paddingBottom: BottomSafeMargin + 20
    },
    button:{
        width: '80%',
        height: 48,
        borderRadius: 24,
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#4A75F5',
        paddingHorizontal: 24
    },
    btnText:{
        fontSize: 16,
        fontWeight: '400',
        color: '#4A75F5',
        marginLeft: 10,
        flex: 1
    },
    btnSetting:{
        width: 24,
        height: 22,
        resizeMode: 'contain'
    },
    arrow:{
        width: 8,
        height: 14,
        resizeMode: 'contain'
    }
});

export default SmartPanel

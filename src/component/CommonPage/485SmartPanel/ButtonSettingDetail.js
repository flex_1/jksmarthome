/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    DeviceEventEmitter,
    Keyboard,
    Image,
    Dimensions,
    ScrollView,
    RefreshControl
} from 'react-native';
import { connect } from 'react-redux';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderTitle from '../Navigation/HeaderTitle';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderRight from '../Navigation/HeaderRight';
import { Colors,NetUrls } from '../../../common/Constants';
import { BottomSafeMargin,StatusBarHeight,NavigationBarHeight } from '../../../util/ScreenUtil';
import { postJson } from '../../../util/ApiRequest';
import Slider from "react-native-slider";
import SwitchButton from '../../../common/CustomComponent/SwitchButton';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class ButtonSettingDetail extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} disableNight={true}/>,
        headerTitle: <HeaderTitle title={navigation.getParam('title')} disableNight={true}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

    constructor(props) {
        super(props);
        
        this.state = {
            name: '',
            separate: true
        }
    }

    componentDidMount(){
        
    }

    componentWillUnmount(){
        
    }

    renderTopTips(){
        return(
            <View style={styles.tipsWrapper}>
                <Text style={styles.tipsText}>为按键设置名称和房间以便于语言操作。如按键名称为吊灯，对音箱或小萨APP说“打开吊灯”。</Text>
            </View>
        )
    }

    renderCommandBtns(){
        const commands = ['吊灯','床头灯','吸顶灯','吊灯','床头灯']

        const btns = commands.map((value, index)=>{
            return(
                <TouchableOpacity key={'btn_'+index} activeOpacity={0.7} style={styles.btn} onPress={()=>{
                    this.setState({
                        name: value
                    })
                }}>
                    <Text style={styles.btnText}>{value}</Text>
                </TouchableOpacity>
            )
        })

        return(
            <View style={styles.btnsWrapper}>
                {btns}
            </View>
        )
    }

    renderNameItem(){
        return(
            <View style={styles.nameItem}>
                <Text style={styles.itemTitle}>按键名称</Text>
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={styles.input}
                        value={this.state.name}
                        placeholder={'请输入按键名称'}
                        placeholderTextColor={Colors.themeBGInactive}
                        returnKeyType={'done'}
                        clearButtonMode={'while-editing'}
                        maxLength={16}
                        onChangeText={(text) => {
                            this.setState({
                                name : text
                            })
                        }}
                    />
                </View>
                {this.renderCommandBtns()}
            </View>
        )
    }

    renderBtnFuction(){
        return(
            <View style={styles.functionWrapper}>
                <TouchableOpacity activeOpacity={0.7} style={styles.functionTouch}>
                    <Text style={styles.itemTitle}>按键功能</Text>
                    <View style={{flex: 1}}/>
                    <Text style={styles.extra}>继电器</Text>
                    <Image style={styles.arrowDown} source={require('../../../images/down.png')}/>
                </TouchableOpacity>
                <View style={styles.split}/>
                <View style={styles.separateWrapper}>
                    <View style={styles.swicthWrapper}>
                        <Text style={styles.itemTitle}>按键分离</Text>
                        <View style={{flex: 1}}/>
                        <SwitchButton
                            style={styles.switch}
                            value = {this.state.separate}
                            isAsync = {false}
                            onPress = {()=>{
                                let target = this.state.separate ? false : true
                                this.setState({
                                    separate: target
                                })
                            }}
                        />
                    </View>
                    <View style={styles.functionTipsWrapper}>
                        <Text style={styles.functionTips}>按键分离启用后，该物理按键不再控制电线的通断电，而且变为一个独立的开关，可以绑定场景来控制其他智能设备。</Text>
                    </View>
                </View>
            </View>
        )
    }

    renderRooms(){
        return(
            <View style={styles.functionWrapper}>
                <TouchableOpacity activeOpacity={0.7} style={styles.titleWrapper} onPress={()=>{

                }}>
                    <Text style={styles.itemTitle}>所属房间</Text>
                    <View style={{flex: 1}}/>
                    <Text style={styles.extra}>房间管理</Text>
                    <Image style={styles.rightArrow} source={require('../../../images/enterLight.png')}/>
                </TouchableOpacity>
                <View>
                    
                </View>
            </View>
        )
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                {this.renderTopTips()}
                {this.renderNameItem()}
                {this.renderBtnFuction()}
                {this.renderRooms()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#F6F7F8',
        flex: 1
    },
    tipsWrapper:{
        backgroundColor: '#FFA9001A',
        paddingHorizontal: 16,
        paddingVertical: 10
    },
    tipsText:{
        fontSize: 12,
        color: '#FD6E00',
        lineHeight: 17
    },
    nameItem:{
        width: '100%',
        paddingHorizontal: 16,
        paddingTop: 13,
        paddingBottom: 3,
        backgroundColor: Colors.white
    },
    input:{
        flex:1,
        paddingHorizontal:12,
        fontSize: 14,
        color: Colors.themeTextBlack
    },
    inputWrapper:{
        width: '100%',
        height: 40,
        backgroundColor: '#F6F6F8',
        borderRadius: 4,
        marginTop: 10
    },
    itemTitle:{
        fontSize: 16,
        color: Colors.themeTextBlack
    },
    btnsWrapper:{
        marginTop: 8,
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap'
    },
    btn:{
        paddingHorizontal: 18,
        height: 32,
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#EFF5FF',
        marginRight: 15,
        marginBottom: 10
    },
    btnText: {
        fontSize: 14,
        color: '#4A75F5'
    },
    functionWrapper:{
        marginTop: 16,
        backgroundColor: Colors.white
    },
    functionTouch:{
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 13,
        paddingHorizontal: 16
    },
    extra:{
        fontSize: 14,
        color: '#969799'
    },
    arrowDown:{
        width: 14,
        height: 7,
        resizeMode: 'contain',
        tintColor: '#969799',
        marginLeft: 5
    },
    split:{
        marginHorizontal: 16,
        backgroundColor: '#F5F6F7',
        height: 1
    },
    separateWrapper:{
        paddingVertical: 13
    },
    swicthWrapper:{
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 16
    },
    functionTipsWrapper:{
        paddingHorizontal: 10,
        paddingVertical: 5,
        backgroundColor: '#F9F9F9',
        borderRadius: 4,
        marginTop: 12,
        marginHorizontal: 16
    },
    functionTips:{
        fontSize: 12,
        color: '#646566',
        lineHeight: 17
    },
    switch:{
        width: 80,
        height: 30,
        justifyContent:'center',
        alignItems: 'center',
    },
    titleWrapper: {
        paddingHorizontal: 16,
        paddingVertical: 13,
        flexDirection: 'row',
        alignItems: 'center'
    },
    rightArrow:{
        width: 8,
        height: 14,
        resizeMode: 'contain',
        marginLeft: 5
    },
});

export default ButtonSettingDetail

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
    SectionList,
    TouchableOpacity
} from 'react-native';
import { Colors,NetUrls } from '../../common/Constants';
import ToastManager from '../../common/CustomComponent/ToastManager';
import { postJson } from '../../util/ApiRequest';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import {ListNoContent,FooterEnd,FooterLoading } from "../../common/CustomComponent/ListLoading";
import { connect } from 'react-redux';
import HeaderLeft from './Navigation/HeaderLeft';
import HeaderBackground from './Navigation/HeaderBackground';
import {ImagesLight, ImagesDark} from '../../common/Themes';

class Logs extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
            headerBackground: <HeaderBackground/>
		}
	}

	constructor(props) {
		super(props);
        const {getParam} = this.props.navigation
		this.type = getParam('type') // smart-智能 device-设备 scene-场景
		this.totalPages = 0
		this.state = {
			logData: null,
			loading: false,
            currentPage: 0,
            closeIds: []
		}
	}

	componentDidMount() {
		this.requestSmartLog()
	}

	componentWillUnmount(){
		
    }
    
    getUrl(){
        if(this.type == 'smart'){
            return NetUrls.smartLog
        }else if(this.type == 'scene'){
            return NetUrls.sceneLog
        }else{
            return NetUrls.deviceLog
        }
    }

	// 获取所有日志
    async requestSmartLog(page) {
		if(this.state.loading){
			return
		}
		if( page && page>this.totalPages){
			return
		}
		let params = {}
		if(page){
			params.page = page
		}
		this.setState({loading: true})
        try {
            let data = await postJson({
                url: this.getUrl(),
                params: {
					...params
                }
			});
			this.setState({loading: false})
            if (data.code == 0) {
				let logs = (data.result && data.result.list) || []
				this.totalPages = data.result && data.result.totalPageNum
				this.state.currentPage = data.result && data.result.curPage

				let dataLength = logs.length
				if(dataLength > 0){
					this.handleDataForSectionList(logs)
				}else{
					this.setState({
						logData: []
					})
				}
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
			this.setState({loading: false})
            ToastManager.show('网络错误')
        }
    }
    
    // 停用
    async updateTimerStatus(id) {
		SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.updateTimerStatus,
                params: {
                    status: 2,
                    timerId: id
                }
			});
			SpinnerManager.close()
            if (data.code == 0) {
                this.state.closeIds.push(id)
				this.setState({
                    closeIds: this.state.closeIds
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
			SpinnerManager.close()
            ToastManager.show('网络错误')
        }
	}

	// 数据 拼接
	handleDataForSectionList(logs){
		let dataLength = logs.length
		let logData = []

		let currentDate = logs[0]['date']
		let currentWeek = logs[0]['week']
		let item = {date:currentDate, week:currentWeek, data:[] }

		if(this.state.logData){
			logData = JSON.parse(JSON.stringify(this.state.logData))
			item = logData[logData.length-1]
			logData.splice(logData.length-1,1)
			currentDate = item['date']
			currentWeek = item['week']
        }
        let index = 0
		for (const log of logs) {
			if(log.date == currentDate){
				item.data.push(log)
			}else{
				logData.push(JSON.parse(JSON.stringify(item)))
				currentDate = log['date']
				currentWeek = log['week']
				item = {date:currentDate, week:currentWeek, data:[log] }
			}
			if(index == dataLength-1){
				// 防指针
				logData.push(JSON.parse(JSON.stringify(item)))
            }
            index ++
		}
		this.setState({
			logData: logData
        })
	}

	renderSectionHeader(section){
        const Colors = this.props.themeInfo.colors
        return(
            <View style={[styles.headerWrapper,{backgroundColor: Colors.themeBaseBg}]}>
				<Text style={[styles.headerTitle,{color:Colors.themeText}]}>{section.date}</Text>
				<Text style={{fontSize:14,color:Colors.themeText}}>{section.week}</Text>
			</View>
        )
    }

	renderRowItem(rowData, index, section){
        const Colors = this.props.themeInfo.colors
		let timeColor = rowData.perform ? Colors.themeButton : Colors.themeTextLight
        let titleColor = rowData.perform ? Colors.themeButton : Colors.themeText
        let dot = rowData.perform ? require('../../images/settingIcon/record_front_active.png') 
        : require('../../images/settingIcon/record_front.png')
        let stopTouch = null

        if(rowData.timerId && this.state.closeIds.includes(rowData.timerId)){
            stopTouch = (
                <View style={[styles.btnTouch,{borderColor:Colors.themeTextLight,borderWidth: 1}]}>
                    <Text style={{color:Colors.themeTextLight,fontSize:13}}>已停用</Text>
                </View>
            )
        }else if(rowData.perform){
            stopTouch = (
                <TouchableOpacity style={[styles.btnTouch,{backgroundColor: Colors.themeButton}]} onPress={()=>{
                    this.updateTimerStatus(rowData.timerId)
                }}>
                    <Text style={{color:Colors.white,fontSize:13}}>停用</Text>
                </TouchableOpacity>
            )
        }
        return(
            <View style={[styles.itemWrapper,{backgroundColor: Colors.themeBg}]}>
				<Text style={{fontSize:13,lineHeight:18,width:65,color:timeColor}}>{rowData.time}</Text>
				<Image style={{width: 12, resizeMode:'contain',height:'100%',marginLeft: 5}} source={dot}/>
				<View style={{marginLeft: 20,flex:1,paddingRight:10}}>
					<Text style={{marginTop:5,fontSize:15,lineHeight:18,color:titleColor}}>{rowData.name}</Text>
					<Text numberOfLines={2} style={{fontSize:13,color:timeColor,marginTop:10}}>{rowData.memo}</Text>
				</View>
                {stopTouch}
			</View>
        )
    }

	getLogListView(){
		let data = this.state.logData
		if(!data){
			return(
				<View style={{alignItems:'center',marginTop:'50%',flex: 1,}}>
					<Text style={{color:Colors.themeBGInactive,fontSize:13}}>加载中...</Text>
				</View>
			)
		}
		if(data.length <= 0){
            const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
			return(
				<ListNoContent img={Images.noLog} text={'暂无日志'}/>
			)
		}
		let Footer = FooterLoading
        if(this.state.currentPage >= this.totalPages && !this.state.loading){
            Footer = FooterEnd
		}

		return(
			<SectionList
				style={{flex: 1}}
				contentContainerStyle={{ paddingBottom: 50}}
				stickySectionHeadersEnabled={true}
  				renderItem={({ item, index, section }) => this.renderRowItem(item, index, section)}
  				renderSectionHeader={({ section }) => this.renderSectionHeader(section)}
				sections={data}
				scrollIndicatorInsets={{right: 1}}
				initialNumToRender={5}
				keyExtractor={(item, index) => 'smart_log'+index}
				onEndReached={()=>{this.requestSmartLog(this.state.currentPage+1)}}
                onEndReachedThreshold={0.1}
                ListFooterComponent={Footer}
			/>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getLogListView()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex:1,
	},
	scrollContent:{
		paddingBottom: 50
	},
	item:{
		width: '100%',
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:20,
		paddingHorizontal: 20,
	},
	headerWrapper:{
		height:60,
		alignItems:'center',
		flexDirection:'row',
		paddingHorizontal:16
	},
	headerTitle:{
		fontSize:14,
		marginLeft: 20,
		fontWeight:'bold',
		flex: 1
    },
    btnTouch:{
        paddingHorizontal:15,
        height:28,
        borderRadius: 14,
        justifyContent: 'center',
        alignItems:'center',
        marginRight: 10
    },
    itemWrapper:{
        paddingHorizontal:20,
        height:80,
        flexDirection: 'row',
        alignItems:'center'
    }
});


export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Logs)

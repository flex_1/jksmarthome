/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StatusBar,
    View,
    DeviceEventEmitter,
    Modal,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    ScrollView,
    Alert,
    NativeModules,
    BackHandler
} from 'react-native';
import { postJson, } from '../../util/ApiRequest';
import { NetUrls, LocalStorageKeys } from '../../common/Constants';
import { Colors } from '../../common/Constants';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import { BottomSafeMargin } from '../../util/ScreenUtil';

class SmartVoiceBoxLocation extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={'位置管理'}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text:'保存', onPress:()=>{
                    navigation.getParam('saveBtn')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })
    
    constructor(props) {
        super(props);
        const { getParam,setParams } = this.props.navigation
        
        this.callBack = getParam('callBack')
        this.deviceId = getParam('deviceId')
        
        this.state = {
            floorList: [{floor: null}],
            roomList: null,
            floor: getParam('floor'),
            floorText: getParam('floorText'),
            
            roomNames: [],
            roomIds: getParam('roomIds') || [],
            
            selectIndex: null,
            showSelector: false
        }

        setParams({
            saveBtn: this.requestSaveRoomSetting.bind(this),
        })
    }

    componentDidMount() {
        this.requestRoomList()
        
    }

    componentWillUnmount() {
        
    }

    // 获取房间 列表
    async requestRoomList() {
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.roomList,
				params: {
                    type: 3
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                let dataList =  data.result || []
                dataList.unshift({floor: null, floorText:'整屋',listRoom:[]})

                let rooms = null
                for (const floors of dataList) {
                    if(floors.floor != null && floors.floor == this.state.floor ){
                        rooms = floors.listRoom
                        break;
                    }
                }
                this.setState({
                    floorList: dataList,
                    roomList: rooms || []
                })
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 保存房间设置
    async requestSaveRoomSetting() {
        let location = {}
        let locationParams = null
        if(this.state.floor){
            location.floor = this.state.floor
        }
        if(this.state.roomIds?.length > 0){
            location.roomIds = this.state.roomIds.toString()
        }
        if(location.floor || location.roomIds){
            locationParams = JSON.stringify(location)
        }
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.saveDevice,
				params: {
                    id: this.deviceId,
                    location: locationParams
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                const { pop } = this.props.navigation
                this.callBack && this.callBack()
                pop()
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }
    
    renderExpand(){
        if(!this.state.showSelector){
            return null
        }
        
        const Colors = this.props.themeInfo.colors
        let floors = this.state.floorList.map((value, index)=>{
            
            let btnBg = value.floor == this.state.floor ? Colors.newBtnBlueBg : Colors.themeBaseBg
            let textColor =  value.floor == this.state.floor ? Colors.white : Colors.themeText
            let floorText = value.floor == null ? '整屋' : value.floor + '楼'

            return (
                <View key={index} style={styles.floorsWrapper}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={[styles.floorBtn,{backgroundColor:btnBg}]} 
                        onPress={()=>{
                            this.setState({
                                floor: value.floor,
                                selectIndex: index,
                                showSelector: false,
                                roomList: value.listRoom,
                                roomIds: [],
                                roomNames: []
                            })
                        }}
                    >
                        <Text style={{fontSize: 14,textAlign:'center',color: textColor}}>{floorText}</Text>
                    </TouchableOpacity>
                </View>
            )
        })
        return(
            <View style={[styles.btnWrapper,{backgroundColor: Colors.themeBg}]}>
                {floors}
            </View>
        )
    }

    renderFloorSelector(){
        const Colors = this.props.themeInfo.colors

        let floor = this.state.floor ? this.state.floor + '楼' : '整屋' 
        
        return(
            <View style={styles.titleWrapper}>
                <Text style={[styles.title,{color: Colors.themeText}]}>请选择楼层</Text>
                <TouchableOpacity
                    activeOpacity={0.7} 
                    style={[styles.floorTouch,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        this.setState({
                            showSelector: !this.state.showSelector
                        })
                    }}
                >
                    <Image style={styles.floorImg} source={require('../../images/voice/floor.png')}/>
                    <Text style={[styles.floorText,{color: Colors.themeText}]}>{floor}</Text>
                    <View style={{flex: 1}}/>
                    <Image style={[styles.arrowDown,{tintColor: Colors.themeTextLight}]} source={require('../../images/down.png')}/>
                </TouchableOpacity>
                {this.renderExpand()}
            </View>
        )
    }

    renderRooms(){
        if(!this.state.roomList){
            return null
        }
        const Colors = this.props.themeInfo.colors
        let rooms = this.state.roomList.map((value,index)=>{
            let s_index = this.state.roomIds.indexOf(value.id)

            let btnBg = s_index == -1 ? Colors.themeBg : Colors.newBtnBlueBg
            let textColor =  s_index == -1 ? Colors.themeText : Colors.white
            
            return( 
                <View key={index} style={styles.floorsWrapper}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={[styles.floorBtn,{backgroundColor: btnBg}]} 
                        onPress={()=>{
                            if(s_index == -1){
                                this.state.roomIds.push(value.id)
                                this.state.roomNames.push(value.name)
                            }else{
                                this.state.roomIds.splice(s_index, 1)
                                this.state.roomNames.splice(s_index, 1)
                            }
                            this.setState({
                                ...this.state
                            })
                        }}
                    >
                        <Text numberOfLines={2} style={{fontSize: 13,color: textColor,textAlign:'center'}}>{value.name}</Text>
                    </TouchableOpacity>
                </View>
            )
        })
        return(
            <View style={[styles.btnWrapper,{paddingHorizontal: 0, flex: 1}]}>
                {rooms}
            </View>  
        )
    }

    renderRoomSelector(){
        if(this.state.showSelector){
            return null
        }
        if(!this.state.roomList || !this.state.roomList.length){
            return null
        }
        const Colors = this.props.themeInfo.colors
        return(
            <View style={[styles.titleWrapper,{flex: 1}]}>
                <Text style={[styles.title,{color: Colors.themeText}]}>请选择房间</Text>
                {this.renderRooms()}
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors
        return (
            <ScrollView style={[styles.container,{backgroundColor: Colors.themeBaseBg}]} contentContainerStyle={styles.content}>
                {this.renderFloorSelector()}
                {this.renderRoomSelector()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1
    },
    content:{
        paddingBottom: BottomSafeMargin + 30
    },
    title:{
        fontSize: 18,
        fontWeight: 'bold',
    },
    itemWrapper:{
        paddingLeft: 16,
        marginTop: 10,
        paddingVertical: 10
    },
    switchWrapper:{
        flexDirection: 'row',
        alignItems: 'center'
    },
    titleWrapper:{
        marginTop: 20,
        marginBottom: 10,
        paddingHorizontal: 16
    },
    floorTouch:{
        marginTop: 15,
        width: '100%',
        height: 40,
        borderRadius: 4,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 16
    },
    floorImg:{
        width: 26,
        height: 19,
        resizeMode: 'contain'
    },
    floorText:{
        fontSize: 16,
        marginLeft: 15
    },
    arrowDown:{
       width: 14,
       height: 7,
       resizeMode: 'contain' 
    },
    expandWrapper:{
        marginTop: 5,
        width: '100%',
        height: 1
    },
    floorsWrapper:{
        width: '33%',
        height: 50, 
        justifyContent:'center',
        alignItems:'center',
        padding: 5
    },
    floorBtn:{
        width:'100%',
        height:'100%',
        justifyContent:'center',
        alignItems:'center',
        borderRadius: 4,
        overflow: 'hidden',
        paddingHorizontal: 5
    },
    btnWrapper:{
        width:'100%',
        flexDirection:'row',
        flexWrap:'wrap',
        marginTop: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    checkIcon: {
        position: 'absolute',
        top: -2,
        right: -2,
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    switchTouch:{
        paddingHorizontal: 20,
        height: '100%',
        alignItems: 'center'
    },
    switchImg:{
        width:50,
        height:40,
        resizeMode:'contain'
    },
    subDetail:{
        fontSize: 13,
        marginTop: 5
    },
    questionBtn:{
        paddingHorizontal:15,
        height: 40,
        justifyContent:'center',
        alignItems:'center'
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SmartVoiceBoxLocation)

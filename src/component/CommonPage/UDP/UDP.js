import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    Alert,
    TextInput,
    Dimensions,
    NativeModules
} from 'react-native';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import { connect } from 'react-redux';
import { NotificationKeys,NetUrls,Colors } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import { postJson,postPicture } from "../../../util/ApiRequest";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderRight from '../Navigation/HeaderRight';
import HeaderBackground from '../Navigation/HeaderBackground';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import dgram from 'react-native-udp'
import { Buffer } from 'buffer';

const { StringDecoder } = require('string_decoder');
const decoder = new StringDecoder('utf8');
const screenH =  Dimensions.get('window').height;
const JKConnectWifiUtils = NativeModules.JKConnectWifiUtils;

const TimeCount = 60  // 超时时间
const RemotePort = 8266  // 目标端口
const RemoteHost = '192.168.4.1'  //目标ip
const LocalPort = 8266   // 本地端口
const IntervalTime = 3   //每隔几（秒），发送一次

class UDP extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'UDP'}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text: '调试', onPress:()=>{
                    navigation.navigate('UDPDebug')
                }},
            ]}/>
        ),
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
        super(props);
        const { getParam } = props.navigation
        
        this.socket = null

        this.ssid = getParam('ssid')
        this.password = getParam('password')
        this.wifiDeviceType = getParam('wifiDeviceType')

		this.state = {
            remarkText: null,
            recieveContent: '',
            udpAvailable: false,
            isConnectting: false,
            timer: 0,
            connectStatus: 0,  //0: 休闲  1:正在连接   2:连接成功  3:连接超时(失败)
		};
	}

	componentDidMount() {
        
    }

    startConnect(){
        if(this.state.isConnectting){
            return
        }
        this.state.isConnectting = true
        
        this.socket = dgram.createSocket('udp4')
        this.socket.once('listening', ()=>{
            this.setState({
                udpAvailable: true,
                connectStatus: 1
            })

            this.sendInterVal = setInterval(() => {
                // 60秒无回应显示超时
                if(this.state.timer >= TimeCount){
                    this.showTimeOut()
                    return
                }
                this.state.timer += IntervalTime;
                this.sendMessage()
            }, IntervalTime*1000);
        })
        this.socket.on('message', (msg, rinfo) => {
            this.recieveMessage(msg, rinfo)
        })
        try {
            this.socket.bind(LocalPort)
        } catch (error) {
            Alert('绑定端口时失败。')
        }
    }
    
    componentWillUnmount() {
        this.sendInterVal && clearInterval(this.sendInterVal)
        this.socket?.close()
    }
    
    // 发送消息
    sendMessage(){
        if(!this.state.udpAvailable){
            ToastManager.show('Udp初始化未完成。')
            return
        }
        try {
            const sendMsg = {
                "cmdType": 1,
                "ssid": this.ssid,
                "password": this.password,
                "token":"b266c906a96f4afffbc396618a8ed547",
                "topic":""
            } 
            const message = Buffer.from(JSON.stringify(sendMsg));
            this.socket.send(message, 0, undefined, RemotePort, RemoteHost, (err) => {
                if (err) {
                    this.setState({
                        recieveContent: this.state.recieveContent + '发送数据时出错: ' + JSON.stringify(err) + '\r\n'
                    })
                }
            })
        } catch (error) {
            this.setState({
                recieveContent: this.state.recieveContent + '数据出错。' + '\r\n'
            })
        }
    }

    //接收消息
    recieveMessage(msg, rinfo){
        console.log('Message received', msg)

        try {
            const bufferMessage = Buffer.from(msg);
            const message = decoder.write(bufferMessage)
            this.setState({
                recieveContent: this.state.recieveContent + message + '\r\n'
            })

            const res = JSON.parse(message)
            if(res.result == 0){
                this.showTimeOut()
            }else if(res.result == 2){
                this.showSuccess()
            }else if(res.result == 3){
                this.showSuccess()
            }
        } catch (error) {
            this.setState({
                recieveContent: this.state.recieveContent + '收到错误的数据格式。' + '\r\n'
            })
        }
    }

    // 显示超时
    showTimeOut(){
        alert('连接超时')
        this.setState({
            connectStatus: 3
        })
        this.state.isConnectting = false
        this.state.timer = 0
        this.socket.close()
        this.sendInterVal && clearInterval(this.sendInterVal);
    }

    //显示成功
    showSuccess(){
        alert('连接成功')
        this.setState({
            connectStatus: 2
        })
        this.state.isConnectting = false
        this.state.timer = 0
        this.socket.close()
        this.sendInterVal && clearInterval(this.sendInterVal);
    }

    renderTips(){
        return(
            <View style={{paddingVertical:5,marginTop: 20}}>
                <Text style={{fontSize: 14, color: Colors.themeTextBlack,marginHorizontal: 16}}>
                    请连接设备发出的热点后，回到此页面，点击【开始连接】后方可配网。
                </Text>
                <TouchableOpacity activeOpacity={0.7} style={styles.bottomButton} onPress={()=>{
                    JKConnectWifiUtils.gotoSystemWifiList()
                }}>
                    <Text style={{fontSize: 15, color: Colors.white}}>切换WiFi</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderRecieve(){
        return(
            <View style={{marginTop:5,paddingVertical:5,paddingHorizontal: 16, marginTop: 20,height:screenH*0.2}}>
                <ScrollView style={{width:'100%',backgroundColor:'white'}}>
                    <Text style={{fontSize:15,color: Colors.themeTextBlack}}>{this.state.recieveContent}</Text>
                </ScrollView>
            </View>
        )
    }

    renderStatus(){
        //0: 休闲  1:正在连接   2:连接成功  3:连接超时(失败)
        let label = '休闲中'
        if(this.state.connectStatus == 1){
            label = '连接中...'
        }else if(this.state.connectStatus == 2){
            label = '连接成功!'
        }else if(this.state.connectStatus == 3){
            label = '连接失败(超时)。'
        }

        return(
            <Text style={{marginTop: 20,color: Colors.tabActiveColor,fontSize: 15,marginLeft: 16}}>{label}</Text>
        )
    }

    renderSendButton(){
        return(
            <TouchableOpacity style={styles.bottomButton} onPress={()=>{
                this.startConnect()
            }}>
                <Text style={{fontSize: 16,color: Colors.white}}>开始连接</Text>
            </TouchableOpacity>
        )
    }
	
	render() {
        const Colors = this.props.themeInfo.colors

		return (
            <View style={{backgroundColor: Colors.themeBaseBg}} >
                {this.renderTips()}
                {this.renderRecieve()}
                {this.renderStatus()}
                {this.renderSendButton()}
            </View>
		)
	}
}

const styles = StyleSheet.create({
	input:{
        fontSize: 15
    },
    inputWrapper:{
        borderBottomColor: Colors.themeTextLightGray,
        borderBottomWidth: 1,
        marginLeft: 20,
        paddingVertical: 5,
        width: '80%'
    },
    bottomButton:{
        marginHorizontal: 16,
        height: 40,
        backgroundColor: Colors.themeTextBlue,
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5
    },
    remarkInputWrapper:{
        width: '100%',
        borderRadius: 5,
        paddingVertical: 5
    },
    remarkInput:{
        width:'100%',
		height:80,
		paddingHorizontal:16,
      	textAlignVertical:'top',
		fontSize:14,
      	borderRadius:4,
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(UDP)

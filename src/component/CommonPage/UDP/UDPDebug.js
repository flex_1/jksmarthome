import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    Alert,
    TextInput,
    Dimensions
} from 'react-native';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import { connect } from 'react-redux';
import { NotificationKeys,NetUrls,Colors } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import { postJson,postPicture } from "../../../util/ApiRequest";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderRight from '../Navigation/HeaderRight';
import HeaderBackground from '../Navigation/HeaderBackground';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import dgram from 'react-native-udp'

import { Buffer } from 'buffer';
const { StringDecoder } = require('string_decoder');
const decoder = new StringDecoder('utf8');

const screenH =  Dimensions.get('window').height;

let socket = null

var STATE = {
    UNBOUND: 0,
    BINDING: 1,
    BOUND: 2
  }

class UDPDebug extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'UDP'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>,
    })

	constructor(props) {
        super(props);
        const { getParam } = props.navigation
        
        this.client = null
		this.state = {
            serverPort: '8266',  // 5188
            serverHost: '192.168.1.9',  // 10.20.30.66
            connectStatus: false,
            remarkText: null,
            recieveContent: '',
            udpAvailable: false
		};
	}

	componentDidMount() {
        socket = dgram.createSocket('udp4')
        socket.once('listening', ()=>{
            this.setState({
                udpAvailable: true
            })
        })
        socket.on('message', (msg, rinfo) => {
            console.log('Message received', msg)

            const bufferMessage = Buffer.from(msg);
            const message = decoder.write(bufferMessage)
            this.setState({
                recieveContent: this.state.recieveContent + message + '\r\n'
            })
        })
        socket.bind(12345)
    }
    
    componentWillUnmount() {
        socket.close()
    }
    
    // 发送消息
    sendMessage(){
        if(!this.state.udpAvailable){
            ToastManager.show('Udp初始化未完成。')
            return
        }
        let remotePort = parseInt(this.state.serverPort)
        let remoteHost = this.state.serverHost

        const message = Buffer.from(this.state.remarkText);

        socket.send(message, 0, undefined, remotePort, remoteHost, function(err) {
            if (err) throw err
        })
    }

    renderHostInput(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={{flexDirection:'row',paddingHorizontal:16,paddingVertical:10,alignItems:'center'}}>
                <Text style={{fontSize: 16}}>Host: </Text>
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={styles.input}
                        defaultValue={this.state.serverHost}
                        placeholder="Host地址"
                        clearButtonMode={'while-editing'}
                        placeholderTextColor={Colors.themeInactive}
                        onChangeText={(text) => {
                            this.state.serverHost = text
                        }}
                    />
                </View>
            </View>
        )
    }

    renderPortInput(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={{flexDirection:'row',paddingHorizontal:16,paddingVertical:10,alignItems:'center'}}>
                <Text style={{fontSize: 16}}>Port : </Text>
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={styles.input}
                        defaultValue={this.state.serverPort}
                        maxLength={4}
                        placeholder="端口号"
                        clearButtonMode={'while-editing'}
                        placeholderTextColor={Colors.themeInactive}
                        onChangeText={(text) => {
                            this.state.serverPort = text
                        }}
                    />
                </View>
                
            </View>
        )
    }

    renderRemarksInput(){
        const Colors = this.props.themeInfo.colors;
        return(
            <View style={{marginTop:5,paddingVertical:5,paddingHorizontal: 16, marginTop: 20}}>
                <View style={[styles.remarkInputWrapper,{backgroundColor: Colors.themeBg}]}>
                    <TextInput
						style={[styles.remarkInput,{color: Colors.themeText}]}
						placeholder={'请输入发送的文字'}
						placeholderTextColor={Colors.themeInactive}
						multiline={true}
                        blurOnSubmit={false}
                        autoFocus={false}
                        returnKeyLabel={'换行'}
                        returnKeyType={'default'}
                        maxLength={200}
                        underlineColorAndroid="transparent"
						onChangeText={(text) => {
							this.state.remarkText = text
						}}
					/>
                </View>
            </View>
        )
    }

    renderSendButton(){
        return(
            <TouchableOpacity style={styles.bottomButton} onPress={()=>{
                Keyboard.dismiss()
                if(!this.state.remarkText){
                    return
                }
                this.sendMessage()
            }}>
                <Text style={{fontSize: 16,color: Colors.white}}>发送</Text>
            </TouchableOpacity>
        )
    }

    renderRecieve(){
        return(
            <View style={{marginTop:5,paddingVertical:5,paddingHorizontal: 16, marginTop: 20,height:screenH*0.3}}>
                <ScrollView style={{width:'100%',backgroundColor:'white'}}>
                    <Text style={{fontSize:14,color: Colors.themeTextBlack}}>{this.state.recieveContent}</Text>
                </ScrollView>
            </View>
        )
    }
	
	render() {
		const Colors = this.props.themeInfo.colors

		return (
            <KeyboardAwareScrollView 
                enableOnAndroid={true}
                extraScrollHeight={120}                   
                keyboardShouldPersistTaps="handled"
                style={{backgroundColor: Colors.themeBaseBg}} 
            >
                {this.renderHostInput()}
                {this.renderPortInput()}
                {this.renderRemarksInput()}
                {this.renderSendButton()}
                {this.renderRecieve()}
            </KeyboardAwareScrollView>
		)
	}
}

const styles = StyleSheet.create({
	input:{
        fontSize: 15
    },
    inputWrapper:{
        borderBottomColor: Colors.themeTextLightGray,
        borderBottomWidth: 1,
        marginLeft: 20,
        paddingVertical: 5,
        width: '80%'
    },
    bottomButton:{
        marginHorizontal: 16,
        height: 40,
        backgroundColor: Colors.themeTextBlue,
        marginTop: 15,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5
    },
    remarkInputWrapper:{
        width: '100%',
        borderRadius: 5,
        paddingVertical: 5
    },
    remarkInput:{
        width:'100%',
		height:80,
		paddingHorizontal:16,
      	textAlignVertical:'top',
		fontSize:14,
      	borderRadius:4,
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(UDPDebug)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ImageBackground,
    TextInput,
    ScrollView,
} from 'react-native';

import { WebView } from 'react-native-webview';
import {postJson} from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { NetUrls,Colors } from '../../../common/Constants';
import LocalTokenHandler from '../../../util/LocalTokenHandler';

class Camera extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: (
                <TouchableOpacity style={styles.navTouch} onPress={() => { 
                    navigation.getParam('goBackWeb')() 
                }}>
                    <Image style={styles.navImg} source={require('../../../images/back.png')} />
                </TouchableOpacity>
            ),
            headerRight: <View/>,
            title: '监控',
            headerTitleStyle: {
				fontSize: 20,
				color: Colors.themeTextBlack,
				textAlign: 'center',
				flex: 1,
			}
        }
    };

    constructor(props) {
        super(props);
        const {getParam,setParams,goBack} = this.props.navigation

        this.deviceId = getParam('deviceId')
        this.state = {
            canGoback: false,
            url: null
        }
        setParams({
            goBackWeb: ()=>{
                if(this.state.canGoback){
                    this.question_webview?.goBack()
                }else{
                    goBack()
                }
            }
        });
    }

    componentDidMount() {
        this.getUserToken()
    }

    componentWillUnmount() {
        
    }

    // 获取用户Token
    async getUserToken(){
        let tokens = await LocalTokenHandler.get()
        // tokens = 'df804d8330a945d887ce52af49e33721'
        this.setState({
            url: NetUrls.hsvideo + '?token='+ tokens.token +'&deviceId=' + this.deviceId
        })
    }

    onNavigationStateChange = (navState)=>{
        this.setState({
            canGoback: navState.canGoBack
        })
    }

    getWebview(){
        if(!this.state.url){
            return null
        }
        console.log(this.state.url)
        return(
            <WebView
                ref={e => this.question_webview = e}
                source={{ uri: this.state.url }}
                style={styles.container}
                containerStyle={styles.scorllContent}
                startInLoadingState={true}
                showsVerticalScrollIndicator={false}
                allowsInlineMediaPlayback={true}
                onNavigationStateChange={this.onNavigationStateChange}
            />
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.getWebview()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scorllContent:{
        marginTop: 10,
    },
    navTouch:{ 
        paddingLeft: 15,
        paddingRight:20,
        flexDirection:'row', 
        alignItems: 'center',
        height:'100%' 
    },
    navImg:{ 
        width: 10, 
        height: 18,
        resizeMode:'contain' 
    }
});

export default Camera

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    View,
} from 'react-native';
import { connect } from 'react-redux';
import { WebView } from 'react-native-webview';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import { BottomSafeMargin } from '../../../util/ScreenUtil'; 

class Authorize extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerBackground: <HeaderBackground/>
    })

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation

        this.callBack = getParam('callBack')
        
        this.state = {
            url: getParam('url'),
            backLock: false
        }
    }

    componentDidMount() {
        
    }

    componentWillUnmount() {
        
    }

    onNavigationStateChange(navState) {
        console.log("navState:  " + JSON.stringify(navState));
        if(navState.url.indexOf("about:blank")!=-1){
            if(!this.state.backLock){
                this.state.backLock = true
                this.props.navigation.goBack()
            }
            this.callBack && this.callBack()
        }
    }

    getWebview(){
        return(
            <WebView
                style={styles.container}
                containerStyle={styles.scorllContent}
                source={{ uri: this.state.url }}
                contentContainerStyle={styles.scrollContent}
                mixedContentMode='always'
                onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                // onMessage={(e) => {
                //     alert(e.nativeEvent.data)
                // }}
            />
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.getWebview()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    scorllContent:{
        paddingBottom: BottomSafeMargin + 20
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Authorize)

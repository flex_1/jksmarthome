/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    ScrollView,
    SwipeableFlatList,
    Alert,
    NativeModules,
    RefreshControl
} from 'react-native';
import {postJson} from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { NetUrls,Colors } from '../../../common/Constants';
import {ListNoContent} from "../../../common/CustomComponent/ListLoading";
import {NetParams} from '../../../common/Constants';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import {ImagesLight, ImagesDark} from '../../../common/Themes';

const JKCameraRNUtils = Platform.select({
    ios: NativeModules.JKCameraRNUtils,
    android: NativeModules.JKCameraAndRNUtils
})

//侧滑最大距离
const maxSwipeDistance = 180
//侧滑按钮个数
const countSwiper = 2

class CameraList extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'安防监控'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text:'授权',onPress:()=>{
                        navigation.getParam('authorizeBtn')()
                    }},
                    {icon:require('../../../images/qrScan.png'),onPress:()=>{
                        navigation.getParam('addCameraClick')()
                    }}
                ]}/>
            ),
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
	}

    constructor(props) {
        super(props);
        const {getParam,setParams,navigate} = this.props.navigation

        this.state = {
            cameraData: null,
            tips: '',
            trustUrl: null,
            isShowTips: true,
            isRefreshing: false
        }

        setParams({
            addCameraClick: ()=>{
                navigate('AddDeviceQRCode', {
                    callBack:()=>{
                        this.getCameraList()
                    }
                })
            },
            authorizeBtn: ()=>{
                if(!this.state.trustUrl) return;
                navigate('Authorize',{url: this.state.trustUrl, title:'授权'})
            }
        })
    }

    componentDidMount() {
        SpinnerManager.show()
        this.getCameraList()
    }

    // 获取摄像头列表
    async getCameraList(){
		try {
			let data = await postJson({
				url: NetUrls.cameraList,
				params: {
				}
			});
            SpinnerManager.close()
            this.setState({isRefreshing: false})
			if (data && data.code == 0) {
                this.setState({
                    cameraData: data.result.cameraList || [],
                    tips: data.result.tips || '',
                    trustUrl: data.result.trustUrl || ''
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            this.setState({isRefreshing: false})
			SpinnerManager.close()
			ToastManager.show('网络错误');
		}
    }

    // 停用摄像头
	async deleteCamera(id){
		SpinnerManager.show()
		try {
			let deviceData = await postJson({
				url: NetUrls.deletDevice,
				params: {
					id: id,
					type: 1 // 1 停用, 2  启用 
				}
			});
			SpinnerManager.close()
			if (deviceData.code == 0) {
				ToastManager.show('摄像头已停用')
				this.getCameraList()
			} else {
				ToastManager.show(deviceData.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }
    
    onRefresh(){
        if (this.state.isRefreshing) return
        this.setState({ isRefreshing: true })
        this.getCameraList()
    }

    //关闭侧滑栏
	closeSwiper() {
		this.swiperFlatlist?.setState({ openRowKey: null })
	}

    //侧滑菜单渲染
	getQuickActions(rowData, index) {
		const { navigate } = this.props.navigation
		return (
			<View style={styles.quickAContent}>
                <TouchableOpacity activeOpacity={0.7} onPress={() => {
                    this.closeSwiper()
                    navigate('DeviceSetting',{
                        deviceData:rowData,
                        isFromManager: true,
                        nameCallBack: ()=>{
                            this.getCameraList()
                        }
                    })
				}}>
					<View style={[styles.quick, { backgroundColor: Colors.tabActiveColor }]}>
						<Text style={{ color: Colors.white, fontSize: 14 }}>设置</Text>
					</View>
				</TouchableOpacity>
				<TouchableOpacity activeOpacity={0.7} onPress={() => {
					this.closeSwiper()
					Alert.alert(
						'提示',
						'确认停用该摄像头?',
						[
							{ text: '取消', onPress: () => { }, style: 'cancel' },
							{ text: '停用', onPress: () => { this.deleteCamera(rowData.deviceId) } },
						]
					)
				}}>
					<View style={[styles.quick, { backgroundColor: Colors.themBgRed,borderTopRightRadius:5,borderBottomRightRadius:5 }]}>
						<Text style={{ color: Colors.white, fontSize: 14 }}>停用</Text>
					</View>
				</TouchableOpacity>
			</View>
		)
	}

    // 渲染 Item
    renderRowItem(rowData, index){
        const Colors = this.props.themeInfo.colors
        
        let floorText = null
        if(rowData.floorText){
            floorText = (
                <Text style={{marginTop:10,fontSize:12,color:Colors.themeTextLight}}>{rowData.floorText+'  '+rowData.roomName}</Text>
            )
        }

        let lineText = rowData.isOnLine ? '在线' : '离线'
        let lineColor = rowData.isOnLine ? Colors.themeButton : Colors.themeTextLight

        return(
            <View style={{width: '100%', paddingHorizontal: 16, marginTop: 20}}>
                <TouchableOpacity activeOpacity={1} style={[styles.item,{backgroundColor:Colors.themeBg}]} onPress={()=>{
                    this.closeSwiper()
                    let serialStr = rowData?.serialNumber
                    let appKey = rowData?.ysAppKey
                    let accessToken = rowData?.ysAccessToken
                    let cameraNo = rowData?.cameraNo || 0
                    let cameraName = rowData?.deviceName || '视频监控'

                    if (Platform.OS === 'android') {
                        JKCameraRNUtils.openCameraView(serialStr,appKey,accessToken,cameraNo,cameraName,NetParams.baseUrl);
                    }else{
                        JKCameraRNUtils.openCameraView(serialStr,appKey,accessToken,cameraNo+1,cameraName,this.props.themeInfo.isDark);
                    }
                }}>
                    <Image style={{width:125,height:70,resizeMode:'contain'}} source={require('../../../images/user_manage/camereBg.png')}/>
                    <View style={{marginLeft:20}}>
                        <Text style={{fontSize:15,color:Colors.themeText}}>{rowData.deviceName}</Text>
                        {floorText}
                    </View>
                    <Text style={[styles.onlineText,{color: lineColor}]}>{lineText}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderTips(){
        if(!this.state.isShowTips){
            return null
        }
        return(
            <View style={styles.tipsWrapper}>
                <Text style={styles.tipsText}>{this.state.tips}</Text>
                <TouchableOpacity style={styles.closeTouch} onPress={()=>{
                    this.setState({ isShowTips: false })
                }}>
                    <Image style={{width:20,height:20}} source={require('../../../images/close.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    //列表无内容
    renderNoContent(){
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight

        return(
            <View style={{flex: 1}}>
                {this.renderTips()}
                <ListNoContent
                    img={Images.noDevice} 
                    text={'暂无监控设备'}
                />
            </View>
        )
    }

    _extraUniqueKey(item, index) {
		return "camera_list_index_" + index;
    }

    getCameraListView() {
        if(!this.state.cameraData){
            return null
        }
        return (
            <SwipeableFlatList
                ref={e => this.swiperFlatlist = e}
                contentContainerStyle={{ paddingBottom: 50 }}
                scrollIndicatorInsets={{ right: 1 }}
                removeClippedSubviews={false}
                data={this.state.cameraData}
                keyExtractor={this._extraUniqueKey}
                ListEmptyComponent = {this.renderNoContent()}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
                renderQuickActions={({ item, index }) => this.getQuickActions(item, index)}//创建侧滑菜单
                maxSwipeDistance={maxSwipeDistance}
                bounceFirstRowOnMount={false}
                initialNumToRender={10}
                refreshControl={
                    <RefreshControl
                        colors={[Colors.themeBGInactive]}
                        tintColor={Colors.themeBGInactive}
                        refreshing={this.state.isRefreshing}
                        onRefresh={this.onRefresh.bind(this)}
                    />
                }
            />
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.getCameraListView()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 16,
    },
    navImg: {
        width: 18,
        height: 18,
        resizeMode:'contain'
    },
    item:{
        width:'100%',
        height: 70,
        borderRadius: 4,
        flexDirection:'row',
        alignItems:'center'
    },
    //侧滑菜单的样式
	quickAContent: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginRight: 16,
		marginTop: 20,
        overflow: 'hidden',
        paddingVertical: 1
	},
	quick: {
		backgroundColor: Colors.white,
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		width: maxSwipeDistance / countSwiper,
    },
    onlineText:{
        position: 'absolute',
        top: 12,
        right: 16,
        fontSize: 13
    },
    tipsWrapper:{
        flexDirection:'row' ,
        paddingVertical: 5,
        backgroundColor: Colors.themeTipsYellow,
        paddingLeft:16,
        alignItems: 'center'
    },
    tipsText:{
        fontSize: 14, 
        lineHeight: 16,
        color: Colors.themeTextBlack,
        width:'80%'
    },
    closeTouch:{
        height:40,
        width:'20%',
        justifyContent:'center',
        alignItems:'center'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(CameraList)

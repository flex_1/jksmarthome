import PCtrPanel1 from './PCtrPanel1';
import PCtrPanel2 from './PCtrPanel2';
import PCtrPanel3 from './PCtrPanel3';
import PCtrPanel4 from './PCtrPanel4';
import PCtrPanelCustom from "./PCtrPanelCustom";

export default {
    PCtrPanel1,
    PCtrPanel2,
    PCtrPanel3,
    PCtrPanel4,
    PCtrPanelCustom
}

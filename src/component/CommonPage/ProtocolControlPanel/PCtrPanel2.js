/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	StyleSheet,
	Image,
	Text,
	TouchableOpacity,
	Dimensions,
    ImageBackground,
    View,
    ScrollView
} from 'react-native';
import { BottomSafeMargin } from '../../../util/ScreenUtil';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const bgW = screenW*0.8
const bgH = screenW*0.8*1.4
const btnW = bgW * 0.18

class PCtrPanel2 extends Component {

    constructor(props) {
		super(props);
        
        this.state = {
            
        }
    }

    btnOnClick(index, isCustomBtn){
        this.props.btnClick && this.props.btnClick(index, isCustomBtn)
    }

    renderMainController(){
        return(
            <ImageBackground style={styles.ctrBg} source={require('../../../images/ProtocolControl/bg1.png')}>
                <View style={styles.lineBtnWrapper}>
                    <TouchableOpacity onPress={()=>{
                        this.btnOnClick(0, false)
                    }}>
                        <ImageBackground style={styles.btn2} source={require('../../../images/ProtocolControl/btn2.png')}>
                            <Text style={styles.btnText}>ON</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{
                        this.btnOnClick(1, false)
                    }}>
                        <ImageBackground style={[styles.btn2]} source={require('../../../images/ProtocolControl/btn2.png')}>
                            <Text style={styles.btnText}>OFF</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>
                <ImageBackground style={styles.btn3}  resizeMode={'contain'} source={require('../../../images/ProtocolControl/btn3.png')}>
                    <TouchableOpacity style={styles.btn3Touch} onPress={()=>{
                        this.btnOnClick(2, false)
                    }}>
                        <Image style={styles.btnIMg1} source={require('../../../images/ProtocolControl/voiceAdd.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.btn3Touch,{justifyContent: 'flex-end'}]} onPress={()=>{
                        this.btnOnClick(3, false)
                    }}>
                        <Image style={styles.btnIMg2} source={require('../../../images/ProtocolControl/voiceDecrease.png')}/>
                    </TouchableOpacity>
                </ImageBackground>
            </ImageBackground>
        )
    }

	render() {
		return(
            <View style={styles.mianCtrWrapper}>
                {this.renderMainController()}
            </View>
        )
	}
}

const styles = StyleSheet.create({
    content:{
        paddingBottom: BottomSafeMargin + 20
    },
    ctrBg:{
        width: bgW, 
        height: bgH, 
        justifyContent:'center',
        alignItems:'center'
    },
    lineBtnWrapper:{
        flexDirection: 'row',
        width: '60%',
        justifyContent: 'space-between'
    },
    btn2:{
        width: btnW,
        height: btnW,
        resizeMode: 'contain',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btn3:{
        width: btnW,
        height: btnW * 2.82,
        marginTop: bgH * 0.1
    },
    btn3Touch:{
        flex: 1,
        width: '100%',
        alignItems: 'center'
    },
    btnIMg1:{
        width: btnW * 0.4,
        height: btnW * 0.4,
        resizeMode: 'contain',
        marginTop: btnW * 0.4
    },
    btnIMg2:{
        width: btnW * 0.4,
        height: btnW * 0.4,
        resizeMode: 'contain',
        marginBottom: btnW * 0.4
    },
    btnText:{
        color: '#646566',
        fontSize: 16,
        fontWeight: '400'
    },
    mianCtrWrapper:{
        marginTop: screenH*0.1, 
        flex: 1,
        width: '100%',
        alignItems: 'center'
    }
});

export default PCtrPanel2



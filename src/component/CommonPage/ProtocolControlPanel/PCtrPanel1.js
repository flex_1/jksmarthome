/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	Text,
	TouchableOpacity,
	Dimensions,
    ImageBackground,
    ScrollView
} from 'react-native';
import { BottomSafeMargin } from '../../../util/ScreenUtil';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const bgW = screenW*0.8
const bgH = screenW*0.8*1.4

class PCtrPanel1 extends Component {

    constructor(props) {
		super(props);
        
        this.btnClick = props.btnClick
        this.state = {
            btnParams : props.btnParams
        }
    }

    btnOnClick(index, isCustomBtn){
        this.props.btnClick && this.props.btnClick(index, isCustomBtn)
    }

    renderMainController(){
        return(
            <ImageBackground style={styles.ctrBg} source={require('../../../images/ProtocolControl/bg1.png')}>
                <TouchableOpacity onPress={()=>{ this.btnOnClick(0, false) }}>
                    <ImageBackground style={styles.ctrBtnBg} source={require('../../../images/ProtocolControl/btn1.png')}>
                        <Text style={styles.btnText}>ON</Text>
                    </ImageBackground>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{ this.btnOnClick(1, false) }}>
                    <ImageBackground style={[styles.ctrBtnBg,{marginTop: bgH*0.1}]} source={require('../../../images/ProtocolControl/btn1.png')}>
                        <Text style={styles.btnText}>OFF</Text>
                    </ImageBackground>
                </TouchableOpacity>
            </ImageBackground>
        )
    }

	render() {
		return(
            <View style={styles.mianCtrWrapper}>
                {this.renderMainController()}
            </View>
        )
	}
}

const styles = StyleSheet.create({
    ctrBg:{
        width: bgW, 
        height: bgH, 
        justifyContent:'center',
        alignItems:'center'
    },
    ctrBtnBg:{
        width: bgW*0.48, 
        height: bgW*0.48*0.36,
        justifyContent:'center',
        alignItems:'center',
        resizeMode: 'contain'
    },
    btnText:{
        color: '#646566',
        fontSize: 16,
        fontWeight: '400'
    },
    mianCtrWrapper:{
        marginTop: screenH*0.1, 
        flex: 1,
        width: '100%',
        alignItems: 'center'
    },
});

export default PCtrPanel1



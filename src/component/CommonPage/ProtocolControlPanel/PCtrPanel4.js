/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	StyleSheet,
	Image,
	Text,
	TouchableOpacity,
	Dimensions,
    ImageBackground,
    View,
    ScrollView
} from 'react-native';
import { BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const bgW = screenW*0.8
const bgH = screenW*0.8*1.9
const btnW = bgW * 0.18

class PCtrPanel4 extends Component {

    constructor(props) {
		super(props);

        this.isSetting = props.isSetting

        this.state = {
            panelState: 0,  // 1-上 2-左 3-下 4-右,
        }
    }

    btnOnClick(index, isCustomBtn){
        this.props.btnClick && this.props.btnClick(index, isCustomBtn)
    }

    renderMainPanelImg(){
        let img = require('../../../images/ProtocolControl/mainPanel.png')
        if(this.state.panelState == 1){
            img = require('../../../images/ProtocolControl/top.png')
        }else if(this.state.panelState == 2){
            img = require('../../../images/ProtocolControl/left.png')
        }else if(this.state.panelState == 3){
            img = require('../../../images/ProtocolControl/bottom.png')
        }else if(this.state.panelState == 4){
            img = require('../../../images/ProtocolControl/right.png') 
        }
        return img
    }

    renderCenterTouch(){
        const mainPanelImg = this.renderMainPanelImg()

        return(
            <ImageBackground style={[styles.center,styles.spaceHeight]} source={mainPanelImg}>
                <TouchableOpacity style={styles.centerTouch} onPress={()=>{
                    this.btnOnClick(4, false)
                }}>
                    <ImageBackground style={styles.centerImg} source={require('../../../images/ProtocolControl/center.png')}>
                        <Text style={styles.okText}>OK</Text>
                    </ImageBackground>
                </TouchableOpacity>
                <TouchableOpacity style={styles.topT} onPressOut={()=>{this.setState({panelState: 0})}}
                    onPress={()=>{
                        this.btnOnClick(2, false)
                    }}
                    onPressIn={()=>{
                        this.setState({panelState: 1})
                    }}
                />
                <TouchableOpacity style={styles.leftT} onPressOut={()=>{this.setState({panelState: 0})}}
                    onPress={()=>{
                        this.btnOnClick(3, false)
                    }}
                    onPressIn={()=>{
                        this.setState({panelState: 2})
                    }}
                />
                <TouchableOpacity style={styles.bottomT} onPressOut={()=>{this.setState({panelState: 0})}}
                    onPress={()=>{
                        this.btnOnClick(6, false)
                    }}
                    onPressIn={()=>{
                        this.setState({panelState: 3})
                    }}
                />
                <TouchableOpacity style={styles.rightT} onPressOut={()=>{this.setState({panelState: 0})}}
                    onPress={()=>{
                        this.btnOnClick(5, false)
                    }}
                    onPressIn={()=>{
                        this.setState({panelState: 4})
                    }}
                />
            </ImageBackground>
        )
    }

    rednerCustomBtns(){
        let btns = this.props.customButtonList.map((value, index)=>{
            let deleteIcon = null
            if(this.props.isDeleting){
                deleteIcon = <Image style={styles.delete} source={require('../../../images/ProtocolControl/delete.png')}/>
            }
            return(
                <TouchableOpacity key={index} style={styles.customBtnTouch} onPress={()=>{
                    this.btnOnClick(index, true)
                }}>
                    <ImageBackground style={styles.btn2} source={require('../../../images/ProtocolControl/btn2.png')}>
                        <Text numberOfLines={2} style={styles.btnText2}>{value.name}</Text>
                    </ImageBackground>
                    {deleteIcon}
                </TouchableOpacity>
            )
        })

        return(
            <View style={styles.customBtnWrapper}>
                {btns}
            </View>
        )
    }

    btnOnClick(index, isCustomBtn){
        this.props.btnClick && this.props.btnClick(index, isCustomBtn)
    }

    rednerAddBtn(){
        if(!this.isSetting){
            return null
        }
        let decreaseText = '-'
        if(this.props.isDeleting){
            decreaseText = '完成'
        }
        return(
            <View style={styles.bottomBtnWrapper}>
                <TouchableOpacity style={styles.addBtn} onPress={()=>{
                    this.props.deleteBtnClick && this.props.deleteBtnClick()
                }}>
                    <Text style={styles.addText}>{decreaseText}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.addBtn} onPress={()=>{
                    if(this.props.customButtonList.length >= 6){
                        ToastManager.show('最多添加6个自定义按钮')
                        return
                    }
                    this.props.addBtnClick && this.props.addBtnClick()
                }}>
                    <Text style={styles.addText}>+</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderMainController(){
        return(
            <ImageBackground style={styles.ctrBg} resizeMode={'contain'} source={require('../../../images/ProtocolControl/bg2.png')}>
                <View style={styles.lineBtnWrapper}>
                    <TouchableOpacity onPress={()=>{
                        this.btnOnClick(0, false)
                    }}>
                        <ImageBackground style={styles.btn2} source={require('../../../images/ProtocolControl/btn2.png')}>
                            <Text style={styles.btnText}>ON</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{
                        this.btnOnClick(1, false)
                    }}>
                        <ImageBackground style={[styles.btn2]} source={require('../../../images/ProtocolControl/btn2.png')}>
                            <Text style={styles.btnText}>OFF</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>
                {this.renderCenterTouch()}
                <View style={[styles.lineBtnWrapper,styles.spaceHeight]}>
                    <TouchableOpacity onPress={()=>{
                        this.btnOnClick(8, false)
                    }}>
                        <ImageBackground style={[styles.btn2]} source={require('../../../images/ProtocolControl/btn2.png')}>
                            <Image style={styles.btnImg} source={require('../../../images/ProtocolControl/voiceDecrease.png')}/>
                        </ImageBackground>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{
                        this.btnOnClick(9, false)
                    }}>
                        <ImageBackground style={[styles.btn2]} source={require('../../../images/ProtocolControl/btn2.png')}>
                            <Image style={styles.btnImg} source={require('../../../images/ProtocolControl/back.png')}/>
                        </ImageBackground>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{
                        this.btnOnClick(7, false)
                    }}>
                        <ImageBackground style={[styles.btn2]} source={require('../../../images/ProtocolControl/btn2.png')}>
                            <Image style={styles.btnImg} source={require('../../../images/ProtocolControl/voiceAdd.png')}/>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>
                {this.rednerCustomBtns()}
            </ImageBackground>
        )
    }

	render() {
		return (
            <ScrollView style={styles.wrapper} scrollIndicatorInsets={{right: 1}}>
                <View style={styles.content}>
                    {this.renderMainController()}
                    {this.rednerAddBtn()}
                </View>
            </ScrollView>
        )
	}
}

const styles = StyleSheet.create({
    wrapper:{
        width: '100%'
    },
    content:{
        width: '100%',
        flex: 1,
        paddingBottom: BottomSafeMargin + 20,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15
    },
    ctrBg:{
        width: bgW, 
        height: bgH,
        justifyContent:'center',
        alignItems:'center'
    },
    lineBtnWrapper:{
        flexDirection: 'row',
        width: '70%',
        justifyContent: 'space-between'
    },
    btnText:{
        color: '#646566',
        fontSize: 16,
        fontWeight: '400'
    },
    btnText2:{
        color: '#646566',
        fontSize: 13,
        fontWeight: '400',
        textAlign: 'center'
    },
    btn2:{
        width: btnW,
        height: btnW,
        resizeMode: 'contain',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5
    },
    center:{
        width: bgW * 0.648,
        height: bgW * 0.648,
        justifyContent: 'center',
        alignItems: 'center'
    },
    centerTouch:{
        width: '40%',
        height: '40%'
    },
    centerImg:{
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    okText:{
        fontSize: 18,
        fontWeight: '500',
        color: '#5E676F'
    },
    topT:{
        position: 'absolute',
        top: 0,
        left: '30%',
        height: '30%',
        width: '40%'
    },
    leftT:{
        position: 'absolute',
        top: '30%',
        left: 0,
        height: '40%',
        width: '30%'
    },
    rightT:{
        position: 'absolute',
        top: '30%',
        right: 0,
        height: '40%',
        width: '30%'
    },
    bottomT:{
        position: 'absolute',
        bottom: 0,
        left: '30%',
        height: '30%',
        width: '40%'
    },
    btnImg:{
        width: btnW * 0.4,
        height: btnW * 0.4
    },
    spaceHeight:{
        marginTop: bgH * 0.04
    },
    addBtn:{
        width: 120, 
        height: 40,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#EDEDED'
    },
    addText:{
        fontWeight: 'bold',
        fontSize: 18,
        color: '#AAAAAA'
    },
    btn4Img:{
        width: bgW * 0.2,
        height: bgW * 0.2 * 0.48,
        justifyContent: 'center',
        alignItems: 'center'
    },
    customBtnWrapper:{
        marginTop: 15,
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '70%',
        flexWrap:'wrap'
    },
    customBtnTouch:{
        marginBottom: 15
    },
    bottomBtnWrapper:{
        marginTop: 10,
        flexDirection: 'row',
        width: '70%',
        justifyContent: 'space-between'
    },
    delete:{
        position: 'absolute',
        width: 20,
        height: 20,
        top: -4,
        right: -4
    }
});

export default PCtrPanel4



/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	StyleSheet,
	Image,
	Text,
	TouchableOpacity,
	Dimensions,
    ImageBackground,
    View,
    ScrollView
} from 'react-native';
import { BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const bgW = screenW*0.8
const bgH = screenW*0.8*1.9
const btnW = bgW * 0.18

class PCtrPanelCustom extends Component {

    constructor(props) {
		super(props);

        this.isSetting = props.isSetting
    }

    btnOnClick(index, isCustomBtn){
        this.props.btnClick && this.props.btnClick(index, isCustomBtn)
    }

    rednerCustomBtns(){
        let btns = this.props.customButtonList.map((value, index)=>{
            let deleteIcon = null
            if(this.props.isDeleting){
                deleteIcon = <Image style={styles.delete} source={require('../../../images/ProtocolControl/delete.png')}/>
            }
            return(
                <TouchableOpacity key={index} style={styles.customBtnTouch} onPress={()=>{
                    this.btnOnClick(index, true)
                }}>
                    <ImageBackground style={styles.btn2} source={require('../../../images/ProtocolControl/btn2.png')}>
                        <Text numberOfLines={2} style={styles.btnText2}>{value.name}</Text>
                    </ImageBackground>
                    {deleteIcon}
                </TouchableOpacity>
            )
        })

        return(
            <ScrollView style={{width: '100%'}} contentContainerStyle={{alignItems: 'center'}}>
                <View style={styles.customBtnWrapper}>
                    {btns}
                </View>
            </ScrollView>
        )
    }

    rednerAddBtn(){
        if(!this.isSetting){
            return null
        }
        let decreaseText = '-'
        if(this.props.isDeleting){
            decreaseText = '完成'
        }
        return(
            <View style={styles.bottomBtnWrapper}>
                <TouchableOpacity style={styles.addBtn} onPress={()=>{
                    this.props.deleteBtnClick && this.props.deleteBtnClick()
                }}>
                    <Text style={styles.addText}>{decreaseText}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.addBtn} onPress={()=>{
                    if(this.props.customButtonList.length >= 15){
                        ToastManager.show('最多添加15个自定义按钮')
                        return
                    }
                    this.props.addBtnClick && this.props.addBtnClick()
                }}>
                    <Text style={styles.addText}>+</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderMainController(){
        return(
            <ImageBackground style={styles.ctrBg} resizeMode={'contain'} source={require('../../../images/ProtocolControl/bg2.png')}>
                {this.rednerCustomBtns()}
            </ImageBackground>
        )
    }

	render() {
		return (
            <ScrollView style={styles.wrapper} scrollIndicatorInsets={{right: 1}}>
                <View style={styles.content}>
                    {this.renderMainController()}
                    {this.rednerAddBtn()}
                </View>
            </ScrollView>
        )
	}
}

const styles = StyleSheet.create({
    wrapper:{
        width: '100%'
    },
    content:{
        width: '100%',
        flex: 1,
        paddingBottom: BottomSafeMargin + 20,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15
    },
    ctrBg:{
        width: bgW, 
        height: bgH,
        alignItems: 'center',
        paddingVertical: bgH * 0.08
    },
    lineBtnWrapper:{
        flexDirection: 'row',
        width: '70%',
        justifyContent: 'space-between'
    },
    btnText:{
        color: '#646566',
        fontSize: 16,
        fontWeight: '400'
    },
    btnText2:{
        color: '#646566',
        fontSize: 12,
        fontWeight: '400',
        textAlign: 'center'
    },
    btn2:{
        width: btnW,
        height: btnW,
        resizeMode: 'contain',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5
    },
    center:{
        width: bgW * 0.648,
        height: bgW * 0.648,
        justifyContent: 'center',
        alignItems: 'center'
    },
    centerTouch:{
        width: '40%',
        height: '40%'
    },
    centerImg:{
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    okText:{
        fontSize: 18,
        fontWeight: '500',
        color: '#5E676F'
    },
    topT:{
        position: 'absolute',
        top: 0,
        left: '30%',
        height: '30%',
        width: '40%'
    },
    leftT:{
        position: 'absolute',
        top: '30%',
        left: 0,
        height: '40%',
        width: '30%'
    },
    rightT:{
        position: 'absolute',
        top: '30%',
        right: 0,
        height: '40%',
        width: '30%'
    },
    bottomT:{
        position: 'absolute',
        bottom: 0,
        left: '30%',
        height: '30%',
        width: '40%'
    },
    btnImg:{
        width: btnW * 0.4,
        height: btnW * 0.4
    },
    spaceHeight:{
        marginTop: bgH * 0.04
    },
    addBtn:{
        width: 120, 
        height: 40,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#EDEDED'
    },
    addText:{
        fontWeight: 'bold',
        fontSize: 18,
        color: '#AAAAAA'
    },
    btn4Img:{
        width: bgW * 0.2,
        height: bgW * 0.2 * 0.48,
        justifyContent: 'center',
        alignItems: 'center'
    },
    customBtnWrapper:{
        marginTop: 15,
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '70%',
        flexWrap:'wrap'
    },
    customBtnTouch:{
        marginBottom: 15
    },
    bottomBtnWrapper:{
        marginTop: 10,
        flexDirection: 'row',
        width: '70%',
        justifyContent: 'space-between'
    },
    delete:{
        position: 'absolute',
        width: 20,
        height: 20,
        top: -4,
        right: -4
    }
});

export default PCtrPanelCustom



import React, { Component } from 'react';
import {
    TouchableOpacity,
    Image,
    Platform,
    Text,
    View,
    StyleSheet,
    Alert,
    DeviceEventEmitter,
    ImageBackground,
    Dimensions
} from 'react-native';
import { NetUrls, NotificationKeys, NetParams, GetdeviceRouter, Colors } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { postJson } from '../../../util/ApiRequest';
import {ColorsLight, ColorsDark, ImagesDark, ImagesLight} from '../../../common/Themes';

const itemHeight = 80
// item 间距
const itemSpace = 12

class SceneListItem extends Component {
    
    //更改场景状态
	async requestUpdateScene(id, target, callBack) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.updateSceneStatus,
				params: {
					id: id,
					status: target,
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误');
		}
    }

    //收藏场景
    async collect(sceneId, collect){
        try {
			let data = await postJson({
				url: NetUrls.collect,
				params: {
                    ids: sceneId,
                    type: 2,
                    isAdd: collect
                }
            });
			if (data.code == 0) {
                this.props.collectClick && this.props.collectClick(collect)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    executeScene(rowData){
        this.requestUpdateScene(rowData.id, 1, () => {
            if(rowData.intelligentCount){
                DeviceEventEmitter.emit(NotificationKeys.kSmartListChangeNotification)
            }
            ToastManager.show(rowData.name + ' 执行成功')
        })
    }
    
    // 点击场景，更改场景状态
	changeSceneStatus(rowData) {
        this.props.callBack && this.props.callBack()
		if(rowData.deviceCount){
            if(rowData.mistakeLock){
                Alert.alert(
                    '提示',
                    '确定要执行：' + rowData.name + '?',
                    [
                        { text: '取消', onPress: () => { }, style: 'cancel' },
                        { text: '执行', onPress: () => {
                            this.executeScene(rowData)
                        } },
                    ]
                )
            }else{
                this.executeScene(rowData)
            }
		}else{
            if(this.props.isJuniorUser){
                ToastManager.show('该场景暂无设备。')
                return
            }
			Alert.alert(
				'提示',
				'该场景暂无设备，是否去设置页面添加设备组合?',
				[
					{ text: '取消', onPress: () => { }, style: 'cancel' },
					{ text: '去添加', onPress: () => {
						const {navigate} = this.props.navigation
						navigate('DeviceCombination',{
							roomId: rowData.roomId, 
                            sceneId: rowData.id,
                            floor: rowData.floor,
                            floorText: rowData.floorText,
                            roomName: rowData.roomName,
							isScene: true
						})
					} },
				]
			)
		}
    }
    
    goSetting(){
        const {rowData} = this.props

        this.props.closeSwiper && this.props.closeSwiper()
        this.props.navigation.navigate('SceneSetting', { 
            title: '场景设置', 
            sceneData: rowData, 
            sceneId: rowData.id,
            roomId: rowData.roomId,
            isCollect: rowData.isCollect
        })
    }

    // 方格
    renderTableItem(){
        const {rowData,index} = this.props
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        
        const sceneIcon = this.props.isDark ? { uri: rowData.nightIconOn } : { uri: rowData.dayIconOn }
        const defaultImg = this.props.isDark ? ImagesDark.default : ImagesLight.default

        let siteText = null
        if (rowData.floorText) {
            siteText = (
                <Text numberOfLines={2} style={[styles.floorText,{color: Colors.themeTextLight,textAlign: 'center'}]}>
                    {rowData.floorText+'  '+rowData.roomName}
                </Text>
            )
        }

        let starIcon = require('../../../images/collect/collect_list.png')
        if(rowData.isCollect){
            starIcon = require('../../../images/collect/collect_blue.png')
        }

        return(
            <View style={[styles.thumbItemWrapper,{paddingLeft: index%2 ? 8:16},{paddingRight: index%2 ? 16:8}]}>
                <TouchableOpacity 
                    style={[styles.thumbItem,{backgroundColor: Colors.themeBg}]} 
                    activeOpacity={0.7} 
                    onPress={()=>{ this.changeSceneStatus(rowData) }}
                >
                    <Image style={styles.gridItemSceneIcon} defaultSource={defaultImg} source={sceneIcon} />
                    <Text style={[styles.gridItemName, {color: Colors.themeText}]}>{rowData.name}</Text>
                    {siteText}
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={styles.gridStarTouch} 
                        onPress={()=>{ this.collect(rowData.id, !rowData.isCollect) }}
                    >
                        <Image style={styles.gridItemStar} source={starIcon} />
                    </TouchableOpacity>
                    <TouchableOpacity 
                        activeOpacity={0.7}
                        style={styles.settingContainer}
                        onPress={()=>{ this.goSetting() }}
                    >
                        <Image style={styles.settingImg} source={require('../../../images/setting_light.png')} />
                    </TouchableOpacity>
                </TouchableOpacity>
            </View>
        )
    }

    // 长条
    renderListItem(){
        const {rowData} = this.props
        const Colors = this.props.isDark ? ColorsDark : ColorsLight

        const sceneIcon = this.props.isDark ? { uri: rowData.nightIconOn } : { uri: rowData.dayIconOn }
        const defaultImg = this.props.isDark ? ImagesDark.default : ImagesLight.default
        
        let siteText = null
        if (rowData.floorText) {
            siteText = <Text style={[styles.floorText,{color: Colors.themeTextLight}]}>
                {rowData.floorText+'  '+rowData.roomName}
            </Text>
        }

        let starIcon = rowData.isCollect ? require('../../../images/collect/collect_blue.png') 
            : require('../../../images/collect/collect_list.png')
        

        return (
            <View style={styles.itemWrapper}>
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={[styles.itemTouch,{backgroundColor: Colors.themeBg}]} 
                    onPress={() => { this.changeSceneStatus(rowData)}}
                >
                    <Image style={styles.sceneIcon} defaultSource={defaultImg} source={sceneIcon} />
                    <View style={styles.detailWrapper}>
                        <Text style={[styles.sceneName,{color: Colors.themeText}]}>{rowData.name}</Text>
                        {siteText}
                    </View>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={styles.starTouch} 
                        onPress={()=>{ this.collect(rowData.id, !rowData.isCollect) }}
                    >
                        <Image style={styles.gridItemStar} source={starIcon} />
                    </TouchableOpacity>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return this.props.numColumns == 2 ? this.renderTableItem() :
            this.renderListItem()
    }
}

const styles = StyleSheet.create({
    sceneName: {
        fontSize: 16,
        fontWeight: 'bold'
	},
	sceneIcon: {
		width: 30,
		height: 30,
		resizeMode: 'contain',
		marginLeft: 20,
	},
    starTouch: {
        height: '100%',
        paddingHorizontal: 16,
        justifyContent: 'center',
        alignItems: 'center'
    },
    itemWrapper:{ 
		width: '100%', 
        marginTop: itemSpace,
        justifyContent: 'center', 
        alignItems: 'center',
        paddingHorizontal: 16
    },
    itemTouch: {
		flexDirection: 'row',
		alignItems: 'center',
        borderRadius: 5,
        height: itemHeight,
        borderRadius: 5,
    },
    thumbItemWrapper:{
        width:'50%',
        height: 128,
        marginTop: itemSpace,
    },
    thumbItem:{
        width:'100%',
        height:'100%',
        padding: 10,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    gridItemSceneIcon: {
        width: 40,
        height: 40,
        resizeMode: 'contain'
    },
    gridItemName: {
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 15,
    },
    gridStarTouch: {
        position: 'absolute',
        top: 0,
        left: 0,
        padding: 12
    },
    gridItemStar: {
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
    settingContainer: {
        position: 'absolute',
        top: 0,
        right: 0,
        padding: 12
    },
    settingImg: {
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
    detailWrapper:{ 
        marginLeft: 20, 
        flex: 1, 
        justifyContent: 'center' 
    },
    floorText: { 
        marginTop: 5,
        fontSize: 12
    },
    // 置顶
    topIcon:{
        borderTopRightRadius:4, 
        backgroundColor:Colors.themBgRed,
        justifyContent:'center',
        alignItems:'center',
        width:20,
        height:20,
        borderBottomLeftRadius:20,
        position:'absolute',
        right:0,
        top:0,
        overflow:'hidden'
    },
    topText:{
        color:Colors.white,
        fontSize:11,
        fontWeight:'bold',
        marginLeft:3,
        marginBottom: 3
    },
    nameWrapper:{
        flexDirection: 'row'
    },
    topWrapper:{
        position: 'absolute',
        top: 0,
        left: 0,
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal: 5,
        paddingVertical: 5,
        borderRadius: 5
    }
})

export default SceneListItem
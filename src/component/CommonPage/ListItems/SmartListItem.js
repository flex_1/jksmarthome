import React, { Component } from 'react';
import {
    TouchableOpacity,
    Image,
    Platform,
    Text,
    View,
    StyleSheet,
    Alert,
    DeviceEventEmitter,
    ImageBackground,
    Dimensions
} from 'react-native';
import { NetUrls, NotificationKeys, NetParams, GetdeviceRouter, Colors } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { postJson } from '../../../util/ApiRequest';
import DateUtil from '../../../util/DateUtil';
import {ColorsLight, ColorsDark, ImagesDark, ImagesLight} from '../../../common/Themes';
import SwitchButton from '../../../common/CustomComponent/SwitchButton';

const itemHeight = 130
// item 间距
const itemSpace = 12

class SmartListItem extends Component {

    async requestUpdateSmart(id, target, callBack) {
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.smartActive,
				params: {
					intelligentId: id,
					activited: target,
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                // let text = target ? '打开' : '关闭'
                // ToastManager.show(text)
                callBack && callBack(target)
                DeviceEventEmitter.emit(NotificationKeys.kSmartListChangeNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误');
		}
    }

    itemClick(rowData){
        // 添加智能-选择智能
        if(this.props.isSmartChoose){
            this.props.callBack && this.props.callBack(rowData.intelligentId)
            return
        }

        //场景-停启用智能
        if(this.props.isStartAndStop){
            return
        }

        const { navigate } = this.props.navigation
        this.props.closeSwiper && this.props.closeSwiper()
        navigate('AddSmart', { 
            title: '设置智能',
            isCollect: rowData.isCollect,
            id:rowData.intelligentId,
            roomId: rowData.roomId
        })
    }

    renderIcons(rowData){
        const defaultImg = this.props.isDark ? ImagesDark.default : ImagesLight.default

        let excuteView = null
        let executeIcons = this.props.isDark ? rowData.nightExecuteIcons : rowData.executeIcons
        let excutes = executeIcons && executeIcons.split(',')

        if( excutes && excutes.length>0 ){
            let icons = excutes.map((val,index)=>{
                if(!val || index>4){
                    return null
                }
                if(index == 4){
                    return <Text key={"icon_image_"+index} style={{marginLeft: 10}}>···</Text>
                }
                return(
                    <Image
                        defaultSource={defaultImg}
                        key={"icon_image_"+index} 
                        style={styles.icon} 
                        source={{uri: val}} 
                    />
                )
            })
            excuteView = (
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    {icons}
                </View>
            )
        }
        return excuteView
    }

    renderDuringText(rowData){
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        let duringTimeView = <View style={{flex:1}}/>

        if(rowData.startDate && rowData.endDate){
            let beginTime = DateUtil(rowData.startDate,'yyyy-MM-dd')
            let endTime = DateUtil(rowData.endDate,'yyyy-MM-dd')
            duringTimeView = (
                <Text numberOfLines={1} style={[styles.dateText,{color: Colors.themeTextLight}]}>{beginTime+' 至 '+endTime}</Text>
            )
        }else if(rowData.startDate && !rowData.endDate){
            let beginTime = DateUtil(rowData.startDate,'yyyy-MM-dd')
            duringTimeView = (
                <Text numberOfLines={1} style={[styles.dateText,{color: Colors.themeTextLight}]}>{beginTime+' 开始 '}</Text>
            )
        }else if(!rowData.startDate && rowData.endDate){
            let endTime = DateUtil(rowData.endDate,'yyyy-MM-dd')
            duringTimeView = (
                <Text numberOfLines={1} style={[styles.dateText,{color: Colors.themeTextLight}]}>{endTime+' 结束 '}</Text>
            )
        }
        return duringTimeView
    }

    renderRoomText(rowData){
        const {roomName, floor} = rowData
        const Colors = this.props.isDark ? ColorsDark : ColorsLight

        if(!floor || !roomName){
            return null
        }
        return(
            <Text style={{marginTop: 5, fontSize: 13, color: Colors.themeTextLight}}>{floor}楼  {roomName}</Text>
        )
    }

    renderItemButton(rowData){
        const Colors = this.props.isDark ? ColorsDark : ColorsLight

        // 添加智能-选择智能
        if(this.props.isSmartChoose){
            let selectIcon = require('../../../images/sceneIcon/dot_unselect.png')
            let tintColor = {}
            if(this.props.isSelected){
                selectIcon = require('../../../images/sceneIcon/dot_select.png')
                tintColor = {tintColor: Colors.themeButton}
            }
            return(
                <Image style={[styles.dotSelect,tintColor]} source={selectIcon}/>
            )
        }

        // 场景-停启用智能
        if(this.props.isStartAndStop){
            return(
                <SwitchButton
                    value = {rowData.status}
                    isAsync = {true}
                    onPress = {()=>{
                        this.props.closeSwiper && this.props.closeSwiper()
                        let target = rowData.status ? 0:1
                        this.props.callBack && this.props.callBack(target)
                    }}
                />
            )
        }

        return(
            <SwitchButton
                value = {rowData.activited}
                isAsync = {true}
                onPress = {()=>{
                    this.props.closeSwiper && this.props.closeSwiper()
                    let target = rowData.activited ? 0:1
                    this.requestUpdateSmart(rowData.intelligentId,target,this.props.callBack)
                }}
            />
        )
    }

    render(){
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        const Images = this.props.isDark ? ImagesDark : ImagesLight
        
        const tintColor = this.props.isDark ? {tintColor: Colors.themeText} : {}
        const rowData = this.props.rowData
        const conditionIcon = this.props.isDark ? rowData.nightConditionIcon : rowData.conditionIcon

        return(
            <View style={{ width: '100%', paddingHorizontal: 16, marginTop:itemSpace }}>
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={[styles.item,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        this.itemClick(rowData)
                    }}
                >
                    <View style={styles.topWrapper}>
                        <View style={{flex: 1,justifyContent: 'center'}}>
                            <View style={{flexDirection: 'row',alignItems:'center'}}>
                                <Text style={{ fontSize: 16, color: Colors.themeText, fontWeight: 'bold' }}>{rowData.intelligentName}</Text>
                                {this.renderDuringText(rowData)}
                            </View>
                            {this.renderRoomText(rowData)}
                        </View>
                        {this.renderItemButton(rowData)}
                    </View>
                    <View style={[styles.line,{backgroundColor: Colors.split}]} />
                    <View style={styles.bottomWrapper}>
                        <Image style={styles.mainIcon} defaultSource={Images.default} source={{uri: conditionIcon}} />
                        <Image style={[styles.dotImg,tintColor]} source={require('../../../images/smart/link.png')} />
                        {this.renderIcons(rowData)}
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    item:{
        width:'100%',
        height: itemHeight,
        borderRadius: 4,
        paddingLeft: 16
    },
    topWrapper:{
        flexDirection:'row',
        alignItems: 'center',
        height:'45%'
    },
    bottomWrapper:{
        flexDirection:'row',
        alignItems: 'center',
        height:'55%',
        paddingRight: 16
    },
    line:{
        width:'100%',
        height:1
    },
    switchTouch:{
        height:'100%',
        paddingHorizontal: 16,
        justifyContent:'center'
    },
    dateText: { 
        marginLeft: 10, 
        fontSize: 12,
        // flex: 1 
    },
    icon:{ 
        width:35,
        height:35,
        resizeMode: 'stretch',
        marginLeft: 10
    },
    dotImg:{ 
        width: 26, 
        height: 13, 
        resizeMode: 'contain', 
        marginLeft: 10 
    },
    mainIcon:{ 
        width: 30, 
        height: 30, 
        resizeMode: 'contain' 
    },
    dotSelect:{
        width:18,
        height:18,
        marginRight: 16
    },
    switchIcon:{ 
        width: 46, 
        height: 28, 
        resizeMode: 'contain' 
    }
});

export default SmartListItem

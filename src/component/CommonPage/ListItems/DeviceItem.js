import React, { Component } from 'react';
import {
    TouchableOpacity,
    Image,
    Platform,
    Text,
    View,
    StyleSheet,
    Alert,
    NativeModules
} from 'react-native';
import { NetUrls,NetParams,GetdeviceRouter,NotificationKeys } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import Slider from "react-native-slider";
import {ColorsLight, ColorsDark, ImagesDark, ImagesLight} from '../../../common/Themes';
import SwitchButton from '../../../common/CustomComponent/SwitchButton';

const itemHeight = 70
const tableItemHeight = 180
// item 间距
const itemSpace = 12
const JKCameraRNUtils = Platform.select({
    ios: NativeModules.JKCameraRNUtils,
    android: NativeModules.JKCameraAndRNUtils
})

class DeviceItem extends Component {

    constructor(props) {
		super(props);
		this.state = {
			showSlider: props.rowData.value && (props.rowData.value >= 0)
		}
	}

    //收藏
    async collect(id, collect){
        try {
			let data = await postJson({
				url: NetUrls.collect,
				params: {
                    ids: id,
                    type: 1,
                    isAdd: collect
                }
            });
			if (data.code == 0) {
                this.props.collectClick && this.props.collectClick(collect)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    //控制设备(status)
    async updateDevieceStatus(id, target, callBack){
        try {
			let data = await postJson({
				url: NetUrls.deviceUpdate,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: {
					status: target,
					id: id
				}
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    //控制设备(value)
    async updateDevieceValue(id, target, callBack){
        try {
			let data = await postJson({
				url: NetUrls.deviceUpdate,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: {
					value: target,
					id: id
				}
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    goToSettings(reason){
        const {rowData} = this.props
        const { navigate } = this.props.navigation;

        let reasonText = '暂不支持该设备，是否去设置页面?'
		let btnText = '去设置'

		if(reason == 'unsupport'){
			reasonText = '暂不支持该设备，是否去设置页面?'
		}else if(reason == 'locked'){
			reasonText = '该设备被锁定,是否去解锁?'
			btnText = '去解锁'
		}

		Alert.alert(
			'提示',
			reasonText,
			[
				{ text: btnText, onPress: () => {
					navigate('DeviceSetting',{deviceData:rowData,isFromManager: true})
				} },
				{ text: '取消', onPress: () => { } },
			]
		)
    }

    onDeviceClick(){
        const {rowData} = this.props
        if(rowData.islock){
			this.goToSettings('locked')
			return
        }
        
		const {navigate, push} = this.props.navigation;
        const routerName = GetdeviceRouter(rowData.attributeTypeName)

        if(routerName == 'Camera'){
            if(!rowData.cameraInfo){
                navigate('DeviceSetting',{deviceData:rowData,isFromManager: true})
                return
            }

            // 摄像头 特殊处理
            let serialStr = rowData.cameraInfo?.serialNumber
            let appKey = rowData.cameraInfo?.ysAppKey
            let accessToken = rowData.cameraInfo?.ysAccessToken
            let cameraNo = rowData.cameraInfo?.cameraNo || 0
            let cameraName = rowData.cameraInfo?.deviceName || '视频监控'

            if (Platform.OS === 'android') {
                JKCameraRNUtils.openCameraView(serialStr,appKey,accessToken,cameraNo,cameraName, NetParams.baseUrl);
            }else{
                JKCameraRNUtils.openCameraView(serialStr,appKey,accessToken,cameraNo+1,cameraName,this.props.isDark);
            }
        }else if(!routerName){
            push('DeviceSetting',{deviceData:rowData, isFromManager: true})
        }else{
			push(routerName,{title:rowData.deviceName, deviceData:rowData})
		}
    }

    changeValue(target){
        const {rowData,updateDeviceCallBack} = this.props

        this.updateDevieceValue(rowData.deviceId, target, ()=>{
            rowData.value = target
            updateDeviceCallBack && updateDeviceCallBack(rowData)
        })
    }

    changeStatus(target){
        const {rowData,updateDeviceCallBack} = this.props

        this.updateDevieceStatus(rowData.deviceId, target, ()=>{
            rowData.status = target
            updateDeviceCallBack && updateDeviceCallBack(rowData)
        })
    }

    // 开关
    _renderSwitchCtrl(){
        const {rowData} = this.props
        
        let switchStatus = false
        if(rowData.itemType == 'slider'){
            if(this.state.showSlider){
                switchStatus = true
            }
        }else{
            if (rowData.status > 0){
                switchStatus = true
            }
        }

        return (
            <SwitchButton
                value = {switchStatus}
                onPress = {()=>{
                    if(rowData.itemType == 'slider'){
                        if(this.state.showSlider){
                            this.changeValue(0) 
                        }
                        this.setState({ showSlider: !this.state.showSlider })
                    }else{
                        let target = rowData.status ? 0 : 1
                        this.changeStatus(target)
                    }
                }}
            />
        )
    }

    // 滑竿
    _renderSLiderCtrl(){
        const {rowData} = this.props
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        
        return (
            <Slider
				style={{width: '90%'}}
				minimumValue={0}
                maximumValue={100}
          		value={Math.floor(rowData.value)}
				onSlidingComplete={(value)=>{
                    this.changeValue(Math.floor(value))
                }}
				minimumTrackTintColor={Colors.themeButton}
				maximumTrackTintColor={Colors.lightGray}
				thumbTintColor={Colors.themeButton}
				trackStyle={{height:5,borderRadius: 3}}
                thumbStyle={{width:16,height:16,borderRadius:8}}
        	/>
        )
    }

    // 数值加减
    _renderValueCtrl(){
        const {rowData} = this.props
        const Colors = this.props.isDark ? ColorsDark : ColorsLight

        let switchIcon = rowData.status ? require('../../../images/deviceIcon/airOn.png') 
        : require('../../../images/deviceIcon/airOff.png')
        let switchTintColor = {}
        if(this.props.isDark){
            switchTintColor = rowData.status ? {tintColor: Colors.themeButton} : {tintColor: Colors.themeTextLight}
        }

        return (
            <View style={{flexDirection:'row',height:'100%'}}>
                <TouchableOpacity  
                    style={styles.mediaTouch} 
                    onPress={()=>{
                        this.changeValue(rowData.value - 1)
                    }}
                >
                    <Image style={styles.mediaIcon} source={require('../../../images/deviceIcon/airDe.png')}/>
                </TouchableOpacity>
                <TouchableOpacity  
                    style={[styles.mediaTouch,{marginHorizontal: 5}]}
                    onPress = {()=>{
                        let target = rowData.status ? 0 : 1
                        this.changeStatus(target)
                    }}
                >
                    <Image style={[styles.mediaIcon,switchTintColor]} source={switchIcon}/>
                </TouchableOpacity>
                <TouchableOpacity  
                    style={styles.mediaTouch}
                    onPress={()=>{
                        this.changeValue(rowData.value + 1)
                    }}
                >
                    <Image style={styles.mediaIcon} source={require('../../../images/deviceIcon/airAdd.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    // 媒体控制
    _renderMediaCtrl(){
        const {rowData} = this.props
        const Colors = this.props.isDark ? ColorsDark : ColorsLight

        let switchIcon = rowData.status ? require('../../../images/deviceIcon/stop.png') :
            require('../../../images/deviceIcon/play.png')

        return (
            <View style={{flexDirection:'row',height:'100%'}}>
                <TouchableOpacity  
                    style={styles.mediaTouch} 
                    onPress={()=>{
                        this.changeValue(1)
                    }}
                >
                    <Image style={styles.mediaIcon} source={require('../../../images/deviceIcon/pre.png')}/>
                </TouchableOpacity>
                <TouchableOpacity  
                    style={[styles.mediaTouch,{marginHorizontal: 5}]}
                    onPress = {()=>{
                        let target = rowData.status ? 0 : 1
                        this.changeStatus(target)
                    }}
                >
                    <Image style={[styles.mediaIcon,{tintColor: Colors.themeText}]} source={switchIcon}/>
                </TouchableOpacity>
                <TouchableOpacity  
                    style={styles.mediaTouch}
                    onPress={()=>{
                        this.changeValue(2)
                    }}
                >
                    <Image style={styles.mediaIcon} source={require('../../../images/deviceIcon/next.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    //质量检测类型设备
    _renderSensorCtr(){
        const {rowData} = this.props
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        let temp = '-'
        let humi = '-'

        try {
            let deviceStatus = JSON.parse(rowData.deviceStatus)
            temp = deviceStatus.temperature
            humi = deviceStatus.humidity 
        } catch (error) {
        }

        return(
            <View style={{flexDirection:'row'}}>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <Image style={[styles.sensorImg,{tintColor: Colors.themeTextLight}]} source={require('../../../images/homeBG/temp.png')}/>
                    <Text style={{marginLeft:5,fontSize:14,color: Colors.themeTextLight}}>{temp}</Text>
                </View>
                <View style={{flexDirection:'row',alignItems:'center',marginLeft:15}}>
                    <Image style={[styles.sensorImg,{tintColor: Colors.themeTextLight}]} source={require('../../../images/homeBG/humidity.png')}/>
                    <Text style={{marginLeft:5,fontSize:14,color: Colors.themeTextLight}}>{humi}</Text>
                </View>
            </View>
        )
    }

    // 获取楼层房间信息
    _renderFloorText(type){
        // type:1-方格   2-长条
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        const {rowData} = this.props
        if (!rowData.floorText){
            return this._renderValueText()
        }
        if(type == 1){
            return(
                <Text numberOfLines={2} style={[styles.floorText,{color: Colors.themeTextLight,textAlign: 'center'}]}>
                    {rowData.floorText+'  '+rowData.roomName} {this._renderValueText()}
                </Text>
            )
        }else{
            return (
                <Text style={[styles.floorText,{color: Colors.themeTextLight}]}>
                    {rowData.floorText+'  '+rowData.roomName}
                </Text>
            )
        }
    }

    // 获取 value 数值信息
    _renderValueText(){
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        const {rowData} = this.props
        if(rowData.value == null){
            return null
        }
        if(rowData.itemType == 'media'){
            return null
        }
        
        let unit = rowData.unit || ''
        let leftSpace = rowData.floor ? '   ' : ''
        return(
            <Text style={[styles.floorText,{color: Colors.themeTextLight, marginLeft: 5}]}>
                {leftSpace+rowData.value}{unit}
            </Text>
        )
    }

    //长条底部滑竿(延伸部分, 只有长条模式下才有)
    renderListSlider(){
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        const {rowData} = this.props

        if(!rowData.controllerStatus || rowData.islock){
            return null
        }

        if(rowData.itemType == 'slider' && this.state.showSlider){
            return(
                <View style={[styles.listSliderWrapper,{backgroundColor:Colors.themeBg}]}>
                    {this._renderSLiderCtrl()}
                </View>
            )
        }else{
            return null
        }
    }

    // 控制栏
    renderTool(){
        const {rowData} = this.props
        const Colors = this.props.isDark ? ColorsDark : ColorsLight

        let isTable = this.props.numColumns == 2 
        let wrapperStyle = isTable ? styles.tableCtrlWrapper : styles.listCtrlWrapper

        //如果离线状态
        if (!rowData.controllerStatus) {
            return(
                <View style={wrapperStyle}>
                    <Text style={{fontSize: 13,color: Colors.themeTextLight}}>离线</Text>
                </View>
            )
        }
 
        // 设备锁定
        if (rowData.islock) {
            return(
                <View style={wrapperStyle}>
                     <Image style={styles.lockIcon} source={require('../../../images/deviceIcon/lock.png')} />
                </View>
            )
        }

        // 控制栏
        if(rowData.itemType == 'switch'){
            return(
                <View style={[wrapperStyle, isTable ? null : {paddingRight: 0}]}>
                    {this._renderSwitchCtrl()}
                </View>
            )
        }else if(rowData.itemType == 'slider'){
            if(isTable){
                return(
                    <View style={wrapperStyle}>
                        {this._renderSLiderCtrl()}
                    </View>
                )
            }else{
                return(
                    <View style={[wrapperStyle,isTable ? null : {paddingRight: 0}]}>
                        {this._renderSwitchCtrl()}
                    </View>
                )
            }
        }else if(rowData.itemType == 'number'){
            return(
                <View style={wrapperStyle}>
                    {this._renderValueCtrl()}
                </View>
            )
        }else if(rowData.itemType == 'media'){
            return(
                <View style={wrapperStyle}>
                    {this._renderMediaCtrl()}
                </View>
            ) 
        }else if(rowData.itemType == 'label'){
            return(
                <View style={wrapperStyle}>
                    <Text style={[styles.statusLable,{color: Colors.themeTextLight}]}>{rowData.statusText}</Text>
                </View>
            )
        }else if(rowData.itemType == 'sensor'){
            return(
                <View style={wrapperStyle}>
                    {this._renderSensorCtr()}
                </View>
            )
        }else{
            return null
        }
    }

    renderCollectStar(){
        const {rowData,index} = this.props
        if(rowData.ishidden){
            return null
        }
        if(rowData.attributeTypeName == 'robot'){
            return null
        }
        if(rowData.attributeTypeName == 'wifi-485-panel'){
            return null
        }

        const starIcon = rowData.isCollect ? require('../../../images/collect/collect_blue.png')
        : require('../../../images/collect/collect_list.png')

        return(
            <TouchableOpacity
                activeOpacity={0.7}
                style={styles.tableStarTouch} 
                onPress={()=>{
                    this.collect(rowData.deviceId, !rowData.isCollect) 
                }}
            >
                <Image style={styles.tableItemStar} source={starIcon} />
            </TouchableOpacity>
        )
    }

    // 方格
    renderTableItem(){
        const {rowData,index} = this.props
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        const deviceIcon = this.props.isDark ? { uri: rowData.nightImg } : { uri: rowData.dayImg }
        const defaultImg = this.props.isDark ? ImagesDark.default : ImagesLight.default
        
        const bgColor = rowData.controllerStatus ? Colors.themeBg : Colors.offlineBg
            
        return(
            <View style={[styles.tablebItemWrapper,{paddingLeft: index%2 ? 8:16},{paddingRight: index%2 ? 16:8}]}>
                <TouchableOpacity 
                    style={[styles.tableItem,{backgroundColor: bgColor}]} 
                    activeOpacity={0.7} 
                    onPress={()=>{ this.onDeviceClick() }}
                >
                    <Image style={styles.tableImg} defaultSource={defaultImg} source={deviceIcon}/>
                    <Text numberOfLines={1} style={[styles.tableName,{color: Colors.themeText}]}>
                        {rowData.deviceName}
                    </Text>
                    <View style={{marginTop: 10,flexDirection:'row'}}>
                        {this._renderFloorText(1)}
                    </View>
                    {this.renderTool()}
                    {this.renderCollectStar()}
                </TouchableOpacity>
            </View>
        )
    }

    // 长条
    renderListItem(){
        const {rowData,index} = this.props
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        const defaultImg = this.props.isDark ? ImagesDark.default : ImagesLight.default
        const deviceIcon = this.props.isDark ? { uri: rowData.nightImg } : { uri: rowData.dayImg }
        const bgColor = rowData.controllerStatus ? Colors.themeBg : Colors.offlineBg
        let showDetail = rowData.value || (rowData.value == null && rowData.itemType == 'media')

        return (
            <View style={styles.itemWrapper}>
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={[styles.itemTouch,{backgroundColor: bgColor}]} 
                    onPress={() => { this.onDeviceClick() }}
                >
                    <Image style={styles.img} defaultSource={defaultImg} source={deviceIcon}/>
                    <View style={{marginLeft: 15,flex:1}}>
                        <Text style={[styles.name,{color: Colors.themeText}]}>{rowData.deviceName}</Text>
                        <View style={{marginTop: 10,flexDirection:'row',alignItems:'center'}}>
                            {this._renderFloorText(2)}
                        </View>
                    </View>
                    {this.renderTool()}
                </TouchableOpacity>
                {this.renderListSlider()}
            </View>
        )
    }

    render() {
        return this.props.numColumns == 2 ? this.renderTableItem() :
            this.renderListItem()
    }
}

const styles = StyleSheet.create({
	itemWrapper:{ 
		width: '100%', 
        marginTop: itemSpace,
        justifyContent: 'center', 
        alignItems: 'center',
        paddingHorizontal: 16
    },
    itemTouch: {
		flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 5,
        width: '100%',
        height: itemHeight,
        paddingLeft: 16
    },
    tablebItemWrapper:{
        width:'50%',
        height: tableItemHeight,
        marginTop: itemSpace
    },
    tableItem:{
        width:'100%',
        height:'100%',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 10
    },
    img:{
        width: 40,
        height: 40,
        resizeMode: 'contain'
    },
    tableImg:{
        width: 40,
        height: 40,
        resizeMode: 'contain'
    },
    name:{
        fontSize: 16,
        fontWeight: 'bold'
    },
    tableName:{
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 15,
        marginHorizontal: 10
    },
    floorText: {
        fontSize: 12
    },
    tableCtrlWrapper:{
        marginTop: 10,
        height: 40,
        width:'100%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10
    },
    statusLable:{
        fontSize: 13
    },
    tableStarTouch: {
        position: 'absolute',
        top: 0,
        left: 0,
        padding: 12
    },
    tableItemStar:{
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
    lockIcon:{ 
        width: 18, 
        height: 18, 
        resizeMode: 'contain'
    },
    switchTouch:{
        height:'100%',
        justifyContent:'center',
        alignItems: 'center',
        width:80,
        height:'100%',
    },
    switchIcon: {
        width:70,
        height:30,
        resizeMode:'contain'
    },
    mediaTouch:{
        height: '100%',
        paddingHorizontal: 5,
        justifyContent:'center',
        alignItems: 'center'
    },
    mediaIcon:{
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    listCtrlWrapper:{
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingRight: 16
    },
    listSliderWrapper:{
        height:45, 
        marginTop: -5,
        width:'100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomLeftRadius:5,
        borderBottomRightRadius:5
    },
    sensorImg:{
        width:16,
        height:20,
        resizeMode:'contain'
    }
});

export default DeviceItem

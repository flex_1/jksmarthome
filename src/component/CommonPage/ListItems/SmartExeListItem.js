import React, { Component } from 'react';
import {
    TouchableOpacity,
    Image,
    Platform,
    Text,
    View,
    StyleSheet,
    Alert,
    DeviceEventEmitter,
    ImageBackground,
    Dimensions
} from 'react-native';
import { NetUrls, NotificationKeys, NetParams, GetdeviceRouter, Colors } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { postJson } from '../../../util/ApiRequest';
import DateUtil from '../../../util/DateUtil';
import {ColorsLight, ColorsDark} from '../../../common/Themes';

const SmartListItem = (props) => {

    //智能 开关
	requestUpdateSmart = async (id, target, callBack)=> {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.smartActive,
				params: {
					intelligentId: id,
					activited: target,
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				callBack && callBack(target)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误');
		}
    }

    const isDark = props.isDark
    const callback = props.callBack;
    const Colors = isDark ? ColorsDark : ColorsLight
    const rowData = props.rowData

    const conditionIcon = isDark ? rowData.nightConditionIcon : rowData.conditionIcon
    const tintColor = isDark ? {tintColor: Colors.themeText} : {}
    
    const executeIcons = isDark ? rowData.nightExecuteIcons : rowData.executeIcons
    let excutes = executeIcons && executeIcons.split(',')
    let excuteView = null
    if( excutes && excutes.length>0 ){
        let icons = excutes.map((val,index)=>{
            if(!val || index>4){
                return null
            }
            if(index == 4){
                return <Text key={"icon_image_"+index} style={{marginLeft: 10}}>···</Text>
            }
            return(
                <Image key={"icon_image_"+index} style={{ width:40,height:40,resizeMode: 'contain',marginLeft: 10}} source={{uri: val}} />
            )
        })
        excuteView = (
            <View style={{flexDirection:'row',alignItems:'center'}}>
                {icons}
            </View>
        )
    }

    return (
        <View style={{ width: '100%', paddingHorizontal: 16, paddingVertical: 10 }}>
            <TouchableOpacity activeOpacity={0.7} style={[styles.item,{backgroundColor: Colors.themeBg}]} onPress={callback}>
                <View style={styles.topWrapper}>
                    <Text style={{ fontSize: 16, color: Colors.themeText, fontWeight: 'bold' }}>{rowData.intelligentName}</Text>
                    <Image style={styles.rightArrow} source={require('../../../images/enterLight.png')} />
                </View>
                <View style={[styles.line,{backgroundColor: Colors.split}]} />
                <View style={styles.bottomWrapper}>
                    <Image style={{ width: 30, height: 30, resizeMode: 'contain' }} source={{uri: conditionIcon}} />
                    <Image style={[styles.link,tintColor]} source={require('../../../images/smart/link.png')} />
                    {excuteView}
                </View>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    item:{
        width:'100%',
        height: 130,
        borderRadius: 4,
        paddingHorizontal: 16
    },
    topWrapper:{
        flexDirection:'row',
        alignItems: 'center',
        height:'40%'
    },
    bottomWrapper:{
        flexDirection:'row',
        alignItems: 'center',
        height:'60%',
    },
    line:{
        width:'100%',
        height:1,
        backgroundColor: Colors.splitLightGray
    },
    rightArrow:{ 
        width: 8, 
        height: 14, 
        resizeMode: 'contain',
        position:'absolute', 
        right:0,
        top:20
    },
    link:{ 
        width: 26, 
        height: 13, 
        resizeMode: 'contain', 
        marginLeft: 10 
    }
});

export default SmartListItem;
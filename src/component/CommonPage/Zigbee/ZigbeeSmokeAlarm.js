/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
    Modal,
    ImageBackground
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from '../../CommonPage/ControlPanel/Panel';
import { connect } from 'react-redux';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

const SliderW = screenW * 0.6
const ThumbSize = SliderW * 0.2523

class ZigbeeSmokeAlarm extends Panel {
	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData') || {}

        this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            alarm: 0,  //0-未检测到报警  1-监测到报警,
            batteryLevel: 0, //0-电量正常  1-电量低
            batteryStatus: 0, //0-正常   1-故障
        }
    }
    
	componentDidMount() {
        super.componentDidMount()

        this.requestDeviceStatus()
	}

	componentWillUnmount(){
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        this.setState({
            batteryLevel: data?.BatteryLevel,
            alarm: data?.Alarm,
            batteryStatus: data?.BatteryStatus
        }) 
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData?.deviceId
                }
			});
			if (data.code == 0) {
                let statusJson = data.result?.deviceStatus && JSON.parse(data.result?.deviceStatus)
                this.setState({
                    batteryLevel: statusJson?.BatteryLevel,
                    alarm: statusJson?.Alarm,
                    batteryStatus: statusJson?.BatteryStatus
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }


    renderBattery(){
        let percent = 100
        let batteryIcon = require('../../../images/zigbee/battery_g.png')
        let battery_bg = '#3AC457'
        let batteryStatus = '电量正常'

        if(this.state.batteryStatus == 1){
            percent = 0
            batteryIcon = require('../../../images/zigbee/battery_r.png')
            battery_bg = '#FF0707'
            batteryStatus = '电池故障'
        }else if(this.state.batteryLevel == 1){
            percent = 20
            batteryIcon = require('../../../images/zigbee/battery_r.png')
            battery_bg = '#FF0707'
            batteryStatus = '电量低'
        }

        return(
            <View style={styles.batteryWrapper}>
                <ImageBackground style={styles.batteryImgBg} source={batteryIcon}>
                    <View style={{height: '100%',  borderRadius:2,width: percent + '%',backgroundColor: battery_bg}}/>
                </ImageBackground>
                <Text style={[styles.batPerText,{color: battery_bg}]}>{batteryStatus}</Text>
            </View>
        )
    }

    renderAlarmStatus(){
        const Colors = this.props.themeInfo.colors
        let alarmText = this.state.alarm ? '检测到报警' : '未检测到报警'
        
        return(
            <View style={styles.batteryWrapper}>
                <Text style={[styles.alarmText,{color: Colors.themeText}]}>{alarmText}</Text>
            </View>
        )
    }

    renderBottomBtn(){
        const Colors = this.props.themeInfo.colors
        
        return(
            <View style={[styles.bottomWrapper,{backgroundColor: Colors.themeBg}]}>
                <TouchableOpacity activeOpacity={0.7} style={[styles.bottomTouch,{backgroundColor: Colors.themeBg}]} onPress={()=>{
                    this.props.navigation.navigate('Setting_Log',{
                        deviceId: this.deviceData?.deviceId
                    })
                }}>
                    <Image style={[styles.icon,{tintColor: Colors.themeTextLight}]} source={require('../../../images/zigbee/log.png')}/>
                    <Text style={[styles.title,{color: Colors.themeText}]}>日志</Text>
                    <Image style={styles.arrow} source={require('../../../images/ProtocolControl/arrow.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    getMainPanelPage(){
        return(
            <View style={styles.wrapper}>
                <Image style={{width: 160, height:160}} source={require('../../../images/zigbee/smoke.png')}/>
                {this.renderBattery()}
                {this.renderAlarmStatus()}
                <View style={{flex: 1}}/>
                {this.renderBottomBtn()}
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors
        
		return (
			<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
				{this.getHeaderView()}
				{this.getMainPanelPage()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight,
    },
    wrapper:{
        marginTop: screenH * 0.08,
        flex: 1,
        alignItems:'center'
    },
    batteryWrapper:{
        flexDirection: 'row', 
        alignItems: 'center',
        marginTop: 20
    },
    batteryImgBg:{
        width: 20, 
        height:15, 
        resizeMode:'contain',
        padding: 2,
        paddingRight: 3
    },
    batPerText:{
        fontSize: 15, 
        fontWeight: '400', 
        marginLeft: 5
    },
    bottomWrapper:{
        paddingBottom: BottomSafeMargin + screenH*0.08,
        width: '100%'
    },
    bottomTouch:{
        marginHorizontal: 16,
        paddingHorizontal: 16,
        alignItems: 'center',
        height: 60,
        borderRadius: 8,
        flexDirection: 'row',
        //阴影四连
        shadowOffset: { width: 0, height: 0 },
        shadowColor: 'rgba(120,120,120,1)',
        shadowOpacity: 0.22,
        shadowRadius: 8,
        elevation: 4
    },
    icon:{
       width: 20,
       height: 20,
       resizeMode: 'contain' 
    },
    content:{
        marginLeft: 15,
        flex: 1
    },
    arrow:{
        width: 16,
        height: 16
    },
    title:{
        fontSize: 16,
        fontWeight: '400',
        marginLeft: 10,
        flex: 1
    },
    subTitle:{
        fontSize: 12,
        fontWeight: '400',
        marginTop: 5
    },
    alarmText:{
        fontSize: 14,
        fontWeight: '400'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ZigbeeSmokeAlarm)



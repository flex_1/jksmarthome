/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	StyleSheet,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
    RefreshControl
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from '../../CommonPage/ControlPanel/Panel';
import { connect } from 'react-redux';
import Echarts from '../../../third/echarts/index';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const EchartH = screenW * 0.55

class ZigbeeAirQuality extends Panel {
	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData') || {}

        this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            qualityData: [],
            proposedMeasures: [],
            airQualityList24: [],
            airQualityList30: [],
            pm25DateType: 1,  //1-24小时  2-30日
            tvocDateType: 1,  //1-24小时  2-30日
            co2DateType: 1,  //1-24小时  2-30日
            tempDateType: 1,  //1-24小时  2-30日
            humiDateType: 1,  //1-24小时  2-30日
            isRefreshing: false
        }
    }
    
	componentDidMount() {
        super.componentDidMount()

        this.requestDeviceStatus()
	}

	componentWillUnmount(){
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        this.setState({
            
        }) 
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData?.deviceId
                }
			});
            this.setState({isRefreshing: false})
			if (data.code == 0) {
                this.setState({
                    qualityData: data.result?.zigbeeAirQualityData || [],
                    proposedMeasures: data.result?.proposedMeasures || [],
                    airQualityList24: data.result?.airQualityZigbeeListOuts24 || [],
                    airQualityList30: data.result?.airQualityZigbeeListOuts30 || []
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            this.setState({isRefreshing: false})
			ToastManager.show('网络错误')
		}
    }

    onRefresh(){
        if (this.state.isRefreshing) return
		this.setState({ isRefreshing: true })
        this.requestDeviceStatus()
    }

    renderTitleView(title, unit, showDateSwicth, key){
        const Colors = this.props.themeInfo.colors
        let bgColor = [Colors.newBtnBlueBg, 'rgba(100,101,102,0.12)']
        let textColor = [Colors.white, '#969799']
        let index = this.state[key]

        return(
            <View style={styles.itemTitleView}>
                <View style={styles.titleIcon}/>
                <Text style={[styles.title,{color: Colors.themeText}]}>{title}</Text>
                {unit ? <Text style={[styles.unitText,{color: Colors.themeText}]}>{unit}</Text> : null}
                <View style={{flex: 1}}/>
                {showDateSwicth ? <View style={styles.dateSwitchWrapper}>
                    <TouchableOpacity style={[styles.dateSwitchTouch,{backgroundColor: bgColor[index == 1 ? 0:1]}]} onPress={()=>{
                        this.state[key] = 1
                        this.setState({
                            ...this.state
                        })
                    }}>
                        <Text style={[styles.dateText,{color: textColor[index == 1 ? 0:1]}]}>24小时</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.dateSwitchTouch,{backgroundColor: bgColor[index == 1 ? 1:0]}]} onPress={()=>{
                        this.state[key] = 2
                        this.setState({
                            ...this.state
                        })
                    }}>
                        <Text style={[styles.dateText,{color: textColor[index == 1 ? 1:0]}]}>30日</Text>
                    </TouchableOpacity>
                </View>: null }
            </View>
        )
    }

    renderDetailItem(index, title, value, unit, quaity, color){
        const Colors = this.props.themeInfo.colors

        return(
            <View key={index} style={[styles.detailItemWrapper,{marginTop: index<=1 ? 27:35}]}>
                <View style={[styles.blueDot,{backgroundColor:color}]}/>
                <View style={{marginLeft: 20}}>
                    <Text style={[styles.enviName,{color: Colors.themeText}]}>{title}</Text>
                    <View style={styles.enviContentWrapper}>
                        <Text style={[styles.enviValue,{color: Colors.themeText}]}>{value}</Text>
                        <Text style={[styles.unitText,{color: Colors.themeText}]}>{unit}</Text>
                    </View>
                    <Text style={[styles.descText,{color: color}]}>{quaity}</Text>
                </View>
            </View>
        )
    }

    renderCurrentData(){
        if(!this.state.qualityData || this.state.qualityData.length <= 0){
            return null
        }
        let list = this.state.qualityData.map((value, index)=>{
            return(
                this.renderDetailItem(index, value.name, value.value, value.unit, value.desc, value.color)
            )
        })
        return(
            <View style={styles.item}>
                {this.renderTitleView('当前环境',null,null)}
                <View style={{flexDirection: 'row',flexWrap: 'wrap'}}>
                    {list}
                </View>
            </View>
        )
    }

    renderSuggestion(){
        if(!this.state.proposedMeasures || this.state.proposedMeasures.length <= 0){
            return null
        }
        const Colors = this.props.themeInfo.colors

        return(
            <View style={styles.item}>
                {this.renderTitleView('建议措施',null,null)}
                {this.state.proposedMeasures.map((value, index)=>{
                    return(
                        <Text key={'suggest_'+index} style={[styles.suggest,{color: Colors.themeTextLight}]}>{value}</Text>
                    )
                })}
            </View>
        )
    }

    renderChart(config, chartList){
        const isDark = this.props.themeInfo.isDark

        if(!chartList || chartList.length<=0){
            return(
                <View style={styles.noContentWrapper}>
                    <Text style={{color:Colors.themeTextLightGray}}>暂无数据</Text>
                </View>
            )
		}

        let eleDataX = []
        let eleDataY = []
        for (const data of chartList) {
			let timeX = data.timeX
			eleDataX.push(timeX)

			let value = data[config.key] || 0
			if( typeof(value) != 'number'){
                value = 0	
			}
            eleDataY.push({value: value, time: data.dateTime, name: config.name, unit: config.unit})
		}

		// 柱状图 是否可以滚动
		let dataZoom = null
		let animation = true
        
		if(eleDataY.length > 18 && config.seriesType == 'bar'){
			dataZoom = {
				type: 'inside',
				start: 0,
				end: 50,
				zoomLock: true,
				filterMode: 'empty'
			}
			animation = false
		}

		let option = {
			color: isDark ? ['#1890FF'] : ['#5B8FF9'],
			tooltip: {
				trigger: 'axis',
				axisPointer: {
					type: 'line',
					lineStyle:{
						width: 1,
						color: Colors.themeTextLightGray
					}
				},
                formatter: (data) => {
					let axisData = data[0]
                    return (
						axisData.data.time
                        +'</br>'+ axisData.data.name +':  '+ axisData.data.value + axisData.data.unit
					)
                }
			},
			legend: {show:false},
            grid: {
				left: '2%',
				right: '2%',
				bottom: '4%',
                top: '5%',
				containLabel: true
			},
			xAxis: {
				data: eleDataX,
                boundaryGap: true,
				axisLine: {
					lineStyle: {
						color: Colors.borderLightGray
					}
				},
				axisLabel: {
					textStyle:{
						color: Colors.themeTextLightGray,
					},
				},
				axisTick:{
					show: true,
                    alignWithLabel: true
				}
			},
			clickable:true,
			yAxis: [
            {
                type: 'value',
                name: '',
                alignTicks: true,
                axisLine: {
                    show: false,
                    lineStyle: {
                        color: '#00000073'
                    }
                },
                splitLine:{
                    show: true,
                    lineStyle:{
                        color: Colors.splitLightGray
                    }
                },
                axisLabel: {
                    formatter: '{value}',
                    textStyle:{
						color: Colors.themeTextLightGray,
					},
                }
            }],
			dataZoom: dataZoom,
			animation: animation,
			series: [{
                name: config.name,
                type: config.seriesType,
                smooth: config.smooth,
                data: eleDataY,
                lineStyle: {
                    width: 2
                },
                areaStyle: {
                    opacity: config.areaOpacity,
                    color: isDark ? '#1890FF40' : '#5B8FF940'
                }
            }]
		};
		return (
            <View style={{width:'100%',marginTop: 25,paddingHorizontal: 10}}>
                <Echarts
				    option={option} 
				    height={EchartH}
				    onPress={(data)=>{
					    // console.log('123');
				    }}
			    />
            </View>
		)
	}

    renderPM25Chart(){
        let config = {}
        let dateType = this.state.pm25DateType
        config.key = 'pm25'
        config.name = 'PM2.5'
        config.seriesType = 'line'
        config.smooth = false
        config.areaOpacity = 1
        config.unit = 'ug/m³'

        return(
            <View style={styles.item}>
                {this.renderTitleView('PM2.5','ug/m³',true, 'pm25DateType')}
                {dateType == 1 ? this.renderChart(config, this.state.airQualityList24) : null}
                {dateType == 2 ? this.renderChart(config, this.state.airQualityList30) : null}
            </View>
        )
    }

    renderTempChart(){
        let config = {}
        let dateType = this.state.tempDateType
        config.key = 'temperature'
        config.name = '温度'
        config.seriesType = 'bar'
        config.smooth = false
        config.areaOpacity = 0
        config.unit = '°C'

        return(
            <View style={styles.item}>
                {this.renderTitleView('温度','°C',true, 'tempDateType')}
                {dateType == 1 ? this.renderChart(config, this.state.airQualityList24) : null}
                {dateType == 2 ? this.renderChart(config, this.state.airQualityList30) : null}
            </View>
        )
    }

    renderHumiChart(){
        let config = {}
        let dateType = this.state.humiDateType
        config.key = 'humidity'
        config.name = '湿度'
        config.seriesType = 'bar'
        config.smooth = false
        config.areaOpacity = 0
        config.unit = '%'

        return(
            <View style={styles.item}>
                {this.renderTitleView('湿度','%',true, 'humiDateType')}
                {dateType == 1 ? this.renderChart(config, this.state.airQualityList24) : null}
                {dateType == 2 ? this.renderChart(config, this.state.airQualityList30) : null}
            </View>
        )
    }

    renderTVOCChart(){
        let config = {}
        let dateType = this.state.tvocDateType
        config.key = 'tvoc'
        config.name = 'TVOC'
        config.seriesType = 'line'
        config.smooth = true
        config.areaOpacity = 0
        config.unit = 'mg/m³'

        return(
            <View style={styles.item}>
                {this.renderTitleView('TVOC','mg/m³',true, 'tvocDateType')}
                {dateType == 1 ? this.renderChart(config, this.state.airQualityList24) : null}
                {dateType == 2 ? this.renderChart(config, this.state.airQualityList30) : null}
            </View>
        )
    }

    renderCO2Chart(){
        let config = {}
        let dateType = this.state.co2DateType
        config.key = 'co2'
        config.name = 'CO₂'
        config.seriesType = 'line'
        config.smooth = true
        config.areaOpacity = 0
        config.unit = 'ppm'

        return(
            <View style={styles.item}>
                {this.renderTitleView('CO₂','ppm',true, 'co2DateType')}
                {dateType == 1 ? this.renderChart(config, this.state.airQualityList24) : null}
                {dateType == 2 ? this.renderChart(config, this.state.airQualityList30) : null}
            </View>
        )
    }

    getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

        return(
            <ScrollView 
                contentContainerStyle={styles.scrollContent}
                refreshControl={
                    <RefreshControl
                        colors={[Colors.activeIndiColor]}
                        tintColor={Colors.activeIndiColor}
                        refreshing={this.state.isRefreshing}
                        onRefresh={this.onRefresh.bind(this)}
                    />
                }
            >
                <View style={[styles.split,{backgroundColor: Colors.themeBaseBg}]}/>
                {this.renderCurrentData()}
                <View style={[styles.split,{backgroundColor: Colors.themeBaseBg}]}/>
                {this.renderSuggestion()}
                <View style={[styles.split,{backgroundColor: Colors.themeBaseBg}]}/>
                {this.renderPM25Chart()}
                <View style={[styles.split,{backgroundColor: Colors.themeBaseBg}]}/>
                {this.renderTempChart()}
                <View style={[styles.split,{backgroundColor: Colors.themeBaseBg}]}/>
                {this.renderHumiChart()}
                <View style={[styles.split,{backgroundColor: Colors.themeBaseBg}]}/>
                {this.renderTVOCChart()}
                <View style={[styles.split,{backgroundColor: Colors.themeBaseBg}]}/>
                {this.renderCO2Chart()}
            </ScrollView>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors
        
		return (
			<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
				{this.getHeaderView()}
				{this.getMainPanelPage()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight,
    },
    scrollContent:{
        paddingBottom: BottomSafeMargin + 20
    },
    split:{
        height: 6,
        width: '100%'
    },
    item:{
        paddingVertical: 16,
        paddingHorizontal: 10
    },
    itemTitleView:{
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 6
    },
    titleIcon:{
        height: 22, 
        width: 3, 
        backgroundColor: Colors.newBtnBlueBg
    },
    title:{
        marginLeft: 8,
        fontSize: 18, 
        fontWeight: '500'  
    },
    suggest:{
        marginLeft: 30,
        fontSize: 14,
        fontWeight: '400',
        marginTop: 16
    },
    unitText:{
        fontSize: 12,
        fontWeight: '400',
        marginLeft: 5
    },
    dateSwitchTouch:{
        flex: 1, 
        justifyContent: 'center', 
        height:'100%',
        alignItems: 'center'
    },
    dateText:{
        fontSize:14, 
        fontWeight:'400'
    },
    dateSwitchWrapper:{
        flexDirection: 'row',
        height: 26, 
        width:120,
        marginRight: 10
    },
    detailItemWrapper:{
        width: '49%',
        marginTop: 40, 
        flexDirection: 'row',
        paddingLeft: 30
    },
    blueDot:{
        width: 12,
        height: 12,
        marginTop: 5,
        borderRadius:6
    },
    enviName:{
        fontSize: 16,
        fontWeight: '400',
        height:22,
        lineHeight:22
    },
    enviContentWrapper:{
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10
    },
    enviValue:{
        fontSize:16, 
        fontWeight: '500'
    },
    descText:{
        marginTop: 10,
        fontSize:16,
        fontWeight:'400'
    },
    noContentWrapper:{
        width: '100%',
        justifyContent:'center',
        alignItems:'center',
        height: EchartH
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ZigbeeAirQuality)



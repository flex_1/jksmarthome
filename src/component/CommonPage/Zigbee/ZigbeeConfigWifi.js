/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
    Modal
} from 'react-native';
import { connect } from 'react-redux';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../../CommonPage/Navigation/HeaderTitle';
import {postJson} from '../../../util/ApiRequest';
import { NetUrls, Colors } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";

const screenH = Dimensions.get('window').height;
const screenW = Dimensions.get('window').width;
const CountTime = 120
const IntervalTime = 10

class ZigbeeConfigWifi extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={'添加设备'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
        super(props)
        const { getParam } = props.navigation;

        this.getwayId = getParam('getwayId')
        this.callBack = getParam('callBack')
        this.state = {
            isConfiging: false,
            count: 0,
            newCount: 0,
            allTime: CountTime
        }
    }
    
	componentDidMount() {
        this.requestList(true)
	}

	componentWillUnmount(){
        this.interVal && clearInterval(this.interVal)
    }

    async requestList(isOrigin) {
        try {
            let data = await postJson({
                url: NetUrls.setmanageSecond,
                params: {
                    deviceId: this.getwayId
                }
            });
            this.setState({loading: false,isRefreshing:false})
            if (data.code == 0) {
                let listLength = data.result?.length || 0
                if(isOrigin){
                    this.setState({
                        count: listLength || 0,
                    })
                }else{
                    let newCount = data.result?.length - this.state.count
                    if(newCount != 0){
                        this.callBack && this.callBack()
                    }
                    this.setState({
                        newCount: newCount || 0,
                    })
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({loading: false,isRefreshing:false})
            ToastManager.show('网络错误')
        }
    }

    async requestZigBeeDuration() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.zigBeeDuration,
                params: {
                    id: this.getwayId,
                    time: CountTime
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.setState({
                    isConfiging: true,
                    allTime: CountTime
                })
                this.interVal = setInterval(() => {
                    this.state.allTime -= 10
                    this.requestList()
                    if(this.state.allTime <= 0){
                        this.setState({
                            isConfiging: false
                        })
                        clearInterval(this.interVal)
                    }
                }, IntervalTime * 1000);
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }
    
    renderConfigImage(){
        const Colors = this.props.themeInfo.colors

        let mainIng = require('../../../images/zigbee/searchDevice.png')
        if(this.state.isConfiging){
            mainIng = require('../../../images/zigbee/searching.gif')
        }

        return(
            <View style={styles.imgWrapper}>
                <Image style={styles.mainImg} source={mainIng}/>
                {this.state.isConfiging ? <Text style={[styles.subTitle,{color: Colors.themeTextLight}]}>正在扫描可用设备</Text> : null}
                <Text style={[styles.content,{color: Colors.themeText}]}>当前该网关下已有<Text style={styles.count}>{this.state.count}</Text>个设备</Text>
                <Text style={[styles.content,{color: Colors.themeText}]}>新增<Text style={styles.count}>{this.state.newCount}</Text>个设备</Text>
            </View>
        )
    }

    renderBottomBtn(){
        return(
            <TouchableOpacity style={styles.bottomBtn} onPress={()=>{
                if(this.state.isConfiging){
                    return
                }
                this.requestZigBeeDuration()
            }}>
                <Text style={styles.bottomText}>允许入网</Text>
            </TouchableOpacity>
        )
    }
	
	render() {
        const Colors = this.props.themeInfo.colors
        
		return (
			<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
                <ScrollView>
                    {this.renderConfigImage()}
                    {this.renderBottomBtn()}
                </ScrollView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1
    },
    imgWrapper:{
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainImg: {
        width: screenW * 0.6,
        height: screenW * 0.6,
    },
    subTitle:{
        fontSize: 14,
        fontWeight: '400',
        marginTop: 10,
        marginBottom: 40
    },
    content:{
        fontSize: 14,
        fontWeight: '400',
        marginTop: 10
    },
    count:{
        fontSize: 14,
        fontWeight: '500',
        color: Colors.newBtnBlueBg
    },
    bottomBtn:{
        marginTop: 80,
        marginHorizontal: '15%',
        justifyContent: 'center',
        alignItems: 'center',
        height: 52,
        borderRadius: 26,
        backgroundColor: Colors.newBtnBlueBg
    },
    bottomText:{
        fontSize: 16,
        color: Colors.white,
        fontWeight: '400'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ZigbeeConfigWifi)



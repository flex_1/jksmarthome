/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
    Modal,
    ImageBackground
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from '../../CommonPage/ControlPanel/Panel';
import { connect } from 'react-redux';
import DateUtil from '../../../util/DateUtil';
import {ColorsDark, ColorsLight} from '../../../common/Themes';
import Draggable from '../../../third/Draggable/Draggable';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

const SliderW = screenW * 0.6
const ThumbSize = SliderW * 0.2523

class ZigbeeDoorLock extends Panel {
	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData') || {}

        _x = 0
        
        this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            shouldReverse: false,
            sliderLockStatus: 1,   //0:不显示  1:正常显示  2:已解锁
            batPercentage: 100,
            fingerList: []
        }
    }
    
	componentDidMount() {
        super.componentDidMount()

        this.requestDeviceStatus()
	}

	componentWillUnmount(){
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        if(data?.BatPercentage != null){
            this.setState({
                batPercentage: data.BatPercentage
            }) 
        }
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
                let statusJson = data.result?.deviceStatus && JSON.parse(data.result?.deviceStatus)
                this.setState({
                    batPercentage: statusJson?.BatPercentage,
                    fingerList: data.result?.zigbeeLockInfo?.value || []
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }
    
    // 开锁
    async controlDevice(callBack){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				params: {
                    id: this.deviceData.deviceId,
                    key: 'status',
                    value: 1
                }
            });
            SpinnerManager.close()
            setTimeout(() => {
                _x = 0
                this.setState({
                    sliderLockStatus: 1
                })
            }, 2500);
			if (data.code == 0) {
				callBack && callBack()
                ToastManager.show('开锁成功')
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误，请稍后重试')
		}
    }

    handleLock(){
        setTimeout(() => {
            _x = 0
            this.setState({
                sliderLockStatus: 2
            },()=>{
                this.controlDevice()
            })
        }, 300);
    }

    renderBattery(){
        if(this.state.batPercentage == null){
            return null
        }
        let percent = parseInt(this.state.batPercentage)
        let batteryIcon = require('../../../images/zigbee/battery_g.png')
        let battery_bg = '#3AC457'

        if(parseInt(percent) <= 20){
            batteryIcon = require('../../../images/zigbee/battery_r.png')
            battery_bg = '#FF0707'
        }

        return(
            <View style={styles.batteryWrapper}>
                <ImageBackground style={styles.batteryImgBg} source={batteryIcon}>
                    <View style={{height: '100%',  borderRadius:2,width: percent + '%',backgroundColor: battery_bg}}/>
                </ImageBackground>
                <Text style={[styles.batPerText,{color: battery_bg}]}>{percent}%</Text>
            </View>
        )
    }

    renderSlider(){
        if(this.state.sliderLockStatus == 2){
            return(
                <ImageBackground style={styles.unlockImgBg} source={require('../../../images/zigbee/unLock.png')}>
                    <Image style={{width: ThumbSize, height: ThumbSize}} source={require('../../../images/zigbee/thumb2.png')}/>
                </ImageBackground>
            )
        }else if(this.state.sliderLockStatus == 0){
            return null
        }
        return(
            <ImageBackground style={styles.sliderImgBg} source={require('../../../images/zigbee/sliderBg.png')}>
                <Text style={styles.tipsText}>滑动到右边开锁</Text>
                <Draggable
                    imageSource={require('../../../images/zigbee/thumb.png')}
                    renderSize={ThumbSize} 
                    x={0} 
                    y={0}
                    minY={0}
                    maxY={ThumbSize}
                    minX={0}
                    maxX={SliderW}
                    touchableOpacityProps={{activeOpacity: 0.7}}
                    animatedViewProps={{activeOpacity: 0.1}}
                    onDragRelease = {(e, gestureState, bounds)=>{
                        _x += gestureState.dx
                        this.setState({shouldReverse: true})
                    }}
                    shouldReverse={this.state.shouldReverse}
                    onReverse={()=>{
                        this.setState({shouldReverse: false})
                        if(_x >=  (SliderW-ThumbSize)/2){
                            this.handleLock()
                            _x = SliderW-ThumbSize
                        }else{
                            _x = 0
                        }
                        return {x:_x, y:0}
                    }}
                />
            </ImageBackground>
        )
    }

    renderBottomBtn(){
        const Colors = this.props.themeInfo.colors
        const count = this.state.fingerList.length
        
        return(
            <View style={[styles.bottomWrapper,{backgroundColor: Colors.themeBg}]}>
                <TouchableOpacity activeOpacity={0.7} style={[styles.bottomTouch,{backgroundColor: Colors.themeBg}]} onPress={()=>{
                    if(count <= 0){
                        ToastManager.show('暂无指纹')
                        return
                    }
                    this.props.navigation.navigate('ZigbeeDoorFingerList',{
                        title: '指纹管理',
                        fingerList: this.state.fingerList
                    })
                }}>
                    <Image style={[styles.icon,{tintColor: Colors.themeTextLight}]} source={require('../../../images/zigbee/lock.png')}/>
                    <View style={styles.content}>
                        <Text style={[styles.title,{color: Colors.themeText}]}>指纹管理</Text>
                        <Text style={[styles.subTitle,{color: Colors.themeTextLight}]}>共有{count}个指纹</Text>
                    </View>
                    <Image style={styles.arrow} source={require('../../../images/ProtocolControl/arrow.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    getMainPanelPage(){
        return(
            <View style={styles.wrapper}>
                <Image style={{width: 160, height:160}} source={require('../../../images/zigbee/doorIcon.png')}/>
                {this.renderBattery()}
                {this.renderSlider()}
                <View style={{flex: 1}}/>
                {this.renderBottomBtn()}
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors
        
		return (
			<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
				{this.getHeaderView()}
				{this.getMainPanelPage()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight,
    },
    wrapper:{
        marginTop: screenH * 0.08,
        flex: 1,
        alignItems:'center'
    },
    unlockImgBg:{
        marginTop: 50, 
        width: SliderW, 
        height: ThumbSize, 
        alignItems:'flex-end'
    },
    sliderImgBg:{
        marginTop: 50, 
        width: SliderW, 
        height: ThumbSize,
        justifyContent:'center',
        alignItems:'center'
    },
    tipsText:{
        color: '#969799', 
        fontSize: 14, 
        fontWeight: '400'
    },
    batteryWrapper:{
        flexDirection: 'row', 
        alignItems: 'center',
        marginTop: 20
    },
    batteryImgBg:{
        width: 20, 
        height:15, 
        resizeMode:'contain',
        padding: 2,
        paddingRight: 3
    },
    batPerText:{
        fontSize: 15, 
        fontWeight: '400', 
        marginLeft: 5
    },
    bottomWrapper:{
        paddingBottom: BottomSafeMargin + screenH*0.08,
        width: '100%'
    },
    bottomTouch:{
        marginHorizontal: 16,
        paddingHorizontal: 16,
        alignItems: 'center',
        height: 60,
        borderRadius: 8,
        flexDirection: 'row',
        //阴影四连
        shadowOffset: { width: 0, height: 0 },
        shadowColor: 'rgba(120,120,120,1)',
        shadowOpacity: 0.22,
        shadowRadius: 8,
        elevation: 4
    },
    icon:{
       width: 20,
       height: 20,
       resizeMode: 'contain' 
    },
    content:{
        marginLeft: 15,
        flex: 1
    },
    arrow:{
        width: 16,
        height: 16
    },
    title:{
        fontSize: 16,
        fontWeight: '400'
    },
    subTitle:{
        fontSize: 12,
        fontWeight: '400',
        marginTop: 5
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ZigbeeDoorLock)



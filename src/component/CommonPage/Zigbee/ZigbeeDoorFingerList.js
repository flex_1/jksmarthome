/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
    Modal,
    ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../../CommonPage/Navigation/HeaderTitle';

class ZigbeeDoorFingerList extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={navigation.getParam('title')}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
        super(props)
        const { getParam } = props.navigation;
        this.state = {
            fingerList: getParam('fingerList')
        }
    }
    
	componentDidMount() {
        
	}

	componentWillUnmount(){
        
    }
    
    renderFingerList(){
        const Colors = this.props.themeInfo.colors

        let list = this.state.fingerList.map((value, index)=>{
            return(
                <TouchableOpacity key={index} activeOpacity={0.7} style={styles.item}>
                    <Text style={[styles.name,{color: Colors.themeText}]}>{value}</Text>
                    <View style={[styles.line,{backgroundColor: Colors.themeBaseBg}]}/>
                </TouchableOpacity>
            )
        })

        return (
            <View style={{marginTop: 6, flex: 1, width: '100%',backgroundColor: Colors.themeBg}}>
                <ScrollView>
                    {list}
                </ScrollView>
            </View>       
        )
    }
	
	render() {
        const Colors = this.props.themeInfo.colors
        
		return (
			<View style={[styles.main,{backgroundColor: Colors.themeBaseBg}]}>
				{this.renderFingerList()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1
    },
    item:{
        width: '100%', 
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    name:{
        fontSize: 16,
        fontWeight: '400'
    },
    line:{
        position: 'absolute',
        height: 1,
        left: 0,
        right: 0,
        bottom: 0
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ZigbeeDoorFingerList)



/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Animated,
	Dimensions,
    Modal,
    Keyboard,
    Alert
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import DateUtil from '../../../util/DateUtil';
import {ColorsDark, ColorsLight} from '../../../common/Themes';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight'
import {FooterLoading,FooterEnd } from "../../../common/CustomComponent/ListLoading";

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const keybordHeight = 180

class ZigbeePanelBindDevice extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text: '保存', onPress:()=>{
                    navigation.getParam('saveBtnClick')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
		super(props);
		const { getParam,setParams } = props.navigation;
        
        this.buttonId = getParam('buttonId')
        this.callBack = getParam('callBack')

        this.state = {
            loading: false,
            ledButtonList: [],
            selectedIds: []
        }

        setParams({
            saveBtnClick: this.saveBtnClick.bind(this)
        })
    }

    getSelectItem(){
        
    }
    
	componentDidMount() {
        this.requestBindList()
	}

	componentWillUnmount(){
        
    }

    saveBtnClick(){
        if(this.state.selectedIds.length <= 0){
            ToastManager.show('请选择一个设备绑定')
            return
        }
        this.requestBindButton()
    }

    async requestBindList() {
        this.setState({loading:true})
        try {
			let data = await postJson({
				url: NetUrls.zigbeeBtnList,
				params: {
                    panelButtonId: this.buttonId
                }
			});
			this.setState({
				loading: false
			})
			if (data.code == 0) {
                this.setState({
                    ledButtonList: data.result?.ledList
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({ 
				loading: false
			})
			ToastManager.show('网络错误');
		}
    }

    // 按键绑定
    async requestBindButton() {
        SpinnerManager.show()
        try {
			let data = await postJson({
				url: NetUrls.bindingZigbeeScenePanel,
				params: {
                    id: this.buttonId,
                    ids: this.state.selectedIds.toString()
                }
			})
            SpinnerManager.close()
			if (data.code == 0) {
                ToastManager.show('绑定成功');
                this.callBack && this.callBack()
                this.props.navigation.pop()
            } else {
				ToastManager.show(data.msg);
			}
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误');
        }
    }

    renderNoContent(){
        if(this.state.loading){
            return (
                <View style={styles.noContentWrapper}>
                    <Text style={styles.noContentText}>正在加载...</Text>
                </View>
            )
        }else{
            return (
                <View style={styles.noContentWrapper}>
                    <Text style={styles.noContentText}>暂无设备</Text>
                </View>
            )
        } 
    }

    renderRowItem(rowData, index){
        const Colors = this.props.themeInfo.colors

        let selectImg = require('../../../images/connectWifi/unselected.png')
        if(this.state.selectedIds.includes(rowData.id)){
            selectImg = require('../../../images/connectWifi/selected.png')
        }
        let roomView = null
        if(rowData.floor && rowData.roomName){
            roomView = <Text style={styles.roomName}>{rowData.floorText + ' ' + rowData.roomName}</Text>
        }

        return(
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={[styles.item,{backgroundColor: Colors.themeBg}]} 
                onPress={()=>{
                    if(this.state.selectedIds.includes(rowData.id)){
                        this.state.selectedIds.remove(rowData.id)
                    }else{
                        this.state.selectedIds.push(rowData.id)
                    }
                    this.setState({
                        selectedIds: this.state.selectedIds
                    })
                }}
            >
                <Image style={styles.selectedImg} source={selectImg}/>
                <View style={[styles.labelWrapper,{flex: 1}]}>
                    <Text style={[styles.name,{color: Colors.themeText}]}>{rowData.name}</Text>
                    {roomView}
                </View>
            </TouchableOpacity>
        )
    }

    renderList(){
        const listData = this.state.ledButtonList

        return(
            <View style={{flex: 1}}>
                <FlatList
                    ref={e => this.flatlist = e}
                    contentContainerStyle={styles.contentStyle}
                    scrollIndicatorInsets={{right: 1}}
                    removeClippedSubviews={false}
                    data={listData}
                    keyExtractor={(item, index)=> 'list_item_'+index}
                    renderItem={({ item, index }) => this.renderRowItem(item, index)}
                    initialNumToRender={10}
                    ListEmptyComponent = {this.renderNoContent()}
                    keyboardDismissMode={'on-drag'}
                    keyboardShouldPersistTaps={'handled'}
                />
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors;

		return (
			<View style={[styles.main,{backgroundColor: Colors.themeBaseBg}]}>
				{this.renderList()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1
    },
    contentStyle:{
        paddingBottom: BottomSafeMargin + 10
    },
    item:{
        height: 60, 
        marginHorizontal: 16,
        paddingHorizontal: 16,
        borderRadius: 5,
        flexDirection: "row",
        alignItems: 'center',
        marginTop: 10,
        backgroundColor: Colors.white
    },
    name:{
        fontSize: 14,
        fontWeight: '400'
    },
    selectedImg:{
        width: 16,
        height:16,
        resizeMode: 'contain'
    },
    roomName:{
        fontSize: 13,
        marginTop: 5
    },
    labelWrapper:{
        marginLeft: 15,
    },
    noContentWrapper:{ 
        marginTop: screenH*0.1, 
        justifyContent:'center',
        alignItems: 'center'
    },
    noContentText:{
        fontSize: 14, 
        color: Colors.themeTextLightGray
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ZigbeePanelBindDevice)



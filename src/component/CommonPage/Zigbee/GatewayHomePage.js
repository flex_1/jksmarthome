/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	StyleSheet,
	Image,
	Text,
	TouchableOpacity,
	Dimensions,
    ImageBackground,
    ScrollView,
    View
} from 'react-native';
import { BottomSafeMargin } from '../../../util/ScreenUtil';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import { connect } from 'react-redux';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const bgW = screenW*0.8
const bgH = screenW*0.8*1.4

class GatewayHomePage extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

    constructor(props) {
		super(props);
        
        this.state = {
           
        }
    }

    renderMainImage(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={[styles.wrapper,{backgroundColor: Colors.themeBg}]}>
                <Image style={styles.mainImg} source={require('../../../images/zigbee/gateway.png')}/>
                <Text style={[styles.sub,{color: Colors.themeText}]}>已通过网络在线</Text>
            </View>
        )
    }

    renderBottomBtn(){
        const Colors = this.props.themeInfo.colors
        
        return(
            <View style={[styles.bottomWrapper,{backgroundColor: Colors.themeBg}]}>
                <TouchableOpacity activeOpacity={0.7} style={[styles.bottomTouch,{backgroundColor: Colors.themeBg}]}>
                    <Image style={styles.icon} source={require('../../../images/zigbee/h.png')}/>
                    <View style={styles.content}>
                        <Text style={[styles.title,{color: Colors.themeText}]}>子设备列表</Text>
                        <Text style={[styles.subTitle,{color: Colors.themeTextLight}]}>共有6个设备已链接在本网关下</Text>
                    </View>
                    <Image style={styles.arrow} source={require('../../../images/ProtocolControl/arrow.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors
        return(
            <View style={{backgroundColor: Colors.themeBaseBg, flex: 1}}>
                {this.renderMainImage()}
                {this.renderBottomBtn()}
            </View>
        )
	}
}

const styles = StyleSheet.create({
    wrapper:{
        flex: 1,
        alignItems: 'center',
        marginTop: 6
    },
    mainImg:{
        width: 160,
        height: 160,
        marginTop: screenH * 0.1
    },
    sub:{
        marginTop: 20,
        fontSize: 14,
        fontWeight: '400'
    },
    bottomWrapper:{
        paddingBottom: BottomSafeMargin + screenH*0.05
    },
    bottomTouch:{
        marginHorizontal: 16,
        paddingHorizontal: 16,
        alignItems: 'center',
        height: 60,
        borderRadius: 8,
        flexDirection: 'row',
        //阴影四连
        shadowOffset: { width: 0, height: 0 },
        shadowColor: 'rgba(120,120,120,1)',
        shadowOpacity: 0.22,
        shadowRadius: 8,
        elevation: 4
    },
    icon:{
       width: 20,
       height: 20,
       resizeMode: 'contain' 
    },
    content:{
        marginLeft: 15,
        flex: 1
    },
    arrow:{
        width: 16,
        height: 16
    },
    title:{
        fontSize: 16,
        fontWeight: '400'
    },
    subTitle:{
        fontSize: 12,
        fontWeight: '400',
        marginTop: 5
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo
    })
)(GatewayHomePage)



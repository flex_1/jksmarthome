/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
    Modal,
    ImageBackground
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from '../../CommonPage/ControlPanel/Panel';
import { connect } from 'react-redux';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

const SliderW = screenW * 0.6
const ThumbSize = SliderW * 0.2523

class ZigbeeHumanInduction extends Panel {
	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData') || {}

        this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            occupancy: 0,  //0-无人  1-有人
            measuredValue: 1,  //光照度,
            batPercentage: 100,
            batVoltage: 0
        }
    }
    
	componentDidMount() {
        super.componentDidMount()

        this.requestDeviceStatus()
	}

	componentWillUnmount(){
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        this.setState({
            occupancy: data?.Occupancy,
            measuredValue: data?.MeasuredValue,
            batPercentage: data?.BatPercentage,
            batVoltage: data?.BatVoltage
        }) 
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData?.deviceId
                }
			});
			if (data.code == 0) {
                let statusJson = data.result?.deviceStatus && JSON.parse(data.result?.deviceStatus)
                this.setState({
                    occupancy: statusJson?.Occupancy,
                    measuredValue: statusJson?.MeasuredValue,
                    batPercentage: statusJson?.BatPercentage,
                    batVoltage: statusJson?.BatVoltage
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }


    renderBattery(){
        if(this.state.batPercentage == null){
            return null
        }
        let percent = parseInt(this.state.batPercentage)
        let batteryIcon = require('../../../images/zigbee/battery_g.png')
        let battery_bg = '#3AC457'

        if(parseInt(percent) <= 20){
            batteryIcon = require('../../../images/zigbee/battery_r.png')
            battery_bg = '#FF0707'
        }

        return(
            <View style={styles.batteryWrapper}>
                <ImageBackground style={styles.batteryImgBg} source={batteryIcon}>
                    <View style={{height: '100%',  borderRadius:2,width: percent + '%',backgroundColor: battery_bg}}/>
                </ImageBackground>
                <Text style={[styles.batPerText,{color: battery_bg}]}>{percent}%</Text>
            </View>
        )
    }

    renderHumanStatus(){
        const Colors = this.props.themeInfo.colors
        let statusText = this.state.occupancy == 1 ? '有人移动' : '无人移动'
        
        return(
            <View style={styles.valueWrapper}>
                <Text style={[styles.statusText,{color: Colors.themeText}]}>{statusText}</Text>
                <Text style={[styles.measuredValue,{color: Colors.themeTextLight}]}>光照度: {this.state.measuredValue} lx</Text>
            </View>
        )
    }

    renderBottomBtn(){
        const Colors = this.props.themeInfo.colors
        
        return(
            <View style={[styles.bottomWrapper,{backgroundColor: Colors.themeBg}]}>
                <TouchableOpacity activeOpacity={0.7} style={[styles.bottomTouch,{backgroundColor: Colors.themeBg}]} onPress={()=>{
                    this.props.navigation.navigate('Setting_Log',{
                        deviceId: this.deviceData?.deviceId
                    })
                }}>
                    <Image style={[styles.icon,{tintColor: Colors.themeTextLight}]} source={require('../../../images/zigbee/log.png')}/>
                    <Text style={[styles.title,{color: Colors.themeText}]}>日志</Text>
                    <Image style={styles.arrow} source={require('../../../images/ProtocolControl/arrow.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    getMainPanelPage(){
        return(
            <View style={styles.wrapper}>
                <Image style={{width: 160, height:160}} source={require('../../../images/zigbee/induction.png')}/>
                {this.renderBattery()}
                {this.renderHumanStatus()}
                <View style={{flex: 1}}/>
                {this.renderBottomBtn()}
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors
        
		return (
			<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
				{this.getHeaderView()}
				{this.getMainPanelPage()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight,
    },
    wrapper:{
        marginTop: screenH * 0.08,
        flex: 1,
        alignItems:'center'
    },
    batteryWrapper:{
        flexDirection: 'row', 
        alignItems: 'center',
        marginTop: 20
    },
    batteryImgBg:{
        width: 20, 
        height:15, 
        resizeMode:'contain',
        padding: 2,
        paddingRight: 3
    },
    batPerText:{
        fontSize: 15, 
        fontWeight: '400', 
        marginLeft: 5
    },
    valueWrapper:{
        justifyContent:'center',
        alignItems:'center',
        marginTop: 20
    },
    bottomWrapper:{
        paddingBottom: BottomSafeMargin + screenH*0.08,
        width: '100%'
    },
    bottomTouch:{
        marginHorizontal: 16,
        paddingHorizontal: 16,
        alignItems: 'center',
        height: 60,
        borderRadius: 8,
        flexDirection: 'row',
        //阴影四连
        shadowOffset: { width: 0, height: 0 },
        shadowColor: 'rgba(120,120,120,1)',
        shadowOpacity: 0.22,
        shadowRadius: 8,
        elevation: 4
    },
    icon:{
       width: 20,
       height: 20,
       resizeMode: 'contain' 
    },
    content:{
        marginLeft: 15,
        flex: 1
    },
    arrow:{
        width: 16,
        height: 16
    },
    title:{
        fontSize: 16,
        fontWeight: '400',
        marginLeft: 10,
        flex: 1
    },
    subTitle:{
        fontSize: 12,
        fontWeight: '400',
        marginTop: 5
    },
    statusText:{
        fontSize: 20,
        fontWeight: '400'
    },
    measuredValue:{
        marginTop: 10,
        fontSize: 14,
        fontWeight: '400'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ZigbeeHumanInduction)



import React, { Component } from 'react';
import {
	View,
    Text,
    Image,
    TouchableOpacity,
    Dimensions,
    StyleSheet,
} from 'react-native';

import { Colors } from '../../common/Constants';

const dialogWidth = Dimensions.get('window').width - 94;

//添加萤石设备弹出的弹窗
export default class YSProtocolDialog extends Component {

    render() {
        return (
			<View style={styles.container}>
				<View style={styles.dialogContainer}>
					<View style={styles.dialogTitleWrapper}>
                        <Text style={styles.dialogTitle}>协议政策</Text>
                    </View>

					<TouchableOpacity
						style={styles.closeWrapper}
                        onPress={()=>{ this.props.close() }}
					>
						<Image style={styles.closeImage} source={require('../../images/close_white.png')}/>
					</TouchableOpacity>

					<Text style={styles.tips}>请阅读并同意以下协议:</Text>

					<View style={styles.protocolWrapper}>
						<View style={styles.point} />
                        <Text style={styles.agreeText}>同意 <Text style={styles.protocolLink} 
                        onPress={()=>{ this.props.serviceAgreement() }}>《翰萨科技摄像头服务协议》</Text></Text>
					</View>

					<View style={[styles.protocolWrapper, { marginTop: 12 }]}>
						<View style={styles.point} />
                        <Text style={styles.agreeText}>同意 <Text style={styles.protocolLink} 
                        onPress={()=>{ this.props.privacyPolicy() }}>《翰萨科技摄像头隐私政策》</Text></Text>
					</View>
                    <TouchableOpacity style={styles.sureTouch} onPress={()=>{ this.props.agree() }}>
                        <Text style={styles.agreeButton}>确定</Text>
                    </TouchableOpacity>
				</View>
			</View>
		)
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        zIndex: 1000,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.translucent,
    },
    dialogContainer: {
        backgroundColor: Colors.white,
        borderRadius: 4,
        width: dialogWidth,
    },
    dialogTitleWrapper: {
        height: 52,
        backgroundColor: Colors.tabActiveColor,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        justifyContent:'center',
        alignItems:'center',
    },
    dialogTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.white,
    },
    closeWrapper: {
        position: 'absolute',
        right: 0,
        top: 0,
        paddingTop: 17,
        paddingBottom: 17,
        paddingLeft: 16,
        paddingRight: 16,
    },
    closeImage: {
        width: 18,
        height: 18,
    },
    tips: {
        marginTop: 28,
        marginLeft: 25,
        marginRight: 25,
        marginBottom: 29,
        fontSize: 18,
        color: Colors.themeTextBlack,
    },
    protocolWrapper: {
        flexDirection: 'row',
        marginLeft: 25,
        marginRight: 25,
        alignItems: 'center',
    },
    point: {
        width: 4,
        height: 4,
        backgroundColor: Colors.themeTextBlack,
        borderRadius: 4,
        marginRight: 10,
    },
    agreeText: {
        fontSize: 15,
        lineHeight: 18,
		color: Colors.themeTextBlack,
    },
    protocolLink: {
        color: Colors.tabActiveColor,
    },
    sureTouch:{
        justifyContent:'center',
        alignItems:'center',
        height: 44,
        backgroundColor: Colors.tabActiveColor,
        borderRadius: 4,
        marginLeft: 25,
        marginRight: 25,
        marginTop: 44,
        marginBottom: 25,
    },
    agreeButton: {
        fontSize: 18,
        color: Colors.white,
    }
});

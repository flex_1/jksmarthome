/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	TextInput,
	DeviceEventEmitter,
	ImageBackground
} from 'react-native';

import { Colors } from '../../../common/Constants';
import HeaderLeftView from '../../../common/CustomComponent/HeaderLeftView';
import { StatusBarHeight } from '../../../util/ScreenUtil';

class Setting_Timeout extends Component {

	static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeftView navigation={navigation}/>,
			title:'超时提醒',
			headerTitleStyle: {
				fontSize: 20,
				color: Colors.themeTextBlack,
				textAlign: 'center',
				flex: 1,
			},
			headerRight: (
				<TouchableOpacity style={styles.navTouch} onPress={() => {
					
				}}>
					<Text style={styles.navText}>保存</Text>
				</TouchableOpacity>
			),
		}
	};

	constructor(props) {
		super(props);
		const {getParam} = this.props.navigation
		this.callBack = getParam('callBack')
		this.state = {
			currentFloor : null,
			floors :[null,1,2,3,4]
		}
	}

	componentDidMount() {
		
	}

	componentWillUnmount(){

	}


	getFloorList(){
		const {pop} = this.props.navigation
		let floors = this.state.floors.map((val,index)=>{
			let selected = null
			let max = val + '小时'
			if(this.state.currentFloor == val){
				selected = <Image style={{width:18,height:18,marginRight:20}} source={require('../../../images/select.png')}/>
			}
			if(!val){
				max = '关闭'
			}
			return(
				<TouchableOpacity activeOpacity={0.7} key={'floor_list_'+index} style={styles.inputWrapper} onPress={()=>{
					this.setState({
						currentFloor : val
					})
					this.callBack && this.callBack(val)
					//pop()
				}}>
					<Text style={{fontSize:15,color:Colors.themeTextBlack,marginLeft:20,flex:1}}>{max}</Text>
					{selected}
				</TouchableOpacity>
			)
		})
		return (
			<View style={{paddingVertical:5,paddingHorizontal:16}}>
				
				{floors}
			</View>
		)
	}

	render() {
		return (
			<ScrollView style={styles.container}>
				{this.getFloorList()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.white,
		flex:1
	},
	inputWrapper:{
		width: '100%',
		height: 60,
		backgroundColor:Colors.white,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:15,
		//阴影四连
		shadowOffset: { width: 0, height: 2 },
		shadowColor: '#070F26',
		shadowOpacity: 0.15,
		shadowRadius: 5,
		elevation: 5
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight:5
	},
	navText:{
		color:Colors.tabActiveColor,
		fontSize:15
	},
});

export default Setting_Timeout

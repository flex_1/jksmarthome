/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    Text,
    View
} from 'react-native';
import { connect } from 'react-redux';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderTitle from '../../CommonPage/Navigation/HeaderTitle';
import DateUtil from "../../../util/DateUtil";

class LogDetail extends Component {

    static navigationOptions = ({navigation}) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={navigation.getParam('title')}/>,
        headerBackground: <HeaderBackground hiddenBorder={false}/>
    })

    constructor(props) {
        super(props);
        const { getParam } = this.props.navigation

        
        this.state = {
            content: getParam('content') || '',
            time: getParam('time') || '',
            type: getParam('type') || '',
            operator: getParam('operator')
        }
    }

    renderContent(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={styles.contentWrapper}>
                <Text style={[styles.time,{color: Colors.themeTextLight}]}>{DateUtil(this.state.time)}</Text>
                <Text style={[styles.time,{color: Colors.themeTextLight,marginTop: 5}]}>指令来源: {this.state.type}</Text>
                <Text style={[styles.time,{color: Colors.themeTextLight,marginTop: 5}]}>操作人: {this.state.operator}</Text>
                <Text style={[styles.content,{color: Colors.themeText}]}>{'    ' + this.state.content}</Text>
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <ScrollView style={{flex:1, backgroundColor: Colors.themeBaseBg}}>
                {this.renderContent()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    contentWrapper:{
        paddingHorizontal: 16,
        marginTop: 20
    },
    time:{
        fontSize: 14,
        lineHeight: 22,
        width:'100%',
        textAlign: 'center'
    },
    content: {
        fontSize: 15,
        lineHeight: 22,
        marginTop: 15
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo
    })
)(LogDetail)

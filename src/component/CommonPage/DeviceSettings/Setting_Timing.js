/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	Image
} from 'react-native';
import { connect } from 'react-redux';
import { Colors, NetUrls } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import {ChangeNumToWeek} from "../../../util/DateUtil";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderRight from '../Navigation/HeaderRight';
import HeaderTitle from '../Navigation/HeaderTitle';
import {ImagesLight, ImagesDark} from '../../../common/Themes';
import SwitchButton from '../../../common/CustomComponent/SwitchButton';
import NoContentButton from '../../../common/CustomComponent/NoContentButton';

const screenH = Dimensions.get('window').height;

class Setting_Timing extends Component {
    
    static navigationOptions = ({ navigation }) => {
        let rightBtn = null
        if(!navigation.getParam('isJuniorUser')){
            rightBtn = (
                <HeaderRight buttonArrays={[
                    {text:'添加定时',onPress:()=>{
                        navigation.getParam('addTiming')()
                    }}
                ]}/>
            )
        }
		return {
			headerLeft: <HeaderLeft navigation={navigation}/>,
			headerTitle: <HeaderTitle title={'定时'}/>,
			headerRight: rightBtn,
            headerBackground: <HeaderBackground/>
		}
	}

	constructor(props) {
		super(props);
		const { getParam,setParams } = this.props.navigation
		this.callBack = getParam('callBack')
		this.sceneId = getParam('sceneId')
		this.deviceData = getParam('deviceData')

		this.state = {
			timers: [],
			timerId: null
        }
        
        setParams({
            addTiming: this.addTiming.bind(this)
        })
	}

	componentDidMount() {
		this.requestTimingList()
	}

	componentWillUnmount() {
		
    }
    
    addTiming(){
        const { navigate } = this.props.navigation
		navigate('Setting_Timing_AddTiming', {
			title: '添加定时',
			sceneId: this.sceneId, 
			deviceId: this.deviceData?.id, 
			callBack: () => {
				this.requestTimingList()
				this.callBack && this.callBack()
			}
		})
    }

	// 获取 定时列表
	async requestTimingList() {
		let params = {}
		if (this.sceneId) {
			params.sceneId = this.sceneId
		}
		if (this.deviceData) {
			params.deviceId = this.deviceData && this.deviceData.id
		}
		try {
			let data = await postJson({
				url: NetUrls.getTimer,
				params: {
					...params,
				}
			});
			if (data && data.code == 0) {
				this.setState({
					timers: data.result || []
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

	// 修改 定时器状态
	async requestSaveTimer(timerData={}, target, callBack) {
		let params = {}
		if (this.sceneId) {
			params.sceneId = this.sceneId
		}
		if (this.deviceData) {
			params.deviceId = this.deviceData && this.deviceData.id
		}
		try {
			let data = await postJson({
				url: NetUrls.addTimer,
				params: {
					...params,
					id: timerData.id,
					uuid: timerData.uuid,
					cyclic: timerData.cyclic,
					status: target,
					hour: timerData.hour,
					minute: timerData.minute
				}
			})
			if (data && data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

	getFloorList() {
        const Colors = this.props.themeInfo.colors;
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight;
        const isJuniorUser = this.props.userInfo.memberType == 2

		if( !this.state.timers || this.state.timers.length <= 0){
			return(
				<View style={styles.noContentWrapper}>
                    <Text style={styles.noContentText}>暂无设置定时</Text>
                    {isJuniorUser ? null :
                        <NoContentButton
                            text={'添加定时'}
                            onPress={()=>{
                                this.addTiming()
                            }}
                        />
                    }
				</View>
			)
		}
		let timers = this.state.timers.map((val, index) => {
			let textColor = val.status ? Colors.themeText : Colors.themeTextLight
			let statusColor = val.status ? Colors.themeButton : Colors.themeTextLight
            let operationText = null
            if(this.sceneId){
                operationText = '执行' 
            }
			// 分钟加 0
			let minu = val.minute
			if (minu < 10) {
				minu = '0' + minu
			}
			// 获取 定时循环周期
			let cyclicText = ChangeNumToWeek(val.cyclic)
            let excuteDate = ''
            if(val.date){
                excuteDate = '于' + val.date + '执行'
            }

			return (
                <TouchableOpacity
                    style={[styles.inputWrapper, {backgroundColor: Colors.themeBg}]}
                    key={'floor_list_' + index}
                    activeOpacity={0.7}  
					onPress={() => {
                        if(this.props.userInfo.memberType == 2){
                            return
                        }
					    const { navigate } = this.props.navigation
					    let currentDate = new Date();
					    currentDate.setHours(val.hour)
					    currentDate.setMinutes(val.minute);
                        navigate('Setting_Timing_AddTiming', {
                            title: '设置定时',
                            timerId: val.id,
                            uuid: val.uuid,
                            sceneId : this.sceneId,
                            deviceId: this.deviceData && this.deviceData.id, 
                            currentDate: currentDate,
                            selects: val.cyclic,
                            status: val.status,
                            operation: val.operation,
                            deviceStatus: val.deviceStatus && JSON.parse(val.deviceStatus),
                            callBack: () => {
                                this.requestTimingList()
                                this.callBack && this.callBack()
                            }
                        })   
				    }}
                >
					<View style={{alignItems: 'center',flexDirection: 'row'}}>
						<Text style={{ fontSize: 24, color: textColor, fontWeight: 'bold' }}>{val.hour}:{minu} </Text>
						{operationText ? <Text style={{marginLeft: 30, color: statusColor}}>{operationText}</Text> : null}
                        <View style={{flex: 1}}/>
                        <SwitchButton
                            style={styles.switchTouch}
                            value = {val.status}
                            onPress = {()=>{
                                let target = val.status ? 0 : 1
                                this.requestSaveTimer(val, target, () => {
                                    this.state.timers[index].status = target
                                    this.setState({
                                        ...this.state
                                    })
                                })
                            }}
                        />
					</View>
					<Text style={{marginTop:5,marginLeft:5,fontSize:12,color:textColor}}>{cyclicText}   {excuteDate}</Text>
				</TouchableOpacity>
			)
		})
		return (
			<View style={{ paddingVertical: 5, paddingHorizontal: 16 }}>
				{timers}
			</View>
		)
	}

	render() {
		const Colors = this.props.themeInfo.colors;
		
		return (
            <ScrollView 
                style={{backgroundColor: Colors.themeBaseBg}} 
                contentContainerStyle={styles.scrollContent}
            >
				{this.getFloorList()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	scrollContent:{
		paddingBottom: 50,
	},
	inputWrapper: {
		width: '100%',
		height: 70,
		borderRadius: 5,
		justifyContent:'center',
		marginTop: 15,
		paddingLeft: 16
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight: 5
    },
    switchTouch:{
        height: '100%',
        paddingHorizontal: 16
    },
    switchIcon:{
        width:50,
        height:30,
        resizeMode:'contain'
    },
    noContentWrapper:{
        marginTop: screenH * 0.3,
        alignItems: 'center', 
        justifyContent: 'center'
    },
    noContentText:{
        color: Colors.themeTextLightGray,
        fontSize: 14,
        marginBottom: 20
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
        userInfo: state.userInfo
    })
)(Setting_Timing)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    NativeModules,
    SwipeableFlatList,
    Alert,
    RefreshControl
} from 'react-native';
import { connect } from 'react-redux';
import {Colors, NetUrls, CurrentState} from '../../../common/Constants';
import {GenerateUUID} from '../../../util/NumberUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import {postJson} from '../../../util/ApiRequest';
import DateUtil from '../../../util/DateUtil';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderTitle from '../Navigation/HeaderTitle';
import {ImagesLight, ImagesDark} from '../../../common/Themes';
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import {ListLoading} from "../../../common/CustomComponent/ListLoading";
import SwitchButton from '../../../common/CustomComponent/SwitchButton';

//侧滑最大距离
const maxSwipeDistance = 90
//侧滑按钮个数
const countSwiper = 1

const JKUMengShare = Platform.select({
    ios: NativeModules.UMShareModule,
    android: NativeModules.JKUMAndShareUtils
});

class Setting_Share extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeft navigation={navigation}/>,
			headerTitle: <HeaderTitle title={navigation.getParam('title')}/>,
            headerBackground: <HeaderBackground/>
		}
	}

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation
        this.callBack = getParam('callBack')

        this.deviceData = getParam('deviceData') || {}
        this.sceneData = getParam('sceneData') || {}
        this.roomData = getParam('roomData') || {}
        this.houseData = getParam('houseData') || {}
        this.id = getParam('id')

        this.shareData = getParam('shareData')
        this.shareType = getParam('shareType')  // 1设备 2场景 3房间 4房屋

        this.state = {
            shareList: null,
            shareNum: 0,
            isRefreshing: false
        }
    }

    componentDidMount() {
        this.requestShareList()
    }

    componentWillUnmount() {

    }

    async requestShareList() {
        try {
            let deviceData = await postJson({
                url: NetUrls.shareList,
                params: {
                    deviceId: this.id,
                    type: this.shareType
                }
            });
            this.setState({isRefreshing: false})
            if (deviceData.code == 0) {
                this.setState({
                    shareList: deviceData.result || [],
                    shareNum: (deviceData.result && deviceData.result.length) || 0
                })
            } else {
                ToastManager.show(deviceData.msg)
            }
        } catch (error) {
            this.setState({isRefreshing: false})
            ToastManager.show('网络错误')
        }
    }

    //创建分享
    async creatShare(uuid, callBack) {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.addShare,
                params: {
                    deviceId: this.id,
                    uuid: uuid,
                    type: this.shareType
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                if (data.result) {
                    callBack && callBack()
                    this.requestShareList()
                } else {
                    ToastManager.show('创建分享失败，请稍后重试')
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

    //删除分享
    async deleteShare(id) {
        SpinnerManager.show()
        try {
            let deviceData = await postJson({
                url: NetUrls.deleteShare,
                params: {
                    id: id
                }
            });
            SpinnerManager.close()
            if (deviceData.code == 0) {
                this.requestShareList()
            } else {
                ToastManager.show(deviceData.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

    //更改分享状态
    async updateShare(id, status) {
        SpinnerManager.show()
        try {
            let deviceData = await postJson({
                url: NetUrls.updateShare,
                params: {
                    id: id,
                    status: status
                }
            });
            SpinnerManager.close()
            if (deviceData.code == 0) {
                this.requestShareList()
            } else {
                ToastManager.show(deviceData.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

    async shareBtnClick(){
        let {shareTitle, shareContent, shareIcon, shareLink, miniprogramLink} = this.shareData
        let id = this.id
        let uuid = GenerateUUID()
        let tokens = await LocalTokenHandler.get()

        this.creatShare(uuid, () => {
            if (shareLink) {
                shareLink = shareLink.replace('${id}', id)
                shareLink = shareLink.replace('${shareId}', uuid)
            }

            if(!miniprogramLink){
                ToastManager.show('分享链接为空，请稍后再试')
                return
            }

            miniprogramLink = miniprogramLink + '?source=1'
            miniprogramLink = miniprogramLink + '&id=' + id
            miniprogramLink = miniprogramLink + '&shareId=' + uuid
            miniprogramLink = miniprogramLink + '&token=' + tokens.token

            console.log('link == ' + miniprogramLink);
            
            /* UShareWXMiniProgramTypeRelease = 0,    正式版  */
            /* UShareWXMiniProgramTypeTest = 1,       开发版  */
            /* UShareWXMiniProgramTypePreview = 2,    体验版  */
            let miniProgramType = CurrentState == 'production' ? 0:2
            
            JKUMengShare.shareToMinProgram(shareTitle, shareIcon, miniprogramLink, shareContent, miniProgramType,(code, msg) => {
                if (code == 200) {
                    ToastManager.show('分享成功')
                } else {
                    ToastManager.show('分享失败')
                }
            })
        });
    }

    onRefresh(){
        if (this.state.isRefreshing) return
        this.setState({ isRefreshing: true })
        this.requestShareList()
    }

    renderIcon(){
        const Colors = this.props.themeInfo.colors
        let icon = null
        let name = ''
        if(this.shareType == 1){
            icon = this.props.themeInfo.isDark ? { uri: this.deviceData.nightImg } : { uri: this.deviceData.dayImg }
            name = this.deviceData.deviceName
        }else if(this.shareType == 2){
            icon = this.sceneData.icon
            name = this.sceneData.name
        }else if(this.shareType == 3){
            icon = { uri: this.roomData.roomImg }
            name = this.roomData.name
        }else if(this.shareType == 4){
            name = this.houseData.name
        }

        return(
            <View style={styles.iconWrapper}>
                {icon ? <Image style={styles.icon} source={icon}/> : null}
                <Text style={[styles.name,{color: Colors.themeText}]}>{name}</Text>
            </View>
        )
    }

    getShareButton() {
        const Colors = this.props.themeInfo.colors;
        let shareNum = this.state.shareNum
        
        return (
            <View style={{paddingHorizontal: 16}}>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.inputWrapper,{backgroundColor: Colors.themeBg}]} 
                    onPress={() => {
                        this.shareBtnClick()
                    }}
                >
                    <Image style={{width: 35, height: 35}} source={require('../../../images/settingIcon/wechat.png')}/>
                    <Text style={[styles.topItemTitle,{color: Colors.themeText}]}>分享到微信</Text>
                    <Text style={{fontSize: 15, color: Colors.themeTextLight}}>{shareNum}个</Text>
                    <Image style={{marginLeft: 10, width: 7, height: 13}} source={require('../../../images/enter_light.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    //关闭侧滑栏
    closeSwiper() {
        this.swiperFlatlist && this.swiperFlatlist.setState({openRowKey: null})
    }

    // 获取侧滑设置
    getQuickActions(rowData, index) {
        return (
            <View style={styles.quickAContent}>
                <TouchableOpacity activeOpacity={0.7} onPress={() => {
                    this.closeSwiper()
                    Alert.alert(
                        '提示',
                        '确认删除?',
                        [
                            {
                                text: '取消', onPress: () => {
                                }, style: 'cancel'
                            },
                            {
                                text: '删除', onPress: () => {
                                    this.deleteShare(rowData.id)
                                }
                            },
                        ]
                    )
                }}>
                    <View style={[styles.quick, {backgroundColor: Colors.themBgRed}]}>
                        <Text style={{color: Colors.white, fontSize: 14}}>删除</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    renderStatusControl(rowData){
        const Colors = this.props.themeInfo.colors
        const startTimeStamp = parseInt(rowData.startDate)
        const endTimeStamp = parseInt(rowData.endDate)
        const serverTime = parseInt(rowData.serverTime)

        if(serverTime < startTimeStamp){
            return <Text style={[styles.statusText,{color: Colors.newTheme}]}>未开始</Text>
        }else if(serverTime > endTimeStamp){
            return <Text style={[styles.statusText,{color: Colors.themeTextLight}]}>已失效</Text>
        }else{
            const status = rowData.status == 1
            
            return(
                <SwitchButton
                    style = {styles.statusTouch}
                    isAsync = {true}
                    value = {status}
                    onPress = {()=>{
                        let target = status ? 0 : 1
                        this.updateShare(rowData.id, target)
                    }}
                />
            )
        }
    }

    renderRowItem(rowData, index) {
        const {navigate} = this.props.navigation
        const Colors = this.props.themeInfo.colors;
        const createTime = DateUtil(rowData.createTime)
        const startDate = DateUtil(rowData.startDate)
        const endDate = DateUtil(rowData.endDate)
        
        return (
            <View style={styles.itemWrapper}>
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={[styles.itemTouch,{backgroundColor: Colors.themeBg}]} 
                    onPress={() => {
                        navigate('ShareDetail',{
                            beginDate: DateUtil(rowData.startDate,'yyyy-MM-dd'),
                            endDate: DateUtil(rowData.endDate,'yyyy-MM-dd'),
                            id: rowData.id,
                            name: rowData.name,
                            callBack: ()=>{ this.requestShareList() }
                        })
                        this.closeSwiper()
                    }}
                >
                    <View style={styles.titleLine}>
                        <Text style={[styles.itemName,{color: Colors.themeText}]}>{rowData.name}</Text>
                        <Image style={styles.settingImg} source={require('../../../images/setting_light.png')} />
                    </View>
                    <View style={{paddingBottom: 10}}>
                        <Text style={[styles.creatTime,{color: Colors.themeTextLight}]}>创建时间: {createTime}</Text>
                        <Text style={[styles.creatTime,{color: Colors.themeTextLight}]}>生效时间: {startDate}</Text>
                        <Text style={[styles.creatTime,{color: Colors.themeTextLight}]}>结束时间: {endDate}</Text>
                    </View>
                    {this.renderStatusControl(rowData)}
                </TouchableOpacity>
            </View>
        )
    }

    _extraUniqueKey(item, index) {
        return "device_index_" + index;
    }

    renderNoContent(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={{alignItems:'center',marginTop: 50}}>
				<Text style={{fontSize:14,color:Colors.themeTextLight}}>暂无分享</Text>
			</View>
        )
    }

    getShareListView() {
        let shareList = this.state.shareList

        if (!shareList) {
            return <ListLoading/>
        }
        return (
            <SwipeableFlatList
                ref={e => this.swiperFlatlist = e}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{paddingBottom: 60}}
                removeClippedSubviews={false}
                data={shareList}
                keyExtractor={this._extraUniqueKey}
                renderItem={({item, index}) => this.renderRowItem(item, index)}
                renderQuickActions={({item, index}) => this.getQuickActions(item, index)}
                maxSwipeDistance={maxSwipeDistance}
                bounceFirstRowOnMount={false}
                initialNumToRender={5}
                ListEmptyComponent = {this.renderNoContent()}
                refreshControl={
                    <RefreshControl
                        colors={[Colors.themeBGInactive]}
                        tintColor={Colors.themeBGInactive}
                        refreshing={this.state.isRefreshing}
                        onRefresh={this.onRefresh.bind(this)}
                    />
                }
            />
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors;

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderIcon()}
                {this.getShareButton()}
                <Text style={[styles.listTitle,{color: Colors.themeText}]}>正在进行的分享</Text>
                {this.getShareListView()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    inputWrapper: {
        width: '100%',
        height: 60,
        borderRadius: 5,
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 15,
        paddingHorizontal: 16
    },
    topItemTitle:{
        fontSize: 15,
        marginLeft: 15,
        flex: 1
    },
    listTitle: {
        marginVertical: 15,
        fontSize: 16,
        marginLeft: 16,
        fontWeight: 'bold'
    },
    quickAContent: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: 15,
        overflow: 'hidden',
        marginRight: 16,
        paddingVertical: 1
    },
    quick: {
        backgroundColor: Colors.white,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: maxSwipeDistance / countSwiper,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
    },
    itemWrapper: {
        width: '100%',
        paddingHorizontal: 16,
        marginTop: 15
    },
    itemTouch: {
        paddingLeft: 16,
        borderRadius: 5
    },
    itemName:{
        fontSize: 16, 
        color: Colors.themeText, 
        fontWeight:'bold',
        flex: 1
    },
    statusImg: {
        width: 50,
        height: 30,
        resizeMode: 'contain'
    },
    creatTime:{
        fontSize: 13,
        flex: 1,
        marginBottom: 5
    },
    iconWrapper:{
        marginTop: 5,
        flexDirection:'row',
        alignItems:'center',
        justifyContent: 'center',
        height: 45
    },
    icon:{
        width:40,
        height:40,
        marginRight: 20,
        resizeMode:'contain'
    },
    name:{
        fontSize:16, 
        fontWeight:'bold'
    },
    statusTouch: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        paddingHorizontal: 20,
        justifyContent: 'center',
        paddingVertical: 15
    },
    statusText:{
        position: 'absolute',
        right: 20,
        bottom: 15,
        fontSize: 15
    },
    titleLine:{
        flexDirection:'row',
        alignItems:'center',
        paddingVertical: 10,
        paddingRight: 20
    },
    settingImg:{
        width: 20,
        height: 20,
        resizeMode: 'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Setting_Share)

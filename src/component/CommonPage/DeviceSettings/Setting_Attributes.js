/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	DeviceEventEmitter
} from 'react-native';
import { connect } from 'react-redux';
import { Colors, NetUrls } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '..//Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderTitle from '../Navigation/HeaderTitle';
import HeaderRight from '../Navigation/HeaderRight';
import {ImagesLight, ImagesDark} from '../../../common/Themes';


class Setting_Attributes extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text:'保存',onPress:()=>{
                        navigation.getParam('saveBtnClick')()
                    }}
                ]}/>
            ),
            headerTitle: <HeaderTitle title={'属性停启用'}/>,
            headerBackground: <HeaderBackground/>,
        }
	}

	constructor(props) {
		super(props);
		const { getParam, setParams } = this.props.navigation
		this.callBack = getParam('callBack')
		this.deviceData = getParam('deviceData')

		this.state = {
			atrributeList: null
        }

        setParams({
            saveBtnClick: this.saveBtnClick.bind(this)
        })
	}

	componentDidMount() {
		this.requestAttribute()
	}

	componentWillUnmount() {
		
	}

    saveBtnClick(){
        if(!this.state.atrributeList){
            ToastManager.show('数据加载中')
            return
        }
        let attributes = []
        for (const attr of this.state.atrributeList) {
            if(attr.state == 0){
                attributes.push(attr.type)
            }
        }
        this.requestDisableAttributes(attributes.toString())
    }

	// 获取属性列表
	async requestAttribute() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.getAttribute,
				params: {
					deviceId: this.deviceData && this.deviceData.id,
				}
			})
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.setState({
                    atrributeList: data.result?.list || []
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    // 请求用属性
	async requestDisableAttributes(attributes) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.disableAttributes,
				params: {
					deviceId: this.deviceData && this.deviceData.id,
                    disabledAttributes: attributes
				}
			})
			SpinnerManager.close()
			if (data && data.code == 0) {
                DeviceEventEmitter.emit('sensorAttributesUpdate')
				this.props.navigation.pop()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	renderList() {
        if(!this.state.atrributeList || !this.state.atrributeList.length){
            return null
        }
        const defaultImg = this.props.themeInfo.isDark ? ImagesDark.default : ImagesLight.default
        const Colors = this.props.themeInfo.colors

        const attributes = this.state.atrributeList.map((value, index)=>{

            const checkImg = value.state == 0 ? require('../../../images/rectangle_unselect.png') :
            require('../../../images/rectangle_select.png')
            const nameColor = value.state == 2 ? Colors.themeTextLight : Colors.themeText
            const nameFontWeight = value.state == 2 ? 'normal' : '500'
            const tintColor = value.state == 2 ? {tintColor: Colors.themeTextLight} : {}

            return(
                <TouchableOpacity
                    key={'attribute_'+index}
                    activeOpacity={0.7}
                    style={styles.item} 
                    onPress={()=>{
                        if(value.state == 2){
                            ToastManager.show('该属性不可被停用。')
                            return
                        }
                        let newRowdata = value
                        newRowdata.state = value.state == 1 ? 0:1
                        this.state.atrributeList[index] = newRowdata
                        this.setState({
                            ...this.state
                        })
                    }}
                >
                    <Image style={[styles.checkIcon,tintColor]} source={checkImg}/>
                    <Image style={styles.icon} defaultSource={defaultImg} source={{uri: value.icon}}/>
                    <Text style={[styles.name,{color: nameColor, fontWeight:nameFontWeight}]}>{value.name}</Text>
                </TouchableOpacity>
            )
        })
        
		return (
			<View style={{marginTop: 10}}>
				{attributes}
			</View>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<ScrollView 
                style={[styles.container,{backgroundColor: Colors.themeBg}]}
                contentContainerStyle={{paddingBottom: 50}}
            >
				{this.renderList()}
			</ScrollView>
		)
	}
}


const styles = StyleSheet.create({
	container: {
		flex: 1
	},
    item:{
        flexDirection:'row',
        alignItems:'center',
        paddingVertical: 5,
        paddingHorizontal: 20,
        marginTop: 10,
    },
    checkIcon:{
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
	icon:{
        width: 32,
        height: 32,
        resizeMode: 'contain',
        marginLeft: 15
    },
    name:{
        fontSize: 15,
        marginLeft: 15
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Setting_Attributes)

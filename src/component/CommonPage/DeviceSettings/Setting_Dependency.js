/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Modal,
	Alert,
    Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import { Colors, NetUrls } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderTitle from '../Navigation/HeaderTitle';
import HeaderRight from '../Navigation/HeaderRight';
import {ListLoading,ListError } from "../../../common/CustomComponent/ListLoading";
import NoContentButton from '../../../common/CustomComponent/NoContentButton';

const screenH = Dimensions.get('window').height

class Setting_Dependency extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'设备依赖'}/>,
            headerRight: (
                navigation.getParam('isJuniorUser') ? null : <HeaderRight buttonArrays={[
                    {text:'添加设备',onPress:()=>{
                        navigation.getParam('addDeviceBtn')()
                    }}
                ]}/> 
            ),
            headerBackground: <HeaderBackground/>,
        }
	}

	constructor(props) {
		super(props);
		const { getParam, setParams } = this.props.navigation
		this.deviceData = getParam('deviceData') || {}
        this.deviceId = this.deviceData.id
        this.isJuniorUser = getParam('isJuniorUser')

		this.state = {
            loading: true,
			deviceList: null,
            modalVisible: false,
            pickerData: [],
            pickerTitle: '',
            allKeys: null,
            allValues: null,
            selectedId: null,
            selectedValue: [],
            selectedKey: null
        }

        setParams({
            addDeviceBtn: this.addDevice.bind(this)
        })
	}

	componentDidMount() {
		this.requestDependcyList()
	}

	componentWillUnmount() {
		
	}

	// 获取依赖列表
	async requestDependcyList() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.dependencyList,
				params: {
					deviceId: this.deviceId,
				}
			})
            this.state.loading = false
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.setState({
                    deviceList: data.result || []
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            this.state.loading = false
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    // 添加依赖设备
	async requestAddDependcyDevice(dpyDeviceId) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.chooseDpyDevice,
				params: {
					deviceId: this.deviceId,
                    dpyDeviceId: dpyDeviceId
				}
			})
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.requestDependcyList()
                ToastManager.show('设备添加成功。')
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    // 添加,删除 属性
    async requestAddAttribute(type, key, value) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.saveDpyAttribute,
				params: {
					deviceId: this.deviceId,
                    id: this.state.selectedId,
                    type: type,
                    key: key,
                    value: value
				}
			})
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.requestDependcyList()
                if(type == 1){
                    ToastManager.show('属性添加成功。')
                }else if(type == 2){
                    ToastManager.show('属性删除成功。')
                }else if(type == 3){
                    ToastManager.show('依赖设备删除成功。')
                }
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    // 修改 属性
    async requestUpdateAttribute(value) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.saveDpyAttribute,
				params: {
					deviceId: this.deviceId,
                    id: this.state.selectedId,
                    type: 4,
                    key: this.state.selectedKey,
                    value: value
				}
			})
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.requestDependcyList()
                ToastManager.show('属性修改成功。')
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    // 添加设备
    addDevice(){
        const {navigate, pop} = this.props.navigation

        navigate('ExecuteDevice',{
            filterType: 3,
            type: 10,
            floorType: 10,
            intelligentId: this.deviceId,
            callBack: (dpyDeviceId)=>{
                pop()
                this.requestAddDependcyDevice(dpyDeviceId)
            }
        })
    }

    //删除设备
    deleteDypDevice(){
        Alert.alert(
            '提示',
            '删除该设备后，属性也会随之删除，确认删除?',
            [
                {text: '取消', onPress: () => {}, style: 'cancel'},
                {text: '删除', onPress: () => { this.requestAddAttribute(3) }},
            ]
        )
    }

    renderModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                statusBarTranslucent={false}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={styles.modalTouchable} 
                    onPress={() => {
                        this.setState({
                            modalVisible: false
                        }, () => {
                            window.CustomPicker.hide()
                        })
                    }}
                >
                </TouchableOpacity>
            </Modal>
        )
    }

    // 筛选框 确认按钮
    pickerConfirmClick(index){
        if(index.length > 1){
            let key_index = index[0]
            let value_index = index[1]
        
            let key = this.state.allKeys[key_index]
            let value = this.state.allValues[key_index][value_index]

            this.requestAddAttribute(1, key, value)
        }else{
            let value_index = index[0]

            let value = this.state.allValues[value_index]

            this.requestUpdateAttribute(value)
        }
    }

    // 属性按钮点击
    arttriClick(rowData, key, valueLabel){
        if(this.isJuniorUser){
            return
        }
        let id = rowData.id
        let atrributes = rowData.attributes
        let pickerData = []
        let allValues = []
        for (const attri of atrributes) {
            if(attri.key == key){
                for (const val of attri.list) {
                    pickerData.push(val.valueLabel)
                    allValues.push(val.value)
                }
            }
        }
        this.setState({
            pickerTitle: rowData.dpyDeviceName,
            pickerData: pickerData,
            selectedValue: [valueLabel],
            selectedId: id,
            allValues: allValues,
            modalVisible: true,
            selectedKey: key
        })
    }

    // 添加属性按钮点击
    addAttriBtnClick(rowData){
        let exitAttributes = []
        for (const attr of rowData.dpyAttributes) {
            exitAttributes.push(attr.key)
        }

        let attributes = rowData.attributes || []
        let pickerData = []
        let allKeys = []
        let allValues = []
        for (const attr of attributes) {
            if(exitAttributes.includes(attr.key)){
                continue
            }
            allKeys.push(attr.key)
            let _values = []
            let attrDic = {}
            let valList = []
            for (const val of attr.list) {
                _values.push(val.value)
                valList.push(val.valueLabel)
            }
            allValues.push(_values)
            attrDic[attr.keyLabel] = valList
            pickerData.push(attrDic)
        }
        this.setState({
            allKeys: allKeys,
            allValues: allValues,
            pickerTitle: rowData.dpyDeviceName,
            pickerData: pickerData,
            selectedId: rowData.id,
            modalVisible: true,
            selectedValue: []
        })                       
    }

    // 弹出 Modal
    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerData || [],
            pickerTitleText: this.state.pickerTitle,
            selectedValue: this.state.selectedValue,
            onPickerConfirm: (data,index) => {
                this.setState({modalVisible: false}, ()=>{
                    this.pickerConfirmClick(index)
                })
            },
            onPickerCancel: data => {
                this.setState({modalVisible: false}, ()=>{
                    window.CustomPicker.hide()
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

    // 添加属性按钮
    renderAddAttriButton(rowData){
        if(this.isJuniorUser){
            return null
        }
        let dypLength = rowData.dpyAttributes.length
        let attributes = rowData.attributes || []
        let attriLength = attributes.length
        if(dypLength >= attriLength){
            return null
        }
        const Colors = this.props.themeInfo.colors

        return(
            <TouchableOpacity
                activeOpacity={0.7}
                style={[styles.attriAddButton,{backgroundColor: Colors.themeBg}]}
                onPress={()=>{
                    // 添加属性按钮点击
                    this.addAttriBtnClick(rowData)
                }}
            >
                <Text style={styles.attriAddText}>添加属性</Text>
            </TouchableOpacity>
        )
    }

    // 添加设备按钮
    renderAddDeviceButton(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={{alignItems: 'center', justifyContent:'center',marginTop: screenH*0.3}}>
                <Text style={{color: Colors.themeTextLight,fontSize: 14}}>暂无依赖设备</Text>
                {this.isJuniorUser ? null : 
                    <NoContentButton 
                        style={{marginTop:20}} 
                        text={'添加设备'} 
                        onPress={()=>{ this.addDevice() }}
                        textColor={Colors.themeTextLight}
                    />
                }
            </View>
        )
    }

    renderAttributes(rowData){
        const Colors = this.props.themeInfo.colors
        let dpyAttributes = rowData.dpyAttributes || []

        let attriList = dpyAttributes.map((value, index)=>{
            let isShowDelete = dpyAttributes.length > 1
            if(this.isJuniorUser){
                isShowDelete = false
            }
            return(
                <View key={'attri_'+index} style={styles.attributeWrapper}>
                    <View style={styles.splitWrapper}>
                        <View style={[styles.attriSplit,{backgroundColor: Colors.themeTextLight}]}/>
                        <View style={[styles.attriSplit,{backgroundColor: Colors.themeTextLight}]}/>
                    </View>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={[styles.attributeItem,{backgroundColor: Colors.themeBg}]} 
                        onPress={()=>{
                            this.arttriClick(rowData, value.key, value.valueLabel)
                        }}
                    >
                        <Text style={[styles.attriSerial,{color: Colors.themeText}]}>{index+1}.</Text>
                        <Text style={[styles.attriName,{color: Colors.themeText}]}>{value.keyLabel}</Text>
                        <Text style={[styles.attriValue,{color: Colors.themeText}]}>{value.valueLabel}</Text>
                        <View style={{flex: 1}}/>
                        {isShowDelete ? <TouchableOpacity style={styles.deleteBtn} onPress={()=>{
                            this.state.selectedId = rowData.id
                            this.requestAddAttribute(2, value.key)
                        }}>
                            <Image style={styles.deleteImg} source={require('../../../images/user_manage/memberDelete.png')}/>
                        </TouchableOpacity> : null}
                    </TouchableOpacity>
                </View>
            )
        })

        return(
            <View>
                {attriList}
                {this.renderAddAttriButton(rowData)}
            </View>
        )
    }


    renderDeviceList(){
        let list = this.state.deviceList

        if(!list && this.state.loading){
            return <ListLoading/>
        }
        if(!list && !this.state.loading){
            return <ListError onPress={()=>{ this.requestDependcyList() }}/>
        }
        if(list.length <= 0){
            return this.renderAddDeviceButton()
        }

        const Colors = this.props.themeInfo.colors

        let deviceList = list.map((value, index)=>{
            let roomName = null
            const deviceIcon = this.props.themeInfo.isDark ? { uri: value.nightIcon } : { uri: value.dayIcon }

            if (value.floorText) {
                roomName = value.floorText + '  ' + value.roomName
            }
            return(
                <View key={'device_'+index} style={styles.itemWrapper}>
                    <View style={[styles.deviceItem,{backgroundColor: Colors.themeBg}]}>
                        <Image style={styles.deviceIcon} source={deviceIcon}/>
                        <View style={styles.nameWrapper}>
                            <Text style={[styles.name,{color: Colors.themeText}]}>{value.dpyDeviceName}</Text>
                            {roomName ? <Text style={[styles.floor,{color: Colors.themeTextLight}]}>{roomName}</Text> : null}
                        </View>
                        {this.isJuniorUser ? null : <TouchableOpacity style={styles.deleteBtn} onPress={()=>{
                            this.state.selectedId = value.id
                            this.deleteDypDevice()
                        }}>
                            <Image style={styles.deleteImg} source={require('../../../images/user_manage/memberDelete.png')}/>
                        </TouchableOpacity>}
                    </View>
                    {this.renderAttributes(value)}
                </View>
            )
        })

        return (
            <ScrollView
                scrollIndicatorInsets={{right: 1}}
                contentContainerStyle={{ paddingBottom: 50 }}
            >
                {deviceList}
            </ScrollView>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.renderDeviceList()}
                {this.renderModal()}
			</View>
		)
	}
}


const styles = StyleSheet.create({
	container: {
		flex: 1
	},
    itemWrapper: {
        marginTop: 20,
        marginBottom: 10,
        paddingHorizontal: 16
    },
    deviceItem:{
        width:'100%',
        paddingLeft:16,
        flexDirection: 'row',
        height: 60,
        alignItems: 'center',
        borderRadius: 5
    },
    deviceAddButton: {
        marginHorizontal: 16,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        marginTop: 20
    },
    deviceAddText:{
        fontSize: 15,
        fontWeight: 'bold',
        color: Colors.newTheme
    },
    deviceIcon:{
        width: 40,
        height:40,
        resizeMode:'contain'
    },
    nameWrapper:{
        marginLeft: 20,
        flex: 1
    },
    name:{
        fontSize: 15,
        fontWeight: 'bold',
    },
    floor:{
        fontSize: 13,
        marginTop: 5,
    },
    deleteBtn:{
        paddingHorizontal: 20,
        height: '100%',
        justifyContent: 'center'
    },
    deleteText:{
        color: Colors.themBgRed,
        fontWeight: 'bold'
    },
    attributeWrapper:{
        paddingHorizontal: 16,
    },
    splitWrapper:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    attriSplit:{
        height: 10,
        width: 2,
    },
    attributeItem:{
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 20
    },
    attriSerial:{
        fontSize: 15,
        fontWeight: 'bold'
    },
    attriName:{
        fontSize: 14,
        fontWeight: 'bold',
        marginLeft: 10
    },
    attriValue:{
        fontSize: 14,
        fontWeight: 'bold',
        marginLeft: 20
    },
    attriAddButton: {
        justifyContent: 'center',
        alignItems: 'center',
        height : 40,
        marginHorizontal: 16,
        marginTop: 10
    },
    attriAddText:{
        fontSize: 14,
        fontWeight: 'bold',
        color: Colors.tabActiveColor
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    deleteImg:{
        width: 24,
        height: 24,
        resizeMode: 'contain'
    },
    moreTouch:{
		borderColor:Colors.borderLightGray,
		borderWidth:1,
		width:120,
		height:30,
		borderRadius:15,
		justifyContent:'center',
		alignItems:'center',
		flexDirection:'row'
	},
	arrow:{
		width:6,
		height:11,
		marginLeft:10,
		resizeMode:'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Setting_Dependency)

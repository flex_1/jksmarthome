/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	TextInput,
	DeviceEventEmitter,
	ImageBackground,
    Switch,
    Modal,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import { Colors,NetUrls } from '../../../common/Constants';
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import ToastManager from "../../../common/CustomComponent/ToastManager";
import HeaderLeft from '..//Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderTitle from '../Navigation/HeaderTitle';

class Setting_ParamsSetting extends Component {

	static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'参数配置'}/>,
            headerBackground: <HeaderBackground/>
        }
	}

	constructor(props) {
		super(props);
		const {getParam, setParams} = this.props.navigation
		this.callBack = getParam('callBack')
		this.deviceData = getParam('deviceData') || {}

		this.state = {
            defaultConfig : null,
            relayConfig : null,
            modelVisible: false,
            pickerData: null,
            pickerTitle: '',
            
            selectType: null,
            currentValue: 0,

            outAirConfigList: null,
            outAirConfigValues: [],
            selectIndex: 0
        }
	}

	componentDidMount() {
        // console.log(this.deviceData);
        if(this.deviceData.attributeTypeName == 'out-air-condition'){
            this.requestOutAirInfo()
        }else{
            this.requestRelayInfo()
        }
	}

	componentWillUnmount(){
		
	}

	// 1P继电器
	async requestRelayInfo(){	
        SpinnerManager.show()	
		try {
			let data = await postJson({
				url: NetUrls.getRelay1pInfo,
				params: {
					deviceId: this.deviceData && this.deviceData.id
				}
			});
            SpinnerManager.close()
			if (data && data.code == 0) {
                let defaultConfig = JSON.parse(data.result.relay1pDefaultConfig)
                let relay1pConfig = JSON.parse(data.result.relay1pConfig)

                let relayConfig = {}
                for (const config of relay1pConfig) {
                    relayConfig[config.type] = parseInt(config.value)
                }

                this.setState({
                    defaultConfig: defaultConfig,
                    relayConfig: relayConfig
                })
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 空调外机
    async requestOutAirInfo(){
        SpinnerManager.show()
        try {
			let data = await postJson({
				url: NetUrls.outAirSettingInfo,
				params: {
					deviceId: this.deviceData && this.deviceData.id
				}
			});
            SpinnerManager.close()
			if (data && data.code == 0) {
                this.setState({
                    outAirConfigList: data.result || {}
                })
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    //请求参数保存
    async requestParamSetting(params, callBack){
        SpinnerManager.show()
        params = params || {}
        try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				params: {
                    ...params,
                    id: this.deviceData && this.deviceData.id,
				}
            });
            SpinnerManager.close()
			if (data && data.code == 0) {
                callBack && callBack()
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 1P 继电器 Picker 确定按钮
    confirmBtnFor1pRelay(data,index){
        this.setState({
            modelVisible: false
        },()=>{
            let selectType = this.state.selectType
            this.requestParamSetting({type: selectType, value: data[0]}, ()=>{
                this.state.relayConfig[selectType] = data[0]
                this.setState({
                    relayConfig: this.state.relayConfig
                })
            })
        })
    }

    // 美的空调 室外机 picker 确定按钮
    confirmBtnForOutAir(data,index){
        this.setState({
            modelVisible: false
        },()=>{
            let selectType = this.state.selectType
            let selectValue = this.state.outAirConfigValues[index[0]]

            let params = {}
            params[selectType] = selectValue

            this.requestParamSetting(params, ()=>{
                let itemData = this.state.outAirConfigList[this.state.selectIndex]
                itemData.currentValue = selectValue
                this.state.outAirConfigList[this.state.selectIndex] = itemData
                this.setState({
                    outAirConfigList: this.state.outAirConfigList
                })
            })
        })
    }

    // 弹出 Modal
    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerData || [],
            pickerTitleText: this.state.pickerTitle,
            selectedValue: [this.state.currentValue],
            onPickerConfirm: (data,index) => {
                if(this.deviceData.attributeTypeName == 'out-air-condition'){
                    this.confirmBtnForOutAir(data,index)
                }else{
                    this.confirmBtnFor1pRelay(data,index)
                }
            },
            onPickerCancel: data => {
                this.setState({
                    modelVisible: false
                }, () => {
                    window.CustomPicker.hide()
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

    //弹出警告框
    showAlert(title, type, tips){
        Alert.alert(
            '提示',
            title,
            [
                { text: '取消', onPress: () => { }, style: 'cancel' },
                { text: '确定', onPress: () => {
                    
                    this.requestParamSetting({type: type, value: 0}, ()=>{
                        ToastManager.show(tips)
                    })
                }},
            ]
        )
    }

    renderModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modelVisible}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {}}
            >
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={styles.modalTouchable} 
                    onPress={() => {
                        this.setState({
                            modelVisible: false
                        }, () => {
                            window.CustomPicker.hide()
                        })
                    }}
                >
                </TouchableOpacity>
            </Modal>
        )
    }

    // 空调外机 配置列表
    renderOutAirList(){
        if(!this.state.outAirConfigList){
            return null
        }
        const Colors = this.props.themeInfo.colors
        let splitLine = <View style={{marginHorizontal: 16,height:1,backgroundColor:Colors.split}}/>

        let configList = this.state.outAirConfigList.map((value, index)=>{
            if(index == this.state.outAirConfigList.length-1){
                splitLine = null
            }
            let currentValue = null
            let currentValueIndex = value.cmdValue.indexOf(value.currentValue)
            if(currentValueIndex != -1){
                currentValue = value.cmdText[currentValueIndex]
            }
            return(
                <View key={'config_index_'+index}>
                    <TouchableOpacity  style={styles.item} onPress={()=>{
                        let pickerData = value.cmdText
                        
                        this.setState({
                            pickerTitle: value.text,
                            pickerData: pickerData,
                            selectType: value.key,
                            outAirConfigValues: value.cmdValue,
                            selectIndex: index,
                            currentValue: currentValue,
                            modelVisible: true
                        })
                    }}>
                        <Text style={[styles.itemTitle,{color: Colors.themeText}]}>{value.text}</Text>
                        <Text style={[styles.itemValue,{color: Colors.themeTextLight}]}>{currentValue}</Text>
                        <Image style={styles.enterImg} source={require('../../../images/enterLight.png')} />
                    </TouchableOpacity>
                    {splitLine}
                </View>
            )
        })
        return configList
    }

    // 1P 继电器 配置列表
	render1pRelayList(){
        if(!this.state.defaultConfig){
            return null
        }

        const Colors = this.props.themeInfo.colors

        let splitLine = <View style={{marginHorizontal: 16,height:1,backgroundColor:Colors.split}}/>

        let configList = this.state.defaultConfig.map((value, index)=>{
            if(index == this.state.defaultConfig.length-1){
                splitLine = null
            }
            let currentValue = this.state.relayConfig[value.type]
            return(
                <View key={'config_index_'+index}>
                    <TouchableOpacity  style={styles.item} onPress={()=>{
                        let pickerData = []
                        for(let i=value.min; i<=value.max; i++){
                            pickerData.push(i)
                        }
                        if(pickerData[pickerData.length-1] != value.max){
                            pickerData.push(value.max)
                        }
                        this.setState({
                            pickerTitle: value.name,
                            pickerData: pickerData,
                            selectType: value.type,
                            currentValue: currentValue,
                            modelVisible: true
                        })
                    }}>
                        <Text style={[styles.itemTitle,{color: Colors.themeText}]}>{value.name}</Text>
                        <Text style={[styles.itemValue,{color: Colors.themeTextLight}]}>{currentValue}{value.unit}</Text>
                        <Image style={styles.enterImg} source={require('../../../images/enterLight.png')} />
                    </TouchableOpacity>
                    {splitLine}
                </View>
            )
        })
        return configList
    }

    renderList(){
        let configList = this.render1pRelayList()

        if(this.deviceData.attributeTypeName == 'out-air-condition'){
            configList = this.renderOutAirList()
        }

        const Colors = this.props.themeInfo.colors
        return(
            <View style={[styles.itemWrapper, {backgroundColor: Colors.themeBg}]}>
                {configList}
			</View>
        )
    }
    
    renderBottomButton(){
        if(this.deviceData.attributeTypeName == 'out-air-condition'){
            return null
        }

        const Colors = this.props.themeInfo.colors

        return(
            <View style={[styles.itemWrapper, {backgroundColor: Colors.themeBg}]}>
                <TouchableOpacity  style={styles.bottomBtn} onPress={()=>{
                    this.showAlert('确定将电量清零?', 8, '指令已下发')
                }}>
                    <Text style={{color: Colors.themeButton,fontSize:15}}>电量清零</Text>
                </TouchableOpacity>
                <View style={{height:1,backgroundColor:Colors.split}}/>
                <TouchableOpacity  style={styles.bottomBtn} onPress={()=>{
                    this.showAlert('确定恢复出厂设置?', 7, '指令已下发')
                }}>
                    <Text style={{color: Colors.red,fontSize:15}}>恢复出厂设置</Text>
                </TouchableOpacity>
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors

		return (
            <ScrollView
                contentContainerStyle={styles.content}
                style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}
            >
				{this.renderList()}
                {this.renderBottomButton()}
                {this.renderModal()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex:1
    },
    content:{
        paddingBottom: 50
    },
	inputWrapper:{
		width: '100%',
		height: 60,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:15
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight:5
	},
	navText:{
		color:Colors.tabActiveColor,
		fontSize:15
    },
    itemWrapper:{
		
		paddingVertical:5,
		borderRadius: 4,
        marginTop:15,
        marginHorizontal: 16
    },
    item:{
		height:50,
		width:'100%',
		flexDirection:'row',
        alignItems:'center',
        paddingHorizontal: 16
    },
    itemTitle:{
        flex: 1,

    },
    itemValue:{
        marginRight: 10
    },
    enterImg:{ 
        width: 8, 
        height: 12,
        resizeMode: 'contain',
        marginRight: 5 
    },
    bottomBtn:{
        height:50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: 'rgba(0,0,0,0.5)' 
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Setting_ParamsSetting)

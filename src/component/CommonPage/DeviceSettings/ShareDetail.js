/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    NativeModules,
    TextInput,
    ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import {Colors, NetUrls, CurrentState} from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import {postJson} from '../../../util/ApiRequest';
import DateUtil from '../../../util/DateUtil';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderTitle from '../Navigation/HeaderTitle';
import moment from 'moment';
// import DatePicker from 'react-native-datepicker';
import DatePicker from '../../../third/Datepicker/datepicker';

//侧滑最大距离
const maxSwipeDistance = 90
//侧滑按钮个数
const countSwiper = 1

const JKUMengShare = Platform.select({
    ios: NativeModules.UMShareModule,
    android: NativeModules.JKUMAndShareUtils
});

class ShareDetail extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeft navigation={navigation}/>,
			headerTitle: <HeaderTitle title={'分享设置'}/>,
            headerBackground: <HeaderBackground/>
		}
	}

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation
        this.callBack = getParam('callBack')
        this.id = getParam('id')

        this.state = {
            beginDate: getParam('beginDate'),
            endDate: getParam('endDate'),
            name: getParam('name')
        }
    }

    componentDidMount() {
        
    }

    componentWillUnmount() {

    }

    //保存设置
    async saveShareSetting() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.shareSetting,
                params: {
                    id: this.id,
                    name: this.state.name,
                    startDate: this.state.beginDate,
                    endDate: this.state.endDate
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.callBack && this.callBack()
                this.props.navigation.pop()
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

   // 日期
	getDateItem(refName,date, callBack) {
        const Colors = this.props.themeInfo.colors

		return (
			<DatePicker
				ref={refName}
				style={{flex:1}}
				date={date}
				mode="date"
				format="YYYY-MM-DD"
				confirmBtnText="确定"
				cancelBtnText="取消"
				showIcon={false}
				androidMode={'spinner'}
				customStyles={{
					dateInput: {
						borderWidth: 0,
						alignItems:'flex-end',
						justifyContent:'center',
						paddingRight:15
					},
					dateText: {
						color: date ? Colors.themeText : Colors.themeBg,
						fontSize: 15,
						textAlign:'right'
					}
				}}
				onDateChange={(date) => {
					callBack(date)
				}}
			/>
		)
	}

    renderNameInput(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={[styles.nameWrapper, {backgroundColor: Colors.themeBg}]}>
                <TextInput
                    defaultValue={this.state.name}
                    style={[styles.input,{color: Colors.themeText}]}
                    placeholder="请输入名称"
                    maxLength={16}
                    clearButtonMode={'while-editing'}
                    placeholderTextColor={Colors.themeInactive}
                    onChangeText={(text) => {
                        this.state.name = text
                    }}
                />
            </View>
        )
    }

    // 日期
	getDatePicker(){
        const Colors = this.props.themeInfo.colors

		return(
			<View style={{paddingHorizontal:16}}>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.inputWrapper,{backgroundColor:Colors.themeBg}]} 
                    onPress={()=>{
					    this.refs.date_picker_begin && this.refs.date_picker_begin.onPressDate()
				    }}
                >
					<Text style={{color:Colors.themeText,fontSize:15}}>生效日期</Text>
					{this.getDateItem('date_picker_begin',this.state.beginDate,(date)=>{
						if(!this.state.endDate){
							this.setState({
								beginDate: date,
							})
							return
						}
						let beginTimeStamp = moment(date)
						let endTimeStamp = moment(this.state.endDate)

						if(beginTimeStamp > endTimeStamp ){
							ToastManager.show('生效时间不能大于结束时间')
							return
						}
						this.setState({
							beginDate: date,
						})
					})}
					<Image style={styles.rightArrow} source={require('../../../images/enter_light.png')} />
				</TouchableOpacity>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.inputWrapper,{backgroundColor:Colors.themeBg}]} 
                    onPress={()=>{
					    this.refs.date_picker_end && this.refs.date_picker_end.onPressDate()
				    }}
                >
					<Text style={{color:Colors.themeText,fontSize:15}}>结束日期</Text>
					{this.getDateItem('date_picker_end',this.state.endDate,(date)=>{
						if(!this.state.beginDate){
							this.setState({
								endDate: date,
							})
							return
						}
                        let beginTimeStamp = moment(this.state.beginDate)
						let endTimeStamp = moment(date)
						
						if(endTimeStamp < beginTimeStamp){
							ToastManager.show('结束时间不能小于生效时间')
							return
						}
						this.setState({
							endDate: date,
						})
					})}
					<Image style={styles.rightArrow} source={require('../../../images/enter_light.png')} />
				</TouchableOpacity>
			</View>
		)
	}

    renderSaveButton(){
        return (
            <TouchableOpacity activeOpacity={0.7} style={styles.sureBtn} onPress={() => {
                this.saveShareSetting()
            }}>
                <Text style={{fontSize: 15, color: Colors.white}}>保存</Text>
            </TouchableOpacity>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors;

        return (
            <ScrollView style={[{backgroundColor: Colors.themeBaseBg}]} keyboardShouldPersistTaps="handled">
                {this.renderNameInput()}
                {this.getDatePicker()}
                {this.renderSaveButton()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    
    nameWrapper:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        borderRadius: 5,
        paddingVertical:4,
        marginHorizontal: 16,
        marginTop: 30
    },
    inputWrapper:{
		width: '100%',
		height: 50,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:30,
		paddingHorizontal: 16
	},
    rightArrow:{ 
        marginLeft: 10, 
        width: 7, 
        height: 13, 
        marginRight: 10,
        resizeMode:'contain' 
    },
    sureBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        marginHorizontal: 20,
        marginTop: 50,
        backgroundColor: Colors.tabActiveColor,
        borderRadius: 4,
    },
    input: {
        flex: 1,
        paddingLeft: 20,
        height:40,
        fontSize: 16
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ShareDetail)

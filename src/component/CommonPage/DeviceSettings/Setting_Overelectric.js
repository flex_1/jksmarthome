/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	TextInput,
	DeviceEventEmitter,
	ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
import { Colors,NetUrls } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '..//Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderRight from '..//Navigation/HeaderRight';
import HeaderTitle from '../Navigation/HeaderTitle';

class Setting_Overelectric extends Component {

	static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text:'保存',onPress:()=>{
                        navigation.getParam('saveBtnClick')()
                    }}
                ]}/>
            ),
            headerTitle: <HeaderTitle title={'过载电流'}/>,
            headerBackground: <HeaderBackground/>
        }
	}

	constructor(props) {
		super(props);
		const {getParam, setParams} = this.props.navigation
		this.callBack = getParam('callBack')
		this.deviceData = getParam('deviceData')

		this.state = {
			current : this.deviceData && this.deviceData.overcurrent,
			floors :[0,1,3,5,6,8,10,12,15,16,20]
        }
        
        setParams({
            saveBtnClick: this.changeOvercurrent.bind(this)
        })
	}

	componentDidMount() {
		
	}

	componentWillUnmount(){
		
	}

	// 设备 设置
	async changeOvercurrent(){
		const { pop } = this.props.navigation			
		try {
			let data = await postJson({
				url: NetUrls.saveDevice,
				params: {
					id: this.deviceData && this.deviceData.id,
					overcurrent: this.state.current
				}
			});
			if (data && data.code == 0) {
				this.callBack && this.callBack()
				pop()
				ToastManager.show('过载电流设置成功')
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

	getCurrentList(){
        const Colors = this.props.themeInfo.colors
        
		let floors = this.state.floors.map((val,index)=>{
			let selected = null
			let max = val + 'A'
			if(this.state.current == val){
				selected = <Image style={{width:18,height:18,marginRight:20}} source={require('../../../images/select.png')}/>
			}
			if(!val){
				max = '不设置过载电流'
			}
			return(
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    key={'floor_list_'+index} 
                    style={[styles.inputWrapper,{backgroundColor:Colors.themeBg}]} 
                    onPress={()=>{
					    this.setState({
						    current : val
					    })
					    this.callBack && this.callBack(val)
			        }}
                >
					<Text style={{fontSize:15,color:Colors.themeText,marginLeft:20,flex:1}}>{max}</Text>
					{selected}
				</TouchableOpacity>
			)
		})
		return (
			<View style={{paddingVertical:5,paddingHorizontal:16}}>
				{floors}
			</View>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
            <ScrollView
                contentContainerStyle={styles.content}
                style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}
            >
				{this.getCurrentList()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex:1
    },
    content:{
        paddingBottom: 50
    },
	inputWrapper:{
		width: '100%',
		height: 60,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:15
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight:5
	},
	navText:{
		color:Colors.tabActiveColor,
		fontSize:15
	},
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Setting_Overelectric)

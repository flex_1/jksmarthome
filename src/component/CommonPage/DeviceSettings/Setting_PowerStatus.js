/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Modal,
	Alert,
    Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import { Colors, NetUrls } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderTitle from '../Navigation/HeaderTitle';


class Setting_PowerStatus extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'通电状态'}/>,
            headerBackground: <HeaderBackground/>,
        }
	}

	constructor(props) {
		super(props);
		const { getParam } = this.props.navigation
		this.deviceData = getParam('deviceData') || {}
        this.deviceId = this.deviceData.id
        this.callBack = getParam('callBack')

		this.state = {
            powerOnStatusValue: getParam('powerOnStatusValue')  // 0 常亮  1  常灭  2  记忆
        }
	}

	componentDidMount() {
		
	}

	componentWillUnmount() {
		
	}

	// 获取依赖列表
	async changeStatus(status) {
        if(this.state.powerOnStatusValue == status){
            return
        }
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.saveDevice,
				params: {
					id: this.deviceId,
                    powerOnStatusValue: status
				}
			})
			SpinnerManager.close()
			if (data && data.code == 0) {
                this.setState({
                    powerOnStatusValue: status
                })
                this.callBack && this.callBack()
				ToastManager.show('设置成功')
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    renderList(){
        const Colors = this.props.themeInfo.colors;

        const selectImg = require('../../../images/select.png')
        const unSelectImg = require('../../../images/connectWifi/unselected.png')

        const img_1 = this.state.powerOnStatusValue == 0 ? selectImg : unSelectImg
        const img_2 = this.state.powerOnStatusValue == 1 ? selectImg : unSelectImg
        const img_3 = this.state.powerOnStatusValue == 2 ? selectImg : unSelectImg

        return(
            <View style={{backgroundColor: Colors.themeBg,marginTop: 5}}>
                <TouchableOpacity activeOpacity={0.7} style={styles.btnTouch} onPress={()=>{
                    this.changeStatus(0)
                }}>
                    <Image style={styles.selecImg} source={img_1}/>
                    <Text style={[styles.title,{color: Colors.themeText}]}>常亮</Text>
                </TouchableOpacity>
                <View style={[styles.split,{backgroundColor: Colors.split}]}/>
                <TouchableOpacity activeOpacity={0.7} style={styles.btnTouch} onPress={()=>{
                    this.changeStatus(1)
                }}>
                    <Image style={styles.selecImg} source={img_2}/>
                    <Text style={[styles.title,{color: Colors.themeText}]}>常灭</Text>
                </TouchableOpacity>
                <View style={[styles.split,{backgroundColor: Colors.split}]}/>
                <TouchableOpacity activeOpacity={0.7} style={styles.btnTouch} onPress={()=>{
                    this.changeStatus(2)
                }}>
                    <Image style={styles.selecImg} source={img_3}/>
                    <Text style={[styles.title,{color: Colors.themeText}]}>记忆</Text>
                </TouchableOpacity>
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors;

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.renderList()}
			</View>
		)
	}
}


const styles = StyleSheet.create({
	container: {
		flex: 1
	},
    btnTouch:{
        
        height: 60,
        borderRadius: 5,
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 20
    },
    selecImg:{
        width: 16,
        height:16,
        resizeMode: 'contain'
    },
    title:{
        fontSize: 16,
        marginLeft: 10
    },
    split:{
        width: '100%',
        height: 1
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Setting_PowerStatus)

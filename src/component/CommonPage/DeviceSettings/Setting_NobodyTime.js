/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	TextInput,
    Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import { Colors, NetUrls } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '..//Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderTitle from '../Navigation/HeaderTitle';
import HeaderRight from '../Navigation/HeaderRight';
import {ImagesLight, ImagesDark} from '../../../common/Themes';


class Setting_NobodyTime extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'设置无人时间'}/>,
            headerBackground: <HeaderBackground/>,
            headerRight: (
				<HeaderRight buttonArrays={[
                    {text:'保存',onPress:()=>{
                        navigation.getParam('saveBtnClick')()
                    }}
                ]}/>
			),
        }
	}

	constructor(props) {
		super(props);
		const { getParam, setParams } = this.props.navigation
		this.callBack = getParam('callBack')
		this.deviceData = getParam('deviceData') || {}

		this.state = {
			time: this.deviceData.noHumanTimeout || 3,
            minTime: 0,
            maxTime: 180
        }

        setParams({
			saveBtnClick: this.saveDevice.bind(this)
		})
	}

	componentDidMount() {
        this.requestHumanTime()
	}

	componentWillUnmount() {
		
	}

	async requestHumanTime() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.getHumanTime,
				params: {
					
				}
			})
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.setState({
                    minTime: data.result?.minTime,
                    maxTime: data.result?.maxTime
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    //保存信息
    async saveDevice(){
        Keyboard.dismiss()
        if(this.state.time == null){
            ToastManager.show('时间不能为空')
            return
        }
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.saveDevice,
                params: {
                    id :this.deviceData.id,
                    noHumanTimeout: this.state.time
                }
            });
            SpinnerManager.close()
            if (data && data.code == 0) {
                this.callBack && this.callBack()
                this.props.navigation.pop()
                ToastManager.show("保存成功")
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error))
        }
    }

    renderTitle(){
        const Colors = this.props.themeInfo.colors

        return(
            <Text style={[styles.title,{color: Colors.themeText}]}>无人移动时间(秒):</Text>
        )
    }

    renderInput(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={styles.timeWrapper}>
                <View style={[styles.inputWrapper,{backgroundColor: Colors.themeBg}]}>
                    <TextInput
                        value={this.state.time.toString()}
                        style={[styles.input,{color: Colors.themeText}]}
                        keyboardType={'number-pad'}
                        underlineColorAndroid={'transparent'}
                        onChangeText={(text) => {
                            let time = text.replace(/[^\d]+/, '')
                            if(time && time != 0){
                                time = parseInt(time)
                                time = Math.max(time, this.state.minTime)
                                time = Math.min(time, this.state.maxTime)
                            }else if(time != 0){
                                time = ''
                            }
                            this.setState({
                                time: time
                            })
                        }}
                    />
                </View>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.btnWrapper,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        this.setState({
                            time : Math.max(this.state.time - 1, this.state.minTime)
                        })
                    }}
                >
                    <Image style={[styles.btnImg,{tintColor: Colors.themeText}]} source={require('../../../images/panel/mus_black.png')}/>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.btnWrapper,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        this.setState({
                            time : Math.min(this.state.time + 1, this.state.maxTime)
                        })
                    }}
                >
                    <Image style={[styles.btnImg,{tintColor: Colors.themeText}]} source={require('../../../images/panel/add_black.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    renderAccountBtns(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={styles.maxminWrapper}>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.minBtnTouch,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        this.setState({
                            time : this.state.minTime
                        })
                    }}
                >
                    <Text style={[{fontSize: 15},{color: Colors.themeText}]}>最小值</Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.minBtnTouch,{backgroundColor: Colors.themeBg,marginLeft: 20}]} 
                    onPress={()=>{
                        this.setState({
                            time : this.state.maxTime
                        })
                    }}
                >
                    <Text style={[{fontSize: 15},{color: Colors.themeText}]}>最大值</Text>
                </TouchableOpacity>
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderTitle()}
				{this.renderInput()}
                {this.renderAccountBtns()}
			</View>
		)
	}
}


const styles = StyleSheet.create({
	container: {
		flex: 1
	},
    title:{
        marginLeft: 20,
        fontSize:18,
        fontWeight:'500',
        marginTop: 20
    },
    timeWrapper:{
        flexDirection:'row',
        justifyContent: 'center',
        marginTop: 30
    },
    maxminWrapper:{
        flexDirection:'row',
        justifyContent: 'center',
        marginTop: 20
    },
	inputWrapper:{
        justifyContent:'center',
        alignItems:'center',
        width: 120,
        height: 50,
        borderWidth: 1,
        borderColor: Colors.borderLightGray,
        borderRadius: 5
    },
    input:{
        width: '100%',
        height: '100%',
        textAlign: 'center',
        fontSize: 18,
        fontWeight: 'bold',
        paddingVertical: 0
    },
    btnWrapper:{
        justifyContent:'center',
        alignItems:'center',
        width: 50,
        height: 50,
        marginLeft: 20,
        borderWidth: 1,
        borderColor: Colors.borderLightGray,
        borderRadius: 5
    },
    btnImg:{
        width:16,
        height:16,
        resizeMode:'contain'
    },
    minBtnTouch:{
        justifyContent:'center',
        alignItems:'center',
        width: 120,
        height: 50,
        borderWidth: 1,
        borderColor: Colors.borderLightGray,
        borderRadius: 5,
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Setting_NobodyTime)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	SwipeableFlatList,
	Alert,
    ScrollView,
    Modal
} from 'react-native';
import { connect } from 'react-redux';
import { Colors,NetUrls } from '../../../common/Constants';
import ToastManager from '../../../common/CustomComponent/ToastManager';
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import {BottomSafeMargin} from '../../../util/ScreenUtil';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderRight from '../Navigation/HeaderRight';
import HeaderTitle from '../Navigation/HeaderTitle';
import DateTimePicker from '@react-native-community/datetimepicker';
import DatePicker from '../../../third/Datepicker/datepicker';

//侧滑最大距离
const maxSwipeDistance = 100
//侧滑按钮个数
const countSwiper = 1

class Setting_Timing_AddTiming extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeft navigation={navigation}/>,
			headerTitle: <HeaderTitle title={navigation.getParam('title')}/>,
			headerRight: (
				<HeaderRight buttonArrays={[
                    {text:'保存',onPress:()=>{
                        navigation.getParam('saveClick')()
                    }}
                ]}/>
			),
            headerBackground: <HeaderBackground/>
		}
	}

	constructor(props) {
		super(props);
		const {getParam,setParams} = this.props.navigation
		this.callBack = getParam('callBack')
		this.sceneId = getParam('sceneId')
		this.deviceId = getParam('deviceId')
		this.timerId = getParam('timerId')
		this.selects = getParam('selects')
		this.uuid = getParam('uuid')
		this.status = getParam('status')
		this.operation = getParam('operation')

		this.state = {
			days:[
				{id:0,name:'单',value:'0'},
				{id:1,name:'一',value:'2'},
				{id:2,name:'二',value:'3'},
				{id:3,name:'三',value:'4'},
				{id:4,name:'四',value:'5'},
				{id:5,name:'五',value:'6'},
				{id:6,name:'六',value:'7'},
				{id:7,name:'日',value:'1'},
			],
			chosenDate: getParam('currentDate') || new Date(),
			selects: this.getDefaultSelects(),
			operation: this.operation == null ? 1 : this.operation,

            conditionList: [],
            modalVisible: false,
            pickerData: [],
            currentValueArr: [],
            pickerTitle: '',
            selectData: [],
            deviceStatus: getParam('deviceStatus') || {},
            currentKey: ''
        }
        
        setParams({
            saveClick: this.save.bind(this)
        })
	}

	//处理默认的选中 周期
	getDefaultSelects(){
		if(!this.selects){
			return ['0']
		}
		let selectArr = []
		selectArr = this.selects.split(',')
		if(Array.isArray(selectArr)){
			console.log( selectArr );
			
			return selectArr
		}else{
			return []
		}
	}

    componentDidMount(){
        if(this.deviceId){
            this.requestConditionList()
        }
    }
 
	// 存储 定时
	async requestSaveTimer(hour,minute,cyclic){
		const {pop} = this.props.navigation
		let params = {}
		if(this.sceneId){
			params.sceneId = this.sceneId
		}
		if(this.deviceId){
			params.deviceId = this.deviceId
		}
		if(this.timerId){
			params.id = this.timerId
		}
		if(this.uuid){
			params.uuid = this.uuid
		}
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.addTimer,
				params: {
					...params,
					hour: hour,
					minute: minute,
					cyclic: cyclic,
					status: this.status,
					operation: this.sceneId ? 1 : null,
                    deviceStatus: this.deviceId ? JSON.stringify(this.state.deviceStatus) : null
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.callBack && this.callBack()
				pop()
			}
            ToastManager.show(data.msg)
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	//删除定时器
	async deleteTimer(){
		const {pop} = this.props.navigation
		let params = {}
		if(this.sceneId){
			params.sceneId = this.sceneId
		}
		if(this.deviceId){
			params.deviceId = this.deviceId
		}
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.delTimer,
				params: {
					...params,
					id : this.timerId,
					uuid: this.uuid,
					cyclic : this.selects.toString()
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.callBack && this.callBack()
				pop()
				ToastManager.show('该定时器已删除')
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    // 获取操作选项
    async requestConditionList(){
        SpinnerManager.show();
        try {
            let data = await postJson({
                url: NetUrls.getExecuteByDeviceId,
                params: {
                    deviceId: this.deviceId,
                    type: 2,   // 2-设备定时
                }
            });
            SpinnerManager.close();
            if (data.code == 0) {
                if (data.result && data.result.length>0) {
                    this.setState({
                        conditionList: data.result || []
                    })
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close();
            ToastManager.show('网络错误')
        }
    }

	save(){
		if(this.state.selects.length <= 0){
			ToastManager.show('请选择定时器周期')
			return
		}
        if(this.deviceId){
            if(!this.state.deviceStatus || JSON.stringify(this.state.deviceStatus) == '{}'){
                ToastManager.show('至少添加一个属性')
			    return
            }
        }
		let selected = this.state.selects.concat()
		selected = selected.sort()

		let timeStr = this.state.chosenDate
		let houser = null
		let minute = null
		try {
			if(typeof timeStr == 'object'){
				houser = timeStr.getHours()
				minute = timeStr.getMinutes()
			}else if(typeof timeStr == 'string'){
				houser = timeStr.split(':') && timeStr.split(':')[0]
				minute = timeStr.split(':') && timeStr.split(':')[1]
			}
			if((houser || houser == 0) && (minute || minute == 0)){
				this.requestSaveTimer( parseInt(houser),parseInt(minute),selected.toString() )
			}else{
				ToastManager.show('时间格式有误')
			}
		} catch (error) {
			ToastManager.show('时间格式有误')
		}
	}

    handlePickerSelectData(data, index){
        let selectValue = this.state.currentValueArr[index[0]]
        this.state.deviceStatus[this.state.currentKey] = selectValue
        this.setState({
            deviceStatus: this.state.deviceStatus
        })
    }

    clearExe(rowData, index){
        let deviceStatus = JSON.parse(JSON.stringify(this.state.deviceStatus))
        if(deviceStatus[rowData.executeKey]){
            delete deviceStatus[rowData.executeKey]
            this.setState({
                deviceStatus: deviceStatus
            })
        }
    }

    //关闭侧滑栏
	closeSwiper() {
        this.swiperFlatlist && this.swiperFlatlist.setState({ openRowKey: null })
    }

    _extraConditionUniqueKey(item, index) {
        return "condition_index_" + index;
    }

    getQuickActions(rowData, index){
        return (
			<View style={styles.quickAContent}>
				<TouchableOpacity activeOpacity={0.7} onPress={() => {
					this.closeSwiper()
					this.clearExe(rowData, index)
				}}>
					<View style={[styles.quick, { backgroundColor: Colors.themBgRed }]}>
						<Text style={{ color: Colors.white, fontSize: 14 }}>清空</Text>
					</View>
				</TouchableOpacity>
			</View>
		)
    }

    renderRowItem(rowData, index){
        const Colors = this.props.themeInfo.colors;
        let conditionValue = this.state.deviceStatus[rowData.executeKey]
        let valueText = ''
        for (const data of rowData.value) {
            if(conditionValue == data.id){
                valueText = data.text
            }
        }
        return(
            <TouchableOpacity activeOpacity={1} style={[styles.item,{backgroundColor: Colors.themeBg}]} onPress={()=>{
                this.closeSwiper()
                let pickerData = []
                let currentValueArr = []
                for (const data of rowData.value) {
                    pickerData.push(data.text)
                    currentValueArr.push(data.id)
                }
                this.setState({
                    modalVisible: true,
                    pickerData: pickerData,
                    currentValueArr: currentValueArr,
                    currentKey: rowData.executeKey,
                    pickerTitle: rowData.text,
                    selectData: [valueText]
                })
            }}>
				<Text style={[styles.title,{color: Colors.themeText}]}>{rowData.text}</Text>
				{valueText ? <Text style={[styles.extra,{color: Colors.themeTextLight}]}>{valueText}</Text> : null}
                <Image style={styles.rightArrow} source={require('../../../images/enterLight.png')}/>
                <View style={[styles.itemLine,{backgroundColor: Colors.themeBaseBg}]}/>
			</TouchableOpacity>
        )
    }

    getOprationList(){
        if(this.sceneId){
            return null
        }
        const Colors = this.props.themeInfo.colors;

        return(
            <View style={[styles.itemWrapper,{backgroundColor:Colors.themeBg}]}>
                <SwipeableFlatList
                    ref={e => this.swiperFlatlist = e}
                    data={this.state.conditionList}
                    keyExtractor={this._extraConditionUniqueKey}
                    renderItem={({item,index}) => this.renderRowItem(item,index)}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator = {false}
                    renderQuickActions={({ item, index }) => this.getQuickActions(item, index)}
                    maxSwipeDistance={maxSwipeDistance}
				    bounceFirstRowOnMount={false}
                />
            </View>
        )
    }

	getOperationSelect(){
        if(this.sceneId){
            return null
        }
        const Colors = this.props.themeInfo.colors;

        let list = this.state.conditionList.map((value, index)=>{
            
        })
        
		return(
			<View style={[styles.itemWrapper,{backgroundColor:Colors.themeBg}]}>
				{list}
			</View>
		)
	}

	getIndicatorHead(){
		const Colors = this.props.themeInfo.colors;

		let list = this.state.days.map((val,index)=>{
			let i = this.state.selects.indexOf(val.value)
			let selectBg = i == -1 ? null : {backgroundColor: Colors.themeButton}
            let selectColor = i == -1 ? null : {color:Colors.white}
			return(
                <TouchableOpacity
                    style={[styles.dayTouch, {backgroundColor:Colors.themeBg}, selectBg]}
                    key={'timing_index_'+index} 
                    onPress={()=>{
                        if(index == 0 && i==-1){
                            this.state.selects = [val.value]
                            this.setState({
                                selects: this.state.selects
                            })
                            return
                        }
                        if(i == -1){
                            let todayIndex = this.state.selects.indexOf('0')
                            if(todayIndex != -1){
                                this.state.selects.splice(todayIndex,1)
                            }
                            this.state.selects.push(val.value)
                        }else{
                            this.state.selects.splice(i,1)
                        }
                        this.setState({
                            selects: this.state.selects
                        })
				    }}
                >
					<Text style={[{color:Colors.themeTextLight,fontSize:15},selectColor]}>{val.name}</Text>
				</TouchableOpacity>
			)
		})

		return(
			<View style={styles.dateBtnWrapper}>
				{list}
			</View>
		)
	}

	getDateTimeItem() {
		const Colors = this.props.themeInfo.colors;
		return (
			<DatePicker
				ref={e => this.timer_date_picker = e}
				style={{flex:1 }}
				date={this.state.chosenDate}
				mode="time"
				format="HH:mm"
				confirmBtnText="确定"
				cancelBtnText="取消"
				showIcon={false}
				androidMode={'spinner'}
				customStyles={{
					dateInput: {
						borderWidth: 0,
						alignItems:'flex-end',
						justifyContent:'center',
						paddingRight:15
					},
					dateText: {
						color: Colors.themeTextLight,
						fontSize: 15,
                        fontWeight: '400',
						textAlign:'right'
					}
				}}
				onDateChange={(time) => {
					this.setState({
						chosenDate: time
					})
				}}
			/>
		)
	}

	getAndroidPicker(){
		if(Platform.OS == 'ios'){
			return null
		}
		const Colors = this.props.themeInfo.colors;
		return(
			<View style={{paddingVertical:5,paddingHorizontal:16,marginTop:30}}>
				<TouchableOpacity activeOpacity={0.7} style={[styles.inputWrapper, {backgroundColor:Colors.themeBg}]} onPress={()=>{
					this.timer_date_picker?.onPressDate()
				}}>
					<Text style={[styles.title,{color:Colors.themeText}]}>选择时间</Text>
					{this.getDateTimeItem()}
					<Image style={styles.rightArrow} source={require('../../../images/enterLight.png')} />
				</TouchableOpacity>
			</View>
		)
	}

	getDatePicker(){
		if(Platform.OS == 'android'){
			return null
        }
        const Colors = this.props.themeInfo.colors;
		return(
			<View style={{marginTop:30,marginHorizontal: 16,borderRadius: 5,backgroundColor: Colors.white}}>
				<DateTimePicker
                    display='spinner'
                    textColor={'#fff'}
					value={this.state.chosenDate}
					mode={'time'}
					onChange={(event, date)=>{
						this.setState({
							chosenDate: date
						})
					}}
				/>
			</View>
		)
	}

	getBottomBtn() {
		const Colors = this.props.themeInfo.colors;

		if (this.timerId) {
			return (
				<View style={styles.bottomBtnWrapper}>
                    <TouchableOpacity 
                        activeOpacity={0.7} 
                        style={[styles.deleteTouch, {backgroundColor: Colors.red}]} 
                        onPress={() => {
                            Alert.alert(
                                '提示',
                                '确认删除该定时器?',
                                [
                                    {text: '取消', onPress: () => {}, style: 'cancel'},
                                    {text: '删除', onPress: () => { this.deleteTimer() }},
                                ]
                            )
					    }}
                    >
						<Text style={{ color: Colors.white }}>删除</Text>
					</TouchableOpacity>
				</View>
			)
		}
	}

    getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.CustomPicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerData,
            pickerTitleText: this.state.pickerTitle,
            selectedValue: this.state.selectData,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false,
                    selectData: data
                })
                this.handlePickerSelectData(data,index);
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                }, () => {
                    window.CustomPicker.hide()
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

	render() {
		const Colors = this.props.themeInfo.colors;

		return (
            <View style={{backgroundColor: Colors.themeBaseBg,flex:1}}>
                <ScrollView contentContainerStyle={styles.scrollContent}>
				    {this.getOprationList()}
				    {this.getIndicatorHead()}
				    {this.getDatePicker()}
				    {this.getAndroidPicker()}
                </ScrollView>
				{this.getBottomBtn()}
                {this.getModal()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	scrollContent:{
		paddingBottom: BottomSafeMargin + 60,
	},
	inputWrapper:{
		width: '100%',
		height: 50,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:15,
		paddingHorizontal: 16
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
        marginRight:5
	},
	dayTouch:{
		width:32,
		height:32,
		borderRadius:4,
		justifyContent:'center',
		alignItems:'center'
	},
	bottomBtnWrapper: {
		position: 'absolute',
		left: 0,
		bottom: BottomSafeMargin,
		width: '100%',
		paddingHorizontal: 16,
		paddingVertical: 10
	},
	deleteTouch: {
		width: '100%',
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 4
	},
	operationWrapper:{
		width:'50%',
		height:30,
		justifyContent:'center',
		alignItems:'center',
    },
    rightArrow:{ 
        marginLeft: 10, 
        width: 7, 
        height: 13, 
        marginRight: 10,
        resizeMode:'contain' 
    },
    dateBtnWrapper: {
        paddingHorizontal:16,
        marginTop:20,
        flexDirection:'row',
        justifyContent:'space-between'
    },
    title:{
        fontSize: 16,
        fontWeight: '500',
        flex: 1
    },
    item:{
        height: 50,
        alignItems:'center',
        flexDirection: 'row',
        paddingHorizontal: 16
    },
    itemWrapper:{
        marginHorizontal:16,
        marginTop:10,
        borderRadius: 5
    },
    itemLine:{
        position: 'absolute',
        width: '100%',
        bottom: 0,
        height: 1
    },
    extra:{
        fontSize: 15,
        fontWeight: '400'
    },
    modalTouchable:{
        width: '100%',
        height: '100%',
        backgroundColor: Colors.translucent
    },
    quickAContent: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginRight: 0,
		marginTop: 0,
		overflow: 'hidden',
	},
	quick: {
		backgroundColor: Colors.white,
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
        width: maxSwipeDistance / countSwiper,
	},
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Setting_Timing_AddTiming)

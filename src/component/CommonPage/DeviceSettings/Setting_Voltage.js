/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	TextInput,
	DeviceEventEmitter,
	ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
import { Colors, NetUrls } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import HeaderLeft from '..//Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderTitle from '../Navigation/HeaderTitle';
import {ImagesLight, ImagesDark} from '../../../common/Themes';


class Setting_Voltage extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'过压/欠压'}/>,
            headerBackground: <HeaderBackground/>,
        }
	}

	constructor(props) {
		super(props);
		const { getParam, setParams } = this.props.navigation
		this.callBack = getParam('callBack')
		this.deviceData = getParam('deviceData')

		this.state = {
			switch: this.deviceData && this.deviceData.voltageStatus,
			currentMin: (this.deviceData && this.deviceData.minvoltage) || 0,
			currentMax: (this.deviceData && this.deviceData.maxvoltage) || 0,
			min: this.deviceData && this.deviceData.minvoltage || 0,
			max: this.deviceData && this.deviceData.maxvoltage || 0
        }
	}

	componentDidMount() {
		
	}

	componentWillUnmount() {
		
	}

	// 修改 过压/欠压 值
	async requestSaveVoltage() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.voltage,
				params: {
					deviceid: this.deviceData && this.deviceData.id,
					status: this.state.switch,
					minvoltage: this.state.min,
					maxvoltage: this.state.max
				}
			})
			SpinnerManager.close()
			if (data && data.code == 0) {
				const { pop } = this.props.navigation
				ToastManager.show('设置成功')
				this.callBack && this.callBack()
				pop()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	renderMainView() {
        const Colors = this.props.themeInfo.colors
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight;
        let switchIcon = this.state.switch == 1 ? Images.switchOn : Images.switchOff
        let currentMin = this.state.currentMin
        let currentMax = this.state.currentMax

		return (
			<View style={{ paddingVertical: 5, paddingHorizontal: 16 }}>
				<View style={[styles.inputWrapper,{backgroundColor: Colors.themeBg}]}>
                    <View style={{flex: 1,paddingLeft: 16}}>
                        <Text style={{ fontSize: 15, color: Colors.themeText }}>过压/欠压时关闭该电路电源</Text>
                        <Text style={{ fontSize: 13, marginTop: 10,color: Colors.themeTextLight }}>当前设定 :
						    <Text style={{ color: Colors.themeBG, marginLeft: 10 }}>  {currentMin}-{currentMax}(V)</Text>
					    </Text>
                    </View>
                    <TouchableOpacity style={styles.switchTouch} onPress={()=>{
                        this.setState({
                            switch: this.state.switch ? 0 : 1
                        })
                    }}>
                        <Image style={styles.switchIcon} source={switchIcon}/>
                    </TouchableOpacity>
				</View>
				<View style={[styles.bottomWrapper,{backgroundColor: Colors.themeBg}]}>
                    <View style={{flexDirection:'row',paddingHorizontal: 10}}>
                        <View style={styles.voltWrapper}>
                            <Text style={[styles.voltText,{color: Colors.themeText}]}>
                                欠压： <Text style={{fontWeight: 'bold',fontSize: 16}}>{this.state.min}</Text> V
                            </Text>
                        </View>
                        <View style={styles.voltWrapper}>
                            <Text style={[styles.voltText,{color: Colors.themeText}]}>
                                欠压： <Text style={{fontWeight: 'bold',fontSize: 16}}>{this.state.max}</Text> V
                            </Text>
                        </View>
                    </View>
					<View style={styles.sliderWrapper}>
						<MultiSlider
							values={[this.state.min, this.state.max]}
							min={0}
							max={280}
							onValuesChange={(values) => {
								this.setState({
									min: values[0],
									max: values[1]
								})
							}}
							onValuesChangeFinish={(values) => {
								this.setState({
									min: values[0],
									max: values[1]
								})
							}}
							pressedMarkerStyle={{}}
							isMarkersSeparated={true}
							selectedStyle={{
								backgroundColor: '#0FA298',
								height: 20,
							}}
							unselectedStyle={{
								backgroundColor: '#80DDD7',
								height: 20,
							}}
							customMarkerLeft={CustomMarkerLeft}
							customMarkerRight={CustomMarkerRight}
						/>
					</View>
				</View>
				<View style={{ marginTop: 35, alignItems: 'center' }}>
					<TouchableOpacity activeOpacity={0.9} style={styles.resetTouch} onPress={() => {
                        this.requestSaveVoltage()
					}}>
						<Text style={{ color: Colors.white }}>保存</Text>
					</TouchableOpacity>
				</View>
			</View>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.renderMainView()}
			</View>
		)
	}
}

class CustomMarkerLeft extends React.Component {
	render() {
		return (
			<View style={{ alignItems: 'center', marginBottom: 32 }}>
				<Text style={{color:'#80DDD7'}}>{this.props.currentValue}</Text>
				<Image style={styles.maker}
					source={this.props.pressed ?
                        require('../../../images/settingIcon/maker_left.png') 
                        : require('../../../images/settingIcon/maker_left.png')
					}
				/>
			</View>
		)
	}
}

class CustomMarkerRight extends React.Component {
	render() {
		return (
			<View style={{ alignItems: 'center', marginBottom: 32 }}>
				<Text style={{color:'#2CC7BC'}}>{this.props.currentValue}</Text>
				<Image style={styles.maker}
					source={this.props.pressed ?
                        require('../../../images/settingIcon/maker_right.png') 
                        : require('../../../images/settingIcon/maker_right.png')
					}
				/>
			</View>
		);
	}
}


const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	inputWrapper: {
		width: '100%',
        height: 80,
		backgroundColor: Colors.white,
		borderRadius: 5,
		alignItems: 'center',
		flexDirection: 'row',
		marginTop: 15
	},
	bottomWrapper: {
		width: '100%',
		backgroundColor: Colors.white,
		borderRadius: 5,
		marginTop: 15,
        padding: 15,
        paddingBottom: 30
    },
    sliderWrapper:{ 
        width: '100%', 
        marginTop: 60, 
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center' 
    },
	resetTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		width: '90%',
		height: 40,
		backgroundColor: Colors.tabActiveColor,
		borderRadius: 4
	},
	maker: {
		width: 25,
		height: 15,
		backgroundColor: 'transparent',
		resizeMode: 'contain'
    },
    switchTouch:{
        height: '100%',
        paddingHorizontal: 16,
        justifyContent:'center',
        alignItems: 'center'
    },
    switchIcon:{
        width:50,
        height:30,
        resizeMode:'contain'
    },
    voltWrapper:{
        flex: 1,
        justifyContent:'center',
        alignItems:'center',
        paddingVertical: 10
    },
    voltText:{
        fontSize: 14
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Setting_Voltage)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
    Modal
} from 'react-native';
import { connect } from 'react-redux';
import { Colors,NetUrls } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from '../../../common/CustomComponent/SpinnerManager';
import { postJson } from '../../../util/ApiRequest';
import DateUtil from '../../../util/DateUtil';
import {ListNoContent,FooterEnd,FooterLoading } from "../../../common/CustomComponent/ListLoading";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderTitle from '../Navigation/HeaderTitle';
import HeaderRight from '../Navigation/HeaderRight';
import {ImagesLight, ImagesDark} from '../../../common/Themes';
import { TouchableOpacity } from 'react-native';

class Setting_Log extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeft navigation={navigation}/>,
			headerTitle: <HeaderTitle title={'日志'}/>,
            // headerRight: (
            //     <HeaderRight buttonArrays={[
            //         navigation.getParam('showSetting') ? {text: '筛选', onPress:()=>{
            //             navigation.getParam('filterBtn')()
            //         }} : null
            //     ]}/>
            // ),
            headerBackground: <HeaderBackground/>
		}
	}

	constructor(props) {
		super(props);
		const { getParam,setParams } = this.props.navigation
		this.deviceId = getParam('deviceId')
		this.sceneId = getParam('sceneId')
        this.attributeType = getParam('attributeType')
        this.isZigbeePanel = this.attributeType == 'zigbee-panel'

		this.totalPages = 0
		this.state = {
			logs: null,
			loading: false,
			currentPage: 0,

            pickerData: null,
            ledList: null,
            modalVisible: false,
            selectValue: []
		}

        // if(this.isZigbeePanel){
        //     setParams({
        //         showSetting: true,
        //         filterBtn: this.filterBtnClcik.bind(this)
        //     })
        // }
	}

	componentDidMount() {
		this.requestLog()
	}

	componentWillUnmount() {

	}

	async requestLog(page, selectId){
		if(this.state.loading){
			return
		}
		if( page && page>this.totalPages){
			return
		}
		let params = {}
		if(page){
			params.page = page
		}
        if(selectId){
            params.type = 1
            params.id = selectId
        }else if(this.deviceId){
			params.type = 1
			params.id = this.deviceId
		}else if(this.sceneId){
			params.type = 2
			params.id = this.sceneId
		}
		this.setState({loading: true})
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.operationlog,
				params: params
			});
			this.setState({loading: false})
            SpinnerManager.close()
			if (data && data.code == 0) {
				this.totalPages = data.result && data.result.totalPageNum
				let logs = []
				if(this.state.logs){
					let currentLogs = JSON.parse(JSON.stringify(this.state.logs))
					let newsLogs = (data.result && data.result.list) || []
					logs = currentLogs.concat(newsLogs)
				}else{
					logs = (data.result && data.result.list) || []
				}
				this.setState({
					logs: logs,
					currentPage: data.result && data.result.curPage
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({loading: false})
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    // 获取按键列表
    async requestZigbeeButtonList(){
        SpinnerManager.show()
        try {
			let data = await postJson({
				url: NetUrls.getCurrentZigbeePanelButtonList,
				params: {
                    id: this.deviceId
                }
			});
            SpinnerManager.close()
			if (data.code == 0) {
                let pickerData = []
                for (const led of data.result?.ledList) {
                    pickerData.push(led.name)
                }
				this.setState({
                    ledList: data.result?.ledList,
                    pickerData: pickerData,
                    modalVisible: true
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    filterBtnClcik(){
        if(!this.state.pickerData){
            this.requestZigbeeButtonList()
        }else{
            this.setState({
                modalVisible: true
            })
        }
    }

    // 弹出 Modal
    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerData,
            pickerTitleText: '选择按键',
            selectedValue: this.state.selectValue,
            onPickerConfirm: (data,index) => {
                const i = index[0]
                const s_id = this.state.ledList[i]?.id
                this.setState({
                    logs: null,
                    currentPage: 0,
                    modalVisible: false,
                    selectValue: data
                },()=>{
                    this.requestLog(0, s_id)
                })
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                }, () => {
                    window.CustomPicker.hide()
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

    renderModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={styles.modalTouchable} 
                    onPress={() => {
                        this.setState({
                            modalVisible: false
                        }, () => {
                            window.CustomPicker.hide()
                        })
                    }}
                >
                </TouchableOpacity>
            </Modal>
        )
    }

	renderRowItem(rowData, index){
		const Colors = this.props.themeInfo.colors;
		const bgColor = index % 2 == 0 ? Colors.themeBaseBg : Colors.themeBg
		const time = DateUtil(rowData.createTime,'MM-dd hh:mm:ss')
		const timeArr = time.split(' ')
		const timeStr = timeArr[0] + '\r\n' + timeArr[1]
        const operator = rowData.creator || '无'

		return(
			<TouchableOpacity activeOpacity={0.7} style={{height: 44,flexDirection: 'row',borderBottomWidth:1,borderBottomColor: Colors.split,backgroundColor: bgColor}} onPress={()=>{
                this.props.navigation.navigate('LogDetail',{
                    title: '日志详情',
                    content: rowData.memo,
                    time: rowData.createTime,
                    type: rowData.type,
                    operator: operator
                })
            }}>
				<View style={styles.itemWrapper}>
					<Text numberOfLines={2} style={[styles.valueText,{color: Colors.themeTextLight}]}>{rowData.type}</Text>
				</View>
				<View style={styles.itemWrapper}>
					<Text numberOfLines={2} style={[styles.valueText,{color: Colors.themeTextLight}]}>{rowData.memo}</Text>
				</View>
				<View style={styles.itemWrapper}>
					<Text numberOfLines={2} style={[styles.valueText,{color: Colors.themeTextLight}]}>{timeStr}</Text>
				</View>
                <View style={styles.itemWrapper}>
					<Text numberOfLines={2} style={[styles.valueText,{color: Colors.themeTextLight}]}>{operator}</Text>
				</View>
			</TouchableOpacity>
		)
	}

	_keyExtractor(item, index) {
		return "logs_index_" + index;
	}

	getLogText(){
		const Colors = this.props.themeInfo.colors;

		if(!this.state.logs){
			return(
				<View style={{alignItems:'center',marginTop:'50%',flex: 1,}}>
					<Text style={{color:Colors.lightGray,fontSize:13}}>加载中...</Text>
				</View>
			)
		}
		if(this.state.logs.length <= 0){
            const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
			return(
				<ListNoContent
					img={Images.noLog} 
					text={'暂无日志'}
				/>
			)
		}
		let Footer = FooterLoading
        if(this.state.currentPage >= this.totalPages && !this.state.loading){
            Footer = FooterEnd
		}
		
        return(
            <FlatList
				removeClippedSubviews={false}
				contentContainerStyle={{ paddingBottom: 50}}
                data={this.state.logs}
				initialNumToRender={10}
				scrollIndicatorInsets={{right: 1}}
                keyExtractor={this._keyExtractor}
                renderItem={({item, index}) => this.renderRowItem(item, index)}
                onEndReached={()=>{this.requestLog(this.state.currentPage+1)}}
                onEndReachedThreshold={0.1}
                ListFooterComponent={Footer}
            /> 
        )
	}

    renderFilterHeader(){
        if(!this.isZigbeePanel){
            return null
        }
        const Colors = this.props.themeInfo.colors;
        let title = '全部按键'
        if(this.state.selectValue.length){
            title = this.state.selectValue.toString()
        }
        return(
            <TouchableOpacity activeOpacity={0.7} style={[styles.filterHead,{backgroundColor: Colors.themeBg}]} onPress={()=>{
                this.filterBtnClcik()
            }}>
                <Text style={[styles.filterTitle,{color: Colors.themeText}]}>{title}</Text>
                <Image style={[styles.arrowDown,{tintColor: Colors.themeTextLight}]} source={require('../../../images/down.png')}/>
            </TouchableOpacity>
        )
    }

	renderHeader(){
		const Colors = this.props.themeInfo.colors;

		return(
			<View style={[styles.headerWrapper,{backgroundColor: Colors.themeBg}]}>
				<View style={styles.titleItemWrapper}>
					<Text style={[styles.topTitle,{color: Colors.themeText}]}>指令来源</Text>
				</View>
				<View style={styles.titleItemWrapper}>
					<Text style={[styles.topTitle,{color: Colors.themeText}]}>指令值</Text>
				</View>
				<View style={styles.titleItemWrapper}>
					<Text style={[styles.topTitle,{color: Colors.themeText}]}>触发时间</Text>
				</View>
                <View style={styles.titleItemWrapper}>
					<Text style={[styles.topTitle,{color: Colors.themeText}]}>操作人</Text>
				</View>
			</View>
		)
	}
	
	render() {
		const Colors = this.props.themeInfo.colors;

		return (
			<View style={{backgroundColor: Colors.themeBaseBg,flex: 1}}>
                {this.renderFilterHeader()}
				{this.renderHeader()}
				{this.getLogText()}
                {this.renderModal()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
    filterHead:{
        marginHorizontal: 16, 
        height: 40, 
        borderRadius: 20, 
        marginTop: 10,
        marginBottom: 5,
        paddingHorizontal: 25,
        flexDirection: 'row',
        alignItems: 'center'
    },
    arrowDown:{
        width: 14,
        height: 7,
        resizeMode: 'contain' 
    },
    filterTitle:{
        fontSize: 15,
        fontWeight: '400',
        flex: 1
    },
    line:{
        flex: 1,
        width: 1,
        marginLeft: 3
    },
    lineDot:{
        position: 'absolute',
        width: 8, 
        height: 8, 
        borderRadius: 4,
        left:0 , 
        top: '50%',
    },
	titleItemWrapper:{
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	itemWrapper:{
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		paddingHorizontal: 10
	},
	topTitle:{
		fontSize: 16,
		fontWeight: '400'
	},
	headerWrapper:{
		marginTop:8, 
		height:48, 
		flexDirection:'row'
	},
	valueText:{
		fontSize: 13,
		fontWeight: '400',
		textAlign: 'center',
		lineHeight: 16
	},
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Setting_Log)

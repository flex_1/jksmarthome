/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    Modal,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import {Colors, NetUrls} from '../../../common/Constants';
import {postJson} from "../../../util/ApiRequest";
import {ChangeHourMinutestr} from "../../../util/DateUtil";
import ToastManager from "../../../common/CustomComponent/ToastManager";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderRight from '../Navigation/HeaderRight';
import HeaderTitle from '../Navigation/HeaderTitle';

class Setting_Maxopenning extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeft navigation={navigation}/>,
			headerTitle: <HeaderTitle title={'最长开启'}/>,
			headerRight: (
				<HeaderRight buttonArrays={[
                    {text:'保存',onPress:()=>{
                        navigation.getParam('saveBtnClick')()
                    }}
                ]}/>
			),
            headerBackground: <HeaderBackground/>
		}
	}

	constructor(props) {
		super(props);
		const {getParam,setParams} = this.props.navigation
		this.callBack = getParam('callBack')
        this.deviceData = getParam('deviceData')
        this.longstopenText = getParam('longstopenText')

		this.state = {
            modalVisible: false,
			currentFloor : 0,
			floors :[0,1,3,5,10,15],

            data: null,
            pickerTitle: '选择时间',
			selectValue:null,
			selectedValue:0,
            selectData: []
		}

		setParams({
			saveBtnClick: ()=>{
				this.saveDevice()
			}
		})
	}

	componentDidMount() {
        this.initTimeData();        
        this.checkDefault();
	}

	componentWillUnmount(){
        
	}

    //保存信息
    async saveDevice(){
        const {pop} = this.props.navigation
        try {
            let data = await postJson({
                url: NetUrls.saveDevice,
                params: {
                    id :this.deviceData.id,
                    longestopen:this.state.selectedValue
                }
            });
            if (data && data.code == 0) {
                this.callBack && this.callBack()
                pop()
                ToastManager.show("保存成功")
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show(JSON.stringify(error))
        }
    }

	initTimeData(){
        let result = [];
        for (let j=0;j<=10;j++) {
            let item = {}
            item.text = j+'小时'
            let temp = {}
            let arrs = []
            for (let i=0;i<=59;i++) {
                if(j==0 && i==0){

                }else{
                    let c = i+'分钟'
                    arrs.push(c)
                }
            }
            temp[item.text] = arrs;
            result.push(temp)
        }
        this.setState({
            data: result
        })
	}

    checkDefault(){
	    let flag = false;
        let value = this.deviceData.longestopen;
        for(let i=0;i<this.state.floors.length;i++){
            if(value == this.state.floors[i]){
                flag = true;
                this.setState({
                    currentFloor: value
                })
            }
        }
        if(!flag){
            if(value > 0){
                // let v = ChangeHourMinutestr(value);
                this.setState({
                    currentFloor:-1,
                    selectValue: this.longstopenText,
                    selectedValue:value
                })
            }
        }
    }
    
    getModal() {
        if(!this.state.data){
            return null
        }
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        window.DoubulePicker.init({
            pickerData: this.state.data,
            pickerConfirmBtnText: '确定',
            pickerCancelBtnText: '取消',
            pickerTitleText: this.state.pickerTitle,
            pickerCancelBtnColor: [38, 80, 215, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: this.state.selectData,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false,
                    selectData: data
                })
                this.handlePickerSelectData(data,index);

            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })
            },
            onPickerSelect: data => {

            }
        });
        window.DoubulePicker.show()
    }

    // Picker 确定按钮 点击处理
    handlePickerSelectData(selectData,selectIndex){
        let selectH = selectIndex[0]>0 ? selectIndex[0]+"小时" : '';
        let selectM = selectIndex[1]>0 ? selectIndex[1]+"分钟" : '';
        let time = selectIndex[0]*60+(selectIndex[1]);

        if(selectIndex[0] == 0){
            selectM = selectIndex[1]>=0 ? (selectIndex[1]+1) +"分钟" : '';
            time = selectIndex[0]*60+(selectIndex[1]+1);
        }
        
        this.setState({
            modalVisible: false,
            selectValue:selectH+selectM,
            currentFloor : -1,
            selectedValue:time
        })
    }

	getHeaderView(){
        const Colors = this.props.themeInfo.colors;

		return(
            <TouchableOpacity 
                activeOpacity={0.7}  
                style={[styles.inputWrapper,{backgroundColor: Colors.themeBg}]} 
                onPress={()=>{
                    this.setState({
                        modalVisible: true
                    })
                }}
            >
                <Text style={{fontSize:15,color:Colors.themeText,marginLeft:20,flex:1}}>自定义时间</Text>
				{this.showSelectValue()}
            </TouchableOpacity>
		)
	}

	showSelectValue(){
        const Colors = this.props.themeInfo.colors;

		if(this.state.selectValue != null){
			return(
				<View style={{flexDirection:'row'}}>
					<Text style={{color: Colors.themeText,marginRight: 15}}>{this.state.selectValue}</Text>
                    <Image style={styles.selectedIcon} source={require('../../../images/select.png')}/>
				</View>
			)
		}else{
			return(
                <Image style={styles.rightArrow} source={require('../../../images/enter_light.png')}/>
			)
		}
	}

	getFloorList(){
        const Colors = this.props.themeInfo.colors;

		let floors = this.state.floors.map((val,index)=>{
			let selected = null
			if(this.state.currentFloor == val){
				selected = <Image style={styles.selectedIcon} source={require('../../../images/select.png')}/>
			}
			return(
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    key={'floor_list_'+index} 
                    style={[styles.inputWrapper,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
					    this.setState({
						    currentFloor : val,
						    selectValue: null,
                            selectedValue : val
					    })
					    this.callBack && this.callBack(val)
					    //pop()
				    }}
                >
					<Text style={{fontSize:15,color:Colors.themeText,marginLeft:20,flex:1}}>{val==0?'永久开启':val+'分钟'}</Text>
					{selected}
				</TouchableOpacity>
			)
		})
		return (
			<View style={{paddingVertical:10,paddingHorizontal:16}}>
				<Text style={{color:Colors.themeTextLight,fontSize: 15}}>从设备打开时开始计算，到设定时间后自动执行关闭动作</Text>
				{this.getHeaderView()}
				{floors}
			</View>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors;

		return (
			<ScrollView style={{backgroundColor: Colors.themeBaseBg}} >
                {this.getModal()}
				{this.getFloorList()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	inputWrapper:{
		width: '100%',
		height: 60,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:15
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight:5
	},
	navText:{
		color:Colors.tabActiveColor,
		fontSize:15
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    rightArrow:{
        width:10,
        height:16,
        marginRight:20,
        marginLeft:10,
        resizeMode:'contain'
    },
    selectedIcon: {
        width:18,
        height:18,
        marginRight:20
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Setting_Maxopenning)

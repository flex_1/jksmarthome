/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
    FlatList,
    DeviceEventEmitter
} from 'react-native';
import { connect } from 'react-redux';
import { Colors,NetUrls,NotificationKeys } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderTitle from '../Navigation/HeaderTitle';
import {ImagesDark, ImagesLight} from '../../../common/Themes';
import DeviceListItem from '../ListItems/DeviceItem';

class Setting_AboutController extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeft navigation={navigation}/>,
			headerTitle: <HeaderTitle title={'所属网关'}/>,
            headerBackground: <HeaderBackground/>
		}
	}

	constructor(props) {
		super(props);
		const {getParam} = this.props.navigation
		this.callBack = getParam('callBack')
		this.deviceData = getParam('deviceData')

		this.state = {
			controllerData: {},
            deviceList: null,
            showSerialNumber: false
		}
	}

	componentDidMount() {
		this.getControllerData()
        this.noti = DeviceEventEmitter.addListener(NotificationKeys.kGatewayNeedUpdate,(data)=>{
            
            this.getControllerData(data.deviceId)

            this.scrollView?.scrollTo({x:0,y: 0,animated:false})
        })
	}

	componentWillUnmount(){
        this.noti.remove()
	}

	//获取 控制器 信息
	async getControllerData(deviceId) {
        let id = this.deviceData && this.deviceData.id
        if(deviceId){
            id = deviceId
        }
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.controllerDetail,
				params: {
					id: id
				}
			});
            SpinnerManager.close()
			if (data.code == 0) {
				this.setState({
					controllerData: data.result || {},
                    deviceList: data.result?.deviceList
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		} 
	}

    renderParams(title, content){
        if(!content){
            return null
        }
        const Colors = this.props.themeInfo.colors;
        return(
            <View style={styles.produceWrapper}>
                <View style={styles.titleWrapper}>
                    <Text style={[styles.titleText,{color: Colors.themeText}]}>{title}</Text>
                </View>
                <View style={styles.contentWrapper}>
                    <Text style={[styles.content,{color: Colors.themeText}]}>{content}</Text>
                </View>
            </View>
        )
    }

	getControllerDetail(){
		let {name,icon,serialNumber,functionParameters,deviceOrder,electricalParameters} = this.state.controllerData
		const defaultImg = this.props.themeInfo.isDark ? ImagesDark.default : ImagesLight.default
        const Colors = this.props.themeInfo.colors;
        // let device_order = null
        // try {
        //     device_order = serialNumber.split('_').pop()
        //     device_order = parseInt(device_order) + 1
        // } catch (error) {
            
        // }

		return(
			<View>
				<TouchableOpacity activeOpacity={1} style={styles.topImgTouch} onLongPress={()=>{
                    this.setState({
                        showSerialNumber: true
                    })
                }}>
                    <Image style={styles.img} defaultSource={defaultImg} source={{uri:icon}}/>
                    {deviceOrder ? <Text style={{marginTop: 10, fontSize: 13, color: Colors.themeTextLight}}>设备序号: {deviceOrder}</Text> : null}
				</TouchableOpacity>
				<View style={{marginTop:15,paddingHorizontal:16}}>
                    {this.renderParams('网关名称:', name)}
                    {this.state.showSerialNumber ? this.renderParams('序列号:', serialNumber) : null}
                    {this.renderParams('功能参数:', functionParameters)}
                    {this.renderParams('电气参数:', electricalParameters)}
				</View>
			</View>
		)
	}

    renderItem(rowData, index){
        return(
			<DeviceListItem
                numColumns={2}
				rowData={rowData} 
				index={index}
				navigation={this.props.navigation}
				updateDeviceCallBack = {(rowData)=>{
					this.state.deviceList[index] = rowData
                    this.setState({
                        ...this.state
                    })
                }}
                collectClick={(collect)=>{
                    this.state.deviceList[index].isCollect = collect
                    this.setState({
                        ...this.state
                    })
                }}
                isDark={this.props.themeInfo.isDark}
			/>
		)
    }

    renderDeviceList(){
        if(!this.state.deviceList || !this.state.deviceList.length){
            return null
        }
        const Colors = this.props.themeInfo.colors;
        return(
            <View style={{marginTop: 10,width:'100%'}}>
                <Text style={[styles.titleText,{color: Colors.themeText,marginLeft: 16}]}>网关下设备:</Text>
                <FlatList
                    numColumns={2}
                    style={{marginTop: 10}}
                    keyExtractor={(item, index) => 'devices_' + index}
                    data={this.state.deviceList}
                    renderItem={({ item, index }) => this.renderItem(item, index)}
                    initialNumToRender={8}
                />
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors;

		return (
			<ScrollView
                ref={e => this.scrollView = e}
                contentContainerStyle={{paddingBottom: 60}}
                style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}
            >
				{this.getControllerDetail()}
                {this.renderDeviceList()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex:1
	},
    titleText:{
        fontSize: 15,
        lineHeight: 20,
        fontWeight: 'bold'
    },
    titleWrapper:{
        flex: 3,
        paddingVertical: 3 
    },
    content:{
        fontSize: 15,
        lineHeight: 20,
    },
    contentWrapper:{
        flex: 7,
        paddingVertical: 3 
    },
    produceWrapper:{
        flexDirection:'row',
        marginTop: 5
    },
    img:{
        width:100,
        height:100,
        resizeMode:'contain'
    },
    topImgTouch:{
        justifyContent:'center',
        alignItems:'center',
        marginTop:20
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Setting_AboutController)

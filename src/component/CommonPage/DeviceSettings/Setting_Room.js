/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	DeviceEventEmitter,
} from 'react-native';
import { connect } from 'react-redux';
import { Colors,NetUrls,NotificationKeys } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { StatusBarHeight } from '../../../util/ScreenUtil';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderTitle from '../Navigation/HeaderTitle';

class Seting_Room extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeft navigation={navigation}/>,
			headerTitle: <HeaderTitle title={'选择位置'}/>,
            headerBackground: <HeaderBackground/>
		}
	}

	constructor(props) {
		super(props);
		const {getParam} = this.props.navigation
		this.callBack = getParam('callBack')
		this.deviceData = getParam('deviceData')
		this.excuteNetwork = getParam('excuteNetwork')

		this.state = {
			currentRoom : this.deviceData?.roomId,
			rooms :null,
			loading: true
		}
	}

	componentDidMount() {
		this.roomList()
	}

	// 获取房间 列表
	async roomList() {
		try {
			let data = await postJson({
				url: NetUrls.roomList,
				params: {
                    type: 2
				}
			})
			this.setState({loading: false})
			if (data.code == 0) {
				let rooms = data.result || []
				this.setState({
					rooms: rooms
				})
			} else {
				ToastManager.show(data.msg);
			}
		} catch (e) {
			this.setState({loading: false})
			ToastManager.show('网络错误');
		}
	}

	// 保存房间设置
	async changeRoom(roomId){
		const { pop } = this.props.navigation
		SpinnerManager.show()			
		try {
			let data = await postJson({
				url: NetUrls.saveDevice,
				params: {
					id: this.deviceData && this.deviceData.id,
					roomId: roomId
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				DeviceEventEmitter.emit(NotificationKeys.kDeviceFloorNotification)
				DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
				this.callBack && this.callBack()
				pop()
				ToastManager.show('设备更换房间成功')
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
	}

    _renderRooms(rowData){
        const Colors = this.props.themeInfo.colors;
        const {pop} = this.props.navigation

        let rooms = rowData.map((val,index) => {

            let textColor = Colors.themeTextLight
            let btnBg = Colors.themeBg
			if(this.state.currentRoom == val.id){
				textColor = Colors.white
                btnBg = Colors.themeButton
			}

            return(
                <View key={'rooms_'+index} style={styles.roomWrapper}>
                    <TouchableOpacity style={[styles.roomTouch,{backgroundColor:btnBg}]} onPress={()=>{
                        if(this.excuteNetwork){
						    this.changeRoom(val.id)
					    }else{
						    this.callBack && this.callBack(val)
						    pop()
		 			    }
                    }}>
                        <Text numberOfLines={2} style={[styles.roomText,{color: textColor}]}>{val.name}</Text>
                    </TouchableOpacity>
                </View>
            )
        })
        
        return(
            <View style={{flexDirection:'row', flexWrap:'wrap'}}>
                {rooms}
            </View>
        )
    }

	getFloorList(){
		const Colors = this.props.themeInfo.colors;

		if(this.state.loading && !this.state.rooms){
			return(
				<View style={{alignItems:'center',marginTop:'50%'}}>
					<Text style={{color:Colors.themeTextLight}}>正在加载...</Text>
				</View>
			)
		}
		if(!this.state.rooms){
			return(
				<View style={{alignItems:'center',marginTop:'50%'}}>
					<Text style={{color:Colors.themeTextLight}}>网络错误</Text>
				</View>
			)
		}
		if(this.state.rooms.length <= 0){
			return(
				<View style={{alignItems:'center',marginTop:'50%'}}>
					<Text style={{color:Colors.themeTextLight}}>暂无房间</Text>
				</View>
			)
		}

        let floors = this.state.rooms.map((val,index)=>{
            if(!val.listRoom || !val.listRoom.length){
                return null
            }
            return(
                <View key={'floor_'+index} style={{marginBottom: 20}}>
                    <Text style={{fontSize: 15, fontWeight:'bold', marginLeft:5, color: Colors.themeText}}>{val.floorText}</Text>
                    {this._renderRooms(val.listRoom)}
                </View>
            )
        })
		return (
			<View style={{paddingVertical:5,paddingHorizontal:16,marginTop: 15}}>
				{floors}
			</View>
		)
	}

	render() {
		const Colors = this.props.themeInfo.colors;

		return (
            <ScrollView 
                style={{backgroundColor: Colors.themeBaseBg,flex:1}} 
                contentContainerStyle={styles.scrollContent}
            >
				{this.getFloorList()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
    scrollContent:{
        paddingBottom: 40
    },
	inputWrapper:{
		width: '100%',
		height: 60,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:10
	},
    roomWrapper:{
        width:'25%', 
        paddingHorizontal:5, 
        marginTop: 10
    },
    roomTouch:{
        width: '100%', 
        height: 40, 
        justifyContent:'center', 
        alignItems:'center',
        borderRadius: 4,
        paddingHorizontal: 5
    },
    roomText:{
        fontSize: 12, 
        textAlign: 'center'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Seting_Room)

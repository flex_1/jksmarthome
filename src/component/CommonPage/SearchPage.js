/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	DeviceEventEmitter,
	TextInput,
	Keyboard
} from 'react-native';
import { postJson, getRequest } from '../../util/ApiRequest';
import { NetUrls, Colors, LocalStorageKeys } from '../../common/Constants';
import DeviceList from '../Home/DeviceList';
import SceneList from '../Scene/SceneList';
import ToastManager from '../../common/CustomComponent/ToastManager';
import { StatusBarHeight, NavigationBarHeight } from '../../util/ScreenUtil';
import AsyncStorage from '@react-native-community/async-storage';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import { connect } from 'react-redux';

class SearchPage extends Component {

	static navigationOptions = {
		header: null,
        headerBackground: <HeaderBackground/>
	}

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.searchType = getParam('searchType')  // Device:设备  Scene:场景
		this.localStorageKey = this.getLocalStorageKey()

		this.state = {
			searchText: null,
			isSearchResult: false,
			historyData: null
		}
	}

	getLocalStorageKey(){
		switch (this.searchType) {
			case 'Device':
				return LocalStorageKeys.kDeviceSearch;
			
			case 'Scene':
				return LocalStorageKeys.kSceneSearch;

			default:
				return LocalStorageKeys.kDeviceSearch;
		}
	}

	componentWillMount(){
		this.getSearchHistory()
	}

	componentDidMount() {
		
	}

	componentWillUnmount() {
		
	}

	searchSubmit(searchText){
		this.setState({
			isSearchResult: true
		})
		this.saveTheSearchTextToLocal(searchText)
	}

	// 存储搜索关键字 到本地
	async saveTheSearchTextToLocal(searchText){
		if(!searchText){
			return
		}
		let historyData = await AsyncStorage.getItem(this.localStorageKey)
		let storage = []
		if(!historyData){
			storage = [searchText]
		}else{
			storage = JSON.parse(historyData)
			let index = storage.indexOf(searchText)
			// 如果搜索重复，去重
			if(index != -1){
				storage.splice(index,1)
			}
			// 如果搜索长度大于10，去掉最后一个
			if(storage.length > 9){
				storage.splice(9, storage.length-9)
			}
			storage.unshift(searchText)
		}
		AsyncStorage.setItem(this.localStorageKey,JSON.stringify(storage),()=>{
			this.getSearchHistory()
		})
	}

	// 获取 本地 搜索历史
	async getSearchHistory(){
		let historyData = await AsyncStorage.getItem(this.localStorageKey)
		this.setState({
			historyData: JSON.parse(historyData)
		})
	}

	// 清除 本地 搜索历史
	async clearSearchHistory(){
		AsyncStorage.removeItem(this.localStorageKey)
		this.setState({
			historyData: null
		})
	}

	// 去空格
	getStringByRemovingSpace(str){
		// 去首空格
		let str1 = str.replace(/\s+$/,'')
		// 去尾格  
		let str2 = str1.replace(/^\s+/,'')
		return str2
	}

	getSearchHeader(){
        const Colors = this.props.themeInfo.colors
		const {pop} = this.props.navigation
		return(
			<View style={[styles.topWrapper,{backgroundColor: Colors.themeBg}]}>
                <View style={styles.searchWrapper}>
                    <Image style={{width:15,height:15,resizeMode:'contain'}} source={require('../../images/search_gray.png')}/>
                    <TextInput
                        style={{flex:1,paddingLeft:10}}
                        value={this.state.searchText}
                        placeholder={'请输入关键字'}
                        placeholderTextColor={Colors.themeBGInactive}
                        returnKeyType={'search'}
                        clearButtonMode={'while-editing'}
                        maxLength={16}
                        onChangeText={(text) => {
                            this.setState({
                                searchText : text
                            })
                        }}
                        onSubmitEditing={()=>{
                            if(!this.state.searchText){
                                ToastManager.show('请输入关键字')
                                return
                            }
                            let searchText = this.getStringByRemovingSpace(this.state.searchText)
                            this.searchSubmit(searchText)
                        }}
                        onFocus={()=>{
                            this.setState({
                                isSearchResult: false
                            })
                        }}
                    />
                </View>
                <TouchableOpacity style={styles.cancelBtn} onPress={()=>{
                    pop()
                }}>
                    <Text style={{fontSize:15,color:Colors.themeText}}>取消</Text>
                </TouchableOpacity>
            </View>
		)
	}

	// 显示搜索历史标签
	getHistoryTags(){
        const Colors = this.props.themeInfo.colors

		if(!this.state.historyData){
			return null
		}
		let tags = this.state.historyData.map((value,index)=>{
			return (
                <TouchableOpacity
                    key={'search_history_'+index}
                    style={[styles.searchTagsWrapper,{backgroundColor: Colors.themeBg}]}  
                    onPress={()=>{
                        Keyboard.dismiss()
                        this.setState({
                            searchText: value,
                            isSearchResult: true
                        })
                        this.saveTheSearchTextToLocal(value)
				    }}
                >
					<Text style={{fontSize: 15,color: Colors.themeText}}>{value}</Text>
				</TouchableOpacity>
			)
		})
		return tags
	}

	getMainPage(){
        const Colors = this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}

		if(this.state.isSearchResult){
			if(this.searchType == 'Device'){
				return(
					<DeviceList 
						navigation={this.props.navigation}
						deviceName={this.state.searchText}
                        isSearchList={true}
                        numColumns={1}
                        isDark={this.props.themeInfo.isDark}
					/>
				)
			}else if(this.searchType == 'Scene'){
				return(
					<SceneList 
                   	 	navigation={this.props.navigation} 
						sceneName={this.state.searchText}
                        isSearchList={true}
                        numColumns={1}
                        isDark={this.props.themeInfo.isDark}
                	/>
				)
			}
		}else{
			return(
				<View style={{flex:1,paddingHorizontal: 16}}>
					<View style={styles.titleWrapper}>
						<Text style={{fontSize:16,fontWeight:'bold',color: Colors.themeText}}>历史搜索</Text>
						<TouchableOpacity style={{padding: 10}} onPress={()=>{
							this.clearSearchHistory()
						}}>
							<Image style={[styles.deleteImg,tintColor]} source={require('../../images/delete.png')}/>
						</TouchableOpacity>
					</View>
					<View style={{flexDirection:'row',flexWrap:'wrap',marginTop:10}}>
						{this.getHistoryTags()}
					</View>
				</View>
			)
		}
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getSearchHeader()}
				{this.getMainPage()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.themBGLightGray,
		flex:1
	},
	status:{
		width:'100%',
        height: StatusBarHeight,
        backgroundColor: Colors.white
	},
	topWrapper:{
		width:'100%',
		alignItems:'center',
		flexDirection:'row',
        paddingLeft: 16,
        paddingTop: Platform.select({
            ios: StatusBarHeight,
            android: StatusBarHeight + 10
        }),
        paddingBottom: Platform.select({
            ios: 0,
            android: 5
        }),
        height: Platform.select({
            ios: 50 + StatusBarHeight,
            android: NavigationBarHeight + StatusBarHeight
        })
	},
	searchWrapper:{
		backgroundColor:Colors.themBGLightGray,
		flex:1,
		borderRadius: 4,
		alignItems:'center',
		flexDirection:'row',
        paddingHorizontal: 10,
        paddingVertical: Platform.select({
            android: 0,
            ios: 10
        })
	},
	searchTagsWrapper:{
		marginRight: 15,
		paddingHorizontal: 15,
		paddingVertical: 8,
		borderRadius:4,
		marginTop:10,
		justifyContent:'center',
        alignItems:'center',
	},
	cancelBtn:{
		paddingRight: 16,
		paddingLeft: 10,
		marginLeft: 5,
		height:'100%',
		justifyContent:'center',
		alignItems:'center'
    },
    titleWrapper:{
        flexDirection:'row',
        marginTop:10,
        justifyContent:'space-between',
        alignItems:'center'
    },
    deleteImg:{
        width:16,
        height:16,
        resizeMode:'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SearchPage)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
    Modal,
    Keyboard
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../util/ScreenUtil';
import ToastManager from "../../common/CustomComponent/ToastManager";
import { postJson } from '../../util/ApiRequest';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';

const passWordLength = 6

class DoorLockAddPassWord extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'新增密码'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
		super(props);
        const { getParam } = props.navigation;
        
        this.deviceId = getParam('deviceId')
        this.callBack = getParam('callBack')
        
        this.state = {
            pass: '',
            pass2: '',
            name: ''
        }
    }
    
	componentDidMount() {
        
	}

	componentWillUnmount(){
        
    }
    
    // 获取当前 状态
    async addPassWord(id){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.changeDoorPwd,
				params: {
                    id: this.deviceId,
                    name: this.state.name,
                    password: this.state.pass
                }
            });
            SpinnerManager.close()
			if (data.code == 0) {
                this.props.navigation.pop()
				this.callBack && this.callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }
    
    renderInputView() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={{paddingHorizontal: 20}}>
                <View style={[styles.inputWrapper, {backgroundColor: Colors.themeBg}]}>
                    <TextInput
                        keyboardType={'number-pad'}
                        maxLength={passWordLength}
                        style={[styles.input,{color: Colors.themeText}]}
                        placeholder="请输入密码"
                        clearButtonMode={'while-editing'}
                        placeholderTextColor={Colors.themeInactive}
                        secureTextEntry={true}
                        onChangeText={(text) => {
                            this.state.pass = text
                        }}
                    />        
                </View>
                <View style={[styles.inputWrapper, {backgroundColor: Colors.themeBg}]}>
                    <TextInput
                        keyboardType={'number-pad'}
                        maxLength={passWordLength}
                        style={[styles.input,{color: Colors.themeText}]}
                        placeholder="请确认密码"
                        clearButtonMode={'while-editing'}
                        placeholderTextColor={Colors.themeInactive}
                        secureTextEntry={true}
                        onChangeText={(text) => {
                            this.state.pass2 = text
                        }}
                    />
                </View>
                <View style={[styles.inputWrapper, {backgroundColor: Colors.themeBg}]}>
                    <TextInput
                        maxLength={12}
                        style={[styles.input,{color: Colors.themeText}]}
                        placeholder="请输入名称"
                        clearButtonMode={'while-editing'}
                        placeholderTextColor={Colors.themeInactive}
                        onChangeText={(text) => {
                            this.state.name = text
                        }}
                    />
                </View>
            </View>
        );
    }

    renderSureButton(){
        return (
            <TouchableOpacity activeOpacity={0.7} style={styles.sureBtn} onPress={()=>{
                Keyboard.dismiss()
                if(this.state.pass.length != passWordLength){
                    ToastManager.show('请输入'+ passWordLength + '位数密码')
                    return
                }
                if(this.state.pass != this.state.pass2){
                    ToastManager.show('请确认两次输入的密码一致')
                    return
                }
                if(!this.state.name){
                    ToastManager.show('请输入名称')
                    return
                }
                this.addPassWord()
            }}>
                <Text style={styles.sureBtnText}>保存</Text>
            </TouchableOpacity>
        )
    }
    
	render() {
        const Colors = this.props.themeInfo.colors
        
		return (
			<ScrollView style={[styles.main,{backgroundColor: Colors.themeBaseBg}]} keyboardShouldPersistTaps="handled">
				{this.renderInputView()}
                {this.renderSureButton()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1
    },
    input: {
        flex: 1,
        paddingLeft: 20,
        height:40,
        fontSize: 16
    },
    inputWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        borderRadius: 10,
        paddingVertical:4,
        marginTop: 20
    },
    tips:{
        fontSize: 15,
        marginTop: 10
    },
    sureBtn:{
        height: 44,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.newTheme,
        marginHorizontal: 20,
        marginTop: 20,
        borderRadius: 5
    },
    sureBtnText:{
        fontSize: 15,
        color: Colors.white
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(DoorLockAddPassWord)



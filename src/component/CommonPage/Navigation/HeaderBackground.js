/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component,PureComponent} from 'react';
import {StyleSheet, Image, Text, View,SafeAreaView} from 'react-native';
import { connect } from 'react-redux';
import {ColorsLight} from '../../../common/Themes';

// * hiddenBorder  : 用来隐藏导航栏底部 border
// * disableNight  : 为true时，不随黑夜模式变色 

export class HeaderBackground extends PureComponent {

    render(){
        const {disableNight,hiddenBorder} = this.props
        const Colors = disableNight ? ColorsLight : this.props.themeInfo.colors
        const borderBottom = hiddenBorder ? {} : {borderBottomColor: Colors.themeBaseBg,borderBottomWidth: 1}

        return(
            <SafeAreaView style={[styles.wrapper,borderBottom,{backgroundColor: Colors.themeBg}]}>
                
            </SafeAreaView>
        )
    }
}

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(HeaderBackground)

const styles = StyleSheet.create({
    wrapper: {
        flex:1
    }
});

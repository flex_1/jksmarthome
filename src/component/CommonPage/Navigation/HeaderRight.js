/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component,PureComponent} from 'react';
import {StyleSheet, Image, Text, View,SafeAreaView, Platform, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import {ColorsLight} from '../../../common/Themes';

// buttonArrays = {icon:'',text:'',onPress:()=>{}, noImgTint:true} 位于右侧的按钮(数组，可以为多个按钮) (noImgTint: 图片不要黑白模式)
// * icon : 图标
// * text : 文字
// * onPress : 按钮回调
// * tintColor : 自定义颜色
// * disableNight  : 为true时，不随黑夜模式变色 

export class HeaderRight extends PureComponent {

    _renderRightBtns(content,key,isLast){
        const {disableNight} = this.props
        const Colors = disableNight ? ColorsLight : this.props.themeInfo.colors

        let tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}
        if(content.tintColor){
            tintColor = content.tintColor
        }

        let btnImg = null
        if(content.icon){
            btnImg = <Image source={content.icon} style={[styles.rightImg,content.noImgTint?{}:tintColor]}/>
        }

        let btnText = null
        if(content.text){
            btnText = (
                <Text style={[
                    styles.rightBtnText,
                    {color: Colors.themeText},
                    content.tintColor ? {color: content.tintColor} : {}
                ]}>
                    {content.text}
                </Text>
            )
        }

        return(
            <TouchableOpacity 
                key={'right_btn_'+key}
                activeOpacity={0.7}
                style={[styles.rightBtn, isLast ? {...styles.last} : null]}
                onPress={()=>{
                    try {
                        content.onPress && content.onPress()
                    } catch (error) {
                        console.log('点击事件未初始化完成');
                    }
                }}
            >
                {btnImg}
                {btnText}
            </TouchableOpacity>
        )
    }

    
    renderRight(){
        const { buttonArrays } = this.props
        
        if(!buttonArrays){
            return <View/>
        }

        let newArrays = []
        buttonArrays.forEach(element => {
            if(element) newArrays.push(element)
        });

        let btns = newArrays.map((value,index)=>{
            let isLast = false
            if( buttonArrays.length == (index + 1)){
                isLast = true
            }
            return this._renderRightBtns(value,index,isLast)
        })
        
        return(
            <View style={styles.rightWrapper}>
                {btns}
            </View>
        )
    }

    render(){
        return this.renderRight()
    }
}

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(HeaderRight)


const styles = StyleSheet.create({
    rightWrapper:{
        height: '100%',
        justifyContent:'center',
        flexDirection: 'row'
    },
    rightBtn:{
        paddingRight: 10,
        paddingLeft: 10,
        marginRight: 10,
        flexDirection: 'row',
        alignItems:'center',
        height: '100%'
    },
    rightImg:{
        width: 22,
        height: 22,
        resizeMode: 'contain'
    },
    rightBtnText:{
        textAlignVertical: 'center',
        fontSize: 15,
        marginLeft: 5
    },
    last:{
        marginRight: 0,
        paddingRight: 16,
        paddingLeft: 10
    }
});

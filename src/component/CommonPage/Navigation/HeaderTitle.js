/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component,PureComponent} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import { connect } from 'react-redux';
import {ColorsLight} from '../../../common/Themes';

// * title : 剧中的大标题
// * disableNight  : 为true时，不随黑夜模式变色 

class HeaderTitle extends PureComponent {

    render(){
        const {disableNight,title} = this.props
        const Colors = disableNight ? ColorsLight : this.props.themeInfo.colors

        return (
            <View style={styles.wrapper}>
                <Text style={[styles.title,{color: Colors.themeText}]}>{title}</Text>
            </View>
        )
    }
}

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(HeaderTitle)


const styles = StyleSheet.create({
    wrapper: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    title:{
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center'
    },
});

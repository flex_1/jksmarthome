/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component,PureComponent} from 'react';
import {StyleSheet, Image, Text, View,SafeAreaView, Platform, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { StatusBarHeight,NavigationBarHeight } from '../../../util/ScreenUtil';
import {ColorsLight} from '../../../common/Themes';

// * title : 居左的大标题(优先级最高)
// * backTitle : 返回按钮的文字(可以为空)
// * onClick : 返回按钮点击事件
// * disableNight  : 为true时，不随黑夜模式变色 

class HeaderLeft extends PureComponent {

    renderLeft(){
        const {pop,goBack} = this.props.navigation
        const {title,backTitle,onClick,disableNight} = this.props
        const Colors = disableNight ? ColorsLight : this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}
        
        if(title){
            return(
                <View style={styles.leftTitleWrapper}>
                    <Text style={[styles.leftTitle,{color: Colors.themeText}]}>{title+' '}</Text>
                </View>
            )
        }

        let backImg = require('../../../images/back.png')
        let text = null
        if (backTitle){
            text = <Text style={[styles.backText,{color: Colors.themeText}]}>{backTitle+' '}</Text>
        }

        return (
            <TouchableOpacity
                style={styles.backTouch}
                activeOpacity = {0.7}
                onPress={() => {
                    if(onClick){
                        onClick()
                    }else{
                        goBack() 
                    }
                }}
            >
                <Image style={[styles.backImg,tintColor]} source={backImg} />
                {text}
            </TouchableOpacity>
        )
    }

    render(){
        return this.renderLeft()
    }
}

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(HeaderLeft)


const styles = StyleSheet.create({
    navWrapper: {
        height: NavigationBarHeight,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    backTouch: {
        paddingLeft: 16,
        flexDirection: 'row',
        alignItems: 'center',
        height: '100%'
    },
    backImg: { 
        width: 10, 
        height: 18, 
        marginRight: 15 
    },
    backText: {
        fontSize:24,
        fontWeight:'bold'
    },
    leftTitleWrapper:{
        height: '100%',
        justifyContent:'center',
        alignItems: 'center'
    },
    leftTitle:{
        fontSize:24,
        fontWeight:'bold',
        marginLeft: 15
    }
});

import React, { Component } from 'react';
import {
	View,
    Text,
    Image,
    TouchableOpacity,
    Dimensions,
    StyleSheet,
} from 'react-native';
import { ScrollView } from 'react-navigation';
import { Colors } from '../../common/Constants';
import {ColorsLight, ColorsDark, ImagesDark, ImagesLight} from '../../common/Themes';

class FilterModal extends Component {

    constructor(props) {
		super(props);
		this.state = {
            filterSelected: JSON.parse(JSON.stringify(this.props.filterSelected))
		}
	}

    filterOnClick(value, index){
        // 点击特殊点
        if(this.props.specialClass == value.nameEn){
            let selected = [value.nameEn]
            if(this.state.filterSelected.includes(this.props.specialClass)){
                selected = []
            }
            this.setState({
                filterSelected: selected
            })
            return
        }
        // 所选里包含了特殊点
        if(this.state.filterSelected.includes(this.props.specialClass)){
            this.state.filterSelected.remove(this.props.specialClass)
            this.state.filterSelected.push(value.nameEn)
            this.setState({
                filterSelected: this.state.filterSelected
            })
            return
        }
        // 不包含特殊点
        // 处理在线离线
        if(value.nameEn == 'ONLINE'){
            this.state.filterSelected.remove('OFFLINE')
        }else if(value.nameEn == 'OFFLINE'){
            this.state.filterSelected.remove('ONLINE')
        }
        if(this.state.filterSelected.includes(value.nameEn)){
            this.state.filterSelected.remove(value.nameEn)
        }else{
            this.state.filterSelected.push(value.nameEn)
        }
        this.setState({
            filterSelected: this.state.filterSelected
        })
    }

    renderSpecailList(){
        if(!this.props.specailFilters || this.props.specailFilters.length<=0){
            return null
        }
        const Colors = this.props.isDark ? ColorsDark : ColorsLight

        let specailList = this.props.specailFilters.map((value, index)=>{
            let btnBg = Colors.themeBaseBg
            let btnText = Colors.themeTextLight
            if(this.state.filterSelected.includes(value.nameEn)){
                btnBg = Colors.newTheme
                btnText = Colors.white
            }
            return(
                <TouchableOpacity 
                    key={'cate_index2_'+index}
                    activeOpacity={0.7}
                    style={styles.filterItem}
                    onPress={()=>{
                        this.filterOnClick(value, index)
                    }}
                >
                    <View style={[styles.itemWrapper,{backgroundColor:btnBg}]}>
                        <Text style={{color: btnText, fontSize: 13}}>{value.name}</Text>
                        <Text style={{color: btnText, fontSize: 13}}>({value.count})</Text>
                    </View>
                </TouchableOpacity>
            )
        })

        return specailList
    }

    renderList(){
        if(!this.props.filters || this.props.filters.length<=0){
            return null
        }
        const Colors = this.props.isDark ? ColorsDark : ColorsLight

        let list = this.props.filters.map((value, index)=>{
            let btnBg = Colors.themeBaseBg
            let btnText = Colors.themeTextLight
            if(this.state.filterSelected.includes(value.nameEn)){
                btnBg = Colors.newTheme
                btnText = Colors.white
            }
            return(
                <TouchableOpacity 
                    key={'cate_index_'+index}
                    activeOpacity={0.7}
                    style={styles.filterItem}
                    onPress={()=>{
                        this.filterOnClick(value, index)
                    }}
                >
                    <View style={[styles.itemWrapper,{backgroundColor:btnBg}]}>
                        <Text style={{color: btnText, fontSize: 13}}>{value.name}</Text>
                        <Text style={{color: btnText, fontSize: 13}}>({value.count})</Text>
                    </View>
                </TouchableOpacity>
            )
        })

        return list
    }

    renderItemList(){
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        return (
            <ScrollView contentContainerStyle={{paddingBottom: 20}}>
                <View style={styles.filterList}>
                    {this.renderList()}
                </View>
                <View style={{height: 1, width:'100%', backgroundColor: Colors.themeBaseBg}}/>
                <View style={[styles.filterList,{paddingTop: 10}]}>
                    {this.renderSpecailList()}
                </View>
            </ScrollView>
        )
    }

    renderFilterModal(){
        const Colors = this.props.isDark ? ColorsDark : ColorsLight

        return(
            <View style={[styles.filterWrapper,{backgroundColor:Colors.lightTransparent},this.props.style]}>
                <View style={{backgroundColor:Colors.themeBg, maxHeight: '70%'}}>
                    {this.renderItemList()}
                    <View style={{flexDirection:'row'}}>
                        <TouchableOpacity style={[styles.filterBottomBtn,{backgroundColor:Colors.themeBaseBg}]} onPress={()=>{
                            this.setState({filterSelected: []})
                        }}>
                            <Text style={{fontSize: 15, color: Colors.themeTextLight}}>清空</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.filterBottomBtn,{backgroundColor:Colors.newTheme}]} onPress={()=>{
                            let filterSelected = JSON.parse(JSON.stringify(this.state.filterSelected))
                            this.props.confirmBtnClick(filterSelected)
                        }}>
                            <Text style={{fontSize: 15,fontWeight:'bold',color: Colors.white}}>查看</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <TouchableOpacity activeOpacity={1} style={{flex: 1}} onPress={()=>{ 
                    this.props.dissmissModal()
                }}>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return this.renderFilterModal()
    }
}

const styles = StyleSheet.create({
    filterWrapper:{
        position:'absolute',
        zIndex: 1000,
        left:0,
        right:0,
        height:'100%',
    },
    filterItem:{
        height:35,
        width:'33.3%',
        alignItems:'center',
        marginBottom:12
    },
    itemWrapper:{
        justifyContent:'center',
        alignItems:'center',
        width:'90%',
        height:'100%',
        borderRadius:4,
        flexDirection: 'row'
    },
    filterList:{
        paddingTop:15,
        paddingHorizontal:10,
        flexDirection:'row',
        flexWrap:'wrap'
    },
    filterBottomBtn:{
        flex: 1, 
        height: 45,
        justifyContent:'center',
        alignItems:'center'
    }
});


export default FilterModal

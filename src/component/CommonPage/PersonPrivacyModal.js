import React, { Component } from 'react';
import {
	View,
    Text,
    TouchableOpacity,
    StyleSheet,
    BackHandler
} from 'react-native';
import { ScrollView } from 'react-navigation';
import { Colors } from '../../common/Constants';
import PropTypes from 'prop-types';


const PersonPrivacyModal = (props)=>{
    const {navigate} = props.navigation

    return(
        <View style={styles.modalWrapper}>
            <View style={styles.secretWrapper}>
                <View style={{width:'100%',height:50,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontSize:16,fontWeight:'bold'}}>个人信息保护指引</Text>
                </View>
                <ScrollView style={{paddingHorizontal: 16}}>
                    <Text style={styles.contentText}>      尊敬的用户，欢迎使用小萨管家。我们将通过
                        <Text style={{color: Colors.newTheme}} onPress={()=>{ navigate('Protocol',{title:'用户协议',type:1}) }}>《用户协议》</Text>
                        和
                        <Text style={{color: Colors.newTheme}} onPress={()=>{ navigate('Protocol',{title:'隐私政策',type:2}) }}>《隐私政策》</Text>
                        帮助你了解我们为您提供的服务，及收集和处理个人信息的方式。{'\r\n'}      经您确认后，本用户协议和隐私权政策即在您和本应用之间产生法律效力。请您务必在使用之前认真阅读全部服务协议内容。
                    </Text>
                </ScrollView>
                <View style={styles.bottomBtnWrapper}>
                    <TouchableOpacity activeOpacity={0.7} style={styles.agreeBtnTouch} onPress={()=>{
                        props.callBack && props.callBack()
                    }}>
                        <Text style={styles.agreeText}>同意并继续</Text>
                    </TouchableOpacity>
                    <View style={styles.notAgreeWrapper}>
                        <TouchableOpacity activeOpacity={0.7} style={styles.notAgreeTouch} onPress={()=>{
                            BackHandler.exitApp()
                        }}>
                            <Text style={styles.notAgreeText}>暂时不使用</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}

PersonPrivacyModal.propTypes = {
    callBack: PropTypes.func,
    navigation: PropTypes.object
}

const styles = StyleSheet.create({
    modalWrapper:{
        position: 'absolute', 
        top: 0, 
        left:0, 
        width:'100%', 
        height:'100%', 
        backgroundColor:Colors.translucent,
        justifyContent:'center',
        alignItems:'center'
    },
    secretWrapper:{
        width: '80%',
        height: '50%',
        backgroundColor: Colors.white,
        borderRadius: 5
    },
    bottomBtnWrapper:{
        paddingVertical:10,
        width:'100%',
        paddingHorizontal: 16
    },
    agreeBtnTouch:{
        width:'100%',
        height:40,
        backgroundColor:'#25cad2',
        justifyContent:'center',
        alignItems:'center'
    },
    agreeText:{
        fontSize: 15, 
        color: Colors.white, 
        fontWeight:'500'
    },
    notAgreeWrapper:{
        marginTop: 10, 
        height:30,
        justifyContent:'center',
        alignItems:'center'
    },
    notAgreeTouch:{
        paddingVertical:5, 
        paddingHorizontal:10
    },
    notAgreeText:{
        fontSize:14, 
        color: Colors.themeTextLightGray
    },
    contentText:{
        fontSize: 14,
        lineHeight: 20,
    }
});


export default PersonPrivacyModal

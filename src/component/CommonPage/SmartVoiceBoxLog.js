/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StatusBar,
    View,
    DeviceEventEmitter,
    Modal,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    ScrollView,
    Alert,
    FlatList
} from 'react-native';
import { postJson, } from '../../util/ApiRequest';
import { NetUrls, LocalStorageKeys } from '../../common/Constants';
import { Colors } from '../../common/Constants';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";


class SmartVoiceBoxLog extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation}/>,
        headerTitle: <HeaderTitle title={'控制记录'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })
    
    constructor(props) {
        super(props);
        const { getParam,setParams } = this.props.navigation
    
        this.deviceId = getParam('deviceId')
        this.boxIcon = getParam('boxIcon')
        
        this.state = {
            logList: null,
            currentPage: 0,
            totalPage: 0
        }
    }

    componentDidMount() {
        this.requestLog(1)
    }

    componentWillUnmount() {
        
    }

    // 获取房间 列表
    async requestLog(page) {
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.boxControlRecord,
				params: {
                    deviceId: this.deviceId,
                    pageNum: page,
                    pageSize: 10
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                const listData = data.result?.outPageInfo || {}
                let logList = []
                if(page <= 1){
                    logList = listData.list || []
                }else{
                    logList = this.state.logList.concat(listData.list)
                }
                this.setState({
                    logList: logList,
                    currentPage: listData.pageNum,
                    totalPage: listData.pages
                })
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    nextPage(){
        if(this.state.currentPage < this.state.totalPage){
            this.requestLog(this.state.currentPage+1)
        }
    }

    renderRowItem(rowData, index){
        const Colors = this.props.themeInfo.colors

        return(
            <View>
                <View style={styles.timeWrapper}>
                    <Text style={[styles.timeText,{color: Colors.themeTextLight}]}>{rowData.createTime}</Text>
                </View>
                <View style={styles.questionItem}>
                    <View style={styles.questionBgWrapper}>
                        <View style={styles.questionBg}>
                            <Text numberOfLines={0} style={styles.orderText}>{rowData.sender}</Text>
                        </View>
                        <View style={styles.arrowWrapper}>
                            <View style={[styles.rightArrow,{borderLeftColor: Colors.newBtnBlueBg}]}/>
                        </View>
                    </View>
                    <View style={styles.userIconBg}>
                        <Image style={styles.icon} source={require('../../images/voiceBoxUser.png')}/>
                    </View>
                </View>
                <View style={styles.answerItem}>
                    <View style={styles.userIconBg}>
                        <Image style={styles.icon} source={{uri: this.boxIcon}}/>
                    </View>
                    <View style={styles.answerBgWrapper}>
                        <View style={styles.arrowWrapper}>
                            <View style={[styles.leftArrow,{borderRightColor: Colors.themeBg}]}/>
                        </View>
                        <View style={[styles.answerBg,{backgroundColor: Colors.themeBg}]}>
                            <Text numberOfLines={0} style={[styles.orderText,{color: Colors.themeText}]}>{rowData.responder}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    renderList(){
        if(this.state.logList?.length <= 0){
            return(
                <View style={styles.noContentWrapper}>
                    <Text style={styles.noContentText}>暂无控制记录</Text>
                </View>
            )
        }
        return(
            <FlatList
                style={{flex: 1}}
				removeClippedSubviews={false}
				contentContainerStyle={{ paddingBottom: 50}}
                data={this.state.logList}
				initialNumToRender={10}
				scrollIndicatorInsets={{right: 1}}
                keyExtractor={(item, index) => 'log_' + index}
                renderItem={({item, index}) => this.renderRowItem(item, index)}
                onEndReached={()=>{this.nextPage()}}
                onEndReachedThreshold={0.1}
            /> 
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors
        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderList()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1
    },
    timeWrapper:{
        justifyContent:'center',
        alignItems:'center',
        marginTop: 20,
        paddingVertical: 5
    },
    questionItem:{
        paddingRight:16,
        width:'100%',
        flexDirection:'row',
        justifyContent: 'flex-end',
        marginTop: 10
    },
    questionBgWrapper:{
        flexDirection:'row',
        flex: 1,
        paddingVertical: 2,
        justifyContent: 'flex-end'
    },
    questionBg:{
        backgroundColor: Colors.newBtnBlueBg,
        paddingVertical: 9, 
        paddingHorizontal:15, 
        borderRadius: 8,
        maxWidth: '60%'
    },
    answerItem:{
        paddingLeft:16,
        marginTop: 10,
        flexDirection:'row'
    },
    answerBgWrapper:{
        flex: 1,
        flexDirection:'row',
        paddingVertical: 2
    },
    answerBg:{
        paddingVertical: 9, 
        paddingHorizontal:15, 
        borderRadius: 8,
        maxWidth: '60%'
    },
    userIconBg:{
        width: 48, 
        height:48, 
        borderRadius: 24,
        justifyContent: 'center',
        alignItems: 'center'
    },
    orderText:{
        fontSize:16,
        fontWeight: '400',
        lineHeight: 22, 
        color: Colors.white
    },
    icon:{
        width:'80%',
        height:'80%'
    },
    timeText:{
        fontSize:11, 
        fontWeight:'400'
    },
    noContentWrapper:{
        flex: 1, 
        justifyContent: 'center',
        alignItems:'center'
    },
    noContentText:{
        fontSize: 15,
        color: Colors.themeTextLightGray
    },
    arrowWrapper:{
        width:10,
        height:42,
        justifyContent: 'center',
        alignItems:'center'
    },
    rightArrow:{
        width:0,
        height:0,
        borderStyle:'solid',
        borderWidth: 5,
        borderTopColor: 'transparent', //下箭头颜色
        borderLeftColor: Colors.newBtnBlueBg, //右箭头颜色
        borderRightColor: 'transparent', //左箭头颜色
        borderBottomColor: 'transparent', //上箭头颜色
    },
    leftArrow:{
        width:0,
        height:0,
        borderStyle:'solid',
        borderWidth: 5,
        borderTopColor: 'transparent', //下箭头颜色
        borderLeftColor: 'transparent', //右箭头颜色
        
        borderBottomColor: 'transparent', //上箭头颜色
    },
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SmartVoiceBoxLog)

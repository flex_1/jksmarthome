/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	TextInput,
	DeviceEventEmitter,
	ImageBackground,
	Alert,
	FlatList,
	RefreshControl
} from 'react-native';
import { postJson } from '../../../util/ApiRequest';
import { NetUrls,NotificationKeys,NetParams,GetdeviceRouter } from '../../../common/Constants';
import { Colors } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import {ListNoContent,FooterEnd,ListLoading,ListError } from "../../../common/CustomComponent/ListLoading";
import {ImagesLight, ImagesDark} from '../../../common/Themes';
import UpdateDeviceList from '../../../util/DeviceUpdate';
import DeviceListItem from '../ListItems/DeviceItem';

class NewDeviceList extends Component {

	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			isRefreshing: false,
			deviceList:null,
			listHidden: [],
			isHidden: true
		}
	}

	componentDidMount() {
		this.requestDeviceList()

		this.deviceStatusNoti = DeviceEventEmitter.addListener(NotificationKeys.kDeviceStatusNotification,()=>{
            this.requestDeviceList()
            this.props.refreshFilter && this.props.refreshFilter()
        })

        //设备状态 websocket 变化通知
        this.websocketNoti = DeviceEventEmitter.addListener(NotificationKeys.kWebsocketDeviceNotification,(data)=>{
            if(!data) return
            this.updateDeviceStatus(data)
        })
	}

	componentWillUnmount(){
        this.deviceStatusNoti.remove()
        this.websocketNoti.remove()
	}

	async requestDeviceList(params){
		params = params || {}
        
		if(this.props.roomId){
			params.roomId = this.props.roomId
        }
        // 自然分类
        if(this.props.naturalClassification && this.props.naturalClassification.length){
            params.naturalClassification = this.props.naturalClassification.toString()
        }
		this.setState({loading:true})
		try {
			let data = await postJson({
				url: NetUrls.homeCustomerList,
				params: {
                    ...params,
                    isSort: 1,
                    showRobot: 1,
					type: 1 // 1设备 2场景 3智能 
				}
			});
			this.setState({loading: false,isRefreshing: false})
			if (data.code == 0) {
				let deviceList = data.result || []
				this.setState({
					deviceList: deviceList,
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({loading: false,isRefreshing: false})
			ToastManager.show('网络错误')
		}
    }

    // 更新设备状态
    updateDeviceStatus(data){
        let deviceList = this.state.deviceList
        if(!deviceList || deviceList.length <= 0) return
        if(this.state.loading || this.state.isRefreshing) return
        if(this.updating) return

        this.updating = true
        let newDeviceList = UpdateDeviceList(deviceList, data)
        if(newDeviceList){
            this.setState({deviceList: newDeviceList})
        }
        this.updating = false
    }

    onRefresh(){
        if (this.state.isRefreshing) return
        this.setState({ isRefreshing: true })
        this.requestDeviceList()
        DeviceEventEmitter.emit(NotificationKeys.kHouseInfoNotification)
    }

	// 渲染设备的每一行
	renderRowItem(rowData, index) {
		if(this.props.limitCount && index >= this.props.limitCount) {
			return null
		}
		return(
			<DeviceListItem
                numColumns={this.props.numColumns}
				rowData={rowData} 
				index={index} 
				navigation={this.props.navigation}
				updateDeviceCallBack = {(rowData)=>{
					this.state.deviceList[index] = rowData
                    this.setState({
                        ...this.state
                    })
                }}
                collectClick={(collect)=>{
                    this.state.deviceList[index].isCollect = collect
                    this.setState({
                        ...this.state
                    })
                }}
                isDark={this.props.isDark}
			/>
		)
    }
    
    // 无内容
    renderNoContent(){
        const Images = this.props.isDark ? ImagesDark : ImagesLight
        return(
            <View style={{alignItems:'center'}}>
				<ListNoContent
					style={{alignItems: 'center'}}
					img={Images.noDevice} 
				/>	
				<TouchableOpacity style={[styles.moreTouch,{marginTop:5}]} onPress={()=>{
					const {navigate} = this.props.navigation
					navigate('Device',{
                        title:'全部设备',
                        isJuniorUser: this.props.isJuniorUser
                    })
				}}>
					<Text style={{fontSize:14,color:Colors.themeTextLightGray}}>全部设备</Text>
					<Image style={styles.arrow} source={require('../../../images/enterLight.png')}/>
				</TouchableOpacity>
			</View>
        )
    }

	_devicesExtraUniqueKey(item, index){
		return "devices_index_" + index;
	}

	// 设备列表
	getListView(){
		if(this.state.loading && !this.state.deviceList){
			return <ListLoading/>
		}
		if(!this.state.deviceList){
			return (
                <ListError onPress={()=>{ 
                    this.requestDeviceList()
                    DeviceEventEmitter.emit(NotificationKeys.kHouseInfoNotification)
                }}/>
            )
		}
		Footer = this.state.deviceList.length > 0 ? FooterEnd: null

		return (
			<View style={{ flex: 1}}>
				<FlatList
					contentContainerStyle={{ paddingBottom: 20}}
					scrollIndicatorInsets={{right: 1}}
					removeClippedSubviews={false}
					data={this.state.deviceList}
					keyExtractor={this._devicesExtraUniqueKey}
					renderItem={({ item, index }) => this.renderRowItem(item, index)}
					initialNumToRender={10}
                    ListFooterComponent={Footer}
                    ListEmptyComponent = {this.renderNoContent()}
                    numColumns={this.props.numColumns}
                    key={this.props.numColumns}
                    refreshControl={
                        <RefreshControl
                            colors={[Colors.themeBGInactive]}
                            tintColor={Colors.themeBGInactive}
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
				/>
			</View>
		)
	}

	render() {
		return this.getListView()
	}
}

const styles = StyleSheet.create({
	deviceIcon:{
		width:60,
		height:60,
		resizeMode:'contain'
	},
	statusTextWrapper:{
		height:30,
		borderRadius:15,
		justifyContent:'center',
		alignItems:'center',
		marginRight:10,
		paddingHorizontal: 15,
	},
	icon:{
        width: 40, 
        height: 40, 
        marginLeft: 20, 
        resizeMode:'contain'
	},
	moreTouch:{
		borderColor:Colors.borderLightGray,
		borderWidth:1,
		width:120,
		height:30,
		borderRadius:15,
		justifyContent:'center',
		alignItems:'center',
		flexDirection:'row'
	},
	arrow:{
		width:6,
		height:11,
		marginLeft:10,
		resizeMode:'contain'
	}
})

export default NewDeviceList

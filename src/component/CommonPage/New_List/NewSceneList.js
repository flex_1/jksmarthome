/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	DeviceEventEmitter,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	FlatList,
	Alert,
	RefreshControl,
	SwipeableFlatList,
	Dimensions,
	ImageBackground
} from 'react-native';
import { Colors, NotificationKeys, NetUrls } from '../../../common/Constants';
import { postJson } from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import {ListNoContent,FooterEnd,ListLoading,ListError } from "../../../common/CustomComponent/ListLoading";
import SceneListitem from "../ListItems/SceneListItem";
import {ImagesLight, ImagesDark} from '../../../common/Themes';

class SceneList extends Component {

	constructor(props) {
		super(props);
		this.state = {
			loading: true,
			isRefreshing: false,
			scenes: null
		}
	}

	componentDidMount() {
		this.getScene(1)
		this.sceneInfoNoti = DeviceEventEmitter.addListener(NotificationKeys.kSceneInfoNotification, () => {
			this.getScene()
		})
	}

	componentWillUnmount() {
		this.sceneInfoNoti.remove()
	}

	// 获取场景列表
	async getScene(isSort) {
		let params = {}
		if(this.props.roomId){
			params.roomId = this.props.roomId
		}
		try {
			let data = await postJson({
				url: NetUrls.homeCustomerList,
				params: {
					...params,
					isSort: isSort ? 1:0,
					type: 2 // 1设备 2场景 3智能 
				}
			});
			this.setState({
				loading: false,
				isRefreshing: false,
			})
			if (data.code == 0) {
				let sceneList = data.result || []
				this.setState({
					scenes: sceneList,
					isRefreshing: false
				})
			} else {
				this.setState({ isRefreshing: false })
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({ 
				isRefreshing: false,
				loading: false
			})
			ToastManager.show('网络错误');
		}
	}

	// 删除场景
	async deleteScene(sceneId) {
		try {
			let data = await postJson({
				url: NetUrls.delScene,
				params: {
					id: sceneId
				}
			});
			if (data && data.code == 0) {
				DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
				DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
				DeviceEventEmitter.emit(NotificationKeys.kSceneFloorNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误');
		}
    }
    
    onRefresh(){
        if (this.state.isRefreshing) return
		this.setState({ isRefreshing: true })
        this.getScene()
        DeviceEventEmitter.emit(NotificationKeys.kHouseInfoNotification)
    }

	_extraUniqueKey(item, index) {
		return "roomInfo_index_" + index;
	}

	// 渲染每一行
	renderRowItem(rowData, index) {
		if(this.props.limitCount && index >= this.props.limitCount) {
			return null
		}

		return(
            <SceneListitem
                numColumns = {this.props.numColumns}
                rowData={rowData} 
                index={index} 
                navigation={this.props.navigation}
                isJuniorUser={this.props.isJuniorUser}
                collectClick={(collect)=>{
                    this.state.scenes[index].isCollect = collect
                    this.setState({
                        ...this.state
                    })
                }}
                isDark={this.props.isDark}
            />
		)
    }
    
    // 无内容
    renderNoContent(){
        const Images = this.props.isDark ? ImagesDark : ImagesLight
        return(
            <View style={{alignItems:'center'}}>
				<ListNoContent
					style={{alignItems: 'center'}}
					img={Images.noScene} 
				/>
				<TouchableOpacity style={[styles.moreTouch,{marginTop:5}]} onPress={()=>{
					const {navigate} = this.props.navigation
					navigate('Scene',{title:'全部场景'})
				}}>
					<Text style={{fontSize:14,color:Colors.themeTextLightGray}}>全部场景</Text>
					<Image style={styles.arrow} source={require('../../../images/enterLight.png')}/>
				</TouchableOpacity>
			</View>  
        )
    }

	//场景列表
	getSceneList() {
		let list = this.state.scenes

		if (this.state.loading && !list) {
			return <ListLoading/>
		}
		if (!list) {
            return (
                <ListError onPress={()=>{ 
                    this.getScene()
                    DeviceEventEmitter.emit(NotificationKeys.kHouseInfoNotification)
                }}/>
            )
        }
        Footer = list.length > 0 ? FooterEnd: null
		
		return (
			<View style={{ flex: 1 }}>
				<FlatList
					scrollEnabled={this.props.scrollable}
					contentContainerStyle={{ paddingBottom: 20 }}
					scrollIndicatorInsets={{right: 1}}
					removeClippedSubviews={false}
					data={list}
					keyExtractor={this._extraUniqueKey}
					renderItem={({ item, index }) => this.renderRowItem(item, index)}
                    ListFooterComponent={Footer}
                    ListEmptyComponent = {this.renderNoContent()}
                    initialNumToRender={10}
                    numColumns={this.props.numColumns}
                    key={this.props.numColumns}
                    refreshControl={
                        <RefreshControl
                            colors={[Colors.themeBGInactive]}
                            tintColor={Colors.themeBGInactive}
                            refreshing={this.state.isRefreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
				/>
			</View>
		)
	}

	render() {
		return this.getSceneList()
	}
}

const styles = StyleSheet.create({
	navTitle: {
		fontSize: 24,
		fontWeight: 'bold',
		marginLeft: 15,
		textAlignVertical: 'center',
		color: Colors.themeTextBlack,
		flex: 1
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight: 10
	},
	navImg: {
		width: 17,
		height: 17
	},
	noSecens: {
		justifyContent: 'center',
		alignItems: 'center',
		flex: 1
	},
	moreTouch:{
		borderColor:Colors.borderLightGray,
		borderWidth:1,
		width:120,
		height:30,
		borderRadius:15,
		justifyContent:'center',
		alignItems:'center',
		flexDirection:'row'
	},
	arrow:{
		width:6,
		height:11,
		marginLeft:10,
		resizeMode:'contain'
    }
})

export default SceneList

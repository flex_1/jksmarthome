/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	DeviceEventEmitter,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	FlatList,
	Alert,
	RefreshControl,
	SwipeableFlatList,
	Dimensions,
	ImageBackground
} from 'react-native';
import { Colors, NotificationKeys, NetUrls, NetParams } from '../../../common/Constants';
import { postJson } from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import {ListNoContent,FooterEnd,ListLoading,ListError } from "../../../common/CustomComponent/ListLoading";
import SmartListItem from "../ListItems/SmartListItem";
import {ImagesLight, ImagesDark} from '../../../common/Themes';

class NewSmartList extends Component {

	constructor(props) {
		super(props);
		this.state = {
			samrtList: null,
			loading: true
		}
	}

	componentDidMount() {
		this.requestSmartList()

		this.smartListNoti = DeviceEventEmitter.addListener(NotificationKeys.kSmartListChangeNotification,()=>{
            this.requestSmartList()
        })
	}

	componentWillUnmount() {
		this.smartListNoti.remove()
	}

	// 获取所有智能列表
    async requestSmartList() {
		let params = {}
		if(this.props.roomId){
			params.roomId = this.props.roomId
		}
        try {
            let data = await postJson({
                url: NetUrls.homeCustomerList,
                params: {
					...params,
					type: 3 // 1设备 2场景 3智能 
                }
            });
            this.setState({
                loading: false,
                isRefreshing: false
            })
            if (data.code == 0) {
				let samrtList = data.result || []
                this.setState({
                    samrtList: samrtList
				})
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({
                loading: false,
                isRefreshing: false
            })
            ToastManager.show('网络错误')
        }
    }
    
    onRefresh(){
        if (this.state.isRefreshing) return
		this.setState({ isRefreshing: true })
        this.requestSmartList()
        DeviceEventEmitter.emit(NotificationKeys.kHouseInfoNotification)
    }

	_extraUniqueKey(item, index) {
		return "smart_list_index_" + index;
    }
    
    renderRowItem(rowData, index){
		if(this.props.limitCount && index >= this.props.limitCount) {
			return null
		}

        return(
            <SmartListItem 
                rowData={rowData} 
                index={index} 
                navigation={this.props.navigation}
                isDark={this.props.isDark}
            />
        )
    }
    
    // 无内容
    renderNoContent(){
        const Images = this.props.isDark ? ImagesDark : ImagesLight
        return(
            <View style={{alignItems:'center'}}>
                <ListNoContent
                    style={{alignItems: 'center'}}
                    img={Images.noSmart} 
                />
                <TouchableOpacity style={[styles.moreTouch,{marginTop:5}]} onPress={()=>{
                    const {navigate} = this.props.navigation
                    navigate('Smart')
                }}>
                    <Text style={{fontSize:14,color:Colors.themeTextLightGray}}>全部智能</Text>
                    <Image style={styles.arrow} source={require('../../../images/enterLight.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

	getSmartListView() {
        let list = this.state.samrtList

		if (this.state.loading && !list) {
			return <ListLoading/>
		}
		if (!list) {
			return (
                <ListError onPress={()=>{ 
                    this.requestSmartList()
                    DeviceEventEmitter.emit(NotificationKeys.kHouseInfoNotification)
                }}/>
            )
        }
        Footer = list.length > 0 ? FooterEnd: null
    
        return (
            <FlatList
				scrollEnabled={this.props.scrollable}
                contentContainerStyle={{ paddingBottom: 50 }}
                scrollIndicatorInsets={{ right: 1 }}
                removeClippedSubviews={false}
                data={list}
                keyExtractor={this._extraUniqueKey}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
				initialNumToRender={5}
                ListFooterComponent={Footer}
                ListEmptyComponent = {this.renderNoContent()}
                refreshControl={
                    <RefreshControl
                        colors={[Colors.themeBGInactive]}
                        tintColor={Colors.themeBGInactive}
                        refreshing={this.state.isRefreshing}
                        onRefresh={this.onRefresh.bind(this)}
                    />
                }
            />
        )
	}
	
	render() {
		return this.getSmartListView()
	}
}

const styles = StyleSheet.create({
	navTitle: {
		fontSize: 24,
		fontWeight: 'bold',
		marginLeft: 15,
		textAlignVertical: 'center',
		color: Colors.themeTextBlack,
		flex: 1
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight: 10
	},
	navImg: {
		width: 17,
		height: 17
	},
	moreTouch:{
		borderColor:Colors.borderLightGray,
		borderWidth:1,
		width:120,
		height:30,
		borderRadius:15,
		justifyContent:'center',
		alignItems:'center',
		flexDirection:'row'
	},
	arrow:{
		width:6,
		height:11,
		marginLeft:10,
		resizeMode:'contain'
	}
})

export default NewSmartList
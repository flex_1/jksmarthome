/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	Dimensions,
    Modal,
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import Slider from "react-native-slider";
import Panel from './Panel';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import TrackImageSlider from '../../../common/CustomComponent/TrackImageSlider';
import MultiSlider from '@ptomasroos/react-native-multi-slider';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const sliderWidth = (screenW - 40) * 0.9

class DoubleLightPanel extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		
		this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            lightVal: 0,
            tempVal: 10,

            pickerData:[
				['立即','1s','2s','3s','4s','5s','6s','7s','8s','9s','10s'],
				['10%','20%','30%','40%','50%','60%','70%','80%','90%','100%',]
            ],
            lightTime: 0,
			lightPercent: '100%',
            
            delayModal: false,
            currentColor: '#254FD7',
            colorTempModalVisible: false,

            minColorTemp: 1000,
            maxColorTemp: 10000,
            colorMin: 1000,
            colorMax: 10000,
		}
	}

	componentDidMount() {
		super.componentDidMount()
		this.requestDeviceStatus()
	}

	componentWillUnmount() {
        super.componentWillUnmount()
    }

    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        this.setState({
            lightVal: Math.floor(data.light) || 0,
            tempVal: Math.floor(data.cct) || 0,
        })
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				let res = data.result?.deviceStatus
				let lightTime = data.result?.lightTime
                let lightPercent = data.result?.lightTimePercent
                let cctScope = data.result.cctScope || ''
                let minColorTemp = cctScope.split('-')[0] || 1000
                let maxColorTemp = cctScope.split('-')[1] || 10000

				try {
					res = JSON.parse(res)
                    let cct = parseInt(res.cct) || 0
                    if((cct * 100) > parseInt(maxColorTemp)){
                        cct = parseInt(maxColorTemp)/100
                    }else if((cct * 100) < parseInt(minColorTemp)){
                        cct = parseInt(minColorTemp)/100
                    }
					this.setState({
                        isCollect: data.result?.isCollect,
                        lightVal: Math.floor(res.light) || 0,
                        tempVal: cct,
						lightTime: lightTime || 0,
                        lightPercent: lightPercent+'%' || '100%',
                        minColorTemp: parseInt(minColorTemp),
                        maxColorTemp: parseInt(maxColorTemp)
					})
				} catch (error) {
					ToastManager.show('数据异常')
				}
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}
    
    // 灯光 控制
    async controlDevice(params,callBack){
        params = params || {}
        params.light = (params.light == null) ? this.state.lightVal : params.light
        params.cct = (params.cct == null) ? this.state.tempVal * 100 : params.cct * 100

		try {
			let data = await postJson({
				url: NetUrls.controlCommon, 
				timeout: NetParams.networkUserReactTimeoutTime,
				params: {
                    id : this.deviceData.deviceId,
                    ...params
                }
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

	// 修改灯光时间
	async updateLightTime(lightTime,lightPercent){
        SpinnerManager.show()
        let percent = lightPercent.replace('%','')
		try {
			let data = await postJson({
				url: NetUrls.saveDevice,
				params: {
					id: this.deviceData && this.deviceData.deviceId,
					lightTime: lightTime,
					lightTimePercent: percent
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                this.setState({
					lightTime: lightTime,
					lightPercent: lightPercent
				})
				ToastManager.show('设置成功')
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    // 设置范围
    async setDeviceProperties(params,callBack){
        params = params || {}
        params.deviceId = this.deviceData.deviceId
        params.type = 1
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.setDeviceProperties,
				params: params
            });
            SpinnerManager.close()
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    // 滚动结束
	_complete = ()=>{
		this.controlDevice()
    }
    
    // 色温
    _onTempChange = (value) => {
        this.setState({
            tempVal: Math.floor(value),
        })
    }

    // 亮度
	_onChange = (value) => {
        this.setState({
            lightVal: Math.floor(value),
        })
    }

    // 色温
    tempValueClick(value) {
        this.controlDevice({ cct: value },()=>{
            this.setState({
                tempVal: value
            })
        })
    }

    // 亮度
    valueClick(value) {
        this.controlDevice({ light: value },()=>{
            this.setState({
                lightVal: value
            })
        })
    }

    renderDoubleSlider(){
        return(
            <View style={styles.colorTempSliderWrapper}>
                <MultiSlider
					values={[this.state.colorMin, this.state.colorMax]}
					min={1000}
                    max={10000}
                    step={100}
					onValuesChange={(values) => {
						this.setState({
							colorMin: values[0],
							colorMax: values[1]
						})
					}}
					onValuesChangeFinish={(values) => {
						this.setState({
							colorMin: values[0],
							colorMax: values[1]
						})
					}}
					pressedMarkerStyle={{}}
					isMarkersSeparated={true}
					selectedStyle={{
						backgroundColor: Colors.newTheme,
						height: 20,
					}}
				    unselectedStyle={{
						backgroundColor: Colors.themeBGInactive,
						height: 20,
					}}
					customMarkerLeft={CustomMarkerLeft}
					customMarkerRight={CustomMarkerRight}
				/>
			</View>
        )
    }

    // 数字弹框
    renderColorTempModel(){
        return(
            <Modal 
                transparent={true}
                visible={this.state.colorTempModalVisible}
                animationType={'fade'}
            >
                <TouchableOpacity
                    activeOpacity={1}
                    style={{flex: 6,backgroundColor:Colors.translucent}}
                    onPress={()=>{
                        this.setState({colorTempModalVisible: false})
                    }}
                />
                <View style={styles.colorTempModalWrapper}>
                    <Text style={styles.colorTempTitle}>自定义色温值</Text>
                    {this.renderDoubleSlider()}
                    <TouchableOpacity style={styles.colorTempSaveTouch} onPress={()=>{
                        let value = this.state.colorMin + '-' + this.state.colorMax

                        this.setDeviceProperties({value: value}, ()=>{
                            this.setState({colorTempModalVisible: false})
                            this.requestDeviceStatus()
                        })
                    }}>
                        <Text style={{fontSize: 15,color: Colors.white}}>保存</Text>
                    </TouchableOpacity>
                </View>
            </Modal>
        )
    }
    
    // 灯光延迟选择器
    renderDelayModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.delayModal}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={styles.modalTouchable} 
                    onPress={() => {
                        this.setState({
                            delayModal: false
                        }, () => {
                         window.DoubulePicker.hide()
                        })
                    }}
                >
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        let selectedValue = this.state.lightTime ? 
            [this.state.lightTime + 's', this.state.lightPercent] : ['立即',this.state.lightPercent]

        window.DoubulePicker.init({
            pickerData: this.state.pickerData,
            pickerConfirmBtnText: '确定',
            pickerCancelBtnText: '取消',
            pickerTitleText: '灯光渐变时间',
            pickerCancelBtnColor: [38, 80, 215, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: selectedValue,
            onPickerConfirm: (data,index) => {
                this.setState({
                    delayModal: false
				})
				this.updateLightTime(index[0],data[1])
            },
            onPickerCancel: data => {
                this.setState({
                    delayModal: false
                })
            }
        });
        window.DoubulePicker.show()
    }

    // 渲染灯光页面
    renderLight(){
        const Colors = this.props.themeInfo.colors

        let delayText = this.state.lightTime ? this.state.lightTime+'s' : '立即'
        let percentText = this.state.lightPercent
        
        return(
            <View style={styles.topWrapper}>
                <View style={styles.lightContain}>
                    <View style={[styles.lightView_large,{backgroundColor: this.state.currentColor}]}/>
                    <View style={[styles.lightView_middle,{backgroundColor: this.state.currentColor}]}/>
                    <View style={[styles.lightView,{backgroundColor: this.state.currentColor}]}>
                        <Image style={styles.lightImg} source={require('../../../images/panel/color_light_white.png')}/>
                    </View>
                </View>
                
                <TouchableOpacity activeOpacity={0.7} style={styles.delayTouch} onPress={()=>{
                    this.setState({ delayModal: true })
                }}>
                    <Text style={styles.delayText}>{delayText +'  '+ percentText}</Text>
                    <Image style={styles.downIcon} source={require('../../../images/deviceIcon/down.png')}/>
                </TouchableOpacity>
                
            </View>
        )
    }

    // 色温控制
    renderColorTempController(){
        const Colors = this.props.themeInfo.colors
        let tempValue = this.state.tempVal / 10.0

        let min = this.state.minColorTemp / 100
        let max = this.state.maxColorTemp / 100

        let btnsArr = [min/10.0, (min+max)/20.0, max/10.0]
        let btnLabelArr = ['暖光','自然光','冷白光']
        let btns = btnsArr.map((value, index)=>{
            return(
                <TouchableOpacity key={'btns_'+index} style={styles.quickBtn} activeOpacity={0.7} onPress={()=>{
                    this.tempValueClick( parseInt(value * 10) )
                }}>
                    {/* <View style={[styles.shortLine,{backgroundColor: Colors.themeTextLight}]}/> */}
                    <Text style={[styles.quickText,{color: Colors.themeTextLight}]}>{btnLabelArr[index]}</Text>
                </TouchableOpacity>
            )
        })

        return (
            <View style={styles.sliderWrapper}>
                <View style={styles.title}>
                    <Text style={[styles.lightText,{color: Colors.themeText}]}>色温</Text>
                    <Text style={[styles.valueText,{color: Colors.themeText}]}>{tempValue}K</Text>
                    <TouchableOpacity style={styles.adjustTouch} onPress={()=>{
                        this.setState({
                            colorMin: this.state.minColorTemp,
                            colorMax: this.state.maxColorTemp,
                            colorTempModalVisible: true
                        })
                    }}>
                        <Image style={styles.adjustImg} source={require('../../../images/panel/adjust.png')}/>
                    </TouchableOpacity>
                </View>
                <TrackImageSlider
                    style={{marginTop: 10}}
                    value={this.state.tempVal}
                    sliderWidth={sliderWidth}
                    minimumValue={min}
                    maximumValue={max}
                    thumbTintColor={'#f5e85e'}
                    thumbWidth={30}
                    trackImage={require('../../../images/slider.png')}
                    onValueChange={this._onTempChange}
                    onSlidingComplete={this._complete}
                />
                <View style={styles.quickBtnWrapper}>
                    {btns}
                </View>
            </View>
        )
    }

    // 调光控制器
    renderLightController(){
        const Colors = this.props.themeInfo.colors
        let lightValue = this.state.lightVal

        let btnsArr = [0, 25, 50, 75, 100]
        let btns = btnsArr.map((value, index)=>{
            return(
                <TouchableOpacity key={'l_btns_'+index} style={[styles.quickBtn,{width: 28}]} activeOpacity={0.7} onPress={()=>{
                    this.valueClick(value)
                }}>
                    <View style={[styles.shortLine,{backgroundColor: Colors.themeTextLight}]}/>
                    <Text style={[styles.quickText,{color: Colors.themeTextLight}]}>{value}</Text>
                </TouchableOpacity>
            )
        })

        return (
            <View style={styles.sliderWrapper}>
                <View style={styles.title}>
                    <Text style={[styles.lightText,{color: Colors.themeText}]}>亮度</Text>
                    <Text style={[styles.valueText,{color: Colors.themeText}]}>{lightValue}%</Text>
                </View>
                <Slider
					style={{width:'90%'}}
					minimumValue={0}
					maximumValue={100}
					step={1}
          			value={this.state.lightVal}
					onValueChange={this._onChange}
					onSlidingComplete={this._complete}
					minimumTrackTintColor='#254FD7'
					maximumTrackTintColor={Colors.themeInactive}
					thumbTintColor='#254FD7'
					trackStyle={{height:8,borderRadius: 4}}
					thumbStyle={{width:30,height:30,borderRadius:15}}
        		/>
                <View style={styles.quickBtnWrapper}>
                    {btns}
                </View>
            </View>
        )
    }

    renderController(){
        return(
            <View style={styles.bottomCtr}>
                {this.renderColorTempController()}
                {this.renderLightController()}
            </View>
        )
    }

	getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

		return(
            <View style={[styles.main,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderLight()}
                {this.renderController()}
                {this.renderDelayModal()}
                {this.renderColorTempModel()}
            </View>
		)
	}
}

class CustomMarkerLeft extends React.Component {
    
	render() {
        let value = this.props.currentValue / 1000
		return (
			<View style={{ alignItems: 'center', marginBottom: 32 }}>
				<Text style={{color:'#25cad2'}}>{value}K</Text>
				<Image style={[styles.maker,{tintColor: '#25cad2'}]} source={require('../../../images/settingIcon/maker_left.png')}/>
			</View>
		)
	}
}

class CustomMarkerRight extends React.Component {
	render() {
        let value = this.props.currentValue / 1000
		return (
			<View style={{ alignItems: 'center', marginBottom: 32 }}>
				<Text style={{color:'#2650D7'}}>{value}K</Text>
				<Image style={[styles.maker,{tintColor: '#2650D7'}]} source={require('../../../images/settingIcon/maker_right.png')}/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
    },
	topWrapper:{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    title:{
        flexDirection: 'row',
        justifyContent:'center',
        alignItems: 'center',
    },
    lightText:{
        fontSize: 15,
        marginTop: 5
    },
    valueText:{
        fontSize: 30,
        fontWeight: 'bold',
        marginLeft: 10
    },
    delayTouch:{
        flexDirection: 'row',
        justifyContent:'center',
        alignItems: 'center',
        paddingVertical: 5,
        paddingHorizontal: 50,
        marginTop: 15,
    },
    delayText:{
        fontSize: 13,
        color: Colors.themeTextLightGray
    },
    downIcon:{
        width: 9,
        height: 5,
        resizeMode: 'contain',
        marginLeft: 10
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: 'rgba(0,0,0,0.5)' 
    },
    lightContain:{
        width: '100%',
        height: screenW*0.4,
    },
    lightView_large:{
        position: 'absolute',
        alignSelf: 'center',
        opacity: 0.2,
        width: screenW*0.4,
        height: screenW*0.4,
        borderRadius: screenW*0.2,
    },
    lightView_middle:{
        position: 'absolute',
        alignSelf: 'center',
        opacity: 0.4,
        top: screenW*0.05,
        width: screenW*0.3,
        height: screenW*0.3,
        borderRadius: screenW*0.15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    lightView:{
        position: 'absolute',
        alignSelf: 'center',
        top: screenW*0.1,
        width: screenW*0.2,
        height: screenW*0.2,
        borderRadius: screenW*0.1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    lightImg:{
        width: screenW*0.10,
        height: screenW*0.10,
        resizeMode: 'contain'
    },
    sliderWrapper:{
        width: '100%',
        paddingHorizontal: 20,
        justifyContent:'center',
        alignItems: 'center',
        marginTop: 20
    },
    bottomCtr:{
        paddingBottom: BottomSafeMargin + 0.1*screenH
    },
    btns:{
        width:30,
        height:30,
        borderRadius:15,
        marginLeft:15
    },
    colorImg:{
        width:30,
        height:30,
        resizeMode:'contain'
    },
    quickBtnWrapper:{
        width: '90%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    quickBtn:{
        justifyContent:'center',
        alignItems:'center',
        marginTop: 10
    },
    quickText:{
        marginTop: 5,
        fontSize: 15
    },
    shortLine:{
        width: 2,
        height: 8
    },
    adjustTouch:{
        marginLeft:20,
        alignItems:'center',
        justifyContent:'center',
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    adjustImg:{
        width:22,
        height:22,
        resizeMode:'contain'
    },
    maker: {
		width: 25,
		height: 15,
		backgroundColor: 'transparent',
		resizeMode: 'contain'
    },
    colorTempModalWrapper:{
        backgroundColor: Colors.white,
        flex: 4,
        paddingBottom: BottomSafeMargin,
        alignItems:'center'
    },
    colorTempSliderWrapper:{
        width: '100%',
        justifyContent:'center',
        alignItems: 'center',
        marginTop: 50
    },
    colorTempTitle:{
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.themeTextBlack,
        marginTop: 15
    },
    colorTempSaveTouch:{
        height: 44, 
        backgroundColor: Colors.tabActiveColor,
        width: '90%',
        marginTop:30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(DoubleLightPanel)



/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Animated,
	Dimensions,
    Modal,
    Keyboard,
    Alert,
    ImageBackground
} from 'react-native';
import { Colors, NetUrls } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import Panel from './Panel';
import Echarts from '../../../third/echarts/index';
import echarts from '../../../third/echarts/components/Echarts/echarts.min';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class Coulometer extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
        
        this.typeId = getParam('typeId')
        this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            deviceData: (this.deviceData && JSON.parse(this.deviceData.deviceStatus)) || {},
            chartList: null,
            isLoadingChart: true
        }
    }
    
	componentDidMount() {
        this.requestDeviceStatus()
	}

	componentWillUnmount(){
        
    }

    async requestDeviceStatus(){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
            SpinnerManager.close()
            this.setState({isLoadingChart: false})
			if (data.code == 0) {
                this.setState({
                    deviceData: JSON.parse(data.result?.deviceStatus) || {},
                    chartList: data.result?.coulombKGFChart?.list || []
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            this.setState({isLoadingChart: false})
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    async requestControlBtn(mode){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				params: {
                    deviceId: this.deviceData.deviceId,
                    mode: mode
                }
			});
            SpinnerManager.close()
			if (data.code == 0) {
                this.requestDeviceStatus()
				ToastManager.show(data.msg)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

    getBatteyImg(capacityPersent){
        if(capacityPersent == 100){
            return require('../../../images/invertor/100.png')
        }else if(capacityPersent > 90){
            return require('../../../images/invertor/90.png')
        }else if(capacityPersent > 80){
            return require('../../../images/invertor/90.png')
        }else if(capacityPersent > 70){
            return require('../../../images/invertor/80.png')
        }else if(capacityPersent > 60){
            return require('../../../images/invertor/70.png')
        }else if(capacityPersent > 50){
            return require('../../../images/invertor/60.png')
        }else if(capacityPersent > 40){
            return require('../../../images/invertor/50.png')
        }else if(capacityPersent > 30){
            return require('../../../images/invertor/40.png')
        }else if(capacityPersent > 20){
            return require('../../../images/invertor/30.png')
        }else if(capacityPersent > 10){
            return require('../../../images/invertor/20.png')
        }else if(capacityPersent > 0){
            return require('../../../images/invertor/10.png')
        }else{
            return require('../../../images/invertor/0.png')
        }
    }

    renderTopBattery(){
        const Colors = this.props.themeInfo.colors;
        let {capacityAreamin, capacityAset, currentAdir} = this.state.deviceData

        capacityAreamin = capacityAreamin || '0Ah'
        capacityAset = capacityAset || '0Ah'

        let capacityPersent = 0
        if(parseInt(capacityAset)){
            capacityPersent = (parseFloat(capacityAreamin) * 100.0)/parseFloat(capacityAset)
        }else if(capacityPersent > 100){
            capacityPersent = 100
        }
        capacityPersent = Math.floor(capacityPersent)

        const batteryImg = this.getBatteyImg(capacityPersent)

        return (
            <View style={styles.topWrapper}>
                <Text style={[styles.valueText,{color: Colors.themeTextLight}]}>容量: {capacityAreamin}/{capacityAset}</Text>
                <ImageBackground style={styles.batteryImg} source={batteryImg}>
                    <Text style={styles.centerValue}>{capacityPersent}%</Text>
                </ImageBackground>
                <View style={styles.statusWrapper}>
                    <View style={styles.line}/>
                    <Text style={styles.statusLabel}>{currentAdir}</Text>
                    <View style={styles.line}/>
                </View>
                {/* <Text style={[styles.valueText,{color: Colors.themeTextLight}]}>电压{voltage}  电流{electricity}</Text> */}
            </View>
        )
    }

    renderInfoItem(value, title){
        const Colors = this.props.themeInfo.colors;

        return(
            <View style={styles.item}>
                <Text style={[styles.itemValue,{color: Colors.themeText}]}>{value}</Text>
                <Text style={[styles.itemSubTitle,{marginTop: 5, color: Colors.themeTextLight}]}>{title}</Text>
            </View>
        )
    }

    renderDetailValuesView(){
        const Colors = this.props.themeInfo.colors;

        let {voltage, electricity, power, batteryAlife, temperature, runAtime, capacityAtotal, resistance} = this.state.deviceData

        return(
            <View style={[styles.valuesBg,{backgroundColor: Colors.themeBg}]}>
                <View style={styles.valueWrapper}>
                    {this.renderInfoItem(voltage, '实时电压')}
                    <View style={[styles.columLine,{backgroundColor: Colors.themeBaseBg}]}/>
                    {this.renderInfoItem(electricity, '实时电流')}
                    <View style={[styles.columLine,{backgroundColor: Colors.themeBaseBg}]}/>
                    {this.renderInfoItem(power, '当前功率')}
                    <View style={[styles.columLine,{backgroundColor: Colors.themeBaseBg}]}/>
                    {this.renderInfoItem(batteryAlife, '续航时间')}
                </View>
                <View style={[styles.splitLine,{backgroundColor: Colors.themeBaseBg}]}/>
                <View style={styles.valueWrapper}>
                    {this.renderInfoItem(temperature, '环境温度')}
                    <View style={[styles.columLine,{backgroundColor: Colors.themeBaseBg}]}/>
                    {this.renderInfoItem(runAtime, '工作时间')}
                    <View style={[styles.columLine,{backgroundColor: Colors.themeBaseBg}]}/>
                    {this.renderInfoItem(capacityAtotal, '累计容量')}
                    <View style={[styles.columLine,{backgroundColor: Colors.themeBaseBg}]}/>
                    {this.renderInfoItem(resistance, '电池内阻')}
                </View>
            </View>
        )
    }

    _Echart(){
        const isDark = this.props.themeInfo.isDark

		if(!this.state.chartList || this.state.isLoadingChart){
			return(
                <View style={{flex:1,justifyContent:'center',alignItems:'center',marginTop:100}}>
                    <Text style={{color:Colors.themeTextLightGray}}>数据加载中...</Text>
                </View>
            )
		}
        if(this.state.chartList.length<=0){
            return(
                <View style={{flex:1,justifyContent:'center',alignItems:'center',marginTop:100}}>
                    <Text style={{color:Colors.themeTextLightGray}}>暂无数据</Text>
                </View>
            )
		}
        let eleDataX = []
        let electricityArr = []
        let voltageArr = []
        for (const data of this.state.chartList) {
			let timeX = data.timeX
			eleDataX.push(timeX)

			let electricity = data.electricity || 0
            let voltage = data.voltage || 0
			if( typeof(electricity) == 'number' && typeof(voltage) == 'number'){
				electricityArr.push({value: electricity, time: data.dateTime})
                voltageArr.push({value: voltage, time: data.dateTime})
			}else{
				electricityArr.push(0)
                voltageArr.push(0)
			}
		}
		// 柱状图 是否可以滚动
		let dataZoom = null
		let animation = true

		let option = {
			color: isDark ? ['#2DC9EB','#B568F3'] : ['#2DC9EB','#B568F3'],
			tooltip: {
				trigger: 'axis',
				axisPointer: {
					type: 'line',
					lineStyle:{
						width: 1,
						color: Colors.borderLightGray
					}
				},
                formatter: function(data){
					let axisData1 = data[0]
                    let axisData2 = data[1]
                    return (
						axisData1.data.time
                        +'</br>电压:  '+ axisData1.data.value + 'V'
                        +'</br>电流:  '+ axisData2.data.value + 'A'
					)
                }
			},
			legend: {data: ['电流', '电压'], show:false},
			xAxis: {
				data: eleDataX,
                boundaryGap: false,
				axisLine: {
					lineStyle: {
						color: Colors.borderLightGray
					}
				},
				axisLabel: {
					textStyle:{
						color: Colors.themeTextLightGray,
					},
				},
				axisTick:{
					show: false
				},
			},
			clickable:true,
			yAxis: [{
                type: 'value',
                name: '电压(V)',
                position: 'left',
                alignTicks: true,
                axisLine: {
                    show: false,
                    lineStyle: {
                        color: '#2DC9EB'
                    }
                },
                splitLine:{
                    show: true,
                    lineStyle:{
                        color: Colors.splitLightGray
                    }
                },
                axisLabel: {
                    formatter: '{value}'
                }, 
            },
            {
                type: 'value',
                name: '电流(A)',
                position: 'right',
                alignTicks: true,
                axisLine: {
                    show: false,
                    lineStyle: {
                        color: '#B568F3'
                    }
                },
                splitLine:{
                    show: false
                },
                axisLabel: {
                    formatter: '{value}',
                    textStyle:{
						color: Colors.themeTextLightGray,
					},
                }
            }],
			// dataZoom: dataZoom,
			animation: animation,
			series: [{
				name: '电流',
				type: 'line',
                smooth: true,
                symbol: 'none',
				data: voltageArr,
                lineStyle: {
                    width: 1.5
                },
                areaStyle: {
                    color: '#2DC9EB20'
                }
            },{
                name: '电压',
                type: 'line',
                smooth: true,
                yAxisIndex: 1,
                symbol: 'none',
                data: electricityArr,
                lineStyle: {
                    width: 1.5
                },
                areaStyle: {
                    color: '#B568F320'
                }
            }]
		};
		return (
			<Echarts
				option={option} 
				height={300}
				onPress={(data)=>{
					// console.log('123');
				}}
			/>
		)
	}

    renderChart(){
        return(
            <View style={{width:'100%',marginTop: 10,paddingHorizontal: 10}}>
                {this._Echart()}
            </View>
        )
    }

    renderBottomBtn(){
        return(
            <View style={styles.bottomWrapper}>
                <TouchableOpacity style={styles.bottomTouch}>
                    <Text style={styles.btnText}>开启</Text>
                </TouchableOpacity>
            </View>
        )
    }

    getMainPanelPage(){
        const Colors = this.props.themeInfo.colors;

		return(
            <View style={[styles.wrapper,{backgroundColor: Colors.themeBg}]}>
                <ScrollView contentContainerStyle={styles.scrollContent}>
                    {this.renderTopBattery()}
                    {this.renderDetailValuesView()}
                    {this.renderChart()}
                    {/* {this.renderBottomBtn()} */}
                </ScrollView>
			</View>
		)
	}

	// render() {
    //     const Colors = this.props.themeInfo.colors;

	// 	return (
	// 		<View style={[styles.wrapper,{backgroundColor: Colors.themeBaseBg}]}>
	// 			<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
    //                 {this.renderMainController()}
    //             </View>
	// 		</View>
	// 	)
	// }
}

const styles = StyleSheet.create({
	wrapper: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
    },
    scrollContent:{
        paddingBottom: BottomSafeMargin + 50
    },
    topWrapper:{
        width: '100%',
        paddingHorizontal: 32,
        marginTop: 25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    batteryImg:{
        width: 200,
        height: 90,
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    valueText:{
        fontSize: 14,
        fontWeight:'400'
    },
    statusWrapper:{
        flexDirection: 'row',
        alignItems:'center',
        marginTop:20,
        marginBottom: 10
    },
    line:{
        height: 1,
        width: 60,
        backgroundColor: 'rgba(151, 151, 151, 0.4)' 
    },
    statusLabel:{
        color: '#3AC457',
        fontSize: 14,
        fontWeight: '500',
        marginHorizontal: 10
    },
    centerValue:{
        fontSize: 27,
        fontWeight: '500',
        color: Colors.themeTextBlack
    },
    valuesBg:{
        marginTop: 20,
        marginHorizontal: 16,
        height: screenW * 0.4,
        borderRadius: 5,
        //阴影四连
        shadowOffset: { width: 0, height: 0 },
        shadowColor: 'rgba(59,74,116,1)',
        shadowOpacity: 0.14,
        shadowRadius: 5,
        elevation: 4
    },
    splitLine:{
        width: '100%', 
        height:1
    },
    valueWrapper:{
        flexDirection: 'row',
        flex: 1
    },
    columLine:{
        height: '100%', 
        width:1
    },
    item:{
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    itemValue:{
        fontSize:13, 
        fontWeight: '500',
    },
    itemSubTitle:{
        fontSize:12, 
        fontWeight: '400',
    },
    bottomWrapper:{
        marginTop:40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    bottomTouch:{
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.newBtnBlueBg,
        height: 44,
        borderRadius: 22,
        width: '80%'
    },
    btnText:{
        color: Colors.white,
        fontWeight: '400',
        fontSize: 16
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Coulometer)



/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Alert,
    Dimensions, 
    PanResponder, 
    Animated,
    DeviceEventEmitter,
    Modal
} from 'react-native';
import { Colors, NetUrls,NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import Panel from './Panel';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import SpinnerManager from '../../../common/CustomComponent/SpinnerManager';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

const bottomControllerH = 180

class AirConditioner extends Panel {

	constructor(props) {
		super(props);
        const { getParam } = props.navigation;
        
		this.state = {
            ...this.state,
			switch: 0,

            pattern: 1, //模式标志
            speed: 4, //风速标志
            direction: 1,  //方向标志

            tempValue: 16, //温度值
            roomTemperature: 16,  //当前室温
            temperatureOffset: 0,  // 温度偏移值

            modalVisible: false,
            pickerData: [5,4,3,2,1,0,-1,-2,-3,-4,-5]
		}
	}

	componentDidMount() {
        super.componentDidMount()
        if(this.isTCP){
            this.requestStatusByTCP()
        }else{
            this.requestDeviceStatus()
        }
    }

    componentWillUnmount() {
        super.componentWillUnmount()
    }

    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        this.setState({
            switch: data.status == 1,
            pattern: Math.floor(data.pattern) || this.state.pattern,
            speed: Math.floor(data.speed) || this.state.speed,
            direction: Math.floor(data.direction) || this.state.direction,
            tempValue: Math.floor(data.temperature) || this.state.tempValue,
            roomTemperature: Math.floor(data.RoomTemperature) || this.state.RoomTemperature,
            temperatureOffset: Math.floor(data.temperatureOffset) || this.state.temperatureOffset
        })
    }

    // tcp 数据状态解析
    updateDeviceStatusByTCP(data){
        if(!data) return
        let status = data.split(',')
        let newData = {}
        for (const stu of status) {
            let key = stu.split('_')[0]
            let value = stu.split('_')[1]
            if(key == 's'){
                newData.status = parseInt(value)
            }
            if(key == 'ptn'){
                newData.pattern = parseInt(value)
            }
            if(key == 'spd'){
                newData.speed = parseInt(value)
            }
            if(key == 'dir'){
                newData.direction = parseInt(value)
            }
            if(key == 'temp'){
                newData.temperature = parseInt(value)
            }
        }
        this.setState({
            switch: newData.status == 1,
            pattern: newData.pattern || this.state.pattern,
            speed: newData.speed || this.state.speed,
            direction: newData.direction || this.state.direction,
            tempValue: newData.temperature || this.state.tempValue
        })
    }

    showSpinner(show){
        if(this.deviceData.attributeTypeName != 'midea-air-condition'){
            return
        }
        if(show){
            SpinnerManager.show()
        }else{
            SpinnerManager.close()
        }
    }

    // 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				let res = data.result && data.result.deviceStatus
				try {
					res = JSON.parse(res)
					this.setState({
                        isCollect: data.result && data.result.isCollect,
						switch: res.status == 1,
						pattern: Math.floor(res.pattern) || this.state.pattern,
                        speed: Math.floor(res.speed) || this.state.speed,
                        direction: Math.floor(res.direction) || this.state.direction,
                        tempValue: Math.floor(res.temperature) || this.state.tempValue,
                        roomTemperature: Math.floor(res.RoomTemperature) || this.state.RoomTemperature,
                        temperatureOffset: Math.floor(res.temperatureOffset) || this.state.temperatureOffset
					})
				} catch (error) {
					ToastManager.show('数据异常')
				}
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}
    
    // 空调 控制
    async controlDevice(params,callBack){
        if(this.isTCP){
            this.controlByTCP(params,callBack)
            return
        }
        params = params || {}
        params.id = this.deviceData.deviceId
        this.showSpinner(true)
		try {
			let data = await postJson({
                url: NetUrls.controlCommon,
				params: params
			});
            this.showSpinner(false)
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            this.showSpinner(false)
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 通过TCP 控制设备
    controlByTCP(params, callBack){
        let req = 'request-2-' + this.tcpDeviceType + '-' + this.tcpDeviceId + '-' + this.tcpOrder + '-' 
        if(params.hasOwnProperty('temperature')){
            req = req + '1-' + params.temperature
        }else if(params.hasOwnProperty('pattern')){
            req = req + '2-' + params.pattern
        }else if(params.hasOwnProperty('speed')){
            req = req + '3-' + params.speed
        }else if(params.hasOwnProperty('direction')){
            req = req + '4-' + params.direction
        }else if(params.hasOwnProperty('status')){
            req = req + '0-' + params.status
        }
        console.log('client-send- ' + req);
        try {
            window.client.write(req)
            callBack && callBack()
        } catch (error) {
            ToastManager.show('控制出错，请检查连接后重试。')
        }
    }
    
    // 温度控制
    _change(val){
        let target = this.state.tempValue
	    switch(val){
	        case 0:
                target = this.state.tempValue-1
                if(target < 16){
                    ToastManager.show('温度不能低于下限')
                    target = 16
                    return
                }
                this.controlDevice({temperature: target},()=>{
                    this.setState({
                        tempValue:target
                    })
                })
	            break;
            case 1:
                target = this.state.tempValue+1
                if(target > 30){
                    ToastManager.show('温度不能高于上限')
                    target = 30
                    return
                }
                this.controlDevice({temperature: target},()=>{
                    this.setState({
                        tempValue:target
                    })
                })
                break;
        }
    }

    //模式控制
    patternControl(){
        let target = 1
        let current  = this.state.pattern

        if(this.deviceData && this.deviceData.attributeTypeName == 'midea-air-condition'){
            target = current % 4 + 1
        }else{
            if(current == 1){
                target = 8
            }else if(current == 8){
                target = 2
            }else if(current == 2){
                target = 9
            }else{
                target = 1
            }
        }   
        this.controlDevice({pattern: target},()=>{
            this.setState({
                pattern: target
            })
        })      
    }

    //风速控制
    speedControl(){
        let target = 1
        let current  = this.state.speed

        if(this.deviceData && this.deviceData.attributeTypeName == 'midea-air-condition'){
            target = (current + 1) % 8
        }else{
            if(current == 4){
                target = 2
            }else if(current == 2){
                target = 1
            }else if(current == 6){
                target = 4
            }else{
                target = 6
            }
        }
        this.controlDevice({speed: target},()=>{
            this.setState({
                speed: target
            })
        })
    }

    //角度控制
    directionControl(){
        let target = 1
        let current  = this.state.direction

        if(this.deviceData && this.deviceData.attributeTypeName == 'midea-air-condition'){
            target = (current + 1) % 6
        }else{
            target = (this.state.direction + 1) % 3 
        }
        this.controlDevice({direction: target},()=>{
            this.setState({
                direction: target
            })
        })
    }

    //美的空调特殊处理
    btnIconTextFormidea(key, type){
        if(key == 'pattern'){
            if(this.state.pattern == 1){
                return type == 1 ? require('../../../images/panel/airConditioner/aircond_chushi_icon.png') : '除湿'
            }else if(this.state.pattern == 2){
                return type == 1 ? require('../../../images/panel/airConditioner/aircond_cold_icon.png') : '制冷'
            }else if(this.state.pattern == 3){
                return type == 1 ? require('../../../images/panel/airConditioner/aircond_blowing_icon.png') : '送风'
            }else{
                return type == 1 ? require('../../../images/panel/airConditioner/air_hot_icon.png') : '制热'
            }
        }else if(key == 'speed'){
            if(this.state.speed == 1){
                return type == 1 ? require('../../../images/panel/airConditioner/midea_speed_1.png') : '睡眠风'
            }else if(this.state.speed == 2){
                return type == 1 ? require('../../../images/panel/airConditioner/midea_speed_2.png') : '微风'
            }else if(this.state.speed == 3){
                return type == 1 ? require('../../../images/panel/airConditioner/midea_speed_3.png') : '低风'
            }else if(this.state.speed == 4){
                return type == 1 ? require('../../../images/panel/airConditioner/midea_speed_4.png') : '中风'
            }else if(this.state.speed == 5){
                return type == 1 ? require('../../../images/panel/airConditioner/midea_speed_5.png') : '高风'
            }else if(this.state.speed == 6){
                return type == 1 ? require('../../../images/panel/airConditioner/midea_speed_6.png') : '强风'
            }else if(this.state.speed == 7){
                return type == 1 ? require('../../../images/panel/airConditioner/midea_speed_7.png') : '超强风'
            }else{
                return type == 1 ? require('../../../images/panel/airConditioner/midea_speed_0.png') : '自动'
            }
        }else if(key == 'direction'){
            if(this.state.direction == 1){
                return type == 1 ? require('../../../images/panel/airConditioner/midea_direction_1.png') : '角度1'
            }else if(this.state.direction == 2){
                return type == 1 ? require('../../../images/panel/airConditioner/midea_direction_2.png') : '角度2'
            }else if(this.state.direction == 3){
                return type == 1 ? require('../../../images/panel/airConditioner/midea_direction_3.png') : '角度3'
            }else if(this.state.direction == 4){
                return type == 1 ? require('../../../images/panel/airConditioner/midea_direction_4.png') : '角度4'
            }else if(this.state.direction == 5){
                return type == 1 ? require('../../../images/panel/airConditioner/midea_direction_5.png') : '角度5'
            }else{
                return type == 1 ? require('../../../images/panel/airConditioner/midea_direction_0.png') : '自动'
            }
        }else{
            return null
        }
    }

    // 获取图标或文字
    getBtnIconText(key, type){
        if(this.deviceData && this.deviceData.attributeTypeName == 'midea-air-condition'){
            return this.btnIconTextFormidea(key, type)
        }

        if(key == 'pattern'){
            if(this.state.pattern == 2){
                return type == 1 ? require('../../../images/panel/airConditioner/aircond_chushi_icon.png') : '除湿'
            }else if(this.state.pattern == 8){
                return type == 1 ? require('../../../images/panel/airConditioner/air_hot_icon.png') : '制热'
            }else if(this.state.pattern == 9){
                return type == 1 ? require('../../../images/panel/airConditioner/aircond_blowing_icon.png') : '送风'
            }else{
                return type == 1 ? require('../../../images/panel/airConditioner/aircond_cold_icon.png') : '制冷'
            }
        }else if(key == 'speed'){
            if(this.state.speed == 2){
                return type == 1 ? require('../../../images/panel/airConditioner/aircond_fon_icon.png') : '中风'
            }else if(this.state.speed == 4){
                return type == 1 ? require('../../../images/panel/airConditioner/aircond_fon_icon_down.png') : '低风'
            }else if(this.state.speed == 6){
                return type == 1 ? require('../../../images/panel/airConditioner/aircond_fon_icon_auto.png') : '自动'
            }else{
                return type == 1 ? require('../../../images/panel/airConditioner/aircond_fon_icon_up.png') : '高风'
            }
        }else if(key == 'direction'){
            if(this.state.direction == 0){
                return type == 1 ? require('../../../images/panel/airConditioner/air_stop_icon.png') : '停止'
            }else if(this.state.direction == 2){
                return type == 1 ? require('../../../images/panel/airConditioner/aircond_down_icon.png') : '左右'
            }else{
                return type == 1 ? require('../../../images/panel/airConditioner/aircond_down_icon_up.png') : '上下'
            }
        }else{
            return null
        }
    }

    //获取中间模式标志符
    getCenterPatternIcon(){
        let icon = require('../../../images/panel/airConditioner/aircond_zl_icon.png')
        if(this.deviceData && this.deviceData.attributeTypeName == 'midea-air-condition'){
            if(this.state.pattern == 4){
                icon = require('../../../images/panel/airConditioner/aircond_zr_icon.jpg')
            }else if(this.state.pattern == 1){
                icon = require('../../../images/panel/airConditioner/aircond_cs_icon.jpg')
            }else if(this.state.pattern == 3){
                icon = require('../../../images/panel/airConditioner/aircond_sf_icon.png')
            }
        }else{
            if(this.state.pattern == 8){
                icon = require('../../../images/panel/airConditioner/aircond_zr_icon.jpg')
            }else if(this.state.pattern == 2){
                icon = require('../../../images/panel/airConditioner/aircond_cs_icon.jpg')
            }else if(this.state.pattern == 9){
                icon = require('../../../images/panel/airConditioner/aircond_sf_icon.png')
            }
        }
        return icon
    }

    getRotateView(rotate,index){
        let opacity = 0.5 + index/20
        if(opacity>1) opacity = 1
        return(
            <View key={'deg_index'+index} style={{width:3,height:'100%',position:'absolute',overflow:'hidden',transform: [{rotateZ:rotate}]}}>
                <View style={{width:3,height:20,backgroundColor:Colors.white,opacity:opacity}}/>
                <View style={{width:3, flex:1,backgroundColor:'transparent'}}/>
            </View>
        )
    }

    getDegreeView(number){
        if(!number || number < 0){
            return null
        }
        if(number > 58){
            number = 58
        }
        number = parseInt(number)
        let degrees = []
        let beginDeg = -170
        for(let i = 1; i<=number; i++){
            let nowDeg = beginDeg + i*6
            nowDeg = nowDeg + 'deg'
            degrees.push( this.getRotateView(nowDeg,i) )
        }
        return degrees
    }

	getCenterView(){
		return(
            <View style={styles.centerViewWrapper}>
                <View style={{width:screenW/2,height:screenW/2,justifyContent:'center',alignItems:'center'}}>
                    <View style={styles.degreeWrapper}>
                        {this.getDegreeView(this.state.tempValue)}
                    </View>
                    <View style={styles.degreeBg}/>
                </View>
                <View style={{position:'absolute',alignItems:'center',justifyContent:'center'}}>
                    <View style={{flexDirection:'row',marginLeft:10}}>
                        <Text style={{color: Colors.themeTextBlack,fontSize:35,fontWeight:'bold'}}>{this.state.tempValue+'°C '}</Text>
                    </View>
                    <Image source={this.getCenterPatternIcon()} style={{width:22,height:22,resizeMode:'contain',marginTop:10,marginLeft:5}}/>
                </View>
            </View>
		)
    }
    
    // 室温
    getRoomTemperature(){
        if(this.deviceData && this.deviceData.attributeTypeName == 'ri-air-condition'){
            return null
        }
        if(this.isTCP){
            return null
        }
        let roomTemperature = this.state.roomTemperature || '-'
        return(
            <View style={{justifyContent:'center',alignItems:'center',marginTop:20}}>
                <Text style={{color: Colors.white,fontSize:15,fontWeight:'bold'}}>当前室温： {roomTemperature + '°C '}</Text>
            </View>
        )
    }

    // 温度补偿
    getOffsetBtn(){
        if(this.deviceData && this.deviceData.attributeTypeName == 'ri-air-condition'){
            return null
        }
        if(this.deviceData && this.deviceData.attributeTypeName == 'midea-air-condition'){
            return null
        }
        if(this.isTCP){
            return null
        }
        return(
            <TouchableOpacity activeOpacity={0.7} style={styles.offsetTouch} onPress={()=>{
                this.setState({modalVisible: true})
            }}>
                <Text style={styles.offsetText}>当前温度偏移量： {this.state.temperatureOffset + '°C '}</Text>
                <Image style={styles.downIcon} source={require('../../../images/deviceIcon/down.png')}/>
            </TouchableOpacity>
        )
    }

    getCenterColdView(){
        return(
            <View style={styles.tempCrtWrapper}>
                <TouchableOpacity
                    style={{flex:1,height:'100%',justifyContent:'center',alignItems:'center'}}
                    activeOpacity={0.5}  
                    onPress={()=>{ this._change(0) }}
                >
                    <Image source={require('../../../images/panel/aricond_mus_icon.png')} style={{width:20,height:2}}></Image>
                </TouchableOpacity>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{color:Colors.white,fontSize:18,marginLeft:10}}>温度</Text>
				</View>
                <TouchableOpacity
                    style={{flex:1,height:'100%',justifyContent:'center',alignItems:'center'}}
                    activeOpacity={0.5}  
                    onPress={()=>{ this._change(1) }}
                >
                    <Image source={require('../../../images/panel/aircond_add_icon.png')} style={{width:20,height:20}}></Image>
                </TouchableOpacity>
            </View>
        )
    }

	getBottomView(){

        let switchIcon = this.state.switch ? require('../../../images/panel/redSwitch_on.png')
        : require('../../../images/panel/redSwitch_off.png')
        
		return(
			<View style={styles.bottomWrapper}>
                <View style={{flexDirection:'row'}}>
                    <TouchableOpacity  style={styles.bottomBtn} activeOpacity={0.7} onPress={()=>{
                        this.patternControl()
                    }}>
                        <Image source={this.getBtnIconText('pattern',1)} style={styles.bottomBtnImg}></Image>
                        <Text style={styles.bottomBtnText}>{this.getBtnIconText('pattern',2)}</Text>
                     </TouchableOpacity>
                    <TouchableOpacity style={styles.bottomBtn} activeOpacity={0.7} onPress={()=>{
                        this.speedControl()
                    }}>
                        <Image source={this.getBtnIconText('speed',1)} style={styles.bottomBtnImg}></Image>
                        <Text style={styles.bottomBtnText}>{this.getBtnIconText('speed',2)}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bottomBtn} activeOpacity={0.7} onPress={()=>{
                        this.directionControl()
                    }}>
                        <Image source={this.getBtnIconText('direction',1)} style={styles.bottomBtnImg}></Image>
                        <Text style={styles.bottomBtnText}>{this.getBtnIconText('direction',2)}</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    style={{marginTop: 25}}
                    onPress={()=>{
                        let target = this.state.switch ? 0:1
                        this.controlDevice({status: target},()=>{
                            this.setState({
                                switch: target
                            })
                        })
					}}
                >
					<Image style={styles.bottomBtnImg} source={switchIcon}/>
				</TouchableOpacity>
			</View>
		)
    }

    handlePickerSelectData(data,index){
        let offsetValue = data[0]
        this.controlDevice({offset: offsetValue},()=>{
            this.setState({
                temperatureOffset: offsetValue
            })
        })
    }
    
    // 温度补偿器
    renderModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                statusBarTranslucent={false}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={styles.modalTouchable} 
                    onPress={() => {
                        this.setState({
                            modalVisible: false
                        }, () => {
                            window.CustomPicker.hide()
                        })
                    }}
                >
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerData,
            pickerTitleText: '温度偏移量(℃)',
            selectedValue: [this.state.temperatureOffset],
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false,
                    selectData: data
                })
                this.handlePickerSelectData(data,index);
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                }, () => {
                    window.CustomPicker.hide()
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

    // 首页面
    getMainPanelPage(){
		return(
			<View style={styles.main}>
                <View style={styles.topWrapper}>
                    {this.getCenterView()}
                    {this.getRoomTemperature()}
                    {this.getOffsetBtn()}
                </View>
				{this.getCenterColdView()}
				{this.getBottomView()}
                {this.renderModal()}
			</View>
		)
    }
}

const styles = StyleSheet.create({
	main: {
		flex: 1,
        backgroundColor: Colors.themeBG,
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
    topWrapper: {
        flex: 1,
        justifyContent:'center',
        alignItems:'center'
    },
    downIcon:{
        width: 9,
        height: 5,
        resizeMode: 'contain',
        marginLeft: 10,
        tintColor: Colors.white
    },
    offsetTouch:{
        marginTop: 15,
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    offsetText:{
        color: Colors.white,
        fontSize:14
    },
	bottomWrapper:{
        height:bottomControllerH + BottomSafeMargin,
        paddingBottom: BottomSafeMargin,
		width:'100%',
        backgroundColor:Colors.white,
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal: 16
	},
    bottomBtn:{
        flex: 1,
        marginHorizontal: 8,
        justifyContent:'center',
        alignItems:'center',    
    },
    bottomBtnText:{
        fontSize: 15,
        marginTop: 12,
        color: Colors.themeTextBlack
    },
    tempCrtWrapper:{
        width:'80%',
        alignItems:'center',
        flexDirection:'row',
        justifyContent:'center',
        borderRadius:50,
        height:60,
        borderColor:Colors.white,
        borderWidth:1,
        alignSelf: 'center',
        marginVertical: 20
    },
    centerViewWrapper:{
        justifyContent:'center',
        alignItems:'center'
    },
    degreeWrapper:{
        width:'100%',
        height:'100%',
        backgroundColor:'rgba(255,255,255,0.3)',
        borderRadius:screenW/4,
        flexDirection: 'row',
        justifyContent:'center'
    },
    degreeBg:{
        width:screenW/2-40,
        height:screenW/2-40,
        backgroundColor:Colors.white,
        borderRadius:screenW/4-20,
        position:'absolute',
        top:20,
        left:20
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    bottomBtnImg:{
        width:45,
        height:45,
        resizeMode: 'contain'
    }
});

export default AirConditioner



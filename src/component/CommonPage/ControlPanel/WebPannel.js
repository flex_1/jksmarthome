/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    View,
} from 'react-native';
import { WebView } from 'react-native-webview';
import { connect } from 'react-redux';
import { NetUrls, NetParams} from '../../../common/Constants';
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../../CommonPage/Navigation/HeaderRight';


class WebPannel extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {icon: require('../../../images/setting.png'), onPress:()=>{
                    navigation.getParam('saveBtn')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

    constructor(props) {
        super(props);
        const {getParam, setParams} = this.props.navigation
        
        this.deviceData = getParam('deviceData') || {}
        this.deviceId = this.deviceData.deviceId
        this.h5Type = this.deviceData.h5Type

        this.state = {
            url: null
        }

        setParams({
            saveBtn: this.saveBtnClick.bind(this)
        })
    }

    saveBtnClick(){
        const { push,setParams } = this.props.navigation;

        push('DeviceSetting', { 
            deviceData: this.deviceData,
            nameCallBack: (name)=>{
                setParams({ title: name })
            }
        })
    }

    componentDidMount() {
        this.getUrl()
    }

    async getUrl(){
        const userToken = await LocalTokenHandler.get()
        const token = userToken.token
        const isDark = this.props.themeInfo.isDark ? 1:0
        const url = NetParams.baseUrl + '/admin/control?token='+token + '&h5Type=' + this.h5Type + '&id=' + this.deviceId + '&isDark=' + isDark
        
        this.setState({
            url: url
        })
    }

    getWebview(){
        if(!this.state.url){
            return null
        }

        return(
            <WebView
                startInLoadingState={true}
                style={styles.container}
                useWebKit={true}
                source={{ uri: this.state.url }}
                // source={{ uri: this.state.url, method: 'GET', headers: { 'Cache-Control':'no-cache'} }}
                // incognito={true}
            />
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.getWebview()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content:{
        fontSize: 16,
        lineHeight: 22
    }
});
 
export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(WebPannel) 

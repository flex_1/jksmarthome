/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    View
} from 'react-native';
import { WebView } from 'react-native-webview';
import { NetUrls, NetParams} from '../../../common/Constants';
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import HeaderLeft from '../../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';


class FlashLightWebPannel extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation
        
        this.deviceData = getParam('deviceData')
        this.deviceId = this.deviceData.deviceId
        this.state = {
            url: null
        }
    }

    componentDidMount() {
        this.getUrl()
    }

    componentWillUnmount() {
        
    }

    async getUrl(){
        let userToken = await LocalTokenHandler.get()
        let token = userToken.token
        let url = NetParams.baseUrl + '/admin/api/lighting/control?' + 'token='+token + '&' +'id=' + this.deviceId

        this.setState({
            url: url
        })
    }

    getWebview(){
        if(!this.state.url){
            return null
        }
        return(
            <WebView
                startInLoadingState={true}
                style={styles.container}
                useWebKit={true}
                source={{ uri: this.state.url }}
            />
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.getWebview()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content:{
        fontSize: 16,
        lineHeight: 22
    }
});

export default FlashLightWebPannel

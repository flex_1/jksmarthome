/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 窗帘
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	Dimensions,
    DeviceEventEmitter,
    TouchableOpacity
} from 'react-native';
import { Colors, NetUrls,NotificationKeys,NetParams } from '../../../common/Constants';
import Slider from "react-native-slider";
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel';
import { connect } from 'react-redux';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class CombineWindow extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		
		this.state = {
			...this.state,
            isWhiteBg: !props.themeInfo.isDark,

			leftValue: 0,
            lastLeftValue: 0,
            rightValue: 0,
            lastRightValue: 0,
            leftId: 0,
            rightId: 0,
            isConnect: true
		}
	}

	componentDidMount() {
		super.componentDidMount()
		this.requestDeviceStatus()
	}

	componentWillUnmount() {
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId == this.state.leftId){
            this.setState({
                leftValue: parseInt(data.unwind)
            })
        }
        if(data.deviceId == this.state.rightId){
            this.setState({
                rightValue: parseInt(data.unwind)
            })
        }
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				let res = data.result?.deviceStatus
				try {
					res = JSON.parse(res)
					this.setState({
                        leftValue:  parseInt(res.leftUnwind),
                        lastLeftValue: parseInt(res.leftUnwind),
                        rightValue: parseInt(res.rightUnwind),
                        lastRightValue: parseInt(res.rightUnwind),
                        leftId: res.leftId,
                        rightId: res.rightId,
                        isConnect: res.windowIsLink
					})
				} catch (error) {
					ToastManager.show('数据异常')
				}
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}
    
    // 窗帘 控制
    async controlDevice(params,callBack,failCallBack){
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
                failCallBack && failCallBack()
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    //链接设置
    async setWindowsLink(isLink){
        SpinnerManager.show()
        try {
			let data = await postJson({
				url: NetUrls.saveDevice,
				params: {
                    id: this.deviceData.deviceId,
                    windowIsLink: isLink
                }
			});
            SpinnerManager.close()
			if (data.code == 0) {
				this.setState({
                    isConnect: isLink
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    getWindowImg(dir, value){
        if(value <= 0){
            return dir ? require('../../../images/panel/combineWindow/l0.png') : require('../../../images/panel/combineWindow/r0.png')
        }else if(value <= 10){
            return dir ? require('../../../images/panel/combineWindow/l10.png') : require('../../../images/panel/combineWindow/r10.png')
        }else if(value <= 20){
            return dir ? require('../../../images/panel/combineWindow/l20.png') : require('../../../images/panel/combineWindow/r20.png')
        }else if(value <= 30){
            return dir ? require('../../../images/panel/combineWindow/l30.png') : require('../../../images/panel/combineWindow/r30.png')
        }else if(value <= 40){
            return dir ? require('../../../images/panel/combineWindow/l40.png') : require('../../../images/panel/combineWindow/r40.png')
        }else if(value <= 50){
            return dir ? require('../../../images/panel/combineWindow/l50.png') : require('../../../images/panel/combineWindow/r50.png')
        }else if(value <= 60){
            return dir ? require('../../../images/panel/combineWindow/l60.png') : require('../../../images/panel/combineWindow/r60.png')
        }else if(value <= 70){
            return dir ? require('../../../images/panel/combineWindow/l70.png') : require('../../../images/panel/combineWindow/r70.png')
        }else if(value <= 80){
            return dir ? require('../../../images/panel/combineWindow/l80.png') : require('../../../images/panel/combineWindow/r80.png')
        }else if(value <= 90){
            return dir ? require('../../../images/panel/combineWindow/l90.png') : require('../../../images/panel/combineWindow/r90.png')
        }else{
            return dir ? require('../../../images/panel/combineWindow/l100.png') : require('../../../images/panel/combineWindow/r100.png')
        }
    }
    
    renderMainWindowImg(){
        const Colors = this.props.themeInfo.colors
        const LImg = this.getWindowImg(1, this.state.leftValue)
        const RImg = this.getWindowImg(0, this.state.rightValue)
        const {leftValue, rightValue} = this.state
        let openText = '左窗打开'+leftValue+'%    右窗打开' + rightValue +'%'
        if(leftValue == rightValue){
            openText = '打开' + leftValue +'%'
        }

        return(
            <View style={styles.mainImgWrapper}>
                <View style={{flexDirection: 'row'}}>
                    <Image style={styles.windowImg} source={LImg}/>
                    <Image style={styles.windowImg} source={RImg}/>
                </View>
                <Text style={[styles.openValue,{color: Colors.themeTextLight}]}>{openText}</Text>
            </View>
        )
    }

    _renderSlider(title, value, onChange, onComplete){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={styles.sliderWrapper}>
                <Text style={[styles.leftTitle,{color: Colors.themeText}]}>{title}</Text>
                <View style={{flex: 1,marginLeft: 10}}>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={{fontSize: 13,flex: 1,color: Colors.themeTextLight}}>开合度</Text>
                        <View style={[styles.valueWrapper,{backgroundColor: Colors.themeBg}]}>
                            <Text style={{fontSize: 13,color: Colors.themeTextLight}}>{value}%</Text>
                        </View>
                    </View>
                    <Slider
					    style={{width:'100%'}}
					    minimumValue={0}
					    maximumValue={100}
					    step={1}
          			    value={value}
					    onValueChange={(val)=> onChange(val)}
					    onSlidingComplete={()=> onComplete()}
					    minimumTrackTintColor='#60BDFF'
					    maximumTrackTintColor='#CACBCC'
					    thumbTintColor='#008CFF'
					    trackStyle={{height: 6,borderRadius: 3}}
					    thumbStyle={{width: 20,height: 20,borderRadius: 10}}
        		    />
                    <View style={{flexDirection: 'row'}}>
                        <Text style={{fontSize: 13,flex: 1,color: Colors.themeTextLight}}>全关</Text>
                        <Text style={{fontSize: 13,color: Colors.themeTextLight}}>全开</Text>
                    </View>
                </View>       
            </View>
        )
    }

    _onControlDevice = (params)=>{
        this.controlDevice(params,()=>{
            // 控制成功
            this.state.lastLeftValue = this.state.leftValue
            this.state.lastRightValue = this.state.rightValue
        },()=>{
            setTimeout(() => {
                //控制失败
                this.setState({
                    leftValue: this.state.lastLeftValue,
                    rightValue: this.state.lastRightValue
                })
            }, 1000);
        })
    }

    _renderLeftSlider(){
        let value = this.state.leftValue
        const onChange = (val)=>{
            let leftValue = parseInt(val)
            let rightValue = this.state.isConnect ? parseInt(val) : this.state.rightValue

            this.setState({
                leftValue: leftValue,
                rightValue: rightValue
            })
        }
        const onComplete = ()=>{
            let params = {leftUnwind: this.state.leftValue}
            if(this.state.isConnect){
                params.rightUnwind = this.state.rightValue 
            }
            this._onControlDevice(params)
        }
        return this._renderSlider('左窗', value, onChange, onComplete)
    }

    _renderRightSlider(){
        let value = this.state.rightValue
        const onChange = (val)=>{
            let leftValue = this.state.isConnect ? parseInt(val) : this.state.leftValue
            let rightValue = parseInt(val)
            
            this.setState({
                leftValue: leftValue,
                rightValue: rightValue
            })
        }
        const onComplete = ()=>{
            let params = {rightUnwind: this.state.rightValue }
            if(this.state.isConnect){
                params.leftUnwind = this.state.leftValue 
            }
            this._onControlDevice(params)
        }
        return this._renderSlider('右窗', value, onChange, onComplete)
    }

    _renderConnectBtn(){
        const icon = this.state.isConnect ? require('../../../images/panel/combineWindow/connetActive.png')
            : require('../../../images/panel/combineWindow/connectInactive.png')
        const lineBg = this.state.isConnect ? Colors.newBtnBlueBg : '#C8C9CC'
        
        return(
            <View style={styles.connectWrapper}>
                <View style={[styles.connectLineT,{backgroundColor: lineBg}]}/>
                <TouchableOpacity style={{padding: 10}} onPress={()=>{
                    this.setWindowsLink(!this.state.isConnect)
                }}>
                    <Image style={{width: 16,height:16}} source={icon}/>
                </TouchableOpacity>
                <View style={[styles.connectLineB,{backgroundColor: lineBg}]}/>
            </View>
        )
    }

    renderControlView(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={[styles.bottomWrapper,{backgroundColor: Colors.themeBaseBg}]}>
                <View style={styles.btnsWrapper}>
                    <TouchableOpacity style={[styles.btnBg,{backgroundColor: Colors.themeBg}]} onPress={()=>{
                        this.controlDevice({number: 1})
                    }}>
                        <Image style={{width:18,height:18}} source={require('../../../images/ProtocolControl/curtain_out.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.btnBg,{backgroundColor: Colors.themeBg}]}  onPress={()=>{
                        this.controlDevice({number: 3})
                    }}>
                        <Image style={{width:18,height:18}} source={require('../../../images/ProtocolControl/curtain_pause.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.btnBg,{backgroundColor: Colors.themeBg}]} onPress={()=>{
                        this.controlDevice({number: 2})
                    }}>
                        <Image style={{width:18,height:18}} source={require('../../../images/ProtocolControl/curtain_in.png')}/>
                    </TouchableOpacity>
                </View>
                <View>
                    {this._renderLeftSlider()}
                    {this._renderConnectBtn()}
                    {this._renderRightSlider()}
                </View>
            </View>
        )
    }

	getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

		return(
			<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
                {this.renderMainWindowImg()}
                {this.renderControlView()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
		flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
    },
    mainImgWrapper:{
        justifyContent: 'center',
        alignItems:'center',
        flex: 1
    },
    windowImg:{
        width: 102, 
        height:194, 
        resizeMode:'contain'
    },
    openValue:{
        marginTop: 20,
        fontSize: 16,
        fontWeight:'400'
    },
    btnBg:{
        height:38, 
        width: 88, 
        borderRadius: 19,
        justifyContent:'center',
        alignItems:'center'
    },
    bottomWrapper:{
        marginHorizontal: 16,
        marginBottom: BottomSafeMargin + screenH * 0.1,
        marginTop:20,
        borderRadius:14,
        paddingVertical:15
    },
    leftTitle:{
        fontSize: 13,
        fontWeight:'500'
    },
    valueWrapper:{
        width:50,
        height:20,
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center'
    },
    sliderWrapper:{
        flexDirection: 'row',
        alignItems:'center',
        paddingHorizontal: 16
    },
    connectLineT:{
        width: 1,
        height: 25,
        marginTop: -25,
    },
    connectLineB:{
        width: 1,
        height: 25,
        marginBottom: -25,
    },
    btnsWrapper:{
        flexDirection: 'row',
        justifyContent:'space-around',
        marginBottom:32
    },
    connectWrapper:{
        width:26,
        marginLeft: 16,
        alignItems:'center'
    }
});


export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(CombineWindow)



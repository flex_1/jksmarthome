/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
    PanResponder,
    DeviceEventEmitter
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import HeaderLeftView from '../../../common/CustomComponent/HeaderLeftView';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import Panel from './Panel';
import { connect } from 'react-redux';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const sliderHeight = screenH * 0.45

class FloorHeating extends Panel {

	constructor(props) {
		super(props);
        const { getParam } = props.navigation;
        
        this.maxValue = 35
        this.minValue = 5
		
		this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,
            
            status: this.getDefaultValue('status') || 0,

            temperature: this.getDefaultValue('temperature') || this.minValue,
            value: this.getDefaultValue('value') || 0,
            lastValue: this.getDefaultValue('lastValue') || 0,
            roomTemp: this.getDefaultValue('roomTemp') || '-'
		}
	}

    getDefaultValue(params){
        const deviceStatus = this.deviceData?.deviceStatus
        if(!deviceStatus){
            return null
        }
        let target = null
        try {
            let device = JSON.parse(deviceStatus)
            let sliderValue = (device.temperature - this.minValue)/(this.maxValue - this.minValue) * 100.0
            if(params == 'status'){
                target = device.status
            }else if(params == 'temperature'){
                target = device.temperature
            }else if(params == 'roomTemp'){
                target = device.RoomTemperature
            }else if(params == 'value'){
                target = sliderValue
            }else if(params == 'lastValue'){
                target = sliderValue
            }
        } catch (error) {
            
        }
        return target
    }

	componentDidMount() {
        super.componentDidMount()
		this.requestDeviceStatus()
    }

    componentWillUnmount() {
        super.componentWillUnmount()
    }

    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        this.setState({
            status: data.status == 1,
            temperature: Math.floor(data.temperature) || this.state.temperature,
            roomTemp: data.RoomTemperature || '-'
        },()=>{
            let value = (this.state.temperature - this.minValue)/(this.maxValue - this.minValue) * 100.0
            this.state.value = value
            this.state.lastValue = value
            this.setSliderValue(value)
        })
    }
    
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				let res = data.result && data.result.deviceStatus
				try {
					res = JSON.parse(res)
					this.setState({
                        status: res.status == 1,
                        temperature: Math.floor(res.temperature) || this.state.temperature,
                        roomTemp: res.RoomTemperature || '-'
					},()=>{
                        let value = (this.state.temperature - this.minValue)/(this.maxValue - this.minValue) * 100.0
                        this.state.value = value
                        this.state.lastValue = value
                        this.setSliderValue(value)
                    })
				} catch (error) {
					ToastManager.show('数据异常')
				}
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 控制
    async controlDevice(params,callBack){
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 设置 进度条的值（以 100 为满刻度基准）
    setSliderValue(value){
        let position = value/100 * 1.0 * sliderHeight
        this.verticalSlider?.setPosition(position)
    }

    trimTemperature(target){
        if(!this.state.status){
            ToastManager.show('设备关闭中，无法调温')
            return
        }
        this.controlDevice({temperature: target},()=>{
            this.setState({
                temperature: target
            })
            let value = (target - this.minValue)/(this.maxValue - this.minValue) * 100.0
            this.state.value = value
            this.state.lastValue = value
            this.setSliderValue(value)
        })
    }

    renderTempView(){
        const Colors = this.props.themeInfo.colors

        let heatingText = this.state.status ? '正在加热' : ' '
        
        return(
            <View style={styles.tempView}>
                <View style={{flexDirection:'row'}}>
                    <Text style={{color: Colors.themeText,fontSize: 60,lineHeight: 64}}>{this.state.temperature}</Text>
                    <Text style={{color: Colors.themeText,fontSize: 30,lineHeight: 34}}>℃</Text>
                </View>
                <Text style={[styles.roomTempText ,{color: Colors.themeTextLight}]}>当前室温: {this.state.roomTemp}℃</Text>
                {/* <Text style={[styles.heatingText ,{color: Colors.themeButton}]}>{heatingText}</Text> */}
            </View>
        )
    }

    renderControlBtn(){
        const Colors = this.props.themeInfo.colors
        let switchIcon = this.state.status ? require('../../../images/panel/blueSwitch_on.png')
        : require('../../../images/panel/redSwitch_off.png')

        return(
            <View style={styles.bottomCtrol}>
                <TouchableOpacity
                    style={[styles.ctrButton,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        if(this.state.temperature >= this.maxValue){
                            ToastManager.show('温度已达到最大值')
                            return
                        }
                        let target = this.state.temperature + 1
                        this.trimTemperature(target)
                    }}
                >
                    <Image style={[styles.ctrImg,{tintColor: Colors.themeText}]} source={require('../../../images/panel/add_black.png')}/>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={[styles.ctrButton,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        if(this.state.temperature <= this.minValue){
                            ToastManager.show('温度已达最低值')
                            return
                        }
                        let target = this.state.temperature - 1
                        this.trimTemperature(target)
                    }}
                >
                    <Image style={[styles.ctrImg,{tintColor: Colors.themeText}]} source={require('../../../images/panel/mus_black.png')}/>
                </TouchableOpacity>
                <TouchableOpacity style={styles.ctrButton} onPress={()=>{
                    let target = this.state.status ? 0:1
                    this.controlDevice({status: target},()=>{
                        this.setState({
                            status: !this.state.status
                        })
                    })
                }}>
                    <Image style={styles.switch} source={switchIcon}/>
                </TouchableOpacity>
            </View>
        )
    }
    
    // 竖直Slider
    renderVerticalSlider(){
        return(
            <View style={styles.sliderWrapper}>
                <VerticalSlider
                    style={{marginLeft: 30,height: sliderHeight}}
                    ref={e => this.verticalSlider = e}
                    initialTop={sliderHeight}
                    sliderWidth={60}
                    sliderBg={'#F4BD5D'}
                    onPanResponderMove={(evt, gs)=>{
                        if(!this.state.status){
                            ToastManager.show('设备关闭中，无法调温')
                            return
                        }

                        let totalValue = 100  //总共有多少刻度值(按100算)
						let step = sliderHeight/totalValue
						let dy = gs.dy/step
						let moveY = parseInt(dy) * -1
                        let value = this.state.lastValue + moveY

						if(value >= totalValue) value = totalValue
                        if(value <= 0) value = 0

						this.setState({
                            value: value,
                            temperature: parseInt(value/100.0 * (this.maxValue - this.minValue)) + this.minValue
                        })
                        this.setSliderValue(value)
					}}
					onComplete={()=>{
                        if(!this.state.status){
                            ToastManager.show('设备关闭中，无法调温')
                            return
                        }
                        this.state.lastValue = Math.floor(this.state.value)
                        this.controlDevice({temperature: this.state.temperature})
					}}
                />        
            </View> 
        )
    }
    
	getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

		return(
			<View style={[styles.main,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderTempView()}
                {this.renderControlBtn()}
                {this.renderVerticalSlider()}
			</View>
		)
	}
}

class VerticalSlider extends Component {

	constructor(props) {
        super(props);
        this.sliderTopStyle = {
            width:'100%',
            backgroundColor: 'white',
            height: props.initialTop,
            borderTopLeftRadius: props.sliderWidth/2,
            borderTopRightRadius: props.sliderWidth/2
        }
	}

	componentWillMount() {
		this._panResponder = PanResponder.create({
      		onMoveShouldSetPanResponder: (evt, gestureState) => true,
      		onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
			onPanResponderGrant: () => {
				// 移动 开始
			},
			onPanResponderMove: (evt, gs) => {
				this.props.onPanResponderMove(evt, gs)
			},
			onPanResponderRelease: (evt, gs) => {
				// 移动结束
				this.props.onComplete()
			}
        });
    }
    
    setPosition(value){
        this.sliderTop.setNativeProps({
            style: {
                ...this.sliderTopStyle,
                height: sliderHeight - value
            }
        })
    }

	render(){
		return(
            <View 
                {...this._panResponder.panHandlers}
                style={{
                    width:this.props.sliderWidth,
                    borderRadius:this.props.sliderWidth/2,
                    overflow: 'hidden',
                    ...this.props.style,
                }}
            >
                <View ref={e => this.sliderTop = e} style={this.sliderTopStyle}/>
                <View 
                    style={{
                        width: '100%',
                        backgroundColor: this.props.sliderBg,
                        flex: 1,
                        borderBottomLeftRadius: this.props.sliderWidth/2,
                        borderBottomRightRadius: this.props.sliderWidth/2
                    }}
                />
            </View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1,
        paddingTop: StatusBarHeight+NavigationBarHeight
    },
   tempView: {
       marginTop: screenH* 0.1,
       marginLeft: 80
    },
    roomTempText:{
        fontSize: 15,
        marginTop: 10,
    },
    heatingText:{
        fontSize: 18,
        marginTop: 15, 
        height: 22,
        lineHeight: 22
    },
    bottomCtrol:{
        marginTop: 50,
        marginLeft: 80
    },
    ctrButton:{
        width: 70, 
        height: 70, 
        borderRadius: 35, 
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 40
    },
    switch:{
        width: '100%',
        height: '100%',
        resizeMode: 'contain'
    },
    ctrImg:{
        width: 30,
        height: 30,
        resizeMode:'contain'
    },
    sliderWrapper:{
        position: 'absolute', 
        right: 50, 
        top: StatusBarHeight+NavigationBarHeight + screenH* 0.1
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(FloorHeating)



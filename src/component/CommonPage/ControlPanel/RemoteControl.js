/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from '../../../common/CustomComponent/SpinnerManager';
import { postJson } from '../../../util/ApiRequest';
import Panel from './Panel';
import { connect } from 'react-redux';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;


class RemoteControl extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		
		this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,
			buttons: null,
            selectIds: []
		}
	}

	componentDidMount() {
		this.requestButtons()
    }

    componentWillUnmount() {
        
    }
    
    // 获取遥控器按钮
    async requestButtons(){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.getVirtualButton,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
            SpinnerManager.close()
			if (data.code == 0) {
				this.setState({
                    buttons: data.result
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 控制
    async controlDevice(params){
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				params: params
			});
			if (data.code == 0) {
				
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 修改别名
    async changeButtonOtherName(id, name){
		try {
			let data = await postJson({
				url: NetUrls.virtualButtonName,
				params: {
                    buttonId: id,
                    name: name
                }
			});
			if (data.code == 0) {
				this.requestButtons()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }
    
    getCenterView(){
        if(!this.state.buttons){
            return null
        }
        let btns = this.state.buttons.map((value,index)=>{
            let selectStyle = this.state.selectIds.includes(value.id) ? {fontWeight: 'bold', fontSize: 18} : {}
        
            return(
                <View key={index} style={styles.btnWrapper}>
                    <TouchableOpacity 
                        style={[styles.btn,{backgroundColor: value.bgcolor}]} 
                        onPress={()=>{
                            this.controlDevice({number: value.number})
                            if(!this.state.selectIds.includes(value.id)){
                                this.state.selectIds.push(value.id)
                            }
                            this.setState({
                                selectIds: this.state.selectIds
                            })
                        }}
                        onLongPress={()=>{
                            const { navigate,pop } = this.props.navigation
                            navigate('ReName',{
                                title:'设置别名',
                                name: value.name || '',
                                subTips: '按钮原始名: ' + value.primitiveName,
                                isAllowEmpty: true,
                                callBack:(name)=>{
                                    pop()
                                    this.changeButtonOtherName(value.buttonId, name)
                                }}
                            )
                        }}
                    >
                        <Text style={[styles.title,{color: value.color},selectStyle]}>{value.name}</Text>
                        {/* <Text numberOfLines={1} style={[styles.subtitle,{color: value.color}]}>({value.primitiveName})</Text> */}
                    </TouchableOpacity>
                </View>
            )
        })
        return (
            <View style={styles.rowWrapper}>
				{btns}
			</View>
        )
    }

	getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

		return(
			<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
                <ScrollView 
                    style={styles.centerWrapper} 
                    contentContainerStyle={styles.contentScroll}
                >
                    {this.getCenterView()}
                </ScrollView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1,
		paddingTop: StatusBarHeight + NavigationBarHeight
    },
    contentScroll:{
        paddingBottom: BottomSafeMargin + 0.1*screenH
    },
    centerWrapper:{
        marginTop: 20,
        width:'100%',
        paddingHorizontal: 15
    },
    rowWrapper:{
        width:'100%',
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    btnWrapper:{
        width: '33%',
        height: 55,
        marginTop: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btn:{
        justifyContent: 'center',
        alignItems: 'center',
        width: '90%',
        height: '100%',
        borderRadius: 5,
        paddingHorizontal: 5
    },
    title: {
        fontSize: 14,
        textAlign: 'center'
    },
    subtitle:{
        fontSize: 12,
        textAlign: 'center',
        marginTop: 3
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(RemoteControl)



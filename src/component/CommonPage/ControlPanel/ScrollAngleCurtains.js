/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 窗帘
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter,
	PanResponder,
	Modal
} from 'react-native';
import { Colors, NetUrls,NotificationKeys,NetParams } from '../../../common/Constants';
import Slider from "react-native-slider";
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class ScrollAngleCurtains extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		
		this.state = {
			...this.state,
			switch: 0,
            isRuning: false,
            value: 0,
            lastValue: 0,
            
            lastAngleValue: 0,
            angleValue: 0
		}
	}

	componentWillMount() {
		
	}

	componentDidMount() {
        super.componentDidMount()
		this.requestDeviceStatus()	
	}

	componentWillUnmount() {
		this.timer && clearTimeout(this.timer);
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        let value = Math.floor(data.unwind) || 0
        let angleValue = Math.floor(data.angle) || 0
		this.setState({
            value: value,
            lastValue: value,
            angleValue: angleValue
        })
    }
	
	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				let res = data.result && data.result.deviceStatus
				try {
					res = JSON.parse(res)
					this.setState({
                        isCollect: data.result && data.result.isCollect,
						value: Math.floor(res.unwind) || 0,
                        lastValue: Math.floor(res.unwind) || 0,
                        angleValue: Math.floor(res.angle) || 0,
					})
				} catch (error) {
					ToastManager.show('数据异常')
				}
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}
    
    // 窗帘 控制
    async controlDevice(params,callBack){
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

    // 卷帘开合程度
	_onChange =(value)=>{
        this.setState({
            value: Math.floor(value),
        })
	};
    
    // 卷帘开合程度
	_complete =(value)=>{
        value = Math.floor(value)
        this.state.lastValue = Math.floor(value)

		this.controlDevice({ unwind: Math.floor(value) })
    }
    
    // 卷帘角度
    _onAngleChange =(value)=>{
        this.setState({
            angleValue: Math.floor(value),
        })
        this.scrollWindow?.changeAngle(value)
    };
    
    // 卷帘角度
    _onAngleComplete =(value)=>{
        value = Math.floor(value)
        this.state.lastAngleValue = Math.floor(value)

        this.controlDevice({ angle: Math.floor(value) })
    }

    valueClick(value){
        this.controlDevice({ angle: value },()=>{
            this.setState({
                angleValue: value,
                lastAngleValue: value
            })
        })
    }

    // 竖直Slider
    renderVerticalSlider(){
        return(
            <View style={{width:'30%'}}>
                <VerticalSlider
                    style={{marginLeft: 30,height: curtainHeight}}
                    ref={e => this.verticalSlider = e}
                    sliderWidth={30}
                    touchWidth={50}
                    sliderBg={Colors.themBGLightGray}
                    onPanResponderMove={(evt, gs)=>{
                        let sliderRealH = curtainHeight-50  //轨道真实长度
                        let totalValue = 180  //最大计数

						let step = sliderRealH/totalValue
						let dy = gs.dy/step
						let moveY = parseInt(dy)
						let value = this.state.angleLastValue + moveY
						if(value >= totalValue) value = totalValue
                        if(value <= 0) value = 0
						this.setState({
							angleValue: value
                        })
                        let position = value/totalValue * 1.0 * sliderRealH
						this.verticalSlider.setPosition(position)
					}}
					onComplete={()=>{
                        // this._complete(this.state.value)
                        this.state.angleLastValue = Math.floor(this.state.angleValue)
					}}
                />        
            </View> 
        )
    }

    getHeaderView() {
        const { navigate,pop } = this.props.navigation;

        return (
            <View style={styles.header}>
                <View style={styles.status}/>
                <View style={styles.nav}>
                    <TouchableOpacity style={styles.backTouch} onPress={() => { pop() }}>
                        <Image style={{ width: 10, height: 18 }} source={require('../../../images/back_white.png')} />
                        <Text numberOfLines={1} style={[styles.navTitle,{color: Colors.white}]}>{this.state.title}</Text>
                    </TouchableOpacity>
                    <View style={{ flex: 1 }} />
                    {this.shareInfo ? <TouchableOpacity style={styles.navTouch} onPress={() => { this.goToShare() }}>
                        <Image style={styles.sahreImg} source={require('../../../images/share.png')} />
                    </TouchableOpacity> : null}
                    <TouchableOpacity style={styles.navTouch} onPress={() => { 
                        navigate('ScrollCurtainsDebug',{
                            deviceId: this.deviceData.deviceId,
                            curtainType: 1   // 1卷帘  2开合帘
                        }) 
                    }}>
                        <Image style={[styles.navImg,{tintColor: Colors.white}]} source={require('../../../images/debug.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.navTouch} onPress={() => { this.goToSetting() }}>
                        <Image style={styles.navImg} source={require('../../../images/setting_white.png')} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    /**
	 * 中间内容展示
     * @returns {*}
     */
    getCenterView(){
		let unwind = (100 - this.state.value) + '%'
		
		let curtainHeight = screenH/3.0
		let curtainTopHeight = screenW*0.8*0.07
		let bottomHeight = curtainHeight - curtainTopHeight

        let valText = this.state.value + '%'
        if(this.state.value == 0){
            valText = '全开'
        }else if(this.state.value == 100){
            valText = '全关'
        }
		
		return(
			<View style={styles.curtainWrapper}>
                <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
					{/* <Text style={{color:'#fff'}}>展开程度</Text> */}
                    <Text style={{color:'#fff',fontSize:25, marginLeft:10,fontWeight:'bold'}}>{valText}</Text>
				</View>
				<View style={{width: '80%',marginTop:30,height:curtainHeight}}>
                    <Image 
                        style={{width:'100%',height:curtainTopHeight,resizeMode:'stretch'}} 
                        source={require('../../../images/deviceIcon/scrollCurtain_head.png')}
                    />
					<ScrollWindow
                        ref={e => this.scrollWindow = e}
                        angleValue={this.state.angleValue}
						unwind={unwind}
						bottomHeight={bottomHeight} 
						onPanResponderMove={(evt, gs)=>{
							
							let step = bottomHeight/100.0
							let dy = gs.dy/step
							let moveY = parseInt(dy)
							let value = this.state.lastValue + moveY
							if(value >= 100) value = 100
							if(value <= 0) value = 0
							this.setState({
								value: value
							})
						}}
						onComplete={()=>{
							this._complete(this.state.value)
						}}
					/>
				</View>
			</View>
		)
	}

    showControllerBtns(){
        return(
            <View style={styles.centerBtn}>
                <TouchableOpacity style={styles.btn} onPress={()=>{
                    this.controlDevice({number: 1},()=>{})
                }}>
                    <Image style={styles.centerBtnImg} source={require('../../../images/panel/curtain/curtain_on.png')}/>
                    <Text style={styles.btnText}>打开</Text>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.btn,{marginHorizontal: 50}]} onPress={()=>{
                    this.controlDevice({number: 3},()=>{})
                }}>
                    <Image style={styles.centerBtnImg} source={require('../../../images/panel/curtain/curtain_pause.png')}/>
                    <Text style={styles.btnText}>暂停</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.btn} onPress={()=>{
                    this.controlDevice({number: 2},()=>{})
                }}>
                    <Image style={styles.centerBtnImg} source={require('../../../images/panel/curtain/curtain_off.png')}/>
                    <Text style={styles.btnText}>关闭</Text>
                </TouchableOpacity>
            </View>
        )
    }

	//角度
    showProcess(){
        let btnsArr = [0, 45, 90, 135, 180]
        let btns = btnsArr.map((value, index)=>{
            return(
                <TouchableOpacity key={'a_btns_'+index} style={styles.quickBtn} activeOpacity={0.7} onPress={()=>{
                    this.valueClick( value )
                }}>
                    <View style={styles.shortLine}/>
                    <Text style={styles.quickText}>{value}</Text>
                </TouchableOpacity>
            )
        })

		return(
			<View style={styles.processWrapper}>
				<View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
					<Text style={{color:'#fff'}}>角度</Text>
                    <Text style={{color:'#fff',fontSize:25, marginLeft:10,fontWeight:'bold'}}>{this.state.angleValue+'°  '}</Text>
				</View>
				<Slider
					// disabled={this.state.isRuning}
					style={{width: '80%',marginTop:15}}
					minimumValue={0}
                    maximumValue={180}
          			value={this.state.angleValue}
					onValueChange={this._onAngleChange}
					onSlidingComplete={this._onAngleComplete}
					minimumTrackTintColor='#E5E5E5'
					maximumTrackTintColor='#E5E5E5'
                    thumbTintColor='#fff'
					trackStyle={{height:8,borderRadius: 4}}
                    thumbStyle={{width:26,height:26,borderRadius:13}}
        		/>
                <View style={styles.quickBtnWrapper}>
                    {btns}
                </View>
			</View>
		)
	}

	

	getMainPanelPage(){
		return(
			<View style={styles.main}>
                {this.getCenterView()}
                {this.showControllerBtns()}
                {this.showProcess()}
			</View>
		)
	}
}

class ScrollWindow extends Component {

	constructor(props) {
        super(props);
        
        this.state = {
            angleValue: props.angleValue
        }
	}

	componentWillMount() {
		this._panResponder = PanResponder.create({
      		onMoveShouldSetPanResponder: (evt, gestureState) => true,
      		onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
			onPanResponderGrant: () => {
				// 移动 开始
			},
			onPanResponderMove: (evt, gs) => {
				this.props.onPanResponderMove(evt, gs)
			},
			onPanResponderRelease: (evt, gs) => {
				// 移动结束
				this.props.onComplete()
			}
		});
    }
    
    changeAngle(value){
        console.log(value);
        this.setState({
            angleValue: value
        })
    }
    
	render(){
        const laminaNum = 8
		let bottomHeight = this.props.bottomHeight - 1
		let unwind = this.props.unwind
        let itemHeigth = bottomHeight/laminaNum

        let h = (Math.abs(this.props.angleValue - 90)/90  * 0.6 + 0.4) * itemHeigth
        // 向下取整
        h = Math.floor(h)
        
        let windowList = []
        for (let index = 0; index < laminaNum; index++) {
            let splitLine = <View style={{position:'absolute',bottom:0,left:0,width:'100%',height: 1,backgroundColor:'#F6F6F6'}}/>
            if(index == laminaNum-1){
                splitLine= null
            }
            windowList.push(
                <View key={'laminaNum_' + index} style={{backgroundColor: '#F6F6F6',height:itemHeigth,justifyContent:'flex-end'}}>
                    <View style={{width:'100%',height:h, backgroundColor:'#59A1FB'}}/>
                    {splitLine}
                </View>
            )
        }
        
		return(
			<View style={{height:bottomHeight}} {...this._panResponder.panHandlers}>
                {windowList}
				<View style={{position:'absolute',width:'100%',height:unwind,bottom:0,left:0,backgroundColor:Colors.white}}/>
                <View style={{width:'100%',bottom: unwind, height:25}}>
                    <View style={styles.windowBtn}>
                        <Image 
                            source={require('../../../images/panel/curtainBtn.png')}
                            style={styles.btnImg}
                        />
                    </View>
                </View>
			</View>
		)
	}
}

class VerticalSlider extends Component {

	constructor(props) {
        super(props);
        this.sliderStyle = {
            width: props.touchWidth,
            height: props.touchWidth,
            borderRadius:props.touchWidth/2,
            backgroundColor: Colors.themBgRed,
            position:'absolute',
            left: (props.sliderWidth-props.touchWidth)/2,
            top: 0
        }
	}

	componentWillMount() {
		this._panResponder = PanResponder.create({
      		onMoveShouldSetPanResponder: (evt, gestureState) => true,
      		onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
			onPanResponderGrant: () => {
				// 移动 开始
			},
			onPanResponderMove: (evt, gs) => {
				this.props.onPanResponderMove(evt, gs)
			},
			onPanResponderRelease: (evt, gs) => {
				// 移动结束
				this.props.onComplete()
			}
        });
    }
    
    setPosition(value){
        this.mainSlider?.setNativeProps({
            style: {
                ...this.sliderStyle,
                top: value
            }
        })
    }

	render(){
		return(
            <View style={{backgroundColor:this.props.sliderBg,width:this.props.sliderWidth,borderRadius:this.props.sliderWidth/2,...this.props.style}}>
                <View ref={e => this.mainSlider = e} style={this.sliderStyle} {...this._panResponder.panHandlers}/>
            </View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
		flex: 1,
        backgroundColor: '#3E7EF7',
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
    processWrapper:{
        width:screenW, 
        paddingBottom: BottomSafeMargin + 0.05*screenH,
        justifyContent:'center',
        alignItems:'center'
    },
    curtainWrapper:{
        alignItems :'center',
        justifyContent: 'center',
        flex: 1,
        width:'100%'
    },
    centerBtn:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        paddingVertical: 30,
    },
    quickBtnWrapper:{
        width: '80%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    quickBtn:{
        width: 28,
        justifyContent:'center',
        alignItems:'center',
        marginTop: 15
    },
    quickText:{
        fontSize: 15,
        color: Colors.white
    },
    shortLine:{
        width: 2,
        height: 8,
        backgroundColor:Colors.white
    },
    windowBtn:{
        width:50,
        height:50,
        borderRadius:25,
        backgroundColor:'#fff',
        position: 'absolute',
        top: -25,
        alignSelf:'center',
        justifyContent:'center',
        alignItems:'center',
        borderWidth: 0.5,
        borderColor: '#E5E5E5'
    },
    btnImg:{
        width:20,
        height:20,
        resizeMode:'contain'
    },
    centerBtnImg:{
        height:50,
        width:50,
        resizeMode:'contain'
    },
    header: {
        position: 'absolute',
        zIndex: 999,
        top: 0,
        left: 0,
        width: '100%',
        height: StatusBarHeight + NavigationBarHeight
    },
    status: {
        height: StatusBarHeight,
        width: '100%',
    },
    nav: {
        height: NavigationBarHeight,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 10,
        paddingLeft: 10,
        marginRight: 5
    },
    navImg: {
        width: 22,
        height: 22,
        resizeMode: 'contain'
    },
    sahreImg:{
        width: 26,
        height: 26,
        resizeMode: 'contain'
    },
    navTitle:{ 
        marginLeft: 15, 
        fontSize: 24, 
        fontWeight: 'bold'
    },
    backTouch: {
        height: '100%',
        marginLeft: 16,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        maxWidth:'50%'
    },
    btn:{
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnText:{
        marginTop: 3,
        fontSize: 15,
        color: Colors.white
    }
});

export default ScrollAngleCurtains



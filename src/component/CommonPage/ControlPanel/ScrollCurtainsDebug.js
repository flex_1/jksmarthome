/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    DeviceEventEmitter,
    Image,
    Dimensions,
    Modal
} from 'react-native';
import { connect } from 'react-redux';
import { NetUrls,Colors } from '../../../common/Constants';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderTitle from '../Navigation/HeaderTitle';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderRight from '../Navigation/HeaderRight';
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import ToastManager from "../../../common/CustomComponent/ToastManager";
import AnimateButton from '../../../common/CustomComponent/AnimateButton';

const screenW = Dimensions.get('window').width;
const screenH = Dimensions.get('window').height;
const TimerCountDefault = 15

class ScrollCurtainsDebug extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'设置行程'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation

        this.deviceId = getParam('deviceId')
        this.curtainType = getParam('curtainType')   // 1卷帘  2开合帘
        this.state={
            showLearningModel: false,
            timerCount: TimerCountDefault,
            isOnBinding: true,
            bindSeriaNO: ''
        }
    }

    // 窗帘 控制
    async deviceDebug(type){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.shutterAddress,
				params: {
                    deviceId: this.deviceId,
                    type: type
                }
			});
            SpinnerManager.close()
			if (data.code == 0) {
                if(type == 2){
                    this.setState({
                        timerCount: TimerCountDefault,
                        bindSeriaNO: data.result || '',
                        showLearningModel: true
                    })
                    this.startCount()
                    this.animateButton.startAnimation();
                }else{
                    ToastManager.show('发送成功')
                }
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    async requestBindStatus() {
        try {
            let data = await postJson({
                url: NetUrls.bindingRIStudyResult,
                timeout: 1.8, // time out when no respones in 2 seconds
                params: {
                    prefix: this.state.bindSeriaNO,
                    deviceId: this.deviceId
                }
            });
            SpinnerManager.close()
            if (data && data.code == 0) {
                if (data.result.status == 0) {
                    // fail
                    this.interval && clearInterval(this.interval);
                    this.animateButton.stopAnimation()
                    this.setState({
                        timerCount: 0,
                        showLearningModel: false
                    })
                    ToastManager.show('失败')
                } else if (data.result.status == 1) {
                    // success
                    this.interval && clearInterval(this.interval);
                    this.animateButton.stopAnimation()
                    this.setState({
                        showLearningModel: false
                    },()=>{
                        ToastManager.show('成功!')
                    })
                } else {
                    // go on
                }
            } else {
                let error = new CommonException(data.msg, data.code)
                throw error
            }
        } catch (error) {
            if (error.code == NetParams.networkTimeout) {
                // 网络 1.8 秒内未完成
                return
            }
        }
    }

    startCount() {
        this.interval = setInterval(() => {
            let timerCount = this.state.timerCount - 1;
            if (timerCount <= 0) {
                this.animateButton.stopAnimation()
                this.interval && clearInterval(this.interval);
                this.setState({
                    showLearningModel: false
                },()=>{
                    ToastManager.show('失败')
                })
            } else {
                if (timerCount % 2 == 0) {
                    this.requestBindStatus()
                }
            }
            this.setState({ timerCount: timerCount })
        }, 1000)
    }

    renderBindingBtn() {
        return(
            <AnimateButton
                style = {{ width:'100%',alignItems:'center',marginTop: 30}}
                ref={e => this.animateButton = e}
                centerLabel = {this.state.timerCount + 's '}
                onClick={()=>{
                    if(this.state.isOnBinding){
                        return
                    }
                    this.requestBinding()
                }}
            />
        )
    }

    renderLearningModel(){
        const Colors = this.props.themeInfo.colors

        return(
            <Modal
                style={{ flex: 1 }}
                animationType='fade'
                transparent={true}
                visible={this.state.showLearningModel}
                onShow={() => {
                }}
                onRequestClose={() => {
                }}
            >
                <View style={styles.modalTouchable}>
                    <Text style={[styles.failTips,{marginTop: screenH * 0.3}]}>对码中...</Text>
                    {this.renderBindingBtn()}
                </View>
            </Modal>
        )
    }

    renderBtnItem(icon, name, type){
        return(
            <View style={styles.itemWrapper}>
                <TouchableOpacity style={styles.btnTouch} onPress={()=>{
                    this.deviceDebug(type)
                }}>
                    <Image style={styles.btnImg} source={icon}/>
                </TouchableOpacity>
                <Text style={styles.btnText}>{name}</Text>
            </View>
        )
    }

    renderTopRow(){
        return(
            <View style={styles.lineWrapper}>
                {this.renderBtnItem(require('../../../images/panel/curtain/checkNum.png'), '对码', 2)}
                {this.renderBtnItem(require('../../../images/panel/curtain/reset.png'), '重设行程',11)}
            </View>
        )
    }

    renderDirectionRow(){
        if(this.curtainType == 1){
            return(
                <View style={styles.lineWrapper}>
                    {this.renderBtnItem(require('../../../images/panel/curtain/up.png'), '上升',4)}
                    {this.renderBtnItem(require('../../../images/panel/curtain/pause.png'), '暂停',5)}
                    {this.renderBtnItem(require('../../../images/panel/curtain/down.png'), '下降',6)}
                </View>
            )
        }else if(this.curtainType == 2){
            return(
                <View style={styles.lineWrapper}>
                    {this.renderBtnItem(require('../../../images/panel/curtain/left.png'), '打开',4)}
                    {this.renderBtnItem(require('../../../images/panel/curtain/pause.png'), '暂停',5)}
                    {this.renderBtnItem(require('../../../images/panel/curtain/right.png'), '关闭',6)}
                </View>
            )
        }else{
            return null
        }
    }

    renderDebugRow(){
        return(
            <View style={styles.lineWrapper}>
                {this.renderBtnItem(require('../../../images/panel/curtain/forward.png'), '电机正向',7)}
                {this.renderBtnItem(require('../../../images/panel/curtain/reverse.png'), '电机反向',8)}
                {this.curtainType == 1 ? this.renderBtnItem(require('../../../images/panel/curtain/top.png'), '设为最高点',10) : null}
            </View>
        )
    }

    render() {
        return (
            <View style={{flex: 1,backgroundColor: '#F6F7F8'}}>
                {this.renderTopRow()}
                {this.renderDirectionRow()}
                {this.renderDebugRow()}
                {this.renderLearningModel()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 15,
        paddingLeft: 10,
        marginRight: 5
    },
    subTips:{
        marginTop: 10, 
        marginLeft: 16,
        fontSize: 14,
    },
    btnTouch:{
        width: screenW * 0.22, 
        height: screenW * 0.22,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: screenW * 0.1,
        // backgroundColor: Colors.white,
        //阴影四连
        // shadowOffset: { width: 0, height: 1 },
        // shadowColor: '#070F26',
        // shadowOpacity: 0.15,
        // shadowRadius: screenW * 0.1,
        // elevation: 5
    },
    btnImg:{
        width:'100%',
        height:'100%',
        resizeMode:'contain'
    },
    lineWrapper:{
        flexDirection: 'row',
        justifyContent:'space-between',
        paddingHorizontal: 32,
        marginTop: 40
    },
    btnText:{
        marginTop: 10,
        fontSize: 16,
        color: '#596387'
    },
    itemWrapper:{
        justifyContent: 'center', 
        alignItems: 'center'
    },
    modalTouchable:{ 
        width: '100%', 
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.8)',
        alignItems:'center'
    },
    failTips:{
        marginTop: 40,
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.white
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo
    })
)(ScrollCurtainsDebug)

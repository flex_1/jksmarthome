/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 窗帘
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	Dimensions,
    DeviceEventEmitter,
    TouchableOpacity
} from 'react-native';
import { Colors, NetUrls,NotificationKeys,NetParams } from '../../../common/Constants';
import Slider from "react-native-slider";
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class DreamShutter extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		
		this.state = {
			...this.state,

			value: 0,
            angleValue: 0
		}
	}

	componentDidMount() {
		super.componentDidMount()
		this.requestDeviceStatus()
	}

	componentWillUnmount() {
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

		this.setState({
            value:  Math.floor(data.unwind)
        })
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				let res = data.result && data.result.deviceStatus
				try {
					res = JSON.parse(res)
					this.setState({
                        isCollect: data.result && data.result.isCollect,
						value: Math.floor(res.unwind) || this.state.value
					})
				} catch (error) {
					ToastManager.show('数据异常')
				}
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}
    
    // 窗帘 控制
    async controlDevice(params,callBack){
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

	_onChange =(value)=>{
        this.setState({
            value: Math.floor(value),
        })
	};
	
	_complete =(value)=>{
		console.log(Math.floor(value));
		this.controlDevice({unwind: Math.floor(value) })
    }

    // 卷帘角度
    _onAngleChange =(value)=>{
        this.setState({
            angleValue: Math.floor(value),
        })
    };
    
    // 卷帘角度
    _onAngleComplete =(value)=>{
        value = Math.floor(value)
        
        this.controlDevice({ angle: Math.floor(value) })
    }
    
    valueClick(unwind, angle){
        let params = {}
        if(unwind != null){
            params.unwind = unwind
        }else{
            params.angle = angle
        }
        this.controlDevice(params,()=>{
            if(unwind != null){
                this.setState({
                    value: unwind
                })
            }else{
                this.setState({
                    angleValue: angle
                })
            }
        })
    }

    getHeaderView() {
        const { navigate,pop } = this.props.navigation;

        return (
            <View style={styles.header}>
                <View style={styles.status}/>
                <View style={styles.nav}>
                    <TouchableOpacity style={styles.backTouch} onPress={() => { pop() }}>
                        <Image style={{ width: 10, height: 18 }} source={require('../../../images/back_white.png')} />
                        <Text numberOfLines={1} style={[styles.navTitle,{color: Colors.white}]}>{this.state.title}</Text>
                    </TouchableOpacity>
                    <View style={{ flex: 1 }} />
                    {this.shareInfo ? <TouchableOpacity style={styles.navTouch} onPress={() => { this.goToShare() }}>
                        <Image style={styles.sahreImg} source={require('../../../images/share.png')} />
                    </TouchableOpacity> : null}
                    <TouchableOpacity style={styles.navTouch} onPress={() => { 
                        navigate('ScrollCurtainsDebug',{
                            deviceId: this.deviceData.deviceId,
                            curtainType: 2   // 1卷帘  2开合帘
                        }) 
                    }}>
                        <Image style={[styles.navImg,{tintColor: Colors.white}]} source={require('../../../images/debug.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.navTouch} onPress={() => { this.goToSetting() }}>
                        <Image style={styles.navImg} source={require('../../../images/setting_white.png')} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    getCenterView(){
		let unwind = (100 - this.state.value) + '%'
		let curtainHeight = screenW*0.78*(3/5)
		let curtainTopHeight = screenW*0.78*0.22

        let angle = parseInt(this.state.angleValue)
        let w = (1 - Math.abs(angle - 90)/90)  * 0.7 * 100
        w = w + '%'

		return(
			<View style={styles.topWrapper}>
				<View style={styles.windowBg}>
					<View style={[styles.leftWindow,{height:curtainHeight}]}>
                        <View style={styles.windowItemLeft}>
                            <View style={[styles.windowImg,{width: w}]}/>
                        </View>
                        <View style={styles.windowItemLeft}>
                            <View style={[styles.windowImg,{width: w}]}/>
                        </View>
                        <View style={styles.windowItemLeft}>
                            <View style={[styles.windowImg,{width: w}]}/>
                        </View>
                        <View style={styles.windowItemLeft}>
                            <View style={[styles.windowImg,{width: w}]}/>
                        </View>
                        <View style={styles.windowItemLeft}>
                            <View style={[styles.windowImg,{width: w}]}/>
                        </View>
                        <View style={[styles.windowBgHand,{width: unwind,right:-1}]}/>
                    </View>
					<View style={[styles.rightWindow,{height:curtainHeight}]}>
                        <View style={styles.windowItemRight}>
                            <View style={[styles.windowImg,{width: w}]}/>
                        </View>
                        <View style={styles.windowItemRight}>
                            <View style={[styles.windowImg,{width: w}]}/>
                        </View><View style={styles.windowItemRight}>
                            <View style={[styles.windowImg,{width: w}]}/>
                        </View><View style={styles.windowItemRight}>
                            <View style={[styles.windowImg,{width: w}]}/>
                        </View><View style={styles.windowItemRight}>
                            <View style={[styles.windowImg,{width: w}]}/>
                        </View>
                        <View style={[styles.windowBgHand,{width: unwind,left:-1}]}/>
                    </View>
					<View style={[styles.windowTopView,{height: curtainTopHeight}]}>
						<Image source={require('../../../images/panel/top_window_icon.png')} style={styles.windowTopImg}/>
					</View>
				</View>
			</View>
		)
    }
    
    //中间按钮
	showControllerBtns(){
        return(
            <View style={styles.btnsWrapper}>
                <TouchableOpacity style={styles.btn} onPress={()=>{
                    this.controlDevice({number: 1},()=>{})
                }}>
                    <Image style={styles.centerBtnImg} source={require('../../../images/panel/curtain/curtain_h_on.png')}/>
                    <Text style={styles.btnText}>打开</Text>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.btn,{marginHorizontal: 50}]} onPress={()=>{
                    this.controlDevice({number: 3},()=>{})
                }}>
                    <Image style={styles.centerBtnImg} source={require('../../../images/panel/curtain/curtain_h_pause.png')}/>
                    <Text style={styles.btnText}>暂停</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.btn} onPress={()=>{
                    this.controlDevice({number: 2},()=>{})
                }}>
                    <Image style={styles.centerBtnImg} source={require('../../../images/panel/curtain/curtain_h_off.png')}/>
                    <Text style={styles.btnText}>关闭</Text>
                </TouchableOpacity>
            </View>
        )
    }

    // 展开关闭程度
    showProcess(){
        let btnsArr = [0, 25, 50, 75, 100]
        let btnslabelArr = ['全开',25,50,75,'全关']

        let btns = btnsArr.map((value, index)=>{
            let smallFont = {}
            if(index == 0 || index == 4){
                smallFont = {fontSize: 12}
            }
            return(
                <TouchableOpacity key={'btns_'+index} style={styles.quickBtn} activeOpacity={0.7} onPress={()=>{
                    this.valueClick(value, null)
                }}>
                    <View style={styles.shortLine}/>
                    <Text style={[styles.quickText,smallFont]}>{btnslabelArr[index]}</Text>
                </TouchableOpacity>
            )
        })

        let valText = this.state.value + '%'
        if(this.state.value == 0){
            valText = '全开'
        }else if(this.state.value == 100){
            valText = '全关'
        }

		return(
			<View style={styles.processWrapper}>
				<View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                    <Text style={{color:'#fff',fontSize:25,fontWeight:'bold'}}>{valText}</Text>
				</View>
				<Slider
					style={{width: '80%',marginTop: 10}}
					minimumValue={0}
                    maximumValue={100}
          			value={this.state.value}
					onValueChange={this._onChange}
					onSlidingComplete={this._complete}
					minimumTrackTintColor='#fff'
					maximumTrackTintColor='#008C82'
					thumbTintColor='#fff'
					trackStyle={{height:8,borderRadius: 4}}
					thumbStyle={{width:26,height:26,borderRadius:13}}
        		/>
                <View style={styles.quickBtnWrapper}>
                    {btns}
                </View>
			</View>
		)
	}

    // 角度
    showAngleProcess(){
        let btnsArr = [0, 45, 90, 135, 180]
        let btns = btnsArr.map((value, index)=>{
            return(
                <TouchableOpacity key={'a_btns_'+index} style={styles.quickBtn} activeOpacity={0.7} onPress={()=>{
                    this.valueClick(null, value)
                }}>
                    <View style={styles.shortLine}/>
                    <Text style={styles.quickText}>{value}</Text>
                </TouchableOpacity>
            )
        })

		return(
			<View style={[styles.processWrapper,{marginTop: 20}]}>
				<View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
					<Text style={{color:'#fff',fontSize:20,fontWeight:'bold'}}>角度</Text>
                    <Text style={{color:'#fff',fontSize:25, marginLeft:10,fontWeight:'bold'}}>{this.state.angleValue+'°  '}</Text>
				</View>
				<Slider
					// disabled={this.state.isRuning}
					style={{width: '80%',marginTop:10}}
					minimumValue={0}
                    maximumValue={180}
          			value={this.state.angleValue}
					onValueChange={this._onAngleChange}
					onSlidingComplete={this._onAngleComplete}
					minimumTrackTintColor='#E5E5E5'
					maximumTrackTintColor='#008C82'
                    thumbTintColor='#fff'
					trackStyle={{height:8,borderRadius: 4}}
                    thumbStyle={{width:26,height:26,borderRadius:13}}
        		/>
                <View style={styles.quickBtnWrapper}>
                    {btns}
                </View>
			</View>
		)
	}

    rednerBottomSlider(){
        return(
            <View style={styles.bottomWrapper}>
                {this.showProcess()}
                {this.showAngleProcess()}
            </View>
        )
    }

	getMainPanelPage(){
		return(
			<View style={styles.main}>
                {this.getCenterView()}
                {this.showControllerBtns()}
                {this.rednerBottomSlider()}
			</View>
		)
	}

	// render() {
	// 	return (
	// 		<View style={styles.container}>
	// 			{this.getHeaderView()}
	// 			<ScrollView style={styles.container}>
	// 				{this.getMainPanelPage()}
	// 			</ScrollView >
	// 		</View>

	// 	)
	// }
}

const styles = StyleSheet.create({
	main: {
		flex: 1,
        backgroundColor: Colors.themeBG,
        paddingTop: StatusBarHeight + NavigationBarHeight
    },
    topWrapper:{
        alignItems:'center',
        justifyContent:'center',
        paddingHorizontal:'12%',
        flex: 1
    },
    btnsWrapper:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        marginTop: 10
    },
    bottomWrapper:{
        paddingBottom: BottomSafeMargin + 10,
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnTouch:{
        height:50,
        width:50,
        backgroundColor: Colors.white,
        borderRadius:25,
        justifyContent: 'center',
        alignItems: 'center',
    },
    processWrapper:{
        width:'100%',
        justifyContent:'center',
        alignItems:'center',
    },
    quickBtnWrapper:{
        width: '80%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    quickBtn:{
        width: 28,
        justifyContent:'center',
        alignItems:'center',
        marginTop: 15
    },
    quickText:{
        fontSize: 15,
        color: Colors.white
    },
    shortLine:{
        width: 2,
        height: 8,
        backgroundColor:Colors.white
    },
    windowTopView:{
        position:'absolute',
        left:-2,
        right:-2,
        top:-2,
        resizeMode:'contain',
        borderRadius:5,
        overflow:'hidden'
    },
    windowTopImg:{
        width:'100%',
        height:'100%',
        resizeMode:'stretch'
    },
    windowBg:{
        width:'100%',
        flexDirection:'row',
        borderRadius: 5
    },
    leftWindow:{
        backgroundColor: Colors.white,
        borderBottomLeftRadius:5,
        overflow: 'hidden',
        flex: 1,
        flexDirection:'row'
    },
    rightWindow:{
        backgroundColor: Colors.white,
        borderBottomRightRadius:5,
        overflow: 'hidden',
        flex: 1,
        flexDirection:'row'
    },
    windowItemLeft:{
        height: '100%',
        width: '20%',
        backgroundColor: 'rgba(44,199,188,0.6)',
        flexDirection:'row',
    },
    windowItemRight:{
        height: '100%',
        width: '20%',
        backgroundColor: 'rgba(44,199,188,0.6)',
        flexDirection:'row',
        justifyContent:'flex-end',
    },
    windowImg:{
        height:'100%',
        backgroundColor: Colors.white
    },
    windowBgHand:{
        position:'absolute',
        height:'100%',
        backgroundColor: Colors.white
    },
    header: {
        position: 'absolute',
        zIndex: 999,
        top: 0,
        left: 0,
        width: '100%',
        height: StatusBarHeight + NavigationBarHeight
    },
    status: {
        height: StatusBarHeight,
        width: '100%',
    },
    nav: {
        height: NavigationBarHeight,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 10,
        paddingLeft: 10,
        marginRight: 5
    },
    navImg: {
        width: 22,
        height: 22,
        resizeMode: 'contain'
    },
    sahreImg:{
        width: 26,
        height: 26,
        resizeMode: 'contain'
    },
    navTitle:{ 
        marginLeft: 15, 
        fontSize: 24, 
        fontWeight: 'bold'
    },
    backTouch: {
        height: '100%',
        marginLeft: 16,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        maxWidth:'50%'
    },
    centerBtnImg:{
        height: 50,
        width: 50,
        resizeMode:'contain'
    },
    btn:{
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnText:{
        marginTop: 3,
        fontSize: 15,
        color: Colors.white
    }
});

export default DreamShutter



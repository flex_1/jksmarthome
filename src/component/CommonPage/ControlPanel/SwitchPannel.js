/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter,
	Switch,
    StatusBar,
    Animated
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel'; 

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class SwitchPannel extends Panel {

    static defaultProps = {
        ripple: 3,       //同时存在的圆数量
        initialDiameter: screenW*0.4,
        endDiameter: screenW*0.6,
        initialPosition: { top: 0, left: screenW*0.5 },
        rippleColor: Colors.white,
        intervals: 120,      //间隔时间
        spreadSpeed: 600,      //扩散间隔
    }

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData')
        this.animating = false
        this.animatedFun = null;

        // let rippleArr = [];
        // for (let i = 0; i < props.ripple; i++) rippleArr.push(0);
        this.state = {
            // anim: rippleArr.map(() => new Animated.Value(0)),
            ...this.state,
			status: this.deviceData?.status,
			isLoading: true,
        }
    }
    
	componentDidMount() {
        super.componentDidMount()
        if(this.isTCP){
            this.requestStatusByTCP()
        }else{
            this.requestDeviceStatus()
        }
	}

	componentWillUnmount(){
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;
        
		this.setState({
            status: data.status == 1
        })
    }

    // tcp 数据状态解析
    updateDeviceStatusByTCP(data){
        if(!data) return
        let status = data.split('_')[1]

        this.setState({
            status: status == 1
        })
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			this.setState({isLoading: false})
			if (data.code == 0) {
				let status = this.state.status
				if(data.result && data.result.deviceStatus){
					let statusJSON = JSON.parse(data.result.deviceStatus)
					status = statusJSON.status
				}
				this.setState({
                    isCollect: data.result && data.result.isCollect,
					status: status
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({isLoading: false})
			ToastManager.show('网络错误')
		}
	}
    
    // 灯 开关 控制
    async controlDevice(params,callBack,failCallback){
        if(this.isTCP){
            this.controlByTCP(params,callBack)
            return
        }
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				failCallback && failCallback()
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误，请稍后重试')
		}
    }

    // 通过TCP 控制设备
    controlByTCP(params, callBack){
        let req = 'request-2-' + this.tcpDeviceType + '-' + this.tcpDeviceId + '-' + this.tcpOrder + '-' 
        if(params.hasOwnProperty('status')){
            req = req + '0-' + params.status
        }
        console.log('client-send- ' + req);
        try {
            window.client.write(req)
            callBack && callBack()
        } catch (error) {
            ToastManager.show('控制出错，请检查连接后重试。')
        }
    }

	// 更改状态
	changeStatus(target){
		// if(this.state.isLoading){
		// 	ToastManager.show('正在请求数据，请稍后再试')
		// 	return
		// }
		// let oldStatus = this.state.status
		// this.setState({
		// 	status: target
		// })
		this.controlDevice({status: target},
			()=>{
				// 更改状态成功的回调
				this.setState({
					isLoading: false,
					status: target
				})      
			},
			()=>{
				// 更改状态失败的回调
				this.setState({
					// status: oldStatus,
					isLoading: false
				})
			},
		)
    }
    
    startAnimation() {
        this.animatedFun = Animated.stagger(this.props.intervals, this.state.anim.map((val) => {
            return Animated.timing(val, { toValue: 1, duration: this.props.spreadSpeed })
        }));
        this.animating = true
        this.animatedFun.start(() => {
            this.animating = false
            this.stopAnimation()
        })
    }
    stopAnimation() {
        this.animatedFun.stop();
        this.state.anim.map((val, index) => val.setValue(0));
    }

    showAniButton() {
        let switchText = this.state.status ? 'ON' : 'OFF'
        let bgColor = this.state.status ? '#17B7AC' : '#16161E'

        const { initialPosition, initialDiameter, endDiameter, rippleColor } = this.props;
        let r = endDiameter - initialDiameter;    // 直径变化量,top与left的变化是直径的一半
        
        let rippleComponent = this.state.anim.map((val, index) => {

            let animateStyle = [{
                borderRadius: 999,
                position: 'absolute',
                backgroundColor: rippleColor
            },
            {
                opacity: val.interpolate({
                    inputRange: [0, 1],
                    outputRange: [1, 0]
                }),
                height: val.interpolate({
                    inputRange: [0, 1],
                    outputRange: [initialDiameter, endDiameter]
                }),
                width: val.interpolate({
                    inputRange: [0, 1],
                    outputRange: [initialDiameter, endDiameter]
                }),
                top: val.interpolate({
                    inputRange: [0, 1],
                    outputRange: [initialPosition.top - initialDiameter / 2, initialPosition.top- initialDiameter / 2 - r / 2]
                }),
                left: val.interpolate({
                    inputRange: [0, 1],
                    outputRange: [initialPosition.left - initialDiameter / 2, initialPosition.left - initialDiameter / 2 - r / 2]
                }),
            }]

            return (
                <Animated.View style={animateStyle} key={"animatedView_" + index}>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={{width:'100%',height:'100%',justifyContent:'center',alignItems:'center'}}
                        onPress={() => {
                            if(this.animating){
                                return
                            }
                            this.startAnimation()
                            let target = this.state.status ? 0:1 
                            this.changeStatus(target)
                        }}
                    >
                        <View style={{ width: 14,height: 14, backgroundColor: bgColor,borderRadius:7, marginBottom: 20 }}/>
                        <Text style={{ fontSize: 26, color: Colors.themeTextBlack, fontWeight: 'bold',marginBottom: 20 }}>{switchText}</Text>
                    </TouchableOpacity>
                </Animated.View>
            )
        })
        return (
            <View style={{ flex: 1, position: 'absolute', left: 0,top: '45%' }}>
                {rippleComponent}
            </View>
        )
    }

    showStaticButton(){
        let light_img = this.state.status ? require('../../../images/panel/light_on.png') : require('../../../images/panel/light_off.png')
        return(
            <TouchableOpacity
                activeOpacity={0.5}
                style={styles.swicthBtn}
                onPress={() => {
                    let target = this.state.status ? 0:1 
                    this.changeStatus(target)
                }}
            >
                <Image style={{width:'100%',height:'100%'}} source={light_img}/>
            </TouchableOpacity>
        )
    }

	getMainPanelPage(){
		return(
			<View style={styles.main}>
                {this.showStaticButton()}
                {/* {this.showAniButton()} */}
			</View>
		)
	}

	render() {
		let bgColor = this.state.status ? '#17B7AC' : '#16161E'
		return (
			<View style={[styles.container,{backgroundColor: bgColor}]}>
				{this.getHeaderView()}
				{this.getMainPanelPage()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#0167FF',
	},
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight,
		justifyContent: 'center',
		alignItems: 'center'
	},
	swicthBtn: {
        width: screenW*0.5,
        height:screenW*0.5,
        justifyContent:'center',
        alignItems:'center'
    }
});

export default SwitchPannel



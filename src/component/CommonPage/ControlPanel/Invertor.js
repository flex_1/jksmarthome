/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Animated,
	Dimensions,
    Modal,
    Keyboard,
    Alert,
    ImageBackground
} from 'react-native';
import { Colors, NetUrls } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import Panel from './Panel';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class Invertor extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
        
        this.typeId = getParam('typeId')
        this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            deviceData: (this.deviceData && JSON.parse(this.deviceData.deviceStatus)) || {}
        }
    }
    
	componentDidMount() {
        this.requestDeviceStatus()
	}

	componentWillUnmount(){
        
    }

    async requestDeviceStatus(){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
            SpinnerManager.close()
			if (data.code == 0) {
                this.setState({
                    deviceData: JSON.parse(data.result?.deviceStatus) || {}
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    async requestControlBtn(mode){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				params: {
                    deviceId: this.deviceData.deviceId,
                    mode: mode
                }
			});
            SpinnerManager.close()
			if (data.code == 0) {
                this.requestDeviceStatus()
				ToastManager.show(data.msg)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

    renderDashBoard(){
        return(
            <View style={{paddingHorizontal: 16,marginTop: 20}}>
                <ImageBackground style={styles.dashBordBg} source={require('../../../images/invertor/instrument.png')}>
                    <View style={[ styles.dashBordBg, {transform: [{rotateZ: '180deg'}]} ]}/>
                </ImageBackground>
            </View>
        )
    }

    renderTopStatus(){
        const Colors = this.props.themeInfo.colors;
        const mode = this.state.deviceData.mode
        let modeImg = require('../../../images/invertor/charging.png')
        let modeName = '市电模式'
        if(mode == 0){
            modeName = '市电模式'
            modeImg = require('../../../images/invertor/charging.png')
        }else if(mode == 1){
            modeName = '电池模式'
            modeImg = require('../../../images/invertor/battery.png')
        }else if(mode == 2){
            modeName = '节能模式'
            modeImg = require('../../../images/invertor/green.png')
        }else if(mode == 4){
            modeName = '无人模式'
            modeImg = require('../../../images/invertor/nobody.png')
        }

        return(
            <View style={styles.topWrapper}>
                <View style={styles.contentWrapper}>
                    <Text style={[styles.modeTitle,{color: Colors.themeTextLight}]}>当前模式</Text>
                    <Text style={[styles.modeName,{color: Colors.themeText}]}>{modeName}</Text>
                </View>
                <View style={{flex: 1}}/>
                <Image style={styles.statusImg} source={modeImg}/>
            </View>
        )
    }

    renderItem(title, content1, content2, extra){
        const Colors = this.props.themeInfo.colors;
        

        return(
            <View style={[styles.itemWrapper,{backgroundColor: Colors.themeBg}]}>
                <View style={styles.titleWrapper}>
                    <Text style={[styles.itemTitle,{color: Colors.themeText}]}>{title}</Text>
                    <ImageBackground style={styles.textBg} source={require('../../../images/invertor/textBg.png')}>
                        <Text style={styles.extra}>{extra}</Text>
                    </ImageBackground>
                </View>
                <Text style={[styles.itemSubTitle,{color: Colors.themeTextLight}]}>{content1}</Text>
                <Text style={[styles.itemSubTitle,{color: Colors.themeTextLight}]}>{content2}</Text>
            </View>
        )
    }

    renderInvertorData(){
        const { inacAvol,inacAfre,statusArun} = this.state.deviceData
        const { batteryAcap,CharAVol,batteryAvol} = this.state.deviceData
        const { outacAvol,outacAfre } = this.state.deviceData

        let statusName = '市电正常'
        if(statusArun == 1){
            statusName = '市电正常'
        }else if(statusArun == 2){
            statusName = '充电运行'
        }else if(statusArun == 3){
            statusName = '逆变运行'
        }else if(statusArun == 9){
            statusName = '输出开启'
        }

        return(
            <View>
                {/* {this.renderItem('太阳能','太阳能电压 20V', '太阳能电流 3A', 'MPPT充电')} */}
                {this.renderItem('市电','市电电压 '+inacAvol+'V', '市电频率 '+inacAfre+'Hz', '市电正常')}
                {this.renderItem('电池','电池容量 '+batteryAcap+'%', '充电电压 '+CharAVol+'V', batteryAvol+'V')}
                {this.renderItem('输出','输出电压 '+outacAvol+'V', '输出频率 '+outacAfre+'Hz', '市电输出')}
            </View>
        )
    }

    renderBottomBtns(){
        const Colors = this.props.themeInfo.colors;

        return(
            <View style={styles.bottomWrapper}>
                <TouchableOpacity style={styles.bottomBtn} onPress={()=>{
                    this.requestControlBtn(0)
                }}>
                    <Image style={styles.bottomImg} source={require('../../../images/invertor/chargingBtn.png')}/>
                    <Text style={[styles.extra,{color: Colors.themeTextLight}]}>市电模式</Text>
                </TouchableOpacity>
               <TouchableOpacity style={styles.bottomBtn} onPress={()=>{
                   this.requestControlBtn(1)
               }}>
                    <Image style={styles.bottomImg} source={require('../../../images/invertor/batteryBtn.png')}/>
                    <Text style={[styles.extra,{color: Colors.themeTextLight}]}>电池模式</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.bottomBtn} onPress={()=>{
                    this.requestControlBtn(2)
                }}>
                    <Image style={styles.bottomImg} source={require('../../../images/invertor/greenBtn.png')}/>
                    <Text style={[styles.extra,{color: Colors.themeTextLight}]}>节能模式</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.bottomBtn} onPress={()=>{
                    this.requestControlBtn(4)
                }}>
                    <Image style={styles.bottomImg} source={require('../../../images/invertor/nobodyBtn.png')}/>
                    <Text style={[styles.extra,{color: Colors.themeTextLight}]}>无人值守</Text>
                </TouchableOpacity>
            </View>
        )
    }

    getMainPanelPage(){
        const Colors = this.props.themeInfo.colors;

		return(
            <View style={[styles.wrapper,{backgroundColor: Colors.themeBg}]}>
                <ScrollView contentContainerStyle={styles.scrollContent}>
				    {/* {this.renderDashBoard()} */}
                    {this.renderTopStatus()}
                    {this.renderInvertorData()}
                    {this.renderBottomBtns()}
                </ScrollView>
			</View>
		)
	}

	// render() {
    //     const Colors = this.props.themeInfo.colors;

	// 	return (
	// 		<View style={[styles.wrapper,{backgroundColor: Colors.themeBaseBg}]}>
	// 			<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
    //                 {this.renderMainController()}
    //             </View>
	// 		</View>
	// 	)
	// }
}

const styles = StyleSheet.create({
	wrapper: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
    },
    scrollContent:{
        paddingBottom: BottomSafeMargin + 30
    },
    main:{
        marginTop: 6,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    dashBordBg:{
        width: screenW -32, 
        height: (screenW -32)*0.5, 
        resizeMode:'contain'
    },
    pointerBg:{
        backgroundColor: 'red',
        position:'absolute', 
        bottom: 0, 
        width: '100%', 
        height: 1
    },
    topWrapper:{
        width: '100%',
        paddingHorizontal: 32,
        flexDirection: 'row',
        marginTop: 25
    },
    statusImg:{
        width: 114,
        height: 114,
        resizeMode: 'contain'
    },
    contentWrapper:{
        justifyContent: 'center'
    },
    modeTitle:{
        fontSize: 14,
        fontWeight: '400'
    },
    modeName:{
        fontSize: 26,
        fontWeight: '500',
        marginTop: 10
    },
    itemWrapper:{
        marginTop: 15,
        marginHorizontal: 32,
        paddingHorizontal: 16,
        justifyContent: 'center',
        height: 86,
        borderRadius: 4,
        //阴影四连
        shadowOffset: { width: 0, height: 0 },
        shadowColor: 'rgba(59,74,116,1)',
        shadowOpacity: 0.14,
        shadowRadius: 4,
        elevation: 4
    },
    titleWrapper:{
        flexDirection: 'row',
        alignItems: 'center'
    },
    itemTitle:{
        fontSize: 16,
        fontWeight: '500',
        flex: 1
    },
    itemSubTitle:{
        fontSize: 13,
        fontWeight: '400',
        marginTop: 5
    },
    textBg:{
        width: 75,
        height: 21,
        resizeMode: 'contain',
        justifyContent: 'center',
        alignItems: 'center'
    },
    extra:{
        fontSize: 12,
        fontWeight: '400',
        color: '#007EF3'
    },
    bottomWrapper:{
        marginHorizontal: 24,
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    bottomBtn:{
        flex: 1,
        justifyContent: 'center', 
        alignItems: 'center',
        height: (screenW - 24)/4
    },
    bottomImg:{
        width: '100%',
        height: '100%',
        resizeMode: 'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Invertor)



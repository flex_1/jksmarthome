/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	Dimensions,
    Modal,
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import Slider from "react-native-slider";
import Panel from './Panel';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import TrackImageSlider from '../../../common/CustomComponent/TrackImageSlider';
import MultiSlider from '@ptomasroos/react-native-multi-slider';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const sliderWidth = (screenW - 40) * 0.9

class BleGroupLight extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		
		this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            lightVal: 0,
            tempVal: 10,
            status: 0,
            
            lightTime: 0,
			lightPercent: '100%',
            
            delayModal: false,
            currentColor: '#254FD7',
            colorTempModalVisible: false,

            minColorTemp: 1000,
            maxColorTemp: 10000,
            colorMin: 1000,
            colorMax: 10000,

            bleLightCode: 3,  //1-开关 2-调光  3-色温
		}
	}

	componentDidMount() {
		super.componentDidMount()
		this.requestDeviceStatus()
	}

	componentWillUnmount() {
        super.componentWillUnmount()
    }

    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        this.setState({
            lightVal: parseInt(data.light) || 0,
            tempVal: parseInt(data.cct) || 0,
            status: data.status
        })
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				let res = data.result?.deviceStatus
				try {
					res = JSON.parse(res)
					this.setState({
                        isCollect: data.result?.isCollect,
                        lightVal: parseInt(res.light) || 0,
                        tempVal: parseInt(res.cct) || 0,
                        status: res.status,
                        bleLightCode: data.result?.bleLightCode
					})
				} catch (error) {
					ToastManager.show('数据异常')
				}
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}
    
    // 灯光 控制
    async controlDevice(params,callBack){
		try {
			let data = await postJson({
				url: NetUrls.controlCommon, 
				timeout: NetParams.networkUserReactTimeoutTime,
				params: {
                    id : this.deviceData.deviceId,
                    ...params
                }
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

    //温度滚动结束
    _onComplete = ()=>{
        this.controlDevice({light: this.state.lightVal})
    }

    // 色温滚动结束
	_onTempComplete = ()=>{
		this.controlDevice({cct: this.state.tempVal})
    }
    
    // 色温
    _onTempChange = (value) => {
        this.setState({
            tempVal: Math.floor(value),
        })
    }

    // 亮度
	_onChange = (value) => {
        this.setState({
            lightVal: Math.floor(value),
        })
    }

    // 色温
    tempValueClick(value) {
        this.controlDevice({ cct: value },()=>{
            this.setState({
                tempVal: value
            })
        })
    }

    // 亮度
    valueClick(value) {
        this.controlDevice({ light: value },()=>{
            this.setState({
                lightVal: value
            })
        })
    }

    // 处于离线时的滚动条
    renderLightSlider(value){
        if(this.state.status != 0){
            return null
        }
        return(
            <Slider
                disabled={true}
			    style={{width:'90%'}}
			    minimumValue={0}
			    maximumValue={100}
			    step={1}
          	    value={value}
			    minimumTrackTintColor={'#D1D1D1'}
			    maximumTrackTintColor={'#E2E2E2'}
			    thumbTintColor={'#D1D1D1'}
			    trackStyle={{height:8,borderRadius: 4}}
			    thumbStyle={{width:30,height:30,borderRadius:15}}
            />
        )
    }

    // 渲染灯光页面
    renderLight(){
        const img = this.state.status == 1 ? require('../../../images/panel/bleLightOn.png') :
        require('../../../images/panel/bleLightOff.png')

        return(
            <View style={styles.topWrapper}>
                <Image style={styles.lightImg} source={img}/>
            </View>
        )
    }

    // 色温控制
    renderColorTempController(){
        if(this.state.bleLightCode != 3){
            return null
        }
        const Colors = this.props.themeInfo.colors
        const textColor = this.state.status == 1 ? Colors.themeText : Colors.themeTextLight

        const btnsArr = [0, 50, 100]
        const btnLabelArr = ['暖光','自然光','冷白光']
        const btns = btnsArr.map((value, index)=>{
            return(
                <TouchableOpacity key={'btns_'+index} style={styles.quickBtn} activeOpacity={0.7} onPress={()=>{
                    this.tempValueClick( parseInt(value) )
                }}>
                    {/* <View style={[styles.shortLine,{backgroundColor: Colors.themeTextLight}]}/> */}
                    <Text style={[styles.quickText,{color: Colors.themeTextLight}]}>{btnLabelArr[index]}</Text>
                </TouchableOpacity>
            )
        })

        return (
            <View style={[styles.sliderWrapper,{marginBottom: 10}]}>
                <View style={styles.title}>
                    <Text style={[styles.lightText,{color: textColor}]}>色温</Text>
                    {/* <Text style={[styles.valueText,{color: Colors.themeText}]}>{tempValue}K</Text>
                    <TouchableOpacity style={styles.adjustTouch} onPress={()=>{
                        this.setState({
                            colorMin: this.state.minColorTemp,
                            colorMax: this.state.maxColorTemp,
                            colorTempModalVisible: true
                        })
                    }}>
                        <Image style={styles.adjustImg} source={require('../../../images/panel/adjust.png')}/>
                    </TouchableOpacity> */}
                </View>
                {this.state.status == 1 ? <TrackImageSlider
                    style={{marginVertical: 5}}
                    value={this.state.tempVal}
                    sliderWidth={sliderWidth}
                    minimumValue={0}
                    maximumValue={100}
                    thumbTintColor={'#f5e85e'}
                    thumbWidth={30}
                    trackImage={require('../../../images/slider.png')}
                    onValueChange={this._onTempChange}
                    onSlidingComplete={this._onTempComplete}
                /> : null}
                {this.renderLightSlider(this.state.tempVal)}
                <View style={styles.quickBtnWrapper}>
                    {btns}
                </View>
            </View>
        )
    }

    // 调光控制器
    renderLightController(){
        if(this.state.bleLightCode == 1){
            return null
        }
        const Colors = this.props.themeInfo.colors
        const textColor = this.state.status == 1 ? Colors.themeText : Colors.themeTextLight
        const lightValue = this.state.lightVal

        const btnsArr = [0, 25, 50, 75, 100]
        const btns = btnsArr.map((value, index)=>{
            return(
                <TouchableOpacity key={'l_btns_'+index} style={[styles.quickBtn,{width: 28}]} activeOpacity={0.7} onPress={()=>{
                    this.valueClick(value)
                }}>
                    <View style={[styles.shortLine,{backgroundColor: Colors.themeTextLight}]}/>
                    <Text style={[styles.quickText,{color: Colors.themeTextLight}]}>{value}</Text>
                </TouchableOpacity>
            )
        })

        return (
            <View style={[styles.sliderWrapper,{marginBottom: 5}]}>
                <View style={styles.title}>
                    <Text style={[styles.lightText,{color: textColor}]}>亮度</Text>
                    <Text style={[styles.valueText,{color: textColor}]}>{lightValue}%</Text>
                </View>
                {this.state.status == 1 ? <Slider
					style={{width:'90%'}}
					minimumValue={0}
					maximumValue={100}
					step={1}
          			value={this.state.lightVal}
					onValueChange={this._onChange}
					onSlidingComplete={this._onComplete}
					minimumTrackTintColor={Colors.newTheme}
					maximumTrackTintColor={'#D4D4D4'}
					thumbTintColor={Colors.newTheme}
					trackStyle={{height:8,borderRadius: 4}}
					thumbStyle={{width:30,height:30,borderRadius:15}}
        		/> : null}
                {this.renderLightSlider(this.state.lightVal)}
                <View style={styles.quickBtnWrapper}>
                    {btns}
                </View>
            </View>
        )
    }

    renderController(){
        if(this.state.bleLightCode == 1){
            return null
        }
        return(
            <View style={{flex: 1}}>
                {this.renderColorTempController()}
                {this.renderLightController()}
            </View>
        )
    }

    renderBottomView(){
        const Colors = this.props.themeInfo.colors
        const img = this.state.status == 1 ? require('../../../images/panel/redSwitch_on.png')
        : require('../../../images/panel/redSwitch_off.png')

        return(
            <View style={[styles.bottomCtr,{backgroundColor: Colors.themeBg}]}>
                <TouchableOpacity activeOpacity={0.7} onPress={()=>{
                    const target = this.state.status == 1 ? 0:1
                    this.controlDevice({status: target},()=>{
                        this.setState({
                            status: target
                        })
                    })
                }}>
                    <Image style={styles.switchIcon} source={img}/>
                </TouchableOpacity>
            </View>
        )
    }

	getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

		return(
            <View style={[styles.main,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderLight()}
                {this.renderController()}
                {this.renderBottomView()}
            </View>
		)
	}
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
    },
	topWrapper:{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    lightImg:{
        width: screenW*0.4,
        height: screenW*0.4*1.2,
        resizeMode: 'contain'
    },
    title:{
        flexDirection: 'row',
        justifyContent:'center',
        alignItems: 'center',
        marginBottom: 10
    },
    lightText:{
        fontSize: 14,
        marginTop: 5,
        fontWeight: '400'
    },
    valueText:{
        fontSize: 24,
        fontWeight: '500',
        marginLeft: 10
    },
    delayTouch:{
        flexDirection: 'row',
        justifyContent:'center',
        alignItems: 'center',
        paddingVertical: 5,
        paddingHorizontal: 50,
        marginTop: 15,
    },
    delayText:{
        fontSize: 13,
        color: Colors.themeTextLightGray
    },
    downIcon:{
        width: 9,
        height: 5,
        resizeMode: 'contain',
        marginLeft: 10
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: 'rgba(0,0,0,0.5)' 
    },
    sliderWrapper:{
        width: '100%',
        paddingHorizontal: 20,
        justifyContent:'center',
        alignItems: 'center',
        flex: 1
    },
    bottomCtr:{
        paddingBottom: BottomSafeMargin,
        height: BottomSafeMargin + 120,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btns:{
        width:30,
        height:30,
        borderRadius:15,
        marginLeft:15
    },
    colorImg:{
        width:30,
        height:30,
        resizeMode:'contain'
    },
    quickBtnWrapper:{
        width: '90%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    quickBtn:{
        justifyContent:'center',
        alignItems:'center',
        marginTop: 5
    },
    quickText:{
        marginTop: 2,
        fontSize: 15
    },
    shortLine:{
        width: 2,
        height: 8
    },
    adjustTouch:{
        marginLeft:20,
        alignItems:'center',
        justifyContent:'center',
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    adjustImg:{
        width:22,
        height:22,
        resizeMode:'contain'
    },
    maker: {
		width: 25,
		height: 15,
		backgroundColor: 'transparent',
		resizeMode: 'contain'
    },
    colorTempModalWrapper:{
        backgroundColor: Colors.white,
        flex: 4,
        paddingBottom: BottomSafeMargin,
        alignItems:'center'
    },
    colorTempSliderWrapper:{
        width: '100%',
        justifyContent:'center',
        alignItems: 'center',
        marginTop: 50
    },
    colorTempTitle:{
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.themeTextBlack,
        marginTop: 15
    },
    colorTempSaveTouch:{
        height: 44, 
        backgroundColor: Colors.tabActiveColor,
        width: '90%',
        marginTop:30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5
    },
    switchIcon:{
        width: 48,
        height: 48
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(BleGroupLight)



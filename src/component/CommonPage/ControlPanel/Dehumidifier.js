/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import { Colors, NetUrls,NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import Panel from './Panel';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import { connect } from 'react-redux';
import { ImageBackground } from 'react-native';
import Slider from "react-native-slider";

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

const bottomControllerH = 180

class Dehumidifier extends Panel {

	constructor(props) {
		super(props);
        const { getParam } = props.navigation;
        
		this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

			status: 0,
            quickBtns: [{label: '干燥', key: 'min'},{label: '舒适', key: 'ave'},{label: '潮湿', key: 'max'}],
            roomHumi: 0,
            humSetting: 0,
            pattern: 0,
            speed: 0,
            anion: 0,
            lock: false,
            ErrorCode: '正常',
            cctScope: null,
            pm25: '-',
            tvoc: '-',
            co2: '-',
            temp: '-'
		}
	}

	componentDidMount() {
        super.componentDidMount()
        this.requestDeviceStatus()
    }

    componentWillUnmount() {
        super.componentWillUnmount()
    }

    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        this.setState({
            status: data.status == 1,
            anion: data.Anion == 1,
            lock: data.lock == 1,
            roomHumi: data.RoomHumidity,
            humSetting: parseInt(data.set_Hum),
            pattern: parseInt(data.pattern),
            speed: parseInt(data.speed),
			ErrorCode: data.ErrorCode
        })
    }

    // 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				let res = data.result && data.result.deviceStatus
				try {
					let resJson = JSON.parse(res)
					this.setState({
                        isCollect: data.result && data.result.isCollect,
						status: resJson.status == 1,
                        anion: resJson.Anion == 1,
                        lock: resJson.lock == 1,
                        roomHumi: resJson.RoomHumidity,
                        humSetting: parseInt(resJson.set_Hum),
                        pattern: parseInt(resJson.pattern),
                        speed: parseInt(resJson.speed),
						ErrorCode: resJson.ErrorCode,
                        cctScope: data.result.cctScope,
                        pm25: resJson.PM2_5,
                        tvoc: resJson.TVOC,
                        co2: resJson.CO2,
                        temp: resJson.temperature
					})
				} catch (error) {
					ToastManager.show('数据异常')
				}
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}
    
    // 控制
    async controlDevice(params,callBack){
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
                url: NetUrls.controlCommon,
                timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    
    // 湿度控制
    _onChange = (val) => {
        this.setState({
            humSetting: Math.floor(val),
        })
    }

    // 控制完成
    _complete = ()=>{
        this.controlDevice({set_Hum: this.state.humSetting})
    }

    // 获取图标或文字
    getBtnIconText(key, type){
        if(key == 'pattern'){
            if(this.state.pattern == 1){
                return type == 1 ? require('../../../images/panel/dehumidifier/pattern_1.png') : '普通'
            }else if(this.state.pattern == 2){
                return type == 1 ? require('../../../images/panel/dehumidifier/pattern_2.png') : '睡眠'
            }else{
                return type == 1 ? require('../../../images/panel/dehumidifier/pattern_0.png') : '连续'
            }
        }else if(key == 'speed'){
            if(this.state.speed == 1){
                return type == 1 ? require('../../../images/panel/dehumidifier/speed_1.png') : '高速'
            }else{
                return type == 1 ? require('../../../images/panel/dehumidifier/speed_0.png') : '低速'
            }
        }else if(key == 'status'){
            if(this.state.status == 0){
                return type == 1 ? require('../../../images/panel/dehumidifier/off.png') : '开机'
            }else{
                return type == 1 ? require('../../../images/panel/dehumidifier/on.png') : '关机'
            }
        }else if(key == 'anion'){
            if(this.state.anion == 1){
                return type == 1 ? require('../../../images/panel/dehumidifier/anionOn.png') : '负离子'
            }else{
                return type == 1 ? require('../../../images/panel/dehumidifier/anionOff.png') : '负离子'
            }
        }else{
            return null
        }
    }

    // 获取边界最小最大值
    getMinOrMax(type){
        let min = 0
        let max = 100
        if(this.state.cctScope){
            let arr = this.state.cctScope.split('-')
            min = parseInt(arr[0])
            max = parseInt(arr[1])
        }
        if(type == 'min'){
            return min
        }else if(type == 'max'){
            return max
        }else{
            return parseInt((min + max)/2 )
        }
    }

    renderEnvironment(){
        const Colors = this.props.themeInfo.colors
        const {pm25, tvoc, co2, temp} = this.state
        
        return(
            <View style={[styles.topEnviWrapper,{backgroundColor:Colors.themeBg}]}>
                <View style={styles.enviWrapper}>
                    <Text style={[styles.enviTitle,{color: Colors.themeTextLight}]}>PM2.5</Text>
                    <Text style={[styles.enviValue,{color: Colors.themeText}]}>{pm25}</Text>
                </View>
                <View style={[styles.split,{backgroundColor: Colors.themeBaseBg}]}/>
                <View style={styles.enviWrapper}>
                    <Text style={[styles.enviTitle,{color: Colors.themeTextLight}]}>TVOC</Text>
                    <Text style={[styles.enviValue,{color: Colors.themeText}]}>{tvoc}</Text>
                </View>
                <View style={[styles.split,{backgroundColor: Colors.themeBaseBg}]}/>
                <View style={styles.enviWrapper}>
                    <Text style={[styles.enviTitle,{color: Colors.themeTextLight}]}>CO₂</Text>
                    <Text style={[styles.enviValue,{color: Colors.themeText}]}>{co2}</Text>
                </View>
                <View style={[styles.split,{backgroundColor: Colors.themeBaseBg}]}/>
                <View style={styles.enviWrapper}>
                    <Text style={[styles.enviTitle,{color: Colors.themeTextLight}]}>温度</Text>
                    <Text style={[styles.enviValue,{color: Colors.themeText}]}>{temp}</Text>
                </View>
            </View>
        )
    }

    renderLocks(){
        const Colors = this.props.themeInfo.colors
        let lockImg = this.state.lock ? require('../../../images/panel/dehumidifier/lock.png')
        : require('../../../images/panel/dehumidifier/unlock.png')
        let lockText = this.state.lock ? '已上锁' : '已打开'
        let tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.white} : {}

        return(
            <TouchableOpacity activeOpacity={0.7} style={styles.lockTouch} onPress={()=>{
                let target = this.state.lock ? 0:1
                this.controlDevice({lock: target},()=>{
                    this.setState({lock: target})
                })
            }}>
                <Text style={{fontSize: 16,color: Colors.themeText}}>童锁</Text>
                <Text style={{fontSize: 16,marginLeft: 10,color: Colors.themeText,fontWeight:'500'}}>{lockText}</Text>
                <Image style={[styles.lockImg,tintColor]} source={lockImg}/>
            </TouchableOpacity>
        )
    }

	getCenterView(){
        const Colors = this.props.themeInfo.colors

		return(
            <ImageBackground 
                style={styles.centerViewWrapper} 
                source={require('../../../images/panel/dehumidifier/centerIcon.png')}
            >
                <Text style={styles.centerText}>{this.state.roomHumi}</Text>
                <Text style={[styles.centerTopText,{color: Colors.themeText}]}>室内湿度（%RH）</Text>
                {this.state.ErrorCode ? <Text style={styles.centerBottomText} numberOfLines={2}>{this.state.ErrorCode}</Text> : null}
            </ImageBackground>
		)
    }

    renderBtns(){
        const Colors = this.props.themeInfo.colors

        let btns = this.state.quickBtns.map((value, index)=>{

            return(
                <TouchableOpacity style={{padding: 5}} key={index} activeOpacity={0.7} onPress={()=>{
                    let num = this.getMinOrMax(value.key)
                    this.controlDevice({set_Hum: num},()=>{
                        this.setState({humSetting: num})
                    })
                }}>
                    <Text style={{color: Colors.themeTextLight,fontSize:14}}>{value.label}</Text>
                </TouchableOpacity>
            )
        })
        return (
            <View style={{flexDirection:'row', width:'90%', justifyContent:'space-between',marginTop: 5}}>
                {btns}
            </View>
        )
    }

    getSliderView(){
        const Colors = this.props.themeInfo.colors
        let humSetting = this.state.humSetting
        if(humSetting < this.getMinOrMax('min')){
            humSetting = this.getMinOrMax('min')
        }else if(humSetting > this.getMinOrMax('max')){
            humSetting = this.getMinOrMax('max')
        }
        
        return(
            <View style={styles.sliderWrapper}>
                <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                    <Text style={[styles.ctrText,{color: Colors.themeText}]}>设置湿度</Text>
                    <Text style={[styles.ctrValue,{color: Colors.themeText}]}>{this.state.humSetting}%</Text>
                </View>
                <Slider
					style={{width:'90%'}}
					minimumValue={this.getMinOrMax('min')}
					maximumValue={this.getMinOrMax('max')}
					step={1}
          			value={humSetting}
					onValueChange={this._onChange}
					onSlidingComplete={this._complete}
					minimumTrackTintColor='#3AD3E3'
					maximumTrackTintColor={'#CACBCC'}
					thumbTintColor='#3AD3E3'
					trackStyle={{height:8,borderRadius: 4}}
					thumbStyle={{width:30,height:30,borderRadius:15}}
        		/>
                {this.renderBtns()}
            </View>
        )
    }

	getBottomView(){
        const Colors = this.props.themeInfo.colors

		return(
            <View style={{backgroundColor: Colors.themeBaseBg}}>
			    <View style={[styles.bottomWrapper,{backgroundColor: Colors.themeBg}]}>
                    <View style={{flexDirection:'row'}}>
                        <TouchableOpacity style={styles.bottomBtn} activeOpacity={0.7} onPress={()=>{
                            let target = (this.state.pattern + 1)%3
                            this.controlDevice({pattern: target},()=>{
                                this.setState({pattern: target})
                            })
                        }}>
                            <Image source={this.getBtnIconText('pattern',1)} style={styles.bottomBtnImg}></Image>
                            <Text style={[styles.bottomBtnText,{color: Colors.themeText}]}>{this.getBtnIconText('pattern',2)}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.bottomBtn} activeOpacity={0.7} onPress={()=>{
                            let target = this.state.anion ? 0:1
                            this.controlDevice({Anion: target},()=>{
                                this.setState({anion: target})
                            })
                        }}>
                            <Image source={this.getBtnIconText('anion',1)} style={styles.bottomBtnImg}></Image>
                            <Text style={[styles.bottomBtnText,{color: Colors.themeText}]}>{this.getBtnIconText('anion',2)}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.bottomBtn} activeOpacity={0.7} onPress={()=>{
                            let target = (this.state.speed + 1)%2
                            this.controlDevice({speed: target},()=>{
                                this.setState({speed: target})
                            })    
                        }}>
                            <Image source={this.getBtnIconText('speed',1)} style={styles.bottomBtnImg}></Image>
                            <Text style={[styles.bottomBtnText,{color: Colors.themeText}]}>{this.getBtnIconText('speed',2)}</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity
                        style={{marginTop: 25}}
                        onPress={()=>{
                            let target = this.state.status ? 0:1
                            this.controlDevice({status: target},()=>{
                                this.setState({status: target})
                            })
					    }}
                    >
					    <Image style={styles.bottomBtnImg} source={this.getBtnIconText('status',1)}/>
				    </TouchableOpacity>
			    </View>
            </View>
		)
    }

    // 首页面
    getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

		return(
			<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
                {this.renderEnvironment()}
                <View style={[styles.topWrapper,{backgroundColor: Colors.themeBaseBg}]}>
                    {this.renderLocks()}
                    {this.getCenterView()}
                    {this.getSliderView()}
                </View>
				{this.getBottomView()}
			</View>
		)
    }
}

const styles = StyleSheet.create({
	main: {
		flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
    topWrapper: {
        flex: 1,
        justifyContent:'center',
        alignItems:'center'
    },
    centerViewWrapper:{
        justifyContent:'center',
        alignItems:'center',
        width: screenW * 0.5,
        height: screenW * 0.5,
        resizeMode: 'contain',
        paddingHorizontal: 16
    },
    centerText:{
        fontSize: 40,
        color: Colors.white, 
        fontWeight: '500',
        marginTop:10,
        fontFamily: 'Avenir Medium'
    },
    centerTopText:{
        position:'absolute',
        top: '15%',
        textAlign: 'center',
        width: '100%',
        fontSize: 12
    },
    centerBottomText:{
        position:'absolute',
        bottom: '15%',
        width: '100%',
        textAlign: 'center',
        fontSize: 14,
        color: Colors.white
    },
    ctrText:{
        fontSize: 14,
        marginRight: 10
    },
    ctrValue:{
        fontSize: 26,
        fontWeight: 'bold'
    },
	bottomWrapper:{
        height:bottomControllerH + BottomSafeMargin,
        paddingBottom: BottomSafeMargin,
		width:'100%',
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal: 16
	},
    bottomBtn:{
        flex: 1,
        marginHorizontal: 8,
        justifyContent:'center',
        alignItems:'center',    
    },
    bottomBtnImg:{
        width: screenW * 0.12,
        height: screenW * 0.12,
        resizeMode: 'contain'
    },
    bottomBtnText:{
        fontSize: 15,
        marginTop: 12,
        color: Colors.themeTextBlack
    },
    sliderWrapper:{
        justifyContent:'center',
        alignItems:'center',
        width:'100%',
        paddingHorizontal:16,
        marginTop: screenH * 0.04
    },
    lockImg:{
        width: 18, 
        height: 18, 
        resizeMode:'contain',
        marginLeft: 5
    },
    lockTouch:{
        flexDirection:'row',
        alignItems:'center',
        paddingVertical: 10,
        marginBottom: 5
    },
    enviWrapper:{
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal: 15
    },
    enviTitle:{
        fontSize:12,
        fontWeight:'400'
    },
    enviValue:{
        fontSize:13,
        fontWeight:'400',
        marginTop:8
    },
    split:{
        width:1, 
        height:22
    },
    topEnviWrapper:{
        width:'100%',
        paddingVertical:10,
        flexDirection: 'row',
        justifyContent:'center',
        alignItems:'center'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Dehumidifier)



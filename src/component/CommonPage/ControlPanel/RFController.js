/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter
} from 'react-native';
import { Colors, NetUrls,NetParams } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import Panel from './Panel';
import { connect } from 'react-redux';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';

class RFController extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		
		this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,
			
		}
	}

	componentDidMount() {
		
    }
    
    componentWillUnmount() {
        
    }

    // 控制
    async controlDevice(params){
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }
    
    getCenterView(){
        return (
            <View style={styles.centerWrapper}>
				<TouchableOpacity
					activeOpacity={0.5}
					style={[styles.ctrTouch, {borderTopLeftRadius:8,borderTopRightRadius:8 }]}
					onPress={()=>{
						this.controlDevice({number:10})
					}}
				>
					<Text style={styles.ctrText}>打开</Text>
				</TouchableOpacity>
				<View style={styles.split}/>
				<TouchableOpacity
					activeOpacity={0.5}
					style={styles.ctrTouch}
					onPress={()=>{
						this.controlDevice({number:12})
					}}
				>
					<Text style={styles.ctrText}>暂停</Text>
				</TouchableOpacity>
				<View style={styles.split}/>
				<TouchableOpacity
					activeOpacity={0.5}
					style={[styles.ctrTouch,{borderBottomLeftRadius:8,borderBottomRightRadius:8} ]}
					onPress={()=>{
						this.controlDevice({number:11})
					}}
				>
					<Text style={styles.ctrText}>关闭</Text>
				</TouchableOpacity>
			</View>
        )
    }

	getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

		return(
			<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
				{this.getCenterView()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
    centerWrapper:{
        justifyContent: 'center',
        alignItems:'center',
        flex: 1
    },
	ctrTouch:{
		backgroundColor:Colors.tabActiveColor,
		height:70,
		width:'30%',
		justifyContent:'center',
		alignItems:'center'
	},
	split:{
		height:3,
		width:'100%',
		backgroundColor: Colors.transparency
	},
	ctrText:{
		fontSize: 15,
		color:Colors.white
	}
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(RFController)



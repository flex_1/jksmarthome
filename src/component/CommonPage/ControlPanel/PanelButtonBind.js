/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Animated,
	Dimensions,
    Modal,
    Keyboard
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel';
import { connect } from 'react-redux';
import DateUtil from '../../../util/DateUtil';
import {ColorsDark, ColorsLight} from '../../../common/Themes';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const keybordHeight = 180

class PanelButtonBind extends Panel {
	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData') || {}
        
        this.state = {
            ...this.state,
            isWhiteBg: true,

            currentSelect: 1,  //1：场景  2：设备,
            loading: false,
            sceneList: null,
            deviceList: null,
            currentPage: 0,
            totalPage: 0,
            modalVisible: false,
            roomselectedData: [],
            pickerData: [],
            allRoomIds: [],
            currentRoomId: this.deviceData.roomId,
            showSearch: false,
            searchText: '',
            translateValue:new Animated.ValueXY({x:0,y:0}),
            panelId: null,

            buttonType: 0,
            buttons: []
        }
    }
    
	componentDidMount() {
        super.componentDidMount()

        // this.requestPanelView()
        this.requestSceneList()
        this.requestRoomList()

        if(this.deviceData.floorText && this.deviceData.roomName){
            this.setState({
                roomselectedData: [this.deviceData.floorText, this.deviceData.roomName]
            })
        }
	}

	componentWillUnmount(){
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    // updateDeviceStatus(data){
    //     if(data.deviceId != this.deviceData.deviceId) return;
        
	// 	this.setState({
            
    //     })
    // }

    // 动画向上位移
    animatedYtoTop(){
        Animated.timing(
            this.state.translateValue,
            {
                toValue: {
                    x:0,
                    y:-keybordHeight
                },
                duration: 300,
                delay:0,
            }
        ).start();
    }

    // 动画 还原
    animatedYtoOrigin(){
        Animated.timing(
            this.state.translateValue,
            {
                toValue: {
                    x:0,
                    y:0
                },
                duration: 300,
                delay:0,
            }
        ).start();
    }

	// 获取面板按钮
    async requestPanelView(){
		try {
			let data = await postJson({
				url: NetUrls.panelView,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
                
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    // 获取场景列表
	async requestSceneList(params) {
		params = params || {}
        params.roomId = this.state.currentRoomId
        params.sceneName = this.state.searchText
        this.setState({loading:true})
		try {
			let data = await postJson({
				url: NetUrls.sceneList,
				params: params
			});
			this.setState({
				loading: false
			})
			if (data.code == 0) {
                let sceneList = []
                if(params.page && params.page > 1){
                    sceneList = this.state.sceneList.concat(data.result.list)
                }else{
                    sceneList = data.result?.list || []
                }
				this.setState({
					sceneList: sceneList,
                    currentPage: data.result?.curPage,
                    totalPage: data.result?.totalPageNum
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({ 
				loading: false
			})
			ToastManager.show('网络错误');
		}
	}

    // 获取设备列表
    async requestDeviceList(params){
        params = params || {}
        params.roomid = this.state.currentRoomId
        params.deviceName = this.state.searchText
		this.setState({loading:true})
		try {
			let data = await postJson({
				url: NetUrls.deviceList,
				params: params
			});
			this.setState({loading: false,isRefreshing: false})
			if (data.code == 0) {
                let deviceList = []
                if(params.page && params.page > 1){
                    deviceList = this.state.deviceList.concat(data.result.list)
                }else{
                    deviceList = data.result.list || []
                }
				this.setState({
					deviceList: deviceList,
                    totalPage: data.result?.totalPageNum,
                    currentPage: data.result?.curPage
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            if(error.code == NetParams.networkBreak){
                return
            }
			this.setState({loading: false,isRefreshing: false})
			ToastManager.show('网络错误')
		}
    }

    // 获取房间 列表
	async requestRoomList() {
		try {
			let data = await postJson({
				url: NetUrls.roomList,
				params: {
                    type: 2
				}
			})
			if (data.code == 0) {
				const floorData = data.result || []
                let pickerData = [{'全部楼层':['全部房间']}]
                let allRoomIds = [[0]]
                for (const floor of floorData) {
                    let f_name = {}
                    let r_names = []
                    let r_ids = []
                    for (const room of floor.listRoom) {
                        r_names.push(room.name)
                        r_ids.push(room.id)
                    }
                    f_name[floor.floorText] = r_names
                    allRoomIds.push(r_ids)
                    pickerData.push(f_name)
                }
                this.setState({
                    pickerData: pickerData,
                    allRoomIds: allRoomIds
                })
			} else {
				ToastManager.show(data.msg);
			}
		} catch (e) {
			ToastManager.show('网络错误');
		}
	}

    search(){
        Keyboard.dismiss()
        this.setState({
            sceneList: null,
            deviceList: null,
            currentPage: 0,
            totalPage: 0
        },()=>{
            if(this.state.currentSelect == 1){
                this.requestSceneList()
            }else{
                this.requestDeviceList()
            }
        })
    }

    onLoadNextPage(){
        if(this.state.loading){
            return
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return
        }
        if(this.state.currentSelect == 1){
            this.requestSceneList({page: this.state.currentPage+1})
        }else{
            this.requestDeviceList({page: this.state.currentPage+1})
        }
    }
    
    renderHeadImage(){
        let panelImg = require('../../../images/panel/panelBind/panel_2.png')
        if(this.state.panelId == 4){
            panelImg = require('../../../images/panel/panelBind/panel_3.png')
        }else if(this.state.panelId == 5){
            panelImg = require('../../../images/panel/panelBind/panel_1.png')
        }
        return(
            <View style={styles.topWrapper}>
                <Image style={styles.topImg} source={panelImg}/>
            </View>
        )
    }

    renderHeadButtons(){

    }

    renderNoContent(){
        if(this.state.loading){
            return (
                <View style={styles.noContentWrapper}>
                    <Text style={styles.noContentText}>正在加载...</Text>
                </View>
            )
        }else{
            let nocontentText = this.state.currentSelect == 1 ? '暂无场景' : '暂无设备'
            return (
                <View style={styles.noContentWrapper}>
                    <Text style={styles.noContentText}>{nocontentText}</Text>
                </View>
            )
        } 
    }

    renderRowItem(rowData, index){
        const icon = this.state.currentSelect == 1 ? rowData.dayIconOn : rowData.dayImg
        const name = this.state.currentSelect == 1 ? rowData.name : rowData.deviceName
        let roomView = null
        if(rowData.floor && rowData.roomName){
            roomView = <Text style={styles.roomName}>{rowData.floorText + ' ' + rowData.roomName}</Text>
        }

        return(
            <TouchableOpacity activeOpacity={0.7} style={styles.item} onPress={()=>{
                ToastManager.show('功能待开发，敬请期待。')
            }}>
                <Image style={styles.icon} source={{uri: icon}}/>
                <View style={styles.nameWrapper}>
                    <Text style={styles.name}>{name}</Text>
                    {roomView}
                </View>
                <Image style={styles.selectedImg} source={require('../../../images/connectWifi/unselected.png')}/>
            </TouchableOpacity>
        )
    }   

    renderList(){
        const listData = this.state.currentSelect == 1 ? this.state.sceneList : this.state.deviceList

        return(
            <FlatList
                ref={e => this.flatlist = e}
                contentContainerStyle={styles.contentStyle}
                scrollIndicatorInsets={{right: 1}}
                removeClippedSubviews={false}
                data={listData}
                keyExtractor={(item, index)=> 'list_item_'+index}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
                initialNumToRender={10}
                ListEmptyComponent = {this.renderNoContent()}
                onEndReached={() => this.onLoadNextPage()}
                onEndReachedThreshold={0.1}
                keyboardDismissMode={'on-drag'}
                keyboardShouldPersistTaps={'handled'}
            />
        )
    }

    renderSearchInput(){
        if(!this.state.showSearch){
            return null
        }
        return(
            <View style={styles.searchWrapper}>
                <View style={styles.inputWrapper}>
                    <TextInput
                        ref={e => this.keyBord = e}
                        style={styles.input}
                        value={this.state.searchText}
                        placeholder={'请输入关键字'}
                        placeholderTextColor={Colors.themeBGInactive}
                        returnKeyType={'search'}
                        onChangeText={(text) => {
                            this.setState({
                                searchText : text
                            })
                        }}
                        onSubmitEditing={()=>{
                            Keyboard.dismiss()
                            this.search()
                        }}
                        onFocus={()=>{
                            this.animatedYtoTop()
                        }}
                        onBlur={()=>{
                            this.animatedYtoOrigin()
                        }}
                    />
                    <TouchableOpacity activeOpacity={0.7} style={styles.clearBtn} onPress={()=>{
                        this.setState({
                            searchText: ''
                        })
                    }}>
                        <Image style={styles.clearImg} source={require('../../../images/panel/panelBind/clear.png')}/>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity activeOpacity={0.7} style={styles.searchBtn} onPress={()=>{
                    this.search()
                }}>
                    <Text style={styles.searchText}>搜索</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderBottomControlView(){
        const underLine = <View style={styles.underLine}/>
        const sel_style = {fontSize: 14,fontWeight:'500',color: Colors.themeTextBlack}
        const unsel_style = {fontSize: 14, fontWeight:'400',color: Colors.newTextGray}
        const filter_style = this.state.currentRoomId ? {tintColor: Colors.tabActiveColor} : {}
        
        return(
            <Animated.View style={{flex: 1,marginTop:this.state.translateValue.y}}>
                <View style={styles.headWraper}>
                    <TouchableOpacity style={styles.headerBtn} onPress={()=>{
                        this.setState({
                            currentSelect: 1,
                            currentPage: 0,
                            totalPage: 0,
                            deviceList: null 
                        })
                        this.requestSceneList()
                    }}>
                        <Text style={this.state.currentSelect == 1 ? sel_style : unsel_style}>场景</Text>
                        {this.state.currentSelect == 1 ? underLine : null}
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.headerBtn,{marginLeft: 30}]} onPress={()=>{
                        this.setState({
                            currentSelect: 2,
                            currentPage: 0,
                            totalPage: 0,
                            sceneList: null 
                        })
                        this.requestDeviceList()
                    }}>
                        <Text style={this.state.currentSelect == 2 ? sel_style : unsel_style}>设备</Text>
                        {this.state.currentSelect == 2 ? underLine : null}
                    </TouchableOpacity>
                    <View style={{flex: 1}}/>
                    <TouchableOpacity style={styles.headerBtn} onPress={()=>{
                        this.setState({
                            modalVisible: true
                        })
                    }}>
                        <Image style={[styles.filterIcon,filter_style]} source={require('../../../images/panel/panelBind/filter.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.headerBtn]} onPress={()=>{
                        if(this.state.showSearch){
                            this.animatedYtoOrigin()
                        }
                        this.setState({
                            searchText: '',
                            showSearch: !this.state.showSearch
                        },()=>{
                            if(!this.state.showSearch){
                                this.search()
                            }
                        })
                    }}>
                        <Image style={styles.filterIcon} source={require('../../../images/panel/panelBind/search.png')}/>
                    </TouchableOpacity>
                </View>
                {this.renderSearchInput()}
                <View style={{flex: 1,backgroundColor: Colors.themBGLightGray}}>
                    {this.renderList()}
                </View>
            </Animated.View>
        )
    }

    // 获取选择框的 Modal
	getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
	}

    handlePickerSelectData(data, index){
        const i_0 = index[0]
        const i_1 = index[1]
        const roomId = this.state.allRoomIds[i_0][i_1]
        const roomselectedData = [data[0], data[1]]
        this.setState({
            roomselectedData: roomselectedData,
            sceneList: null,
            deviceList: null,
            currentPage: 0,
            totalPage: 0,
            currentRoomId: roomId
        },()=>{
            if(this.state.currentSelect == 1){
                this.requestSceneList()
            }else{
                this.requestDeviceList()
            }
        })
    }
	
	// 弹出 Modal
    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerData,
            pickerTitleText: '房间筛选',
            selectedValue: this.state.roomselectedData,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false
                })
                this.handlePickerSelectData(data,index);
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                }, () => {
                    window.CustomPicker.hide()
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

    getMainPanelPage(){
        return(
            <View style={{flex: 1}}>
                {this.renderHeadImage()}
                {this.renderBottomControlView()}
                {this.getModal()}
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors
        
		return (
			<View style={[styles.main,{backgroundColor: 'rgba(243,248,248,1)'}]}>
				{this.getHeaderView()}
				{this.getMainPanelPage()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
    },
    headWraper:{
        height: 44, 
        width:'100%',
        backgroundColor: Colors.white,
        paddingLeft: 16,
        flexDirection: 'row'
    },
    headerBtn:{
        paddingHorizontal: 16,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    underLine:{
        position: 'absolute',
        height: 3,
        borderRadius: 1.5,
        width: 40,
        backgroundColor: Colors.tabActiveColor,
        bottom: 0,
        alignSelf: 'center'
    },
    contentStyle:{
        paddingBottom: BottomSafeMargin + 10
    },
    item:{
        height: 54, 
        marginHorizontal: 16,
        paddingHorizontal: 16,
        borderRadius: 5,
        flexDirection: "row",
        alignItems: 'center',
        
        marginTop: 10,
        backgroundColor: Colors.white
    },
    icon:{
        width: 24,
        height: 24,
        resizeMode: 'center'
    },
    name:{
        fontSize: 14,
        color: Colors.themeTextBlack,
        fontWeight: '400'
    },
    selectedImg:{
        width: 16,
        height:16,
        resizeMode: 'contain'
    },
    filterIcon:{
        width: 16,
        height: 17,
        resizeMode: 'contain'
    },
    roomName:{
        fontSize: 13,
        color: Colors.themeTextLightGray,
        marginTop: 5
    },
    nameWrapper:{
        marginLeft: 10,
        flex: 1
    },
    topWrapper:{
        height: screenH * 0.4, 
        justifyContent:'center', 
        alignItems: 'center'
    },
    topImg:{
        width:'100%', 
        height: screenH * 0.4, 
        resizeMode:'contain'
    },
    noContentWrapper:{ 
        marginTop: screenH*0.1, 
        justifyContent:'center',
        alignItems: 'center'
    },
    noContentText:{
        fontSize: 14, 
        color: Colors.themeTextLightGray
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    searchWrapper:{
        height: 52, 
        width:'100%', 
        backgroundColor: Colors.white,
        borderBottomColor: Colors.borderLightGray,
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 16
    },
    inputWrapper:{
        backgroundColor:Colors.themBGLightGray,
        height: 36,
        borderRadius: 18,
		flex:1,
		alignItems:'center',
		flexDirection:'row',
        paddingLeft: 10,
        // paddingVertical: Platform.select({
        //     android: 0,
        //     ios: 10
        // })
    },
    input:{
        flex:1,
        paddingLeft:10,
        fontSize: 14,
        height: '100%'
    },
    searchBtn:{
        height:'100%',
        paddingHorizontal: 10, 
        marginLeft: 5,
        justifyContent: 'center',
        alignItems:'center'
    },
    searchText:{
        fontSize: 16, 
        fontWeight: '400', 
        color: Colors.themeTextBlack
    },
    clearBtn:{
        height:'100%',
        paddingHorizontal: 16,
        justifyContent:'center',
        alignItems:'center'
    },
    clearImg:{
        width:16,
        height:16,
        resizeMode: 'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(PanelButtonBind)



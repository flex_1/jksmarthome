/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Alert,
    Dimensions,
    PanResponder,
    Animated,
    DeviceEventEmitter
} from 'react-native';
import { Colors, NetUrls, NetParams, NotificationKeys } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight, BottomSafeMargin } from '../../../util/ScreenUtil';
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { LineChart } from "react-native-chart-kit";

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class SwiperPanel extends Component {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        const { getParam } = props.navigation;
        this.deviceData = getParam('deviceData')

        this.state = {
            titleColorFlag: false,
            
            /**用电相关 */
			indicators: ['日', '周', '月', '年'],
			select: '日',
        }

        // 初始状态
        this._panResponder = PanResponder.create({
            onMoveShouldSetPanResponderCapture: this._handleMoveShouldSetPanResponderCapture.bind(this),
            onStartShouldSetResponderCapture: this._handleMoveShouldSetPanResponderCapture.bind(this),
            onPanResponderMove: this._handlePanResponderMove.bind(this),
            onPanResponderRelease: this._handlePanResponderEnd.bind(this),
            onPanResponderGrant: this._handlePanGrant.bind(this),
            onPanResponderTerminate: () => {
                console.log('onPanResponderTerminate');
                this._aniBack1.setValue(0);
                Animated.spring(this._aniBack1, {
                    toValue: 1
                }).start(() => {
                    this._handlePanResponderEnd();
                });
            },
            onShouldBlockNativeResponder: (event, gestureState) => false,//表示是否用 Native 平台的事件处理，默认是禁用的，全部使用 JS 中的事件处理，注意此函数目前只能在 Android 平台上使用
        });
        this._panResponder2 = PanResponder.create({
            onMoveShouldSetPanResponderCapture: this._handleMoveShouldSetPanResponderCapture2.bind(this),
            onStartShouldSetResponderCapture: this._handleMoveShouldSetPanResponderCapture2.bind(this),
            onPanResponderMove: this._handlePanResponderMove2.bind(this),
            onPanResponderRelease: this._handlePanResponderEnd2.bind(this),
            onPanResponderGrant: this._handlePanGrant2.bind(this),
            onPanResponderTerminate: () => {
                this._container2.setNativeProps({
                    marginTop: 0
                })
                this._aniBack2.setValue(0);
                Animated.spring(this._aniBack2, {
                    toValue: 1
                }).start(() => {
                    this._handlePanResponderEnd2();
                });
                console.log('onPanResponderTerminate2');
            },
            onShouldBlockNativeResponder: (event, gestureState) => false,//表示是否用 Native 平台的事件处理，默认是禁用的，全部使用 JS 中的事件处理，注意此函数目前只能在 Android 平台上使用
        });
        this._reachEnd1 = false;
        this._reachEnd2 = true;
        this._aniBack = new Animated.Value(0);
        this._aniBack1 = new Animated.Value(0);
        this._aniBack2 = new Animated.Value(0);
    }

    componentDidMount() {
        //this.requestElectricityData(1)
    }

    componentWillUnmount() {
        
    }

    // 请求电量数据
    async requestElectricityData(type) {
        this.setState({loadingElectric: true})
        try {
            let data = await postJson({
                url: NetUrls.electricity,
                params: {
                    deviceId: this.deviceData.deviceId,
                    type: type
                }
            });
            this.setState({loadingElectric: false})
            if (data.code == 0) {
                this.setState({
                    electricityData: data.result || {}
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({loadingElectric: false})
            ToastManager.show(JSON.stringify(error))
        }
    }

    async requestDeviceStatus() {

    }

    _handlePanGrant(event, gestureState ) {
        this._scroll1.setNativeProps({
            scrollEnabled: false
        })
    }
    _handleMoveShouldSetPanResponderCapture(event, gestureState ) {
        
        return this._reachEnd1 && gestureState.dy < 0;
    }
    _handlePanResponderMove(event, gestureState) {
        this._scroll1.setNativeProps({
            scrollEnabled: false
        })
        let nowLeft = gestureState.dy * 0.5;
        this._container1.setNativeProps({
            marginTop: nowLeft
        })
    }
    _handlePanResponderEnd(event, gestureState) {
        this._aniBack.setValue(0);
        this._scroll1.setNativeProps({
            scrollEnabled: true
        })
        this._scroll1.scrollTo({ y: 0 }, true);
        Animated.timing(this._aniBack, {
            duration: 500,
            toValue: 1
        }).start();
        this._aniBack1.setValue(1);
        Animated.spring(this._aniBack1, {
            toValue: 0
        }).start();
        this.setState({
            titleColorFlag: true
        })
    }
    _handleMoveShouldSetPanResponderCapture2(event, gestureState, ) {
        return this._reachEnd2 && gestureState.dy >= 10;
    }
    _handlePanResponderMove2(event, gestureState) {
        let nowLeft = gestureState.dy * 0.5;
        this._scroll2.setNativeProps({
            scrollEnabled: false
        })
        this._container2.setNativeProps({
            marginTop: -100 + nowLeft
        })
    }
    _handlePanGrant2(event, gestureState, ) {
        this._scroll2.setNativeProps({
            scrollEnabled: false
        })
    }
    _handlePanResponderEnd2(event, gestureState) {
        this._aniBack.setValue(1);
        this._scroll2.setNativeProps({
            scrollEnabled: true
        })
        Animated.timing(this._aniBack, {
            duration: 500,
            toValue: 0
        }).start();
        this._aniBack2.setValue(1);
        Animated.spring(this._aniBack2, {
            toValue: 0
        }).start();
        this.setState({
            titleColorFlag: false
        })
    }
    _onScroll(event) {
        this._reachEnd1 = false;
        let y = event.nativeEvent.contentOffset.y;
        let height = event.nativeEvent.layoutMeasurement.height;
        let contentHeight = event.nativeEvent.contentSize.height;
        if (contentHeight > height && y > 0) {
            this._reachEnd1 = true;
        }
    }
    _onScroll2(event) {
        this._reachEnd2 = false;
        let y = event.nativeEvent.contentOffset.y;
        if (y <= 0) {
            this._reachEnd2 = true;
        }
    }

    // 获取 头部
    getHeaderView() {
        const { navigate, pop, push } = this.props.navigation;

        return (
            <View style={styles.header}>
                <View style={styles.status} />
                <View style={styles.nav}>
                    <TouchableOpacity style={styles.backTouch} onPress={() => {
                        pop()
                    }}>
                        <Image style={{ width: 10, height: 18 }} source={this.state.titleColorFlag ? require('../../../images/back.png') : require('../../../images/back_white.png')} />
                        <Text style={this.state.titleColorFlag
                            ? { marginLeft: 15, fontSize: 24, color: Colors.themeTextBlack, fontWeight: 'bold' }
                            : { marginLeft: 15, fontSize: 24, color: Colors.white, fontWeight: 'bold' }}>{this.state.title}</Text>
                    </TouchableOpacity>
                    <View style={{ flex: 1 }} />
                    <TouchableOpacity style={styles.navTouch} onPress={() => {
                        push('DeviceSetting', { 
                            deviceData: this.deviceData,
                            nameCallBack: (name)=>{
                                this.setState({ title: name })
                            }
                        })
                    }}>
                        <Image style={styles.navImg} source={this.state.titleColorFlag ? require('../../../images/setting.png') : require('../../../images/setting_white.png')} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

	/**
	 * 电量页面- 日周月年
	 * */
    getHeaderSegment() {
        let list = this.state.indicators.map((val, index) => {
            let bgStyle = Colors.white
            let tetxStyle = Colors.themeBGInactive
            if (this.state.select == val) {
                bgStyle = Colors.themeBG
                tetxStyle = Colors.white
            }
            return (
                <View style={styles.segmentWrapper} key={'energy_index_' + index}>
                    <TouchableOpacity style={[styles.segmentTouch, { backgroundColor: bgStyle }]} onPress={() => {
                        this.setState({
                            select: val,
                            showPopCell: false
                        })
                        this.requestElectricityData(index + 1)
                    }}>
                        <Text style={{ color: tetxStyle, fontSize: 13 }}>{val}</Text>
                    </TouchableOpacity>
                </View>
            )
        })
        return (
            <View style={{ marginTop: 10, paddingHorizontal: 12, flexDirection: 'row' }}>
                {list}
            </View>
        )
    }

    /**
     * 电量页面--用电量与电价
     * */
    getEletricDetaiView() {
        let quantity = 0
        let quantityIcon = null
        let price = 0
        let priceIcon = null

        if (this.state.electricityData && this.state.electricityData.quantity) {
            quantity = this.state.electricityData.quantity
            quantityIcon = <Image style={styles.sameIcon} source={require('../../../images/user_manage/same.png')} />
        }
        if (this.state.electricityData && this.state.electricityData.price) {
            price = this.state.electricityData.price
            priceIcon = <Image style={styles.upIcon} source={require('../../../images/user_manage/up.png')} />
        }

        return (
            <View style={styles.headerContain}>
                <View style={styles.cardWrapper}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontSize: 30, fontWeight: 'bold' }}>{quantity || '0'}</Text>
                        <Text style={styles.unit}>kWH</Text>
                    </View>
                    {quantityIcon}
                    <Text style={styles.electTips}>本{this.state.select}用电量</Text>
                </View>
                <View style={styles.cardWrapper}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontSize: 15, color: Colors.themeTextBlack, marginTop: 10 }}>￥</Text>
                        <Text style={styles.price}>{price || '0'}</Text>
                    </View>
                    {priceIcon}
                    <Text style={styles.electTips}>总电费</Text>
                </View>
            </View>
        )
    }

    getXDate(timeStamp){
        let currentSelect = this.state.select
        try {
            let d = new Date(timeStamp)
            var year = d.getFullYear(); 
            var month = d.getMonth()+1; 
            var date = d.getDate(); 
            var hour = d.getHours(); 
            var minute = d.getMinutes(); 
            var second = d.getSeconds();

            switch (currentSelect) {
                case '日': return hour + '时'
                case '周': return date + '日'
                case '月': return date + '日'
                case '年': return month + '月'
                default:   return 0
            }
        } catch (error) {
            return 0    
        }
    }

    _chart() {
        //测试数据（可以直接删除）
        // this.state.electricityData = {}
        // this.state.electricityData.list =  [
        //     { "quantity": 0,"times": 1569254400000},
        //     { "quantity": 1.2,"times": 1569254400000},
        //     { "quantity": 2.2,"times": 1569254400000},
        //     { "quantity": 1.3,"times": 1569254400000},
        //     { "quantity": 8.2,"times": 1569254400000},
        //     { "quantity": 0.1,"times": 1569254400000},
        //     { "quantity": 0,"times": 1569254400000},
        //     { "quantity": 1.2,"times": 1569254400000},
        //     { "quantity": 2.2,"times": 1569254400000},
        //     { "quantity": 1.3,"times": 1569254400000},
        //     { "quantity": 8.2,"times": 1569254400000},
        //     { "quantity": 19.1,"times": 1569254400000},
        // ]
        
        if(this.state.loadingElectric || !this.state.electricityData){
            return(
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{color:Colors.themeTextLightGray}}>数据加载中...</Text>
                </View>
            )
        }
        if(!this.state.electricityData.list || this.state.electricityData.list.length<=0){
            return(
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{color:Colors.themeTextLightGray}}>暂无数据</Text>
                </View>
            )
        }
        let eleData = this.state.electricityData.list
        let eleDataX = []
        let eleDataY = []
        for (const data of eleData) {
            let timeX = this.getXDate(data.times)
            eleDataX.push(timeX)
            eleDataY.push(data.quantity)
        }
        const chartConfig = {
            backgroundGradientFrom: Colors.white,
            backgroundGradientFromOpacity: 1,
            backgroundGradientTo: Colors.white,
            backgroundGradientToOpacity: 1,
            color: (opacity = 1) => `rgba(29, 148, 254, ${opacity})`,
            labelColor: (opacity = 1) => Colors.themeTextLightGray,
            strokeWidth: 1,
            barPercentage:0.5,
            propsForDots: {
                r: "5",
                strokeWidth: "2", 
            }
        }
        const data = {
            labels: eleDataX,
            datasets: [{
                data: eleDataY,
                color: () => '#1D94FE',
                strokeWidth: 2
            }]
        }
        return (
            <LineChart
                data={data}
                width={screenW}
                height={220}
                chartConfig={chartConfig}
                fromZero={true}
                onDataPointClick={({index,value,x,y})=>{
                    this.setState({
                        x:x,
                        y:y,
                        electValue: value,
                        showPopCell: true
                    })
                }}
            />
        )
    }

    // 电量 图表
    getElerticChartView() {
        let popCell = null
        if(this.state.showPopCell){
            let width = 80
            let height = 30
            let x = this.state.x - width/2
            let y = this.state.y - height - 8

            // 边界处理
            if(x + width > (screenW-10)){
                x = screenW - width - 10
            }else if(x < 10){
                x = 10
            }
                        
            popCell = (
                <View style={{position:'absolute', backgroundColor:'#1D94FE',left:x,top:y,width:width,height:height,justifyContent:"center",alignItems:'center',borderRadius: 5}}>
                    <Text style={{color:Colors.white,fontSize:13}}>{this.state.electValue}kWh</Text>
                </View>
            )
        }
        return (
            <View style={{ marginTop: 30, width:'100%' }}>
                <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 16,color: Colors.themeTextBlack }}>用电量统计图</Text>
                <View style={{ marginTop: 20, width: '100%', height: 220 }}>
                    {this._chart()}
                    <Text style={{position:'absolute',right:10,top:-10,fontSize:12,color:Colors.themeTextLightGray}}>单位: kWh</Text>
                    {popCell}
                </View>
            </View>
        )
    }


    /**
     * 电量页面--记录
     * */
    getRecordList() {
        let day = 0
        let createTime = null

        if (this.state.electricityData && this.state.electricityData.day) {
            day = this.state.electricityData.day
        }
        if (this.state.electricityData && this.state.electricityData.createTime) {
            createTime = this.state.electricityData.createTime
        }

        return (
            <View style={{ paddingBottom: 20, marginTop: 15 }}>
                <View style={{ width: screenW - 32, marginLeft: 16, marginRight: 16, height: 70, backgroundColor: '#FAFAFA', borderRadius: 2 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 16, paddingLeft: 10, paddingRight: 10 }}>
                        <Text style={{ color: '#999999', fontSize: 14 }}>已使用</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ color: '#1E1E1E', fontSize: 16, fontWeight: 'bold' }}>{day}</Text>
                            <Text style={{ fontSize: 12, color: '#1E1E1E', marginTop: 5 }}>天</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 8, paddingBottom: 16, paddingLeft: 10, paddingRight: 10 }}>
                        <Text style={{ color: '#999999', fontSize: 14 }}>设备添加日期</Text>
                        <Text style={{ color: '#999999', fontSize: 14 }}>{createTime}</Text>
                    </View>
                </View>
            </View>
        )
    }

    // 上拉 页面
    getNextPanelPage() {
        return (
            <View style={styles.second}>
                {this.getHeaderSegment()}
                {this.getEletricDetaiView()}
                {this.getElerticChartView()}
                {this.getRecordList()}
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.getHeaderView()}
                {/*第一部分*/}
                <Animated.View
                    style={[styles.container1, {
                        marginTop: this._aniBack.interpolate({
                            inputRange: [0, 1],
                            outputRange: [0, -screenH],
                        })
                    }]}
                    {...this._panResponder.panHandlers}>
                    <Animated.View
                        ref={(ref) => this._container1 = ref}
                        style={{
                            width: screenW, height: screenH,
                            marginTop: this._aniBack1.interpolate({
                                inputRange: [0, 1],
                                outputRange: [0, -100],
                            })
                        }}>
                        <ScrollView
                            ref={(ref) => this._scroll1 = ref}
                            bounces={false}
                            scrollEventThrottle={10}
                            onScroll={this._onScroll.bind(this)}
                            overScrollMode={'never'}>
                            {this.getMainPanelPage()}
                        </ScrollView >
                    </Animated.View>
                    <View style={{ width: screenW, height: 100, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }}>
                        <Text>上拉查看用电量</Text>
                    </View>
                </Animated.View>

                {/*第二部分*/}
                <View
                    style={styles.container2}
                    {...this._panResponder2.panHandlers}>
                    <Animated.View
                        ref={(ref) => this._container2 = ref}
                        style={{
                            width: screenW, height: 100, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center',
                            marginTop: this._aniBack2.interpolate({
                                inputRange: [0, 1],
                                outputRange: [-100, 0],
                            })
                        }}>
                        <Text>下拉回到顶部</Text>
                    </Animated.View>
                    <View
                        style={{ width: screenW, height: screenH, }}>
                        <ScrollView
                            style={{ height: screenH, marginTop: StatusBarHeight + NavigationBarHeight }}
                            ref={(ref) => this._scroll2 = ref}
                            bounces={true}
                            scrollEventThrottle={10}
                            onScroll={this._onScroll2.bind(this)}
                            overScrollMode={'never'}>
                            {this.getNextPanelPage()}
                        </ScrollView>
                    </View>
                </View>
            </View>

        )
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        flex: 1,
        position: 'relative'
    },
    container1: {
        width: screenW,
        height: screenH
    },
    second: {
        height: screenH,
        width: screenW,
        backgroundColor: Colors.white
    },
    container2: {
        width: screenW,
        height: screenH,
        backgroundColor: Colors.white,
        overflow: 'hidden'
    },
    header: {
        position: 'absolute',
        zIndex: 999,
        top: 0,
        left: 0,
        width: '100%',
        height: StatusBarHeight + NavigationBarHeight
    },
    headerContain: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20,
        paddingHorizontal: 16
    },
    status: {
        height: StatusBarHeight,
        width: '100%',
    },
    nav: {
        height: NavigationBarHeight,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 15,
        paddingLeft: 10,
        marginRight: 5
    },
    navImg: {
        width: 22,
        height: 22
    },
    backTouch: {
        height: '100%',
        marginLeft: 16,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    segmentWrapper: {
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    segmentTouch: {
        height: 30,
        width: '100%',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        //阴影四连
        shadowOffset: { width: 0, height: 1 },
        shadowColor: '#070F26',
        shadowOpacity: 0.15,
        shadowRadius: 15,
        elevation: 5
    },
    cardWrapper: {
        width: '48%',
        height: 120,
        backgroundColor: Colors.white,
        borderRadius: 5,
        paddingVertical: 10,
        paddingHorizontal: 12,
        //阴影四连
        shadowOffset: { width: 0, height: 1 },
        shadowColor: '#070F26',
        shadowOpacity: 0.15,
        shadowRadius: 4,
        elevation: 5
    },
    sameIcon: {
        width: 20,
        height: 2.5,
        resizeMode: 'contain',
        position: 'absolute',
        right: 10,
        top: 15
    },
    upIcon: {
        width: 13,
        height: 18,
        resizeMode: 'contain',
        position: 'absolute',
        right: 10,
        top: 15
    },
    electTips: {
        color: Colors.themeBGInactive,
        fontSize: 15,
        marginTop: 35
    },
    price: {
        fontSize: 30,
        fontWeight: 'bold',
        color: Colors.themeTextBlack,
        marginLeft: 5
    },
    unit: {
        fontSize: 15,
        color: Colors.themeBGInactive,
        marginLeft: 5,
        marginTop: 10
    },
    electUseTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.themeTextBlack
    }
});

export default SwiperPanel



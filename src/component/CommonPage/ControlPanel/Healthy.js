/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	ImageBackground,
	DeviceEventEmitter
} from 'react-native';
import { Colors, NetUrls,NetParams,NotificationKeys } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import Panel from './Panel';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const bottomControllerH = 190

class Healthy extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData')
		this.state = {
			...this.state,
            heartWarnning: 0,
			breathe:0,
			heartRate:0,
			status:0,
			bodyMoving:0,
			heartRateMin: 0,
			heartRateMax: 0
		}
	}

	componentDidMount() {
		super.componentDidMount()
		this.requestDeviceStatus()
	}

	componentWillUnmount() {
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        this.setState({
            status: data.status == 1,
            breathe: parseInt(data.breathe) || 0,
            heartRate: parseInt(data.heartRate) || 0,
            bodyMoving: parseInt(data.bodyMoving) || 0,
            heartRateMin: parseInt(data.heartRateMin) || 0,
            heartRateMax: parseInt(data.heartRateMax) || 0,
            heartWarnning: parseInt(data.heartRate) && (parseInt(data.heartRate) > 100)
        })
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				let resJson = data.result && data.result.deviceStatus
				try {
					let res = JSON.parse(resJson)
					this.setState({
                        isCollect: data.result && data.result.isCollect,
						breathe: res.breathe || 0,
						heartRate: res.heartRate || 0,
						status: res.status,
						bodyMoving: res.bodyMoving || 0,
						heartRateMin: (data.result && data.result.heartRateMin) || 0,
						heartRateMax: (data.result && data.result.heartRateMax) || 0,
						heartWarnning: res.heartRate && (res.heartRate > 100)
					})
				} catch (error) {
					ToastManager.show('数据异常')
				}
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

	// 健康 控制
    async controlDevice(params,callBack){
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
                url: NetUrls.controlCommon,
                timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

	getCenterView(){
		let bg = this.state.heartWarnning ? require('../../../images/panel/healthy_bg_icon_up.png') : require('../../../images/panel/healthy_bg_icon.png')
		let tips = this.state.heartWarnning ? '心率过高' : '心率'
		let centerTextColor = this.state.heartWarnning ? '#FC534C':Colors.themeBG

		return(
            <View style={styles.centerPannel}>
            	<ImageBackground source={bg} style={{width:screenW/2,height:screenW/2,justifyContent:'center',alignItems:'center'}}>
					<Text style={{color:Colors.themeTextBlack,fontSize:16,fontWeight:'bold'}}>{tips}</Text>
                    <Text style={{marginTop:5,color:centerTextColor,fontSize:35,fontWeight:'bold'}}>{this.state.heartRate}</Text>
                    <Text style={{marginTop:5,color:Colors.themeBGInactive,fontSize:16,fontWeight:'bold'}}>BPM</Text>
				</ImageBackground>
            </View>
		)
	}

	getCenterTextView(){
		return(
            <View style={styles.bottomTextWrapper}>
                <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                    <Text style={{color:Colors.white,fontSize:16}}>最高心率</Text>
                    <View style={{flexDirection:'row',marginTop:10}}>
						<Text style={{color:Colors.white,fontSize:28,fontWeight:'bold'}}>{this.state.heartRateMax}</Text>
                        <Text style={{color:Colors.white,fontSize:16,marginTop:11,marginLeft:10}}>BPM</Text>
                    </View>
                </View>
                <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                    <Text style={{color:Colors.white,fontSize:16}}>最低心率</Text>
                    <View style={{flexDirection:'row',marginTop:10}}>
                        <Text style={{color:Colors.white,fontSize:28,fontWeight:'bold'}}>{this.state.heartRateMin}</Text>
                        <Text style={{color:Colors.white,fontSize:16,marginTop:11,marginLeft:10}}>BPM</Text>
                    </View>
                </View>
            </View>
		)
	}

	getBottomView(){
        let switchIcon = this.state.status ? require('../../../images/panel/redSwitch_on.png')
        : require('../../../images/panel/redSwitch_off.png')
		
		return(
			<View style={styles.bottomWrapper}>
				
					<View style={styles.bottomTabWrapper}>
                        <Image style={{width:23,height:26,resizeMode:'contain'}} source={require('../../../images/panel/breath.png')}/>
                        <Text style={styles.bottomTapTitle}>{this.state.breathe}</Text>
                        <Text style={styles.bottomTabValue}>呼吸次（分）</Text>
                    </View>
                    <View style={styles.bottomTabWrapper}>
                        <Image style={{width:15,height:28,resizeMode:'contain'}} source={require('../../../images/panel/healthy_fs_icon.png')} />
                        <Text style={styles.bottomTapTitle}>{this.state.bodyMoving}</Text>
                        <Text style={styles.bottomTabValue}>体动</Text>
                    </View>
                    <TouchableOpacity style={styles.bottomTabWrapper} onPress={()=>{
						let target = this.state.status ? 0:1
                        this.controlDevice({status: target},()=>{
                            this.setState({
                                status: target
                            })
                        })
					}}>
						<Image style={{width:45,height:45,resizeMode:'contain'}} source={switchIcon}/>
                    </TouchableOpacity>
				
			</View>
		)
	}

	getMainPanelPage(){
		let bg = this.state.heartWarnning ? '#EE6658': Colors.themeBG
		return(
			<View style={[styles.main,{backgroundColor: bg}]}>
				{this.getCenterView()}
				{this.getCenterTextView()}
				{this.getBottomView()}
			</View>
		)
	}

	// render() {
	// 	return (
	// 		<View style={styles.container}>
	// 			{this.getHeaderView()}
	// 			<ScrollView style={styles.container}>
	// 				{this.getMainPanelPage()}
	// 			</ScrollView >
	// 		</View>

	// 	)
	// }
}

const styles = StyleSheet.create({
	main: {
		flex: 1,
		paddingTop: StatusBarHeight + NavigationBarHeight,
	},
	centerPannel:{
		justifyContent:'center',
		alignItems:'center',
		flex: 1
	},
	bottomTextWrapper:{
		flexDirection:'row',
		alignItems:'center',
		justifyContent:'center',
		marginBottom: 30,
	},
	bottomWrapper:{
		height: bottomControllerH + BottomSafeMargin,
		width:'100%',
		backgroundColor:Colors.white,
		borderTopLeftRadius:30,
		borderTopRightRadius:30,
        paddingBottom: BottomSafeMargin,
        justifyContent:'center',
		alignItems:'center',
		flexDirection: 'row',
	},
	bottomTabWrapper:{
		flex:1,
		justifyContent:'center',
		alignItems:'center',
	},
	bottomTapTitle:{
		marginTop:10,
		color:Colors.themeTextBlack,
		fontSize:15,
	},
	bottomTabValue:{
		marginTop:10,
		color:Colors.themeTextLightGray,
		fontSize:12
	}
});

export default Healthy



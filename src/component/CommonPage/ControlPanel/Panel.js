/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Alert,
    Dimensions,
    PanResponder,
    Animated,
    DeviceEventEmitter
} from 'react-native';
import { Colors, NetUrls, NetParams, NotificationKeys } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight, BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import HeaderBackground from '../../CommonPage/Navigation/HeaderBackground';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const TCPDeviceStatusNoti = 'kTCPDeviceStatusNoti'

class Panel extends Component {

    static navigationOptions = {
        header: null,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    }

    constructor(props) {
        super(props);
        const { getParam } = props.navigation;
        this.deviceData = getParam('deviceData') || {}
        this.shareInfo = this.deviceData.shareInfo

        // about TCP
        this.isTCP = getParam('isTCP')
        this.tcpOrder = getParam('tcpOrder')
        this.tcpDeviceId = getParam('tcpDeviceId')
        this.tcpDeviceType = getParam('tcpDeviceType')
        
        this.state = {
            isWhiteBg: false,
            title: getParam('title'),
            isCollect: this.deviceData.isCollect,
        }
    }

    componentDidMount() {
        // websockt notification center
        if(this.isTCP){
            this.tcpStatusNoti = DeviceEventEmitter.addListener(TCPDeviceStatusNoti, (data)=>{
                if(!data) return
                try {
                    let tcpUniqueId = this.tcpDeviceType + '-' + this.tcpDeviceId + '-' + this.tcpOrder
                    let tcpDevice = data.split('*')[0]
                    let statusStr = data.split('*')[1]
                    if(tcpDevice != tcpUniqueId){
                        return
                    }
                    this.updateDeviceStatusByTCP(statusStr)
                } catch (error) {
                    
                }
            })
        }else{
            this.websocketNoti = DeviceEventEmitter.addListener(NotificationKeys.kWebsocketDeviceNotification,(data)=>{
                if(!data) return
                this.updateDeviceStatus(data)
            })
        }
    }

    componentWillUnmount() {
        if(this.isTCP){
            this.tcpStatusNoti.remove()
        }else{
            this.websocketNoti.remove()
        }
    }

    // get status by tcp
    requestStatusByTCP(){
        let req = 'request-2-' + this.tcpDeviceType + '-' + this.tcpDeviceId + '-' + this.tcpOrder + '-a-a'
        console.log('client-request-status- ' + req);
        try {
            window.client.write(req)
        } catch (error) {
            
        }
    }

    // update status about websockt (overwrite by subclass)
    updateDeviceStatus(data){

    }

    // update status about TCP (overwrite by subclass)
    updateDeviceStatusByTCP(data){

    }

    async requestCollect(){
        let target = !this.state.isCollect
		try {
			let data = await postJson({
				url: NetUrls.collect,
				params: {
                    ids: this.deviceData.deviceId,
                    type: 1,
                    isAdd: target
                }
			});
			if (data.code == 0) {
				this.setState({
                    isCollect: target
                },()=>{
                    let warnText = target ? '收藏成功' : '取消收藏'
                    ToastManager.show(warnText)
                })
                DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }
    
    goToShare(){
        const { navigate } = this.props.navigation

        navigate('Setting_Share',{
            title: '设备分享',
            id: this.deviceData.deviceId,
            deviceData: this.deviceData,
            shareData: this.shareInfo,
            shareType: 1,   // 1设备 2场景
        })
    }

    goToSetting(){
        const { navigate,push } = this.props.navigation;

        push('DeviceSetting', { 
            deviceData: this.deviceData,
            nameCallBack: (name)=>{
                this.setState({ title: name })
            },
            collectCallBack: (isCollect)=>{
                this.setState({isCollect: isCollect})
            }
        })
    }

    getHeaderView() {
        const { pop } = this.props.navigation;

        let backIcon = this.state.isWhiteBg ? require('../../../images/back.png') : require('../../../images/back_white.png')
        let settingIcon = this.state.isWhiteBg ? require('../../../images/setting.png') : require('../../../images/setting_white.png')
        let sahreIcon = require('../../../images/share.png')
        let titleColor = this.state.isWhiteBg ? Colors.themeTextBlack : Colors.white
        
        return (
            <View style={styles.header}>
                <View style={styles.status}/>
                <View style={styles.nav}>
                    <TouchableOpacity style={styles.backTouch} onPress={() => { pop() }}>
                        <Image style={{ width: 10, height: 18 }} source={backIcon} />
                        <Text numberOfLines={1} style={[styles.navTitle,{color: titleColor}]}>{this.state.title}</Text>
                    </TouchableOpacity>
                    <View style={{ flex: 1 }} />
                    {!this.isTCP && this.shareInfo ? <TouchableOpacity style={styles.navTouch} onPress={() => { this.goToShare() }}>
                        <Image style={styles.sahreImg} source={sahreIcon} />
                    </TouchableOpacity> : null}
                    {!this.isTCP ? <TouchableOpacity style={styles.navTouch} onPress={() => { this.goToSetting() }}>
                        <Image style={styles.navImg} source={settingIcon} />
                    </TouchableOpacity> : null}
                </View>
            </View>
        )
    }

    getMainPanelPage(){
        return null
    }

    render() {
        return (
            <View style={styles.container}>
                {this.getHeaderView()}
                {this.getMainPanelPage()}
            </View>
        )
    }
	
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        flex: 1,
        position: 'relative'
    },
    header: {
        position: 'absolute',
        zIndex: 999,
        top: 0,
        left: 0,
        width: '100%',
        height: StatusBarHeight + NavigationBarHeight
    },
    headerContain: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20,
        paddingHorizontal: 16
    },
    status: {
        height: StatusBarHeight,
        width: '100%',
    },
    nav: {
        height: NavigationBarHeight,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 15,
        paddingLeft: 10,
        marginRight: 5
    },
    navImg: {
        width: 22,
        height: 22,
        resizeMode: 'contain'
    },
    sahreImg:{
        width: 26,
        height: 26,
        resizeMode: 'contain'
    },
    navTitle:{ 
        marginLeft: 15, 
        fontSize: 24, 
        fontWeight: 'bold'
    },
    backTouch: {
        height: '100%',
        marginLeft: 16,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        maxWidth:'50%'
    },
});

export default Panel



/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 窗帘
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter,
	PanResponder,
	Modal
} from 'react-native';
import { Colors, NetUrls,NotificationKeys,NetParams } from '../../../common/Constants';
import Slider from "react-native-slider";
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;


class ScrollCurtains extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		
		this.state = {
			...this.state,
			switch: 0,
            isRuning: false,
            value: 0,
            lastValue: 0,
            
            angleLastValue: 0,
            angleValue: 0,
            direction: 0  //0-正  1-反

		}
	}

	componentDidMount() {
        super.componentDidMount()
        if(this.isTCP){
            this.requestStatusByTCP()
        }else{
            this.requestDeviceStatus()
        }
	}

	componentWillUnmount() {
        this.timer && clearTimeout(this.timer);
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        let value = Math.floor(data.unwind) || 0
		this.setState({
            value: value,
            lastValue: value
        })
    }

    // tcp 数据状态解析
    updateDeviceStatusByTCP(data){
        if(!data) return
        let value = data.split('_')[1] || 0
        
        this.setState({
            value: parseInt(value),
            lastValue: parseInt(value)
        })
    }
	
	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				let res = data.result && data.result.deviceStatus
				try {
					res = JSON.parse(res)
					this.setState({
                        isCollect: data.result && data.result.isCollect,
                        direction: data.result && data.result.unwindPositive,
						value: Math.floor(res.unwind) || this.state.value,
						lastValue: Math.floor(res.unwind) || this.state.value
					})
				} catch (error) {
					ToastManager.show('数据异常')
				}
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}
    
    // 窗帘 控制
    async controlDevice(params,callBack){
        if(this.isTCP){
            this.controlByTCP(params,callBack)
            return
        }
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 设置正反
    async setDeviceProperties(params,callBack){
        params = params || {}
        params.deviceId = this.deviceData.deviceId
        params.type = 0 //窗帘
		try {
			let data = await postJson({
				url: NetUrls.setDeviceProperties,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 通过TCP 控制设备
    controlByTCP(params, callBack){
        let req = 'request-2-' + this.tcpDeviceType + '-' + this.tcpDeviceId + '-' + this.tcpOrder + '-' 
        if(params.hasOwnProperty('unwind')){
            req = req + '0-' + params.unwind
        }else if(params.hasOwnProperty('number')){
            req = req + '0-' + params.number
        }
        console.log('client-send- ' + req);
        try {
            window.client.write(req)
            callBack && callBack()
        } catch (error) {
            ToastManager.show('控制出错，请检查连接后重试。')
        }
    }

	_onChange =(value)=>{
		// if(this.state.isRuning){
		// 	ToastManager.show('卷帘运行中')
		// 	return
		// }
        this.setState({
            value: Math.floor(value),
            lastValue: Math.floor(value)
        })
	};
	
	_complete =(value)=>{
		// 滑动 结束
		// if(this.state.isRuning){
		// 	return
		// }

        value = Math.floor(value)
		this.state.lastValue = Math.floor(value)

		this.controlDevice({unwind: Math.floor(value) },()=>{
			
		})
    }
    
    valueClick(value){
        this.controlDevice({ unwind: value },()=>{
            this.setState({
                value: value,
                lastValue: value
            })
        })
    }

    getHeaderView() {
        const { navigate,pop } = this.props.navigation;

        return (
            <View style={styles.header}>
                <View style={styles.status}/>
                <View style={styles.nav}>
                    <TouchableOpacity style={styles.backTouch} onPress={() => { pop() }}>
                        <Image style={{ width: 10, height: 18 }} source={require('../../../images/back_white.png')} />
                        <Text numberOfLines={1} style={[styles.navTitle,{color: Colors.white}]}>{this.state.title}</Text>
                    </TouchableOpacity>
                    <View style={{ flex: 1 }} />
                    {this.shareInfo ? <TouchableOpacity style={styles.navTouch} onPress={() => { this.goToShare() }}>
                        <Image style={styles.sahreImg} source={require('../../../images/share.png')} />
                    </TouchableOpacity> : null}
                    <TouchableOpacity style={styles.navTouch} onPress={() => { 
                        navigate('ScrollCurtainsDebug',{
                            deviceId: this.deviceData.deviceId,
                            curtainType: 1   // 1卷帘  2开合帘
                        }) 
                    }}>
                        <Image style={[styles.navImg,{tintColor: Colors.white}]} source={require('../../../images/debug.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.navTouch} onPress={() => { this.goToSetting() }}>
                        <Image style={styles.navImg} source={require('../../../images/setting_white.png')} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    /**
	 * 中间内容展示
     * @returns {*}
     */
    getCenterView(){
		let unwind = (100 - this.state.value) + '%'
		
		let curtainHeight = screenH/3.0
		let curtainTopHeight = screenW*0.8*0.07
		let bottomHeight = curtainHeight - curtainTopHeight
		
		return(
			<View style={[styles.curtainWrapper,{height: curtainHeight}]}>
				<View style={{width: '80%',height:curtainHeight}}>
					<Image 
                        style={{width:'100%',height:curtainTopHeight,resizeMode:'stretch'}} 
                        source={require('../../../images/deviceIcon/scrollCurtain_head.png')}
                    />
					<ScrollWindow 
						unwind={unwind} 
						bottomHeight={bottomHeight} 
						onPanResponderMove={(evt, gs)=>{
							// if(this.state.isRuning){
							// 	ToastManager.show('卷帘运行中')
							// 	return
							// }
                            if(this.deviceData.attributeTypeName == 'shutter-wifi'){
                                return null
                            }
							let step = bottomHeight/100.0
							let dy = gs.dy/step
							let moveY = parseInt(dy)
							let value = this.state.lastValue + moveY
							if(value >= 100) value = 100
							if(value <= 0) value = 0
							this.setState({
								value: value
							})
						}}
						onComplete={()=>{
							this._complete(this.state.value)
						}}
					/>
				</View>
			</View>
		)
    }

    renderTopView(){
        const attributeType = this.deviceData.attributeTypeName
        const topWrapper = attributeType == 'shutter-wifi' ? styles.wifi_topWrapper : styles.topWrapper
        return(
            <View style={topWrapper}>
                {this.getCenterView()}
            </View>
        )
    }
    
    // 打开 关闭 暂停
	showControllerBtns(){
        const attributeType = this.deviceData.attributeTypeName
        const btnsWrapper = attributeType == 'shutter-wifi' ? styles.wifi_btnsWrapper : styles.btnsWrapper

        return(
            <View style={btnsWrapper}>
                <TouchableOpacity style={styles.btn} onPress={()=>{
                    if(this.isTCP){
                        this.controlByTCP({number: 0})
                    }else{
                        this.controlDevice({number: 1},()=>{})
                    }
                }}>
                    <Image style={styles.centerBtnImg} source={require('../../../images/panel/curtain/curtain_on.png')}/>
                    <Text style={styles.btnText}>打开</Text>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.btn,{marginHorizontal: 50}]} onPress={()=>{
                    if(this.isTCP){
                        this.controlByTCP({number: 110})
                    }else{
                        this.controlDevice({number: 3},()=>{})
                    }
                }}>
                    <Image style={styles.centerBtnImg} source={require('../../../images/panel/curtain/curtain_pause.png')}/>
                    <Text style={styles.btnText}>暂停</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.btn} onPress={()=>{
                    if(this.isTCP){
                        this.controlByTCP({number: 100})
                    }else{
                        this.controlDevice({number: 2},()=>{})
                    }
                }}>
                    <Image style={styles.centerBtnImg} source={require('../../../images/panel/curtain/curtain_off.png')}/>
                    <Text style={styles.btnText}>关闭</Text>
                </TouchableOpacity>
            </View>
        )
    }

	// 展开程度
    showProcess(){
        if(this.deviceData.attributeTypeName == 'shutter-wifi'){
            return null
        }
        let btnsArr = [0, 25, 50, 75, 100]
        let btnslabelArr = ['全开',25,50,75,'全关']
        
        let btns = btnsArr.map((value, index)=>{
            let valueText = btnslabelArr[index]
            // if(this.state.direction == 1){
            //     valueText = 100 - value
            // }
            let smallFont = {}
            if(index == 0 || index == 4){
                smallFont = {fontSize: 12}
            }
            return(
                <TouchableOpacity key={'btns_'+index} style={styles.quickBtn} activeOpacity={0.7} onPress={()=>{
                    this.valueClick(value)
                }}>
                    <View style={styles.shortLine}/>
                    <Text style={[styles.quickText,smallFont]}>{valueText}</Text>
                </TouchableOpacity>
            )
        })

        // let directionIcon = this.state.direction ? require('../../../images/panel/dir_down.png') :
        // require('../../../images/panel/dir_up.png')
        // let value = this.state.direction == 1 ? (100 - this.state.value) : this.state.value

        let valText = this.state.value + '%'
        if(this.state.value == 0){
            valText = '全开'
        }else if(this.state.value == 100){
            valText = '全关'
        }

		return(
			<View style={styles.processWrapper}>
				<View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                    <Text style={styles.valueText}>{valText}</Text>
                    {/* <TouchableOpacity style={{paddingHorizontal: 20}} onPress={()=>{
                        let target = this.state.direction ? 0:1
                        this.setDeviceProperties({value: target}, ()=>{
                            this.setState({ direction: target })
                        })
                    }}>
                        <Image style={{width: 20,height: 20,resizeMode:'contain'}} source={directionIcon}/>
                    </TouchableOpacity> */}
				</View>
				<Slider
					// disabled={this.state.isRuning}
					style={{width: '80%',marginTop:20}}
					minimumValue={0}
                    maximumValue={100}
          			value={this.state.value}
					onValueChange={this._onChange}
					onSlidingComplete={this._complete}
					minimumTrackTintColor='#fff'
					maximumTrackTintColor='#2461D4'
					thumbTintColor='#fff'
					trackStyle={{height:8,borderRadius: 4}}
					thumbStyle={{width:26,height:26,borderRadius:13}}
					onSlidingStart={()=>{
						// if(this.state.isRuning){
						// 	ToastManager.show('卷帘运行中')
						// 	return
						// }
					}}
        		/>
                <View style={styles.quickBtnWrapper}>
                    {btns}
                </View>
			</View>
		)
	}

	getMainPanelPage(){
		return(
			<View style={styles.main}>
                {this.renderTopView()}
                {this.showControllerBtns()}
                {this.showProcess()}
			</View>
		)
	}

	// render() {
	// 	return (
	// 		<View style={styles.container}>
	// 			{this.getHeaderView()}
	// 			<ScrollView style={styles.container}>
	// 				{this.getMainPanelPage()}
	// 			</ScrollView >
	// 		</View>

	// 	)
	// }
}

class ScrollWindow extends Component {

	constructor(props) {
		super(props);
	}

	componentWillMount() {
		this._panResponder = PanResponder.create({
      		onMoveShouldSetPanResponder: (evt, gestureState) => true,
      		onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
			onPanResponderGrant: () => {
				// 移动 开始
			},
			onPanResponderMove: (evt, gs) => {
				this.props.onPanResponderMove(evt, gs)
			},
			onPanResponderRelease: (evt, gs) => {
				// 移动结束
				this.props.onComplete()
			}
		});
	}

	render(){
		let bottomHeight = this.props.bottomHeight
		let unwind = this.props.unwind
		let itemHeigth = (bottomHeight - 4 * 2)/5

		return(
			<View style={{height:bottomHeight,backgroundColor:Colors.white,justifyContent:'space-between'}} {...this._panResponder.panHandlers}>
				<View style={{backgroundColor: '#59A1FB',height:itemHeigth}}/>
				<View style={{backgroundColor: '#59A1FB',height:itemHeigth}}/>
				<View style={{backgroundColor: '#59A1FB',height:itemHeigth}}/>
				<View style={{backgroundColor: '#59A1FB',height:itemHeigth}}/>
				<View style={{backgroundColor: '#59A1FB',height:itemHeigth}}/>
				<View style={[styles.windowWrapper,{height:unwind}]}/>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
		flex: 1,
        backgroundColor: '#3E7EF7',
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
    topWrapper:{
        flex: 1,
        justifyContent:'center',
        alignItems:'center',
    },
    curtainWrapper:{
        justifyContent:'center',
        width:'100%',
        flexDirection:'row'
    },
    btnsWrapper:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        paddingVertical: 20
    },
    processWrapper:{
        paddingTop: 30,
        paddingBottom: BottomSafeMargin + 0.1*screenH,
        width:'100%',
        justifyContent:'center',
        alignItems:'center',
    },
    quickBtnWrapper:{
        width: '80%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    quickBtn:{
        justifyContent:'center',
        alignItems:'center',
        marginTop: 15
    },
    quickText:{
        fontSize: 15,
        color: Colors.white
    },
    shortLine:{
        width: 2,
        height: 8,
        backgroundColor:Colors.white
    },
    valueText:{
        color:'#fff',
        fontSize:25,
        fontWeight:'bold',
        textAlign: 'center',
        marginTop: 5
    },
    windowWrapper:{
        position:'absolute',
        width:'100%',
        bottom:0,
        left:0,
        backgroundColor:Colors.white
    },
    wifi_topWrapper:{
        justifyContent:'center',
        alignItems:'center',
        marginTop: (screenH- StatusBarHeight - NavigationBarHeight)  * 0.15,
    },
    wifi_btnsWrapper:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        paddingVertical: 20,
        marginTop: 20
    },
    centerBtnImg:{
        height:50,
        width:50,
        resizeMode:'contain'
    },
    header: {
        position: 'absolute',
        zIndex: 999,
        top: 0,
        left: 0,
        width: '100%',
        height: StatusBarHeight + NavigationBarHeight
    },
    status: {
        height: StatusBarHeight,
        width: '100%',
    },
    nav: {
        height: NavigationBarHeight,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 10,
        paddingLeft: 10,
        marginRight: 5
    },
    navImg: {
        width: 22,
        height: 22,
        resizeMode: 'contain'
    },
    sahreImg:{
        width: 26,
        height: 26,
        resizeMode: 'contain'
    },
    navTitle:{ 
        marginLeft: 15, 
        fontSize: 24, 
        fontWeight: 'bold'
    },
    backTouch: {
        height: '100%',
        marginLeft: 16,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        maxWidth:'50%'
    },
    btn:{
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnText:{
        marginTop: 5,
        fontSize: 15,
        color: Colors.white
    }
});

export default ScrollCurtains



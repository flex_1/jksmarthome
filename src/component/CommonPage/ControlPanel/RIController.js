/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import HeaderLeftView from '../../../common/CustomComponent/HeaderLeftView';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import Panel from './Panel';
import { connect } from 'react-redux';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class RIController extends Panel {

	constructor(props) {
		super(props);
		
		this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,
		}
	}

	componentDidMount() {
		
    }
    
    componentWillUnmount() {
        
    }

    // 遥控器
    async controlDevice(params,callBack){
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}


	getCircularBg(){
		if(!this.state.currentPress){
			return null
        }
        let selectIcon = null
        // if(this.state.currentPress == 1){
        //     selectIcon = <Image source={require('../../../images/panel/TV/up_select.png')} style={styles.btnSelectIcon}/>
        // }else if(this.state.currentPress == 2){
        //     selectIcon = <Image source={require('../../../images/panel/TV/right_select.png')} style={styles.btnSelectIcon}/>
        // }else if(this.state.currentPress == 3){
        //     selectIcon = <Image source={require('../../../images/panel/TV/down_select.png')} style={styles.btnSelectIcon}/>
        // }else if(this.state.currentPress == 4){
        //     selectIcon = <Image source={require('../../../images/panel/TV/down_select.png')} style={styles.btnSelectIcon}/>
        // }
        
		let percent = '71.71067812%'
		let rotateZ = 90 * (this.state.currentPress-1) + 'deg'
		return(
			<View style={[styles.selectWrapper,{width:percent,transform:[{rotateZ:rotateZ}]}]}>
				<Image style={styles.selectImg} source={require('../../../images/panel/tv_volate_bg_icon.png')}/>
                {selectIcon}
			</View>
		)
	}

	getCircularButton(index,icon,btnStyle){
		return(
			<TouchableOpacity style={{...styles.cirlBtn,...btnStyle}} 
				onPress={()=>{
					if(index == 1){
						//上
						this.controlDevice({number:20})
					}
					else if(index == 2){
						//右
						this.controlDevice({number:21})
					}
					else if(index == 3){
						//下
						this.controlDevice({number:22})
					}
					else if(index == 4){
						//左
						this.controlDevice({number:23})
					}
				}}
				onPressIn={()=>{
					this.setState({currentPress:index})
				}}
				onPressOut={()=>{
					this.setState({currentPress:null})
				}}
			>
				<Image source={icon} style={styles.btnImg}/>
			</TouchableOpacity>
		)
	}

	getCenterView(){
		return(
            <View style={styles.centerWrapper}>
				{this.getCircularBg()}
				<TouchableOpacity activeOpacity={0.7} style={styles.okBtnWrapper} onPress={()=>{
                    this.controlDevice({number:19})
                }}>
                    <Text style={{color:Colors.tabActiveColor,fontSize:25,fontWeight:'bold'}}>OK</Text>
					
				</TouchableOpacity>
				{this.getCircularButton(1,require('../../../images/panel/TV/up.png'),{left:'25%',height:'25%',top:0,width:'50%'})}
				{this.getCircularButton(2,require('../../../images/panel/TV/right.png'),{right:0,height:'50%',top:'25%',width:'25%'})}
				{this.getCircularButton(3,require('../../../images/panel/TV/down.png'),{left:'25%',height:'25%',bottom:0,width:'50%'})}
				{this.getCircularButton(4,require('../../../images/panel/TV/left.png'),{left:0,height:'50%',top:'25%',width:'25%'})}
            </View>
		)
	}

	getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

		return(
			<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
				{this.getCenterView()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
		flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight,
        justifyContent: 'center',
        alignItems: 'center'
	},
	cirlBtn:{
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		zIndex: 1000,
	},
	centerWrapper:{
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: Colors.tabActiveColor,
		borderRadius: screenW*0.3,
		width: screenW*0.6,
		height: screenW*0.6,
	},
	okBtnWrapper:{
		position: 'absolute',
		alignItems: 'center',
		justifyContent: 'center',
		width: '50%',
		height: '50%',
		borderRadius: screenW*0.15,
		backgroundColor: Colors.themBGLightGray
    },
    selectWrapper:{
        position:'absolute',
        alignItems:'center',
        height:'100%',
        backgroundColor:'transparent',
    },
    selectImg:{
        width:'100%',
        height:'50%',
        resizeMode:'contain',
        marginTop:-2,
        tintColor: Colors.white
    },
    btnImg:{
        position: 'absolute',
        width:24,
        height:24,
        resizeMode:'contain',
        tintColor: Colors.themBGLightGray
    },
    btnSelectIcon:{
        position: 'absolute',
        top:'8%', 
        width:24,
        height:24,
        resizeMode:'contain'
    }

});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(RIController) 



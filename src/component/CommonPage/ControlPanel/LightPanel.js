/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter,
    Modal,
    Picker
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import Slider from "react-native-slider";
import Panel from './Panel';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class LightPanel extends Panel {

	constructor(props) {
		super(props);
        const { getParam } = props.navigation;
        
		this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

			lightVal: 0,
            pickerData:[
				['立即','1s','2s','3s','4s','5s','6s','7s','8s','9s','10s'],
				['10%','20%','30%','40%','50%','60%','70%','80%','90%','100%',]
            ],
            lightTime: 0,
			lightPercent: '100%',
            
            delayModal: false,
            currentColor: '#254FD7'
		}
	}

	componentDidMount() {
		super.componentDidMount()
		if(this.isTCP){
            this.requestStatusByTCP()
        }else{
            this.requestDeviceStatus()
        }
	}

	componentWillUnmount() {
        super.componentWillUnmount()
    }

    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        this.setState({
            lightVal: parseInt(data.light) || 0
        })
    }

    // tcp 数据状态解析
    updateDeviceStatusByTCP(data){
        if(!data) return
        let light = data.split('_')[1]

        this.setState({
            lightVal: parseInt(light) || 0
        })
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				let res = data.result && data.result.deviceStatus
				let lightTime = data.result && data.result.lightTime
				let lightPercent = data.result && data.result.lightTimePercent
				try {
					res = JSON.parse(res)
					let lightVal = Math.floor(res.light) || 0
					this.setState({
                        isCollect: data.result && data.result.isCollect,
						lightVal: lightVal,
						lightTime: lightTime || 0,
						lightPercent: lightPercent+'%' || '100%'
					})
				} catch (error) {
					ToastManager.show('数据异常')
				}
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}
    
    // 灯光 控制
    async controlDevice(params,callBack){
        if(this.isTCP){
            this.controlByTCP(params,callBack)
            return
        }
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 通过TCP 控制设备
    controlByTCP(params, callBack){
        let req = 'request-2-' + this.tcpDeviceType + '-' + this.tcpDeviceId + '-' + this.tcpOrder + '-' 
        if(params.hasOwnProperty('light')){
            req = req + '0-' + params.light
        }
        console.log('client-send- ' + req);
        try {
            window.client.write(req)
            callBack && callBack()
        } catch (error) {
            ToastManager.show('控制出错，请检查连接后重试。')
        }
    }

	// 修改灯光时间
	async updateLightTime(lightTime,lightPercent){
        SpinnerManager.show()
        let percent = lightPercent.replace('%','')
		try {
			let data = await postJson({
				url: NetUrls.saveDevice,
				params: {
					id: this.deviceData && this.deviceData.deviceId,
					lightTime: lightTime,
					lightTimePercent: percent
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                this.setState({
					lightTime: lightTime,
					lightPercent: lightPercent
				})
				ToastManager.show('设置成功')
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
	}

	_onChange =(value)=>{
		let target = Math.floor(value)
        this.setState({
            lightVal: target,
        })
	};
	
	_complete =()=>{
		this.controlDevice({light: Math.floor(this.state.lightVal) })
    }

    valueClick(value){
        this.controlDevice({ light: value },()=>{
            this.setState({
                lightVal: value
            })
        })
    }
    
    // 灯光延迟选择器
    renderDelayModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.delayModal}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={styles.modalTouchable} 
                    onPress={() => {
                        this.setState({
                            delayModal: false
                        }, () => {
                         window.DoubulePicker.hide()
                        })
                    }}
                >
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        let selectedValue = this.state.lightTime ? 
            [this.state.lightTime + 's', this.state.lightPercent] : ['立即',this.state.lightPercent]

        window.DoubulePicker.init({
            pickerData: this.state.pickerData,
            pickerConfirmBtnText: '确定',
            pickerCancelBtnText: '取消',
            pickerTitleText: '灯光渐变时间',
            pickerCancelBtnColor: [38, 80, 215, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: selectedValue,
            onPickerConfirm: (data,index) => {
                this.setState({
                    delayModal: false
				})
				this.updateLightTime(index[0],data[1])
            },
            onPickerCancel: data => {
                this.setState({
                    delayModal: false
                })
            }
        });
        window.DoubulePicker.show()
    }

    renderDelayButton(){
        if(this.isTCP){
            return null
        }
        const Colors = this.props.themeInfo.colors
        let delayText = this.state.lightTime ? this.state.lightTime+'s' : '立即'
        let percentText = this.state.lightPercent

        return(
            <TouchableOpacity activeOpacity={0.7} style={styles.delayTouch} onPress={()=>{
                this.setState({ delayModal: true })
            }}>
                <Text style={styles.delayText}>{delayText +'  '+ percentText}</Text>
                <Image style={styles.downIcon} source={require('../../../images/deviceIcon/down.png')}/>
            </TouchableOpacity>
        )
    }

    // 渲染灯光页面
    renderLight(){
        const Colors = this.props.themeInfo.colors
        let lightValue = this.state.lightVal
        
        return(
            <View style={styles.topWrapper}>
                <View style={styles.title}>
                    <Text style={[styles.lightText,{color: Colors.themeText}]}>亮度</Text>
                    <Text style={[styles.valueText,{color: Colors.themeText}]}>{lightValue}%</Text>
                </View>
                {this.renderDelayButton()}
                <View style={styles.lightContain}>
                    <View style={[styles.lightView_large,{backgroundColor: this.state.currentColor}]}/>
                    <View style={[styles.lightView_middle,{backgroundColor: this.state.currentColor}]}/>
                    <View style={[styles.lightView,{backgroundColor: this.state.currentColor}]}>
                        <Image style={styles.lightImg} source={require('../../../images/panel/color_light_white.png')}/>
                    </View>
                </View>
            </View>
        )
    }

    renderController(){
        const Colors = this.props.themeInfo.colors

        let btnsArr = [0, 25, 50, 75, 100]
        let btns = btnsArr.map((value, index)=>{
            return(
                <TouchableOpacity key={'l_btns_'+index} style={styles.quickBtn} activeOpacity={0.7} onPress={()=>{
                    this.valueClick( value )
                }}>
                    <View style={[styles.shortLine,{backgroundColor: Colors.themeTextLight}]}/>
                    <Text style={[styles.quickText,{color: Colors.themeTextLight}]}>{value}</Text>
                </TouchableOpacity>
            )
        })

        return (
            <View style={styles.bottomCtr}>
                <Slider
					style={{width:'90%'}}
					minimumValue={0}
					maximumValue={100}
					step={1}
          			value={this.state.lightVal}
					onValueChange={this._onChange}
					onSlidingComplete={this._complete}
					minimumTrackTintColor='#254FD7'
					maximumTrackTintColor={Colors.themeInactive}
					thumbTintColor='#254FD7'
					trackStyle={{height:8,borderRadius: 4}}
					thumbStyle={{width:26,height:26,borderRadius:13}}
        		/>
                <View style={styles.quickBtnWrapper}>
                    {btns}
                </View>
            </View>
        )
    }

	getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

		return(
            <View style={[styles.main,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderLight()}
                {this.renderController()}
                {this.renderDelayModal()}
            </View>
		)
	}
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
    },
	topWrapper:{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
        alignItems:'center'
    },
    title:{
        flexDirection: 'row',
        justifyContent:'center',
        alignItems: 'center',
    },
    lightText:{
        fontSize: 15,
        marginTop: 5
    },
    valueText:{
        fontSize: 30,
        fontWeight: 'bold',
        marginLeft: 10
    },
    delayTouch:{
        marginTop: 5,
        flexDirection: 'row',
        justifyContent:'center',
        alignItems: 'center',
        paddingVertical: 15,
        paddingHorizontal: 50
    },
    delayText:{
        fontSize: 13,
        color: Colors.themeTextLightGray
    },
    downIcon:{
        width: 9,
        height: 5,
        resizeMode: 'contain',
        marginLeft: 10
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: 'rgba(0,0,0,0.5)' 
    },
    lightContain:{
        marginTop: 20,
        width: '100%',
        height: screenW*0.6,
    },
    lightView_large:{
        position: 'absolute',
        alignSelf: 'center',
        opacity: 0.2,
        width: screenW*0.6,
        height: screenW*0.6,
        borderRadius: screenW*0.3,
    },
    lightView_middle:{
        position: 'absolute',
        alignSelf: 'center',
        opacity: 0.4,
        top: screenW*0.075,
        width: screenW*0.45,
        height: screenW*0.45,
        borderRadius: screenW*0.225,
        justifyContent: 'center',
        alignItems: 'center'
    },
    lightView:{
        position: 'absolute',
        alignSelf: 'center',
        top: screenW*0.15,
        width: screenW*0.3,
        height: screenW*0.3,
        borderRadius: screenW*0.15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    lightImg:{
        width: screenW*0.15,
        height: screenW*0.15,
        resizeMode: 'contain'
    },
    bottomCtr:{
        paddingTop: 30,
        paddingBottom: BottomSafeMargin + 0.1*screenH,
        width: '100%',
        justifyContent:'center',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    btns:{
        width:30,
        height:30,
        borderRadius:15,
        marginLeft:15
    },
    colorImg:{
        width:30,
        height:30,
        resizeMode:'contain'
    },
    quickBtnWrapper:{
        width: '90%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    quickBtn:{
        width: 28,
        justifyContent:'center',
        alignItems:'center',
        marginTop: 15
    },
    quickText:{
        fontSize: 15
    },
    shortLine:{
        width: 2,
        height: 8
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(LightPanel)



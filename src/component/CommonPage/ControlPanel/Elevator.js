/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter,
	Switch,
    StatusBar,
    Animated
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel';
import { connect } from 'react-redux';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

//websocket
let deviceWebSocket = null

class Elevator extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData')

        this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

        }
    }
    
	componentDidMount() {
        super.componentDidMount()
		
	}

	componentWillUnmount(){
        super.componentWillUnmount()
    }
    
    // 灯 开关 控制
    async controlDevice(params, callBack){
        params = params || {}
        params.deviceId = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				failCallback && failCallback()
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误，请稍后重试')
		}
	}

    renderTopSwitch(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={styles.topSwitch}>
                <View style={styles.btnWrapper}>
                    <TouchableOpacity 
                        style={[styles.switchBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]}
                        onPress={()=>{
                            this.controlDevice({number: 0})
                        }}
                    >
                        <Image style={[styles.switchImg,{tintColor: Colors.red}]} source={require('../../../images/panel/TV/tv_switch.png')}/>
                    </TouchableOpacity>
                    {/* <Text style={[styles.switchText,{color: Colors.themeTextLight}]}>关机</Text> */}
                </View>
                <View style={styles.btnWrapper}>
                    <TouchableOpacity 
                        style={[styles.switchBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]}
                        onPress={()=>{
                            this.controlDevice({number: 1})
                        }}
                    >
                        <Image style={[styles.switchImg,{tintColor: Colors.blue}]} source={require('../../../images/panel/TV/tv_switch.png')}/>
                    </TouchableOpacity>
                    {/* <Text style={[styles.switchText,{color: Colors.themeTextLight}]}>开机</Text> */}
                </View>
            </View>
        )
    }

    renderCenterView(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={styles.centerWrapper}>
                <TouchableOpacity 
                    style={[styles.centerBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        this.controlDevice({number: 2})
                    }}
                >
                    <Image style={[styles.centerImg,{tintColor: Colors.themeText}]} source={require('../../../images/panel/elevator_up.png')}/>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={[styles.centerBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        this.controlDevice({number: 3})
                    }}
                >
                    <Image style={[styles.centerImg,{tintColor: Colors.themeText}]} source={require('../../../images/panel/elevator_pause.png')}/>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={[styles.centerBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        this.controlDevice({number: 4})
                    }}
                >
                    <Image style={[styles.centerImg,{tintColor: Colors.themeText}]} source={require('../../../images/panel/elevator_down.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

	getMainPanelPage(){
        return(
            <View style={{flex: 1}}>
                {this.renderTopSwitch()}
                {this.renderCenterView()}
            </View>
        )
    }

	render() {
		const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getHeaderView()}
				{this.getMainPanelPage()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
    topSwitch:{
        flexDirection: 'row',
        paddingHorizontal: '12%',
        marginTop: StatusBarHeight + NavigationBarHeight + 40,
        justifyContent:'space-between'
    },
    switchBtn:{
        width: 70,
        height: 70,
        borderRadius: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor:Colors.borderLightGray,
        borderWidth:0.5,
    },
    switchImg:{
        width: 40,
        height: 40,
        resizeMode: 'contain'
    },
    switchText:{
        marginTop: 5,
        fontSize: 13
    },
    btnWrapper:{
        justifyContent: 'center',
        alignItems: 'center'
    },
    centerWrapper:{
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        paddingBottom: BottomSafeMargin + screenH * 0.1
    },
    centerBtn:{
        marginBottom: 35,
        height: 70,
        width: 150,
        borderWidth: 0.5,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    centerImg:{
        width: 30,
        height: 30,
        resizeMode: 'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Elevator)



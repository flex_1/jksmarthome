/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter,
    Modal,
    Picker
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import Slider from "react-native-slider";
import Panel from './Panel';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import { WebView } from 'react-native-webview';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;


class ColorLightPanel extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		
		this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

			lightVal: 0,
            pickerData: [],
            lightMode: null,
            
            delayModal: false,
            colorModal: false,
            currentColor: '#d9aaaa',
            colors: ['#ff0000','#00ff00','#0000ff','#ffffff',''],

            modesNumbersArrs: []
		}
	}

	componentDidMount() {
		super.componentDidMount()
		this.requestDeviceStatus()
	}

	componentWillUnmount() {
        super.componentWillUnmount()
    }

    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        this.setState({
            lightVal: parseInt(data.light) || 0,
            currentColor: data.color
        })
        this.getCurrentLightMode(this.state.modes, data.mode)
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				let res = data.result && data.result.deviceStatus
				try {
					res = JSON.parse(res)
                    let lightVal = Math.floor(res.light) || 0
                    let lightModeNumber = res.mode
					this.setState({
                        isCollect: data.result && data.result.isCollect,
                        modes: data.result && data.result.modes,
                        lightVal: lightVal,
                        currentColor: res.color,
					})
                    if(data.result && data.result.modes){
                        this.initPickerData(data.result.modes, lightModeNumber)
                    }
				} catch (error) {
					ToastManager.show('数据异常')
				}
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}
    
    // 灯光 控制
    async controlDevice(params,callBack){
        params = params || {}
        params.id = this.deviceData.deviceId
        if(!params.color_hex){
            params.color_hex = this.state.currentColor
        }
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
                timeout: NetParams.networkUserReactTimeoutTime,
				params: {
                    ...params,
                    light: Math.floor(this.state.lightVal)
                }
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }
    
    // 初始化灯光效果
    initPickerData(modes, lightModeNumber){
        if(!modes || modes.length<=0){
            return
        }
        
        let pickerData = []
        for (const mode of modes) {
            pickerData.push(mode.name)
            this.state.modesNumbersArrs.push(mode.number)
        }
        this.setState({
            pickerData: pickerData,
            modesNumbersArrs: this.state.modesNumbersArrs
        },()=>{
            this.getCurrentLightMode(modes,lightModeNumber)
        })
    }

    // 获取 灯光效果 状态
    getCurrentLightMode(modes, mode){
        if(!this.state.modesNumbersArrs){
            return
        }
        let index = this.state.modesNumbersArrs.indexOf(mode)
        let lightMode = modes[index] && modes[index]['name']
        if(lightMode){
            this.setState({
                lightMode: lightMode
            })
        }
    }

	_onChange =(value)=>{
		let target = Math.floor(value)
        this.setState({
            lightVal: target,
        })
	};
	
	_complete =()=>{
		this.controlDevice({},()=>{
            this.setState({lightMode: null})
		})
    }
    
    onMessage(colorValue){
        if(colorValue && colorValue.includes('#')){
            this.controlDevice({color_hex: colorValue},()=>{
                this.setState({
                    currentColor: colorValue
                })
            })
        }
        this.setState({
            colorModal: false,
            lightMode: null
        })
    }

    // 灯光延迟选择器
    renderDelayModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.delayModal}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={styles.modalTouchable} 
                    onPress={() => {
                        this.setState({
                            delayModal: false
                        }, () => {
                         window.DoubulePicker.hide()
                        })
                    }}
                >
                </TouchableOpacity>
            </Modal>
        )
    }

    handlePickerSelectData(data, index){
        let modelIndex = index[0]
        let model = this.state.modes[modelIndex]['name']
        let modelNumber = this.state.modes[modelIndex]['number']
        this.setState({
            lightMode: model
        })
        this.controlDevice({mode: modelNumber})
    }

    //适配灯光不同的颜色
    getMainBgColor(){
        const Colors = this.props.themeInfo.colors
        let bgColor = Colors.themeBaseBg

        if(!this.props.themeInfo.isDark){
            if(this.state.currentColor == '#ffffff'){
                bgColor = '#beb1b2'
            }
        }
        return bgColor
    }

    // 弹出 Modal
    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.pickerData,
            pickerTitleText: '请选择灯光效果',
            selectedValue: [this.state.lightMode],
            onPickerConfirm: (data,index) => {
                this.setState({
                    delayModal: false
                })
                this.handlePickerSelectData(data,index);
            },
            onPickerCancel: data => {
                this.setState({
                    delayModal: false
                }, () => {
                    window.CustomPicker.hide()
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

    // 渲染灯光页面
    renderLight(){
        const Colors = this.props.themeInfo.colors

        let lightMode = this.state.lightMode ? '  ' + this.state.lightMode : ''

        let lightValue = this.state.lightVal
        let lightImg = this.state.currentColor == '#ffffff' ? require('../../../images/panel/color_light.png')
        : require('../../../images/panel/color_light_white.png')

        return(
            <View style={styles.topWrapper}>
                <View style={styles.title}>
                    <Text style={[styles.lightText,{color: Colors.themeTextLight}]}>亮度</Text>
                    <Text style={[styles.valueText,{color: Colors.themeText}]}>{lightValue}%</Text>
                    <View style={styles.colorTitle}>
                        <Text style={[styles.colorText,{color: Colors.themeTextLight}]}>色值</Text>
                        <Text style={[styles.colorValue,{color: Colors.themeText}]}>{this.state.currentColor}</Text>
                    </View>
                </View>
               
                <TouchableOpacity activeOpacity={0.7} style={styles.delayTouch} onPress={()=>{
                    this.setState({ delayModal: true })
                }}>
                    <Text style={[styles.delayText,{color: Colors.themeText}]}>灯光效果{lightMode}</Text>
                    <Image style={[styles.downIcon,{tintColor: Colors.themeText}]} source={require('../../../images/deviceIcon/down.png')}/>
                </TouchableOpacity>
                <View style={styles.lightContain}>
                    <View style={[styles.lightView_large,{backgroundColor: this.state.currentColor}]}/>
                    <View style={[styles.lightView_middle,{backgroundColor: this.state.currentColor}]}/>
                    <View style={[styles.lightView,{backgroundColor: this.state.currentColor}]}>
                        <Image style={styles.lightImg} source={lightImg}/>
                    </View>
                </View>
            </View>
        )
    }

    _renderColorsBtns(){
        const Colors = this.props.themeInfo.colors
        let btns = this.state.colors.map((value,index)=>{
            if(index == this.state.colors.length-1){
                return(
                    <TouchableOpacity
                        key={index}
                        style={{width:30,height:30,marginLeft:15}}
                        onPress={()=>{
                            this.setState({
                                colorModal: true
                            })
                        }}
                    >
                        <Image style={styles.colorImg} source={require('../../../images/panel/colorful.png')}/>
                    </TouchableOpacity>
                )
            }
            let border = {}
            if(value == '#ffffff'){
                border = {borderWidth: 1, borderColor: Colors.borderLight}
            }
            return(
                <TouchableOpacity 
                    key={index} 
                    style={[styles.btns,border,{backgroundColor:value}]}
                    onPress={()=>{
                        this.controlDevice({color_hex:value},()=>{
                            this.setState({
                                currentColor: value,
                                lightMode: null
                            })
                        })
                    }}
                >
                </TouchableOpacity>
            )
        })
        return(
            <View style={{flexDirection:'row',marginTop:screenH*0.1}}>
                {btns}
            </View>
        )
    }

    renderController(){
        const Colors = this.props.themeInfo.colors

        return (
            <View style={styles.bottomCtr}>
                <Slider
					style={{width:'90%'}}
					minimumValue={0}
					maximumValue={100}
					step={1}
          			value={this.state.lightVal}
					onValueChange={this._onChange}
					onSlidingComplete={this._complete}
					minimumTrackTintColor= {this.state.currentColor}
					maximumTrackTintColor={Colors.themeInactive}
					thumbTintColor= {this.state.currentColor}
					trackStyle={{height:8,borderRadius: 4}}
					thumbStyle={{width:26,height:26,borderRadius:13}}
        		/>
                {this._renderColorsBtns()}
            </View>
        )
    }

    _renderColorPanel(){
        return(
            <View style={{flex: 1}}>
                <WebView
                    startInLoadingState={true}
                    scrollEnabled = {false}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    source={{uri: NetUrls.colorPanel}}
                    originWhitelist={['*']}
                    scalesPageToFit={false}
                    onMessage={event => this.onMessage(event.nativeEvent.data) }
                />
			</View>
        )
    }

    // 灯光颜色选择器
    renderColorModal(){
        return(
            <Modal 
                transparent={true}
                visible={this.state.colorModal}
                animationType={'slide'}
            >
                <TouchableOpacity
                    activeOpacity={1}
                    style={{flex: 4}}
                    onPress={()=>{
                        this.setState({colorModal: false})
                    }}
                />
                <View style={styles.panelBg}>
                    {this._renderColorPanel()}
                </View>
            </Modal>
        )
    }

	getMainPanelPage(){
		return(
            <View style={[styles.main,{backgroundColor: this.getMainBgColor()}]}>
                {this.renderLight()}
                {this.renderController()}
                {this.renderDelayModal()}
                {this.renderColorModal()}
            </View>
		)
	}
}

const styles = StyleSheet.create({
    main: {
		flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
	topWrapper:{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 0.05*screenH,
        flex: 1
    },
    title:{
        flexDirection: 'row',
        justifyContent:'center',
        alignItems: 'center',
    },
    colorTitle:{
        marginLeft: 30,
        flexDirection: 'row',
        justifyContent:'center',
        alignItems: 'center',
    },
    lightText:{
        fontSize: 14,
        marginTop: 5
    },
    valueText:{
        fontSize: 25,
        fontWeight: 'bold',
        marginLeft: 10
    },
    colorText:{
        fontSize: 14,
    },
    colorValue:{
        fontSize: 20,
        fontWeight: 'bold',
        marginLeft: 8
    },
    delayTouch:{
        marginTop: 15,
        flexDirection: 'row',
        justifyContent:'center',
        alignItems: 'center',
        paddingVertical: 15,
        paddingHorizontal: 50
    },
    delayText:{
        fontSize: 13
    },
    downIcon:{
        width: 9,
        height: 5,
        resizeMode: 'contain',
        marginLeft: 10
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: 'rgba(0,0,0,0.5)' 
    },
    lightContain:{
        marginTop: 20,
        width: '100%',
        height: screenW*0.6,
    },
    lightView_large:{
        position: 'absolute',
        alignSelf: 'center',
        opacity: 0.2,
        width: screenW*0.6,
        height: screenW*0.6,
        borderRadius: screenW*0.3
    },
    lightView_middle:{
        position: 'absolute',
        alignSelf: 'center',
        opacity: 0.4,
        top: screenW*0.075,
        width: screenW*0.45,
        height: screenW*0.45,
        borderRadius: screenW*0.225,
        justifyContent: 'center',
        alignItems: 'center'
    },
    lightView:{
        position: 'absolute',
        alignSelf: 'center',
        top: screenW*0.15,
        width: screenW*0.3,
        height: screenW*0.3,
        borderRadius: screenW*0.15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    lightImg:{
        width: screenW*0.15,
        height: screenW*0.15,
        resizeMode: 'contain'
    },
    bottomCtr:{
        paddingBottom: BottomSafeMargin + 0.1*screenH,
        paddingTop: 40,
        width: '100%',
        justifyContent:'center',
        alignItems: 'center',
    },
    btns:{
        width:30,
        height:30,
        borderRadius:15,
        marginLeft:15
    },
    colorImg:{
        width:30,
        height:30,
        resizeMode:'contain'
    },
    panelBg:{
        backgroundColor:Colors.white,
        flex: 6,
        paddingBottom: BottomSafeMargin + 10
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ColorLightPanel)



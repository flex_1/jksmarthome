/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 窗帘
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter,
	PanResponder,
	Modal
} from 'react-native';
import { Colors, NetUrls,NotificationKeys,NetParams } from '../../../common/Constants';
import Slider from "react-native-slider";
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;


class Cupboard extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		
		this.state = {
			...this.state,
			switch: 0,
            isRuning: false,
            value: 0,
            lastValue: 0
		}
	}

	componentDidMount() {
        super.componentDidMount()
        if(this.isTCP){
            this.requestStatusByTCP()
        }else{
            this.requestDeviceStatus()
        }
	}

	componentWillUnmount() {
        this.timer && clearTimeout(this.timer);
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        let value = Math.floor(data.unwind) || 0
		this.setState({
            value: value,
            lastValue: value
        })
    }

    // tcp 数据状态解析
    updateDeviceStatusByTCP(data){
        if(!data) return
        let value = data.split('_')[1] || 0
        
        this.setState({
            value: parseInt(value),
            lastValue: parseInt(value)
        })
    }
	
	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				let res = data.result && data.result.deviceStatus
				try {
					res = JSON.parse(res)
					this.setState({
                        isCollect: data.result && data.result.isCollect,
						value: Math.floor(res.unwind) || this.state.value,
						lastValue: Math.floor(res.unwind) || this.state.value
					})
				} catch (error) {
					ToastManager.show('数据异常')
				}
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}
    
    // 窗帘 控制
    async controlDevice(params,callBack){
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 设置正反
    async setDeviceProperties(params,callBack){
        params = params || {}
        params.deviceId = this.deviceData.deviceId
        params.type = 0 //窗帘
		try {
			let data = await postJson({
				url: NetUrls.setDeviceProperties,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

	_onChange =(value)=>{
        this.setState({
            value: Math.floor(value),
            lastValue: Math.floor(value)
        })
	};
	
	_complete =(value)=>{
		this.controlDevice({unwind: Math.floor(value) },()=>{
			// 设置 间隔时间
			value = Math.floor(value)
			this.state.lastValue = Math.floor(value)
		})
    }
    
    valueClick(value){
        this.controlDevice({ unwind: value },()=>{
            this.setState({
                value: value,
                lastValue: value
            })
        })
    }

    /**
	 * 中间内容展示
     * @returns {*}
     */
    getCenterView(){
        const curtainHeight = screenH/3.0
        let drawerImg = require('../../../images/cupboard/0.png')
        if(this.state.value > 0 && this.state.value <= 50){
            drawerImg = require('../../../images/cupboard/10.png')
        }else if(this.state.value > 50 && this.state.value < 100){
            drawerImg = require('../../../images/cupboard/50.png')
        }else if(this.state.value == 100){
            drawerImg = require('../../../images/cupboard/100.png')
        }
		return(
			<View style={[styles.curtainWrapper,{height: curtainHeight}]}>
				<Image style={{width:'100%',height:'100%',resizeMode:'contain'}} source={drawerImg}/>
			</View>
		)
    }

    //中间按钮
	showControllerBtns(){
        return(
            <View style={styles.btnsWrapper}>
                <TouchableOpacity style={[styles.btnTouch,{marginRight:50}]} onPress={()=>{
                    this.controlDevice({number: 1},()=>{})
                }}>
                    <Text style={{color:Colors.white,fontSize:16}}>打开</Text>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.btnTouch,]} onPress={()=>{
                    this.controlDevice({number: 3},()=>{})
                }}>
                    <Text style={{color:Colors.white,fontSize:16}}>暂停</Text>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.btnTouch,{marginLeft:50}]} onPress={()=>{
                    this.controlDevice({number: 2},()=>{})
                }}>
                    <Text style={{color:Colors.white,fontSize:16}}>关闭</Text>
                </TouchableOpacity>
            </View>
        )
    }

	// 展开程度
    showProcess(){
        let btnsArr = [0, 25, 50, 75, 100]
        
        let btns = btnsArr.map((value, index)=>{
            let valueText = value
            
            return(
                <TouchableOpacity key={'btns_'+index} style={styles.quickBtn} activeOpacity={0.7} onPress={()=>{
                    this.valueClick(value)
                }}>
                    <View style={styles.shortLine}/>
                    <Text style={styles.quickText}>{valueText}</Text>
                </TouchableOpacity>
            )
        })

		return(
			<View style={styles.processWrapper}>
				<View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                    <Text style={styles.valueText}>{this.state.value+'%  '}</Text>
				</View>
				<Slider
					// disabled={this.state.isRuning}
					style={{width: '80%',marginTop:20}}
					minimumValue={0}
                    maximumValue={100}
          			value={this.state.value}
					onValueChange={this._onChange}
					onSlidingComplete={this._complete}
					minimumTrackTintColor='#fff'
					maximumTrackTintColor='#2461D4'
					thumbTintColor='#fff'
					trackStyle={{height:8,borderRadius: 4}}
					thumbStyle={{width:26,height:26,borderRadius:13}}
					onSlidingStart={()=>{
						// if(this.state.isRuning){
						// 	ToastManager.show('卷帘运行中')
						// 	return
						// }
					}}
        		/>
                <View style={styles.quickBtnWrapper}>
                    {btns}
                </View>
			</View>
		)
	}

	getMainPanelPage(){
		return(
			<View style={styles.main}>
                <View style={styles.topWrapper}>
                    {this.getCenterView()}
                </View>
                {this.showControllerBtns()}
                {this.showProcess()}
			</View>
		)
	}

	// render() {
	// 	return (
	// 		<View style={styles.container}>
	// 			{this.getHeaderView()}
	// 			<ScrollView style={styles.container}>
	// 				{this.getMainPanelPage()}
	// 			</ScrollView >
	// 		</View>

	// 	)
	// }
}



const styles = StyleSheet.create({
	main: {
		flex: 1,
        backgroundColor: '#3E7EF7',
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
    topWrapper:{
        flex: 1,
        justifyContent:'center',
        alignItems:'center',
    },
    curtainWrapper:{
        justifyContent:'center',
        width:'80%',
        flexDirection:'row',
        marginTop: '10%'
    },
    btnsWrapper:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        paddingVertical: 20
    },
    btnTouch:{
        height:50,
        width:50,
        backgroundColor:'#59A1FB',
        borderRadius:25,
        justifyContent: 'center',
        alignItems: 'center',
    },
    processWrapper:{
        paddingTop: 30,
        paddingBottom: BottomSafeMargin + 0.1*screenH,
        width:'100%',
        justifyContent:'center',
        alignItems:'center',
    },
    quickBtnWrapper:{
        width: '80%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    quickBtn:{
        width: 28,
        justifyContent:'center',
        alignItems:'center',
        marginTop: 15
    },
    quickText:{
        fontSize: 15,
        color: Colors.white
    },
    shortLine:{
        width: 2,
        height: 8,
        backgroundColor:Colors.white
    },
    valueText:{
        color:'#fff',
        fontSize:25, 
        marginLeft:10,
        fontWeight:'bold',
        width: 80,
        textAlign: 'center'
    }
    
});

export default Cupboard



/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	Modal,
    SafeAreaView,
    Vibration
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import Panel from './Panel';
import { connect } from 'react-redux';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class TVRemoteController extends Panel {

	constructor(props) {
		super(props);
		
		this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            btnsModelVisible: false,
            brandModelVisible: false,
            modelType: 1,  // 1-数字  2-平台按钮,
            brandList: null,
            currentBrand: null,
            brandButtonList: null
		}
	}

	componentDidMount() {
        this.requestTVBrands()
        
    }
    
    componentWillUnmount() {
        
    }

    // 获取电视品牌
    async requestTVBrands(){
		try {
			let data = await postJson({
				url: NetUrls.getTVBrand,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: {
                    deviceId : this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
                let brands = data.result && data.result.brandList
                let currentBrand = data.result && data.result.brand

                let brandList = []
                if(brands){
                    brandList = brands.split(',')
                    if(!currentBrand){
                        currentBrand = brandList[0]
                    }
                    this.setState({
                        brandList: brandList,
                        currentBrand: currentBrand
                    },()=>{
                        this.requestTVButtons()
                    })
                }
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 获取电视按钮
    async requestTVButtons(){
        try {
			let data = await postJson({
				url: NetUrls.getTVButtonByBrand,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: {
                    deviceId : this.deviceData.deviceId,
                    brand: this.state.currentBrand
                }
			});
			if (data.code == 0) {
                this.setState({
                    brandButtonList: data.result || []
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 遥控器
    async controlDevice(params){
        this.vibrate()
        params = params || {}
        params.id = this.deviceData.deviceId
        params.brand = this.state.currentBrand
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 按钮点击振动
    vibrate(){
        if(Platform.OS == 'android'){
            Vibration.vibrate([0,50], false)
        }
    }
    
	getCircularBg(){
		if(!this.state.currentPress){
			return null
        }
        const Colors = this.props.themeInfo.colors
        let selectIcon = null
        // if(this.state.currentPress == 1){
        //     selectIcon = <Image source={require('../../../images/panel/TV/up_select.png')} style={styles.btnSelectIcon}/>
        // }else if(this.state.currentPress == 2){
        //     selectIcon = <Image source={require('../../../images/panel/TV/right_select.png')} style={styles.btnSelectIcon}/>
        // }else if(this.state.currentPress == 3){
        //     selectIcon = <Image source={require('../../../images/panel/TV/down_select.png')} style={styles.btnSelectIcon}/>
        // }else if(this.state.currentPress == 4){
        //     selectIcon = <Image source={require('../../../images/panel/TV/down_select.png')} style={styles.btnSelectIcon}/>
        // }
        
		let percent = '71.71067812%'
		let rotateZ = 90 * (this.state.currentPress-1) + 'deg'
		return(
			<View style={[styles.selectWrapper,{width:percent,transform:[{rotateZ:rotateZ}]}]}>
				<Image style={[styles.selectImg,{tintColor: Colors.borderLight}]} source={require('../../../images/panel/tv_volate_bg_icon.png')}/>
                {selectIcon}
			</View>
		)
	}

	getCircularButton(index,icon,btnStyle){
        const Colors = this.props.themeInfo.colors

		return(
			<TouchableOpacity style={{...styles.cirlBtn,...btnStyle}} 
				onPress={()=>{
					if(index == 1){
						//上
						this.controlDevice({number:20})
					}
					else if(index == 2){
						//右
						this.controlDevice({number:21})
					}
					else if(index == 3){
						//下
						this.controlDevice({number:22})
					}
					else if(index == 4){
						//左
						this.controlDevice({number:23})
					}
				}}
				onPressIn={()=>{
					this.setState({currentPress:index})
				}}
				onPressOut={()=>{
					this.setState({currentPress:null})
				}}
			>
				<Image source={icon} style={[styles.btnImg,{tintColor: Colors.themeTextLight}]}/>
			</TouchableOpacity>
		)
    }

    //获取上部分按钮
    renderTopBtns(){
        const Colors = this.props.themeInfo.colors
        
        return(
            <View style={styles.topWrapper}>
                <View style={{flex: 1,paddingRight: 12}}>
                    <TouchableOpacity 
                        activeOpacity={0.7} 
                        style={[styles.brandsBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]} 
                        onPress={()=>{
                            if(!this.state.brandList || this.state.brandList.length<=0){
                                ToastManager.show('暂无更多品牌')
                                return
                            }
                            this.setState({ brandModelVisible: true })
                        }}
                    >
                        <Text style={[styles.brandsText,{color: Colors.themeTextLight}]}>{this.state.currentBrand}</Text>
                        <Image style={[styles.navImg,{tintColor: Colors.themeTextLight}]} source={require('../../../images/enter.png')} />
                    </TouchableOpacity>
                    <View style={{flexDirection:'row',marginTop:15,justifyContent:'space-between'}}>
                        <TouchableOpacity style={[styles.topBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]} onPress={()=>{
                            this.controlDevice({number:12})
                        }}>
                            <Image style={styles.tvBtnImg} source={require('../../../images/panel/TV/tv_home.png')}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.topBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]} onPress={()=>{
                            this.controlDevice({number:13})
                        }}>
                            <Image style={styles.tvBtnImg} source={require('../../../images/panel/TV/tv_menus.png')}/>
                        </TouchableOpacity>
                    </View>
                </View>
                
                <View style={styles.topRightWrapper}>
                    <TouchableOpacity style={[styles.topBtn,{marginBottom: 15,borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]} onPress={()=>{
                        this.controlDevice({number:10})
                    }}>
                        <Image style={styles.tvBtnImg} source={require('../../../images/panel/TV/tv_rightArrow.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.topBtn,{marginBottom: 15,borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]} onPress={()=>{
                        this.controlDevice({number:11})
                    }}>
                        <Image style={styles.tvBtnImg} source={require('../../../images/panel/TV/tv_switch.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.topBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]} onPress={()=>{
                        this.setState({ btnsModelVisible: true, modelType: 1 })
                    }}>
                        <Text style={{fontSize: 18, color: Colors.themeTextLight}}>123</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.topBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]} onPress={()=>{
                        this.controlDevice({number:14})
                    }}>
                        <Image style={styles.tvBtnImg} source={require('../../../images/panel/TV/tv_back.png')}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    // 中间控制按钮
	renderCenterView(){
        const Colors = this.props.themeInfo.colors

		return(
            <View style={styles.centerWrapper}>
                <View style={[styles.centerBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]}>
				    {this.getCircularBg()}
				    <TouchableOpacity style={[styles.okBtnWrapper,{backgroundColor: Colors.themeBaseBg}]} onPress={()=>{
                        this.controlDevice({number:24})
                    }}>
                        <Text style={{color:Colors.themeTextLight,fontSize:25,fontWeight:'bold'}}>OK</Text>
				    </TouchableOpacity>
				    {this.getCircularButton(1,require('../../../images/panel/TV/up.png'),{left:'25%',height:'25%',top:0,width:'50%'})}
				    {this.getCircularButton(2,require('../../../images/panel/TV/right.png'),{right:0,height:'50%',top:'25%',width:'25%'})}
				    {this.getCircularButton(3,require('../../../images/panel/TV/down.png'),{left:'25%',height:'25%',bottom:0,width:'50%'})}
				    {this.getCircularButton(4,require('../../../images/panel/TV/left.png'),{left:0,height:'50%',top:'25%',width:'25%'})}
                </View>
            </View>
		)
    }
    
    //底部控制按钮
    renderBottomBtns(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={styles.bottomWrapper}>
                <View>
                    <View style={[styles.chanelWrapper,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]}>
                        <TouchableOpacity style={styles.chanelBtn} onPress={()=>{
                            this.controlDevice({number:15})
                        }}>
                            <Image style={styles.tvBtnImg} source={require('../../../images/panel/TV/tv_add.png')}/>
                        </TouchableOpacity>
                        <Text style={{color: Colors.themeText}}>CH</Text>
                        <TouchableOpacity style={styles.chanelBtn} onPress={()=>{
                            this.controlDevice({number:16})
                        }}>
                            <Image style={styles.tvBtnImg} source={require('../../../images/panel/TV/tv_reduce.png')}/>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity 
                        style={[styles.bottomMoreBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]} 
                        onPress={()=>{
                            this.setState({ btnsModelVisible: true, modelType: 2 })
                        }}
                    >
                        <Image style={styles.tvBtnImg} source={require('../../../images/panel/TV/tv_points.png')}/>
                    </TouchableOpacity>
                </View>
                <View>
                    <View style={[styles.chanelWrapper,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]}>
                        <TouchableOpacity style={styles.chanelBtn} onPress={()=>{
                            this.controlDevice({number:17})
                        }}>
                            <Image style={styles.tvBtnImg} source={require('../../../images/panel/TV/tv_add.png')}/>
                        </TouchableOpacity>
                        <Text style={{color: Colors.themeText}}>VOL</Text>
                        <TouchableOpacity style={styles.chanelBtn} onPress={()=>{
                            this.controlDevice({number:18})
                        }}>
                            <Image style={styles.tvBtnImg} source={require('../../../images/panel/TV/tv_reduce.png')}/>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity 
                        style={[styles.bottomMoreBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]} 
                        onPress={()=>{
                            this.controlDevice({number:19})
                        }}
                    >
                        <Image style={styles.tvBtnImg} source={require('../../../images/panel/TV/tv_voice.png')}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    // 数字按钮
    _renderNumbersView(){
        if(this.state.modelType == 2){
            return null
        }
        const Colors = this.props.themeInfo.colors
        const numbers = [1,2,3,4,5,6,7,8,9,'-',0]

        let btns = numbers.map((value, index)=>{
            if(value == '-'){
                return <View key={'number_btn_'+index} style={styles.numberTouchWrapper}/>
            }

            return(
                <View key={'number_btn_'+index} style={styles.numberTouchWrapper}>
                    <TouchableOpacity style={[styles.numberTouch,{backgroundColor:Colors.themeBg,borderColor: Colors.borderLight}]} onPress={()=>{
                        this.controlDevice({number:value})
                    }}>
                        <Text style={[styles.numberText,{color: Colors.themeText}]}>{value}</Text>
                    </TouchableOpacity>
                </View>
            )
        })
        return(
            <View style={[styles.btnsWrapper,{backgroundColor:Colors.themeBaseBg}]}>
                {btns}
            </View>
        )
    }

    // 其他按钮
    _renderOthersView(){
        if(this.state.modelType == 1){
            return null
        }
        const Colors = this.props.themeInfo.colors

        if(!this.state.brandButtonList || this.state.brandButtonList.length<=0){
            return(
                <View style={[styles.emtyList,{backgroundColor: Colors.themeBg}]}>
                    <Text style={{fontSize: 15,color: Colors.themeTextLight}}>暂无按钮</Text>
                </View>
            )
        }

        let btns = this.state.brandButtonList.map((value, index)=>{
            return(
                <TouchableOpacity 
                    key={'brand_btn_'+index} 
                    style={[styles.brandBtnTouch,{backgroundColor:Colors.themeBg,borderColor: Colors.borderLight}]}
                    onPress={()=>{
                        this.controlDevice({number:value.buttonNumber})
                    }}
                >
                    <Text style={[styles.brandBtnText,{color: Colors.themeTextLight}]}>{value.buttonName}</Text>
                </TouchableOpacity>
            )
        })
        return (
            <ScrollView contentContainerStyle={styles.scrollContent} style={{backgroundColor:Colors.themeBaseBg}}>
                <View style={styles.brandBtnWrapper}>
                    {btns}
                </View>
            </ScrollView>
        )
    }

    // 数字弹框
    renderBtnsModel(){
        return(
            <Modal 
                transparent={true}
                visible={this.state.btnsModelVisible}
                animationType={'fade'}
            >
                <TouchableOpacity
                    activeOpacity={1}
                    style={{flex: 4,backgroundColor:Colors.translucent}}
                    onPress={()=>{
                        this.setState({btnsModelVisible: false})
                    }}
                />
                <View style={{backgroundColor: 'white',flex: 6}}>
                    {this._renderNumbersView()}
                    {this._renderOthersView()}
                </View>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.brandList || [],
            pickerTitleText: '请选择电视品牌',
            selectedValue: [this.state.currentBrand],
            onPickerConfirm: (data,index) => {
                this.setState({
                    brandModelVisible: false,
                    currentBrand: data[0]
                },()=>{
                    this.requestTVButtons()
                })
            },
            onPickerCancel: data => {
                this.setState({
                    brandModelVisible: false
                }, () => {
                    window.CustomPicker.hide()
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

    // 电视品牌选择器
    renderTVBrandModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.brandModelVisible}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={styles.modalTouchable} 
                    onPress={() => {
                        this.setState({
                            brandModelVisible: false
                        }, () => {
                            window.CustomPicker.hide()
                        })
                    }}
                >
                </TouchableOpacity>
            </Modal>
        )
    }

	getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

		return(
			<View style={[styles.main,{backgroundColor: Colors.themeBaseBg}]}>
                {/* {this.renderTVBrands()} */}
                {this.renderTopBtns()}
				{this.renderCenterView()}
                {this.renderBottomBtns()}
                {this.renderBtnsModel()}
                {this.renderTVBrandModal()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
		flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
	cirlBtn:{
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		zIndex: 1000,
    },
    centerWrapper:{
        width: '100%',
        alignItems:'center',
        justifyContent:'center',
        flex: 1
	},
	centerBtn:{
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: screenW*0.25,
		width: screenW*0.5,
        height: screenW*0.5,
        borderWidth: 0.5,
	},
	okBtnWrapper:{
		position: 'absolute',
		alignItems: 'center',
		justifyContent: 'center',
		width: '50%',
		height: '50%',
		borderRadius: screenW*0.15
    },
    selectWrapper:{
        position:'absolute',
        alignItems:'center',
        height:'100%',
        backgroundColor:'transparent',
    },
    selectImg:{
        width:'100%',
        height:'50%',
        resizeMode:'contain',
        marginTop:-2
    },
    btnImg:{
        position: 'absolute',
        width:24,
        height:24,
        resizeMode:'contain'
    },
    btnSelectIcon:{
        position: 'absolute',
        top:'8%', 
        width:24,
        height:24,
        resizeMode:'contain'
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: 'rgba(0,0,0,0.5)' 
    },
    tvBtnImg:{
        width: 28,
        height: 28,
        resizeMode: 'contain'
    },
    
    topWrapper:{
        width:'100%',
        paddingHorizontal:20,
        marginTop:15,
        alignItems:'center',
        flexDirection:'row'
    },
    brandsBtn:{
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingVertical: 5,
        width: '100%',
        height: 54,
        marginVertical:5,
        borderRadius: 30,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.5
    },
    brandsText:{
        fontSize: 15
    },
    navImg: {
		width: 12,
		height: 12,
        resizeMode: 'contain',
        marginLeft: 5
    },
    topRightWrapper:{
        flexDirection:'row',
        flex: 1,
        justifyContent:'space-between',
        flexWrap:'wrap',
        paddingLeft: 12
    },
    topBtn:{
        width:64,
        height:64,
        borderRadius:32,
        justifyContent:'center',
        alignItems: 'center',
        borderWidth: 0.5
    },
    bottomWrapper:{
        width:'100%',
        paddingHorizontal:'15%',
        paddingBottom: BottomSafeMargin+20,
        flexDirection:'row',
        justifyContent:'space-between'
    },
    chanelWrapper:{
        width:60,
        borderRadius: 30,
        alignItems:'center',
        borderWidth: 0.5
    },
    chanelBtn:{
        width:60,
        height:60,
        justifyContent:'center',
        alignItems:'center'
    },
    bottomMoreBtn:{
        marginTop: 10,
        width: 60,
        height: 60,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.5
    },
    btnsWrapper:{
        paddingHorizontal:'8%',
        flex: 1,
        flexDirection:'row',
        flexWrap:'wrap',
        paddingVertical: 20
    },
    numberTouchWrapper:{
        width: '33%',
        justifyContent:'center',
        alignItems:'center',
        marginBottom: 15
    },
    numberTouch:{
        width:70,
        height:70,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:35,
        borderWidth: 0.5
    },
    numberText:{
        fontSize: 20,
        fontWeight: 'bold'
    },
    emtyList:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    brandBtnTouch:{
        width:'30%',
        height: 44,
        borderRadius: 5,
        justifyContent:'center',
        alignItems:'center',
        marginBottom:15,
        borderWidth: 0.5
    },
    brandBtnWrapper:{
        flexDirection:'row',
        justifyContent:'space-between',
        width:'100%',
        paddingTop: 20,
        flexWrap:'wrap',
        paddingHorizontal:16
    },
    brandBtnText:{
        fontSize: 15,
        textAlign: 'center'
    },
    scrollContent:{
        paddingBottom: 50
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(TVRemoteController) 



/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Animated,
	Dimensions,
    Modal,
    Keyboard,
    Alert,
    ImageBackground
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import Panel from './Panel';
import ActionSheet from 'react-native-actionsheet';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const bgW = screenW*0.8
const bgH = screenW*0.8*1.4

class RadioCurtains extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
        
        this.typeId = getParam('typeId')
        this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            status: null,   //1-打开  2-关闭  3-暂停
            direction: 1,   //1-上下  2-左右
        }
    }
    
	componentDidMount() {
        super.componentDidMount()
        this.requestDeviceStatus()
	}

	componentWillUnmount(){
        super.componentWillUnmount()
    }

    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;
        
        this.setState({
            status: data.status
        })
    }

    async requestDeviceStatus(){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
            SpinnerManager.close()
			if (data.code == 0) {
				this.setState({
                    status: data.result?.status,
                    direction: data.result?.direction
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    async requestControlBtn(status, callBack){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				params: {
                    deviceId: this.deviceData.deviceId,
                    status: status
                }
			});
            SpinnerManager.close()
			if (data.code == 0) {
                callBack && callBack()
				ToastManager.show(data.msg)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    // 修改设备参数
	async updateDeviceParams(direction){
		SpinnerManager.show()
		try {
			let deviceData = await postJson({
				url: NetUrls.saveDevice,
				params: {
					id: this.deviceData.deviceId,
					direction: direction
				}
			});
			SpinnerManager.close()
			if (deviceData.code == 0) {
				this.setState({
                    direction: direction
                })
			} else {
				ToastManager.show(deviceData.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
	}

    btnOnClick(status){
        this.requestControlBtn(status, ()=>{
            this.setState({
                status: status
            })
        })
    }

    renderBtnBg(status){
        if(this.state.status == status){
            return require('../../../images/ProtocolControl/btn_highlight.png')
        }else{
            return require('../../../images/ProtocolControl/btn1.png')
        }
    }

    renderBtnImg(status){
        if(this.state.direction == 2){
            if(status == 1){
                return require('../../../images/ProtocolControl/curtain_out.png')
            }else{
                return require('../../../images/ProtocolControl/curtain_in.png')
            }
        }else{
            if(status == 1){
                return require('../../../images/ProtocolControl/curtain_up.png')
            }else{
                return require('../../../images/ProtocolControl/curtain_down.png')
            }  
        }
    }

    checkCode(){
        this.actionSheet.show()
    }

    renderActionSheet() {
		return (
			<ActionSheet
				ref={e => this.actionSheet = e}
				options={['设为卷帘','设为开合帘','对码','设置行程','取消']}
				cancelButtonIndex={4}
				onPress={(index) => {
					if(index == 0){
                        this.updateDeviceParams(1)
                    }else if(index == 1){
                        this.updateDeviceParams(2)
                    }else if(index == 2){
                        this.requestControlBtn(4)
                    }else if(index == 3){
                        this.requestControlBtn(5)
                    }
				}}
			/>
		)
    }

    getHeaderView() {
        const { pop } = this.props.navigation;

        const Colors = this.props.themeInfo.colors;
        let backIcon = this.state.isWhiteBg ? require('../../../images/back.png') : require('../../../images/back_white.png')
        let settingIcon = this.state.isWhiteBg ? require('../../../images/setting.png') : require('../../../images/setting_white.png')
        let titleColor = this.state.isWhiteBg ? Colors.themeTextBlack : Colors.white
        
        return (
            <View style={styles.header}>
                <View style={styles.status}/>
                <View style={styles.nav}>
                    <TouchableOpacity style={styles.backTouch} onPress={() => { pop() }}>
                        <Image style={{ width: 10, height: 18 }} source={backIcon} />
                        <Text numberOfLines={1} style={[styles.navTitle,{color: titleColor}]}>{this.state.title}</Text>
                    </TouchableOpacity>
                    <View style={{ flex: 1 }} />
                    <TouchableOpacity style={styles.navTouch} onPress={() => { 
                        this.checkCode()
                    }}>
                        <Image style={[styles.sahreImg,{tintColor: Colors.themeText}]} source={require('../../../images/ProtocolControl/setting.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.navTouch} onPress={() => { this.goToSetting() }}>
                        <Image style={styles.navImg} source={settingIcon} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    renderMainController(){
        return(
            <ImageBackground style={styles.ctrBg} source={require('../../../images/ProtocolControl/bg1.png')}>
                <TouchableOpacity onPress={()=>{ this.btnOnClick(1) }}>
                    <ImageBackground style={styles.ctrBtnBg} source={this.renderBtnBg(1)}>
                        <Image style={styles.btnImg} source={this.renderBtnImg(1)}/>
                    </ImageBackground>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{ this.btnOnClick(3) }}>
                    <ImageBackground style={[styles.ctrBtnBg,{marginTop: bgH*0.1}]} source={this.renderBtnBg(3)}>
                        <Image style={styles.btnImg} source={require('../../../images/ProtocolControl/curtain_pause.png')}/>
                    </ImageBackground>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{ this.btnOnClick(2) }}>
                    <ImageBackground style={[styles.ctrBtnBg,{marginTop: bgH*0.1}]} source={this.renderBtnBg(2)}>
                        <Image style={styles.btnImg} source={this.renderBtnImg(3)}/>
                    </ImageBackground>
                </TouchableOpacity>
            </ImageBackground>
        )
    }

    getMainPanelPage(){
        const Colors = this.props.themeInfo.colors;

		return(
			<View style={[styles.wrapper,{backgroundColor: Colors.themeBg}]}>
				<View style={styles.main}>
                    {this.renderMainController()}
                </View>
                {this.renderActionSheet()}
			</View>
		)
	}

	// render() {
    //     const Colors = this.props.themeInfo.colors;

	// 	return (
	// 		<View style={[styles.wrapper,{backgroundColor: Colors.themeBaseBg}]}>
	// 			<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
    //                 {this.renderMainController()}
    //             </View>
	// 		</View>
	// 	)
	// }
}

const styles = StyleSheet.create({
	wrapper: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
    },
    main:{
        marginTop: screenH*0.1,
        flex: 1,
        alignItems: 'center'
    },
    ctrBg:{
        width: bgW, 
        height: bgH, 
        justifyContent:'center',
        alignItems:'center'
    },
    ctrBtnBg:{
        width: bgW*0.48, 
        height: bgW*0.48*0.36,
        justifyContent:'center',
        alignItems:'center',
        resizeMode: 'contain'
    },
    btnImg:{
        width: bgW*0.48*0.36*0.4,
        height: bgW*0.48*0.36*0.4,
        resizeMode: 'contain'
    },

    header: {
        position: 'absolute',
        zIndex: 999,
        top: 0,
        left: 0,
        width: '100%',
        height: StatusBarHeight + NavigationBarHeight
    },
    headerContain: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20,
        paddingHorizontal: 16
    },
    status: {
        height: StatusBarHeight,
        width: '100%',
    },
    nav: {
        height: NavigationBarHeight,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 15,
        paddingLeft: 10,
        marginRight: 5
    },
    navImg: {
        width: 22,
        height: 22,
        resizeMode: 'contain'
    },
    sahreImg:{
        width: 22,
        height: 22,
        resizeMode: 'contain'
    },
    navTitle:{ 
        marginLeft: 15, 
        fontSize: 24, 
        fontWeight: 'bold'
    },
    backTouch: {
        height: '100%',
        marginLeft: 16,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        maxWidth:'50%'
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(RadioCurtains)



/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 窗帘
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter
} from 'react-native';
import { Colors, NetUrls,NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel';


class Ventilation extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		
		this.state = {
			...this.state,
			switch: 0,
			speed: 0,
			temperature: 0
		}
	}

	componentDidMount() {
		super.componentDidMount()
		this.requestDeviceStatus()
	}

	componentWillUnmount() {
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

		this.setState({
            speed: parseInt(data.speed),
            switch: data.status == 1,
            temperature: data.temperature
        })
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				let res = data.result && data.result.deviceStatus
				try {
					res = JSON.parse(res)
					this.setState({
                        isCollect: data.result && data.result.isCollect,
						speed: parseInt(res.speed),
						switch: res.status == 1,
						temperature: res.temperature
					})
				} catch (error) {
					ToastManager.show('数据异常')
				}
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}
    
    // 新风 控制
    async controlDevice(params,callBack){
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

	
    /**
	 * 中间内容展示
     * @returns {*}
     */
    getCenterView(){
		
		let status_on_Bg = '#1EBCB1'
		let status_on_Color = Colors.white

		let status_off_Bg = Colors.white
		let status_off_Color = Colors.themeBG

		let speed = this.state.speed

		return(
			<View style={{alignItems:'center',width:'100%'}}>
				<TouchableOpacity
					activeOpacity={0.7}
					style={[styles.ctrTouch, {borderTopLeftRadius:8,borderTopRightRadius:8,backgroundColor:speed===3?status_on_Bg:status_off_Bg }]}
					onPress={()=>{
						this.controlDevice({speed:3},()=>{
							this.setState({ speed: 3 })
						})
					}}
				>
					<Text style={[styles.ctrText,{color:speed==3?status_on_Color:status_off_Color }]}>高速</Text>
				</TouchableOpacity>
				<View style={styles.split}/>
				<TouchableOpacity
					activeOpacity={0.7}
					style={[styles.ctrTouch,{backgroundColor:speed===2?status_on_Bg:status_off_Bg} ]}
					onPress={()=>{
						this.controlDevice({speed:2},()=>{
							this.setState({ speed: 2 })
						})
					}}
				>
					<Text style={[styles.ctrText,{color:speed==2?status_on_Color:status_off_Color }]}>中速</Text>
				</TouchableOpacity>
				<View style={styles.split}/>
				<TouchableOpacity
					activeOpacity={0.7}
					style={[styles.ctrTouch,{borderBottomLeftRadius:8,borderBottomRightRadius:8,backgroundColor:speed===1?status_on_Bg:status_off_Bg} ]}
					onPress={()=>{
						this.controlDevice({speed:1},()=>{
							this.setState({ speed: 1 })
						})
					}}
				>
					<Text style={[styles.ctrText,{color:speed==1?status_on_Color:status_off_Color }]}>低速</Text>
				</TouchableOpacity>
				{/* <View style={styles.split}/>
				<TouchableOpacity
					activeOpacity={0.7}
					style={[styles.ctrTouch, {borderBottomLeftRadius:8,borderBottomRightRadius:8,backgroundColor:speed===0?status_on_Bg:status_off_Bg }]}
					onPress={()=>{
						this.controlDevice({speed:0},()=>{
							this.setState({ speed: 0 })
						})
					}}
				>
					<Text style={[styles.ctrText,{color:speed==0?status_on_Color:status_off_Color }]}>自动</Text>
				</TouchableOpacity> */}
			</View>
		)
	}

	getCurrentTemp(){
		return(
			<View style={{marginTop:60,justifyContent:'center',alignItems:'center',flexDirection: 'row'}}>
				<Text style={{color:Colors.white}}>室温</Text>
				<Text style={{fontSize:30,color:Colors.white,fontWeight:'bold',marginLeft: 20}}>{this.state.temperature+' '}</Text>
			</View>
		)
	}

	
    showSwitchButton(){
		let switchBtn = require('../../../images/panel/ventilation_off.png')
		if(this.state.switch){
			switchBtn = require('../../../images/panel/ventilation_on.png')
		}
		return(
			<TouchableOpacity style={styles.switchBtn} onPress={()=>{
				let target = this.state.switch ? 0:1
				this.controlDevice({status: target},()=>{
					this.setState({
						switch: target
					})
				})
			}}>
				<Image style={{width:55,height:55,resizeMode:'contain'}} source={switchBtn}/>
			</TouchableOpacity>
		)
	}

	getMainPanelPage(){
		return(
			<View style={styles.main}>
				<View style={styles.contentWrapper}>
					{this.getCenterView()}
					{this.getCurrentTemp()}
                	{this.showSwitchButton()}
				</View>
			</View>
		)
	}

	// render() {
	// 	return (
	// 		<View style={styles.container}>
	// 			{this.getHeaderView()}
	// 			<ScrollView style={styles.container}>
	// 				{this.getMainPanelPage()}
	// 			</ScrollView >
	// 		</View>

	// 	)
	// }
}

const styles = StyleSheet.create({
	main: {
		flex: 1,
        backgroundColor: Colors.themeBG,
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
	contentWrapper:{
		flex: 1,
		width:'100%',
		justifyContent:'center',
		alignItems:'center'
	},
	ctrTouch:{
		backgroundColor:Colors.white,
		height:70,
		width:'30%',
		justifyContent:'center',
		alignItems:'center'
	},
	split:{
		height:3,
		width:'100%',
		backgroundColor: Colors.transparency
	},
	ctrText:{
		fontSize: 15,
		color:Colors.themeBG
	},
	switchBtn:{
		justifyContent:'center',
		alignItems:'center',
		marginTop:60,
		width:100,
		height:100
	}
});

export default Ventilation



/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter,
    ImageBackground
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { postJson } from '../../../util/ApiRequest';
import Panel from './Panel';
import Slider from "react-native-slider";
import { connect } from 'react-redux';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class LoraPannel extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		
		this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            speed: 0,
            brightness: 0,
            mode: 0,
            modeCount: 0
			
		}
	}

	componentDidMount() {
		this.requestDeviceStatus()
    }

    componentWillUnmount() {
        
    }
    
    // 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
                let statusString = data.result.deviceStatus
                let deviceStatus = null
                try {
                    deviceStatus = JSON.parse(statusString)
                    this.setState({
                        speed: deviceStatus.speed || this.state.speed,
                        brightness: deviceStatus.brightness || this.state.brightness,
                        mode: deviceStatus.mode || this.state.mode,
                        modeCount: deviceStatus.modeqty || this.state.modeCount
                    })
                } catch (error) {
                    
                }
				
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

    // 控制
    async controlDevice(){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.lora,
				params: {
                    id :this.deviceData.deviceId,
                    speed: this.state.speed,
                    bright: this.state.brightness,
                    mode: this.state.mode
                }
            });
            SpinnerManager.close()
			if (data.code == 0) {
				ToastManager.show('修改成功')
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    _onBrightnessChange = (value)=>{
        let target = Math.floor(value)
		this.setState({
			brightness: target
		})
    }
    
    _onSpeedChange = (value)=>{
        let target = Math.floor(value)
        this.setState({
			speed: target
		})
    }
    
    renderModelPannel(){
        const Colors = this.props.themeInfo.colors
        let btns = []
        for(let i=0; i<this.state.modeCount + 1; i++){
            let selectTouch = {backgroundColor: Colors.themeBg}
            let selectText = {color: Colors.themeTextLight}
            let tintColor = null
            if(this.state.mode == i){
                selectTouch = {backgroundColor: Colors.themeButton}
                selectText = {color: Colors.white}
                tintColor = {tintColor: Colors.white}
            }

            let btnText = <Text style={[styles.btnText, selectText]}>{i}</Text>
            if(i == 0){
                btnText = <Image style={[styles.repeatImg,tintColor]} source={require('../../../images/panel/repeat.png')}/>
            }

            let btn = (
                <View key={i} style={styles.btnWrapper}>
                    <TouchableOpacity style={[styles.btnTouch, selectTouch]} onPress={()=>{
                        this.setState({ mode: i })
                    }}>
                        {btnText}
                    </TouchableOpacity>
                </View>
            )
            btns.push(btn)
        }
        return (
            <View style={styles.modelWrapper}>
                <Text style={[styles.modelTitle,{color: Colors.themeText}]}>选择效果</Text>
				<ScrollView style={{marginTop: 10,paddingHorizontal: 16}}>
                    <View style={{flexDirection:'row', flexWrap:'wrap' }}>
                        {btns}
                    </View>
                </ScrollView>
			</View>
        )
    }

    renderSlider(){
        const Colors = this.props.themeInfo.colors
        return (
            <View style={styles.sliderWrapper}>
                <View style={styles.titleWrapper}>
                    <Text style={[styles.modelTitle,{color: Colors.themeText}]}>亮度</Text>
                    <Text style={styles.value}>{this.state.brightness}</Text>
                </View>
                <View style={styles.sliderContain}>
                    <Text style={styles.numberText}>0</Text>
                    <Slider
                        style={styles.slider}
                        step={1}
					    minimumValue={0}
					    maximumValue={8}
          			    value={this.state.brightness}
					    onValueChange={this._onBrightnessChange}
					    minimumTrackTintColor='#2650D7'
					    maximumTrackTintColor='#E5E5E5' 
					    thumbTintColor='#2650D7'
					    trackStyle={{height:8,borderRadius: 4}}
					    thumbStyle={{width:26,height:26,borderRadius:13}}
        		    />
                    <Text style={styles.numberText}>  8</Text>
                </View>
                
                <View style={styles.titleWrapper}>
                    <Text style={[styles.modelTitle,{color: Colors.themeText}]}>速度</Text>
                    <Text style={styles.value}>{this.state.speed}</Text>
                </View>
                <View style={styles.sliderContain}>
                    <Text style={styles.numberText}>4</Text>
                    <Slider
					    style={styles.slider}
					    minimumValue={4}
					    maximumValue={100}
          			    value={this.state.speed}
					    onValueChange={this._onSpeedChange}
					    minimumTrackTintColor='#2650D7'
					    maximumTrackTintColor='#E5E5E5' 
					    thumbTintColor='#2650D7'
					    trackStyle={{height:8,borderRadius: 4}}
					    thumbStyle={{width:26,height:26,borderRadius:13}}
        		    />
                    <Text style={styles.numberText}>100</Text>
                </View>
            </View>
        )
    }

    renderButton(){
        const Colors = this.props.themeInfo.colors
        return(
            <View style={styles.bottomWrapper}>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.bottomBtn,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        this.controlDevice()
                    }}
                >
                    <Text style={styles.bottomText}>确定修改</Text>
                </TouchableOpacity>
            </View>
        )
    }

	getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

		return(
			<View style={[styles.main,{backgroundColor: Colors.themeBaseBg}]}>
				{this.renderModelPannel()}
				{this.renderSlider()}
                {this.renderButton()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
    },
    modelWrapper:{
        marginTop: 30,
        flex: 1
    },
    modelTitle:{
        fontSize: 16,
        fontWeight: 'bold',
        marginLeft: 20
    },
    titleWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20
    },
    btnWrapper:{
        width: (screenW-35) *0.2,
        height: (screenW-35) *0.2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnTouch:{
        width: '70%',
        height: '70%',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnText:{
        fontSize: 20,
    },
    sliderWrapper:{
        marginTop: 10
    },
    sliderContain:{
        flexDirection: 'row',
        paddingHorizontal: 16,
        alignItems: 'center',
        marginTop: 10
    },
    slider:{
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
    },
    numberText:{
        fontSize: 16,
        color: Colors.themeTextLightGray
    },
    repeatImg:{
        width:20,
        height:15,
        resizeMode:'contain'
    },
    value:{
        color: Colors.tabActiveColor,
        fontSize: 16,
        marginLeft: 30
    },
    bottomWrapper:{
        paddingHorizontal: 16,
        marginTop: 35,
        paddingBottom: BottomSafeMargin + 0.05*screenH
    },
    bottomBtn:{
        width: '100%',
        height: 44,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    bottomText:{
        fontSize: 16,
        color: Colors.tabActiveColor
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(LoraPannel)



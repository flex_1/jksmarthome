/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import { Colors, NetUrls,NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import Panel from './Panel';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import { connect } from 'react-redux';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;


class SmartVoiceBox extends Panel {

	constructor(props) {
		super(props);
        const { getParam } = props.navigation;
        
		this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            commonStatement: [],
			multiRemark: null,
            tip: '',
            roomIds: [],
            floor: '',
            floorText: ''
		}
	}

	componentDidMount() {
        super.componentDidMount()
        this.requestDeviceStatus()
    }

    componentWillUnmount() {
        super.componentWillUnmount()
    }


    // 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				this.setState({
                    commonStatement: data.result?.commonStatement || [],
                    multiRemark: data.result?.multiRemark,
                    tip: data.result?.code || '',
                    roomIds: data.result?.roomIds || [],
                    floor: data.result?.floor || '',
                    floorText: data.result?.floorText || ''
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

    renderMainImg(){
        const {multiRemark} = this.state

        return(
            <View style={styles.topWrapper}>
                <Image style={styles.mianImg} source={{uri: multiRemark}}/>
            </View>
        )
    }

    renderTips(){
        const {commonStatement} = this.state

        return(
            <View style={{marginTop: 12,flexDirection: 'row',flexWrap:'wrap'}}>
                {commonStatement.map((value, index)=>{
                    return(
                        <View key={index} style={styles.tipItem}>
                            <Text style={{fontSize:14, fontWeight:'400', color: '#1080FE'}}>{value}</Text>
                        </View>
                    )
                })}
            </View>
        )
    }

    renderBottomView(){
        const Colors = this.props.themeInfo.colors
        const {tip, roomIds, floor, floorText} = this.state
        const {navigate} = this.props.navigation

        return(
            <View style={styles.bottomWrapper}>
                <View style={[styles.tipsWrapper,{backgroundColor: Colors.themeBg}]}>
                    <Text style={{fontSize:14, fontWeight: '400', color: Colors.themeText}}>你可以对音箱说：{tip}</Text>
                    {this.renderTips()}
                </View>
                <TouchableOpacity activeOpacity={0.7} style={[styles.btnWrapper,{backgroundColor: Colors.themeBg}]} onPress={()=>{
                    navigate('SmartVoiceBoxLog',{
                        deviceId: this.deviceData?.deviceId,
                        boxIcon: this.state.multiRemark
                    })
                }}>
                    <Text style={{fontSize: 16, fontWeight: '400', flex:1,color: Colors.themeText}}>控制记录</Text>
                    <Image style={styles.arrow} source={require('../../../images/ProtocolControl/arrow.png')}/>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.7} style={[styles.btnWrapper,{backgroundColor: Colors.themeBg}]} onPress={()=>{
                    navigate('SmartVoiceBoxLocation',{
                        roomIds: JSON.parse(JSON.stringify(roomIds)),
                        floor: floor,
                        floorText: floorText,
                        deviceId: this.deviceData?.deviceId,
                        callBack: ()=>{
                            this.requestDeviceStatus()
                        }
                    })
                }}>
                    <Text style={{fontSize: 16, fontWeight: '400', flex:1,color: Colors.themeText}}>位置管理</Text>
                    <Image style={styles.arrow} source={require('../../../images/ProtocolControl/arrow.png')}/>
                </TouchableOpacity>
            </View>
        )
    }
    
    // 首页面
    getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

		return(
			<View style={[styles.main,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderMainImg()}
				{this.renderBottomView()}
			</View>
		)
    }
}

const styles = StyleSheet.create({
	main: {
		flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
    topWrapper: {
        flex: 1,
        justifyContent:'center',
        alignItems:'center'
    },
    mianImg:{
        width: screenW * 0.5,
        height: screenW * 0.5,
        resizeMode: 'contain'
    },
	bottomWrapper:{
        paddingBottom: BottomSafeMargin + screenH * 0.05,
		width:'100%',
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal: 16
	},
    tipsWrapper:{
        width: '100%',
        paddingLeft: 16,
        paddingRight: 8,
        paddingTop: 16,
        paddingBottom: 8,
        borderRadius: 8
    },
    tipItem:{
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 15,
        height: 32,
        borderRadius: 16,
        backgroundColor: '#EFF8FF',
        marginBottom: 12,
        marginRight: 8
    },
    arrow:{
        width: 16,
        height: 16
    },
    btnWrapper:{
        height:48,
        width:'100%',
        flexDirection: 'row',
        alignItems:'center',
        borderRadius: 8,
        paddingHorizontal: 16,
        marginTop: 12
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SmartVoiceBox)



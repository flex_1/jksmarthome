/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	Modal,
    SafeAreaView,
    Vibration
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import Panel from './Panel';
import { connect } from 'react-redux';
import { ColorsDark } from '../../../common/Themes';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class IPTVController extends Panel {

	constructor(props) {
		super(props);
		
		this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            iptvchannel: [],
            mute: 0,
            power: 0,
            channel: '',
            channelModalVisible: false
		}
	}

	componentDidMount() {
        this.requestDeviceStatus()
        
    }
    
    componentWillUnmount() {
        
    }

    // 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			this.setState({isLoading: false})
			if (data.code == 0) {
				if(data.result && data.result.deviceStatus){
                    let statusJSON = JSON.parse(data.result.deviceStatus)
                    this.setState({
                        mute: statusJSON.mute == '1',
                        power: statusJSON.power == '1',
                        channel: statusJSON.channel
                    })
				}
				this.setState({
                    iptvchannel: data.result?.iptvchannel
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({isLoading: false})
			ToastManager.show('网络错误')
		}
    }

    // 遥控器
    async controlDevice(params, callBack){
        this.vibrate()
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 按钮点击振动
    vibrate(){
        if(Platform.OS == 'android'){
            Vibration.vibrate([0,50], false)
        }
    }

    //获取上部分按钮
    renderTopBtns(){
        const Colors = this.props.themeInfo.colors
        const switchTintColor = this.state.power ? {} : {tintColor: Colors.themeTextLight}
        const voiceIcon = this.state.mute ? require('../../../images/panel/TV/tv_voice.png') 
        : require('../../../images/panel/TV/tv_voice_on.png')
        
        return(
            <View style={styles.topWrapper}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.topBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        let target = this.state.mute ? 0 : 1
                        this.controlDevice({mute: target}, ()=>{
                            this.setState({
                                mute: target
                            })
                        })
                    }}
                >
                    <Image style={styles.tvBtnImg} source={voiceIcon}/>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.topBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        let target = this.state.power ? 0 : 1
                        this.controlDevice({power: target}, ()=>{
                            this.setState({
                                power: target
                            })
                        })
                    }}
                >
                    <Image style={[styles.tvBtnImg,switchTintColor]} source={require('../../../images/panel/TV/tv_switch.png')}/>
                </TouchableOpacity>
            </View>
        )
    }
    
	getCircularBg(){
		if(!this.state.currentPress){
			return null
        }
        const Colors = this.props.themeInfo.colors
        let selectIcon = null
        // if(this.state.currentPress == 1){
        //     selectIcon = <Image source={require('../../../images/panel/TV/up_select.png')} style={styles.btnSelectIcon}/>
        // }else if(this.state.currentPress == 2){
        //     selectIcon = <Image source={require('../../../images/panel/TV/right_select.png')} style={styles.btnSelectIcon}/>
        // }else if(this.state.currentPress == 3){
        //     selectIcon = <Image source={require('../../../images/panel/TV/down_select.png')} style={styles.btnSelectIcon}/>
        // }else if(this.state.currentPress == 4){
        //     selectIcon = <Image source={require('../../../images/panel/TV/down_select.png')} style={styles.btnSelectIcon}/>
        // }
        
		let percent = '71.71067812%'
		let rotateZ = 90 * (this.state.currentPress-1) + 'deg'
		return(
			<View style={[styles.selectWrapper,{width:percent,transform:[{rotateZ:rotateZ}]}]}>
				<Image style={[styles.selectImg,{tintColor: Colors.borderLight}]} source={require('../../../images/panel/tv_volate_bg_icon.png')}/>
                {selectIcon}
			</View>
		)
	}

	getCircularButton(index,icon,btnStyle){
        const Colors = this.props.themeInfo.colors

		return(
			<TouchableOpacity style={{...styles.cirlBtn,...btnStyle}} 
				onPress={()=>{
					if(index == 1){
						//上
						this.controlDevice({channelAdjust:1})
					}
					else if(index == 2){
						//右
						this.controlDevice({volumeAdjust:1})
					}
					else if(index == 3){
						//下
						this.controlDevice({channelAdjust:-1})
					}
					else if(index == 4){
						//左
						this.controlDevice({volumeAdjust:-1})
					}
				}}
				onPressIn={()=>{
					this.setState({currentPress:index})
				}}
				onPressOut={()=>{
					this.setState({currentPress:null})
				}}
			>
				<Image source={icon} style={[styles.btnImg,{tintColor: Colors.themeTextLight}]}/>
			</TouchableOpacity>
		)
    }

    // 中间控制按钮
	renderCenterView(){
        const Colors = this.props.themeInfo.colors

		return(
            <View style={styles.centerWrapper}>
                <View style={[styles.centerBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]}>
				    {this.getCircularBg()}
				    <TouchableOpacity 
                        activeOpacity={1} 
                        style={[styles.okBtnWrapper,{backgroundColor: Colors.themeBaseBg}]} 
                        onPress={()=>{
                            // this.controlDevice({number:24})
                        }}
                    >
                        {/* <Text style={{color:Colors.themeTextLight,fontSize:25,fontWeight:'bold'}}>OK</Text> */}
				    </TouchableOpacity>
				    {this.getCircularButton(1,require('../../../images/panel/TV/up.png'),{left:'25%',height:'25%',top:0,width:'50%'})}
				    {this.getCircularButton(2,require('../../../images/panel/TV/tv_add.png'),{right:0,height:'50%',top:'25%',width:'25%'})}
				    {this.getCircularButton(3,require('../../../images/panel/TV/down.png'),{left:'25%',height:'25%',bottom:0,width:'50%'})}
				    {this.getCircularButton(4,require('../../../images/panel/TV/tv_reduce.png'),{left:0,height:'50%',top:'25%',width:'25%'})}
                </View>
            </View>
		)
    }
    
    // 选择频道
    renderChannelBtn(){
        const Colors = this.props.themeInfo.colors
        let channelText = this.state.channel || '选择频道' 

        return(
            <TouchableOpacity
                activeOpacity={0.7}
                style={[styles.channelBtn,{borderColor: Colors.borderLight,backgroundColor: Colors.themeBg}]} 
                onPress={()=>{
                    this.setState({
                        channelModalVisible: true
                    })
                }}
            >   
                <View style={{flex: 1, justifyContent:'center', alignItems:'center'}}>
                    <Text style={{fontSize: 18, color: Colors.themeTextLight, fontWeight: '500'}}>{channelText}</Text>
                </View>
                <Image style={[styles.downArrow,{tintColor: Colors.themeTextLight}]} source={require('../../../images/down.png')}/>
            </TouchableOpacity>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        window.CustomPicker.init({
            pickerData: this.state.iptvchannel || [],
            pickerTitleText: '请选择频道',
            selectedValue: [this.state.channel],
            onPickerConfirm: (data,index) => {
                this.setState({
                    channelModalVisible: false,
                    channel: data[0]
                },()=>{
                    this.controlDevice({channel: data[0]})
                })
            },
            onPickerCancel: data => {
                this.setState({
                    brandModelVisible: false
                }, () => {
                    window.CustomPicker.hide()
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

    // 频道弹框
    renderChannelModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.channelModalVisible}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={styles.modalTouchable} 
                    onPress={() => {
                        this.setState({
                            channelModalVisible: false
                        }, () => {
                            window.CustomPicker.hide()
                        })
                    }}
                >
                </TouchableOpacity>
            </Modal>
        )
    }

	getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

		return(
			<View style={[styles.main,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderTopBtns()}
                {this.renderCenterView()}
                {this.renderChannelBtn()}
                {this.renderChannelModal()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
		flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
	cirlBtn:{
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		zIndex: 1000,
    },
    topWrapper:{
        width:'100%',
        paddingHorizontal:30,
        marginTop: 20,
        alignItems:'center',
        flexDirection:'row',
        justifyContent:'space-between'
    },
    centerWrapper:{
        width: '100%',
        alignItems:'center',
        justifyContent:'center',
        marginTop: screenH * 0.04,
	},
	centerBtn:{
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: screenW*0.3,
		width: screenW*0.6,
        height: screenW*0.6,
        borderWidth: 0.6,
	},
	okBtnWrapper:{
		position: 'absolute',
		alignItems: 'center',
		justifyContent: 'center',
		width: '50%',
		height: '50%',
		borderRadius: screenW*0.15
    },
    selectWrapper:{
        position:'absolute',
        alignItems:'center',
        height:'100%',
        backgroundColor:'transparent',
    },
    selectImg:{
        width:'100%',
        height:'50%',
        resizeMode:'contain',
        marginTop:-2
    },
    btnImg:{
        position: 'absolute',
        width:24,
        height:24,
        resizeMode:'contain'
    },
    btnSelectIcon:{
        position: 'absolute',
        top:'8%', 
        width:24,
        height:24,
        resizeMode:'contain'
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: 'rgba(0,0,0,0.5)' 
    },
    tvBtnImg:{
        width: 28,
        height: 28,
        resizeMode: 'contain'
    },
    topBtn:{
        width:64,
        height:64,
        borderRadius:32,
        justifyContent:'center',
        alignItems: 'center',
        borderWidth: 0.5
    },
    channelBtn:{
        marginHorizontal: 40,
        height: 54,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 27,
        flexDirection: 'row',
        paddingHorizontal: 20,
        marginTop: screenH * 0.08,
    },
    downArrow:{
        width: 18,
        height: 12,
        resizeMode: 'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(IPTVController) 



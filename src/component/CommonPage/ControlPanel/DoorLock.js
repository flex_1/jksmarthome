/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
    Modal
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel';
import { connect } from 'react-redux';
import DateUtil from '../../../util/DateUtil';
import {ColorsDark, ColorsLight} from '../../../common/Themes';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class DoorLock extends Panel {
	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData') || {}
        
        this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            passCount: 4,
            passModalVisible: false,
            pwdList: null,
            statusText: '-',
            battlevel: 0
        }
    }
    
	componentDidMount() {
        super.componentDidMount()

        this.requestDeviceStatus()
	}

	componentWillUnmount(){
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        if(data.isRecovery == 1){
            this.requestDeviceStatus()
            return
        }
        
		this.setState({
            statusText: data.statusText || '-',
            battlevel: data.battlevel || 0
        })
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
                let statusJson = JSON.parse(data.result.deviceStatus)

				this.setState({
                    pwdList: (data.result && data.result.pwdList) || [],
                    statusText: statusJson.statusText,
                    battlevel: statusJson.battlevel
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }
    
    // 获取当前 状态
    async deletePassWord(id){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.changeDoorPwd,
				params: {
                    id: id
                }
            });
            SpinnerManager.close()
			if (data.code == 0) {
				this.requestDeviceStatus()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
	}
    
    // 灯 开关 控制
    async controlDevice(params, callBack){
        params = params || {}
        params.id = this.deviceData.deviceId

		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
            });
            SpinnerManager.close()
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误，请稍后重试')
		}
    }

    //弹出警告框
    showPassAlert(name, id){
        Alert.alert(
            '提示',
            '确定要删除密码: ' + name + '?',
            [
                { text: '取消', onPress: () => { }, style: 'cancel' },
                { text: '确定', onPress: () => { 
                    this.deletePassWord(id)
                }},
            ]
        )
    }

    renderDoorStatus(){
        const Colors = this.props.themeInfo.colors
        
        return(
            <View style={styles.topWrapper}>
                <View style={styles.topItem}>
                    <Image style={{width:18, height:18, resizeMode:'contain'}} source={require('../../../images/panel/door/door_status.png')}/>
                    <Text style={{fontSize: 16, color: Colors.themeTextLight, marginLeft: 8}}>门状态:</Text>
                    <Text style={{fontSize: 18, fontWeight:'bold', marginLeft: 5,color: Colors.themeText}}>{this.state.statusText}</Text>
                </View>
                <View style={styles.topItem}>
                    <Image style={{width:18, height:18, resizeMode:'contain'}} source={require('../../../images/panel/door/battery.png')}/>
                    <Text style={{fontSize: 16, marginLeft: 8,color: Colors.themeTextLight}}>电池状态:</Text>
                    <Text style={{fontSize: 18, fontWeight:'bold', marginLeft: 5,color: Colors.themeText}}>{this.state.battlevel + '%'}</Text>
                </View>
            </View>
        )
    }

    renderSwitch(){
        const Colors = this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}
        const centerBtnColors = this.props.themeInfo.isDark ? ColorsLight : ColorsDark

        return(
            <View style={styles.centerWrapper}>
                <TouchableOpacity
                    activeOpacity={0.5} 
                    style={[styles.btn,{backgroundColor:Colors.themeBg,borderColor: Colors.borderLight}]} 
                    onPress={()=>{
                        // 关锁
                        this.controlDevice({type:1, value:0})
                    }}
                >
                    <Text style={[styles.btnText,{color: Colors.themeText}]}>关锁</Text>
                    <Image style={[styles.btnImg,tintColor]} source={require('../../../images/panel/door/shutdown.png')}/>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.5} 
                    style={[styles.btn,{backgroundColor:centerBtnColors.themeBg}]} 
                    onPress={()=>{
                        // 锁常开
                        this.controlDevice({type:1, value:2})
                    }}
                >
                    <Text style={[styles.btnText,{color: centerBtnColors.themeText}]}>锁常开</Text>
                    <Image style={styles.btnImg} source={require('../../../images/panel/door/usual_open.png')}/>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.5} 
                    style={[styles.btn,{backgroundColor:Colors.themeBg,borderColor: Colors.borderLight}]} 
                    onPress={()=>{
                        // 开锁
                        this.controlDevice({type:1, value:1})
                    }}
                >
                    <Text style={[styles.btnText,{color: Colors.themeText}]}>开锁</Text>
                    <Image style={[styles.btnImg,tintColor]} source={require('../../../images/panel/door/open.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    renderFunctionList(){
        const Colors = this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}
        let passCount = (this.state.pwdList && this.state.pwdList.length) || 0

        return(
            <View style={styles.bottomWrapper}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.cardBtn,{backgroundColor:Colors.themeBg,borderColor: Colors.borderLight}]}
                    onPress={()=>{
                        this.controlDevice({type:3},()=>{ 
                            ToastManager.show('命令已发送，请及时刷卡!')
                        })
                    }}
                >   
                    <Image style={[styles.funcBtnImg,tintColor]} source={require('../../../images/panel/door/card.png')}/>
                    <Text style={[styles.btnText,{color: Colors.themeTextLight}]}>刷卡</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={[styles.passWordBtn,{backgroundColor:Colors.themeBg,borderColor: Colors.borderLight}]}
                    onPress={()=>{
                        this.setState({passModalVisible: true})
                    }}
                >
                    <Image style={[styles.funcBtnImg,tintColor]} source={require('../../../images/panel/door/pass.png')}/>
                    <Text style={[styles.btnText,{color: Colors.themeTextLight}]}>密码管理（{passCount}个）</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderPassList(){
        if(!this.state.pwdList || !this.state.pwdList.length){
            return (
                <View style={{marginTop:50,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontSize: 16,color: Colors.themeTextLight}}>暂无密码</Text>
                </View>
            )
        }
        
        let list = this.state.pwdList.map((value, index)=>{
            let timeString = DateUtil(value.createTime)

            return(
                <View style={{paddingLeft: 20,flexDirection:'row',paddingVertical: 10,alignItems:'center'}} key={'pass_list_'+index}>
                    <Text style={{fontSize: 20, color: Colors.themeText}}>{index + 1}</Text>
                    <View style={{flex: 1,marginLeft: 16}}>
                        <Text style={{color: Colors.themeTextBlack,fontSize: 15}}>{value.name}</Text>
                        <Text style={{marginTop: 5,color: Colors.themeTextLightGray,fontSize: 13}}>{timeString}</Text>
                    </View>
                    <TouchableOpacity style={{paddingHorizontal: 30}} onPress={()=>{
                        this.showPassAlert(value.name, value.id)
                    }}>
                        <Image style={styles.deleteImg} source={require('../../../images/delete.png')}/>
                    </TouchableOpacity>
                </View>
            )
        })
        
        return list
    }

    renderPassListView(){
        return(
            <View style={{flex: 1}}>
                <View style={styles.titleWrapper}>
                    <Text style={styles.passTitle}>密码管理</Text>
                    <TouchableOpacity style={styles.topCloseTouch} onPress={()=>{
                        this.setState({passModalVisible: false})
                    }}>
                        <Image style={styles.closeImg} source={require('../../../images/close_black.png')}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.topAddTouch} onPress={()=>{
                        this.setState({passModalVisible: false})
                        this.props.navigation.navigate('DoorLockAddPassWord',{
                            deviceId: this.deviceData.deviceId,
                            callBack: ()=>{
                                this.requestDeviceStatus()
                                this.setState({passModalVisible: true})
                            }
                        })
                    }}>
                        <Image style={styles.addImg} source={require('../../../images/add.png')}/>
                    </TouchableOpacity>
                </View>
                <ScrollView>
                    {this.renderPassList()}
                </ScrollView>
            </View>
        )
    }

    // 密码弹框
    renderBtnsModel(){
        return(
            <Modal 
                transparent={true}
                visible={this.state.passModalVisible}
                animationType={'fade'}
            >
                <TouchableOpacity
                    activeOpacity={1}
                    style={{flex: 4,backgroundColor:Colors.translucent}}
                    onPress={()=>{
                        this.setState({passModalVisible: false})
                    }}
                />
                <View style={{backgroundColor: 'white',flex: 6,paddingBottom: BottomSafeMargin}}>
                    {this.renderPassListView()}
                </View>
            </Modal>
        )
    }

    getMainPanelPage(){
        return(
            <View style={{flex: 1,paddingBottom: BottomSafeMargin}}>
                {this.renderDoorStatus()}
                {this.renderSwitch()}
                {this.renderFunctionList()}
                {this.renderBtnsModel()}
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors
        
		return (
			<View style={[styles.main,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getHeaderView()}
				{this.getMainPanelPage()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight,
    },
    btn:{
        width: '100%',
        flexDirection: 'row',
        height: screenH * 0.1,
        borderRadius: 5,
        justifyContent:'space-between',
        alignItems:'center',
        borderWidth: 0.5,
        marginTop: 20,
        paddingHorizontal: '15%'
    },
    btnText:{
        fontSize: 16,
        fontWeight: 'bold',
        marginLeft: 5
    },
    btnImg:{
        height: screenH * 0.1 * 0.8 , 
        resizeMode:'contain',
        width: screenH * 0.1 * 0.9
    },
    topWrapper:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems: 'center',
        marginTop: screenH * 0.05,
        justifyContent: 'space-between',
        paddingHorizontal: 30
    },
    topItem:{
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'center'
    },
    centerWrapper:{
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal: 20,
        flex: 1
    },
    bottomWrapper:{
        paddingHorizontal: 30,
        paddingBottom: screenH * 0.12,
        flexDirection: 'row'
    },
    functionBtnWrapper:{
        marginTop: 30,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'space-between'
    },
    cardBtn:{
        flex: 4,  
        height: 50,
        borderRadius: 5,
        borderWidth: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    passWordBtn:{
        marginLeft: 20,
        flex: 6,
        height: 50,
        borderRadius: 5,
        borderWidth: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    funcBtnImg:{
        width:18,
        height:18,
        resizeMode:'contain'
    },
    titleWrapper:{
        height: 50,
        justifyContent:'center',
        alignItems:'center'
    },
    passTitle:{
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.themeTextBlack
    },
    topCloseTouch:{
        paddingHorizontal: 20, 
        height:'100%',
        position:'absolute',
        left:0,
        top: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    topAddTouch:{
        paddingHorizontal: 20, 
        height:'100%',
        position:'absolute',
        right:0,
        top: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    closeImg:{
        width:25,
        height:25,
        tintColor: 'black',
        resizeMode:'contain'
    },
    addImg:{
        width:30,
        height:30,
        resizeMode:'contain'
    },
    deleteImg:{
        width:16,
        height:16,
        resizeMode:'contain',
        tintColor: Colors.themBgRed
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(DoorLock)



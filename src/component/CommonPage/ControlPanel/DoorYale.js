/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
    Modal
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel';
import { connect } from 'react-redux';
import DateUtil from '../../../util/DateUtil';
import {ColorsDark, ColorsLight} from '../../../common/Themes';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class DoorYale extends Panel {
	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData') || {}
        
        this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            status: 0,
            warningText: ''
        }
    }
    
	componentDidMount() {
        super.componentDidMount()

        this.requestDeviceStatus()
	}

	componentWillUnmount(){
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        if(data.isRecovery == 1){
            this.requestDeviceStatus()
            return
        }
        
		this.setState({
            status: data.status,
            warningText: data.AlarmDesc || ''
        })
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
                let statusJson = JSON.parse(data.result.deviceStatus)

				this.setState({
                    status: statusJson.status,
                    warningText: statusJson.AlarmDesc
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }
    
    // 获取当前 状态
    async deletePassWord(id){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.changeDoorPwd,
				params: {
                    id: id
                }
            });
            SpinnerManager.close()
			if (data.code == 0) {
				this.requestDeviceStatus()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
	}
    
    // 灯 开关 控制
    async controlDevice(params, callBack){
        params = params || {}
        params.id = this.deviceData.deviceId

		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
            });
            SpinnerManager.close()
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误，请稍后重试')
		}
    }

    //弹出警告框
    showPassAlert(name, id){
        Alert.alert(
            '提示',
            '确定要删除密码: ' + name + '?',
            [
                { text: '取消', onPress: () => { }, style: 'cancel' },
                { text: '确定', onPress: () => { 
                    this.deletePassWord(id)
                }},
            ]
        )
    }

    renderDoorStatus(){
        const Colors = this.props.themeInfo.colors
        const statusText = this.state.status ? '开' : '关'
        
        return(
            <View style={styles.topWrapper}>
                <View style={styles.topItem}>
                    <Image style={{width:18, height:18, resizeMode:'contain'}} source={require('../../../images/panel/door/door_status.png')}/>
                    <Text style={{fontSize: 15, color: Colors.themeTextLight, marginLeft: 8}}>门状态:</Text>
                    <Text style={{fontSize: 18, fontWeight:'bold', marginLeft: 5,color: Colors.themeText}}>{statusText}</Text>
                </View>
                <View style={styles.topItem}>
                    {/* <Image style={{width:18, height:18, resizeMode:'contain'}} source={require('../../../images/panel/door/battery.png')}/> */}
                    <Text style={{fontSize: 15, marginLeft: 8,color: Colors.themeTextLight}}>报警信息:</Text>
                    <Text style={{fontSize: 18, fontWeight:'bold', marginLeft: 5,color: Colors.themeText}}>{this.state.warningText}</Text>
                </View>
            </View>
        )
    }

    renderSwitch(){
        const Colors = this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}

        return(
            <View style={styles.centerWrapper}>
                <TouchableOpacity
                    activeOpacity={0.5} 
                    style={[styles.btn,{backgroundColor:Colors.themeBg,borderColor: Colors.borderLight}]} 
                    onPress={()=>{
                        // 关锁
                        this.controlDevice({status:0})
                    }}
                >
                    <Text style={[styles.btnText,{color: Colors.themeText}]}>关锁</Text>
                    <Image style={[styles.btnImg,tintColor]} source={require('../../../images/panel/door/shutdown.png')}/>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.5} 
                    style={[styles.btn,{marginTop: 30,backgroundColor:Colors.themeBg,borderColor: Colors.borderLight}]} 
                    onPress={()=>{
                        // 开锁
                        this.controlDevice({status:1})
                    }}
                >
                    <Text style={[styles.btnText,{color: Colors.themeText}]}>开锁</Text>
                    <Image style={[styles.btnImg,tintColor]} source={require('../../../images/panel/door/open.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    getMainPanelPage(){
        return(
            <View style={{flex: 1}}>
                {this.renderDoorStatus()}
                {this.renderSwitch()}
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors
        
		return (
			<View style={[styles.main,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getHeaderView()}
				{this.getMainPanelPage()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
    },
    btn:{
        width: '100%',
        flexDirection: 'row',
        height: screenH * 0.12,
        borderRadius: 5,
        justifyContent:'space-between',
        alignItems:'center',
        borderWidth: 0.5,
        paddingHorizontal: '15%'
    },
    btnText:{
        fontSize: 16,
        fontWeight: 'bold',
        marginLeft: 5
    },
    btnImg:{
        height: screenH * 0.12 * 0.8 , 
        resizeMode:'contain',
        width: screenH * 0.12 * 0.9
    },
    topWrapper:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems: 'center',
        marginTop: screenH * 0.05,
        justifyContent: 'space-between',
        paddingHorizontal: 30
    },
    topItem:{
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'center'
    },
    centerWrapper:{
        flex: 1,
        alignItems:'center',
        justifyContent: 'center',
        paddingHorizontal: 20,
        paddingBottom: BottomSafeMargin + screenH*0.15,
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(DoorYale)



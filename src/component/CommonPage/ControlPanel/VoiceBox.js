/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	Dimensions
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel'; 
import Slider from "react-native-slider";

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class VoiceBox extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData')

        this.state = {
            ...this.state,
			status: 0,
			isLoading: true,
            musicName: '',
            volume: 0,
            musicDeviceId: null,
            musicDeviceName: null
        }
    }
    
	componentDidMount() {
        super.componentDidMount()
        this.requestDeviceStatus()
	}

	componentWillUnmount(){
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;
        
		this.setState({
            status: data.status == 1,
            musicDeviceName: data.name,
            volume: parseInt(data.volume)
        })
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			this.setState({isLoading: false})
			if (data.code == 0) {
				let statusJSON = {}
				if(data.result && data.result.deviceStatus){
					statusJSON = JSON.parse(data.result.deviceStatus)
				}
				this.setState({
                    isCollect: data.result && data.result.isCollect,
					status: statusJSON.status,
                    musicName: statusJSON.name,
                    volume: parseInt(statusJSON.volume),
                    musicDeviceId: data.result.musicDeviceId,
                    musicDeviceName: data.result.musicDeviceName
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({isLoading: false})
			ToastManager.show('网络错误')
		}
	}
    
    // 控制
    async controlDevice(params,callBack,failCallback){
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				failCallback && failCallback()
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误，请稍后重试')
		}
    }

    // 音量点击
	_onchangeStep(value){
		let target = Math.floor(value)
		if(target <= 0) target = 0
		if(target >= 15) target = 15
		this.controlDevice({volume: target},()=>{
			this.setState({
				volume: target
			})
		})
	}

	// 音量拖动
    _onChange =(value)=>{
		let target = Math.floor(value)
		if(target <= 0) target = 0
		if(target >= 15) target = 15
		this.setState({
			volume: target
		})
	}
	
	_complete =(value)=>{
		let target = Math.floor(value)
		if(target <= 0) target = 0
		if(target >= 15) target = 15
		this.controlDevice({volume: target},()=>{
			this.setState({
				volume: target
			})
		})
	}

	// 更改状态
	changeStatus(target){
		// if(this.state.isLoading){
		// 	ToastManager.show('正在请求数据，请稍后再试')
		// 	return
		// }
		// let oldStatus = this.state.status
		// this.setState({
		// 	status: target
		// })
		this.controlDevice({status: target},
			()=>{
				// 更改状态成功的回调
				this.setState({
					isLoading: false,
					status: target
				})      
			},
			()=>{
				// 更改状态失败的回调
				this.setState({
					// status: oldStatus,
					isLoading: false
				})
			},
		)
    }

    getMusicTitle(){
        const {navigate} = this.props.navigation

        return(
            <View style={styles.topWrapper}>
				<Text numberOfLines={2} style={styles.musicName}>{this.state.musicName}</Text>
                <TouchableOpacity style={styles.mainMusicTouch} onPress={()=>{
                    navigate('Music',{
                        title: this.state.musicDeviceName,
                        deviceData: {deviceId: this.state.musicDeviceId},
                        speakerId: this.deviceData.deviceId,
                        statusCallBack: (status)=>{
                            this.setState({status: status})
                        }
                    })
                }}>
                    <Text style={{fontSize: 18,color: Colors.white,fontWeight:'500'}}>{this.state.musicDeviceName}</Text>
                    <Image style={{marginLeft: 5,width:7,height:18,resizeMode:'contain'}} source={require('../../../images/enter_white.png')}/>
                </TouchableOpacity>
			</View>
        )
    }
    
    showStaticButton(){
        let light_img = this.state.status ? require('../../../images/panel/music_on.png') : require('../../../images/panel/light_off.png')
        return(
            <View style={{flex: 1, justifyContent: 'center',alignItems: 'center'}}>
                <TouchableOpacity
                    activeOpacity={0.5}
                    style={styles.swicthBtn}
                    onPress={() => {
                        let target = this.state.status ? 0:1 
                        this.changeStatus(target)
                    }}
                >
                    <Image style={{width:'100%',height:'100%'}} source={light_img}/>
                </TouchableOpacity>
            </View>
        )
    }

    showVolume(){
        return(
            <View style={styles.centerContent}>
				<TouchableOpacity style={{paddingHorizontal:16}} activeOpacity={0.5} onPress={()=>{ 
					this._onchangeStep(this.state.volume - 1 ) 
				}}>
					<Image source={require('../../../images/panel/volute_mus_icon.png')} style={{width:22,height:22,resizeMode:'contain'}}/>
				</TouchableOpacity>
				<Slider
					style={{flex: 1,marginLeft:5,marginRight:5}}
					minimumValue={0}
					maximumValue={15}
					step={1}
          			value={this.state.volume}
					onValueChange={this._onChange}
					onSlidingComplete={this._complete}
					minimumTrackTintColor='#FCB447'
					maximumTrackTintColor='#fff'
					thumbTintColor='#fff'
					trackStyle={{height:8,borderRadius: 4}}
					thumbStyle={{width:26,height:26,borderRadius:13}}
        		/>
                <TouchableOpacity style={{paddingHorizontal:16}} activeOpacity={0.5} onPress={()=>{ 
					this._onchangeStep(this.state.volume + 1 ) 
				}}>
					<Image source={require('../../../images/panel/volute_add_icon.png')} style={{width:22,height:22,resizeMode:'contain'}}/>
				</TouchableOpacity>
            </View>
        )
    }

	getMainPanelPage(){
		return(
			<View style={styles.main}>
                {this.getMusicTitle()}
                {this.showStaticButton()}
                <View style={styles.bottomCtr}>
                    {this.showVolume()}
                </View>
			</View>
		)
	}

	render() {
		let bgColor = this.state.status ? '#5151e3' : '#16161E'

		return (
			<View style={[styles.container,{backgroundColor: bgColor}]}>
				{this.getHeaderView()}
				{this.getMainPanelPage()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight,
	},
	swicthBtn: {
        width: screenW*0.5,
        height:screenW*0.5,
        justifyContent:'center',
        alignItems:'center'
    },
    topWrapper:{
		justifyContent:'center',
		alignItems:'center',
        paddingHorizontal: 16,
        width: '100%',
        marginTop: screenH * 0.05
	},
    musicName:{
        fontSize:20,
        color:Colors.white,
        fontWeight:'bold',
        textAlign:'center'
    },
    bottomCtr:{
		paddingTop: 30,
        paddingBottom: BottomSafeMargin + 0.1*screenH,
        width:'100%',
        justifyContent:'center',
        alignItems:'center',
	},
    centerContent:{
		paddingHorizontal:5,
		width:'100%',
		flexDirection:'row',
		alignItems:'center',
		justifyContent:'center',
	},
    mainMusicTouch:{
        marginTop: 30,
        flexDirection: 'row',
        alignItems:'center',
        paddingHorizontal: 20,
        paddingVertical: 5
    }
});

export default VoiceBox



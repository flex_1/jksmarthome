/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Animated,
	Dimensions,
    Modal,
    Keyboard,
    Alert
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import { connect } from 'react-redux';
import PCtrPanel from '../../CommonPage/ProtocolControlPanel';
import Panel from './Panel';

const PCPanelStyleList = [  PCtrPanel.PCtrPanelCustom, PCtrPanel.PCtrPanel1, 
                            PCtrPanel.PCtrPanel2, PCtrPanel.PCtrPanel3,
                            PCtrPanel.PCtrPanel4]

class ProtocolController extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
        
        this.typeId = getParam('typeId')
        this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,

            currentId: 1,
            currentComponet: PCtrPanel.PCtrPanel1,
            buttonList: null,
            customButtonList: null
        }
    }
    
	componentDidMount() {
        this.requestDeviceStatus()
	}

	componentWillUnmount(){
        
    }

    async requestDeviceStatus(){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.instructionProtocolButton,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
            SpinnerManager.close()
			if (data.code == 0) {
                const styleIndex = data.result?.style
                if(styleIndex == null || styleIndex > 4){
                    ToastManager.show('样式加载出错。')
                }
				this.setState({
                    currentComponet: PCPanelStyleList[styleIndex],
                    buttonList: data.result?.buttonList || [],
                    customButtonList: data.result?.customButtonList || []
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    async requestControlBtn(btn){
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				params: {
                    deviceId: this.deviceData.deviceId,
                    btn: btn
                }
			});
			if (data.code == 0) {
				ToastManager.show(data.msg)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

    onBtnClick(index, isCustomBtn){
        let btnParams = {}
        if(isCustomBtn){
            btnParams = this.state.customButtonList[index]
        }else{
            btnParams = this.state.buttonList[index]
        }
        if(!btnParams){
            ToastManager.show('数据配置出错，请稍后再试.')
            return
        }
        let btn = btnParams['btn']
        this.requestControlBtn(btn)
    }

    renderMainController(){
        if(!this.state.buttonList){
            return null
        }
        const Panel = this.state.currentComponet
        if(!Panel){
            return null
        }
        return(
            <Panel 
                isSetting={false}
                buttonList={this.state.buttonList}
                customButtonList={this.state.customButtonList}
                btnClick={(index, isCustomBtn)=>{
                    this.onBtnClick(index, isCustomBtn)
                }}
            />
        )
    }

    getMainPanelPage(){
        const Colors = this.props.themeInfo.colors;

		return(
			<View style={[styles.wrapper,{backgroundColor: Colors.themeBg}]}>
				<View style={styles.main}>
                    {this.renderMainController()}
                </View>
			</View>
		)
	}

	// render() {
    //     const Colors = this.props.themeInfo.colors;

	// 	return (
	// 		<View style={[styles.wrapper,{backgroundColor: Colors.themeBaseBg}]}>
	// 			<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
    //                 {this.renderMainController()}
    //             </View>
	// 		</View>
	// 	)
	// }
}

const styles = StyleSheet.create({
	wrapper: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
    },
    main:{
        marginTop: 6,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    mianCtrWrapper:{
        marginTop: 15, 
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    selectWrapper:{
        flexDirection: 'row',
        marginTop: 20,
        flexWrap:'wrap',
        paddingLeft: 20,
        paddingRight: 10
    },
    item:{
        flexDirection: 'row',
        alignItems:'center',
        paddingHorizontal:5,
        paddingVertical: 6,
        marginRight: 8
    },
    selectIcon:{
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    name:{
        fontSize: 16,
        fontWeight: '400',
        marginLeft: 6,
    },
    modalWrapper:{
        position: 'absolute',
        width: '100%',
        height: '100%',
        left: 0,
        top: 0
    },
    topWrapper:{
        backgroundColor: Colors.translucent,
        flex: 3
    },
    bottomWrapper:{
        flex: 7,
        backgroundColor: Colors.white
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ProtocolController)



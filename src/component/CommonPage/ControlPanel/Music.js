/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 音乐
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	Animated,
	Easing
} from 'react-native';
import { Colors, NetUrls,NetParams } from '../../../common/Constants';
import Slider from "react-native-slider";
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class Music extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
        this.statusCallBack = getParam('statusCallBack')
        this.speakerId = getParam('speakerId')

		this.state = {
			...this.state,
			value: 50,
			play: 0,
			musicName: '',
			mode: 1,  // 0：顺序， 1：全部循环， 2：单曲， 3：随机
			source: 1, //1：本地 ， 2：蓝牙， 3：外部
            loudspeakers: null,   //喇叭

			rotateValue: new Animated.Value(0), //旋转角度的初始值
		}

		this.playerAnimated  = Animated.timing(this.state.rotateValue,{
			toValue: 1, // 最终值 为1，这里表示最大旋转 360度
			duration: 4000,
			easing: Easing.linear,
		})
	}

	componentDidMount() {
		super.componentDidMount()
		this.requestDeviceStatus()
	}

	componentWillUnmount() {
        this.animatedTimer && clearTimeout(this.animatedTimer)
        super.componentWillUnmount()
    }
    
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        let volume = this.state.value
		if(data.volume != null){
            volume = Math.floor(data.volume) / 10
            volume = volume * 10
        }
        let oldPlayStatus = this.state.play
        this.setState({
            play:  data.runningStatus == 1, //1播放  2暂停
            value: volume,
            musicName: data.name,
            mode: parseInt(data.mode),
            source: parseInt(data.source)
        },()=>{
            if(oldPlayStatus != this.state.play){
                this.playMusic()
            }
        })
    }

	rotating() {
		if(this.state.play){
			this.state.rotateValue.setValue(0);
        	this.playerAnimated.start(() => {
            	this.rotating()
        	})
		}
	};
	
	playMusic(){
		if(this.state.play){
			this.playerAnimated.start(() => {
				this.playerAnimated = Animated.timing(this.state.rotateValue, {
					toValue: 1, //角度从0变1
					duration: 4000, //从0到1的时间
					easing: Easing.linear, //线性变化，匀速旋转
				});
				this.rotating();
			});
		}else{
			this.state.rotateValue.stopAnimation((oneTimeRotate) => {
				//计算角度比例
				this.playerAnimated = Animated.timing(this.state.rotateValue, {
					toValue: 1,
					duration: (1-oneTimeRotate) * 4000,
					easing: Easing.linear,
				});
			});
		}
	}

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				let resJson = data.result && data.result.deviceStatus
				try {
					let res = JSON.parse(resJson)
					let volume = this.state.value
					if(res.volume != null){
						volume = Math.floor(res.volume) / 10
						volume = volume * 10
					}
					this.setState({
                        isCollect: data.result && data.result.isCollect,
						play: res.runningStatus == 1,  //1播放  2暂停
						value: volume,
						musicName: res.name,
						mode: parseInt(res.mode),
						source: parseInt(res.source),
                        loudspeakers: data.result.loudspeakers
					},()=>{
						this.playMusic()
					})
				} catch (error) {
					ToastManager.show('数据异常')
				}
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}
    
    // 音乐 控制
    async controlDevice(params,callBack){
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

    // 控制 喇叭
    async controlSpeaker(params,callBack){
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

	// 上一首 下一首
    _changeVal(val){
		this.controlDevice({number: val},()=>{
			ToastManager.show(val==2 ? '下一首':'上一首' )
		})
	}
	
	// 播放 暂停
	_play(){
		// status 1播放  0暂停
		let target = this.state.play == 1 ? 2:1
		this.controlDevice({runningStatus: target},()=>{
			this.setState({
				play: target == 1 ? 1:0
			},()=>{
				this.playMusic()
			})
		})	
	}

	// 音量点击
	_onchangeStep(value){
		let target = Math.floor(value)
		if(target <= 0) target = 0
		if(target >= 100) target = 100
		this.controlDevice({volume: target},()=>{
			this.setState({
				value: target
			})
		})
	}

	// 音量拖动
    _onChange =(value)=>{
		let target = Math.floor(value)
		if(target <= 0) target = 0
		if(target >= 100) target = 100
		this.setState({
			value: target
		})
	}
	
	_complete =(value)=>{
		let target = Math.floor(value)
		if(target <= 0) target = 0
		if(target >= 100) target = 100
		this.controlDevice({volume: target},()=>{
			this.setState({
				value: target
			})
		})
	}

	/**
	 * 中间内容展示
     * @returns {*}
     */
    getCenterView(){
		let musicName = this.state.musicName
		const spin = this.state.rotateValue.interpolate({
            inputRange: [0, 1],//输入值
            outputRange: ['0deg', '360deg'] //输出值
		})
		  
		return(
			<View style={styles.topWrapper}>
				<Text numberOfLines={2} style={{fontSize:20,color:Colors.white,fontWeight:'bold',textAlign:'center'}}>{musicName}</Text>
				<Animated.Image style={{width:screenW/2,height:screenW/2,marginTop:30,transform:[{"rotate": spin }]}} source={require('../../../images/panel/music_top_icon.png')}/>
			</View>
		)
	}

    getVoiceBox(){
        if(!this.state.loudspeakers){
            return null
        }

        let speaker = this.state.loudspeakers.map((value, index)=>{
            let boxStatus = value.status == 1 ? styles.boxStatusOn : styles.boxStatusOff
            let boxImg = value.status == 1 ? require('../../../images/panel/voiceBoxOn.png') : require('../../../images/panel/voiceBoxOff.png')
            let boxTextColor = value.status ? '#5151e3' : Colors.white
            
            return(
                <TouchableOpacity
                    key={'index_'+index}
                    activeOpacity={0.7} 
                    style={[styles.boxWrapper,boxStatus,{width: screenW * 0.2}]} 
                    onPress={()=>{
                        let target = value.status == 1 ? 0:1
                        this.controlSpeaker({status: target, id: value.deviceId},()=>{
                            this.state.loudspeakers[index].status = target
                            this.setState({
                                ...this.state
                            })
                            if(this.speakerId == value.deviceId){
                                this.statusCallBack && this.statusCallBack(target)
                            }
                        })
                    }}
                >
                    <Image style={styles.boxImg} source={boxImg}/>
                    <Text numberOfLines={1} style={{fontSize: 14,marginLeft: 5,color: boxTextColor}}>{value.name}</Text>
                </TouchableOpacity>
            )
        })

        return(
            <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false} 
                alwaysBounceVertical={false} 
                style={{marginBottom: 25}}
            >
                <View style={{flexDirection: 'row', paddingHorizontal: 16}}>
                    {speaker}
                </View>
            </ScrollView>
        )
    }

    /**
     * 音量
     * **/
    showVolume(){
        return(
            <View style={styles.centerContent}>
				<TouchableOpacity style={{paddingHorizontal:16}} activeOpacity={0.5} onPress={()=>{ 
					this._onchangeStep(this.state.value -10 ) 
				}}>
					<Image source={require('../../../images/panel/volute_mus_icon.png')} style={{width:22,height:22,resizeMode:'contain'}}/>
				</TouchableOpacity>
				<Slider
					style={{flex: 1,marginLeft:5,marginRight:5}}
					minimumValue={0}
					maximumValue={100}
					step={10}
          			value={this.state.value}
					onValueChange={this._onChange}
					onSlidingComplete={this._complete}
					minimumTrackTintColor='#FCB447'
					maximumTrackTintColor='#fff'
					thumbTintColor='#fff'
					trackStyle={{height:8,borderRadius: 4}}
					thumbStyle={{width:26,height:26,borderRadius:13}}
        		/>
                <TouchableOpacity style={{paddingHorizontal:16}} activeOpacity={0.5} onPress={()=>{ 
					this._onchangeStep(this.state.value +10 ) 
				}}>
					<Image source={require('../../../images/panel/volute_add_icon.png')} style={{width:22,height:22,resizeMode:'contain'}}/>
				</TouchableOpacity>
            </View>
        )
    }

	/**
	 * 播放按钮
	 * **/
    showPlay(){
		let palyIcon = this.state.play ? require('../../../images/panel/music_play_icon.png') : require('../../../images/panel/music_pouse_icon.png')
		let cycleIcon = require('../../../images/panel/music_all_circle.png')
		let cycleText = '循环'
		if(this.state.mode == 0){
			cycleIcon = require('../../../images/panel/music_order_circle.png')
			cycleText = '顺序'
		}
		else if(this.state.mode == 1){
			cycleIcon = require('../../../images/panel/music_all_circle.png')
			cycleText = '循环'
		}
		else if(this.state.mode == 2){
			cycleIcon = require('../../../images/panel/music_single.png')
			cycleText = '单曲'
		}
		else if(this.state.mode == 3){
			cycleIcon = require('../../../images/panel/music_random.png')
			cycleText = '随机'
		}
		

		let source = '本地'
		if(this.state.source == 1){
			source = '本地'
		}
		else if(this.state.source == 2){
			source = '蓝牙'
		}
		// else if(this.state.source == 3){
		// 	source = '外部'
		// }

		return(
			<View style={styles.bottomContent}>
				<View style={styles.bottomTouchWrapper}>
					<TouchableOpacity style={styles.bottomTouch} activeOpacity={0.5} onPress={()=>{ 
						let target = (this.state.mode + 1) % 4
						
						this.controlDevice({mode: target},()=>{
							this.setState({
								mode: target
							})
						})
					}}>
						<Image source={cycleIcon} style={{width:26,height:20,resizeMode:'contain',marginTop:10}}/>
						<Text style={{marginTop:5,fontSize:11,color:Colors.white}}>{cycleText}</Text>
					</TouchableOpacity>
				</View>
				<View style={styles.bottomTouchWrapper}>
					<TouchableOpacity style={styles.bottomTouch}  activeOpacity={0.5} onPress={()=>{ this._changeVal(1) }}>
						<Image source={require('../../../images/panel/music_play_left_icon.png')} style={{width:15,height:18,resizeMode:'contain'}}/>
					</TouchableOpacity>
				</View>
				<View style={[styles.bottomTouchWrapper,{flex:2}]}>
					<TouchableOpacity style={styles.bottomCenterTouch} activeOpacity={0.5} onPress={()=>{this._play()}}>
						<Image source={palyIcon} style={{width:20,height:27,resizeMode:'contain'}}/>
					</TouchableOpacity>
				</View>
				<View style={styles.bottomTouchWrapper}>
					<TouchableOpacity style={styles.bottomTouch} activeOpacity={0.5} onPress={()=>{ this._changeVal(2) }}>
						<Image source={require('../../../images/panel/music_play_right_icon.png')} style={{width:15,height:18,resizeMode:'contain'}}/>
					</TouchableOpacity>
				</View>
				<View style={styles.bottomTouchWrapper}>
					<TouchableOpacity style={styles.bottomTouch} activeOpacity={0.5} onPress={()=>{ 
						let target = (this.state.source % 2) + 1
						this.controlDevice({source: target},()=>{
							this.setState({
								source: target
							})
						})
					}}>
						<Text style={{fontSize:16,color:Colors.white}}>{source}</Text>
					</TouchableOpacity>
				</View>
			</View>
		)
	}

	getMainPanelPage(){
		return(
			<View style={styles.main}>
                {this.getCenterView()}
				<View style={styles.bottomCtr}>
                    {this.getVoiceBox()}
					{this.showVolume()}
                	{this.showPlay()}
				</View>
			</View>
		)
	}

	// render() {
	// 	return (
	// 		<View style={styles.container}>
	// 			{this.getHeaderView()}
	// 			<ScrollView style={styles.container}>
	// 				{this.getMainPanelPage()}
	// 			</ScrollView >
	// 		</View>
	// 	)
	// }
}

const styles = StyleSheet.create({
	main: {
		flex: 1,
		backgroundColor: '#5151e3',
		paddingTop: StatusBarHeight + NavigationBarHeight,
	},
	topWrapper:{
		justifyContent:'center',
		alignItems:'center',
        paddingHorizontal: 16,
        flex: 1
	},
	bottomCtr:{
		paddingTop: 30,
        paddingBottom: BottomSafeMargin + 0.1*screenH,
        width:'100%',
        justifyContent:'center',
        alignItems:'center',
	},
	centerContent:{
		paddingHorizontal:5,
		width:'100%',
		flexDirection:'row',
		alignItems:'center',
		justifyContent:'center',
	},
	bottomContent:{
		width:'100%',
		alignItems:'center',
		flexDirection:'row',
		paddingHorizontal: 10,
		height:100,
		marginTop:25,
	},
	bottomTouchWrapper:{
		flex:1,
		justifyContent:'center',
		alignItems:'center'
	},
	bottomTouch:{
		height:'100%',
		width:'100%',
		justifyContent:'center',
		alignItems:'center'
	},
	bottomCenterTouch:{
		width:80,
		height: 80,
		backgroundColor:Colors.white,
		borderRadius:40,
		justifyContent:'center',
		alignItems:'center'
	},
    boxImg:{
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    boxWrapper:{
        flexDirection: 'row', 
        alignItems:'center',
        height:36,
        paddingHorizontal: 5,
        borderRadius: 2,
        justifyContent: 'center',
        marginHorizontal: 6
    },
    boxStatusOff:{
        borderWidth: 1,
        borderColor: Colors.white
    },
    boxStatusOn:{
        backgroundColor:Colors.white,
    }
});

export default Music



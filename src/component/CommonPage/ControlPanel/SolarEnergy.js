/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter,
	Switch,
    StatusBar,
    Animated
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel';
import { connect } from 'react-redux';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class SolarEnergy extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData')
        this.animating = false
        this.animatedFun = null;

        
        this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,
            solarData: {}
        }
    }
    
	componentDidMount() {
        super.componentDidMount()
        this.requestDeviceStatus()
        //设置定时器 定时刷新
		this.interval = setInterval(()=>{
			if(this.state.status != 0){
                this.requestDeviceStatus()
            }
        },10*1000)
	}

	componentWillUnmount(){
        this.interval && clearInterval(this.interval)
        
        super.componentWillUnmount()
    }
    
	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			this.setState({isLoading: false})
			if (data.code == 0) {
				if(data.result && data.result.deviceStatus){
                    let statusJSON = JSON.parse(data.result.deviceStatus)
                    this.setState({
                        solarData: statusJSON
                    })
				}
				this.setState({
                    isCollect: data.result && data.result.isCollect
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({isLoading: false})
			ToastManager.show('网络错误')
		}
	}
    
    // 充电控制
    async controlDevice(params,callBack,failCallback){
        params = params || {}
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: {
                    ...params,
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				failCallback && failCallback()
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误，请稍后重试')
		}
    }

    renderItem(title, content){
        const Colors = this.props.themeInfo.colors
        content = content || '-'
        
        return(
            <View style={styles.item}>
                <Text style={[styles.titleText,{color: Colors.themeText}]}>{title} : </Text>
                <Text style={[styles.valueText,{color: Colors.themeText}]} numberOfLines={2}>{content} </Text>
            </View>
        )
    }
    
    renderCurrentData(){
        const Colors = this.props.themeInfo.colors
        const {wChgMode, wPvVolt, wrealBatCurr, wLoadVolt, wLoadCurr, wrealPower, wrealBatVolt,
            wLoadPower, wBatTemp, wInnerTemp, wBatCap, dwCO2, wFault} = this.state.solarData

        return(
            <View style={[styles.wrapper,{borderColor: Colors.borderLight}]}>
                <ScrollView style={styles.scrollWrapper} contentContainerStyle={styles.scrollContent}>
                    {this.renderItem('充电模式', wChgMode)}
                    {this.renderItem('输入电压', wPvVolt)}
                    {this.renderItem('实时电池电压', wrealBatVolt)}
                    {this.renderItem('实时充电电流', wrealBatCurr)}
                    {this.renderItem('实时 Load 电压', wLoadVolt)}
                    {this.renderItem('实时 Load 电流', wLoadCurr)}
                    {this.renderItem('实时充电功率', wrealPower)}
                    {this.renderItem('实时 Load 功率', wLoadPower)}
                    {this.renderItem('实时电池温度', wBatTemp)}
                    {this.renderItem('实时内部温度', wInnerTemp)}
                    {this.renderItem('电池电量', wBatCap)}
                    {this.renderItem('CO2 减排', dwCO2)}
                    {this.renderItem('故障信息详细', wFault)}
                </ScrollView>
            </View>
        )
    }

    showStaticButton(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={styles.bottomBtnWrapper}>
                <TouchableOpacity
                    activeOpacity={0.5}
                    style={[styles.btn,{backgroundColor: Colors.red}]}
                    onPress={() => {
                        this.controlDevice({status: 0},()=>{
                            ToastManager.show('操作成功')
                        })
                    }}
                >
                    <Text style={styles.btnText}>停止充电</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.5}
                    style={[styles.btn,{marginLeft: 40,backgroundColor: Colors.lightGreen}]}
                    onPress={() => {
                        this.controlDevice({status: 1},()=>{
                            ToastManager.show('操作成功')
                        })
                    }}
                >
                    <Text style={styles.btnText}>开始充电</Text>
                </TouchableOpacity>
            </View>
        )
    }
	

	getMainPanelPage(){
		return(
			<View style={styles.main}>
                {this.renderCurrentData()}
                {this.showStaticButton()}
			</View>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors
		
		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getHeaderView()}
				{this.getMainPanelPage()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#0167FF',
	},
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
	wrapper:{
        marginTop: 0.05*screenH,
        borderWidth: 5, 
        borderColor: 'black',
        marginHorizontal: 20,
        borderRadius: 5,
        height: screenH*0.6
    },
    scrollWrapper:{
        paddingHorizontal: 25,
        
    },
    scrollContent:{
        paddingBottom: 10
    },
    item:{
        paddingVertical: 8,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    bottomBtnWrapper:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20,
        flex: 1,
        paddingBottom: BottomSafeMargin,
    },
    btn:{
        paddingVertical: 12,
        paddingHorizontal: 25,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        height: 40
    },
    btnText:{
        fontSize: 15,
        color: Colors.white,
        fontWeight: 'bold'
    },
    titleText:{
        fontSize: 14, 
        marginRight: 20
    },
    valueText:{
        fontSize: 14, 
        maxWidth: '60%'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SolarEnergy)



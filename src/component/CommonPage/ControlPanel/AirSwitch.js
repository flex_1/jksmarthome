/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter,
	Switch,
    StatusBar,
    Animated
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

//websocket
let deviceWebSocket = null

class AirSwitch extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData')

        this.state = {
            ...this.state,
            status: 0,
            temperature: 0,
            effectiveEfficiency: 0,
            invalidEfficiency: 0,
            
			isLoading: true,
        }
    }
    
	componentDidMount() {
        super.componentDidMount()
		this.requestDeviceStatus()
	}

	componentWillUnmount(){
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;

        this.setState({
            status: data.status == 1,
            effectiveEfficiency: parseInt(data.effectiveEfficiency),
            temperature: data.temperature,
            invalidEfficiency: parseInt(data.invalidEfficiency),
        })
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			this.setState({isLoading: false})
			if (data.code == 0) {
				if(data.result && data.result.deviceStatus){
					let statusJSON = JSON.parse(data.result.deviceStatus)
                    this.setState({
                        isCollect: data.result && data.result.isCollect,
                        status: statusJSON.status,
                        effectiveEfficiency: statusJSON.effectiveEfficiency,
                        temperature: statusJSON.temperature,
                        invalidEfficiency: statusJSON.invalidEfficiency,
                    })
				}
				
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({isLoading: false})
			ToastManager.show('网络错误')
		}
	}
    
    // 灯 开关 控制
    async controlDevice(params,callBack,failCallback){
        params = params || {}
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				failCallback && failCallback()
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误，请稍后重试')
		}
	}

	// 更改状态
	changeStatus(target){
		if(this.state.isLoading){
			ToastManager.show('正在请求数据，请稍后再试')
			return
		}
		// let oldStatus = this.state.status
		// this.setState({
		// 	status: target
		// })
		this.controlDevice({status: target, id: this.deviceData.deviceId},
			()=>{
				// 更改状态成功的回调
				this.setState({
					isLoading: false,
					status: target
				})         
			},
			()=>{
				// 更改状态失败的回调
				this.setState({
					// status: oldStatus,
					isLoading: false
				})
			},
		)
    }
    
    showStaticButton(){
        let light_img = this.state.status ? require('../../../images/panel/air_switch_on.png') : require('../../../images/panel/air_switch_off.png')
        let switch_text = this.state.status ? 'ON' : 'OFF'
        return(
            <View style={styles.topWrapper}>
                <TouchableOpacity
                    activeOpacity={0.5}
                    style={styles.switchBtn}
                    onPress={() => {
                        let target = this.state.status ? 0:1 
                        this.changeStatus(target)
                    }}
                >
                    <Text style={{fontSize:26,color:Colors.white,fontWeight:'bold'}}>{switch_text}</Text>
                    <Image style={{width:'100%',height:'100%',marginTop: 20}} source={light_img}/>
                </TouchableOpacity>
            </View>
        )
    }

    getBottomDetails(){
        return(
            <View style={styles.bottomWrapper}>
                <Text style={{color:Colors.white,fontSize:16,marginLeft:16}}>温度功率</Text>
                <View style={{flexDirection:'row',marginTop:20}}>
                    <View style={styles.infoWrapper}>
                        <Text style={styles.value}>{this.state.temperature}</Text>
                        <Text style={styles.title}>当前温度</Text>
                    </View>
                    <View style={styles.infoWrapper}>
                        <Text style={styles.value}>{this.state.effectiveEfficiency}</Text>
                        <Text style={styles.title}>有效能耗</Text>
                    </View>
                    <View style={styles.infoWrapper}>
                        <Text style={styles.value}>{this.state.invalidEfficiency}</Text>
                        <Text style={styles.title}>无效能耗</Text>
                    </View>
                </View>
            </View>
        )
    }

	getMainPanelPage(){
		return(
			<View style={styles.main}>
                {this.showStaticButton()}
                {this.getBottomDetails()}
			</View>
		)
	}

	render() {
		let bgColor = this.state.status ? '#17B7AC' : '#16161E'
		return (
			<View style={[styles.container,{backgroundColor: bgColor}]}>
				{this.getHeaderView()}
				{this.getMainPanelPage()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#0167FF',
	},
	main: {
		flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
	header: {
		width: '100%',
		height: StatusBarHeight + NavigationBarHeight
	},
	status: {
		height: StatusBarHeight,
		width: '100%',
	},
	nav: {
		height: NavigationBarHeight,
		width: '100%',
		flexDirection: 'row',
		alignItems: 'center'
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight: 5
	},
	navImg: {
		width: 22,
		height: 22,
		resizeMode: 'contain',
	},
	backTouch:{
		height: '100%',
		marginLeft: 16,
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'row'
    },
    topWrapper:{
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    switchBtn:{
        width: screenW*0.6,
        height: screenW*0.6,
        justifyContent:'center',
        alignItems:'center'
    },
    bottomWrapper:{
        paddingBottom: BottomSafeMargin + 0.1*screenH,
        paddingTop: 20
    },
    infoWrapper:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center'
    },
    value:{
        color:Colors.white,
        fontSize:18,
        fontWeight: 'bold'
    },
    title:{
        color:Colors.white,
        fontSize:12,
        marginTop: 10
    }
	
});

export default AirSwitch



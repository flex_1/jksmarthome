/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter,
    Animated,
    Modal,
    Keyboard
} from 'react-native';
import { NetUrls,NetParams,Colors } from '../../../common/Constants';
import { NavigationBarHeight,StatusBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel'; 
import { connect } from 'react-redux';
import AnimateButton from '../../../common/CustomComponent/AnimateButton';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const TimerCountDefault = 15

class RISelfLearn extends Panel {

	constructor(props) {
		super(props);
        const { getParam } = props.navigation;
        
        this.keyboardWillShowListener = null
		this.deviceData = getParam('deviceData')
        
        this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,
            currentModel: 0,   //0-控制 1-学习 2-编辑
            mainButtons: [],
            showInputModel: false,
            showLearningModel: false,
            inputString: '',
            selectBtnId: null,
            bindSeriaNO: '',

            isOnBinding: false,
            timerCount: TimerCountDefault
        }
    }
    
	componentDidMount() {
        super.componentDidMount()
        this.requestDeviceStatus()

        if(Platform.OS == 'ios'){
            this.keyboardWillShowListener = Keyboard.addListener('keyboardWillShow', this._keyboardWillShow.bind(this))
        }else{
            this.keyboardWillShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardWillShow.bind(this))
        }
	}

	componentWillUnmount(){
        this.keyboardWillShowListener.remove();
        this.interval && clearInterval(this.interval);
        super.componentWillUnmount()
    }

    _keyboardWillShow(e){
        const keyboardHeight = e.endCoordinates.height

        if(Platform.OS == 'android'){
            return
        }
        this.bottom_wrapper && this.bottom_wrapper.setNativeProps({
            ...styles.bottomWrapper,
            height: keyboardHeight + 70
        })
    }

    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
                let deviceStatus = data.result && data.result.deviceStatus
                let btns = JSON.parse(deviceStatus)
                this.setState({
                    mainButtons: btns || []
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({isLoading: false})
			ToastManager.show('网络错误')
		}
    }

    // send btn request
    async sendRequest(id){
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				params: {
                    deviceId: this.deviceData.deviceId,
                    number: id
                }
            });
			if (data.code == 0) {
                ToastManager.show('控制成功')
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    // edit buttons
    async requestEditBtns(params){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.saveRIStudyButton,
				params: {
                    ...params,
                    deviceId: this.deviceData.deviceId
                }
            });
            SpinnerManager.close()
			if (data.code == 0) {
                this.requestDeviceStatus()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    // delete RIStudy Button
    async requestDeleteButton(id){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.deleteRIStudyButton,
				params: {
                    buttonId: id,
                    deviceId: this.deviceData.deviceId
                }
            });
            SpinnerManager.close()
			if (data.code == 0) {
                this.requestDeviceStatus()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    // request binging
    async requestBinding() {
        SpinnerManager.show()
        this.setState({isOnBinding: true})
        try {
            let data = await postJson({
                url: NetUrls.bindingRIStudy,
                params: {
                    deviceId: this.deviceData.deviceId,
                    buttonId: this.state.selectBtnId,
                    sourceType: 1
                }
            });
            SpinnerManager.close()
            if (data && data.code == 0) {
                this.setState({
                    bindSeriaNO: data.result || '',
                    timerCount: TimerCountDefault
                })
                this.startCount()
                this.animateButton.startAnimation();
            } else {
                this.setState({ isOnBinding: false })
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({ isOnBinding: false })
            ToastManager.show(JSON.stringify(error))
        }
    }

    // request quick binging
    async requestQuickBinding() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.checkmark,
                params: {
                    deviceId: this.deviceData.deviceId,
                    buttonId: this.state.selectBtnId
                }
            });
            SpinnerManager.close()
            if (data && data.code == 0) {
                this.requestDeviceStatus()
                ToastManager.show('绑定成功')
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({ isOnBinding: false })
            ToastManager.show(JSON.stringify(error))
        }
    }

    async requestBindStatus() {
        try {
            let data = await postJson({
                url: NetUrls.bindingRIStudyResult,
                timeout: 1.8, // time out when no respones in 2 seconds
                params: {
                    prefix: this.state.bindSeriaNO,
                    deviceId: this.deviceData.deviceId,
                }
            });
            SpinnerManager.close()
            if (data && data.code == 0) {
                if (data.result.status == 0) {
                    // fail
                    this.interval && clearInterval(this.interval);
                    this.animateButton.stopAnimation()
                    this.setState({timerCount: 0})
                } else if (data.result.status == 1) {
                    // success
                    this.interval && clearInterval(this.interval);
                    this.animateButton.stopAnimation()
                    this.setState({
                        showLearningModel: false
                    },()=>{
                        ToastManager.show(this.state.selectBtnName + '绑定成功')
                        this.requestDeviceStatus()
                    })
                } else {
                    // go on
                }
            } else {
                let error = new CommonException(data.msg, data.code)
                throw error
            }
        } catch (error) {
            if (error.code == NetParams.networkTimeout) {
                // 网络 1.8 秒内未完成
                return
            }
        }
    }

    // btn click events
    mianBtnClick(rowData){
        if(this.state.currentModel == 0){
            if(rowData.study == 0){
                ToastManager.show('该按钮暂未绑定功能')
                return
            }
            this.sendRequest(rowData.id)
        }else if(this.state.currentModel == 1){
            this.setState({
                showLearningModel: true,
                selectBtnId: rowData.id,
                selectBtnName: rowData.name,
                timerCount: TimerCountDefault,
                isOnBinding: false
            })
        }else if(this.state.currentModel == 2){
            this.setState({
                inputString: rowData.name,
                showInputModel: true,
                selectBtnId: rowData.id
            })
        }
    }

    startCount() {
        this.interval = setInterval(() => {
            let timerCount = this.state.timerCount - 1;
            if (timerCount <= 0) {
                this.animateButton.stopAnimation()
                this.interval && clearInterval(this.interval);
            } else {
                if (timerCount % 2 == 0) {
                    this.requestBindStatus()
                }
            }
            this.setState({ timerCount: timerCount })
        }, 1000)
    }

    showFailView(){
        return (
            <View style={styles.failWrapper}>
                <Image style={styles.failImg} source={require('../../../images/sceneIcon/failed.png')}/>
                <Text style={styles.failTips}>绑定失败!</Text>
                <View style={styles.failBtnsWrapper}>
                    <TouchableOpacity style={styles.failBtn} onPress={()=>{
                        this.setState({ showLearningModel: false })
                    }}>
                        <Text style={styles.btnText}>取消</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.failBtn,{marginLeft: 40}]} onPress={()=>{
                        this.requestBinding()
                    }}>
                        <Text style={styles.btnText}>再试一次</Text>
                    </TouchableOpacity>
                </View>
            </View>
            
        )
    }

    // center animation button
    showAniButton(textVal) {
        return(
            <AnimateButton
                style = {{ width:'100%',alignItems:'center',marginTop: 30}}
                ref={e => this.animateButton = e}
                centerLabel = {textVal}
                onClick={()=>{
                    if(this.state.isOnBinding){
                        return
                    }
                    this.requestBinding()
                }}
            />
        )
    }

    getBindingBtn() {
        if(this.state.timerCount == 0){
            return this.showFailView()
        }
        if (this.state.isOnBinding && this.state.timerCount >0 ) {
            return this.showAniButton(this.state.timerCount + 's ')
        } else if(!this.state.isOnBinding){
            return this.showAniButton('点击开始')
        }else{
            return this.showAniButton('正在绑定...')
        }
    }

    getQuickBtn(){
        if(this.state.isOnBinding){
            return null
        }
        return(
            <TouchableOpacity
                activeOpacity={0.7}
                style={[styles.quickBtn,{backgroundColor:Colors.newTheme}]}
                onPress={()=>{
                    this.setState({ showLearningModel: false })
                    this.requestQuickBinding()
                }}
            >
                <Text style={{fontSize: 16,fontWeight:'bold',color: Colors.white}}>快速学习</Text>
            </TouchableOpacity>
        )
    }
    
    renderEnditCtr(){
        const Colors = this.props.themeInfo.colors
        let btnsArr = [{name: '控制', icon: require('../../../images/panel/icons/control.png')},
                       {name: '学习', icon: require('../../../images/panel/icons/learn.png')},
                       {name: '编辑', icon: require('../../../images/panel/icons/edit.png')},]
        let btns = btnsArr.map((value, index)=>{
            let showImg = this.state.currentModel == index
            let textColor = this.state.currentModel == index ? Colors.themeText : Colors.themeTextLight
            let textSize = this.state.currentModel == index ? 20 : 16
            let textFont = this.state.currentModel == index ? 'bold' : 'normal'

            return(
                <TouchableOpacity
                    key={'btns_'+index}
                    activeOpacity={0.7} 
                    style={styles.headTouch}
                    onPress={()=>{
                        if(this.state.currentModel == index){
                            return
                        }
                        this.setState({ currentModel: index })
                    }}
                >
                    {showImg ? <Image style={[styles.headerIcon,{tintColor: Colors.themeText}]} source={value.icon}/> : null}
                    <Text style={{color: textColor,fontSize: textSize,fontWeight: textFont}}>{value.name}</Text>
                </TouchableOpacity>
            )
        })

        return(
            <View style={styles.headWrapper}>
                {btns}
            </View>
        )
    }

    renderDeleteBtn(rowData){
        if(this.state.currentModel != 2){
            return null
        }
        return(
            <TouchableOpacity activeOpacity={0.7} style={styles.deleteBtn} onPress={()=>{
                if(rowData.study == 0){
                    this.requestDeleteButton(rowData.id)
                    return
                }
                Alert.alert(
                    '提示',
                    rowData.name + '已被绑定功能，确定要删除？',
                    [   
                        { text: '取消', onPress: () => { }, style: 'cancel' },
                        { text: '删除', onPress: () => { this.requestDeleteButton(rowData.id) }}
                    ],
                )
                
            }}>
                <View style={styles.deleteView}>
                    <Image style={styles.deleteImg} source={require('../../../images/btn_delete.png')}/>
                </View>
            </TouchableOpacity>
        )
    }

    renderMainButtons(){
        const Colors = this.props.themeInfo.colors

        let mainBtns = this.state.mainButtons.map((value, index)=>{
            let btnBg = Colors.lightGray
            if(value.study == 1){
                btnBg = Colors.lightGreen
            }else{
                if(this.props.themeInfo.isDark){
                    btnBg = Colors.themeBg
                }else{
                    btnBg = this.state.currentModel == 1 ? Colors.white : Colors.lightGray
                }
            }
            let textColor = value.study == 1 ? Colors.white : Colors.themeTextLight

            return (
                <TouchableOpacity
                    activeOpacity={0.7}
                    key={'mainBtns_'+index}
                    style={[styles.mainBtnTouch,{backgroundColor: btnBg}]}
                    onPress={()=>{
                        this.mianBtnClick(value)
                    }}
                >   
                    <Text numberOfLines={2} style={{color: textColor, fontSize: 15}}>{value.name}</Text>
                    {this.renderDeleteBtn(value)}
                </TouchableOpacity>
            )
        })

        // add button
        if(this.state.currentModel == 2){
            mainBtns.push(
                <TouchableOpacity
                    activeOpacity={0.7}
                    key={'mainBtns_add'}
                    style={[styles.mainBtnTouch,{backgroundColor: Colors.themeBg}]}
                    onPress={()=>{
                        this.requestEditBtns()
                    }}
                >
                    <Image source={require('../../../images/btn_add.png')} style={{width:15,height:15}}/>
                    <Text style={{fontSize:15,marginLeft:5,color: Colors.themeButton}}>添加</Text>
                </TouchableOpacity>
            )
        }

        return (
            <ScrollView contentContainerStyle={styles.scrollContent}>
                <View style={styles.mainBtnWrapper}>
                    {mainBtns}
                </View>
            </ScrollView>
        )
    }

    renderLearningModalClose(){
        if(this.state.isOnBinding){
            return null
        }
        return(
            <TouchableOpacity style={styles.closeBtnTouch} onPress={()=>{
                this.setState({showLearningModel: false})
            }}>
                <Image style={styles.closeBtnImg} source={require('../../../images/panel/icons/close.png')}/>
            </TouchableOpacity>
        )
    }

    renderLearningModel(){
        return(
            <Modal
                style={{ flex: 1 }}
                animationType='fade'
                transparent={true}
                visible={this.state.showLearningModel}
                onShow={() => {
                }}
                onRequestClose={() => {
                }}
            >
                <View style={styles.modalTouchable}>
                    <Text style={[styles.failTips,{marginTop: screenH * 0.3}]}>绑定按钮:{'   '}
                        <Text style={{marginLeft: 10,color: Colors.themeBG}}>{this.state.selectBtnName}</Text>
                    </Text>
                    {this.renderLearningModalClose()}
                    {this.getBindingBtn()}
                    {this.getQuickBtn()}
                </View>
            </Modal>
        )
    }

    renderTextInputModel(){
        const Colors = this.props.themeInfo.colors
        let touchBg = Colors.themeButton
        if(this.state.inputString.length <=0 || this.state.inputString.length >6){
            touchBg = Colors.tabInactive
        }

        return(
            <Modal
                style={{ flex: 1 }}
                animationType='fade'
                transparent={true}
                visible={this.state.showInputModel}
                onShow={() => {
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={styles.modalTouchable} 
                    onPress={() => {
                        this.setState({
                            showInputModel: false
                        })
                    }}
                />
                <View ref={e => this.bottom_wrapper = e} style={styles.bottomWrapper}>
                    <View style={styles.inputWrapper}>
                        <TextInput
                            value={this.state.inputString}
                            style={styles.input}
                            autoFocus={true}
                            returnKeyLabel={'完成'}
                            returnKeyType={'done'}
                            underlineColorAndroid="transparent"
                            onChangeText={(text)=>{
                                this.setState({inputString:text})
                            }}
                            onSubmitEditing={()=>{
                                this.setState({showInputModel: false})
                            }}
                        />
                        <TouchableOpacity
                            activeOpacity={0.7}
                            style={[styles.sureBtnTouch,{backgroundColor:touchBg}]} 
                            onPress={()=>{
                                if(!this.state.inputString || this.state.inputString.length > 6){
                                    return
                                }
                                this.setState({showInputModel: false})
                                this.requestEditBtns({
                                    buttonId: this.state.selectBtnId, 
                                    name: this.state.inputString
                                })
                            }}
                        >
                            <Text style={{fontSize: 15, color: Colors.white}}>确定</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.split}/>
                </View>
            </Modal>
        )
    }

	getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

		return(
			<View style={[styles.main,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderEnditCtr()}
                {this.renderMainButtons()}
                {this.renderTextInputModel()}
                {this.renderLearningModel()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
    },
    scrollContent:{
        paddingBottom: BottomSafeMargin + 10
    },
    headWrapper:{
        marginTop: 10,
        flexDirection:'row',
        paddingVertical:5,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headTouch:{
        height: 40,
        width: 80,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 20,
        borderRadius: 4,
        flexDirection: 'row'
    },
    headerIcon:{
        width: 20,
        height: 20,
        resizeMode:'contain',
        marginRight: 5
    },
    mainBtnWrapper:{
        paddingLeft: 20, 
        paddingVertical:5,
        flexDirection: 'row',
        flexWrap: 'wrap',
        // justifyContent: 'space-between',
        marginTop: 10
    },
    mainBtnTouch:{
        height: 45,
        width: (screenW - 85)/ 3,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        marginBottom: 20,
        flexDirection: 'row',
        marginRight: 20
    },
    deleteBtn:{
        position:'absolute',
        height:28,
        width:28,
        top:-10,
        right:-10,
        justifyContent:'center',
        alignItems:'center'
    },
    deleteView:{
        backgroundColor: 'red',
        height: 18,
        width: 18,
        borderRadius: 9,
        justifyContent:'center',
        alignItems:'center'
    },
    deleteImg:{
        width:12,
        height:12,
        resizeMode:'contain'
    },
    modalTouchable:{ 
        width: '100%', 
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.8)',
        alignItems:'center'
    },
    input: {
        height:50,
        paddingHorizontal:10,
        color: Colors.themeTextBlack,
        fontSize: 18,
        flex: 1
    },
    bottomWrapper:{
        position:'absolute',
        height: 0,
        width:'100%',
        bottom: 0,
        left:0,
        backgroundColor:'white',
        height: 70
    },
    inputWrapper:{
        height: 50, 
        flexDirection:'row',
        alignItems:'center',
        paddingHorizontal: 16
    },
    sureBtnTouch:{
        height: 40,
        width:80,
        borderRadius:4,
        justifyContent:'center',
        alignItems:'center'
    },
    split:{
        height: 1, 
        width: '60%', 
        backgroundColor: Colors.splitLightGray, 
        marginLeft: 20
    },
    failWrapper:{
        marginTop: 50,
        justifyContent:'center',
        alignItems:'center'
    },
    failBtnsWrapper:{
        flexDirection:'row',
        marginTop: 50
    },
    failImg:{
        width:120,
        height:120,
        resizeMode:'contain'
    },
    failTips:{
        marginTop: 40,
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.white
    },
    failBtn:{
        height: 40,
        width: 100,
        backgroundColor: Colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    btnText:{
        fontSize: 15,
        color: Colors.themeTextBlack
    },
    closeBtnTouch:{
        position:'absolute',
        top: '20%',
        right: 30
    },
    closeBtnImg:{
        width: 40,
        height: 40,
    },
    quickBtn:{
        position:'absolute',
        bottom:'15%',
        justifyContent:'center',
        alignItems:'center',
        left:'25%',
        borderRadius:5,
        width:'50%',
        height:50,
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(RISelfLearn)
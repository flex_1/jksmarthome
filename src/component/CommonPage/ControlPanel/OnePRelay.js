/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	Dimensions,
    Modal
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel'; 

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class OnePRelay extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData')
        this.animating = false
        this.animatedFun = null;

        
        this.state = {
            ...this.state,
			status: this.deviceData?.status,
            temperature: '-',
            electricity: '-',
            voltage: '-',
            power: '-',
            electricQuantity: '-',
            alertStatus: '温度预警',
            version: null,
            modalVisible: false,
            lockStatus: null,
            powerStatus: null
        }
    }
    
	componentDidMount() {
        super.componentDidMount()
		this.requestDeviceStatus()
        //设置定时器 定时刷新
		this.interval = setInterval(()=>{
			if(this.state.status != 0){
                this.requestDeviceStatus()
            }
        },10*1000)
	}

	componentWillUnmount(){
        this.interval && clearInterval(this.interval)

        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;
        
		this.setState({
            status: data.status
        })
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			this.setState({isLoading: false})
			if (data.code == 0) {
				if(data.result?.deviceStatus){
                    let statusJSON = JSON.parse(data.result.deviceStatus) || {}
                    this.setState({
                        status: statusJSON.status,
                        temperature: statusJSON.temperature, 
                        electricity: statusJSON.electricity,
                        voltage: statusJSON.voltage,
                        power: statusJSON.power,
                        electricQuantity: statusJSON.electricQuantity,
                        alertStatus: statusJSON.alertStatus,
                        version: data.result?.version,
                        lockStatus: statusJSON.lock,
                        powerStatus: statusJSON.powerOnReset,
                    })
				}
				this.setState({
                    isCollect: data.result?.isCollect
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({isLoading: false})
			ToastManager.show('网络错误')
		}
	}
    
    // 灯 开关 控制
    async controlDevice(params,callBack,failCallback){
        params = params || {}
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: {
                    ...params,
                    id: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				failCallback && failCallback()
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误，请稍后重试')
		}
    }

    getHeaderView() {
        const { pop } = this.props.navigation;
        return (
            <View style={styles.header}>
                <View style={styles.status}/>
                <View style={styles.nav}>
                    <TouchableOpacity style={styles.backTouch} onPress={() => { pop() }}>
                        <Image style={{ width: 10, height: 18 }} source={require('../../../images/back_white.png')} />
                        <Text numberOfLines={1} style={styles.navTitle}>{this.state.title}</Text>
                    </TouchableOpacity>
                    <View style={{ flex: 1 }} />
                    {this.state.version ? <TouchableOpacity style={styles.navTouch} onPress={() => {
                        this.setState({modalVisible: true})
                    }}>
                        <Image style={styles.sahreImg} source={require('../../../images/ProtocolControl/setting.png')} />
                    </TouchableOpacity> : null}
                    <TouchableOpacity style={styles.navTouch} onPress={() => { this.goToSetting() }}>
                        <Image style={styles.navImg} source={require('../../../images/setting_white.png')} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    
    renderCurrentData(){
        let temp = this.state.temperature
        let a = this.state.electricity
        let v = this.state.voltage
        let w = this.state.power
        let p = this.state.electricQuantity

        return(
            <View style={{width:'100%',paddingVertical:10,paddingHorizontal:16}}>
                <View style={{flexDirection:'row',flexWrap:'wrap'}}>
                    <View style={styles.item}> 
                        <Image style={styles.titleIcon} source={require('../../../images/panel/icons/temp.png')}/>
                        <Text style={styles.valueText}>{temp}℃</Text>
                    </View>
                    <View style={styles.item}>
                        <Image style={styles.titleIcon} source={require('../../../images/panel/icons/a.png')}/>
                        <Text style={styles.valueText}>{a}A</Text>
                    </View>
                    <View style={styles.item}>
                        <Image style={styles.titleIcon} source={require('../../../images/panel/icons/v.png')}/>
                        <Text style={styles.valueText}>{v}V</Text>
                    </View>
                    <View style={styles.item}>
                        <Image style={styles.titleIcon} source={require('../../../images/panel/icons/w.png')}/>
                        <Text style={styles.valueText}>{w}W</Text>
                    </View>
                    <View style={styles.item}>
                        <Image style={styles.titleIcon} source={require('../../../images/panel/icons/p.png')}/>
                        <Text style={styles.valueText}>{p}KW/H</Text>
                    </View>
                </View>
                <View style={[styles.item,{flexWrap:'wrap'}]}>
                    <Image style={styles.titleIcon} source={require('../../../images/panel/icons/warn.png')}/>
                    <Text style={[styles.valueText,{lineHeight: 18,marginLeft: 10}]} numberOfLines={2}>{this.state.alertStatus+'     '}</Text>
                    <TouchableOpacity style={styles.clearBtn} onPress={()=>{
                        this.controlDevice({type: 9, value:0},()=>{
                            this.setState({
                                alertStatus: '无报警'
                            })
				            ToastManager.show('报警已清除')     
			            })
                    }}>
                        <Text style={styles.clearText}>清除报警状态</Text>
                    </TouchableOpacity>
                    <Text style={[styles.valueText,{marginLeft: 20}]}>版本: {this.state.version || '未知'}</Text>
                </View>
            </View>
        )
    }

    showStaticButton(){
        let light_img = <Image style={styles.lightImg} source={require('../../../images/panel/light_off.png')}/>
        if(this.state.status == 1){
            light_img = <Image style={styles.lightImg} source={require('../../../images/panel/light_on.png')}/>
        }else if(this.state.status == 2){
            light_img = (
                <View style={styles.lockWrapper}>
                    <Image style={{width:50,height:50,resizeMode:'contain'}} source={require('../../../images/panel/lock.png')}/>
                    <Text style={styles.lockText}>锁定</Text>
                </View>
            )
        }
        
        return(
            <View style={{flex: 1,alignItems:'center',width:'100%',marginTop: screenH * 0.18}}>
                <TouchableOpacity
                    activeOpacity={0.5}
                    style={styles.swicthBtn}
                    onPress={() => {
                        if(this.state.status == 2){
                            ToastManager.show('设备锁定，不可控制')
                            return
                        }
                        let target = this.state.status == 1 ? 0:1 
                        this.controlDevice({value: target, type: 0},()=>{
				            // 更改状态成功的回调
				            this.setState({
					            status: target
				            })      
			            })
                    }}
                >
                    {light_img}
                </TouchableOpacity>
            </View>
        )
    }

    renderDebugView(){
        const {powerStatus, lockStatus} = this.state
        const lockArr = [{name: '锁定开',status: 1, val:11}, {name: '锁定关',status: 0, val:12}, {name: '解除锁定',status: -1, val:13}]
        const powerArr = [{name: '上电开',status: 1, val:15}, {name: '上电关',status: 0, val:16}, {name: '上电记忆',status: 2, val:17}]

        return(
            <View style={{backgroundColor: Colors.white, flex: 7, justifyContent:'center',alignItems:'center'}}>
                <View style={styles.debugWrapper}>
                    {lockArr.map((value, index)=>{
                        return(
                            <TouchableOpacity key={index} style={lockStatus == value.status ? styles.touchOn : styles.touchOff} onPress={()=>{
                                this.controlDevice({type: 0, value: value.val}, ()=>{
                                    this.setState({ lockStatus: value.status})
                                })
                            }}>
                                <Text style={lockStatus == value.status ? styles.textOn : styles.textOff}>{value.name}</Text>
                            </TouchableOpacity>
                        )
                    })}
                </View>
                <View style={styles.debugWrapper}>
                    {powerArr.map((value, index)=>{
                        return(
                            <TouchableOpacity key={index} style={powerStatus == value.status ? styles.touchOn : styles.touchOff} onPress={()=>{
                                this.controlDevice({type: 0, value: value.val}, ()=>{
                                    this.setState({ powerStatus: value.status})
                                })
                            }}>
                                <Text style={powerStatus == value.status ? styles.textOn : styles.textOff}>{value.name}</Text>
                            </TouchableOpacity>
                        )
                    })}
                </View>
                <TouchableOpacity style={styles.closeTouch} onPress={()=>{
                    this.setState({modalVisible: false})
                }}>
                    <Text style={styles.closeText}>关闭</Text>
                </TouchableOpacity>
            </View>
        )
    }

    // 获取选择框的 Modal
	getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.requestDeviceStatus()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
                {this.renderDebugView()}
            </Modal>
        )
	}
	

	getMainPanelPage(){
		return(
			<View style={styles.main}>
                {this.renderCurrentData()}
                {this.showStaticButton()}
			</View>
		)
	}

	render() {
        let bgColor = '#16161E'
        if(this.state.status == 1){
            bgColor = '#17B7AC'
        }else if(this.state.status == 2){
            bgColor = Colors.middleGray
        }
		return (
			<View style={[styles.container,{backgroundColor: bgColor}]}>
				{this.getHeaderView()}
				{this.getMainPanelPage()}
                {this.getModal()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#0167FF',
	},
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
	swicthBtn: {
        width: screenW*0.5,
        height:screenW*0.5,
        justifyContent:'center',
        alignItems:'center'
    },
    item:{
        flexDirection:'row',
        alignItems:'center',
        marginRight: 10,
        marginBottom: 10
    },
    valueText:{
        fontSize: 13,
        color: Colors.white,
        marginLeft: 5
    },
    titleIcon:{
        resizeMode: 'contain',
        width: 16,
        height: 16,
    },
    clearBtn:{
        paddingVertical: 10
    },
    clearText:{
        fontSize: 14,
        color: Colors.themBgRed,
        fontWeight: 'bold'
    },
    lightImg:{
        width: '100%',
        height: '100%'
    },
    lockWrapper:{
        width:'100%', 
        height:'100%',
        backgroundColor: Colors.white,
        borderRadius: screenW*0.25,
        justifyContent:'center',
        alignItems: 'center'
    },
    lockText:{
        fontSize: 16, 
        color: Colors.themeTextLightGray,
        marginTop:20
    },
    header: {
        position: 'absolute',
        zIndex: 999,
        top: 0,
        left: 0,
        width: '100%',
        height: StatusBarHeight + NavigationBarHeight
    },
    headerContain: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20,
        paddingHorizontal: 16
    },
    status: {
        height: StatusBarHeight,
        width: '100%',
    },
    nav: {
        height: NavigationBarHeight,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 15,
        paddingLeft: 10,
        marginRight: 5
    },
    navImg: {
        width: 22,
        height: 22,
        resizeMode: 'contain'
    },
    sahreImg:{
        width: 22,
        height: 22,
        resizeMode: 'contain',
        tintColor: Colors.white
    },
    navTitle:{ 
        marginLeft: 15, 
        fontSize: 24, 
        fontWeight: 'bold',
        color: Colors.white
    },
    backTouch: {
        height: '100%',
        marginLeft: 16,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        maxWidth:'50%'
    },
    modalTouchable:{ 
        flex: 3,
        backgroundColor: Colors.translucent
    },
    touchOn:{
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        width: screenW * 0.25,
        borderRadius: 5,
        backgroundColor: '#008CFF'
    },
    touchOff:{
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        width: screenW * 0.25,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: Colors.borderLightGray,
    },
    textOn:{
        color: Colors.white,
        fontSize: 14,
        fontWeight: '400'
    },
    textOff:{
        color: Colors.themeTextBlack,
        fontSize: 14,
        fontWeight: '400'
    },
    debugWrapper:{
        flexDirection: 'row',
        alignItems: 'center',
        width:'100%',
        paddingHorizontal: 20,
        justifyContent:'space-between',
        marginBottom: 25
    },
    closeTouch:{
        position: 'absolute',
        top: 0,
        right: 0,
        width: 80,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    closeText:{
        fontSize: 16,
        color: Colors.themeTextBlack,
        fontWeight: '400'
    }
});

export default OnePRelay



/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	DeviceEventEmitter,
	Switch,
    StatusBar,
    Animated
} from 'react-native';
import { NetUrls } from '../../../common/Constants';
import { NavigationBarHeight,StatusBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel'; 
import { connect } from 'react-redux';

class Sensor extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData')
        this.animating = false
        this.animatedFun = null;

        this.state = {
            ...this.state,
            isWhiteBg: !props.themeInfo.isDark,
            sensorData: {},
            sensorList: [],
            showButton: false
        }
    }
    
	componentDidMount() {
        super.componentDidMount()
		this.requestDeviceStatus()
        this.sensorAttributesUpdate = DeviceEventEmitter.addListener('sensorAttributesUpdate',()=>{
            this.requestDeviceStatus()
        })
	}

	componentWillUnmount(){
        this.sensorAttributesUpdate.remove()
        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;
        
		// this.setState({
        //     sensorData: data || {}
        // })

        this.requestDeviceStatus()
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getMergeDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			this.setState({isLoading: false})
			if (data.code == 0) {
				if(data.result?.deviceStatus){
                    let statusJSON = JSON.parse(data.result.deviceStatus)
                    this.setState({
                        sensorData: statusJSON || {},
                        sensorList: data.result.list || [],
                    })
				}
				this.setState({
                    isCollect: data.result?.isCollect,
                    showButton: data.result?.showButton
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({isLoading: false})
			ToastManager.show('网络错误')
		}
    }

    // 开关灯
    async controlDevice(params){
        
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				params: params
			});
			if (data.code == 0) {
				ToastManager.show('指令已下发')
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
    }

    renderSenorPage(){
        const Colors = this.props.themeInfo.colors;
        
        let sensors = this.state.sensorList.map((val, index)=>{
            let data = this.state.sensorData[val.id] || '-'
            return(
                <View key={'sensor_'+index} style={styles.item}>
                    <Image style={[styles.img,{tintColor: Colors.themeText}]} source={{uri: val.icon}}/>
                    <Text style={[styles.title,{color: Colors.themeText}]}>{val.name}:</Text>
                    <View style={{flex: 1}}/>
                    <Text style={[styles.value,{color: Colors.themeText}]}>{data}</Text>
                </View>
            )
        })
        return(
            <ScrollView contentContainerStyle={{paddingBottom: BottomSafeMargin + 10}}>
                {sensors}
            </ScrollView>
        )
    }

    renderBottomButton(){
        if(!this.state.showButton){
            return null
        }

        const Colors = this.props.themeInfo.colors

        return(
            <View style={styles.bottomWrapper}>
                <TouchableOpacity activeOpacity={0.7} style={[styles.btn,{marginRight: 60}]} onPress={()=>{
                    this.controlDevice({status: 1})
                }}>
                    <Image style={styles.btnImg} source={require('../../../images/panel/flash_on.png')}/>
                    <Text style={{fontSize: 12, marginTop: 10,color: Colors.themeTextLight}}>打开指示灯</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.7} style={styles.btn} onPress={()=>{
                    this.controlDevice({status: 0})
                }}>
                    <Image style={{width: 40, height: 40}} source={require('../../../images/panel/flash_off.png')}/>
                    <Text style={{fontSize: 12,marginTop: 10,color: Colors.themeTextLight}}>关闭指示灯</Text>
                </TouchableOpacity>
            </View>
        )
    }
    
	getMainPanelPage(){
        const Colors = this.props.themeInfo.colors

		return(
			<View style={[styles.main,{backgroundColor: Colors.themeBg}]}>
                {this.renderSenorPage()}
                {this.renderBottomButton()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
    item: {
        flexDirection:'row',
        alignItems:'center',
        marginTop: 20,
        paddingHorizontal: 30
    },
    img:{
        width:30,
        height:30,
        resizeMode:'contain'
    },
    title:{
        fontSize: 16,
        marginLeft: 10
    },
    value:{
        fontSize: 16,
        marginLeft: 5,
        fontWeight: 'bold'
    },
    bottomWrapper:{
        flexDirection:'row',
        paddingBottom: BottomSafeMargin + 20,
        paddingTop: 10,
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal: 20
    },
    btn:{
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnImg:{
        width: 40, 
        height: 40,
        resizeMode: 'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(Sensor)



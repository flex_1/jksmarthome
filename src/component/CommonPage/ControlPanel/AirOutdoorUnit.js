/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	FlatList,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter,
	Switch,
    StatusBar,
    Animated
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import Panel from './Panel'; 

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class AirOutdoorUnit extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData')
        this.animating = false
        this.animatedFun = null;

        
        this.state = {
            ...this.state,
			status: 0,
            statusData: {}
        }
    }
    
	componentDidMount() {
        super.componentDidMount()
		this.requestDeviceStatus()
        //设置定时器 定时刷新
		this.interval = setInterval(()=>{
			if(this.state.status != 0){
                this.requestDeviceStatus()
            }
        },10*1000)
	}

	componentWillUnmount(){
        this.interval && clearInterval(this.interval)

        super.componentWillUnmount()
    }
    
    // 重写父类方法
    updateDeviceStatus(data){
        if(data.deviceId != this.deviceData.deviceId) return;
        
		this.setState({
            status: data.status 
        })
    }

	// 获取当前 状态
    async requestDeviceStatus(){
		try {
			let data = await postJson({
				url: NetUrls.getDeviceStatus,
				params: {
                    deviceId: this.deviceData.deviceId
                }
			});
			this.setState({isLoading: false})
			if (data.code == 0) {
				if(data.result && data.result.deviceStatus){
                    let statusJSON = JSON.parse(data.result.deviceStatus)
                    this.setState({
                        status: statusJSON.status == 1,
                        statusData: statusJSON || {}
                    })
				}
				this.setState({
                    isCollect: data.result && data.result.isCollect
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			this.setState({isLoading: false})
			ToastManager.show('网络错误')
		}
	}
    
    // 灯 开关 控制
    async controlDevice(params,callBack,failCallback){
        params = params || {}
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: {
                    ...params,
                    id: this.deviceData.deviceId
                }
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				failCallback && failCallback()
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误，请稍后重试')
		}
    }
    
    renderCurrentData(){
        const {ErrorCode,settem,sysouttem,sysbacktem,Outtemperature,mainstatus,num1status,num2status,I1curcal,I2curcal } = this.state.statusData
        let specialColor = ErrorCode == '正常' ? Colors.white : Colors.themBgRed

        return(
            <View style={{width:'100%',paddingVertical:10,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                <View>
                    <Text style={styles.itemTitle}>运行状态:  <Text style={[styles.valueText,{color: specialColor}]}>{ErrorCode|| '-'}</Text></Text>
                    <Text style={styles.itemTitle}>系统出水温度:  <Text style={styles.valueText}>{sysouttem || '-'}</Text></Text>
                    <Text style={styles.itemTitle}>室外温度:  <Text style={styles.valueText}>{Outtemperature || '-'}</Text></Text>
                    <Text style={styles.itemTitle}>回路一开关:  <Text style={styles.valueText}>{num1status || '-'}</Text></Text>
                    <Text style={styles.itemTitle}>回路二开关:  <Text style={styles.valueText}>{num2status || '-'}</Text></Text>
                </View>
                <View style={{marginLeft: 25}}>
                    <Text style={styles.itemTitle}>设置温度:  <Text style={styles.valueText}>{settem || '-'}</Text></Text>
                    <Text style={styles.itemTitle}>系统回水温度:  <Text style={styles.valueText}>{sysbacktem || '-'}</Text></Text>
                    <Text style={styles.itemTitle}>主回路开关:  <Text style={styles.valueText}>{mainstatus || '-'}</Text></Text>
                    <Text style={styles.itemTitle}>回路一电流:  <Text style={styles.valueText}>{I1curcal || '-'}</Text></Text>
                    <Text style={styles.itemTitle}>回路二电流:  <Text style={styles.valueText}>{I2curcal || '-'}</Text></Text>
                </View>
            </View>
        )
    }

    showStaticButton(){
        let light_img = <Image style={styles.lightImg} source={require('../../../images/panel/light_off.png')}/>
        if(this.state.status == 1){
            light_img = <Image style={styles.lightImg} source={require('../../../images/panel/light_on.png')}/>
        }else if(this.state.status == 2){
            light_img = (
                <View style={styles.lockWrapper}>
                    <Image style={{width:50,height:50,resizeMode:'contain'}} source={require('../../../images/panel/lock.png')}/>
                    <Text style={styles.lockText}>锁定</Text>
                </View>
            )
        }
        
        return(
            <View style={{flex: 1,justifyContent:'center', alignItems:'center',paddingBottom: BottomSafeMargin}}>
                <TouchableOpacity
                    activeOpacity={0.5}
                    style={styles.swicthBtn}
                    onPress={() => {
                        if(this.state.status == 2){
                            ToastManager.show('设备锁定，不可控制')
                            return
                        }
                        let target = this.state.status == 1 ? 0:1 
                        this.controlDevice({status: target},()=>{
				            // 更改状态成功的回调
				            this.setState({
					            status: target
				            })      
			            })
                    }}
                >
                    {light_img}
                </TouchableOpacity>
            </View>
        )
    }
	

	getMainPanelPage(){
		return(
			<View style={styles.main}>
                {this.renderCurrentData()}
                {this.showStaticButton()}
			</View>
		)
	}

	render() {
        let bgColor = '#16161E'
        if(this.state.status == 1){
            bgColor = '#17B7AC'
        }else if(this.state.status == 2){
            bgColor = Colors.middleGray
        }
		return (
			<View style={[styles.container,{backgroundColor: bgColor}]}>
				{this.getHeaderView()}
				{this.getMainPanelPage()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#0167FF',
	},
	main: {
        flex: 1,
        paddingTop: StatusBarHeight + NavigationBarHeight
	},
	swicthBtn: {
        width: screenW*0.5,
        height:screenW*0.5,
        justifyContent:'center',
        alignItems:'center'
    },
    item:{
        flexDirection:'row',
        alignItems:'center',
        marginRight: 10,
        marginBottom: 10
    },
    
    titleIcon:{
        resizeMode: 'contain',
        width: 16,
        height: 16,
    },
    clearBtn:{
        paddingVertical: 10
    },
    clearText:{
        fontSize: 14,
        color: Colors.themBgRed,
        fontWeight: 'bold'
    },
    lightImg:{
        width: '100%',
        height: '100%'
    },
    lockWrapper:{
        width:'100%', 
        height:'100%',
        backgroundColor: Colors.white,
        borderRadius: screenW*0.25,
        justifyContent:'center',
        alignItems: 'center'
    },
    lockText:{
        fontSize: 16, 
        color: Colors.themeTextLightGray,
        marginTop:20
    },
    itemTitle:{
        marginTop: 20,
        fontSize: 14,
        color: Colors.white
    },
    valueText:{
        fontSize: 14,
        color: Colors.white,
        fontWeight: 'bold'
    },
});

export default AirOutdoorUnit



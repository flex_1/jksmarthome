/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Alert,
	Dimensions,
	DeviceEventEmitter
} from 'react-native';
import { Colors, NetUrls, NotificationKeys,NetParams } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from '../../../util/ApiRequest';
import Panel from './Panel';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

const bottomControllerH = 190

class Television extends Panel {

	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		
		this.state = {
			...this.state,
			switch: 0,
			mute: 0,//静音
			menu: 0,//菜单
			topbox: 0,//机顶盒
			currentPress: null, // 1 上  2 右  3 下  4左
		}
	}

	componentDidMount() {
		
    }

    componentWillUnmount() {
        
    }
    
    // 电视 控制
    async controlDevice(params,callBack){
        params = params || {}
        params.id = this.deviceData.deviceId
		try {
			let data = await postJson({
				url: NetUrls.controlCommon,
				timeout: NetParams.networkUserReactTimeoutTime,
				params: params
			});
			if (data.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show(JSON.stringify(error))
		}
	}

	// 控制频道
	changeChannel(val){
		let tips = val ? '频道+1' : '频道-1'
		this.controlDevice({channel:val},()=>{
			ToastManager.show(tips)
		})
	}

	// 控制音量
	changeVolume(val){
		let tips = val ? '音量 加' : '音量 减'
		this.controlDevice({volume:val},()=>{
			ToastManager.show(tips)
		})
	}

	getCircularBg(){
		if(!this.state.currentPress){
			return null
		}
		let percent = '71.71067812%'
		let rotateZ = 90 * (this.state.currentPress-1) + 'deg'
		return(
			<View style={{position:'absolute',alignItems:'center',height:'100%',width:percent,backgroundColor:'transparent',transform:[{rotateZ:rotateZ}] }}>
				<Image style={{width:'100%',height:'50%',resizeMode:'contain',marginTop:-2}} source={require('../../../images/panel/tv_volate_bg_icon.png')}/>
				<View style={{position: 'absolute',top:'12%', width:4,height:4,backgroundColor:Colors.white,borderRadius:2}}/>
			</View>
		)
	}

	getCircularButton(index,btnStyle){
		return(
			<TouchableOpacity style={{...styles.cirlBtn,...btnStyle}} 
				onPress={()=>{
					if(index == 1){
						//上
						this.controlDevice({number:20})
					}
					else if(index == 2){
						//右
						this.controlDevice({number:21})
					}
					else if(index == 3){
						//下
						this.controlDevice({number:22})
					}
					else if(index == 4){
						//左
						this.controlDevice({number:23})
					}
				}}
				onPressIn={()=>{
					this.setState({currentPress:index})
				}}
				onPressOut={()=>{
					this.setState({currentPress:null})
				}}
			>
				<View style={{width:4,height:4,backgroundColor:Colors.themeBG,borderRadius:2}}/>
			</TouchableOpacity>
		)
	}

	getCenterView(){
		return(
            <View style={styles.topWrapper}>
                <View style={styles.centerWrapper}>
				    {this.getCircularBg()}
				    <View style={styles.okBtnWrapper}>
					    <TouchableOpacity style={{padding:5}} onPress={()=>{
						    this.controlDevice({number:15})
					    }}>
						    <Text style={{color:'#ffffff',fontSize:25,fontWeight:'bold'}}>OK</Text>
					    </TouchableOpacity>
				    </View>
				    {this.getCircularButton(1,{left:'25%',height:'25%',top:0,width:'50%'})}
				    {this.getCircularButton(2,{right:0,height:'50%',top:'25%',width:'25%'})}
				    {this.getCircularButton(3,{left:'25%',height:'25%',bottom:0,width:'50%'})}
				    {this.getCircularButton(4,{left:0,height:'50%',top:'25%',width:'25%'})}
                </View>
            </View>
		)
	}

	getBottomView(){
        // let muteIcon = require('../../../images/panel/tv_jy_icon_off.png')
        // if(this.state.mute){
        //     muteIcon = require('../../../images/panel/tv_jy_icon.png')
		// }
		let muteIcon = require('../../../images/panel/tv_jy_icon.png')
		
        // let menuIcon = require('../../../images/panel/tv_menu_icon.png')
        // if(this.state.menu){
        //     menuIcon = require('../../../images/panel/tv_menu_icon.png')
		// }
		let menuIcon = require('../../../images/panel/tv_menu_icon.png')
		
        // let topboxIcon = require('../../../images/panel/tv_jdh_icon_off.png')
        // if(this.state.topbox){
        //     topboxIcon = require('../../../images/panel/tv_jdh_icon.png')
		// }
		let topboxIcon = require('../../../images/panel/tv_jdh_icon.png')
		
		let switchIcon = require('../../../images/panel/redSwitch_on.png')

		return(
			<View style={styles.bottomWrapper}>
                <View style={{flexDirection:'row',width:screenW-40}}>
                    <TouchableOpacity onPress={()=>{
						this.controlDevice({number:18})
                    }}>
                        <View style={{justifyContent:'center',alignItems:'center',width:(screenW-40)/3}}>
                            <Image source={muteIcon} style={{width:screenW/10*1.2,height:screenW/10*1.2}}></Image>
                            <Text style={{marginTop:10,color:'#333333',fontSize:15}}>静音</Text>
                        </View>
					</TouchableOpacity>
                    <TouchableOpacity onPress={()=>{
						this.controlDevice({number:13})
                    }}>
                        <View style={{justifyContent:'center',alignItems:'center',width:(screenW-40)/3}}>
                            <Image source={menuIcon} style={{width:screenW/10*1.2,height:screenW/10*1.2}}></Image>
                            <Text style={{marginTop:10,color:'#333333',fontSize:15}}>菜单</Text>
                        </View>
					</TouchableOpacity>
                    <TouchableOpacity onPress={()=>{
						this.controlDevice({number:19})
                    }}>
                        <View style={{justifyContent:'center',alignItems:'center',width:(screenW-40)/3}}>
                            <Image source={topboxIcon} style={{width:screenW/10*1.2,height:screenW/10*1.2}}></Image>
                            <Text style={{marginTop:10,color:'#333333',fontSize:15}}>机顶盒</Text>
                        </View>
					</TouchableOpacity>
                </View>
				<TouchableOpacity onPress={()=>{	
					this.controlDevice({number:10})
				}}>
					<Image style={{width:45,height:45,marginTop:20}} source={switchIcon}/>
				</TouchableOpacity>
			</View>
		)
	}

	getMainPanelPage(){
		return(
			<View style={styles.main}>
				{this.getCenterView()}
				{this.getBottomView()}
			</View>
		)
	}

	// render() {
	// 	return (
	// 		<View style={styles.container}>
	// 			{this.getHeaderView()}
	// 			<ScrollView style={styles.container}>
	// 				{this.getMainPanelPage()}
	// 			</ScrollView >
	// 		</View>
	// 	)
	// }
}

const styles = StyleSheet.create({
	main: {
        flex: 1,
        backgroundColor: Colors.themeBG,
        paddingTop: StatusBarHeight + NavigationBarHeight,
    },
    topWrapper:{
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
	bottomWrapper:{
        height:bottomControllerH + BottomSafeMargin,
        paddingBottom: BottomSafeMargin,
		width:'100%',
        backgroundColor:Colors.white,
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        justifyContent:'center',
        alignItems:'center'
	},
	cirlBtn:{
		justifyContent:'center',
		alignItems:'center',
		position:'absolute',
		zIndex:1000,
	},
	centerWrapper:{
		justifyContent:'center',
		alignItems:'center',
		backgroundColor:Colors.white,
		borderRadius:screenW*0.25,
		width:screenW*0.5,
		height:screenW*0.5
	},
	okBtnWrapper:{
		position:'absolute',
		alignItems:'center',
		justifyContent:'center',
		width:'50%',
		height:'50%',
		borderRadius:screenW*0.125,
		backgroundColor:Colors.themeBG
	}
});

export default Television



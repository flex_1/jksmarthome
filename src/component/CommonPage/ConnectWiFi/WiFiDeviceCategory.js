/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    FlatList,
    Dimensions
} from 'react-native';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import { Colors, NetUrls, NotificationKeys } from '../../../common/Constants';
import ToastManager from '../../../common/CustomComponent/ToastManager';
import SpinnerManager from '../../../common/CustomComponent/SpinnerManager';
import { postJson } from '../../../util/ApiRequest';
import { BottomSafeMargin } from '../../../util/ScreenUtil';
import { connect } from 'react-redux';
import { ColorsDark } from '../../../common/Themes';

const screenW = Dimensions.get('screen').width;

class WiFiDeviceCategory extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'选择设备类型'}/>,
            headerBackground: <HeaderBackground hiddenBorder={true}/>,
        }
	}

    constructor(props) {
        super(props);

        this.state = {
            categoryList: [],
            subTypeList: null,
            selectCategory: null
        }
    }

    componentDidMount(){
        this.requestCategoryList()
    }

    // 获取房间 列表
	async requestCategoryList() {
		try {
			let data = await postJson({
				url: NetUrls.getWifiDeviceType,
				params: {
				}
			})
            this.setState({loading: false})
			if (data.code == 0) {
                const categoryList = data.result || []
				this.setState({
                    categoryList: categoryList,
                    subTypeList: categoryList[0]?.list || [],
                    selectCategory: categoryList[0]?.id
                })
			} else {
				ToastManager.show(data.msg);
			}
		} catch (e) {
            this.setState({loading: false})
			ToastManager.show('网络错误');
		}
	}

    // 左边一级分类
    renderLeftRowItem(rowData, index){
        const Colors = this.props.themeInfo.colors
        const isOn = this.state.selectCategory == rowData.id
        const bg = isOn ? Colors.themeBaseBg : Colors.themeBg
        const fontWeight = isOn ? {fontWeight: 'bold'} : {}
        const leftIndicatorView = isOn ? <View style={styles.leftIndicator}/> : null
        
        return(
            <TouchableOpacity style={[styles.leftItem,{backgroundColor: bg}]} onPress={()=>{
                this.setState({
                    subTypeList: rowData.list,
                    selectCategory: rowData.id,
                })
            }}>
                <Text style={[styles.cateName,fontWeight,{color: Colors.themeText}]}>{rowData.name}</Text>
                {leftIndicatorView}
            </TouchableOpacity>
        )
    }

    renderLeftList(){
        return(
            <View style={{height:'100%', flex: 3}}>
                <FlatList
                    style={{flex: 1}}
                    ref={e => this.leftList = e}
                    data={this.state.categoryList}
			        contentContainerStyle={styles.scrollContentStyle}
			        removeClippedSubviews={true}
			        keyExtractor={(item, index)=> 'left_list'+index}
			        renderItem={({ item, index }) => this.renderLeftRowItem(item, index)}
			        initialNumToRender={5}
		        />
            </View>
        )
    }

    // 右边二级分类
    renderRightRowItem(rowData, index){
        const Colors = this.props.themeInfo.colors

        return(
            <TouchableOpacity style={index%2 == 0 ? styles.rightItem : styles.rightItem_2} onPress={()=>{
                if(rowData.code == '2'){
                    this.props.navigation.replace('WiFiGuide', {
                        typeCode: rowData.typeCode,
                        remarkImg: rowData.remarkImg,
                        remark: rowData.remark,
                        apImg: rowData.apImg,
                        apRemark: rowData.apRemark,
                        apName: rowData.apName || 'APHS_xxxxxxx',
                        apPassword: rowData.apPassword || '88888888',
                        apPrefix: rowData.apPrefix || "APHS_",
                        deviceName: rowData.name,
                        deviceIcon: rowData.img
                    })
                }else{
                    ToastManager.show('该类型设备将在后续版本集成，敬请期待。')
                }
            }}>
                <Image style={styles.icon} source={{uri: rowData.img}} defaultSource={require('../../../images/default.png')}/>
                <Text style={[styles.typeName,{color: Colors.themeText}]}>{rowData.name}</Text>
            </TouchableOpacity>
        )
    }

    renderRightList(){
        if(!this.state.subTypeList){
            return null
        }
        return(
            <View style={{height:'100%', flex: 7}}>
                <FlatList
                    style={{flex: 1}}
                    numColumns={2}
                    ref={e => this.rightList = e}
                    data={this.state.subTypeList}
			        showsHorizontalScrollIndicator={false}
			        contentContainerStyle={styles.scrollContentStyle}
			        removeClippedSubviews={false}
			        keyExtractor={(item, index)=> 'right_list'+index}
			        renderItem={({ item, index }) => this.renderRightRowItem(item, index)}
			        initialNumToRender={5}
		        />
            </View>
        )
    }

    renderMainView(){
        if(!this.state.categoryList){
            return (
                <View style={styles.noCotentWrapper}>
                    <Text style={styles.noContentText}>数据加载中...</Text>  
                </View>
            )
        }
        if(this.state.categoryList.length <= 0){
            return(
                <View style={styles.noCotentWrapper}>
                    <Text style={styles.noContentText}>暂无可配网的设备类型</Text>  
                </View>
            )
        }
        return(
            <View style={styles.contentWrapper}>
                {this.renderLeftList()}
                {this.renderRightList()}
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBg}]}>
                {this.renderMainView()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: Colors.white
    },
    scrollContentStyle:{
        paddingBottom: BottomSafeMargin
    },
    leftItem:{
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    leftIndicator:{
        position: 'absolute',
        height: 20,
        width: 5,
        top: 15,
        left: 0,
        backgroundColor: '#008CFF'
    },
    rightItem:{
        width: '50%',
        justifyContent: 'center',
        paddingLeft: screenW * 0.7 * 0.5 * 0.2,
        paddingRight: screenW * 0.7 * 0.5 * 0.1,
        marginBottom: 15,
        paddingVertical: 10,
    },
    rightItem_2:{
        width: '50%',
        justifyContent: 'center',
        paddingLeft: screenW * 0.7 * 0.5 * 0.1,
        paddingRight: screenW * 0.7 * 0.5 * 0.2,
        marginBottom: 10,
        paddingVertical: 8
    },
    icon:{
        width: screenW * 0.7 * 0.5 * 0.7,
        height: screenW * 0.7 * 0.5 * 0.7,
        resizeMode: 'contain'
    },
    cateName:{
        fontSize: 14
    },
    typeName:{
        marginTop: 6,
        fontSize: 15,
        textAlign: 'center'
    },
    contentWrapper:{
        flexDirection: 'row',
        flex: 1,
        marginTop: 20
    },
    noCotentWrapper:{
        flex: 1,
        justifyContent:'center',
        alignItems:'center'
    },
    noContentText:{
        fontSize: 14,
        color: Colors.themeTextLightGray
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo
    })
)(WiFiDeviceCategory)

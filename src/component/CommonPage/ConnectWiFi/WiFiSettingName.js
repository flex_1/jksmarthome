/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    Keyboard,
    TextInput,
    DeviceEventEmitter
} from 'react-native';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import { Colors, NetUrls,GetdeviceRouter,NotificationKeys } from '../../../common/Constants';
import ToastManager from '../../../common/CustomComponent/ToastManager';
import SpinnerManager from '../../../common/CustomComponent/SpinnerManager';
import { postJson } from '../../../util/ApiRequest';

class WiFiSettingName extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} disableNight={true}/>,
            headerBackground: <HeaderBackground hiddenBorder={true} disableNight={true}/>
        }
	}

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation

        this.deviceData = getParam('deviceData') || {}
        this.deviceIcon = getParam('deviceIcon')
        this.state = {
            name: this.deviceData.deviceName || ''
        }
    }

    componentDidMount(){
        
    }

    // 设置房间
    async requestSettingRoom() {
        SpinnerManager.show()
        try {
			let data = await postJson({
				url: NetUrls.saveDevice,
				params: {
					id: this.deviceData?.deviceId,
					name: this.state.name
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                const {navigate, pop, push} = this.props.navigation
				DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
                pop(5)
			    const routerName = GetdeviceRouter(this.deviceData.attributeTypeName)
                if(routerName){
                    push(routerName,{title:this.state.name, deviceData:this.deviceData})
                }

			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    renderTitleView(){
        const mainImg = this.deviceIcon ? {uri: this.deviceIcon} : require('../../../images/connectWifi/mianImg.png')
        return(
            <View style={styles.titleWrapper}>
                <Image style={styles.mainImg} source={mainImg}/>
                <Text style={styles.mainTitle}>设备命名</Text>
                <Text style={styles.subTitle}>查找设备更快捷</Text>
            </View>
        )
    }

    renderNameInput(){
        return(
            <View style={{marginTop: 58, paddingHorizontal: 16,width:'100%'}}>
                <TextInput
                    style={{width:'100%',fontSize: 18, color: '#303030'}}
                    defaultValue={this.state.name}
                    maxLength={16}
                    placeholder="请输入设备名称"
                    clearButtonMode={'while-editing'}
                    onChangeText={(text) => {
                        this.state.name = text
                    }}
                />
                <View style={{height:1,width:'100%',backgroundColor: '#F2F2F2',marginTop: 12}}/>
            </View>
        )
    }

    renderBottomView(){
        const {navigate, pop} = this.props.navigation

        return(
            <View style={styles.bottomWrapper}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.nextBtn} 
                    onPress={()=>{
                        if(!this.state.name){
                            ToastManager.show('设备名称不能为空')
                            return
                        }
                        this.requestSettingRoom()
                    }}
                >
                    <Text style={{color: '#fff', fontSize: 18}}>下一步</Text>
                </TouchableOpacity>
            </View>
        )
    }
    
    render() {
        return (
            <TouchableOpacity activeOpacity={1} style={styles.container} onPress={()=>{Keyboard.dismiss()}}>
                {this.renderTitleView()}
                {this.renderNameInput()}
                {this.renderBottomView()}
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    titleWrapper:{
        width: '100%',
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainImg:{
        width: 76,
        height: 94,
        resizeMode: 'contain'
    },
    mainTitle:{
        fontSize: 20,
        color: '#008CFF'
    },
    subTitle:{
        fontSize: 14,
        color: '#969799',
        marginTop: 4
    },
    roomWrapper:{
        width: '100%',
        marginTop: 20,
        flex: 1
    },
    bottomWrapper:{
        position: 'absolute',
        width:'100%',
        left:0, 
        bottom:0, 
        paddingBottom: 41, 
        justifyContent:'center',
        alignItems:'center'
    },
    nextBtn:{
        marginTop: 14,
        width:'80%',
        height: 49, 
        borderRadius: 24.5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#008CFF'
    },
});

export default WiFiSettingName

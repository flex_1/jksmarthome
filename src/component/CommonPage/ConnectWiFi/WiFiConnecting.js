/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    NativeModules,
    NativeEventEmitter,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    Alert,
    Animated,
    Easing,
    DeviceEventEmitter
} from 'react-native';
import HeaderBackground from '../Navigation/HeaderBackground';
import { Colors,NetUrls,NotificationKeys } from '../../../common/Constants';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';
import { postJson } from '../../../util/ApiRequest';
import ToastManager from "../../../common/CustomComponent/ToastManager";

const JKConnectWifiUtils = NativeModules.JKConnectWifiUtils;
const JKWifiManager = new NativeEventEmitter(JKConnectWifiUtils)

const kWifiConnectSuccess  = "WifiConnectSuccess"
const kWifiConnectFail = "WifiConnectFail"
const ConnectTypeChange = 'kConnectTypeChangedNoti'

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

const TotalRequestTime = 60  // 总共的轮询时间(秒)
const IntervalTime = 3  // 间隔的轮询时间(秒)

class WiFiConnecting extends Component {

    static navigationOptions = {
        header: null,
        headerBackground: <HeaderBackground hiddenBorder={true} disableNight={true}/>
    }

    constructor(props) {
        super(props);
        const {getParam, setParams} = this.props.navigation
        
        this.restReqTime = 0
        this.wifiDeviceType = getParam('wifiDeviceType')
        this.deviceIcon = getParam('deviceIcon')

        this.state = {
            connectNetworkStatus: 0,  // 0:未开始  1:进行中  2:成功
            settingNetworkStatus: 0,  // 0:未开始  1:进行中  2:成功
            bindUserStatus : 0, // 0:未开始  1:进行中  2:成功

            rotateValue: new Animated.Value(0), //旋转角度的初始值
        }

        this.playerAnimated  = Animated.timing(this.state.rotateValue,{
			toValue: 1, // 最终值 为1，这里表示最大旋转 360度
			duration: 4000,
			easing: Easing.linear,
		})
    }

    componentDidMount(){
        const {getParam, navigate} = this.props.navigation
        const ssid = getParam('ssid')
        const bssid = getParam('bssid')
        const password = getParam('password')

        // wifi 配网成功， data 为对象 {bssid: '123123123'，address： '192.168,1,1'}
        this.wifiSuccessNoti = JKWifiManager.addListener(kWifiConnectSuccess, (data)=>{
            this.setState({
                connectNetworkStatus: 2,
                settingNetworkStatus: 2,
                bindUserStatus: 1
            })
            this.restReqTime = TotalRequestTime
            this.startCount(data.bssid)
        })

        // wifi 配网失败
        this.wifiFailNoti = JKWifiManager.addListener(kWifiConnectFail, (data)=>{
            this.setState({
                connectNetworkStatus: 0,
                settingNetworkStatus: 0,
                bindUserStatus: 0
            })
            let title = '配网失败'
            let msg = '请检查网络或Wifi密码是否正确后重新进行配网。您也可以尝试使用AP配网。'
            this.handleFailMessage(title,msg,1)
        })

        JKConnectWifiUtils.startConnectWifi(ssid, bssid, password)
        this.setState({
            connectNetworkStatus: 1
        })

        this.playerAnimated.start(() => {
            this.playerAnimated = Animated.timing(this.state.rotateValue, {
                toValue: 1, //角度从0变1
                duration: 3000, //从0到1的时间
                easing: Easing.linear, //线性变化，匀速旋转
            });
            this.rotating();
        });
    }

    componentWillUnmount(){
        // 取消配网
        JKConnectWifiUtils.stopConnectWifi()
        this.wifiSuccessNoti?.remove()
        this.wifiFailNoti?.remove()
        this.interval && clearInterval(this.interval);
    }

    // 绑定设备
    async requestBindUser(bssid){
        try {
            let data = await postJson({
                url: NetUrls.bindWifiDevice,
                timeout: 2.5,
                params: {
                    typeCode: this.wifiDeviceType,
                    sname: bssid
                }
            });
            if (data.code == 0) {
                this.interval && clearInterval(this.interval);
                this.setState({
                    bindUserStatus: 2
                },()=>{
                    const {navigate, pop} = this.props.navigation
                    if(this.wifiDeviceType == '140'){
                        pop(3)
                        navigate('GroupManageList', {
                            getwayData: data.result,
                            title: data.result.name,
                            callBack: () => {
                                DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
                            }
                        })
                        navigate('GatewaySetting',{
                            title: data.result.name + '设置',
                            name: data.result.name,
                            getwayData: data.result,
                            callBack: ()=>{
                                DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
                            }
                        })
                    }else{
                        navigate('WiFiSettingRoom',{
                            deviceData : data.result,
                            deviceIcon: this.deviceIcon
                        })
                    }
                })
                DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
                ToastManager.show('设备添加成功。');
            } else if(data.code == -1){
                this.interval && clearInterval(this.interval);
                let title = '添加设备出错'
                this.handleFailMessage(title,data.msg,2)
            } else{
                this.restReqTime -= IntervalTime
            }
        } catch (e) {
            this.interval && clearInterval(this.interval);
            ToastManager.show('网络错误，请稍后重试。')
        }
    }

    // 开启定时轮询设备状态
    startCount(bssid) {
        this.interval = setInterval(() => {
            if(this.restReqTime <= 0){
                let title = '设备添加失败'
                let msg = '设备添加失败，请检查网络后重试。'
                this.handleFailMessage(title,msg,2)
                this.interval && clearInterval(this.interval);
            }else{
                this.requestBindUser(bssid)
            }
        }, IntervalTime*1000)
    }

    rotating() {
		this.state.rotateValue.setValue(0);
        this.playerAnimated.start(() => {
            this.rotating()
        })
	};

    //type: 1-配网失败  2-与服务端交互失败
    handleFailMessage(title,msg,type){
        const {navigate, pop, dispatch} = this.props.navigation

        Alert.alert(
            title,
            msg,
            [   
                { text: 'AP配网', onPress: () => {
                    DeviceEventEmitter.emit(ConnectTypeChange)
                    pop(2)
                } },
                { text: '取消', onPress: () => { pop() }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    renderHeader(){
        return(
            <TouchableOpacity style={styles.backTouch} onPress={()=>{
                this.props.navigation.pop()
            }}>
                <Image style={styles.leftImg} source={require('../../../images/back.png')}/>
            </TouchableOpacity>
        )
    }

    renderCenterWifi(){
        return(
            <View style={{width:'100%',justifyContent:'center',alignItems:'center'}}>
                <Image style={styles.wifiImg} source={require('../../../images/connectWifi/wifi.png')}/>
            </View>
        )
    }

    renderAnimateView(status){
        if(status == 2){
            return <Image source={require('../../../images/connectWifi/complete.png')} style={styles.contentImg}/>
        }

		const spin = this.state.rotateValue.interpolate({
            inputRange: [0, 1],//输入值
            outputRange: ['0deg', '360deg'] //输出值
		})
		
		return(
			<Animated.Image style={[styles.contentImg,{transform:[{"rotate": spin }]}]} source={require('../../../images/connectWifi/configing.png')}/>
		)
    }

    _renderConnectNetworkContent(){
        if(this.state.connectNetworkStatus == 0){
            return null
        }
        const textColor = this.state.connectNetworkStatus == 1 ? '#646566' : '#008CFF'
        const contentText = this.state.connectNetworkStatus == 1 ? '手机连接设备中...' : '手机连接设备成功'

        return(
            <View style={[styles.contentItem,{marginTop: 24}]}>
                {this.renderAnimateView(this.state.connectNetworkStatus)}
                <Text style={[styles.content,{color: textColor}]}>{contentText}</Text>
            </View>
        )
    }

    _renderSettingNetworkContent(){
        if(this.state.settingNetworkStatus == 0){
            return null
        }
        const textColor = this.state.settingNetworkStatus == 1 ? '#646566' : '#008CFF'
        const contentText = this.state.settingNetworkStatus == 1 ? '设备连接网络中…' : '设备连接网络成功'

        return(
            <View style={[styles.contentItem,{marginTop: 24}]}>
                {this.renderAnimateView(this.state.settingNetworkStatus)}
                <Text style={[styles.content,{color: textColor}]}>{contentText}</Text>
            </View>
        )
    }

    _renderBindUserContent(){
        if(this.state.bindUserStatus == 0){
            return null
        }
        const textColor = this.state.bindUserStatus == 1 ? '#646566' : '#008CFF'
        const contentText = this.state.bindUserStatus == 1 ? '设备绑定账户中…' : '设备添加成功'

        return(
            <View style={[styles.contentItem,{marginTop: 24}]}>
                {this.renderAnimateView(this.state.bindUserStatus)}
                <Text style={[styles.content,{color: textColor}]}>{contentText}</Text>
            </View>
        )
    }

    renderContent(){
        return(
            <View style={styles.bottomWrapper}>
                <View style={styles.contentWrapper}>
                    <Text style={styles.title}>正在为设备配置网络…</Text>
                    {this._renderConnectNetworkContent()}
                    {this._renderSettingNetworkContent()}
                    {this._renderBindUserContent()}
                </View>
            </View>
        )
    }

    render() {
        return (
            <ImageBackground style={styles.container} source={require('../../../images/connectWifi/bg.png')}>
                {this.renderHeader()}
                {this.renderCenterWifi()}
                {this.renderContent()}
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    backTouch:{
        marginTop: StatusBarHeight,
        paddingHorizontal: 16,
        height: NavigationBarHeight,
        justifyContent: 'center',
        alignItems: 'center',
        width: 42
    },
    leftImg:{
        width: 10, 
        height: 18,
        resizeMode: 'contain',
        tintColor: Colors.white
    },
    bottomWrapper:{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 0,
        bottom: screenH * 0.2 + BottomSafeMargin,
    },
    contentWrapper:{
        height: 160
    },
    wifiImg:{
        width: screenW * 0.7, 
        height: screenW * 0.7,
        resizeMode: 'contain',
        marginTop: 24
    },
    title:{
        color: '#008CFF', 
        fontSize: 20,
        fontWeight: '500'
    },
    contentImg:{
        width: 16,
        height: 16,
        resizeMode: 'contain'
    },
    contentItem:{
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    content: {
        fontSize: 14,
        marginLeft: 8
    }
});

export default WiFiConnecting

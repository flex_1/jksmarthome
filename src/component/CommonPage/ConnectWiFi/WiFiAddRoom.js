/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    TextInput,
    DeviceEventEmitter,
    Alert,
    Modal,
    Keyboard
} from 'react-native';
import { postJson, postPicture } from '../../../util/ApiRequest';
import { NetUrls, NotificationKeys,Colors } from '../../../common/Constants';
import ToastManager from '../../../common/CustomComponent/ToastManager';
import SpinnerManager from '../../../common/CustomComponent/SpinnerManager';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderRight from '../Navigation/HeaderRight';

class WiFiAddRoom extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'添加房间'} disableNight={true}/>,
            headerRight: (
                <HeaderRight disableNight={true} buttonArrays={[
                    {text:'保存',onPress:()=>{
                        navigation.getParam('addRoomBtnClick')()
                    }},
                ]}/>
            ),
            headerBackground: <HeaderBackground hiddenBorder={true} disableNight={true}/>
        }
    }

    constructor(props) {
        super(props);
        const { getParam,setParams } = this.props.navigation
        
        this.callBack = getParam('callBack')
        this.state = {
            
            avatarSource: getParam('avatarSource'),
            name: getParam('name') || '',

            roomImg: getParam('roomImg'),
            imgId: null,
            
            customFloor: getParam('floorText') || '',
        }

        setParams({
            addRoomBtnClick: ()=>{ this.requestAddRoom() }
        })
    }

    componentDidMount() {
        
    }

    componentWillUnmount() {
        
    }

    //新增 修改 房屋
    async requestAddRoom() {
        Keyboard.dismiss()
        if (!this.state.name) {
            ToastManager.show('请输入房间名称')
            return
        }
        if(this.state.customFloor == null){
            ToastManager.show('请输入楼层名称')
            return
        }
        SpinnerManager.show()
        let params = {}
        if(this.state.imgId){
            params.imgId = this.state.imgId
        }
        try {
            let data = await postJson({
                url: NetUrls.addRoom,
                params: {
                    ...params,
                    name: this.state.name,
                    floor: this.state.customFloor
                }
            })
            SpinnerManager.close()
            if (data && data.code == 0) {
                this.props.navigation.pop()
                this.callBack && this.callBack(data.result?.id, data.result?.floor, data.result?.floorText)

                DeviceEventEmitter.emit(NotificationKeys.kRoomFloorChangeNotification)
                DeviceEventEmitter.emit(NotificationKeys.kRoomInfoNotification)
                
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show(JSON.stringify(error));
        }
    }

    getPickImageView(){
        
        const { navigate } = this.props.navigation

        let centerView = (
            <View style={[styles.headerPick,{backgroundColor: Colors.white}]}>
                <Image style={{ width: 70, height: 70 }} source={require('../../../images/cameral_icon.png')} />
                <Text style={{ fontSize: 14, color: '#1E1E1E', marginTop: 10 }}>选择图片</Text>
            </View>
        )
        if (this.state.roomImg) {
            centerView = (
                <View style={[styles.headerPick,{backgroundColor: Colors.white}]}>
                    <Image style={styles.roomImg}  source={{uri: this.state.roomImg}} />
                </View>
            )
        }
        return(
            <View style={{alignItems:'center',marginTop: 20}}>
                <TouchableOpacity onPress={()=>{
                    navigate('SelectDefaultImage',{callBack:(data)=>{
                        let roomImg = data.dayImg
                        this.setState({
                            roomImg: roomImg,
                            imgId: data.id,
                            name: this.state.name || data.name
                        })
                    }})
                }}>
                    {centerView}
                </TouchableOpacity>
            </View>
        )
    }

    // 自定义楼层
    renderFloorInput(){
        let defaultFloor = this.state.customFloor ? this.state.customFloor+'' : ''
        if(parseInt(defaultFloor) <= 0){
            defaultFloor = ''
        }

        return(
            <View style={[styles.inputWrapper,{marginTop: 15,backgroundColor: Colors.white}]}>
                <Text style={{ marginLeft: 20, fontSize: 15, color: Colors.themeTextBlack }}>楼层名称</Text>
                <TextInput
                    defaultValue={defaultFloor}
                    style={[styles.input,{color: Colors.themeTextBlack}]}
                    placeholder={'请输入楼层名称'}
                    // maxLength={2}
                    placeholderTextColor={Colors.themeBGInactive}
                    onChangeText={(text) => {
                        this.state.customFloor = text
                    }}
                    returnKeyType={'done'}
                />
            </View>
        )
    }

    // 房间名称
    getInfoTextInput() {
        
        return (
            <View style={{ marginTop: 25, paddingVertical: 10, paddingHorizontal: 16 }}>
                <View style={[styles.inputWrapper,{backgroundColor: Colors.white}]}>
                    <Text style={{ marginLeft: 20, fontSize: 15, color: Colors.themeTextBlack }}>房间名称</Text>
                    <TextInput
                        style={[styles.input,{color: '#1E1E1E'}]}
                        placeholder={'请输入房间名(不超过8个字)'}
                        defaultValue={this.state.name}
                        placeholderTextColor={Colors.themeBGInactive}
                        onChangeText={(text) => {
                            this.state.name = text
                        }}
                        returnKeyType={'done'}
                    />
                </View>
                {this.renderFloorInput()}
            </View>
        )
    }

    render() {

        return (
            <View style={styles.container}>
                <ScrollView keyboardDismissMode={'on-drag'}>
                    {this.getPickImageView()}
                    {this.getInfoTextInput()}
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.themBGLightGray,
        flex: 1
    },
    headerPick: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
        borderRadius: 4
    },
    headerPickTouch: {
        marginTop: 23,
        paddingHorizontal: 16,
    },
    inputWrapper: {
        width: '100%',
        height: 55,
        borderRadius: 5,
        alignItems: 'center',
        flexDirection: 'row'
    },
    input: {
        flex: 1,
        height: 40,
        marginLeft: 5,
        marginRight: 10,
        textAlign: 'right',
        paddingRight: 16
    },
    floorTouch: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: '90%',
        flex: 1,
        paddingRight: 10
    },
    roomImg:{
        width: 50,
        height: 50,
        resizeMode: 'contain'
    },
});

export default WiFiAddRoom

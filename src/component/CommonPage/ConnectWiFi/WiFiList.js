/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    FlatList,
    ImageBackground,
    Dimensions,
    Image,
    TouchableOpacity,
    Alert
} from 'react-native';
import HeaderBackground from '../Navigation/HeaderBackground';
import { Colors,LocalStorageKeys } from '../../../common/Constants';
import WifiManager from "react-native-wifi-reborn";
import HeaderLeft from '../Navigation/HeaderLeft';
import AsyncStorage from '@react-native-community/async-storage';
import SpinnerManager from '../../../common/CustomComponent/SpinnerManager';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;

class WiFiList extends Component {

    static navigationOptions = ({ navigation }) => {
        return{
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'选择路由器'}/>,
            headerBackground: <HeaderBackground hiddenBorder={true} />
        }
    }

    constructor(props) {
        super(props);
        const {getParam, setParams} = this.props.navigation
        
        this.wifiCallback = getParam('wifiCallback')
        this.state = {
            wifiList: [],
            passwordDict: {}
        }
    }

    componentDidMount(){
        this.loadWifiList()
        this.loadLocalWifiNamePassword()
    }

    componentWillUnmount(){
        
    }

    loadWifiList(){
        WifiManager.loadWifiList().then(
            wifiList => {
                this.setState({
                    wifiList: wifiList
                })
                console.log(wifiList);
            },
            error => {

            }
        )
    }

    async loadLocalWifiNamePassword(){
        const passwordString = await AsyncStorage.getItem(LocalStorageKeys.kWifiSsidPassword)
        try {
            let passwordDict = JSON.parse(passwordString)
            this.setState({
                passwordDict: passwordDict
            })
        } catch (error) {
            
        }
    }

    renderTitle(){
        return(
            <View style={styles.titleWrapper}>
                <Text style={styles.title}>选择WiFi网络</Text>
                <View style={styles.split}/>
            </View>
        )
    }

    renderRowItem(rowData, index){
        let wifiLevelIcon = require('../../../images/connectWifi/wifi_strong.png')
        if(rowData.level > -50){
            wifiLevelIcon = require('../../../images/connectWifi/wifi_strong.png')
        }else if(rowData.level <= -50 && rowData.level >= -70){
            wifiLevelIcon = require('../../../images/connectWifi/wifi_middle.png')
        }else{
            wifiLevelIcon = require('../../../images/connectWifi/wifi_weak.png')
        }

        let lockView = <Image style={styles.lock} source={require('../../../images/connectWifi/wifi_lock.png')}/>
        if(this.state.passwordDict[rowData.SSID]){
            lockView = null
        }

        return(
            <TouchableOpacity activeOpacity={0.7} style={styles.item} onPress={()=>{
                this.props.navigation.pop()
                this.wifiCallback(rowData.SSID, rowData.BSSID)
            }}>
                <Text style={styles.itemTitle}>{rowData.SSID}</Text>
                {lockView}
                <Image style={styles.lock} source={wifiLevelIcon}/>
            </TouchableOpacity>
        )
    }

    renderList(){
        return(
            <FlatList
                style={styles.list}
			    data={this.state.wifiList}
			    removeClippedSubviews={false}
			    keyExtractor={(item, index)=> 'wifi_list_'+index}
			    renderItem={({ item, index }) => this.renderRowItem(item, index)}
			    initialNumToRender={5}
		    />
        )
    }

    renderBottomBtn(){
        return(
            <View style={styles.bottomWrapper}>
                <TouchableOpacity style={styles.bottomBtn} onPress={()=>{
                    SpinnerManager.show()
                    WifiManager.reScanAndLoadWifiList().then(
                        wifiList => {
                            SpinnerManager.close()
                            this.setState({
                                wifiList: wifiList
                            })
                            console.log(wifiList);
                        },
                        error => {
                            SpinnerManager.close()
                        }
                    )
                }}>
                    <Text style={styles.bottomText}>重新扫描WiFi</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderTitle()}
                {this.renderList()}
                {this.renderBottomBtn()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    item:{
        paddingVertical: 10,
        marginBottom: 13,
        paddingHorizontal: 16,
        flexDirection: 'row',
        alignItems: 'center'
    },
    list:{
        marginTop: 10,
        flex: 1
    },
    titleWrapper:{
        marginTop: 28, 
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal: 16
    },
    title:{
        color: '#008CFF',
        fontSize: 20
    },
    split:{
        width: '100%',
        height: 1,
        backgroundColor: '#F2F2F2',
        marginTop: 16
    },
    bottomWrapper:{
        paddingBottom: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    bottomBtn:{
        paddingHorizontal: 16,
        paddingVertical: 20
    },
    bottomText:{
        fontSize: 16,
        color: '#008CFF'
    },
    itemTitle:{
        color: '#303030',
        fontSize: 14,
        flex: 1
    },
    lock:{
        width: 16,
        height: 16,
        resizeMode: 'contain',
        marginLeft: 9
    }
});

export default WiFiList

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    NativeModules,
    PermissionsAndroid,
    Image,
    Dimensions,
    DeviceEventEmitter,
    StatusBar
} from 'react-native';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import { Colors, NotificationKeys } from '../../../common/Constants';
import ToastManager from '../../../common/CustomComponent/ToastManager';
import DraggableManager from '../../../common/CustomComponent/DraggableManager';
import { connect } from 'react-redux';
import { ScrollView } from 'react-native';

const JKConnectWifiUtils = NativeModules.JKConnectWifiUtils;
const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const ConnectTypeChange = 'kConnectTypeChangedNoti'

class WiFiGuide extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'设置网络'} disableNight={true}/>,
            headerBackground: <HeaderBackground hiddenBorder={true} disableNight={true}/>
        }
	}

    constructor(props) {
        super(props);
        const {getParam, setParams} = props.navigation

        this.wifiDeviceType = getParam('typeCode')
        this.remarkImg = getParam('remarkImg')
        this.remark = getParam('remark')
        this.apImg = getParam('apImg')
        this.apRemark = getParam('apRemark')
        this.apName = getParam('apName')
        this.apPassword = getParam('apPassword')
        this.apPrefix = getParam('apPrefix')
        this.deviceName = getParam('deviceName')
        this.deviceIcon = getParam('deviceIcon')

        this.state = {
            accessible: false,
            connectWifiType: 1,  //1.一键配网  2.AP配网 
        }
    }

    componentDidMount(){
        if(this.props.themeInfo.isDark){
            StatusBar.setBarStyle('dark-content')
        }
        DeviceEventEmitter.emit(NotificationKeys.kWebsocketCloseNotification)
        DraggableManager.hide()
        // 一键配网变换为AP配网
        this.connectNoti = DeviceEventEmitter.addListener(ConnectTypeChange, ()=>{
            this.setState({
                connectWifiType: 2
            })
        })
    }

    componentWillUnmount(){
        if(this.props.themeInfo.isDark){
            StatusBar.setBarStyle('light-content')
        }
        DeviceEventEmitter.emit(NotificationKeys.kWebsocketReconnectNotification)
        DraggableManager.show()
        this.connectNoti.remove()
    }

    requestAuth(callBack){
        // 请求权限（否则无法配网）
        if(Platform.OS == 'ios'){
            JKConnectWifiUtils.requestAuth((authAccessible)=>{
                if(authAccessible == 'true'){
                    callBack && callBack()
                }
            })
        }else{
            this.requesAndroidAuth(callBack)
        }
    }

    // 安卓权限申请
    async requesAndroidAuth(callBack){
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                title: '配网功能需要开启定位权限',
                message: '小萨管家需要您开启定位权限后才能使用配网功能。是否同意开启定位权限？',
                buttonNegative: '不同意',
                buttonPositive: '同意',
            },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            // You can now use react-native-wifi-reborn
            callBack && callBack()
        } else {
            // Permission denied
        }
    }

    renderSegement(){
        return(
            <View style={styles.segementWrapper}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={this.state.connectWifiType == 1 ? styles.selectItem : styles.unselectItem}
                    onPress={()=>{
                        this.setState({
                            connectWifiType: 1
                        })
                    }}
                >
                    <Text style={this.state.connectWifiType == 1 ? styles.selectText : styles.unselectText}>一键配网</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={this.state.connectWifiType == 2 ? styles.selectItem : styles.unselectItem}
                    onPress={()=>{
                        this.setState({
                            connectWifiType: 2
                        })
                    }}
                >
                    <Text style={this.state.connectWifiType == 2 ? styles.selectText : styles.unselectText}>AP配网</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderMainView(){
        const mainImg = this.state.connectWifiType == 1 ? this.remarkImg : this.apImg
        const subTips = this.state.connectWifiType == 1 ? this.remark : this.apRemark

        return (
            <View style={styles.mianView}>
                <Image style={styles.mainImg} source={{uri: mainImg}}/>
                <View style={styles.warnningTitle}>
                    <Image style={styles.warrningImg} source={require('../../../images/connectWifi/warning.png')}/>
                    <Text style={styles.warningText}>注意</Text>
                </View>
                <View style={styles.tipsWrapper}>
                    <Text style={styles.tipsText}>{subTips}</Text>
                </View>
            </View>
        )
    }

    renderNextStepButton(){
        const { navigate } = this.props.navigation
        let btnBg = this.state.accessible ? '#008CFF' : '#EEEEEE'
        let textColor = this.state.accessible ? '#FFF' : '#999999'

        return(
            <TouchableOpacity
                activeOpacity={0.7}
                style={[styles.nextBtn,{backgroundColor: btnBg}]} 
                onPress={()=>{
                    if(!this.state.accessible){
                        ToastManager.show('请先确认上述操作。')
                        return
                    }
                    navigate('WiFiPassword',{
                        wifiDeviceType: this.wifiDeviceType,
                        connectWifiType: this.state.connectWifiType,    // 1.一键配网   2.Ap配网
                        title: this.state.connectWifiType == 1 ? '一键配网' : 'AP配网',
                        apName: this.apName,
                        apPassword: this.apPassword,
                        apPrefix: this.apPrefix,
                        deviceName: this.deviceName,
                        deviceIcon: this.deviceIcon
                    })
                }}
            >
                <Text style={{color: textColor, fontSize: 18}}>下一步</Text>
            </TouchableOpacity>
        )
    }

    renderBottomView(){
        const selecImg = this.state.accessible ? require('../../../images/connectWifi/selected.png')
        : require('../../../images/connectWifi/unselected.png')

        return(
            <View style={styles.bottomWrapper}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.bottomTipsWrapper}
                    onPress={()=>{
                        this.requestAuth(()=>{
                            this.setState({
                                accessible: !this.state.accessible
                            })
                        })
                    }}
                >
                    <Image style={styles.selectedImg} source={selecImg}/>
                    <Text style={styles.bottomTipsText}>已确认上述操作</Text>
                </TouchableOpacity>
                {this.renderNextStepButton()}
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderSegement()}
                <ScrollView>
                    {this.renderMainView()}
                    {this.renderBottomView()}
                </ScrollView>
            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: Colors.white, 
        flex: 1
    },
    mianView:{
        marginTop: 20,
        alignItems: 'center',
    },
    title:{
        fontSize: 20,
        color: '#008CFF'
    },
    mainImg:{
        width: screenW * 0.85 ,
        height: screenW * 0.85 * 1.1,
        resizeMode: 'contain'
    },
    warnningTitle:{
        marginTop: 15,
        width: screenW * 0.55,
        flexDirection: 'row',
        alignItems: 'center'
    },
    warningText:{
        fontSize: 12,
        lineHeight: 17,
        color: '#C60000',
        marginLeft: 5
    },
    warrningImg:{
        width:12, 
        height:12, 
        resizeMode:'contain'
    },
    tipsWrapper:{
        width: screenW * 0.55
    },
    tipsText:{
        lineHeight: 17,
        fontSize: 12,
        color: '#969799'
    },
    nextBtn:{
        marginTop: 14,
        width:'80%',
        height: 49, 
        borderRadius: 24.5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    bottomWrapper:{
        marginTop: screenH * 0.06,
        width:'100%',
        paddingBottom: 41, 
        justifyContent:'center',
        alignItems:'center'
    },
    bottomTipsWrapper:{
        flexDirection: 'row', 
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical:10
    },
    selectedImg:{
        width: 16,
        height:16,
        resizeMode: 'contain'
    },
    bottomTipsText:{
        marginLeft: 7, 
        fontSize: 14, 
        color: '#303030'
    },
    apBtnWrapper:{
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    apTouch:{
        paddingVertical: 10,
        paddingHorizontal: 15
    },
    apConnectText:{
        fontSize: 15,
        fontWeight: '500',
        color: '#008CFF'
    },
    segementWrapper:{
        width: 240,
        height: 34,
        backgroundColor: 'rgba(0,0,0,0.08)',
        borderRadius: 8,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 3,
        paddingVertical: 2,
        marginLeft: (screenW - 240)/2,
        marginTop: 20
    },
    selectItem:{
        width:117,
        height: '100%',
        backgroundColor: Colors.white,
        borderRadius: 7,
        justifyContent: 'center',
        alignItems: 'center'
    },
    selectText:{
        fontSize: 16,
        fontWeight: '500',
        color: '#242424'
    },
    unselectItem:{
        width:117,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    unselectText:{
        fontSize: 16,
        fontWeight: '400',
        color: '#9A9A9A'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo
    })
)(WiFiGuide)

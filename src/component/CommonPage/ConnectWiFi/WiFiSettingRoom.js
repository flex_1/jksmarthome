/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    FlatList,
    ScrollView,
    DeviceEventEmitter
} from 'react-native';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import { Colors, NetUrls, NotificationKeys } from '../../../common/Constants';
import ToastManager from '../../../common/CustomComponent/ToastManager';
import SpinnerManager from '../../../common/CustomComponent/SpinnerManager';
import { postJson } from '../../../util/ApiRequest';

class WiFiSettingRoom extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} disableNight={true}/>,
            headerBackground: <HeaderBackground hiddenBorder={true} disableNight={true}/>,
            gesturesEnabled: false
        }
	}

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation

        this.deviceData = getParam('deviceData') || {}
        this.deviceIcon = getParam('deviceIcon')
        this.state = {
            isCollect: false,
            floorData: null,
            roomData: [],

            selectFloor: null,
            selectRoom: null,
            selectFloorText: ''
        }
    }

    componentDidMount(){
        this.requestRoomList()
    }

    // 获取房间 列表
	async requestRoomList() {
		try {
			let data = await postJson({
				url: NetUrls.roomList,
				params: {
                    type: 2
				}
			})
			if (data.code == 0) {
				const floorData = data.result || []

                if(!this.state.selectFloor){
                    this.setState({
                        floorData: floorData,
                        selectFloor: floorData[0]?.floor,
                        selectFloorText: floorData[0]?.floorText,
                        roomData: floorData[0]?.listRoom
                    })
                }else{
                    let selectIndex = 0
                    for (let index = 0; index < floorData.length; index++) {
                        const floor = floorData[index];
                        if(this.state.selectFloor == floor.floor){
                            selectIndex = index
                            break
                        }
                    }
                    this.setState({
                        floorData: floorData,
                        roomData: floorData[selectIndex]?.listRoom
                    },()=>{
                        setTimeout(() => {
                            this.roomScrollHead?.scrollToIndex({index: selectIndex,viewPosition:0.5})
                        }, 500);
                    })
                }
			} else {
				ToastManager.show(data.msg);
			}
		} catch (e) {
			ToastManager.show('网络错误');
		}
	}

    // 设置房间
    async requestSettingRoom() {
        SpinnerManager.show()
        try {
			let data = await postJson({
				url: NetUrls.saveDevice,
				params: {
					id: this.deviceData?.deviceId,
					roomId: this.state.selectRoom
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				DeviceEventEmitter.emit(NotificationKeys.kDeviceFloorNotification)
				DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
			    this.props.navigation.navigate('WiFiSettingName',{
                    deviceData: this.deviceData,
                    deviceIcon: this.deviceIcon
                })
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    // 收藏设备
    async requestCollect(target, callBack){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.collect,
				params: {
                    ids: this.deviceData?.deviceId,
                    type: 1,
                    isAdd: target
                }
            });
            SpinnerManager.close()
			if (data.code == 0) {
                callBack && callBack()
                DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
	}

    // 添加房屋
    addRoom(){
        this.props.navigation.navigate('WiFiAddRoom',{
            floorText: this.state.selectFloorText,
            callBack: (roomId, floor, floorText)=>{
                this.setState({
                    selectFloor: floor,
                    selectFloorText: floorText,
                    selectRoom: roomId
                },()=>{
                    this.requestRoomList()
                })
            }
        })
    }

    renderTitleView(){
        const mainImg = this.deviceIcon ? {uri: this.deviceIcon} : require('../../../images/connectWifi/mianImg.png')
        return(
            <View style={styles.titleWrapper}>
                <Image style={styles.mainImg} source={mainImg}/>
                <Text style={styles.mainTitle}>选择房间</Text>
                <Text style={styles.subTitle}>管理设备更方便</Text>
            </View>
        )
    }

    renderRowItem(rowData, index){
        let underLine = null
        let activeText = {color: Colors.themeTextLightGray}

		if(this.state.selectFloor == rowData.floor){
			underLine = <View style={styles.underLine} />
			activeText = {color: Colors.themeTextBlack,fontSize:18}
		}

		return (
			<TouchableOpacity activeOpacity={0.7} style={styles.floorTouch} onPress={()=>{
                this.setState({
                    selectFloor: this.state.floorData[index]?.floor,
                    selectFloorText: this.state.floorData[index]?.floorText,
                    roomData: this.state.floorData[index]?.listRoom
                })
			}}>
				<Text style={[styles.floorText,activeText]}>{rowData.floorText}</Text>
				{underLine}
			</TouchableOpacity>
		)
    }

    renderNoContent(){
        return(
            <View style={styles.noContentWrapper}>
                <Text style={styles.noContentText}>暂无房屋，去添加</Text>
                <TouchableOpacity activeOpacity={0.7} style={[styles.roomItem,styles.dashBorder,{width: '40%'}]} onPress={()=>{
                    this.addRoom()
                }}>
                    <Text style={{fontSize: 14, color: '#969799'}}>+</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderRoomList(){
        if(!this.state.roomData){
            return this.renderNoContent()
        }
        let roomData = JSON.parse(JSON.stringify(this.state.roomData))
        roomData.push({id: 0})

        const roomList = roomData.map((value, index)=>{
            let btnText = value.name
            let btnTextColor = '#303030'
            let btnStyle = styles.roomItem

            if(value.id == 0){
                btnText = '+'
                btnTextColor = '#969799'
                btnStyle = [styles.roomItem,styles.dashBorder]
            }else if(value.id == this.state.selectRoom){
                btnTextColor = '#008CFF'
                btnStyle = [styles.roomItem,{backgroundColor: 'rgba(0, 140, 255, 0.1)'}]
            }

            return(
                <View key={'room_item_'+index} style={styles.roomBtnWrapper}>
                    <TouchableOpacity activeOpacity={0.7} style={btnStyle} onPress={()=>{
                        if(value.id == 0){
                            this.addRoom()
                            return
                        }
                        this.setState({
                            selectRoom: value.id
                        })
                    }}>
                        <Text style={{fontSize: 14, color: btnTextColor}}>{btnText}</Text>
                    </TouchableOpacity>
                </View>
            )
        })

        return(
            <ScrollView>
                <View style={styles.roomItemWrapper}>
                    {roomList}
                </View>
            </ScrollView>
        )
    }

    renderRoomsChooseView(){
        if(!this.state.floorData){
            return null
        }

        return(
            <View style={styles.roomWrapper}>
                <View style={{height: 50,width:'100%'}}>
                    <FlatList
                        ref={e => this.roomScrollHead = e}
                        style={{marginRight: 10}}
			            data={this.state.floorData}
			            horizontal={true}
			            showsHorizontalScrollIndicator={false}
			            contentContainerStyle={styles.headScrollContainer}
			            removeClippedSubviews={false}
			            keyExtractor={(item, index)=> 'room_header'+index}
			            renderItem={({ item, index }) => this.renderRowItem(item, index)}
			            initialNumToRender={5}
		            />
                </View>
                {this.renderRoomList()}
            </View>
        )
    }

    renderBottomView(){
        const switchImg = this.state.isCollect ? require('../../../images/connectWifi/switchOn.png')
        : require('../../../images/connectWifi/switchOff.png')
        const {navigate} = this.props.navigation

        return(
            <View style={styles.bottomWrapper}>
                <View style={styles.bottomTipsWrapper}>
                    <Text style={styles.bottomTips}>设为首页常用设备</Text>
                    <TouchableOpacity activeOpacity={0.7} style={styles.btnWrapper} onPress={()=>{
                        let target = !this.state.isCollect
                        this.requestCollect(target, ()=>{
                            this.setState({
                                isCollect: target
                            })
                        })
                    }}>
                        <Image style={styles.switchImg} source={switchImg}/>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.nextBtn} 
                    onPress={()=>{
                        if(this.state.selectRoom){
                            this.requestSettingRoom()
                        }else{
                            navigate('WiFiSettingName',{
                                deviceData: this.deviceData,
                                deviceIcon: this.deviceIcon
                            })
                        }
                    }}
                >
                    <Text style={{color: '#fff', fontSize: 18}}>下一步</Text>
                </TouchableOpacity>
            </View>
        )
    }
    
    render() {
        return (
            <View style={styles.container}>
                {this.renderTitleView()}
                {this.renderRoomsChooseView()}
                {this.renderBottomView()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    titleWrapper:{
        width: '100%',
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainImg:{
        width: 76,
        height: 94,
        resizeMode: 'contain'
    },
    mainTitle:{
        fontSize: 20,
        color: '#008CFF'
    },
    subTitle:{
        fontSize: 14,
        color: '#969799',
        marginTop: 4
    },
    roomWrapper:{
        width: '100%',
        marginTop: 20,
        flex: 1
    },
    bottomWrapper:{
        width: '100%',
        paddingBottom: 41
    },
    bottomTipsWrapper:{
        flexDirection: 'row',
        paddingLeft: 16,
        alignItems: 'center'
    },
    nextBtn:{
        marginTop: 20,
        
        height: 49, 
        borderRadius: 24.5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#008CFF',
        marginHorizontal: '10%'
    },
    btnWrapper:{
        paddingHorizontal: 16,
        paddingVertical: 5
    },
    bottomTips:{
        fontSize: 16,
        color: '#303030',
        flex: 1
    },
    switchImg:{
        width: 40,
        height: 26,
        resizeMode: 'contain'
    },
    headScrollContainer: {
		alignItems: 'center',
		paddingLeft: 10,
		paddingRight:20
	},
    underLine: {
		width: 12,
		height: 3,
		backgroundColor: '#008CFF',
		borderRadius: 0,
		marginTop: 5
	},
    floorTouch: {
		alignItems: 'center',
		paddingHorizontal: 12,
		marginRight: 5
	},
    floorText: {
		fontSize: 15,
		fontWeight: 'bold'
	},
    roomItemWrapper:{
        paddingVertical: 15,
        flexDirection: 'row', 
        flexWrap: 'wrap', 
        paddingLeft: 16
    },
    roomItem:{
        width: '100%',
        height: 40,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: '#F5F5F5',
        borderRadius: 20
    },
    roomBtnWrapper:{
        width: '33%',
        paddingRight: 16,
        marginBottom: 16,
    },
    dashBorder:{
        borderColor: '#C8C9CC',
        borderWidth:1,
        borderStyle:'dashed'
    },
    noContentWrapper:{
        flex: 1,
        justifyContent:'center',
        alignItems:'center'
    },
    noContentText:{
        color: Colors.themeTextLightGray, 
        fontSize:14,
        marginBottom: 15
    }
});

export default WiFiSettingRoom

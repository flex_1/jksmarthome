import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    Alert,
    TextInput,
    Dimensions,
    NativeModules,
    ImageBackground,
    Animated,
    Easing,
} from 'react-native';
import { NotificationKeys,NetUrls,Colors } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import { postJson } from "../../../util/ApiRequest";
import HeaderBackground from '../Navigation/HeaderBackground';
import dgram from 'react-native-udp'
import { Buffer } from 'buffer';
import { StatusBarHeight, NavigationBarHeight,BottomSafeMargin } from '../../../util/ScreenUtil';

const { StringDecoder } = require('string_decoder');
const decoder = new StringDecoder('utf8');
const screenH = Dimensions.get('window').height;
const screenW = Dimensions.get('screen').width;

const TimeCount = 60  // 超时时间
const RemotePort = 8266  // 目标端口
const RemoteHost = '192.168.4.1'  //目标ip   // .4.1
const LocalPort = 8266   // 本地端口
const IntervalTime = 3   //每隔几（秒），发送一次

const TotalRequestTime = 60  // 总共的轮询时间(秒)
const RequestInterval = 3  // 间隔的轮询时间(秒)

class WiFiUDPConnecting extends Component {

    static navigationOptions = {
        header: null,
        headerBackground: <HeaderBackground hiddenBorder={true} disableNight={true}/>
    }

	constructor(props) {
        super(props);
        const { getParam } = props.navigation
        
        this.socket = null

        this.ssid = getParam('ssid')
        this.password = getParam('password')
        this.wifiDeviceType = getParam('wifiDeviceType')
        this.deviceIcon = getParam('deviceIcon')
        this.restReqTime = 0

		this.state = {
            remarkText: null,
            recieveContent: '',
            udpAvailable: false,
            isConnectting: false,
            timer: 0,
            connectStatus: 0,  //0: 休闲  1:正在连接   2:连接成功  3:连接超时(失败)

            connectNetworkStatus: 0,  // 0:未开始  1:进行中  2:成功
            settingNetworkStatus: 0,  // 0:未开始  1:进行中  2:成功
            bindUserStatus : 0, // 0:未开始  1:进行中  2:成功,

            rotateValue: new Animated.Value(0), //旋转角度的初始值
		};

        this.playerAnimated  = Animated.timing(this.state.rotateValue,{
			toValue: 1, // 最终值 为1，这里表示最大旋转 360度
			duration: 4000,
			easing: Easing.linear,
		})
	}

	componentDidMount() {
        this.startConnect()

        this.playerAnimated.start(() => {
            this.playerAnimated = Animated.timing(this.state.rotateValue, {
                toValue: 1, //角度从0变1
                duration: 3000, //从0到1的时间
                easing: Easing.linear, //线性变化，匀速旋转
            });
            this.rotating();
        });
    }

    componentWillUnmount() {
        this.sendInterVal && clearInterval(this.sendInterVal)
        this.requestInterval && clearInterval(this.requestInterval)
        this.socket?.close()
    }

    // 绑定设备(服务端交互层)
    async requestBindUser(mac){
        try {
            let data = await postJson({
                url: NetUrls.bindWifiDevice,
                timeout: 2.5,
                params: {
                    typeCode: this.wifiDeviceType,
                    sname: mac
                }
            });
            this.setState({
                settingNetworkStatus: 2,
                bindUserStatus: 1
            })
            if (data.code == 0) {
                this.requestInterval && clearInterval(this.requestInterval);
                this.setState({
                    bindUserStatus: 2,
                },()=>{
                    const {navigate, pop, replace} = this.props.navigation
                    if(this.wifiDeviceType == '140'){
                        pop(3)
                        navigate('GroupManageList', {
                            getwayData: data.result,
                            title: data.result.name,
                            callBack: () => {
                                DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
                            }
                        })
                        navigate('GatewaySetting',{
                            title: data.result.name + '设置',
                            name: data.result.name,
                            getwayData: data.result,
                            callBack: ()=>{
                                DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
                            }
                        })
                    }else{
                        replace('WiFiSettingRoom',{
                            deviceData : data.result,
                            deviceIcon: this.deviceIcon
                        })
                    }
                })
                DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
                ToastManager.show('设备添加成功。');
            } else if(data.code == -1){
                this.requestInterval && clearInterval(this.requestInterval);
                let title = '添加设备出错'
                this.handleFailMessage(title,data.msg,2)
            } else{
                this.restReqTime -= RequestInterval
            }
        } catch (e) {
            // 这里可能出现无网络的状况
            this.restReqTime -= RequestInterval
        }
    }

    //查询与绑定(服务端交互层)
    startRequestCount(mac) {
        this.requestInterval = setInterval(() => {
            if(this.restReqTime <= 0){
                let title = '设备添加失败'
                let msg = '设备添加失败，请检查网络后重试。'
                this.handleFailMessage(title,msg,2)
                this.requestInterval && clearInterval(this.requestInterval);
            }else{
                this.requestBindUser(mac)
            }
        }, IntervalTime*1000)
    }

    //开始连接(UDP层)
    startConnect(){
        if(this.state.isConnectting){
            return
        }
        this.state.isConnectting = true
        
        this.socket = dgram.createSocket('udp4')
        
        // 绑定端口时失败
        this.socket.on('error',(err)=>{
            this.sendInterVal && clearInterval(this.sendInterVal)
            this.requestInterval && clearInterval(this.requestInterval)
            this.socket?.close()

            Alert.alert(
                '提示',
                '配网过程出错了，请稍后重试。',
                [
                    { text: '知道了', onPress: () => {
                        this.props.navigation.pop()
                    }, style: 'cancel' },
                ],
                { cancelable: false }
            )
        })
        this.socket.once('listening', ()=>{
            this.setState({
                udpAvailable: true,
                connectStatus: 1,
                connectNetworkStatus: 1
            })

            this.sendInterVal = setInterval(() => {
                // 60秒无回应显示超时
                if(this.state.timer >= TimeCount){
                    this.showTimeOut()
                    return
                }
                this.state.timer += IntervalTime;
                this.sendMessage()
            }, IntervalTime*1000);
        })
        this.socket.on('message', (msg, rinfo) => {
            this.recieveMessage(msg, rinfo)
        })
        this.socket.bind(LocalPort)
    }
    
    // 发送消息
    sendMessage(){
        if(!this.state.udpAvailable){
            ToastManager.show('Udp初始化未完成。')
            return
        }
        try {
            const sendMsg = {
                "cmdType": 1,
                "ssid": this.ssid,
                "password": this.password,
                "token":"b266c906a96f4afffbc396618a8ed547",
                "topic":""
            } 
            const message = Buffer.from(JSON.stringify(sendMsg));
            this.socket?.send(message, 0, undefined, RemotePort, RemoteHost, (err) => {
                if(err){
                    ToastManager.show('数据发送出错。')
                }
                // if (err) {
                //     this.setState({
                //         recieveContent: this.state.recieveContent + '发送数据时出错: ' + JSON.stringify(err) + '\r\n'
                //     })
                // }
            })
        } catch (error) {
            ToastManager.show('数据出错。')
            // this.setState({
            //     recieveContent: this.state.recieveContent + '数据出错。' + '\r\n'
            // })
        }
    }

    //接收消息
    recieveMessage(msg, rinfo){
        console.log('Message received', msg)

        try {
            const bufferMessage = Buffer.from(msg);
            const message = decoder.write(bufferMessage)
            this.setState({
                recieveContent: this.state.recieveContent + message + '\r\n'
            })

            const res = JSON.parse(message)
            if(res.result == 0){
                this.showTimeOut()
            }else if(res.result == 2){
                this.showSuccess(res.Mac)
            }else if(res.result == 3){
                this.showSuccess(res.Mac)
            }
        } catch (error) {
            ToastManager.show('数据解析出错')
        }
    }

    // 显示超时
    showTimeOut(){
        this.setState({
            connectStatus: 3
        })
        this.state.isConnectting = false
        this.state.timer = 0
        this.socket?.close()
        this.sendInterVal && clearInterval(this.sendInterVal);

        this.handleFailMessage('提示','连接设备超时,请检查Wifi密码是否正确后重试。',1)
    }

    //type: 1-配网失败  2-与服务端交互失败
    handleFailMessage(title,msg,type){
        const {pop} = this.props.navigation
        Alert.alert(
            title,
            msg,
            [   
                { text: '确定', onPress: () => { pop() }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    //显示成功
    showSuccess(mac){
        this.setState({
            connectStatus: 2,
            connectNetworkStatus: 2,
            settingNetworkStatus: 1
        })
        this.state.isConnectting = false
        this.state.timer = 0
        this.socket?.close()
        this.sendInterVal && clearInterval(this.sendInterVal);

        this.restReqTime = TotalRequestTime
        this.startRequestCount(mac)
    }

    rotating() {
		this.state.rotateValue.setValue(0);
        this.playerAnimated.start(() => {
            this.rotating()
        })
	};

    renderHeader(){
        return(
            <TouchableOpacity style={styles.backTouch} onPress={()=>{
                this.props.navigation.pop()
            }}>
                <Image style={styles.leftImg} source={require('../../../images/back.png')}/>
            </TouchableOpacity>
        )
    }

    renderCenterWifi(){
        return(
            <View style={{width:'100%',justifyContent:'center',alignItems:'center'}}>
                <Image style={styles.wifiImg} source={require('../../../images/connectWifi/wifi.png')}/>
            </View>
        )
    }

    renderAnimateView(status){
        if(status == 2){
            return <Image source={require('../../../images/connectWifi/complete.png')} style={styles.contentImg}/>
        }

		const spin = this.state.rotateValue.interpolate({
            inputRange: [0, 1],//输入值
            outputRange: ['0deg', '360deg'] //输出值
		})
		
		return(
			<Animated.Image style={[styles.contentImg,{transform:[{"rotate": spin }]}]} source={require('../../../images/connectWifi/configing.png')}/>
		)
    }

    _renderConnectNetworkContent(){
        if(this.state.connectNetworkStatus == 0){
            return null
        }
        const textColor = this.state.connectNetworkStatus == 1 ? '#646566' : '#008CFF'
        const contentText = this.state.connectNetworkStatus == 1 ? '手机连接设备中...' : '手机连接设备成功'

        return(
            <View style={[styles.contentItem,{marginTop: 24}]}>
                {this.renderAnimateView(this.state.connectNetworkStatus)}
                <Text style={[styles.content,{color: textColor}]}>{contentText}</Text>
            </View>
        )
    }

    _renderSettingNetworkContent(){
        if(this.state.settingNetworkStatus == 0){
            return null
        }
        const textColor = this.state.settingNetworkStatus == 1 ? '#646566' : '#008CFF'
        const contentText = this.state.settingNetworkStatus == 1 ? '连接网络中…' : '连接网络成功'

        return(
            <View style={[styles.contentItem,{marginTop: 24}]}>
                {this.renderAnimateView(this.state.settingNetworkStatus)}
                <Text style={[styles.content,{color: textColor}]}>{contentText}</Text>
            </View>
        )
    }

    _renderBindUserContent(){
        if(this.state.bindUserStatus == 0){
            return null
        }
        const textColor = this.state.bindUserStatus == 1 ? '#646566' : '#008CFF'
        const contentText = this.state.bindUserStatus == 1 ? '设备绑定账户中…' : '设备添加成功'

        return(
            <View style={[styles.contentItem,{marginTop: 24}]}>
                {this.renderAnimateView(this.state.bindUserStatus)}
                <Text style={[styles.content,{color: textColor}]}>{contentText}</Text>
            </View>
        )
    }

    renderContent(){
        return(
            <View style={styles.bottomWrapper}>
                <View style={styles.contentWrapper}>
                    <Text style={styles.title}>正在为设备配置网络…</Text>
                    {this._renderConnectNetworkContent()}
                    {this._renderSettingNetworkContent()}
                    {this._renderBindUserContent()}
                </View>
            </View>
        )
    }

    render(){
        return (
            <ImageBackground style={styles.container} source={require('../../../images/connectWifi/bg.png')}>
                {this.renderHeader()}
                {this.renderCenterWifi()}
                {this.renderContent()}
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
	input:{
        fontSize: 15
    },
    backTouch:{
        marginTop: StatusBarHeight,
        paddingHorizontal: 16,
        height: NavigationBarHeight,
        justifyContent: 'center',
        alignItems: 'center',
        width: 42
    },
    leftImg:{
        width: 10, 
        height: 18,
        resizeMode: 'contain',
        tintColor: Colors.white
    },
    bottomWrapper:{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 0,
        bottom: screenH * 0.2 + BottomSafeMargin,
    },
    contentWrapper:{
        height: 160
    },
    wifiImg:{
        width: screenW * 0.7, 
        height: screenW * 0.7,
        resizeMode: 'contain',
        marginTop: 24
    },
    title:{
        color: '#008CFF', 
        fontSize: 20,
        fontWeight: '500'
    },
    contentImg:{
        width: 16,
        height: 16,
        resizeMode: 'contain'
    },
    contentItem:{
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    content: {
        fontSize: 14,
        marginLeft: 8
    },

    inputWrapper:{
        borderBottomColor: Colors.themeTextLightGray,
        borderBottomWidth: 1,
        marginLeft: 20,
        paddingVertical: 5,
        width: '80%'
    },
    bottomButton:{
        marginHorizontal: 16,
        height: 40,
        backgroundColor: Colors.themeTextBlue,
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5
    },
    remarkInputWrapper:{
        width: '100%',
        borderRadius: 5,
        paddingVertical: 5
    },
    remarkInput:{
        width:'100%',
		height:80,
		paddingHorizontal:16,
      	textAlignVertical:'top',
		fontSize:14,
      	borderRadius:4,
    }
})

export default WiFiUDPConnecting

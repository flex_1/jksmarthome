/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    NativeModules,
    NativeEventEmitter,
    AppState,
    TextInput,
    Image,
    Keyboard,
    Alert,
    Linking
} from 'react-native';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';
import HeaderTitle from '../Navigation/HeaderTitle';
import { Colors,LocalStorageKeys } from '../../../common/Constants';
import WifiManager from "react-native-wifi-reborn";
import ToastManager from '../../../common/CustomComponent/ToastManager';
import AsyncStorage from '@react-native-community/async-storage';

const JKConnectWifiUtils = NativeModules.JKConnectWifiUtils;
const JKWifiManager = new NativeEventEmitter(JKConnectWifiUtils)

const kWifiHaveBeenChanged  = "WifiHaveBeenChanged"

class WiFiPassword extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')} disableNight={true}/>,
        headerBackground: <HeaderBackground hiddenBorder={true} disableNight={true}/>
    })

    constructor(props) {
        super(props);
        const {getParam, setParams} = this.props.navigation
        
        this.wifiDeviceType = getParam('wifiDeviceType')
        this.apName = getParam('apName')
        this.apPassword = getParam('apPassword')
        this.apPrefix = getParam('apPrefix')
        this.deviceName = getParam('deviceName')
        this.deviceIcon = getParam('deviceIcon')

        this.state = {
            ssid: '',
            bssid: null,
            password: '',
            appState: AppState.currentState,
            secureTextEntry: true,
            rememberPass: true,
            connectWifiType: getParam('connectWifiType'), // 1.一键配网  2.Ap配网
            iosLocalAuth: true   // ios 本地网络如果未开启的话 无法进行配网
        }
    }

    componentDidMount(){
        // 获取到本地连接的 wifi {"ssid":ssid, "bssid":bssid}
        if(Platform.OS == 'ios'){
            JKConnectWifiUtils.requestLocalNetworkAuth((auth)=>{
                this.setState({iosLocalAuth: auth})
            })
            // 获取网络 ssid 和 bssid
            this.wifiBeenChangedNoti = JKWifiManager.addListener(kWifiHaveBeenChanged, (data)=>{
                if(!data){
                    return
                }
                if(this.state.connectWifiType == 2 && data.ssid?.includes(this.apPrefix)){
                    return
                }
                if(data.ssid){
                    if(data.ssid != this.state.ssid){
                        this.setState({
                            password: ''
                        })
                    }
                    this.setState({
                        ssid: data.ssid,
                        bssid: data.bssid
                    })
                    this.checkLocationPassword(data.ssid)
                }
            })
            JKConnectWifiUtils.configWifi()
        }else{
            this.getWifiInfo()
        }
        // 监听App状态
        AppState.addEventListener("change", this._handleAppStateChange);
    }

    componentWillUnmount(){
        AppState.removeEventListener("change", this._handleAppStateChange);
        this.wifiBeenChangedNoti?.remove()
        this.connectNoti?.remove()
        if(Platform.OS == 'ios'){
            JKConnectWifiUtils.stopMonitoringWifi()
        }
    }

    // 查看本地是否存有 wifi 名称和密码
    async checkLocationPassword(ssid){
        if(!ssid){
            return
        }
        const passwordString = await AsyncStorage.getItem(LocalStorageKeys.kWifiSsidPassword)
        if(passwordString && ssid){
            try {
                let passwordDict = JSON.parse(passwordString)
                const password = passwordDict[ssid]
                if(password){
                    this.setState({
                        password: password
                    })
                }
            } catch (error) {
                
            }
        }
    }

    // 将账号密码存储本地
    async savePasswordToLocal(ssid, password){
        if(!this.state.rememberPass){
            return
        }
        let passwordString = await AsyncStorage.getItem(LocalStorageKeys.kWifiSsidPassword)
        let passwordDict = {}
        if(passwordString){
            try {
                passwordDict = JSON.parse(passwordString)
            } catch (error) {
                
            }
        }
        password = password || ''
        passwordDict[ssid] = password
        AsyncStorage.setItem(LocalStorageKeys.kWifiSsidPassword, JSON.stringify(passwordDict))
    }

    // 获取wifi信息
    getWifiInfo(){
        if(Platform.OS == 'ios'){
            JKConnectWifiUtils.wifiUpdate()
        }else{
            WifiManager.getCurrentWifiSSID().then(
                ssid => {
                    if(this.state.connectWifiType == 2 && ssid?.includes(this.apPrefix)){
                        return
                    }
                    this.setState({ssid: ssid})
                    if(ssid){
                        this.checkLocationPassword(ssid)
                        WifiManager.getBSSID().then(
                            bssid => {
                                this.setState({bssid: bssid})
                            },
                            () => {
                                console.log("Cannot get current BSSID!");
                            }
                        )
                    }
                },
                () => {
                  console.log("Cannot get current SSID!");
                }
            );
        }
    }

    // 处理App状态
    _handleAppStateChange = nextAppState => {
        // App从后台进入前台。
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            this.getWifiInfo()
            if(Platform.OS == 'ios'){
                JKConnectWifiUtils.requestLocalNetworkAuth((auth)=>{
                    this.setState({iosLocalAuth: auth})
                })
            }
        }
        this.setState({ appState: nextAppState });
    }

    // 下一步
    nextStep(){
        if(Platform.OS == 'ios' && !this.state.iosLocalAuth){
            Alert.alert(
                '提示',
                '配网前请先打开小萨管家的[本地网络]权限,否则无法成功配网。',
                [
                    { text: '知道了', onPress: () => { }, style: 'cancel' },
                    { text: '前往设置', onPress: () => {
                        this.state.iosLocalAuth = true
                        Linking.openURL('app-settings:').catch(err => console.log('error', err))
                    }},
                ]
            )
            return
        }
        if(this.state.connectWifiType == 1){
            if(!this.state.ssid || !this.state.bssid){
                Alert.alert(
                    '提示',
                    '请确认您当前已连接Wifi网络。',
                    [
                        { text: '知道了', onPress: () => { }, style: 'cancel' },
                    ]
                )
                return
            }
            this.props.navigation.navigate('WiFiConnecting',{
                ssid: this.state.ssid,
                bssid: this.state.bssid,
                password: this.state.password,
                wifiDeviceType: this.wifiDeviceType,
                deviceIcon: this.deviceIcon
            })
        }else if(this.state.connectWifiType == 2){
            if(!this.state.ssid || !this.state.password){
                Alert.alert(
                    '提示',
                    '请输入Wifi名称和密码',
                    [
                        { text: '知道了', onPress: () => { }, style: 'cancel' },
                    ]
                )
                return
            }
            // 进入Ap配网
            this.props.navigation.navigate('WiFiUDPPrepare',{
                ssid: this.state.ssid,
                password: this.state.password,
                wifiDeviceType: this.wifiDeviceType,
                apName: this.apName,
                apPassword: this.apPassword,
                apPrefix: this.apPrefix,
                deviceName: this.deviceName,
                deviceIcon: this.deviceIcon
            })
        }
        this.savePasswordToLocal(this.state.ssid, this.state.password)
    }

    renderSsidName(){
        return(
            <View style={{width:'100%',marginTop: 32}}>
                <Text style={{marginLeft: 16,color: '#969799',fontSize:14}}>WiFi名称</Text>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <TextInput
                        editable={this.state.connectWifiType == 2}
                        style={{marginLeft:16,flex: 1,fontSize: 18, color: '#303030'}}
                        defaultValue={this.state.ssid}
                        clearButtonMode={'while-editing'}
                        onChangeText={(text) => {
                            this.state.ssid = text
                        }}
                    />
                    {/* <Text style={{marginLeft:16,flex: 1,fontSize: 18, color: '#303030'}}>{this.state.ssid}</Text> */}
                    <TouchableOpacity activeOpacity={0.7} style={{paddingHorizontal: 16,paddingVertical: 12}} onPress={()=>{
                        JKConnectWifiUtils.gotoSystemWifiList()
                        // if(Platform.OS == 'ios'){
                        //     JKConnectWifiUtils.gotoSystemWifiList()
                        // }else{
                        //     this.props.navigation.navigate('WiFiList',{wifiCallback:(ssid, bssid)=>{
                        //         this.setState({
                        //             ssid: ssid,
                        //             bssid: bssid,
                        //             password: ''
                        //         })
                        //         this.checkLocationPassword(ssid)
                        //     }})
                        // }
                    }}>
                        <Text style={{fontSize: 14, color: '#969799'}}>更换WiFi</Text>
                    </TouchableOpacity>
                </View>
                <View style={{height:1,marginHorizontal:16,backgroundColor: '#F2F2F2'}}/>
            </View>
        )
    }

    renderPasswordTips(){
        if(this.state.connectWifiType != 2){
            return null
        }
        return(
            <Text style={styles.passwordTips}>注意:请输入设备所要连接的WiFi网络的密码。</Text>
        )
    }

    renderPasswordTextInput(){
        const passSecureImg = this.state.secureTextEntry ? require('../../../images/connectWifi/passSecureOff.png')
        : require('../../../images/connectWifi/passSecureOn.png')

        return(
            <View style={{width:'100%',marginTop: 32}}>
                <Text style={{marginLeft: 16,color: '#969799',fontSize:14}}>WiFi密码</Text>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <TextInput
                        style={{marginLeft:16,flex: 1,fontSize: 18, color: '#303030'}}
                        defaultValue={this.state.password}
                        // maxLength={16}
                        // placeholder="请输入6至16位密码"
                        clearButtonMode={'while-editing'}
                        secureTextEntry={this.state.secureTextEntry}
                        onChangeText={(text) => {
                            this.state.password = text
                        }}
                    />
                    <TouchableOpacity activeOpacity={0.7} style={{paddingHorizontal: 16,paddingVertical: 12,marginLeft:10}} onPress={()=>{
                        this.setState({secureTextEntry: !this.state.secureTextEntry})
                    }}>
                        <Image style={{width:16,height:16,resizeMode:'contain'}} source={passSecureImg}/>
                    </TouchableOpacity>
                </View>
                <View style={{height:1,marginHorizontal:16,backgroundColor: '#F2F2F2'}}/>
                {this.renderPasswordTips()}
            </View>
        )
    }

    renderRememberPassword(){
        const rememberPassImg = this.state.rememberPass  ? require('../../../images/connectWifi/selected.png')
        : require('../../../images/connectWifi/unselected.png')

        return(
            <TouchableOpacity  
                activeOpacity={0.7} 
                style={{paddingLeft: 16, marginTop: 32, flexDirection:'row',alignItems:'center'}}
                onPress={()=>{
                    this.setState({rememberPass: !this.state.rememberPass})
                }}
            >
                <Image style={{width:16,height:16,resizeMode:'contain'}} source={rememberPassImg}/>
                <Text style={{marginLeft: 5, fontSize:14, color: '#303030'}}>记住密码</Text>
            </TouchableOpacity>
        )
    }

    renderBottomView(){
        const labelText = this.state.connectWifiType == 2 ? '下一步' : '一键配网'

        return(
            <View style={styles.bottomWrapper}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.nextBtn} 
                    onPress={()=>{
                        this.nextStep()
                    }}
                >
                    <Text style={{color:'#FFFFFF',fontSize: 18}}>{labelText}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <TouchableOpacity activeOpacity={1} style={styles.container} onPress={()=>{Keyboard.dismiss()}}>
                {this.renderSsidName()}
                {this.renderPasswordTextInput()}
                {this.renderRememberPassword()}
                {this.renderBottomView()}
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: Colors.white, 
        flex: 1
    },
    bottomWrapper:{
        position: 'absolute',
        width:'100%',
        left:0, 
        bottom:0, 
        paddingBottom: 41, 
        justifyContent:'center',
        alignItems:'center',
        
    },
    nextBtn:{
        marginTop: 14,
        width:'80%',
        height: 49, 
        borderRadius: 24.5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#008CFF'
    },
    passwordTips:{
        fontSize: 13,
        color: '#969799',
        marginTop: 15,
        marginLeft: 16
    }
});

export default WiFiPassword

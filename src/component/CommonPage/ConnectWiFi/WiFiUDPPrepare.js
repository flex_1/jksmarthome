import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    Alert,
    TextInput,
    Dimensions,
    NativeModules,
    NativeEventEmitter,
    AppState
} from 'react-native';
import { NotificationKeys,NetUrls,Colors } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderRight from '../Navigation/HeaderRight';
import HeaderBackground from '../Navigation/HeaderBackground';
import WifiManager from "react-native-wifi-reborn";


const JKConnectWifiUtils = NativeModules.JKConnectWifiUtils;
const JKWifiManager = new NativeEventEmitter(JKConnectWifiUtils)
const screenW = Dimensions.get('screen').width;

const kWifiHaveBeenChanged  = "WifiHaveBeenChanged"

class WiFiUDPPrepare extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'AP配网'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
        super(props);
        const { getParam } = props.navigation
        
        this.ssid = getParam('ssid')
        this.password = getParam('password')
        this.wifiDeviceType = getParam('wifiDeviceType')
        this.apName = getParam('apName')
        this.apPassword = getParam('apPassword')
        this.apPrefix = getParam('apPrefix')
        this.deviceName = getParam('deviceName')
        this.deviceIcon = getParam('deviceIcon')

		this.state = {
            canGoNext: false,
            appState: AppState.currentState
		};
	}

	componentDidMount() {
        // 获取到本地连接的 wifi {"ssid":ssid, "bssid":bssid}
        if(Platform.OS == 'ios'){
            // 获取网络 ssid 和 bssid
            this.wifiBeenChangedNoti = JKWifiManager.addListener(kWifiHaveBeenChanged, (data)=>{
                if(!data){
                    return
                }
                if(data.ssid && data.ssid.includes(this.apPrefix)){
                    this.setState({
                        canGoNext: true
                    })
                }
            })
            JKConnectWifiUtils.configWifi()
        }else{
            this.getWifiInfo()
        }
        // 监听App状态
        AppState.addEventListener("change", this._handleAppStateChange);
    }

    componentWillUnmount(){
        AppState.removeEventListener("change", this._handleAppStateChange);
        this.wifiBeenChangedNoti?.remove()
    }

    // 获取wifi信息
    getWifiInfo(){
        if(Platform.OS == 'ios'){
            JKConnectWifiUtils.wifiUpdate()
        }else{
            WifiManager.getCurrentWifiSSID().then(
                ssid => {
                    if(ssid && ssid.includes(this.apPrefix)){
                        this.setState({
                            canGoNext: true
                        })
                    }
                },
                () => {
                  console.log("Cannot get current SSID!");
                }
            );
        }
    }

    // 处理App状态
    _handleAppStateChange = nextAppState => {
        // App从后台进入前台。
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            this.getWifiInfo()
        }
        this.setState({ appState: nextAppState });
    }

    renderTips(){
        return(
            <View style={{marginTop: 20,width:'100%',justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize: 18, color: Colors.themeTextBlack,fontWeight:'500',marginTop: 15}}>
                    请连接{this.deviceName}发出的热点:
                </Text>
                <Text style={{fontSize: 16, color: '#008CFF',fontWeight:'500',marginTop: 14}}>
                    "{this.apName}"
                </Text>
                <Text style={{fontSize: 16, color: '#008CFF',fontWeight:'500',marginTop: 10}}>
                    密码:  {this.apPassword}
                </Text>
                <Text style={{fontSize: 14, color: '#646566',fontWeight:'500',marginTop: 12}}>
                    并在连接成功后返回APP
                </Text>
            </View>
        )
    }

    renderGuideImg(){
        let img = require('../../../images/connectWifi/ios_ap_guide.png')
        let imgStyle = {width: screenW * 0.65, height: screenW * 0.65 * 1.04}
        if(Platform.OS == 'android'){
            img = require('../../../images/connectWifi/android_ap_guide.png')
            imgStyle = {width: screenW * 0.6, height: screenW * 0.6 * 1.48}
        }
        return(
            <View style={{marginTop: 20, justifyContent: 'center', alignItems:'center'}}>
                <Image style={[{resizeMode: 'contain'},imgStyle]} source={img}/>
            </View>
        )
    }

    renderbottomBtn(){
        const labelText = this.state.canGoNext ? '下一步' : '去设置'

        return(
            <TouchableOpacity style={styles.btnTouch} onPress={()=>{
                if(this.state.canGoNext){
                    this.props.navigation.replace('WiFiUDPConnecting',{
                        ssid: this.ssid,
                        password: this.password,
                        wifiDeviceType: this.wifiDeviceType,
                        deviceIcon: this.deviceIcon
                    })
                }else{
                    JKConnectWifiUtils.gotoSystemWifiList()
                }
            }}>
                <Text style={{color:'#FFFFFF',fontSize: 18}}>{labelText}</Text>
            </TouchableOpacity>
        )
    }

	render() {
		return (
            <View style={{flex: 1}}>
                {this.renderTips()}
                {this.renderGuideImg()}
                {this.renderbottomBtn()}
            </View>
		)
	}
}

const styles = StyleSheet.create({
	btnTouch:{
        marginHorizontal: '10%',
        height: 49, 
        borderRadius: 24.5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#008CFF',
        marginTop: 20
    }
})

export default WiFiUDPPrepare

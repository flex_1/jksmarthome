/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import { 
    Dimensions, 
    Image, 
    Platform, 
    ScrollView, 
    StyleSheet, 
    Text, 
    View,
    TouchableOpacity,
    ImageBackground,
    StatusBar
} from 'react-native';
import { Colors } from '../../common/Constants';
import {StatusBarHeight,BottomSafeMargin} from '../../util/ScreenUtil';

const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;
const PageCount = 3

class GuidePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }

    componentDidMount() {
        // 状态栏 样式
		StatusBar.setBarStyle('light-content')
    }

    componentWillUnmount() {
        // 状态栏 样式
		StatusBar.setBarStyle('dark-content')
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView
                    contentContainerStyle={{ width:screenW*PageCount, height: screenH}}
                    bounces={false}
                    pagingEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    horizontal={true}
                >   
                    <Image source={require('../../images/guidePage/1.png')} style={styles.page}/>
                    <Image source={require('../../images/guidePage/2.png')} style={styles.page}/>
                    <ImageBackground source={require('../../images/guidePage/3.png')} style={styles.page}>
                        <TouchableOpacity activeOpacity={0.8} style={styles.bottomBtn} onPress={()=>{
                            this.props.skipPress()
                        }}>
                    
                        </TouchableOpacity>
                    </ImageBackground>
                </ScrollView>
                <TouchableOpacity style={styles.topBtn} onPress={()=>{
                    this.props.skipPress()
                }}>
                    <Text style={{color:Colors.white,fontSize:15}}>跳过</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.themeBG,
        flex: 1
    },
    page:{
        width:screenW, 
        height:screenH
    },
    topBtn:{
        position: 'absolute',
        top:StatusBarHeight+10,
        right:10,
        paddingHorizontal:20,
        paddingVertical:10
    },
    bottomBtn:{
        position: 'absolute',
        bottom: BottomSafeMargin,
        left: '20%',
        right: '20%',
        height:'20%'
    }
});

export default GuidePage

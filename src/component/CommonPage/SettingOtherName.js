/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	DeviceEventEmitter,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Alert,
	Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import { Colors, NotificationKeys, NetUrls } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import {BottomSafeMargin} from '../../util/ScreenUtil';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import {ListLoading,ListError } from "../../common/CustomComponent/ListLoading";
import NoContentButton from '../../common/CustomComponent/NoContentButton';

const screenH = Dimensions.get('screen').height;

class SettingOtherName extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
			headerRight: (
				<HeaderRight buttonArrays={[
                    {text:'添加',onPress:()=>{
                        navigation.getParam('addBtnClick')()
                    }},
                ]}/>
			),
            headerBackground: <HeaderBackground/>
		}
	}

	constructor(props) {
		super(props);
		const { getParam,setParams } = props.navigation;
		this.callBack = getParam('callBack')
        this.type = getParam('type')
        this.id = getParam('id')

		this.state = {
            nameList: null
		}

		setParams({
			addBtnClick: ()=>{
                this.addOtherName()
            },
		})
	}

	componentDidMount() {
		this.requestNameList()
	}

	componentWillUnmount() {
        
    }
    
	async requestNameList(){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.labelList,
				params: {
					iid : this.id,
                    type: this.type  // 设备:1 场景：2
				}
            });
            SpinnerManager.close()
			if (data && data.code == 0) {
				this.setState({
                    nameList : data.result || []
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	// 保存
	async requestSaveOtherName(params) {
        params = params || {}
		if(this.id){
			params.iid = this.id
		}
        params.type = this.type
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.addLabels,
				params: params
			});
			SpinnerManager.close()
			if (data.code == 0) {
                this.requestNameList()
                this.callBack && this.callBack()
                ToastManager.show('添加成功')
			} else if(data.code == 1000){
                this.checkAndCoverName(params.remarks, 4)
            }else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    async delteteOtherName(id) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.addLabels,
				params: {
                    type: 3,
                    iid: id
                }
			});
			SpinnerManager.close()
			if (data.code == 0) {
                this.requestNameList()
                this.callBack && this.callBack()
                ToastManager.show('删除成功')
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

    // 检查并覆盖别名
    checkAndCoverName(name, type){
        Alert.alert(
            '提示',
            '‘'+ name +'’ 该别名已存在，是否覆盖该别名？',
            [
                { text: '取消', onPress: () => { }, style: 'cancel' },
                { text: '覆盖', onPress: () => {
                    this.requestSaveOtherName({type:type, remarks:name})
                } },
            ]
        )
    }

    addOtherName(){
        const { navigate,pop } = this.props.navigation
        navigate('ReName',{title:'添加别名', callBack:(name)=>{
            pop()
            this.requestSaveOtherName({type:this.type, remarks:name})
        }})
    }

    renderNoContent(){
        return(
            <View style={styles.noContentWrapper}>
                <Text style={styles.noContentText}>暂无别名</Text>
                <NoContentButton
                    text={'添加别名'}
                    onPress={()=>{
                        this.addOtherName()
                    }}
                />
            </View>
        )
    }

    renderList(){
        if(!this.state.nameList){
            return <ListLoading/>
        }
        if(this.state.nameList.length <= 0){
            return this.renderNoContent()
        }
        const Colors = this.props.themeInfo.colors;

        let list = this.state.nameList.map((value, index)=>{
            return(
                <View key={'other_name_'+index} style={[styles.item,{backgroundColor: Colors.themeBg}]}>
                    <Text style={[styles.indexText,{color: Colors.themeText}]}>{index +1}</Text>
                    <Text style={[styles.name,{color: Colors.themeText}]}>{value.name}</Text>
                    <View style={{flex: 1}}/>
                    <TouchableOpacity style={styles.delete} onPress={()=>{
                        this.delteteOtherName(value.id)
                    }}>
                        <Image style={styles.closeImg} source={require('../../images/close_black.png')}/>
                    </TouchableOpacity>
                </View>
            )
        })
        return (
            <ScrollView>
                {list}
            </ScrollView>
        )
    }

	render() {
		const Colors = this.props.themeInfo.colors;
		
		return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.renderList()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
    },
	noContentWrapper:{
        marginTop: screenH * 0.3,
        alignItems: 'center', 
        justifyContent: 'center'
    },
    noContentText:{
        color: Colors.themeTextLightGray,
        fontSize: 14,
        marginBottom: 20
    },
    item:{
        paddingLeft: 20,
        flexDirection:'row',
        alignItems:'center',
        height: 60,
        borderRadius: 5,
        marginTop: 15,
        marginHorizontal: 16
    },
    indexText:{
        fontSize: 16
    },
    name:{
        fontSize: 16,
        fontWeight: 'bold',
        marginLeft: 20
    },
    closeImg:{
        width:25,
        height:25,
        tintColor: Colors.themBgRed,
        resizeMode:'contain'
    },
    delete:{
        paddingHorizontal: 20,
        height:'100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SettingOtherName)
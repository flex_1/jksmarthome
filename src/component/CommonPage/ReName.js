/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    DeviceEventEmitter,
    Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import ToastManager from "../../common/CustomComponent/ToastManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';

class ReName extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text:'保存', onPress:()=>{
                    navigation.getParam('saveBtn')()
                }}
            ]}/>
        ),
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

    constructor(props) {
        super(props);
        const {getParam, setParams} = this.props.navigation
        this.callBack = getParam('callBack')
        this.maxLength = getParam('maxLength') || 12
        this.isAllowEmpty = getParam('isAllowEmpty') || false
        this.subTips = getParam('subTips')

        this.state = {
            name: getParam('name')
        }

        setParams({
            saveBtn: this.submit.bind(this)
        })
    }

    submit(){
        Keyboard.dismiss()
        if(!this.state.name && !this.isAllowEmpty){
			ToastManager.show('名称不能为空')
			return
        }
        this.callBack && this.callBack(this.state.name)
    }

    renderSubTips(){
        if(!this.subTips){
            return null
        }
        const Colors = this.props.themeInfo.colors
        return(
            <Text style={[styles.subTips,{color: Colors.themeTextLight}]}>{this.subTips}</Text>
        )
    }

    getInput() {
        const Colors = this.props.themeInfo.colors
        return (
            <View style={{marginTop:10,width:'100%'}}>
                <TextInput
                    style={{
                        width:'100%',
                        backgroundColor:Colors.themeBg,
                        color:Colors.themeText,
                        height:50,
                        paddingHorizontal: 20,
                    }}
                    defaultValue={this.state.name}
                    autoFocus={true}
                    underlineColorAndroid='transparent'
                    maxLength={this.maxLength}
                    clearButtonMode={'while-editing'}
                    returnKeyType={'done'}
                    onChangeText={(text)=>{
                        this.state.name = text
                    }}
                    onSubmitEditing={()=>{
                        this.submit()
                    }}
                />
                {this.renderSubTips()}
            </View> 
            
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors
        return (
            <View style={{backgroundColor: Colors.themeBaseBg, flex: 1}}>
                {this.getInput()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 15,
        paddingLeft: 10,
        marginRight: 5
    },
    subTips:{
        marginTop: 10, 
        marginLeft: 16,
        fontSize: 14,
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo
    })
)(ReName)

import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    Alert,
    TextInput,
    Dimensions,
    FlatList,
    DeviceEventEmitter
} from 'react-native';
import { connect } from 'react-redux';
import { NotificationKeys,NetUrls,Colors } from '../../../common/Constants';
import {ListNoContent,FooterEnd,ListLoading,ListError } from "../../../common/CustomComponent/ListLoading";
import ToastManager from "../../../common/CustomComponent/ToastManager";
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import {ColorsDark, ColorsLight} from '../../../common/Themes';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import RoomList from './TCPRoomList';

class TCPRoom extends Component {

	constructor(props) {
        super(props);
        
		this.state = {
            floors: [],
            loading: false
		};
	}

	componentDidMount() {
        this.requestForData()
        // 房间列表
        this.roomListListener = DeviceEventEmitter.addListener('kTCPRoomListNoti',(data)=>{
            this.handleForData(data)
        })
    }
    
    componentWillUnmount() {
        this.roomListListener.remove()
    }

    //请求数据
    requestForData(){
        try {
            window.client.write('request-4-0-')
        } catch (error) {
            ToastManager.show('获取数据出错，请检查TCP连接')
        }
    }

    // 处理房屋列表
    handleForData(data){
        try {
            let roomDatas = []
            let floorData = data.split(',')
            for (const fd of floorData) {
                let floorName = fd.split('*')[0]
                let data = fd.split('*')[1]
                 
                let floorRooms = []
                let rooms = data.split('_')
                for (const room of rooms) {
                    let roomId = room.split('.')[0]
                    let roomName = room.split('.')[1]

                    floorRooms.push({roomId:roomId, name: roomName, floor: floorName})
                }
                roomDatas.push({floor: floorName, rooms: floorRooms})
            }
            this.setState({ 
                floors: roomDatas,
                currentFloor: roomDatas[0]['floor']
            })
        } catch (error) {
            
        }
    }

    // 渲染楼层 Header
	renderRowItem(rowData, index) {
        const Colors = this.props.isDark ? ColorsDark : ColorsLight

		let dot = null
        let activeText = {color:Colors.themeTextLight}
		if(this.state.currentFloor == rowData.floor){
			dot = <View style={styles.dot} />
			activeText = {color:Colors.themeText,fontSize:18}
		}
		return (
			<TouchableOpacity activeOpacity={0.7} style={styles.floorTouch} onPress={()=>{
				this.setState({
					currentFloor: rowData.floor,
				})
				this.scrollableTab && this.scrollableTab.goToPage(index)	
			}}>
				<Text style={[styles.floorText,activeText]}>{rowData.floor == 0 ? '整屋' : rowData.floor+'楼'}</Text>
				{dot}
			</TouchableOpacity>
		)
	}

    //头部向导
	renderFloorHeader() {
		if(!this.state.floors || this.state.floors.length<=0){
			return null
        }
        const Colors = this.props.isDark ? ColorsDark : ColorsLight
        
		return (
			<View style={[styles.sectionHead,{backgroundColor: Colors.themeBg,borderBottomColor: Colors.themeBaseBg}]}>
                <FlatList
                    style={{marginRight: 10}}
					ref={e => this.roomScrollHead = e}
					horizontal={true}
					showsHorizontalScrollIndicator={false}
					contentContainerStyle={styles.headScrollContainer}
					removeClippedSubviews={false}
					data={this.state.floors}
					keyExtractor={(item, index)=> 'room_header'+index}
					renderItem={({ item, index }) => this.renderRowItem(item, index)}
					initialNumToRender={5}
				/>
			</View>
		)
    }

    // 无内容
    renderNoContent(){
        return(
            <View style={{alignItems:'center',marginTop: '40%'}}>
				<Text style={{fontSize: 14,color: Colors.themeTextLightGray}}>暂无房间</Text>
                <TouchableOpacity style={styles.reconnectTouch} onPress={()=>{
                    this.requestForData()
                }}>
                    <Text style={{fontSize: 14, color: Colors.tabActiveColor}}>重新获取数据</Text>
                </TouchableOpacity>
			</View>
        )
    }

    renderRoomListScrollView(){
		if(!this.state.floors && this.state.loading){
			return <ListLoading/>
        }
        if(!this.state.floors){
            return <ListError onPress={()=>{ this.getRoomFloor() }}/>
        }
		// 暂无房间
		if (this.state.floors.length<=0) {
			return this.renderNoContent()
        }
        
		let rooms = this.state.floors.map((val, index)=>{
			return(
				<View key={'scrol_tab_index'+index} style={{flex:1}}>
                    <RoomList 
                        navigation={this.props.navigation} 
                        currentFloor={val.floor} 
                        isDark={this.props.isDark}
                        roomList={val.rooms}
                    />
				</View>
			)
		})

		return(
			<ScrollableTabView
				ref={e => this.scrollableTab = e}
				initialPage={0}
				prerenderingSiblingsNumber={0}
            	onChangeTab={(obj)=>{
					let currentFloor = this.state.floors[obj.i]['floor']
					this.setState({
                        currentFloor: currentFloor,
                        floors: JSON.parse(JSON.stringify(this.state.floors))
                    })
                    this.roomScrollHead?.scrollToIndex({index:obj.i,viewPosition:0.5})
				}}
				renderTabBar={()=><View/>}
          	>
				{rooms}
			</ScrollableTabView>
		)
	}

	render() {
		return (
            <View style={styles.container}>
                {this.renderFloorHeader()}
                {this.renderRoomListScrollView()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
        flex: 1
    },
    item:{
        width: '100%',
        height: 55,
        paddingHorizontal: 16,
        marginTop: 15,
        alignItems: 'center',
        borderRadius: 5,
        flexDirection: 'row'
    },
    name:{
        fontSize: 15,
        fontWeight: 'bold'
    },
    floorText:{
        marginLeft: 20,
        fontSize: 13
    },
    reconnectTouch:{
        paddingHorizontal: 20,
        paddingVertical: 20,
        marginTop: 20
    },
    sectionHead: {
		width: '100%',
		height: 40,
        flexDirection:'row',
        borderBottomWidth: 1,
        alignItems: 'center'
	},
	headScroll: {
        height: '100%',
        marginRight: 10
	},
	headScrollContainer: {
		alignItems: 'center',
		paddingLeft: 10,
		paddingRight:20
	},
	floorTouch: {
		alignItems: 'center',
		paddingHorizontal: 10,
		marginRight: 5
	},
	dot: {
		width: 4,
		height: 4,
		backgroundColor: Colors.themeTextBlue,
		borderRadius: 2,
		marginTop: 5
	},
	floorText: {
		fontSize: 15,
		fontWeight: 'bold'
	},
})

export default TCPRoom

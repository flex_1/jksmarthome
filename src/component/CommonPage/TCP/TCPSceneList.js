import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    Alert,
    TextInput,
    Dimensions,
    FlatList,
    DeviceEventEmitter
} from 'react-native';
import { connect } from 'react-redux';
import { NotificationKeys,NetUrls,Colors } from '../../../common/Constants';
import {ListNoContent,FooterEnd,ListLoading,ListError } from "../../../common/CustomComponent/ListLoading";
import ToastManager from "../../../common/CustomComponent/ToastManager";
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';

const kTCPConnectStatus = 'TCPConnectStatus'

class TCPSceneList extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'场景'}/>,
        headerBackground: <HeaderBackground/>
    })

	constructor(props) {
        super(props);
        const { getParam } = props.navigation

        this.roomId = getParam('roomId')
		this.state = {
            sceneList: [],
            loading: false,
            connectStatus: true
		};
	}

	componentDidMount() {
        this.requestForData()
        this.sceneListListener = DeviceEventEmitter.addListener('kTCPSceneListNoti',(data)=>{
            this.handleForData(data)
        })
        this.tcpConnectNoti = DeviceEventEmitter.addListener(kTCPConnectStatus,(isConnect)=>{
            this.setState({
                connectStatus: isConnect
            })
        })
    }
    
    componentWillUnmount() {
        this.sceneListListener.remove()
        this.tcpConnectNoti.remove()
    }

    //请求数据
    requestForData(){
        try {
            // 请求场景列表
            let req = 'request-4-' + this.roomId + '-'
            window.client.write(req)
        } catch (error) {
            ToastManager.show('获取数据出错，请检查TCP连接')
        }
    }

    // 处理场景列表
    handleForData(data){
        try {
            let sceneData = []
            let scenes = data.split(',')
            for (const scene of scenes) {
                let sceneId = scene.split('-')[0]
                let name = scene.split('-')[1]
                sceneData.push({sceneId: sceneId, name: name})
            }
            this.setState({ sceneList: sceneData })
        } catch (error) {
            
        }
    }

    // 离线提醒
    renderOfflineWarnning(){
        if(this.state.connectStatus){
            return null
        }
        return(
            <View style={styles.offlineWarnning}>
                <Text style={styles.offlineText}>TCP链接已断开，请重新连接</Text>
            </View>
        )
    }

    // 无内容
    renderNoContent(){
        return(
            <View style={{alignItems:'center',marginTop: '40%'}}>
				<Text style={{fontSize: 14,color: Colors.themeTextLightGray}}>暂无场景</Text>
                <TouchableOpacity style={styles.reconnectTouch} onPress={()=>{
                    this.requestForData()
                }}>
                    <Text style={{fontSize: 14, color: Colors.tabActiveColor}}>重新获取数据</Text>
                </TouchableOpacity>
			</View>
        )
    }

    renderRowItem(item, index){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={{paddingHorizontal: 16}}>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.item,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        let req = 'request-3-' + item.sceneId + '-'
                        try {
                            console.log('client-send-' + req);
                            window.client.write(req)
                        } catch (error) {
                            ToastManager.show('控制出错，请检查TCP连接')
                        }
                    }}
                >
                    <Text style={[styles.name,{color: Colors.themeText}]}>{item.name}</Text>
                </TouchableOpacity> 
            </View>
        )
    }

    // 设备列表
	renderSceneList(){
		if(this.state.loading && !this.state.sceneList){
			return <ListLoading/>
		}
		if(!this.state.sceneList){
			return <ListError onPress={()=>{ this.requestForData() }}/>
		}
		Footer = this.state.sceneList.length > 0 ? FooterEnd: null

		return (
			<FlatList
				contentContainerStyle={{ paddingBottom: 20}}
				scrollIndicatorInsets={{right: 1}}
				removeClippedSubviews={false}
                data={this.state.sceneList}
                keyExtractor={(item, index)=> 'devices_index_'+index}
				renderItem={({item, index}) => this.renderRowItem(item, index)}
				initialNumToRender={10}
                ListFooterComponent={Footer}
                ListEmptyComponent = {this.renderNoContent()}
                // refreshControl={
                //     <RefreshControl
                //         colors={[Colors.themeBGInactive]}
                //         tintColor={Colors.themeBGInactive}
                //         refreshing={this.state.isRefreshing}
                //         onRefresh={this.onRefresh.bind(this)}
                //     />
                // }
			/>
        )
	}
	
	render() {
        const Colors = this.props.themeInfo.colors

		return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderOfflineWarnning()}
                {this.renderSceneList()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
        flex: 1
    },
    item:{
        width: '100%',
        height: 55,
        paddingHorizontal: 16,
        marginTop: 15,
        alignItems: 'center',
        borderRadius: 5,
        flexDirection: 'row'
    },
    name:{
        fontSize: 15,
        fontWeight: 'bold'
    },
    reconnectTouch:{
        paddingHorizontal: 20,
        paddingVertical: 20,
        marginTop: 20
    },
    offlineWarnning:{
        width: '100%',
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.themBgRed
    },
    offlineText:{
        fontSize: 13,
        color: Colors.white
    },
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(TCPSceneList)

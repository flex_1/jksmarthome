import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    Alert,
    TextInput,
    Dimensions,
    DeviceEventEmitter,
    BackHandler
} from 'react-native';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import { connect } from 'react-redux';
import { NotificationKeys,NetUrls,Colors } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import { postJson,postPicture } from "../../../util/ApiRequest";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderRight from '../Navigation/HeaderRight';
import HeaderTitle from '../Navigation/HeaderTitle';
import HeaderBackground from '../Navigation/HeaderBackground';
import TcpSocket from 'react-native-tcp-socket';
import deviceInfo from '../../../util/DeviceInfo';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BottomSafeMargin } from '../../../util/ScreenUtil';
import AsyncStorage from '@react-native-community/async-storage';

const kTCPServerHostAndPort = 'TCPServerHostAndPort'
const kTCPConnectStatus = 'TCPConnectStatus'

class TCPSetting extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerTitle: <HeaderTitle title={'TCP设置'}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text:'调试',onPress:()=>{
                    navigation.navigate('TCP')
                }},
            ]}/>
        ),
        headerLeft: null,
        headerBackground: <HeaderBackground hiddenBorder={true}/>,
        gesturesEnabled: false
    })

	constructor(props) {
        super(props);
        const { getParam } = props.navigation
        
        window.client = null
        window.isTcpRootPage = true
        this.isTryToDestroy = false

        this.lockReconnect = true
		this.state = {
            serverPort: '1235',
            serverHost: '192.168.0.1',
            connectStatus: false,
            remarkText: null,
            recieveContent: ''
		};
	}

	componentDidMount() {
        this.localAddress = deviceInfo.localAddress
        this.getHostAndPort()

        if (Platform.OS === 'android'){
            BackHandler.addEventListener('hardwareBackPress', this.onTCPBackAndroid);
        }
    }
    
    componentWillUnmount() {
        this.lockReconnect = true
        this.reconnectInterval && clearInterval(this.reconnectInterval)

        window.client && window.client.destroy()
        window.client && window.client.removeAllListeners()
        window.client = null;
        
        if (Platform.OS === 'android'){
            BackHandler.removeEventListener('hardwareBackPress',this.onTCPBackAndroid)
        }
    }

    // 处理安卓返回按钮
    onTCPBackAndroid = () => {
        if(window.isTcpRootPage){
            this.backTCP()
        }else{
            return false
        }
        return true
    }

    connectToTCP(){
        if(this.state.connectStatus){
            return
        }
        if(!this.state.serverPort){
            ToastManager.show('请输入端口号')
            return;
        }
        if(!this.state.serverHost){
            ToastManager.show('请输入Host地址')
            return;
        }

        let client;

        client = TcpSocket.createConnection({
            port: parseInt(this.state.serverPort),
            host: this.state.serverHost,
            localAddress: this.localAddress,
            reuseAddress: true,
            // localPort: 20000,
            // interface: "wifi",
            // tls: true
        }, (address) => {
            this.setState({ connectStatus: true })
            this.lockReconnect = false
            DeviceEventEmitter.emit(kTCPConnectStatus, true)
            ToastManager.show('连接成功')
            console.log('client  --  连接成功');
            
            this.saveHostAndPort()
            this.reconnectInterval && clearInterval(this.reconnectInterval)
        });

        client.setEncoding('utf-8')
        
        client.on('data', (data) => {

            let reciveData = data.replace(/\s/g,"");
            let recieve = ''
            try {
                recieve = this.hexCharCodeToStr(reciveData)
            } catch (error) {
                ToastManager.show('数据解析错误')
            }
            console.log('client  --  ' + recieve);

            if(recieve.split(':')[0] == 'rlist'){
                let roomListData = recieve.split(':')[1]
                DeviceEventEmitter.emit('kTCPRoomListNoti', roomListData)
            }else if(recieve.split(':')[0] == 'slist'){
                let sceneListData = recieve.split(':')[1]
                DeviceEventEmitter.emit('kTCPSceneListNoti', sceneListData)
            }else if(recieve.split(':')[0] == 'wlist'){
                let gateListData = recieve.split(':')[1]
                DeviceEventEmitter.emit('kTCPGateListNoti', gateListData)
            }else if(recieve.split(':')[0] == 'dlist'){
                let deviceListData = recieve.split(':')[1]
                DeviceEventEmitter.emit('kTCPDeviceListNoti', deviceListData)
            }else if(recieve.split(':')[0] == 'dstatus'){
                let deviceStatus = recieve.split(':')[1]
                DeviceEventEmitter.emit('kTCPDeviceStatusNoti', deviceStatus)
            }
        });

        client.on('error', (error) => {
            console.log('client' + '错误');
            this.lossConnect()
            this.tryReconnect()
            console.log('client-' + JSON.stringify(error));
            ToastManager.show('连接错误')
        });

        client.on('close', () => {
            console.log('client' + '关闭');
            this.lossConnect()
            this.tryReconnect()
            ToastManager.show('连接已断开')
        });

        window.client = client;
    }

    tryReconnect(){
        if(this.lockReconnect) return
        this.lockReconnect = true

        this.reconnectInterval = setInterval(()=>{
            console.log('client - 正在重连' );
			this.connectToTCP()
        },5*1000)
    }

    // 失去连接
    lossConnect(){
        this.setState({ connectStatus: false })
        if(this.lockReconnect) return
        DeviceEventEmitter.emit(kTCPConnectStatus, false)
    }

    // 退出
    backTCP(){
        const {popToTop} = this.props.navigation

        if(this.state.connectStatus){
            Alert.alert(
                '提示',
                '当前TCP正在连接中，确定要断开连接后退出？',
                [
                    {text: '取消', onPress: () => {}, style: 'cancel'},
                    {text: '退出', onPress: () => { popToTop() }},
                ]
            )
        }else{
            popToTop()
        }
    }

    // 存储 地址和端口
    saveHostAndPort(){
        let hostPort = [this.state.serverHost, this.state.serverPort]
        AsyncStorage.setItem(kTCPServerHostAndPort, JSON.stringify(hostPort))
    }

    // 获取 TCP地址和端口
    async getHostAndPort(){
        let hostPortString = await AsyncStorage.getItem(kTCPServerHostAndPort)
        try {
            let hostPort = JSON.parse(hostPortString)
            this.setState({
                serverHost: hostPort[0],
                serverPort: hostPort[1]
            })
        } catch (error) {
            
        }
    }

    renderHostInput(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={{flexDirection:'row',paddingHorizontal:16,paddingVertical:10,alignItems:'center'}}>
                <Text style={{fontSize: 16, color: Colors.themeText}}>Host: </Text>
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={[styles.input,{color: Colors.themeText}]}
                        defaultValue={this.state.serverHost}
                        placeholder="Host地址"
                        clearButtonMode={'while-editing'}
                        placeholderTextColor={Colors.themeInactive}
                        onChangeText={(text) => {
                            this.state.serverHost = text
                        }}
                    />
                </View>
            </View>
        )
    }

    renderPortInput(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={{flexDirection:'row',paddingHorizontal:16,paddingVertical:10,alignItems:'center'}}>
                <Text style={{fontSize: 16, color: Colors.themeText}}>Port : </Text>
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={[styles.input,{color: Colors.themeText}]}
                        defaultValue={this.state.serverPort}
                        maxLength={5}
                        placeholder="端口号"
                        clearButtonMode={'while-editing'}
                        placeholderTextColor={Colors.themeInactive}
                        onChangeText={(text) => {
                            this.state.serverPort = text
                        }}
                    />
                </View>
            </View>
        )
    }

    renderStatusIcon(){
        let warnBg = 'red'
        let warnText = '连接已断开!'
        if(this.state.connectStatus){
            warnBg = 'green'
            warnText = '连接正常'
        }
        return(
            <View style={{paddingVertical:5,paddingHorizontal: 20,flexDirection:'row',alignItems:'center'}}>
                <View style={{width:10,height:10,borderRadius:5,backgroundColor:warnBg}}/>
                <Text style={{fontSize: 14, color: warnBg, marginLeft:10}}>{warnText}</Text>
            </View>
        )
    }

    renderConnectButton(){
        let btnText = '连接'
        let btnBg = Colors.themeTextBlue

        if(this.state.connectStatus){
            btnText = '断开'
            btnBg = Colors.themBgRed
        }

        return(
            <TouchableOpacity style={[styles.bottomButton,{backgroundColor: btnBg}]} onPress={()=>{
                Keyboard.dismiss()
                if(this.state.connectStatus){
                    this.lockReconnect = true
                    window.client && window.client.destroy()
                }else{
                    this.connectToTCP()
                }
            }}>
                <Text style={{fontSize: 16,color: Colors.white}}>{btnText}</Text>
            </TouchableOpacity>
        )
    }

    renderEnterButton(){
        if(!this.state.connectStatus){
            return null
        }
        const {navigate} = this.props.navigation
        return(
            <TouchableOpacity style={[styles.bottomButton,{marginTop: 30}]} onPress={()=>{
                navigate('TCPHome')
            }}>
                <Text style={{fontSize: 16,color: Colors.white}}>进入控制页面</Text>
            </TouchableOpacity>
        )
    }

    // 返回主页按钮
    renderBackHomeButton(){
        return(
            <TouchableOpacity
                activeOpacity={0.7}
                style={[styles.backButton,{backgroundColor:Colors.themeBG}]} 
                onPress={()=>{
                    this.backTCP()
                }}
            >
                <Text style={{fontSize: 16,color: Colors.white}}>退出TCP</Text>
            </TouchableOpacity>
        )
    }

    // 将十六机制准换成字符串
    hexCharCodeToStr(hexCharCodeStr) {
        var trimedStr = hexCharCodeStr.trim();
        var rawStr = trimedStr.substr(0,2).toLowerCase() === "0x" ? 
            trimedStr.substr(2) : trimedStr;
        var len = rawStr.length;
        if(len % 2 !== 0) {
            ToastManager.show("ASCII 格式错误!");
            return "";
        }
        var resultStr = '';
        for(var i = 0; i < len;i = i + 2) {
            resultStr += '%' + rawStr.charAt(i) + rawStr.charAt(i+1);
        }
        let str = decodeURI(resultStr);
        str = str.replace(/%2c/g,",").replace(/%2C/g,",")
        str = str.replace(/%3a/g,":").replace(/%3A/g,":")
        return str;
    }
	
	render() {
        const Colors = this.props.themeInfo.colors
        
		return (
            <View style={{backgroundColor: Colors.themeBaseBg, flex: 1}}>
                <ScrollView>
                    {this.renderHostInput()}
                    {this.renderPortInput()}
                    {this.renderStatusIcon()}
                    {this.renderConnectButton()}
                    {this.renderEnterButton()}
                </ScrollView>
                {this.renderBackHomeButton()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	input:{
        fontSize: 15
    },
    inputWrapper:{
        borderBottomColor: Colors.themeTextLightGray,
        borderBottomWidth: 1,
        marginLeft: 20,
        paddingVertical: 5,
        width: '80%'
    },
    bottomButton:{
        marginHorizontal: 16,
        height: 40,
        marginTop: 15,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        backgroundColor: Colors.themeTextBlue
    },
    remarkInputWrapper:{
        width: '100%',
        borderRadius: 5,
        paddingVertical: 5
    },
    remarkInput:{
        width:'100%',
		height:80,
		paddingHorizontal:16,
      	textAlignVertical:'top',
		fontSize:14,
      	borderRadius:4,
    },
    backButton:{
        position: 'absolute',
        bottom: BottomSafeMargin + 20,
        left : 16,
        right: 16,
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(TCPSetting)

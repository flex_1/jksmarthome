import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    Alert,
    TextInput,
    Dimensions,
    FlatList,
    DeviceEventEmitter
} from 'react-native';
import { connect } from 'react-redux';
import { NotificationKeys,NetUrls,Colors } from '../../../common/Constants';
import {ListNoContent,FooterEnd,ListLoading,ListError } from "../../../common/CustomComponent/ListLoading";
import ToastManager from "../../../common/CustomComponent/ToastManager";
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderBackground from '../Navigation/HeaderBackground';

const kTCPConnectStatus = 'TCPConnectStatus'

class TCPDeviceList extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('gateName')}/>,
        headerBackground: <HeaderBackground/>
    })

	constructor(props) {
        super(props);
        const { getParam } = props.navigation

        this.order = getParam('order')
        this.gateId = getParam('gateId')
        this.type = getParam('type')
                          
		this.state = {
            deviceList: [],
            loading: false,
            connectStatus: true
		};
	}

	componentDidMount() {
        this.requestForData()
        // 房间列表
        this.deviceListListener = DeviceEventEmitter.addListener('kTCPDeviceListNoti',(data)=>{
            this.handleForData(data)
        })
        this.tcpConnectNoti = DeviceEventEmitter.addListener(kTCPConnectStatus,(isConnect)=>{
            this.setState({
                connectStatus: isConnect
            })
        })
    }
    
    componentWillUnmount() {
        this.deviceListListener.remove()
        this.tcpConnectNoti.remove()
    }

    //请求数据
    requestForData(){
        try {
            let req = 'request-5-' + this.gateId + '-'
            console.log(req);
            window.client.write(req)
        } catch (error) {
            ToastManager.show('获取数据出错，请检查TCP连接')
        }
    }

    // 处理网关列表
    handleForData(data){
        try {
            let deviceList = []
            let deviceData = data.split(',')
            for (const device of deviceData) {
                let id = device.split('_')[0]
                let name = device.split('_')[1]
                deviceList.push({id: id, name: name})
            }
            this.setState({ deviceList: deviceList })
        } catch (error) {
            Alert.alert('提示','列表数据出错')
        }
    }

    // 离线提醒
    renderOfflineWarnning(){
        if(this.state.connectStatus){
            return null
        }
        return(
            <View style={styles.offlineWarnning}>
                <Text style={styles.offlineText}>TCP链接已断开，请重新连接</Text>
            </View>
        )
    }

    // 无内容
    renderNoContent(){
        return(
            <View style={{alignItems:'center',marginTop: '40%'}}>
				<Text style={{fontSize: 14,color: Colors.themeTextLightGray}}>暂无设备</Text>
                <TouchableOpacity style={styles.reconnectTouch} onPress={()=>{
                    this.requestForData()
                }}>
                    <Text style={{fontSize: 14, color: Colors.tabActiveColor}}>重新获取数据</Text>
                </TouchableOpacity>
			</View>
        )
    }

    renderRowItem(item, index){
        const {navigate} = this.props.navigation
        const Colors = this.props.themeInfo.colors

        return(
            <View style={{paddingHorizontal: 16}}>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.item,{backgroundColor: Colors.themeBg}]}
                    onPress={()=>{
                        let deviceData = {
                            title:item.name,
                            isTCP:true,
                            tcpOrder: this.order,
                            tcpDeviceId: item.id,
                            tcpDeviceType: this.type
                        }
                        // 卷帘-1 空调-2 灯(开关)-3 调光灯-4
                        if(this.type == 1){
                            navigate('ScrollCurtains',deviceData)
                        }else if(this.type == 2){
                            navigate('AirConditioner',deviceData)
                        }else if(this.type == 3){
                            navigate('SwitchPannel',deviceData)
                        }else if(this.type == 4){
                            navigate('LightPanel',deviceData)
                        }
                    }}
                >
                    <Text style={[styles.name,{color: Colors.themeText}]}>{item.name}</Text>
                </TouchableOpacity> 
            </View>
        )
    }

    // 设备列表
	renderDeviceList(){
		if(this.state.loading && !this.state.deviceList){
			return <ListLoading/>
		}
		if(!this.state.deviceList){
			return <ListError onPress={()=>{ this.requestForData() }}/>
		}
		Footer = this.state.deviceList.length > 0 ? FooterEnd: null

		return (
			<FlatList
				contentContainerStyle={{ paddingBottom: 20}}
				scrollIndicatorInsets={{right: 1}}
				removeClippedSubviews={false}
                data={this.state.deviceList}
                keyExtractor={(item, index)=> 'devices_index_'+index}
				renderItem={({item, index}) => this.renderRowItem(item, index)}
				initialNumToRender={10}
                ListFooterComponent={Footer}
                ListEmptyComponent = {this.renderNoContent()}
                // refreshControl={
                //     <RefreshControl
                //         colors={[Colors.themeBGInactive]}
                //         tintColor={Colors.themeBGInactive}
                //         refreshing={this.state.isRefreshing}
                //         onRefresh={this.onRefresh.bind(this)}
                //     />
                // }
			/>
		)
	}
	
	render() {
        const Colors = this.props.themeInfo.colors

		return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderOfflineWarnning()}
                {this.renderDeviceList()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
        flex: 1
    },
    item:{
        width: '100%',
        height: 55,
        paddingHorizontal: 16,
        marginTop: 15,
        justifyContent: 'center',
        borderRadius: 5
    },
    name:{
        fontSize: 15,
        fontWeight: 'bold'
    },
    reconnectTouch:{
        paddingHorizontal: 20,
        paddingVertical: 20,
        marginTop: 20
    },
    offlineWarnning:{
        width: '100%',
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.themBgRed
    },
    offlineText:{
        fontSize: 13,
        color: Colors.white
    },
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(TCPDeviceList)

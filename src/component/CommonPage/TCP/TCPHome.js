import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    Alert,
    TextInput,
    Dimensions,
    DeviceEventEmitter
} from 'react-native';
import { connect } from 'react-redux';
import { NotificationKeys,NetUrls,Colors } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderTitle from '../Navigation/HeaderTitle';
import HeaderBackground from '../Navigation/HeaderBackground';
import { StatusBarHeight,TabbarHeight } from '../../../util/ScreenUtil';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import TCPRoom from './TCPRoom';
import TCPGateList from './TCPGateList';

const kTCPConnectStatus = 'TCPConnectStatus'

class TCPHome extends Component {

    static navigationOptions = {
		header: null,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
	}

	constructor(props) {
        super(props);
        const { getParam } = props.navigation
        
		this.state = {
            connectStatus: true,
            currentIndicator: 0
		};
    }
    
    onTCPBackAndroid = () => {
        
        return true
    }

	componentDidMount() {
        window.isTcpRootPage = false
        this.tcpConnectNoti = DeviceEventEmitter.addListener(kTCPConnectStatus,(isConnect)=>{
            this.setState({
                connectStatus: isConnect
            })
        })
    }
    
    componentWillUnmount() {
        window.isTcpRootPage = true
        this.tcpConnectNoti.remove()
    }

    renderHeader(){
        const Colors = this.props.themeInfo.colors

        let sceneLineBg = this.state.currentIndicator == 0 ? Colors.themeButton : Colors.fullyTransparent
        let sceneTextColor = this.state.currentIndicator == 0 ? Colors.themeText : Colors.themeTextLight
        let sceneTextWeight = this.state.currentIndicator == 0 ? {fontWeight: 'bold'} : {}

        let deviceLineBg = this.state.currentIndicator == 1 ? Colors.themeButton : Colors.fullyTransparent
        let deviceTextColor = this.state.currentIndicator == 1 ? Colors.themeText : Colors.themeTextLight
        let deviceTextWeight = this.state.currentIndicator == 1 ? {fontWeight: 'bold'} : {}

        return(
            <View style={[styles.headerWrapper,{backgroundColor: Colors.themeBg}]}>
                <TouchableOpacity style={styles.topBtnTouch} onPress={()=>{
                    this.setState({
                        currentIndicator: 0
                    })
                    this.scrollableTab?.goToPage(0)
                }}>
                    <Text style={[styles.topBtnText,sceneTextWeight,{color: sceneTextColor}]}>房间</Text>
                    <View style={[styles.line,{backgroundColor: sceneLineBg}]}/>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.topBtnTouch,{marginLeft: 30}]} onPress={()=>{
                    this.setState({
                        currentIndicator: 1
                    })
                    this.scrollableTab?.goToPage(1)
                }}>
                    <Text style={[styles.topBtnText,deviceTextWeight,{color: deviceTextColor}]}>设备</Text>
                    <View style={{height: 3,width: 20, marginTop: 5,backgroundColor: deviceLineBg}}/>
                </TouchableOpacity>
                <TouchableOpacity style={styles.backBtnTouch} onPress={()=>{
                    this.props.navigation.pop()
                }}>
                    <Image style={[styles.backImg,{tintColor: Colors.themeText}]} source={require('../../../images/back.png')} />
                </TouchableOpacity>
            </View>
        )
    }

    renderOfflineWarnning(){
        if(this.state.connectStatus){
            return null
        }
        return(
            <View style={styles.offlineWarnning}>
                <Text style={styles.offlineText}>TCP链接已断开，请重新连接</Text>
            </View>
        )
    }

    renderList(){
        return(
            <ScrollableTabView
                ref={e => this.scrollableTab = e}
                locked={true}
				initialPage={0}
				prerenderingSiblingsNumber={0}
            	onChangeTab={(obj)=>{
					this.setState({
						currentIndicator: obj.i,
					})
				}}
				renderTabBar={()=><View/>}
          	>
				<TCPRoom navigation={this.props.navigation} isDark={this.props.themeInfo.isDark}/>
                <TCPGateList navigation={this.props.navigation} isDark={this.props.themeInfo.isDark}/>
          	</ScrollableTabView>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors

		return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderHeader()}
                {this.renderOfflineWarnning()}
				{this.renderList()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.themBGLightGray
    },
    headerWrapper:{
        paddingTop: StatusBarHeight,
        height: StatusBarHeight + 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    topBtnText:{
        fontSize: 18,
        color: Colors.themeTextBlack
    },
    line:{
        height: 3,
        width: 20, 
        marginTop: 5,
    },
    topBtnTouch:{
        justifyContent:'center',
        alignItems:'center'
    },
    offlineWarnning:{
        width: '100%',
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.themBgRed
    },
    offlineText:{
        fontSize: 13,
        color: Colors.white
    },
    backBtnTouch:{
        position: 'absolute',
        left: 0,
        bottom: 0,
        height: '100%',
        width: 50,
        paddingLeft: 16,
        justifyContent: 'center'
    },
    backImg:{
        width: 10, 
        height: 18, 
        resizeMode: 'contain'
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(TCPHome)

import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    Alert,
    TextInput,
    Dimensions,
    FlatList,
    DeviceEventEmitter
} from 'react-native';
import { connect } from 'react-redux';
import { NotificationKeys,NetUrls,Colors } from '../../../common/Constants';
import {ListNoContent,FooterEnd,ListLoading,ListError } from "../../../common/CustomComponent/ListLoading";
import ToastManager from "../../../common/CustomComponent/ToastManager";
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import {ColorsDark, ColorsLight} from '../../../common/Themes';

class TCPRoomList extends Component {

	constructor(props) {
        super(props);
        
		this.state = {
            roomList: props.roomList || [],
            loading: false
		};
	}

	componentDidMount() {
        
    }
    
    componentWillUnmount() {
        
    }

    // 无内容
    renderNoContent(){
        return(
            <View style={{alignItems:'center',marginTop: '40%'}}>
				<Text style={{fontSize: 14,color: Colors.themeTextLightGray}}>暂无房间</Text>
			</View>
        )
    }

    renderRowItem(item, index){
        const {navigate} = this.props.navigation
        const Colors = this.props.isDark ? ColorsDark : ColorsLight

        let floorView = <Text style={[styles.floorText,{color: Colors.themeTextLight}]}>{item.floor + '楼'}</Text>
        if(item.floor == 0){
            floorView = null
        }

        return(
            <View style={{paddingHorizontal: 16}}>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.item,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        navigate('TCPSceneList',{roomId: item.roomId})
                    }}
                >
                    <Text style={[styles.name,{color: Colors.themeText}]}>{item.name}</Text>
                    {floorView}
                </TouchableOpacity> 
            </View>
        )
    }

    // 设备列表
	renderRoomList(){
		Footer = this.state.roomList.length > 0 ? FooterEnd: null
		return (
			<FlatList
				contentContainerStyle={{ paddingBottom: 20}}
				scrollIndicatorInsets={{right: 1}}
				removeClippedSubviews={false}
                data={this.state.roomList}
                keyExtractor={(item, index)=> 'devices_index_'+index}
				renderItem={({item, index}) => this.renderRowItem(item, index)}
				initialNumToRender={10}
                ListFooterComponent={Footer}
                ListEmptyComponent = {this.renderNoContent()}
                // refreshControl={
                //     <RefreshControl
                //         colors={[Colors.themeBGInactive]}
                //         tintColor={Colors.themeBGInactive}
                //         refreshing={this.state.isRefreshing}
                //         onRefresh={this.onRefresh.bind(this)}
                //     />
                // }
			/>
        )
	}
	
	render() {
		return (
            <View style={styles.container}>
                {this.renderRoomList()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
        flex: 1
    },
    item:{
        width: '100%',
        height: 55,
        paddingHorizontal: 16,
        marginTop: 15,
        alignItems: 'center',
        borderRadius: 5,
        flexDirection: 'row'
    },
    name:{
        fontSize: 15,
        fontWeight: 'bold'
    },
    floorText:{
        marginLeft: 20,
        fontSize: 13
    },
    reconnectTouch:{
        paddingHorizontal: 20,
        paddingVertical: 20,
        marginTop: 20
    }
})

export default TCPRoomList
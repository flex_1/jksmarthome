import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    Alert,
    TextInput,
    Dimensions
} from 'react-native';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import { connect } from 'react-redux';
import { NotificationKeys,NetUrls,Colors } from '../../../common/Constants';
import ToastManager from "../../../common/CustomComponent/ToastManager";
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import { postJson,postPicture } from "../../../util/ApiRequest";
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../Navigation/HeaderLeft';
import HeaderRight from '../Navigation/HeaderRight';
import HeaderBackground from '../Navigation/HeaderBackground';
import TcpSocket from 'react-native-tcp-socket';
import deviceInfo from '../../../util/DeviceInfo';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const screenH =  Dimensions.get('window').height;

class TCP extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'TCP'}/>,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
    })

	constructor(props) {
        super(props);
        const { getParam } = props.navigation
        
        this.client = null
		this.state = {
            serverPort: '1235',  // 5188
            serverHost: '192.168.2.100',  // 10.20.30.66
            connectStatus: false,
            remarkText: null,
            recieveContent: ''
		};
	}

	componentDidMount() {
        this.localAddress = deviceInfo.localAddress
    }
    
    componentWillUnmount() {
        this.client && this.client.removeAllListeners()
        this.client && this.client.destroy()
        this.client = null;
    }

    connect(){
        if(this.state.connectStatus){
            return
        }
        if(!this.state.serverPort){
            ToastManager.show('请输入端口号')
            return;
        }
        if(!this.state.serverHost){
            ToastManager.show('请输入Host地址')
            return;
        }

        let server;
        let client;

        client = TcpSocket.createConnection({
            port: parseInt(this.state.serverPort),
            host: this.state.serverHost,
            localAddress: this.localAddress,
            reuseAddress: true,
            // localPort: 20000,
            // interface: "wifi",
            // tls: true
        }, (address) => {
            
            this.setState({
                connectStatus: true
            })
            ToastManager.show('连接成功')

            console.log('client' + JSON.stringify(address));
            client.write('Hello, server! Love, Client.');
        });

        client.setEncoding('hex')
        
        client.on('data', (data) => {
            let reciveData = data.replace(/\s/g,"");
            let recieve = ''
            try {
                recieve = this.hexCharCodeToStr(reciveData)
            } catch (error) {
                alert('数据解析错误')
            }
            this.setState({
                recieveContent: this.state.recieveContent + recieve + '\r\n'
            })
        });

        client.on('error', (error) => {
            this.setState({
                connectStatus: false
            })
            console.log('client' + '错误');
            ToastManager.show('连接错误')
        });

        client.on('close', () => {
            this.setState({
                connectStatus: false
            })
            console.log('client' + '关闭');
            ToastManager.show('连接关闭')
        });

        this.client = client;
    }

    renderHostInput(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={{flexDirection:'row',paddingHorizontal:16,paddingVertical:10,alignItems:'center'}}>
                <Text style={{fontSize: 16}}>Host: </Text>
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={styles.input}
                        defaultValue={this.state.serverHost}
                        placeholder="Host地址"
                        clearButtonMode={'while-editing'}
                        placeholderTextColor={Colors.themeInactive}
                        onChangeText={(text) => {
                            this.state.serverHost = text
                        }}
                    />
                </View>
            </View>
        )
    }

    renderPortInput(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={{flexDirection:'row',paddingHorizontal:16,paddingVertical:10,alignItems:'center'}}>
                <Text style={{fontSize: 16}}>Port : </Text>
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={styles.input}
                        defaultValue={this.state.serverPort}
                        maxLength={4}
                        placeholder="端口号"
                        clearButtonMode={'while-editing'}
                        placeholderTextColor={Colors.themeInactive}
                        onChangeText={(text) => {
                            this.state.serverPort = text
                        }}
                    />
                </View>
                
            </View>
        )
    }

    renderStatusIcon(){
        let warnBg = 'red'
        let warnText = '连接已断开!'
        if(this.state.connectStatus){
            warnBg = 'green'
            warnText = '连接正常'
        }
        return(
            <View style={{paddingVertical:5,paddingHorizontal: 20,flexDirection:'row',alignItems:'center'}}>
                <View style={{width:10,height:10,borderRadius:5,backgroundColor:warnBg}}/>
                <Text style={{fontSize: 14, color: warnBg, marginLeft:10}}>{warnText}</Text>
            </View>
        )
    }

    renderConnectButton(){
        return(
            <TouchableOpacity style={styles.bottomButton} onPress={()=>{
                Keyboard.dismiss()
                this.connect()
            }}>
                <Text style={{fontSize: 16,color: Colors.white}}>连接</Text>
            </TouchableOpacity>
        )
    }

    renderRemarksInput(){
        const Colors = this.props.themeInfo.colors;
        return(
            <View style={{marginTop:5,paddingVertical:5,paddingHorizontal: 16, marginTop: 20}}>
                <View style={[styles.remarkInputWrapper,{backgroundColor: Colors.themeBg}]}>
                    <TextInput
						style={[styles.remarkInput,{color: Colors.themeText}]}
						placeholder={'请输入发送的文字'}
						placeholderTextColor={Colors.themeInactive}
						multiline={true}
                        blurOnSubmit={false}
                        autoFocus={false}
                        returnKeyLabel={'换行'}
                        returnKeyType={'default'}
                        maxLength={200}
                        underlineColorAndroid="transparent"
						onChangeText={(text) => {
							this.state.remarkText = text
						}}
					/>
                </View>
            </View>
        )
    }

    renderSendButton(){
        return(
            <TouchableOpacity style={styles.bottomButton} onPress={()=>{
                Keyboard.dismiss()
                if(!this.state.remarkText){
                    return
                }
                if(this.state.connectStatus){ 
                    this.client.write(this.state.remarkText)
                }
            }}>
                <Text style={{fontSize: 16,color: Colors.white}}>发送</Text>
            </TouchableOpacity>
        )
    }

    renderRecieve(){
        return(
            <View style={{marginTop:5,paddingVertical:5,paddingHorizontal: 16, marginTop: 20,height:screenH*0.3}}>
                <ScrollView style={{width:'100%',backgroundColor:'white'}}>
                    <Text style={{fontSize:14,color: Colors.themeTextBlack}}>{this.state.recieveContent}</Text>
                </ScrollView>
            </View>
        )
    }

    // 将十六机制准换成字符串
    hexCharCodeToStr(hexCharCodeStr) {
        var trimedStr = hexCharCodeStr.trim();
        var rawStr = trimedStr.substr(0,2).toLowerCase() === "0x" ? 
            trimedStr.substr(2) : trimedStr;
        var len = rawStr.length;
        if(len % 2 !== 0) {
            alert("ASCII 格式错误!");
            return "";
        }
        var resultStr = '';
        for(var i = 0; i < len;i = i + 2) {
            resultStr += '%' + rawStr.charAt(i) + rawStr.charAt(i+1);
        }
        let str = decodeURI(resultStr);
        str = str.replace(/%2c/g,",").replace(/%2C/g,",")
        str = str.replace(/%3a/g,":").replace(/%3A/g,":")
        // str = str.replaceAll("%2c",",").replaceAll("%2C",",")
        // str = str.replaceAll("%3a",":").replaceAll("%3A",",")
        return str;
    }
	
	render() {
		const Colors = this.props.themeInfo.colors
		return (
            <KeyboardAwareScrollView 
                enableOnAndroid={true}
                extraScrollHeight={120}                   
                keyboardShouldPersistTaps="handled"
                style={{backgroundColor: Colors.themeBaseBg}} 
            >
				{this.renderHostInput()}
                {this.renderPortInput()}
                {this.renderStatusIcon()}
                {this.renderConnectButton()}
                {this.renderRemarksInput()}
                {this.renderSendButton()}
                {this.renderRecieve()}
			</KeyboardAwareScrollView>
		)
	}
}

const styles = StyleSheet.create({
	input:{
        fontSize: 15
    },
    inputWrapper:{
        borderBottomColor: Colors.themeTextLightGray,
        borderBottomWidth: 1,
        marginLeft: 20,
        paddingVertical: 5,
        width: '80%'
    },
    bottomButton:{
        marginHorizontal: 16,
        height: 40,
        backgroundColor: Colors.themeTextBlue,
        marginTop: 15,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5
    },
    remarkInputWrapper:{
        width: '100%',
        borderRadius: 5,
        paddingVertical: 5
    },
    remarkInput:{
        width:'100%',
		height:80,
		paddingHorizontal:16,
      	textAlignVertical:'top',
		fontSize:14,
      	borderRadius:4,
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(TCP)

import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Keyboard,
    Alert,
    TextInput,
    Dimensions,
    FlatList,
    DeviceEventEmitter
} from 'react-native';
import { connect } from 'react-redux';
import { NotificationKeys,NetUrls,Colors } from '../../../common/Constants';
import {ListNoContent,FooterEnd,ListLoading,ListError } from "../../../common/CustomComponent/ListLoading";
import ToastManager from "../../../common/CustomComponent/ToastManager";
import LocalTokenHandler from '../../../util/LocalTokenHandler';
import SpinnerManager from "../../../common/CustomComponent/SpinnerManager";
import {ColorsDark, ColorsLight} from '../../../common/Themes';

class TCPGateList extends Component {

	constructor(props) {
        super(props);
        
		this.state = {
            gateList: [],
            loading: false
		};
	}

	componentDidMount() {
        this.requestForData()
        // 房间列表
        this.gateListListener = DeviceEventEmitter.addListener('kTCPGateListNoti',(data)=>{
            this.handleForData(data)
        })
    }
    
    componentWillUnmount() {
        this.gateListListener.remove()
    }

    //请求数据
    requestForData(){
        try {
            window.client.write('request-5-0-')
        } catch (error) {
            ToastManager.show('获取数据出错，请检查TCP连接')
        }
    }

    // 处理网关列表
    handleForData(data){
        try {
            let gateList = []
            let gateData = data.split(',')
            for (const gate of gateData) {

                console.log('client--  ' + gate);
                let id = gate.split('_')[0]
                let order = gate.split('_')[1]
                let type = gate.split('_')[2]
                let name = gate.split('_')[3]
                gateList.push({id:id, order:order, type:type, name:name})
            }
            this.setState({ gateList: gateList })
        } catch (error) {
            Alert.alert('提示','列表数据出错')
        }
    }

    // 无内容
    renderNoContent(){
        return(
            <View style={{alignItems:'center',marginTop: '40%'}}>
				<Text style={{fontSize: 14,color: Colors.themeTextLightGray}}>暂无设备</Text>
                <TouchableOpacity style={styles.reconnectTouch} onPress={()=>{
                    this.requestForData()
                }}>
                    <Text style={{fontSize: 14, color: Colors.tabActiveColor}}>重新获取数据</Text>
                </TouchableOpacity>
			</View>
        )
    }

    renderRowItem(item, index){
        const {navigate} = this.props.navigation
        const Colors = this.props.isDark ? ColorsDark : ColorsLight

        return(
            <View style={{paddingHorizontal: 16}}>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.item,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        navigate('TCPDeviceList',{
                            order: item.order,
                            gateId: item.id,
                            type: item.type,
                            gateName: item.name
                        })
                    }}
                >
                    <Text style={[styles.name,{color: Colors.themeText}]}>{item.name}</Text>
                </TouchableOpacity> 
            </View>
        )
    }

    // 设备列表
	renderRoomList(){
		if(this.state.loading && !this.state.gateList){
			return <ListLoading/>
		}
		if(!this.state.gateList){
			return <ListError onPress={()=>{ this.requestForData() }}/>
		}
		Footer = this.state.gateList.length > 0 ? FooterEnd: null

		return (
			<FlatList
				contentContainerStyle={{ paddingBottom: 20}}
				scrollIndicatorInsets={{right: 1}}
				removeClippedSubviews={false}
                data={this.state.gateList}
                keyExtractor={(item, index)=> 'devices_index_'+index}
				renderItem={({item, index}) => this.renderRowItem(item, index)}
				initialNumToRender={10}
                ListFooterComponent={Footer}
                ListEmptyComponent = {this.renderNoContent()}
                // refreshControl={
                //     <RefreshControl
                //         colors={[Colors.themeBGInactive]}
                //         tintColor={Colors.themeBGInactive}
                //         refreshing={this.state.isRefreshing}
                //         onRefresh={this.onRefresh.bind(this)}
                //     />
                // }
			/>
        )
	}
	
	render() {
		return (
            <View style={styles.container}>
                {this.renderRoomList()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
        flex: 1
    },
    item:{
        width: '100%',
        height: 55,
        paddingHorizontal: 16,
        marginTop: 15,
        alignItems: 'center',
        borderRadius: 5,
        flexDirection: 'row'
    },
    name:{
        fontSize: 15,
        fontWeight: 'bold'
    },
    reconnectTouch:{
        paddingHorizontal: 20,
        paddingVertical: 20,
        marginTop: 20
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(TCPGateList)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Alert,
	DeviceEventEmitter,
	Modal,
    TextInput,
    NativeModules,
    NativeEventEmitter
} from 'react-native';
import { connect } from 'react-redux';
import { Colors, NetUrls,NotificationKeys } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from '../../common/CustomComponent/ToastManager';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import ActionSheet from 'react-native-actionsheet';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import {ImagesLight, ImagesDark} from '../../common/Themes';
import SwitchButton from '../../common/CustomComponent/SwitchButton';

const JKRNUtils = NativeModules.JKRNUtils;
const ShortCutManager = Platform.select({
    ios: new NativeEventEmitter(JKRNUtils),
    android: null
})

class DeviceSetting extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
			headerLeft: <HeaderLeft navigation={navigation}/>,
			headerTitle: <HeaderTitle title={'设备设置'}/>,
            headerBackground: <HeaderBackground/>
		}
    }
    
	constructor(props) {
		super(props);
		const { getParam } = props.navigation;
		this.deviceData = getParam('deviceData')
        this.nameCallBack = getParam('nameCallBack')
        this.collectCallBack = getParam('collectCallBack')
		this.isFromManager = getParam('isFromManager')
		this.state = {
			deviceData:this.deviceData || {},
			datetime: null,
            sceneModalVisible: false,
            relationModalVisible: false,
            sceneData: [],
            name: '',
            relationDevices: null,
            pickerData: [1,2,3,4,5,6,7,8],
            selectedValue: [],
            addressModal: false
		}
	}

	componentDidMount() {
		this.getSettingData()
		this.sceneInfoNoti = DeviceEventEmitter.addListener(NotificationKeys.kSceneInfoNotification, () => {
			this.getSettingData()
        })
        //添加快捷指令成功回调
        if(Platform.OS == 'ios'){
            this.shortcutsNoti = ShortCutManager.addListener(NotificationKeys.kRNAddSiriSuccess,(data)=>{
                if(data == 1){
                    ToastManager.show('添加快捷成功')
                }else if(data == 2){
                    ToastManager.show('快捷已更新')
                }else if(data == 3){
                    ToastManager.show('删除快捷成功')
                }
            })
        }
	}

	componentWillUnmount(){
        this.sceneInfoNoti.remove()
        if(Platform.OS == 'ios'){
            this.shortcutsNoti.remove()
        }
	}

	//获取设备参数
	async getSettingData() {
        SpinnerManager.show()
		try {
			let deviceData = await postJson({
				url: NetUrls.setDevice,
				params: {
					id: this.deviceData?.deviceId
				}
			});
            SpinnerManager.close()
			if (deviceData.code == 0) {
				this.setState({
					deviceData: deviceData.result || {},
                    sceneData: (deviceData.result && deviceData.result.sceneList) || [],
                    relationDevices: deviceData.result && deviceData.result.relationDevices,
                    name: deviceData.result.name
				})
			} else {
				ToastManager.show(deviceData.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }
    
    // 设备改名改图标
	async updateDevice(name,icon,imgId) {
        if(!this.state.name){
            ToastManager.show('设备名不能为空')
            return
        }
        SpinnerManager.show()
        let params = {}
        if(icon){
            params.icon = icon
		}
		if(imgId) {
			params.imgId = imgId
        }
        if(name){
            params.name = name
        }
		try {
			let data = await postJson({
				url: NetUrls.saveDevice,
				params: {	
                    id: this.deviceData?.deviceId,
					...params
				}
			});
            SpinnerManager.close()
            this.getSettingData()
			if (data.code == 0) {
                name && this.nameCallBack && this.nameCallBack(name)
				DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	// 修改设备参数(parmas: 需要修改的参数)
	async updateDeviceParams(parmas, callBack){
		SpinnerManager.show()
        parmas = parmas || {}
		try {
			let deviceData = await postJson({
				url: NetUrls.saveDevice,
				params: {
					id: this.deviceData?.deviceId,
					...parmas
				}
			});
			SpinnerManager.close()
			if (deviceData.code == 0) {
				callBack && callBack()
			} else {
				ToastManager.show(deviceData.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
	}

	// 锁定设备
	async requestLockDevice(lock){
		SpinnerManager.show()
		const { popToTop,pop } = this.props.navigation
		try {
			let deviceData = await postJson({
				url: NetUrls.lockDevice,
				params: {
					id: this.deviceData && this.deviceData.deviceId,
					islock: lock
				}
			});
			SpinnerManager.close()
			if (deviceData.code == 0) {
				let warningText = lock ? '设备已被锁定':'设备已解锁'
				this.getSettingData()
				DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
				ToastManager.show(warningText)
			} else {
				ToastManager.show(deviceData.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误')
		}
	}

	// 停用设备
	async deleteDevice(){
		const { popToTop,pop,navigate } = this.props.navigation
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.deletDeviceV2,
				params: {
					id: this.deviceData?.deviceId,
					type: 1 // 1 停用, 2  启用 
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
				ToastManager.show('设备已停用')
				DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
				if(this.isFromManager){
					pop()
				}else{
					pop(2)
				}
			} else if(data.code == 1000){
                // 如果该设备在场景或智能下，需要去确认
				navigate('UnbindGateway',{
                    title: '停用设备', 
                    type: 1, 
                    isFromManager: this.isFromManager,
                    deviceData: data.result,
                    ids: [this.deviceData?.deviceId]
                })
			} else {
                ToastManager.show(data.msg)
            }
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	// 设备置顶
	async setTopDevice(target) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.setTopScene,
				params: {
					id: this.deviceData && this.deviceData.deviceId,
					istop: target,
					type: 2
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				let text = target ? '置顶成功' : '取消置顶'
				this.getSettingData()
				DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
				ToastManager.show(text)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误');
		}
    }
    
    // 收藏设备
    async requestCollect(target){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.collect,
				params: {
                    ids: this.deviceData && this.deviceData.deviceId,
                    type: 1,
                    isAdd: target
                }
            });
            SpinnerManager.close()
			if (data.code == 0) {
                this.getSettingData()
                this.collectCallBack && this.collectCallBack(target)
                DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
	}

	// 清除数据
	async cleanData(){
		SpinnerManager.show()
		try {
			let deviceData = await postJson({
				url: NetUrls.clearData,
				params: {
					id: this.deviceData && this.deviceData.deviceId,
					type: 1
				}
			});
			SpinnerManager.close()
			if (deviceData.code == 0) {
				ToastManager.show('数据清除成功')
			} else {
				ToastManager.show(deviceData.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误，请稍后重试')
		}
    }

    //背光灯开关
    async controlBackLightMode(status){
        SpinnerManager.show()
		try {
			let deviceData = await postJson({
				url: NetUrls.controlBackLightMode,
				params: {
					id: this.deviceData?.deviceId,
					backLightStatus: status
				}
			});
			SpinnerManager.close()
			if (deviceData.code == 0) {
				this.state.deviceData.backLightMode = status
                this.setState({
                    deviceData: this.state.deviceData
                })
			} else {
				ToastManager.show(deviceData.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误，请稍后重试')
		}
    }

	getHeaderBtn(icon){
		const {navigate} = this.props.navigation
		const Colors = this.props.themeInfo.colors;
        const isJuniorUser = this.props.userInfo.memberType == 2
		
		return(
			<View  style={[styles.headerTouch, {backgroundColor:Colors.themeBg}]}>
                <TextInput
                    style={[styles.nameInput,{color:Colors.themeText}]}
                    defaultValue={this.state.name}
                    clearButtonMode={'while-editing'}
                    maxLength={16}
                    returnKeyType={'done'}
                    editable={!isJuniorUser}
                    onChangeText={(text) => {
                        this.setState({
                            name: text
                        })
                    }}
                    onBlur={()=>{
                        this.updateDevice(this.state.name,null, null)
                    }}
                />
                <TouchableOpacity activeOpacity={0.7} style={styles.iconTouch} onPress={()=>{
                    if(isJuniorUser) return
                    navigate('SelectThumb', { 
                        thumbClass: 1, 
                        callBack: (data) => {
                            this.updateDevice(null,data.img, data.id)
                        }
                    })
                }}>
                    <Image style={{width:42,height:42,marginRight:10,resizeMode:'contain'}} source={{uri: icon}}/>
				    {isJuniorUser ? null : <Image style={styles.rightArr} source={require('../../images/enterLight.png')}/>}
                </TouchableOpacity>
			</View>
		)
	}

	getSection(itemsData){
        const Colors = this.props.themeInfo.colors;
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight;
        
		let count = itemsData.length
		let realIndex = -1
		let items = itemsData.map((val,index)=>{
			realIndex += 1
			if(val.isHidden){
				count = count - 1
				realIndex = realIndex -1
				return null
			}
			let arrow = null
			let splitLine = <View style={{width:'100%',height:1,backgroundColor:Colors.split}}/>
			let switchBtn = null
			if(val.showArrow){
				arrow = <Image style={{width:10,height:16,marginRight:16}} source={require('../../images/enterLight.png')}/>
			}
			if(index == 0 || count <= 1 || realIndex <= 0){
				splitLine = null
			}
			if(val.switchStatus == 0 || val.switchStatus == 1){
                switchBtn = (
                    <SwitchButton
                        value = {val.switchStatus}
                        isAsync = {true}
                        onPress = {()=>{
                            val.action && val.action()
                        }}
                    />
                )
            }
            //是否漂白
            let tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}
            if(val.notTint){
                tintColor = {}
            }
			return(
				<View key={'device_stting_item_'+index} style={{alignItems:'center',paddingLeft:16}}>
					{splitLine}
					<TouchableOpacity activeOpacity={0.8}  style={styles.item} onPress={()=>{
						if(val.switchStatus == 0 || val.switchStatus == 1){
							return
						}
						val.action && val.action()
					}}>
						<Image style={[{width:22,height:22},tintColor]} source={val.icon}/>
						<Text style={{marginLeft:15,fontSize:15,color:Colors.themeText,flex:1}}>{val.name}</Text>
						<Text style={{marginRight:10,fontSize:13,color:Colors.themeTextLight}}>{val.extra}</Text>
						{arrow}
						{switchBtn}
					</TouchableOpacity>
				</View>
			)
		})
		if(count <= 0 ){
			return null
		}else{
			return(
				<View style={[styles.itemWrapper, {backgroundColor: Colors.themeBg}, count<=1?{paddingVertical:0}:null]}>
					{items}
				</View>
			)
		}
	}

	getSceneList(){
		const {navigate} = this.props.navigation
		const Colors = this.props.themeInfo.colors;

		let list = this.state.sceneData.map((val,index)=>{
			let roomNameView = null
			if(val.roomName && val.floorText){
				roomNameView = <Text style={[styles.roomName, {color:Colors.themeTextLight}]}>{val.floorText+'  '+val.roomName}</Text>
			}
			return(
                <TouchableOpacity 
                    activeOpacity={0.8} 
                    key={'scene_list_'+index} 
                    style={[styles.sceneWrapper, {backgroundColor:Colors.themeBg}]} 
                    onPress={()=>{
					    navigate('SceneSetting', { title: '场景设置', sceneId: val.id, sceneData:val })
				    }}
                >
					<View>
						<Text style={[styles.itemName, {color:Colors.themeText}]}>{val.name}</Text>
						{roomNameView}
					</View>
					<Image style={{width:8,height:12,resizeMode:'contain'}} source={require('../../images/enterLight.png')}/>
				</TouchableOpacity>
			)
		})
		return list
	}

    // 关联场景Modal
	getSceneModal() {
		if(!this.state.sceneModalVisible){
			return null
		}
		const Colors = this.props.themeInfo.colors;
		
        return (
            <View style={[styles.modalBg, {backgroundColor:Colors.lightTransparent}]}>
				<View style={[styles.modalWrapper, {backgroundColor: Colors.themeBaseBg}]}>
					<View style={{flexDirection: 'row',paddingLeft:16,marginBottom: 20}}>
						<Text style={{fontSize:24,fontWeight:'bold',flex: 1, color: Colors.themeText}}>设备所在场景</Text>
						<TouchableOpacity style={{paddingHorizontal: 16}} onPress={()=>{
							this.setState({
								sceneModalVisible: false
							})
						}}>
							<Image style={{width:24,height:24,resizeMode:'contain'}} source={require('../../images/settingIcon/close.png')}/>
						</TouchableOpacity>
					</View>
					<ScrollView style={{flex: 1}} contentContainerStyle={{paddingHorizontal:16,paddingBottom: 20}}>
						{this.getSceneList()}
					</ScrollView>
				</View>
			</View>
        )
    }

    getActionSheet() {
        if(Platform.OS == 'android'){
            return
        }
		return (
			<ActionSheet
				ref={e => this.actionSheet = e}
				title={'选择需要执行的操作'}
				options={['打开设备', '关闭设备', '取消']}
				cancelButtonIndex={2}
				onPress={(index) => {
					if (index == 0) {
						JKRNUtils.addDeviceOrderToSiriShortCut(this.deviceData.deviceId, this.state.name, 1)
					} else if (index == 1) {
						JKRNUtils.addDeviceOrderToSiriShortCut(this.deviceData.deviceId, this.state.name, 0)
					}
				}}
			/>
		)
	}

    // 获取关联的设备列表
    getRelationdeviceList(){
        if(!this.state.relationDevices){
			return null
		}

		const Colors = this.props.themeInfo.colors;

		let list = this.state.relationDevices.map((val,index)=>{
			let roomNameView = null
			if(val.roomName && val.floorText){
				roomNameView = <Text style={[styles.roomName, {color:Colors.themeTextLight}]}>{val.floorText+'  '+ val.roomName}</Text>
			}
			return(
				<View key={'scene_list_'+index} style={[styles.deviceWrapper, {backgroundColor:Colors.themeBg}]} >
                    <Image style={[styles.deviceImg, {backgroundColor:Colors.themeBaseBg}]} source={{uri: val.img}}/>
					<Text style={[styles.itemName,{color:Colors.themeText, marginTop:10}]}>{val.name}</Text>
					{roomNameView}
				</View>
			)
		})
		return (
            <View style={{flexDirection:'row',flexWrap:'wrap',justifyContent:'space-between'}}>
                {list}
            </View>
        )
	}

    // 设备关联的 Modal
    getRelationDeviceModal() {
		const Colors = this.props.themeInfo.colors;

        return (
            <Modal
                animationType={'fade'}
                style={{flex:1}}
                visible={this.state.relationModalVisible}
                transparent = {true}
                onRequestClose={() => {}}
			>
                <View style={[styles.modalBg, {backgroundColor:Colors.lightTransparent}]}>
					<View style={[styles.modalWrapper, {backgroundColor: Colors.themeBaseBg}]}>
						<View style={{flexDirection: 'row',paddingLeft:16,marginBottom: 20}}>
							<Text style={{fontSize:24,fontWeight:'bold',flex: 1}}>关联的设备</Text>
							<TouchableOpacity style={{paddingHorizontal: 16}} onPress={()=>{
								this.setState({
									relationModalVisible: false
								})
							}}>
								<Image style={{width:24,height:24,resizeMode:'contain'}} source={require('../../images/settingIcon/close.png')}/>
							</TouchableOpacity>
						</View>
						<ScrollView style={{flex: 1}} contentContainerStyle={{paddingHorizontal:16,paddingBottom: 20}}>
							{this.getRelationdeviceList()}
						</ScrollView>
					</View>
				</View>
            </Modal>
        )
    }

    getAddSiriBtn(isPower){
        if(Platform.OS == 'android'){
            return {isHidden: true}
		}
        return(
            {icon: require('../../images/settingIcon/siri.png'),
                name:'添加Siri快捷',
				isHidden: !isPower,
                showArrow:true,
                notTint: true,
                action:()=>{ 
                    this.actionSheet?.show() 
                }
			}
        )
    }

    getLongstopenText(longestopen){
        if(!longestopen){
            return '永久'
        }
        longestopen = parseInt(longestopen)
        let hour = parseInt(longestopen/60);
        let min = longestopen%60;
        if(hour == 0){
            return min + '分钟'
        }else{
            if(min == 0){
                return hour + '小时'
            }else{
                return hour + '小时' + min + '分钟'
            }
        }
    }

	getSettingList() {
		const {navigate,pop,push} = this.props.navigation

		let deviceData = this.state.deviceData
		//设置的参数
		let { longestopen, overcurrent, switchbinding, floorText, roomName,  
            controllerName, ishidden, islock, istop,  timerCount, isPower, 
            isCollect, dayImg, nightImg, showMenu, noHumanTimeout, powerOnStatusValue,
			labelCount, unwindPositive, backLightMode
        } = deviceData
        
        //设备图标
        let deviceIcon = this.props.themeInfo.isDark ? nightImg : dayImg

		// 该隐藏的选项
		showMenu = showMenu || []

		// 最长开启
		let longstopenText = this.getLongstopenText(longestopen)

		// 过载电流
		let current = (overcurrent && overcurrent + 'A') || ''

		// 定时
		let timingText = timerCount || 0
		timingText = timingText + '个'
		
		// 所在场景
		let scenenExtaText = this.state.sceneData.length>0 ? this.state.sceneData.length +'个场景' : ''

		// 是否置顶
        istop = istop ? 1:0
        
        // 收藏
        isCollect = isCollect ? 1:0

		// 开关绑定
        switchbinding = switchbinding ? switchbinding:0
        
        //位置
        let siteText = floorText ?  (floorText + '/' + roomName) : null

        // 无人时间
        noHumanTimeout = noHumanTimeout ? noHumanTimeout+'秒' : ''

        //电机反转
        unwindPositive = unwindPositive ? 1:0

        // 通电状态
        powerOnStatusText = ''
        if(powerOnStatusValue == 0){
            powerOnStatusText = '常亮'
        }else if(powerOnStatusValue == 1){
            powerOnStatusText = '常灭'
        }else if(powerOnStatusValue == 2){
            powerOnStatusText = '记忆'
        }

        //普通成员
        const isJuniorUser = this.props.userInfo.memberType == 2

		return (
			<View style={{width:'100%',paddingHorizontal:16,paddingVertical:10}}>
				{this.getHeaderBtn(deviceIcon)}
				
				{this.getSection([
					{icon: require('../../images/settingIcon/settop.png'),name:'置顶',extra:'',switchStatus:istop,
						isHidden: showMenu.indexOf('hide_device') == -1,
						showArrow:false,action:()=>{ 
							if(ishidden){
								ToastManager.show('设备被隐藏,无法置顶')
								return
							}else if(islock){
								ToastManager.show('设备被锁定,无法置顶')
								return
							}
							let target = istop ? 0:1 
							this.setTopDevice(target)
						}
					},
					{icon: require('../../images/settingIcon/hidden.png'),name:'隐藏设备',extra:'',switchStatus:ishidden,
						isHidden: showMenu.indexOf('top') == -1,
						showArrow:false,action:()=>{ 
							if(istop){
								ToastManager.show('设备被置顶,无法隐藏')
								return
							}
							let target = ishidden ? 0:1 
                            this.updateDeviceParams({ishidden: target},()=>{
                                let warningText = target ? '设备已被隐藏':'取消隐藏'
                                ToastManager.show(warningText)

                                this.state.deviceData.ishidden = target
                                this.setState({
                                    deviceData: this.state.deviceData
                                })
				                DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
                            })
						}
					},
                    {icon: require('../../images/settingIcon/lock.png'),name:'锁定设备',extra:'',switchStatus:islock,
                        isHidden: showMenu.indexOf('lock') == -1,
						showArrow:false,action:()=>{ 
							if(istop){
								ToastManager.show('设备被置顶,无法锁定')
								return
							}
							let target = islock ? 0:1 
							this.requestLockDevice(target)
						}
                    },
                    {icon: require('../../images/settingIcon/collect.png'),name:'收藏',extra:'',switchStatus:isCollect,
						isHidden: showMenu.indexOf('favorite') == -1,
						showArrow:false,action:()=>{ 
							let target = isCollect ? false:true
							this.requestCollect(target)
						}
					},
				])}
				{this.getSection([
                    {icon: require('../../images/settingIcon/room.png'),name:'位置',extra:siteText,
                        isHidden: showMenu.indexOf('location') == -1,
						showArrow: !isJuniorUser , action:()=>{
                            if(isJuniorUser) return
                            navigate('Seting_Room',{
                                excuteNetwork:true,
                                deviceData:this.state.deviceData,
                                callBack:()=>{
							        this.getSettingData()
                                }
                            })
                        }
					},
                    {icon: require('../../images/settingIcon/scene.png'),name:'所在场景',extra:scenenExtaText,
                        isHidden: showMenu.indexOf('sceneList') == -1,
						showArrow:true,action:()=>{
							if(this.state.sceneData.length <= 0){
								ToastManager.show('该设备暂未关联场景')
								return
							}
							this.setState({
								sceneModalVisible: true
							})
						}
                    },
                    {icon: require('../../images/settingIcon/device_relation.png'),name:'设备关联',extra:scenenExtaText,
                        isHidden: !this.state.relationDevices,
						showArrow:true,action:()=>{
							this.setState({
								relationModalVisible: true
							})
						}
					},
                    {icon: require('../../images/settingIcon/dependency.png'),name:'设备依赖',extra:'',
                        isHidden: showMenu.indexOf('dependency') == -1,
						showArrow:true,action:()=>{
							navigate('Setting_Dependency',{
                                isJuniorUser: isJuniorUser,
                                deviceData:this.state.deviceData
                            })
						}
					},
				])}
				{this.getSection([
					{icon: require('../../images/settingIcon/timing.png'),name:'定时',extra:timingText,
						isHidden: showMenu.indexOf('timing') == -1,
						showArrow:true,action:()=>{ 
                            navigate('Setting_Timing',{
                                isJuniorUser: isJuniorUser,
                                deviceData:this.state.deviceData,
                                callBack:()=>{
							        this.getSettingData()
						        }} 
                            )
                        }
					},
					{icon: require('../../images/settingIcon/countdown.png'),name:'最长开启',extra:longstopenText,
						isHidden: showMenu.indexOf('longestopen') == -1,
						showArrow: !isJuniorUser,action:()=>{
                            if(isJuniorUser) return
                            navigate('Setting_Maxopenning',{ 
                                deviceData:this.state.deviceData,
                                longstopenText: longstopenText,
                                callBack:()=>{
                                    this.getSettingData()
						        }} 
                            )
                        } 
                    },
                    {icon: require('../../images/settingIcon/device_sense.png'),name:'设置无人时间',extra: noHumanTimeout,
						isHidden: showMenu.indexOf('noHumanTimeout') == -1,
						showArrow: true,action:()=>{
                            navigate('Setting_NobodyTime',{ 
                                deviceData:this.state.deviceData,
                                callBack:()=>{
                                    this.getSettingData()
						        }}
                            )
                        } 
                    },
                    {icon:require('../../images/settingIcon/paramSetting.png'),name:'参数配置',extra:'',
                        isHidden: showMenu.indexOf('param_config') == -1,
						showArrow:true,action:()=>{
                            navigate('Setting_ParamsSetting',{
                                deviceData:this.state.deviceData,
                                callBack:()=>{
							        this.getSettingData()
						        }
                            })    
                        }
					},
                    {icon:require('../../images/settingIcon/attributes.png'),name:'属性停启用',extra:'',
                        isHidden: showMenu.indexOf('disabledAttributes') == -1,
						showArrow:true,action:()=>{navigate('Setting_Attributes',{deviceData:this.state.deviceData,callBack:()=>{
							this.getSettingData()
						}} )} 
					},
                    {icon:require('../../images/settingIcon/price.png'),name:'电价设置',extra:'',
                        isHidden:  showMenu.indexOf('electric_price') == -1,
						showArrow:true,action:()=>{navigate('PriceSetting',{deviceData:this.state.deviceData,callBack:()=>{
							this.getSettingData()
						}} )} 
					},
                    {icon:require('../../images/settingIcon/powerStatus.png'),name:'上电状态',extra: powerOnStatusText,
                        isHidden: showMenu.indexOf('powerOnStatus') == -1,
						showArrow:true,action:()=>{navigate('Setting_PowerStatus',{
                            deviceData:this.state.deviceData,
                            powerOnStatusValue: powerOnStatusValue,
                            callBack:()=>{
							    this.getSettingData()
						    }
                        })} 
					},
                    {icon: require('../../images/settingIcon/deverse.png'),name:'电机反转',extra:'',switchStatus:unwindPositive,
						isHidden: showMenu.indexOf('reverse') == -1,
						showArrow:false,action:()=>{ 
							let target = unwindPositive ? 0:1
                            this.updateDeviceParams({unwindPositive: target}, ()=>{
                                this.state.deviceData.unwindPositive = target
                                this.setState({
                                    deviceData: this.state.deviceData
                                })
                            })
						}
					},
                    {icon: require('../../images/settingIcon/led.png'),name:'面板待机背光灯',extra:'',switchStatus: backLightMode,
						isHidden: showMenu.indexOf('backLightMode') == -1,
						showArrow:false,action:()=>{ 
							let target = backLightMode ? 0:1
                            this.controlBackLightMode(target)
						}
					},
                    // {icon:require('../../images/settingIcon/electric_current.png'),name:'过载电流',extra:current,
                    //     isHidden: showMenu.indexOf('overelectric') == -1,
					// 	showArrow:true,action:()=>{navigate('Setting_Overelectric',{deviceData:this.state.deviceData,callBack:()=>{
					// 		this.getSettingData()
					// 	}} )} 
					// },
                    // {icon:require('../../images/settingIcon/volt.png'),name:'过压/欠压',extra:voltage_scope,
                    //     isHidden: showMenu.indexOf('voltage') == -1,
					// 	showArrow:true,action:()=>{navigate('Setting_Voltage',{ deviceData:this.state.deviceData,callBack:()=>{
					// 		this.getSettingData()
					// 	}} )} 
					// },
					// {icon:require('../../images/settingIcon/warning.png'),name:'超时提醒',extra:'未设定',showArrow:true,action:()=>{navigate('Setting_Timeout') } },
				])}
				{this.getSection([
                    this.getAddSiriBtn(isPower),
					{icon: require('../../images/settingIcon/swtich_biding.png'),name:'开关绑定',extra:switchbinding+'个 ',
						isHidden: (showMenu.indexOf('switchbinding') == -1) || isJuniorUser,
						showArrow: true, action:()=>{
                            navigate('SwitchList',{ 
                                deviceData:this.state.deviceData,
                                callBack:()=>{
                                    this.getSettingData()
						        }} 
                            )
                        }
					},
					{icon: require('../../images/settingIcon/remark.png'),name:'设备别名', extra:labelCount+'个 ',
						isHidden: (showMenu.indexOf('deviceOtherName') == -1) || isJuniorUser,
						showArrow: true, action:()=>{
                            navigate('SettingOtherName',{
								title: '设备别名',
								id: this.deviceData?.deviceId,
                                type: 1,
                                callBack:()=>{
                                    this.getSettingData()
						        }} 
                            )
                        }
					},
                    {icon: require('../../images/settingIcon/controller.png'),name:'所属网关',extra:controllerName,
                        isHidden: showMenu.indexOf('controller') == -1,
						showArrow:true,action:()=>{
							DeviceEventEmitter.emit(NotificationKeys.kGatewayNeedUpdate,{deviceId: this.deviceData?.deviceId})
                            navigate('Setting_AboutController',{ 
                                deviceData:this.state.deviceData 
                            }) 
                        } 
					},
                    {icon: require('../../images/settingIcon/log.png'),name:'日志',extra:'',
                        isHidden: showMenu.indexOf('log') == -1,
                        showArrow:true,action:()=>{
                            navigate('Setting_Log',{
                                deviceId: this.state.deviceData.id,
                                attributeType: this.deviceData?.attributeTypeName
                            })
					    }
                    },
				])}
			</View>
		)
    }
    
    getRecoverButton(){
        const { showMenu } = this.state.deviceData
        const Colors = this.props.themeInfo.colors

        if(!showMenu || showMenu.indexOf('restoreFactory') == -1){
            return null
        }

        return(
            <TouchableOpacity activeOpacity={0.7} style={[styles.deleteTouch, {backgroundColor:Colors.themeBg}]} onPress={() => {
                Alert.alert(
                    '提示',
                    '确定要恢复出厂设置?',
                    [
                        {text: '取消', onPress: () => {}, style: 'cancel'},
                        {text: '确定', onPress: () => { 
                            this.updateDeviceParams({restoreFactory: 1},()=>{
                                ToastManager.show('恢复出厂设置成功!')
                            })
                        }},
                    ]
                )
            }}>
                <Text style={{ color: Colors.themeButton,fontSize:15 }}>恢复出厂设置</Text>
            </TouchableOpacity>
        )

    }

	getDeleteButton() {
        const { showMenu } = this.state.deviceData
        const Colors = this.props.themeInfo.colors;

        if(!showMenu || showMenu.indexOf('inactivate') == -1){
            return null
        }
        if(this.props.userInfo.memberType == 2){
            return null
        }
        
        return (
			<View style={styles.bottomBtnWrapper}>
                {this.getRecoverButton()}
				<TouchableOpacity activeOpacity={0.7} style={[styles.deleteTouch, {backgroundColor:Colors.themeBg}]} onPress={() => {
					Alert.alert(
						'提示',
						'确认停用该设备?',
						[
							{text: '取消', onPress: () => {}, style: 'cancel'},
							{text: '停用', onPress: () => { this.deleteDevice() }},
						]
					)
				}}>
					<Text style={{ color: Colors.red,fontSize:15 }}>停用</Text>
				</TouchableOpacity>
			</View>
		)
    }

	render() {
		const Colors = this.props.themeInfo.colors;

		return (
			<View style={{backgroundColor: Colors.themeBaseBg, flex: 1}}>
				<ScrollView contentContainerStyle={styles.scrollContainer}>
					{this.getSettingList()}
					{this.getDeleteButton()}
				</ScrollView >
				{this.getSceneModal()}
                {this.getRelationDeviceModal()}
                {this.getActionSheet()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	scrollContainer:{
		paddingBottom: 50
	},
	headerTouch:{
		flexDirection:'row',
		height:60,
		borderRadius:4,
		alignItems:'center'
	},
	itemWrapper:{
		width: '100%',
		paddingVertical:5,
		borderRadius: 4,
		marginTop:15
	},
	item:{
		height:54,
		width:'100%',
		flexDirection:'row',
		alignItems:'center'
	},
	bottomBtnWrapper: {
		marginTop: 30,
		width: '100%',
		paddingHorizontal: 16,
		paddingVertical: 10,
	},
	deleteTouch: {
        width: '100%',
        height: 44,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        marginTop: 10
	},
	modalBg:{
		width:'100%',
		height:'100%',
		justifyContent:'center',
		alignItems:'center',
		position:'absolute',
		elevation: 10,
		zIndex:100
	},
	modalWrapper:{
		width:'90%',
		height:'80%',
		borderRadius:5,
		paddingTop: 20
	},
	sceneWrapper:{
		marginTop:15,
		width:'100%',
		height:65,
		flexDirection: 'row',
		justifyContent:'space-between',
		alignItems:'center',
		paddingHorizontal: 16
    },
    deviceWrapper:{
        marginTop:10,
		width:'46%',
		height:150,
        alignItems:'center',
        justifyContent: 'center',
        paddingHorizontal: 16,
        borderRadius: 4
    },
    iconTouch:{
        paddingLeft: 10,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        height:'100%',
        marginLeft:10
    },
    deviceImg:{
        width:50,
        height:50,
        resizeMode:'contain',
    },
    itemName: {
        fontSize:15,
        fontWeight:'bold'
    },
    roomName:{
        marginTop:10,
        fontSize:14,
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    rightArr:{
        width:10,
        height:16,
        marginRight:20
    },
    nameInput:{
        marginLeft:20,
        flex:1,
        fontSize:18,
        fontWeight:'bold'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
        userInfo: state.userInfo
    })
)(DeviceSetting)

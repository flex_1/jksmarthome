/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
    DeviceEventEmitter,
    NativeEventEmitter,
    NativeModules
} from 'react-native';
import { postJson } from '../../util/ApiRequest';
import { NetUrls, Colors, NotificationKeys, NetParams } from '../../common/Constants';
import DeviceList from '../Home/DeviceList';
import ToastManager from '../../common/CustomComponent/ToastManager';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import {ColorsLight, ColorsDark} from '../../common/Themes';
import PopUp from '../../common/CustomComponent/PopUp';
import FilterModal from '../CommonPage/FilterModal';
import FloorScrollTab from '../../common/CustomComponent/FloorScrollTab';

const JKRNUtils = NativeModules.JKRNUtils;
const ShortCutManager = Platform.select({
    ios: new NativeEventEmitter(JKRNUtils),
    android: null
})

class Device extends Component {

	static navigationOptions = ({ navigation }) => {
        const addBtn = {icon:require('../../images/add.png'),onPress:()=>{
            navigation.getParam('dismissFilter')()
            navigation.getParam('addBtnClick')()
        }}
        const logBtn = {icon: require('../../images/log.png'),title: '设备日志',onPress:()=>{
            navigation.navigate('Logs',{title:'设备日志',type:'device'})
        }}
        const rightBtn = navigation.getParam('isJuniorUser') ? logBtn : addBtn

        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'全部设备'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {icon:require('../../images/search.png'),onPress:()=>{
                        navigation.getParam('dismissPop')()
                        navigation.getParam('dismissFilter')()
                        navigation.navigate('SearchPage',{searchType: 'Device'})
                    }},
                    rightBtn
                ]}/>
            ),
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
	}

	constructor(props) {
		super(props);
        const { getParam,setParams,navigate } = props.navigation;

        this.shortCutsDeviceId = getParam('shortCutsDeviceId')
        this.shortCutsDeviceName = getParam('shortCutsDeviceName')
        this.shortCutsStatus = getParam('shortCutsStatus')
        this.isJuniorUser = getParam('isJuniorUser')

		this.state = {
			currentFloor: 0,
            currentIndex: 0,
            floors: [{ floor: 0, floorText: '整屋' }],
            numColumns: getParam('numColumns') || 1,
            popVisible: false,
            filterVisible: false,
            filters: null,
            filterSelected: [],
            specailFilters: null
        }
        
        setParams({
            addBtnClick: ()=>{ this.setState({popVisible: true}) },
            dismissPop: ()=>{ this.setState({popVisible: false}) },
            dismissFilter: ()=>{ this.setState({filterVisible: false}) }
        })
	}

	componentDidMount() {
        this.getDeviceFloor()
        this.requestFilterCategory()
		this.deviceFloorNoti = DeviceEventEmitter.addListener(NotificationKeys.kDeviceFloorNotification,()=>{
            this.requestFilterCategory()
			this.getDeviceFloor()
        })
        // iOS Siri快捷指令
        if(this.shortCutsDeviceId){
            this.updateDeviceStatus(this.shortCutsDeviceId,this.shortCutsDeviceName,this.shortCutsStatus)
        }
        if(Platform.OS == 'ios'){
            this.shortcutsNoti = ShortCutManager.addListener(NotificationKeys.kRNShortCutsNotification,(data)=>{
                if(data.type == 'excuteDevice'){
                    this.updateDeviceStatus(data.deviceId,data.deviceName,data.status)
                }
            })
        }
	}

	componentWillUnmount() {
        this.deviceFloorNoti.remove()
        if(Platform.OS == 'ios'){
            this.shortcutsNoti.remove()
        }
	}

	// 获取所有设备的楼层
    async getDeviceFloor(){
		try {
			let data = await postJson({
				url: NetUrls.deviceFloor,
				params: {
				}
			});
			if (data.code == 0) {
                this.processFloors(data)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }
    
    //开关设备
    async updateDeviceStatus(id,name,status){
        try {
			let data = await postJson({
				url: NetUrls.deviceUpdate,
				params: {
					status: status,
					id: id
				}
			});
			if (data.code == 0) {
                let tipsText = name + '已' + (status ? '打开':'关闭')
				ToastManager.show(tipsText)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    //获取筛选类型
    async requestFilterCategory(floor){
        try {
			let data = await postJson({
				url: NetUrls.natural_classification,
				params: {
                    floor: floor
				}
			});
			if (data.code == 0) {
                let filters = data.result?.categoryList || []
                let specailFilters = [{ nameEn: 'ONLINE', name: '在线', count: data.result?.onLineCount },
                                      { nameEn: 'OFFLINE', name: '离线', count: data.result?.offLineCount }]
                if(data.result?.hiddenCount > 0){
                    specailFilters.push({ nameEn: 'HIDDEN', name: '隐藏设备', count: data.result?.hiddenCount })
                }
                this.setState({
                    filters: filters,
                    specailFilters: specailFilters
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    async creatCustomDevice(){
        SpinnerManager.show()
        try {
			let data = await postJson({
				url: NetUrls.createRIStudyDevice,
				params: {
				}
            });
            SpinnerManager.close()
			if (data.code == 0) {
                const {navigate} = this.props.navigation

                navigate('RISelfLearn',{
                    title: data.result && data.result.name, 
                    deviceData: data.result || {}
                })

                DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    // 处理不同情况下 楼层问题
    processFloors(data){
        let floors = [{ floor: 0, floorText: '整屋' }]
        let selectFloor = this.state.currentFloor
        
        if(data.result && data.result.length > 0){
            if(selectFloor != 0){
                selectFloor = null
            }
			floors = floors.concat(data.result)
			// 避免设备被删除后 楼层消失
			for (const floorData of data.result) {
				if(floorData.floor == this.state.currentFloor){
                    selectFloor = floorData.floor
                    break;
				}
			}
        }
		if(selectFloor === null){
            // 这种情况表示当前选中的楼层消失了（被删除了）
            this.setState({
                floors: floors,
                currentFloor: 0,
                filterSelected: []
            },()=>{
                this.refreshDeviceList()
            })
        }else{
            this.setState({
                floors: floors,
                currentFloor: selectFloor
            })
        }		
    }

	// 刷新列表
	refreshDeviceList(params){
        this.devices_list.reloadDeviceList(params)
    }
    
    renderModal(){
        if(!this.state.popVisible){
            return null
        }
        const {navigate} = this.props.navigation
        const Colors = this.props.themeInfo.isDark ? ColorsLight : ColorsDark

        let buttonArrays = [
            {icon: require('../../images/qrScan.png'),title: '扫码添加',onPress:()=>{
                navigate('AddDeviceQRCode')
                this.setState({popVisible: false})
            }},
            {icon: require('../../images/log.png'),title: '设备日志',onPress:()=>{
                navigate('Logs',{title:'设备日志',type:'device'})
                this.setState({popVisible: false})
            }},
            {icon: require('../../images/custom.png'),title: '自定义设备',onPress:()=>{
                this.setState({popVisible: false})
                this.creatCustomDevice()
            }},
            {icon: require('../../images/meidi.png'),
                iconStyle: {width: 30,height:30,marginLeft: 10},
                title: '设备授权',
                onPress:()=>{
                    navigate('MideaAuthonList')
                    this.setState({popVisible: false})
                }
            },
        ]
        // 普通成员只能查看日志
        if(this.props.userInfo.memberType == 2){
            buttonArrays = [
                {icon: require('../../images/log.png'),title: '设备日志',onPress:()=>{
                    navigate('Logs',{title:'设备日志',type:'device'})
                    this.setState({popVisible: false})
                }}
            ]
        }

        return (
            <PopUp
                frame={{top: 0, right: 10, width: 140}}
                triangleRight={15}
                dismiss={()=>{ this.setState({popVisible: false}) }}
                bgColor={Colors.themeBg}
                textColor={Colors.themeText}
                borderColor={Colors.split}
                buttonArrays={buttonArrays}
            />
        )   
    }

    renderFilterModal(){
        if(!this.state.filterVisible){
            return null
        }
        // if(!this.state.filters || this.state.filters.length<=0){
        //     return null
        // }

        return(
            <FilterModal
                style={{top: 40}}
                isDark={this.props.themeInfo.isDark}
                filters={this.state.filters}
                specailFilters={this.state.specailFilters}
                filterSelected={this.state.filterSelected}
                specialClass={'HIDDEN'}
                confirmBtnClick={(filterSelected)=>{
                    this.setState({
                        filterVisible: false,
                        filterSelected: filterSelected
                    },()=>{
                        // let params = { naturalClassification: filterSelected.toString() }
                        this.refreshDeviceList()
                    })
                }}
                dissmissModal={()=>{
                    this.setState({filterVisible: false})
                }}
            />
        )
    }

    renderFilterBtn(){
        if(!this.state.filters || this.state.filters.length<=0){
            return null
        }

        const Colors = this.props.themeInfo.colors
        let tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}
        if(this.state.filterSelected && this.state.filterSelected.length>0){
            tintColor = {tintColor: Colors.newTheme}
        }

        return(
            <TouchableOpacity style={[styles.thumbnailTouch,{paddingLeft: 10}]} onPress={()=>{
                this.setState({filterVisible: !this.state.filterVisible})
            }}>
                <Image style={[styles.filterImg,tintColor]} source={require('../../images/deviceIcon/filter.png')}/>
            </TouchableOpacity>
        )
    }

	//头部向导
	getHeaderView() {
        let switchThumbImg = this.state.numColumns == 1 ? require('../../images/homeBG/list.png') : 
            require('../../images/homeBG/table.png')
        const Colors = this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? {tintColor: Colors.themeText} : {}
        
		return (
			<View style={[styles.sectionHead,{backgroundColor: Colors.themeBg,borderBottomColor: Colors.themeBaseBg}]}>
				<FloorScrollTab
                    floors = {this.state.floors}
                    currentFloor = {this.state.currentFloor}
                    currentIndex = {this.state.currentIndex}
                    isDark = {this.props.themeInfo.isDark}
                    onClick = {(floor, index)=>{
                        this.setState({
                            currentFloor: floor,
                            currentIndex: index,
                            filterSelected: []
                        },()=>{
                            this.requestFilterCategory(floor)
						    this.refreshDeviceList()
                        })
                    }}
                />
                <View style={styles.split}/>
                <TouchableOpacity style={[styles.thumbnailTouch,{paddingHorizontal: 10}]} onPress={()=>{
                    let target = this.state.numColumns%2 + 1
                    this.setState({ numColumns: target })
                }}>
                    <Image style={[styles.thumbImg,tintColor]} source={switchThumbImg}/>
                </TouchableOpacity>
                {this.renderFilterBtn()}
			</View>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getHeaderView()}
				<DeviceList 
					ref={e => this.devices_list = e} 
					navigation={this.props.navigation}
                    floor={this.state.currentFloor}
                    numColumns={this.state.numColumns}
                    isDark={this.props.themeInfo.isDark}
                    naturalClassification={this.state.filterSelected}
                    isJuniorUser={this.props.userInfo.memberType == 2}
                    refreshFilter={()=>{ 
                        this.setState({
                            filterSelected: []
                        },()=>{
                            this.requestFilterCategory()
                        })
                    }}
				/>
                {this.renderModal()}
                {this.renderFilterModal()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex:1
	},
	navTitle: {
		fontSize: 24,
		fontWeight: 'bold',
		marginLeft: 15,
		textAlignVertical: 'center',
		color: Colors.themeTextBlack,
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 5,
		paddingLeft: 10,
		marginRight:5,
		flexDirection:'row'
	},
	searchNavImg:{
		width: 20,
		height: 20,
		resizeMode:'contain'
	},
	navImg: {
		width: 18,
		height: 18,
		resizeMode:'contain'
	},
	navText: {
		color: Colors.themeTextBlack,
		marginLeft:5,
		fontSize: 15
	},
	sectionHead: {
		width: '100%',
		height: 40,
        flexDirection:'row',
        alignItems: 'center',
        borderBottomWidth: 1
	},
	headScroll: {
		height: '100%',
	},
	headScrollContainer: {
		alignItems: 'center',
		paddingLeft: 10,
		paddingRight: 20
	},
	deviceItem:{
		width: '50%',
		marginBottom: 5,
		paddingHorizontal: 8,
		paddingVertical: 5,
	},
	deviceIcon:{
		width:46,
		height:40,
		resizeMode:'contain'
	},
	switchTouch:{
		marginTop:10,
		width:100,
		height:30,
		borderRadius:15,
		borderColor:Colors.borderLightGray,
		borderWidth:1,
		justifyContent:'center',
		alignItems:'center'
	},
	floorTouch: {
		alignItems: 'center',
		paddingHorizontal: 10,
		marginRight: 10
	},
	dot: {
		width: 4,
		height: 4,
		backgroundColor: Colors.themeTextBlue,
		borderRadius: 2,
		marginTop: 5
	},
	floorText: {
		fontSize: 15,
		fontWeight: 'bold'
    },
    logIcon:{
        width: 17,
        height: 19,
        resizeMode: 'contain'
    },
    thumbnailTouch:{  
        height:'100%',
        alignItems: 'center',
        paddingHorizontal: 15,
        justifyContent:'center'
    },
    split:{
        marginLeft: 10,
        width: 1,
        height:'50%',
        backgroundColor:Colors.themBGLightGray
    },
    thumbImg:{
        width:16,
        height:16,
        resizeMode:'contain'
    },
    filterImg:{
        width:14,
        height:14,
        resizeMode:'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
        userInfo: state.userInfo
    })
)(Device)

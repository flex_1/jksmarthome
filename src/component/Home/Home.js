/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    DeviceEventEmitter,
    StatusBar,
    NativeModules,
    NativeEventEmitter,
    Alert
} from 'react-native';
import { postJson } from '../../util/ApiRequest';
import { NetUrls, NotificationKeys, NetParams, LocalStorageKeys, WebsocketKeys } from '../../common/Constants';
import { BottomSafeMargin, StatusBarHeight } from '../../util/ScreenUtil';
import ToastManager from '../../common/CustomComponent/ToastManager';
import { DecimalTheNumber } from '../../util/NumberUtil';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import { connect } from 'react-redux';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import LocalTokenHandler from '../../util/LocalTokenHandler';
import WebsocketCenter from '../../util/WebsocketCenter';
import FilterModal from '../CommonPage/FilterModal';
import { updateUserInfo } from '../../redux/actions/UserInfoAction';
import AsyncStorage from '@react-native-community/async-storage';
import {HSTips} from '../../common/Strings';

import NewDeviceList from '../CommonPage/New_List/NewDeviceList';
import NewSceneList from '../CommonPage/New_List/NewSceneList';
import NewSmartList from '../CommonPage/New_List/NewSmartList';

// import MessageQueue from 'react-native/Libraries/BatchedBridge/MessageQueue.js';


const JKRNUtils = NativeModules.JKRNUtils;
const ShortCutManager = Platform.select({
    ios: new NativeEventEmitter(JKRNUtils),
    android: null
})
const indiHeight = 45

class Home extends Component {

    static navigationOptions = {
        header: null,
        headerBackground: <HeaderBackground hiddenBorder={true} />
    }

    constructor(props) {
        super(props);
        this.state = {
            homeInfo: {},
            enviInfo: {},
            totalEnergy: 0,
            currentEnergy: 0,
            indicators: [{ name: '场景', type: 1 }, { name: '设备', type: 2 }, { name: '智能', type: 3 }],
            currentIndicator: 1,
            numColumns: 2,   // 1-显示大图列表  2-显示缩略图
            houseChaning: false,

            filterVisible: false,
            filters: [],
            filterSelected: [],
            specailFilters: null
        }
    }

    componentWillMount() {
        // const spyFunction = (msg) => {
        //     console.log(msg);
        // }
        // MessageQueue.spy(spyFunction);

        this.props.updateUserInfo()

        //处理 shortcuts
        if (Platform.OS == 'ios') {
            this.shortcutsNoti = ShortCutManager.addListener(NotificationKeys.kRNShortCutsNotification, (data) => {
                const { navigate } = this.props.navigation
                if (data.type == 'scene') {
                    navigate('Scene', { title: '全部场景' })
                } else if (data.type == 'device') {
                    navigate('Device', { title: '全部设备' })
                } else if (data.type == 'excuteScene') {
                    //执行某个场景
                    navigate('Scene', { title: '全部场景', shortCutsSceneId: data.sceneId, shortCutsSceneName: data.sceneName })
                } else if (data.type == 'excuteDevice') {
                    //执行某个设备
                    navigate('Device', {
                        title: '全部设备',
                        shortCutsDeviceId: data.deviceId,
                        shortCutsDeviceName: data.deviceName,
                        shortCutsStatus: data.status
                    })
                }
            })
        }
        this.settingStatusBar()
    }

    settingStatusBar() {
        // 状态栏 样式
        if (this.props.themeInfo.isDark) {
            StatusBar.setBarStyle('light-content')
        } else {
            StatusBar.setBarStyle('dark-content')
        }
    }

    componentDidMount() {
        this.checkLocalTokenInfo()
        this.requestHouseInfo()
        this.requestFilterCategory()
        // 首页常用场景 监听
        this.houseInfoNoti = DeviceEventEmitter.addListener(NotificationKeys.kHouseInfoNotification, () => {
            this.requestHouseInfo()
        })
        // 语音相关操作
        this.enterVoiceSetting = DeviceEventEmitter.addListener(NotificationKeys.kEnterVoiceSetting, () => {
            const { navigate } = this.props.navigation
            navigate('VoiceSetting', {
                fromVoicePop: true
            })
        })
        // 重新连接websocket
        this.reconnectWebsocketNoti = DeviceEventEmitter.addListener(NotificationKeys.kWebsocketReconnectNotification, () => {
            this.reconnectWebsocket()
        })
        // 断开当前websocket
        this.shutdownWebsocketNoti = DeviceEventEmitter.addListener(NotificationKeys.kWebsocketCloseNotification, () => {
            this.ws?.shutDownWebsocket()
        })
        this.initWebsocket()
        // 处理推送事件通知
        this.handlePushEvent()
    }

    componentWillUnmount() {
        this.ws && this.ws.shutDownWebsocket()
        this.houseInfoNoti.remove()
        this.enterVoiceSetting.remove()
        this.reconnectWebsocketNoti.remove()
        this.shutdownWebsocketNoti.remove()
        this.jpushNoti.remove()

        if (Platform.OS == 'ios') {
            this.shortcutsNoti.remove()
        }
    }

    //处理推送通知
    handlePushEvent() {
        //处理推送通知
        this.jpushNoti = DeviceEventEmitter.addListener(NotificationKeys.kJpushMessageNotification, () => {
            this.props.navigation.navigate('User')
            this.props.navigation.navigate('Message')
        })
        if (Platform.OS == 'android') {
            if (window.appLaunchOptions) {
                this.props.navigation.navigate('User')
                this.props.navigation.navigate('Message')
                window.appLaunchOptions = null
            }
        }
    }

    //强制用户去创建房屋
    pushToCreatHouse() {
        const { navigate } = this.props.navigation
        navigate('AddHouse', {
            force: true, callBack: () => {
                this.refreshAllInfos()
            }
        })
    }

    // 检查用户Token 看用户是否需要切换房屋
    async checkLocalTokenInfo() {
        // 如果能进入首页，则后面不需要再显示 隔热信息指引弹框
        AsyncStorage.setItem(LocalStorageKeys.kPrivacyProtocol, 'true')

        let userToken = await LocalTokenHandler.get()
        if (!userToken) {
            return
        }
        if (userToken.isNewHouse) {
            this.showNewHouseAlert()
            userToken.isNewHouse = false
            LocalTokenHandler.save(userToken)
        }
    }

    // 首页房屋信息(首个请求)
    async requestHouseInfo() {
        try {
            let data = await postJson({
                url: NetUrls.houseInfo
            });
            if (data && data.code == 0) {
                if (!data.result) {
                    //不存在 默认房屋,强制用户去创建 房屋
                    this.pushToCreatHouse()
                } else {
                    // 环境数据
                    let enviInfo = {}
                    if (data.result.status) {
                        enviInfo = JSON.parse(data.result.status)
                    }
                    this.setState({
                        homeInfo: data.result,
                        enviInfo: enviInfo,
                        totalEnergy: data.result.totalEnergy || 0,
                        currentEnergy: data.result.quantity || 0
                    })
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            if (error.code == NetParams.noTokenErrorCode) {
                // 不做处理
            } else {
                ToastManager.show('网络错误')
            }
        }
    }

    //获取筛选类型
    async requestFilterCategory() {
        try {
            let data = await postJson({
                url: NetUrls.natural_classification,
                params: {
                    type: 1  // 0-所有列表 1-首页收藏列表
                }
            });
            if (data.code == 0) {
                let filters = data.result?.categoryList || []
                let specailFilters = [{ nameEn: 'ONLINE', name: '在线', count: data.result?.onLineCount },
                                      { nameEn: 'OFFLINE', name: '离线', count: data.result?.offLineCount }]
                this.setState({
                    filters: filters,
                    specailFilters: specailFilters
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    // 断开并重新连接websocket
    reconnectWebsocket() {
        this.ws && this.ws.shutDownWebsocket()
        this.initWebsocket()
    }

    // initial project websocket
    initWebsocket() {
        const ws = new WebsocketCenter()
        ws.openWebSocket((e) => {
            try {
                let socketData = JSON.parse(e.data)
                if (socketData.type == WebsocketKeys.newHouse) {
                    this.showNewHouseAlert()
                } else if (socketData.type == WebsocketKeys.newDeviceData) {
                    // 设备状态 变化的通知
                    DeviceEventEmitter.emit(NotificationKeys.kWebsocketDeviceNotification, socketData)
                } else if (socketData.type == WebsocketKeys.newEnviData) {
                    // 更新环境数据
                    this.updateEnviData(socketData)
                } else if (socketData.type == WebsocketKeys.newPowerData) {
                    // 更新耗能数据
                    this.updatePowerData(socketData)
                } else if (socketData.type == WebsocketKeys.houseBeChanged) {
                    this.houseBeChanged(HSTips.houseBeenChanged)
                } else if (socketData.type == WebsocketKeys.floorBeRefeshed) {
                    // 房屋楼层 变化的通知
                    DeviceEventEmitter.emit(NotificationKeys.kRoomFloorChangeNotification)
                } else if (socketData.type == WebsocketKeys.disableMember){
                    // 成员失效
                    this.houseBeChanged(HSTips.memberDisable)
                }
            } catch (error) {

            }
        })
        this.ws = ws
    }

    // 提示用户去选择新的房屋
    showNewHouseAlert() {
        const { navigate } = this.props.navigation
        Alert.alert(
            '提示',
            '该房屋没有任何设备，是否切换房屋？',
            [
                { text: '取消', onPress: () => { }, style: 'cancel' },
                {
                    text: '去切换', onPress: () => {
                        navigate('MyHouse', {
                            refreshAllInfos: () => {
                                this.refreshAllInfos()
                            }
                        })
                    }
                },
            ]
        )
    }

    // 更新环境数据
    updateEnviData(enviData) {
        let enviInfo = JSON.parse(JSON.stringify(this.state.enviInfo))

        enviInfo.temperature = enviData.temperature
        enviInfo.humidity = enviData.humidity
        enviInfo.PM2_5 = enviData.PM2_5
        enviInfo.CO2 = enviData.CO2
        enviInfo.CH2O_NH3 = enviData.CH2O_NH3

        this.setState({ enviInfo: enviInfo })
    }

    // 更新耗能数据
    updatePowerData(enviData) {
        if(enviData.totalEnergy !== null && enviData.totalEnergy !== undefined){
            this.setState({totalEnergy: enviData.totalEnergy})
        }
        if(enviData.quantity !== null && enviData.quantity !== undefined){
            this.setState({currentEnergy: enviData.quantity})
        }
        // this.setState({
        //     totalEnergy: enviData.totalEnergy || 0,
        //     currentEnergy: enviData.quantity || 0
        // })
    }

    //解决房屋被动切换问题
    houseBeChanged(tips) {
        if (this.state.houseChaning) {
            return
        }
        this.state.houseChaning = true
        const { popToTop, navigate } = this.props.navigation
        Alert.alert(
            '提示',
            tips,
            [
                {
                    text: '确定', onPress: () => {
                        this.state.houseChaning = false
                        popToTop()
                        navigate('Home')
                        this.refreshAllInfos()
                    }
                }
            ],
            { cancelable: false }
        )
    }

    refreshAllInfos() {
        //refresh home info
        DeviceEventEmitter.emit(NotificationKeys.kHouseInfoNotification)

        DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
        DeviceEventEmitter.emit(NotificationKeys.kSceneInfoNotification)
        DeviceEventEmitter.emit(NotificationKeys.kSmartListChangeNotification)
        DeviceEventEmitter.emit(NotificationKeys.kRoomFloorChangeNotification)

        this.props.updateUserInfo()
        this.reconnectWebsocket()
    }

    _renderTitleView() {
        const { name } = this.state.homeInfo
        const { navigate } = this.props.navigation
        const Colors = this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? { tintColor: Colors.themeText } : {}

        return (
            <TouchableOpacity activeOpacity={0.8} style={styles.titleViewTouch} onPress={() => {
                this.setState({ filterVisible: false })
                navigate('MyHouse', {
                    refreshAllInfos: () => {
                        this.refreshAllInfos()
                    }
                })
            }}>
                <Text style={[styles.titleText, { color: Colors.themeText }]}>{name || '我的房屋'}</Text>
                <View style={styles.navTouch}>
                    <Image style={[styles.navImg, tintColor]} source={require('../../images/enter.png')} />
                </View>
            </TouchableOpacity>
        )
    }

    getInfo(num, imgSrc, level) {
        if(num == null){
            return null
        }
        const Colors = this.props.themeInfo.colors
        let value = '-'
        let valueColor = Colors.themeText

        if (level == '1') {
            value = num
            valueColor = Colors.lightGreen
        } else if (level == '2') {
            valueColor = Colors.yellow
            value = num
        } else if (level == '3') {
            valueColor = Colors.red
            value = num
        }
        return (
            <View style={styles.infoWrapper}>
                <Image style={[styles.infoIcon, { tintColor: Colors.themeText }]} source={imgSrc}/>
                <Text style={[styles.infoText, { color: valueColor }]}>{value}</Text>
            </View>
        )
    }

    // 环境数据
    _renderAirInfo() {
        const { navigate } = this.props.navigation
        const { id } = this.state.homeInfo
        const enviInfo = this.state.enviInfo

        const {temperature, humidity, PM2_5_text, CH2O_NH3_text, CO2_text} = enviInfo
        if(!(temperature || humidity || PM2_5_text || CH2O_NH3_text || CO2_text)){
            return null
        }

        let roomTemperature = temperature ? (DecimalTheNumber(temperature, 1, true) + '℃') : null
        let humidityText = humidity ? (DecimalTheNumber(humidity, 2, true) + '%') : null
        let pm25 = PM2_5_text
        let formaldehyde = CH2O_NH3_text
        let co2 = CO2_text

        return (
            <TouchableOpacity activeOpacity={0.8} style={styles.enviTouch} onPress={() => {
                navigate('EnvironmentObservation', { houseId: id, title: '环境监测' })
            }}>
                {this.getInfo(roomTemperature, require('../../images/homeBG/temp.png'), enviInfo['temperature_label'])}
                {this.getInfo(humidityText, require('../../images/homeBG/humidity.png'), enviInfo['humidity_label'])}
                {this.getInfo(pm25, require('../../images/homeBG/pm.png'), enviInfo['PM2_5_label'])}
                {this.getInfo(co2, require('../../images/homeBG/co2.png'), enviInfo['CO2_label'])}
                {this.getInfo(formaldehyde, require('../../images/homeBG/formaldehyde.png'), enviInfo['CH2O_NH3_label'])}
            </TouchableOpacity>
        )
    }

    _renderDetail() {
        const Colors = this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? { tintColor: Colors.themeText } : {}
        const { navigate } = this.props.navigation
        let { cameraCount, id } = this.state.homeInfo

        let unit = '个'
        if (cameraCount == null) {
            cameraCount = '暂无'
            unit = ''
        }

        return (
            <View style={styles.detailWrapper}>
                <TouchableOpacity style={[styles.detailTouch, { flex: 2 }]} onPress={() => {
                    if (id == null) return
                    navigate('ElectricityRank', {
                        houseId: id,
                        title: '用电排行'
                    })
                }}>
                    <View style={styles.detailTouch}>
                        <Image style={[styles.detailIcon, tintColor]} source={require('../../images/homeBG/power.png')} />
                        <View>
                            <Text style={{ color: Colors.themeText, fontSize: 12 }}>
                                <Text style={{ fontSize: 16 }}>{this.state.totalEnergy}</Text> kWh
                            </Text>
                            <Text style={[styles.detailSubtitle, { color: Colors.themeTextLight }]}>总耗能</Text>
                        </View>
                    </View>
                    <View style={styles.detailTouch}>
                        <Image style={[styles.detailIcon, tintColor]} source={require('../../images/homeBG/power_now.png')} />
                        <View>
                            <Text style={{ color: Colors.themeText, fontSize: 12 }}>
                                <Text style={{ fontSize: 16 }}>{this.state.currentEnergy}</Text> kWh
                            </Text>
                            <Text style={[styles.detailSubtitle, { color: Colors.themeTextLight }]}>实时耗能</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.detailTouch, { flex: 1 }]} onPress={() => {
                    this.setState({ filterVisible: false })
                    navigate('CameraList')
                }}>
                    <Image style={[styles.detailIcon, tintColor, { width: 32, height: 32 }]} source={require('../../images/homeBG/camera.png')} />
                    <View>
                        <Text style={{ color: Colors.themeText, fontSize: 12 }}>
                            <Text style={{ fontSize: 16 }}>{cameraCount}</Text> {unit}
                        </Text>
                        <Text style={[styles.detailSubtitle, { color: Colors.themeTextLight }]}>摄像头</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    _renderFilterModal() {
        if (!this.state.filterVisible) {
            return null
        }
        if (this.state.currentIndicator != 2) {
            return null
        }
        if (!this.state.filters || this.state.filters.length <= 0) {
            return null
        }

        return (
            <FilterModal
                style={{ top: 0 }}
                isDark={this.props.themeInfo.isDark}
                filters={this.state.filters}
                specailFilters={this.state.specailFilters}
                filterSelected={this.state.filterSelected}
                confirmBtnClick={(filterSelected) => {
                    this.setState({
                        filterVisible: false,
                        filterSelected: filterSelected
                    }, () => {
                        let params = { naturalClassification: this.state.filterSelected.toString() }
                        this.new_device_list.requestDeviceList(params)
                    })
                }}
                dissmissModal={() => {
                    this.setState({ filterVisible: false })
                }}
            />
        )
    }

    _renderFilterBtn() {
        if (!this.state.filters || this.state.filters.length <= 0) {
            return null
        }
        if (this.state.currentIndicator != 2) {
            return null
        }

        const Colors = this.props.themeInfo.colors
        let tintColor = this.props.themeInfo.isDark ? { tintColor: Colors.themeText } : {}
        if (this.state.filterSelected && this.state.filterSelected.length > 0) {
            tintColor = { tintColor: Colors.newTheme }
        }

        return (
            <TouchableOpacity style={[styles.filterTouch, { paddingLeft: 10 }]} onPress={() => {
                this.setState({ filterVisible: !this.state.filterVisible })
            }}>
                <Image style={[styles.filterImg, tintColor]} source={require('../../images/deviceIcon/filter.png')} />
            </TouchableOpacity>
        )
    }

    renderHeader() {
        const Colors = this.props.themeInfo.colors

        return(
            <View style={[styles.topHeadWrapper,{backgroundColor: Colors.themeBg}]}>
                {this._renderTitleView()}
                {this._renderAirInfo()}
                {this._renderDetail()}
            </View>
        )
    }

    // React Native Head Tab View
    renderIndicator() {
        const Colors = this.props.themeInfo.colors
        const tintColor = this.props.themeInfo.isDark ? { tintColor: Colors.themeText } : {}
        const { navigate } = this.props.navigation
        let thumbIcon = this.state.numColumns == 1 ? require('../../images/homeBG/list.png') :
            require('../../images/homeBG/table.png')
        let showThumb = this.state.currentIndicator != 3

        let indicators = this.state.indicators.map((val, index) => {
            let textStyle = { color: Colors.themeTextLight, fontSize: 15 }
            if (val.type == this.state.currentIndicator) {
                textStyle = { color: Colors.themeText, fontSize: 20, fontWeight: 'bold' }
            }
            return (
                <TouchableOpacity
                    key={'indicator_index_' + index}
                    style={styles.indicatorTouch}
                    onPress={() => {
                        this.setState({
                            filterVisible: false,
                            currentIndicator: val.type
                        })
                        this.scrollableTab && this.scrollableTab.goToPage(index)
                    }}
                >
                    <Text style={textStyle}>{val.name}</Text>
                </TouchableOpacity>
            )
        })

        return (
            <View style={[styles.headerIndicator, { backgroundColor: Colors.themeBaseBg }]}>
                {indicators}
                <View style={{ flex: 1 }} />
                {this._renderFilterBtn()}
                {showThumb ? <TouchableOpacity style={styles.rightTouchL} onPress={() => {
                    let target = this.state.numColumns % 2 + 1
                    this.setState({ numColumns: target })
                }}>
                    <Image style={[styles.rightIcon, tintColor]} source={thumbIcon} />
                </TouchableOpacity> : null}
                <TouchableOpacity style={styles.rightTouchR} onPress={() => {
                    this.setState({ filterVisible: false })

                    if (this.state.currentIndicator == 1) {
                        navigate('Scene', { title: '全部场景', numColumns: this.state.numColumns })
                    } else if (this.state.currentIndicator == 2) {
                        navigate('Device', { title: '全部设备', numColumns: this.state.numColumns })
                    } else {
                        navigate('Smart')
                    }
                }}>
                    <Image style={[styles.rightIcon, tintColor]} source={require('../../images/homeBG/more.png')} />
                </TouchableOpacity>
            </View>
        )
    }

    renderListView() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={{ backgroundColor: Colors.themeBaseBg, flex: 1 }}>
                <ScrollableTabView
                    ref={e => this.scrollableTab = e}
                    initialPage={0}
                    prerenderingSiblingsNumber={0}
                    onChangeTab={(obj) => {
                        this.setState({
                            currentIndicator: obj.i + 1,
                        })
                    }}
                    renderTabBar={() => <View />}
                >
                    <NewSceneList
                        scrollable={true}
                        navigation={this.props.navigation}
                        numColumns={this.state.numColumns}
                        isDark={this.props.themeInfo.isDark}
                        isJuniorUser={this.props.userInfo.memberType == 2}
                    />
                    <NewDeviceList
                        ref={e => this.new_device_list = e}
                        scrollable={true}
                        navigation={this.props.navigation}
                        numColumns={this.state.numColumns}
                        isDark={this.props.themeInfo.isDark}
                        naturalClassification={this.state.filterSelected}
                        refreshFilter={() => {
                            this.setState({
                                filterSelected: []
                            }, () => {
                                this.requestFilterCategory()
                            })
                        }}
                    />
                    <NewSmartList
                        scrollable={true}
                        navigation={this.props.navigation}
                        isDark={this.props.themeInfo.isDark}
                    />
                </ScrollableTabView>
                {this._renderFilterModal()}
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderHeader()}
                {this.renderIndicator()}
                {this.renderListView()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    topHeadWrapper: {
        paddingTop: StatusBarHeight
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 15,
        flexDirection: 'row',
        height: 40
    },
    titleViewTouch: {
        paddingLeft: 16,
        flexDirection: 'row',
        alignItems: 'center'
    },
    titleText: {
        fontSize: 25,
        fontWeight: 'bold',
        flex: 1,
    },
    navImg: {
        width: 16,
        height: 16,
        resizeMode: 'contain'
    },
    infoWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 15
    },
    infoIcon: {
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    infoText: {
        fontSize: 14,
        marginLeft: 5,
        fontWeight: 'bold'
    },
    enviTouch: {
        flexDirection: 'row',
        paddingLeft: 16,
        marginTop: 5,
        paddingVertical: 5
    },
    detailWrapper: {
        paddingHorizontal: 16,
        paddingVertical: 16,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    detailTouch: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    detailIcon: {
        width: 26,
        height: 26,
        resizeMode: 'contain',
        marginRight: 10
    },
    detailSubtitle: {
        fontSize: 12,
        marginTop: 2
    },
    headerIndicator: {
        paddingLeft: 10,
        width: '100%',
        height: indiHeight,
        alignItems: 'center',
        flexDirection: 'row',
    },
    indicatorTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        marginRight: 10,
        paddingHorizontal: 10,
    },
    rightTouchL: {
        height: '100%',
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    rightTouchR: {
        height: '100%',
        paddingRight: 16,
        paddingLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5
    },
    rightIcon: {
        width: 16,
        height: 16,
        resizeMode: 'contain'
    },
    filterImg: {
        width: 14,
        height: 14,
        resizeMode: 'contain'
    },
    filterTouch: {
        height: '100%',
        alignItems: 'center',
        paddingHorizontal: 16,
        justifyContent: 'center'
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
        userInfo: state.userInfo
    }),
    (dispatch) => ({
        updateUserInfo: () => dispatch(updateUserInfo())
    })
)(Home)


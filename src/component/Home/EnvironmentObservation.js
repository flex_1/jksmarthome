/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	DeviceEventEmitter,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Modal
} from 'react-native';
import { postJson } from '../../util/ApiRequest';
import { NetUrls, NotificationKeys, Colors } from '../../common/Constants';
import ToastManager from '../../common/CustomComponent/ToastManager';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import Echarts from '../../third/echarts/index';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import { connect } from 'react-redux';
import { ListNoContent } from "../../common/CustomComponent/ListLoading";
import {ImagesLight, ImagesDark} from '../../common/Themes';
import { BottomSafeMargin } from '../../util/ScreenUtil';

const GoodLineColor = '#92F0DD'
const NormalLineColor = '#FFD38D'
const BadLineColor = '#FF8996'

class EnvironmentObservation extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
	}

	constructor(props) {
		super(props);
		const { getParam, navigate } = this.props.navigation
		this.houseId = getParam('houseId')
		
		this.state = {
			topSelected: null,
            indicators: ['日', '周', '月', '年'],
            topIndicators: [],
            devices: null,

            select: '日',
            currentDateType: 1,
			chartTittle:  '温度',
			chartUnit: '°C',
            currentData: 0,
            chartData: null,
            currentDevice: null,

            pickerData: [],
            
            pickerIndex: 0,
            
            isLoadingChart: false,
            modalVisible: false,

            good_interval: [],
            normal_interVal: [],
            bad_interVal: [],
            feelingsTexts: '',
            offline: true,
            homePageDeviceId: null
		}
	}

	componentDidMount() {
        this.requestAirQuality()
        this.deviceUpdateNoti = DeviceEventEmitter.addListener(NotificationKeys.kDeviceStatusNotification, ()=>{
            this.requestAirQuality()
        })
	}

	componentWillUnmount() {
		this.deviceUpdateNoti.remove()
	}

	//请求环境数据
	async requestAirQuality() {
		SpinnerManager.show()
		this.setState({isLoadingChart: true})
		try {
			let data = await postJson({
				url: NetUrls.airQualityChart,
				params: {
					type: this.state.currentDateType,
                    id: this.houseId,
                    name: this.state.topSelected,
                    deviceId: this.state.currentDevice && this.state.currentDevice.deviceId
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                if(!this.state.currentDevice){
                    let homePageDeviceId = data.result?.homePageDeviceId
                    let devices = data.result?.devices || []
                    let pickerData = []
                    let currentDevice = {}
                    for (const device of devices) {
                        pickerData.push(device.deviceName)
                        if(homePageDeviceId == device.deviceId){
                            currentDevice = device
                        }
                    }
                    this.setState({
                        devices: devices,
                        currentDevice: currentDevice,
                        pickerData: pickerData,
                        
                        offline: data.result?.offline,
                        homePageDeviceId: homePageDeviceId
                    }) 
                }
                if(!this.state.topSelected){
                    let topIndicators = data.result?.data || []
                    this.setState({
                        chartTittle: topIndicators[0] && topIndicators[0]['text'],
                        chartUnit: topIndicators[0] && topIndicators[0]['unit'],
                        topSelected: topIndicators[0] && topIndicators[0]['name'],
                        currentData: topIndicators[0] && topIndicators[0]['value'],
                        topIndicators: topIndicators,
                        good_interval: topIndicators[0] && topIndicators[0]['good'],
                        normal_interVal: topIndicators[0] && topIndicators[0]['normal'],
                        bad_interVal: topIndicators[0] && topIndicators[0]['bad'],
                        feelingsTexts: topIndicators[0]?.feelingsTexts,
                    }) 
                }
				this.setState({
                    chartData: data.result?.list || [],
				},()=>{
					this.setState({
						isLoadingChart: false
					})
				})
                DeviceEventEmitter.emit(NotificationKeys.kHouseInfoNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			console.log(JSON.stringify(error));
		}
    }

    //请求环境数据
	async setDefaultDevice(deviceId) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.homePageDevice,
				params: {
                    id: this.houseId,
                    deviceId: deviceId
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			console.log(JSON.stringify(error));
		}
    }

    

    _renderSelectedTitle(){
        const Colors = this.props.themeInfo.colors
        const device = this.state.currentDevice || {}

        if(device.deviceName){
            return (
                <Text style={[styles.deviceText,{color: Colors.themeText}]}>
                    {device.deviceName}
                    {device.roomName ? <Text style={{fontSize: 15, color: Colors.themeTextLight}}>    {device.roomName}</Text> : null}
                </Text>
            )
        }

        return <Text style={{fontSize: 14, color: Colors.themeTextLight}}>点击选择空气检测设备</Text>
    }
    
    // 选择设备
    getDeviceSelect(){
        if(!this.state.devices || this.state.devices.length<=0){
            return null
        }
        const Colors = this.props.themeInfo.colors
        
        return(
            <View style={{paddingVertical:5, backgroundColor: Colors.themeBg,paddingHorizontal: 16}}>
                <TouchableOpacity
                    activeOpacity={0.7} 
                    style={[styles.deviceTouch,{backgroundColor: Colors.themeBaseBg}]} 
                    onPress={()=>{
                        this.setState({
                            modalVisible: true
                        })
                    }}
                >
                    {this._renderSelectedTitle()}
                    <View style={{flex: 1}}/>
                    <Image style={[styles.arrowDown,{tintColor: Colors.themeTextLight}]} source={require('../../images/down.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

	// 温度，湿度....
	getTopSelect(){
        const Colors = this.props.themeInfo.colors

        if(!this.state.topIndicators || this.state.topIndicators.length<=0){
            return <View style={{height: 40,backgroundColor: Colors.themeBg}}/>
        }

		let indicators = this.state.topIndicators.map((val,index)=>{
			let textColor = Colors.themeText
			let textFontweight = 'normal'
			let underLineColor = Colors.transparency

			if(this.state.topSelected == val.name){
				textColor = Colors.themeButton
				textFontweight = 'bold'
				underLineColor = Colors.themeButton
			}
			return(
				<TouchableOpacity key={'top_indicators_'+index} style={styles.btns} onPress={()=>{
					this.setState({ 
						topSelected: val.name,
						chartTittle: val.text,
                        chartUnit: val.unit,
                        currentData: val.value,
						chartData: null,
                        good_interval: val.good,
                        normal_interVal: val.normal,
                        bad_interVal: val.bad,
                        feelingsTexts: val.feelingsTexts
					},()=>{
						this.requestAirQuality()
					})
				}}>
					<Text style={{color:textColor,fontSize:15,fontWeight:textFontweight}}>{val.text}</Text>
					<View style={{height:3,width:25,backgroundColor:underLineColor,marginTop:8}}/>
				</TouchableOpacity>
			)
		})
		
		return(
            <View style={{height: 40,backgroundColor: Colors.themeBg}}>
                <ScrollView
                    style={styles.topWrapper}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                >
				    {indicators}
			    </ScrollView>
            </View>
		)
	}

	// 年月日
	getHeaderSegment() {
        const Colors = this.props.themeInfo.colors
        
        let list = this.state.indicators.map((val, index) => {
            let bgStyle = Colors.themeBg
            let textStyle = Colors.themeTextLight
            if (this.state.select == val) {
                bgStyle = Colors.themeButton
                textStyle = Colors.white
            }
            return (
                <View style={styles.segmentWrapper} key={'energy_index_' + index}>
                    <TouchableOpacity style={[styles.segmentTouch, { backgroundColor: bgStyle }]} onPress={() => {
                        this.setState({
                            select: val,
							currentDateType: index + 1
                        },()=>{
							this.requestAirQuality()
						})
                        
                    }}>
                        <Text style={{ color: textStyle, fontSize: 13 }}>{val}</Text>
                    </TouchableOpacity>
                </View>
            )
        })
        return (
            <View style={{ marginTop: 10, paddingHorizontal: 12, flexDirection: 'row' }}>
                {list}
            </View>
        )
	}
	
	getXDate(timeStamp){
        let currentSelect = this.state.select
        try {
            let d = new Date(timeStamp)
            var year = d.getFullYear(); 
            var month = d.getMonth()+1; 
            var date = d.getDate(); 
            var hour = d.getHours(); 
            var minute = d.getMinutes(); 
            var second = d.getSeconds();

            switch (currentSelect) {
                case '日': return hour + '时'
                case '周': return date + '日'
                case '月': return date + '日'
                case '年': return month + '月'
                default:   return 0
            }
        } catch (error) {
            return 0    
        }
	}

    getVisualMapData(){
        let good_interval = this.state.good_interval || []
        let normal_interVal = this.state.normal_interVal || []
        let bad_interVal = this.state.bad_interVal || []
        let visualMapData = null

        if(good_interval.length && normal_interVal.length & bad_interVal.length){
            visualMapData = {
                show: false,
                type: 'piecewise',
                precision: 2,
                pieces: [{
                    gt: good_interval[0],
                    lte: good_interval[1],
                    color: GoodLineColor
                }, {
                    gt: normal_interVal[0],
                    lte: normal_interVal[1],
                    color: NormalLineColor
                },
                {
                    gt: bad_interVal[0],
                    lte: bad_interVal[1],
                    color: BadLineColor
                }]
            }
        }
        return visualMapData
    }

    getAreaData(){
        let good_interval = this.state.good_interval || []
        let normal_interVal = this.state.normal_interVal || []
        let bad_interVal = this.state.bad_interVal || []
        
        return(
            [
                {
                    name: '良好',
                    type: "line",
                    animation: false,
                    areaStyle: {
                        normal: {},
                    },
                    lineStyle: {
                        normal: {
                            width: 1,
                        },
                    },
                    markArea: {
                        silent: true,
                        data: [[{yAxis: good_interval[1]}, {yAxis: good_interval[0]}]]
                    }
                },{
                    name: '一般',
                    type: "line",
                    animation: false,
                    areaStyle: {
                        normal: {},
                    },
                    lineStyle: {
                        normal: {
                            width: 1,
                        },
                    },
                    markArea: {
                        silent: true,
                        data: [[{yAxis: normal_interVal[1]}, {yAxis: normal_interVal[0]}]]
                    }
                },{
                    name: '较差',
                    type: "line",
                    animation: false,
                    areaStyle: {
                        normal: {},
                    },
                    lineStyle: {
                        normal: {
                            width: 1,
                        },
                    },
                    markArea: {
                        silent: true,
                        data: [[{yAxis: bad_interVal[1]}, {yAxis: bad_interVal[0]}]]
                    }
                },
            ]
        ) 
    }
	
	_Echart(){
		if(!this.state.chartData || this.state.isLoadingChart){
			return(
                <View style={styles.loadingWrapper}>
                    <Text style={{color:Colors.themeTextLightGray}}>数据加载中...</Text>
                </View>
            )
		}
        if(this.state.chartData.length<=0){
            return(
                <View style={styles.loadingWrapper}>
                    <Text style={{color:Colors.themeTextLightGray}}>暂无数据</Text>
                </View>
            )
		}
		let eleData = this.state.chartData
        let eleDataX = []
		let eleDataY = []
        for (const data of eleData) {
			//let timeX = this.getXDate(data.time)
			let timeX = data.timeX
			eleDataX.push(timeX)
			let valueY = data.value
			if( typeof(valueY) == 'number' ){
				eleDataY.push({value: valueY, time: data.dateTime, tipTitle: this.state.chartTittle})
			}else{
				eleDataY.push(0)
			}
		}
		// eleDataY = [1,2,3,4,5,6,7,8,9,4,5,6,7,0,6,7,8,0,1,2,3,4,5,6,7,8,9,4,5,10,100]
		// eleDataX = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
		// 柱状图 是否可以滚动
		let toopTipTitle = this.state.chartTittle

        // 不同颜色 用来显示分段数据
        let visualMapData = this.getVisualMapData()

        // areaData
        let areaData = this.getAreaData()

		let option = {
			color: [GoodLineColor,NormalLineColor,BadLineColor,'#1D94FE'],
			tooltip: {
                show: true,
				trigger: 'axis',
				axisPointer: {
					type: 'line',
					lineStyle:{
						width:1,
						color: Colors.borderLightGray
					}
				},
				formatter: function(data){
					let axisData = data[0]
                    
                    return (
						axisData.data.time
                        +'</br>' + axisData.data.tipTitle + ' : ' + axisData.data.value
					)
                }
			},
			legend: {show: false},
			grid: {
				left: '3%',
				right: '4%',
				bottom: '3%',
				containLabel: true
			},
			xAxis: {
				data: eleDataX,
				axisLine: {
					lineStyle: {
						color: Colors.borderLightGray
					}
				},
				axisLabel: {
					textStyle:{
						color: Colors.themeTextLightGray,
					},
				},
				axisTick:{
					show: false
				},
			},
			clickable:true,
			yAxis: {
				axisLine: {
					lineStyle: {
						color: Colors.borderLightGray
					}
				},
				axisLabel: {
					textStyle:{
						color: Colors.themeTextLightGray,
					},
				},
				axisTick:{
					show: false
				},
				splitLine:{
					lineStyle:{
						type: 'dashed',
						color: Colors.borderLightGray
					}
				},
			},
            // visualMap: visualMapData,
			// dataZoom: dataZoom,
			// animation: animation,
			series: [
                areaData[0],
                areaData[1],
                areaData[2],
                {
                    name: '',
                    type: 'line',
                    smooth: true,
                    width: 1,
                    // barWidth: 20,
                    data: eleDataY
                },
            ]
		};
		return (
			<Echarts
				option={option} 
				height={300} 
				onPress={(data)=>{
					console.log('点击事件。');
				}}
			/>
		)
	}
	
	getCurrentVaule(){
        const Colors = this.props.themeInfo.colors

		let title = '当前' + this.state.chartTittle + ':'
		let currentValue = this.state.currentData
		let unit = this.state.chartUnit

        if(this.state.offline){
            return(
                <View style={{paddingHorizontal: 20,marginTop:20,flexDirection: 'row',alignItems:'center'}}>
				    <Text style={{fontSize:15,color:Colors.themeText}}>{title}</Text>
				    <Text style={{fontSize:16,marginLeft: 10,color: Colors.themeTextLight}}>
                        <Text style={{color: Colors.themeText}}>- </Text>
                        (设备离线)
                    </Text>
			    </View>
            )
        }
		
		return(
			<View style={{paddingHorizontal: 20,marginTop:20,flexDirection: 'row',alignItems:'center'}}>
				<Text style={{fontSize:15,color:Colors.themeText}}>{title}</Text>
				<Text style={{fontSize:32,marginLeft: 20,color: Colors.themeText}}>{currentValue}{unit+'  '}</Text>
			</View>
		)
	}

    // 右上角 标识
    renderLegend(){
        if(!this.state.chartData || !this.state.chartData.length || !this.state.feelingsTexts){
			return null
		}
        const Colors = this.props.themeInfo.colors
        const feelings = this.state.feelingsTexts.split(',')

        return(
            <View style={styles.legendWrapper}>
                <View style={styles.legendItem}>
                    <View style={{backgroundColor: GoodLineColor, width:24,height:12}}/>
                    <Text style={{fontSize: 12, marginLeft: 5,color: Colors.themeTextLight}}>{feelings[0]}</Text>
                </View>
                <View style={styles.legendItem}>
                    <View style={{backgroundColor: NormalLineColor, width:24,height:12}}/>
                    <Text style={{fontSize: 12, marginLeft: 5,color: Colors.themeTextLight}}>{feelings[1]}</Text>
                </View>
                <View style={styles.legendItem}>
                    <View style={{backgroundColor: BadLineColor, width:24,height:12}}/>
                    <Text style={{fontSize: 12, marginLeft: 5,color: Colors.themeTextLight}}>{feelings[2]}</Text>
                </View>
            </View>
        )
    }

    // 电量 图表
    getElerticChartView() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={{ marginTop: 25, width:'100%' }}>
				<Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 16,color: Colors.themeText }}>{this.state.chartTittle}统计图</Text>
				<View style={{ marginTop: 10, width:'100%'}}>
					{this._Echart()}
					<Text style={[styles.chartUnit,{color: Colors.themeTextLight}]}>单位: {this.state.chartUnit}</Text>
				</View>
                {this.renderLegend()}
            </View>
        )
    }

    renderDeviceList(){
        if(!this.state.devices){
            return (
                <View style={{marginTop:50,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontSize: 16}}>暂无设备</Text>
                </View>
            )
        }
        
        let list = this.state.devices.map((value, index)=>{

            const selectView = this.state.homePageDeviceId == value.deviceId ? 
            <Image style={styles.selectIcon} source={require('../../images/select.png')}/>
            : <View style={styles.selectedView} />
            
            const roomName = (value.floorText || '' ) + '  ' +(value.roomName || '')
            
            return(
                <TouchableOpacity
                    style={styles.deviceItem}
                    activeOpacity={0.7}
                    key={'pass_list_'+index}
                    onPress={()=>{
                        if(this.state.homePageDeviceId == value.deviceId) return
                        this.setState({
                            currentDevice: value,
                            homePageDeviceId: value.deviceId,
                            select: '日',
                            currentDateType: 1,
                            topSelected: null,
                            topIndicators: null,
                            feelingsTexts: null,
                            modalVisible: false
                        },()=>{
                            this.requestAirQuality()
                            this.setDefaultDevice(value.deviceId)
                            ToastManager.show('已选择 '+ value.deviceName)
                        })
                    }}
                >
                    <Image style={styles.deviceIcon} source={{uri: value.icon}}/>
                    <View style={{flex: 1,marginLeft: 10}}>
                        <Text style={styles.deviceName}>{value.deviceName}</Text>
                        {value.roomName ? <Text style={styles.roomName}>{roomName}</Text> : null}
                    </View>
                    {selectView}
                </TouchableOpacity>
            )
        })
        
        return list
    }

    renderDeviceListWrapper(){
        return(
            <View style={{flex: 1}}>
                <View style={styles.titleWrapper}>
                    <Text style={styles.passTitle}>选择空气检测设备</Text>
                    <TouchableOpacity style={styles.topAddTouch} onPress={()=>{
                        this.setState({modalVisible: false})
                    }}>
                        <Text style={{fontSize: 15, color: Colors.tabActiveColor, fontWeight:'500'}}>完成</Text>
                    </TouchableOpacity>
                </View>
                <ScrollView showsVerticalScrollIndicator={true} contentContainerStyle={{paddingBottom: 20}}>
                    {this.renderDeviceList()}
                </ScrollView>
            </View>
        )
    }

    // 显示设备弹框
    renderDeviceModel(){
        return(
            <Modal 
                transparent={true}
                visible={this.state.modalVisible}
                animationType={'fade'}
            >
                <TouchableOpacity
                    activeOpacity={1}
                    style={{flex: 5,backgroundColor:Colors.translucent}}
                    onPress={()=>{
                        this.setState({modalVisible: false})
                    }}
                />
                <View style={styles.listWrapper}>
                    {this.renderDeviceListWrapper()}
                </View>
            </Modal>
        )
    }

    renderContentView(){
        if(!this.state.topIndicators){
            return null
        }
        if(this.state.topIndicators.length <= 0){
            return (
                <View style={{flex: 1,alignItems:'center'}}>
                    <Text style={styles.noContent}>暂无数据</Text>
                </View>
            )
        }

        return(
            <>
                {this.getTopSelect()}
                <ScrollView>
				    {this.getHeaderSegment()}
				    {this.getCurrentVaule()}
				    {this.getElerticChartView()}
			    </ScrollView>
            </>
        )
    }

    renderMainView(){
        
        if(this.state.devices && this.state.devices.length <= 0){
            const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
            return (
                <View style={{alignItems:'center'}}>
                    <ListNoContent
                        style={{marginTop: '30%',alignItems: 'center'}}
                        img={Images.noDevice}
                        text={'暂无空气检测设备'}
                    />
                    <TouchableOpacity style={styles.moreTouch} onPress={()=>{
                        const {navigate} = this.props.navigation
                        navigate('AddDeviceQRCode')
                    }}>
                        <Text style={{fontSize:14,color:Colors.themeTextLightGray}}>去添加</Text>
                        <Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
                    </TouchableOpacity>
                </View>
            )
        }

        return(
            <>
                {this.getDeviceSelect()}
                {this.renderContentView()}
                {this.renderDeviceModel()}
            </>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors

		return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderMainView()}
            </View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	contentStyle: {
		paddingBottom: 30
	},
	navTitle: {
		fontSize: 24,
		fontWeight: 'bold',
		marginLeft: 15,
		textAlignVertical: 'center',
		color: Colors.themeTextBlack,
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 16,
		paddingLeft: 10
	},
	navText:{
		fontSize: 16,
		color: Colors.tabActiveColor
    },
    topWrapper:{
        height:'100%',
        flexDirection: 'row'
    },
	segmentWrapper: {
		flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    segmentTouch: {
        height: 30,
        width: '100%',
        borderRadius: 15,
        justifyContent: 'center',
		alignItems: 'center'
	},
	popCellView:{
		position:'absolute', 
		backgroundColor:'#1D94FE',
		justifyContent:"center",
		alignItems:'center',
		borderRadius: 5
	},
	chartUnit:{
		position:'absolute',
		left:16,
		top:'5%',
		fontSize:12
	},
	paramTouch:{
		height: 25,
		borderRadius: 25,
		paddingHorizontal:10,
		justifyContent:'center',
		alignItems:'center',
		backgroundColor:Colors.tabActiveColor
    },
    btns:{
        paddingHorizontal: 15,
        justifyContent:'flex-end',
        alignItems:'center'
    },
    deviceTouch:{
        width: '100%',
        height: 40,
        borderRadius: 4,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 16
    },
    deviceText:{
        fontSize: 15
    },
    arrowDown:{
        width: 14,
        height: 7,
        resizeMode: 'contain' 
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    moreTouch:{
		borderColor:Colors.borderLightGray,
		borderWidth:1,
		width:120,
		height:30,
		borderRadius:15,
		justifyContent:'center',
		alignItems:'center',
        flexDirection:'row',
        marginTop: 20
    },
    arrow:{
		width:6,
		height:11,
		marginLeft:10,
		resizeMode:'contain'
	},
    loadingWrapper:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        marginTop:100
    },
    legendWrapper:{
        position: 'absolute',
        right:20,
        top: 0
    },
    legendItem:{
        flexDirection:'row', 
        justifyContent: 'center', 
        alignItems:'center',
        marginTop: 5
    },
    noContent:{
        marginTop: '40%',
        fontSize: 14,
        color: Colors.themeTextLightGray
    },
    titleWrapper:{
        height: 60,
        justifyContent:'center',
        alignItems:'center'
    },
    passTitle:{
        fontSize: 18,
        fontWeight: 'bold',
        color: Colors.themeTextBlack
    },
    topCloseTouch:{
        paddingHorizontal: 20, 
        height:'100%',
        position:'absolute',
        left:0,
        top: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    topAddTouch:{
        paddingHorizontal: 20, 
        height:'100%',
        position:'absolute',
        right:0,
        top: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    closeImg:{
        width:25,
        height:25,
        tintColor: 'black',
        resizeMode:'contain'
    },
    addImg:{
        width:30,
        height:30,
        resizeMode:'contain'
    },
    deviceItem:{
        flexDirection:'row',
        alignItems:'center',
        paddingHorizontal: 16,
        marginHorizontal: 16,
        marginBottom: 10,
        height: 60,
        backgroundColor: Colors.white,
        borderRadius: 5
    },
    selectIcon:{
        width:18,
        height:18,
        resizeMode: 'contain'
    },
    selectedView:{ 
		width: 18, 
		height: 18, 
		borderColor: Colors.borderLightGray, 
		borderWidth: 1, 
		borderRadius: 9 
	},
    deviceName:{
        color: Colors.themeTextBlack,
        fontSize: 15,
        fontWeight:'500'
    },
    roomName:{
        marginTop: 5,
        color: Colors.themeTextLightGray,
        fontSize: 13
    },
    deviceIcon: {
        width: 45,
        height: 45,
        resizeMode: 'contain'
    },
    listWrapper:{
        backgroundColor: '#F6F6F6',
        flex: 5,
        paddingBottom: BottomSafeMargin
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(EnvironmentObservation)

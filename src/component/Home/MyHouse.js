/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	ImageBackground,
	DeviceEventEmitter,
	SwipeableFlatList,
	Alert
} from 'react-native';
import { postJson } from '../../util/ApiRequest';
import { NetUrls, NotificationKeys, Colors} from '../../common/Constants';
import ToastManager from '../../common/CustomComponent/ToastManager';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import {DecimalTheNumber} from '../../util/NumberUtil';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import AppConfigs from '../../util/AppConfigs';
import { HSTips } from '../../common/Strings';

const screenW = Dimensions.get('window').width;
const homeCardH = (screenW - 32) * 0.5 // 32为两边padding值，0.5为卡片设计高度比

//侧滑最大距离
const maxSwipeDistance = 160
//侧滑按钮个数
const countSwiper = 2

class MyHouse extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'我的房屋'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {icon:require('../../images/add.png'),onPress:()=>{
                        navigation.getParam('addBtnClick')()
                    }},
                ]}/>
            ),
            headerBackground: <HeaderBackground/>
        }
	}

	constructor(props) {
		super(props);
        const { getParam,setParams } = this.props.navigation
        this.refreshAllInfos = getParam('refreshAllInfos')

		this.state = {
			defaultHouse: 0,
			houseList: null,
			loading: false
        }
        
        setParams({
            addBtnClick: this.addBtnClick.bind(this)
        })
	}

	componentDidMount() {
		this.requestHouseList()
	}

	componentWillUnmount() {
		
    }
    
    addBtnClick(){
        const { navigate } = this.props.navigation
        navigate('AddHouse', {
            title:'添加房屋',
            callBack: () => { this.requestHouseList() }
        })
    }

	//房屋列表
	async requestHouseList() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.houseList,
				params: {}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				let result = data.result || {}
				this.setState({
					defaultHouse: result.defaultHouse,
					houseList: result.listhouse || [],
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			console.log(JSON.stringify(error));
		}
	}

	// 切换房屋
	async requestChooseHouse(id) {
		if (this.state.loading || this.state.defaultHouse == id) {
			return
		}
		this.state.loading = true
		const { navigate,pop } = this.props.navigation
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.chooseHouse,
				params: {
					id: id
				}
			});
            SpinnerManager.close()
            this.state.loading = false
			if (data && data.code == 0) {
				this.setState({
					defaultHouse: id,
                })
                this.refreshAllInfos()
                ToastManager.show('房屋切换成功')
				pop()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            this.state.loading = false
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error));
		}
	}

	// 删除房屋
	async requestDeleteHouse(houseId,callBack,type){
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.deleteHouse,
				params: {
					id: houseId
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.requestHouseList()
				callBack && callBack()
                if(type == 1){
                    ToastManager.show('房屋删除成功')
                }else{
                    ToastManager.show('房屋权限已移除')
                }
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error));
		}
    }
    
	getTemAndPowerView(name, val, unit) {
        let valueText = val + unit
		if (!val) {
			valueText = '-'
		}else{
            valueText = val + unit
        }
		return (
			<View style={{ flexDirection: 'row', alignItems: 'center',flex: 1}}>
				<Text style={{ fontSize: 14, fontWeight: 'bold', color: Colors.white }}>{name}</Text>
				<Text style={{ fontSize: 14, marginLeft: 5, color: Colors.white }}>{valueText}</Text>
			</View>
		)
	}

	getInfoView(name, val) {
		if (!val) {
			return null
		}
		return (
			<View style={{ flexDirection: 'row', alignItems: 'center', justifyContent:'center',marginTop: 8 }}>
				<Text style={{ fontSize: 13, color: Colors.white }}>{name+': '}</Text>
				<Text style={{ fontSize: 13, marginLeft: 3, color: Colors.white, marginRight: 15, }}>{val+' '}</Text>
			</View>
		)
	}

	//关闭侧滑栏
    closeSwiper() {
        this.houseList && this.houseList.setState({ openRowKey: null })
	}
	
	deleteHouse(rowData,callBack,type){
		// if(this.state.defaultHouse == rowData.id){
		// 	ToastManager.show('当前房屋为选中房屋，不可删除')
		// 	return
		// }

        let tips = rowData.householder ? HSTips.holderDeleteHouse : HSTips.nonHolerRemoveHouse
        Alert.alert(
            '提示',
            tips,
            [
                { text: '取消', onPress: () => { }, style: 'cancel' },
                { text: '确定', onPress: () => { 
					this.requestDeleteHouse(rowData.id,callBack,type)
				}},
            ]
        )
	}

    // 分享按钮点击
    shareBtnClick(rowData){
        if(!rowData.shareInfo){
            ToastManager.show('您无权限分享此房屋。')
            return
        }
        const { navigate } = this.props.navigation
        let houseData = {name: rowData.name}

        navigate('Setting_Share',{
            title: '房屋分享',
            id: rowData.id,
            houseData: houseData,
            shareData: rowData.shareInfo,
            shareType: 4,   // 1设备 2场景 3房间 4房屋
        })
    }

    // 非户主的右滑按钮
    getNonHouseholderActions(rowData){
        const { pop } = this.props.navigation
        return(
            <View style={[styles.quickAContent,{marginRight: 30}]}>
                <TouchableOpacity activeOpacity={0.7} onPress={() => {
                    this.closeSwiper()
					let callBack = null
					// 如果是 默认房屋 被删除
					if(rowData.id == this.state.defaultHouse){
						callBack = ()=>{
							this.refreshAllInfos()
							pop()
						}
					}
					this.deleteHouse(rowData,callBack,2)
                }}>
                    <View style={[styles.quick2, { backgroundColor: Colors.themBgRed }]}>
                        <Text style={{ color: Colors.white, fontSize: 14 }}>移除权限</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

	//侧滑菜单渲染
    getQuickActions(rowData, index) {
		const { navigate,pop } = this.props.navigation
        if(!rowData.householder){
            return this.getNonHouseholderActions(rowData)
        }
        return (
            <View style={styles.quickAContent}>
                <TouchableOpacity activeOpacity={0.7} onPress={() => {
                    this.closeSwiper()
					let callBack = null
					// 如果是 默认房屋 被删除
					if(rowData.id == this.state.defaultHouse){
						callBack = ()=>{
							this.refreshAllInfos()
							pop()
						}
					}
					this.deleteHouse(rowData,callBack,1)
                }}>
                    <View style={[styles.quick, { backgroundColor: Colors.themBgRed }]}>
                        <Text style={{ color: Colors.white, fontSize: 14 }}>删除</Text>
                    </View>
                </TouchableOpacity>
				<TouchableOpacity activeOpacity={0.7} onPress={() => {
					this.closeSwiper()
                    navigate('AddHouse', {
                        callBack: () => { this.requestHouseList() },
                        title:'房屋设置',
                        id: rowData.id,
                        name: rowData.name,
                        city: rowData.city,
                        address: rowData.address
                    })
				}}>
					<View style={[styles.quick, { backgroundColor: Colors.tabActiveColor, borderTopRightRadius: 5, borderBottomRightRadius: 5 }]}>
						<Text style={{ color: Colors.white, fontSize: 14 }}>设置</Text>
					</View>
				</TouchableOpacity>
            </View>
        )
    }

	renderRowItem(val, index) {
		let selectIcon = val.id == this.state.defaultHouse ? require('../../images/homeBG/selected.png') : require('../../images/homeBG/unselected.png')
		
		let status = val.status
		let roomTemperature = null
		let outsideTemperature = null
		let humidity = null
		let pm25 = null
		let formaldehyde = null
		let co2 = null
		try {
			let homeInfos = JSON.parse(status)
			roomTemperature = DecimalTheNumber(homeInfos['temperature'])

			humidity = DecimalTheNumber(homeInfos['humidity'])
			pm25 = DecimalTheNumber(homeInfos['PM2_5'])
			co2 = DecimalTheNumber(homeInfos['CO2'])
			formaldehyde = DecimalTheNumber(homeInfos['CH2O_NH3'], 3)
		} catch (error) {

		}

		// 判断用户类型
        let bg = require('../../images/homeBG/home_card_bg2.png')
		let userType = ''
		if(val.householder){
			userType = '户主'
            bg = require('../../images/homeBG/home_card_bg3.png')
		}else if(!val.householder && val.memberType == 1){
			userType = '管理员'
            bg = require('../../images/homeBG/home_card_bg1.png')
		}else if(val.memberType == 2){
			userType = '普通成员'
            bg = require('../../images/homeBG/home_card_bg2.png')
		}

        const Colors = this.props.themeInfo.colors

		return (
			<TouchableOpacity activeOpacity={1} style={styles.homeCardTouch} onPress={() => {
				this.closeSwiper()
				this.requestChooseHouse(val.id)
			}}>
				<ImageBackground style={styles.homeCardBg} imageStyle={styles.homeCardImage} source={bg}>
					<View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 22 }}>
						<Text style={{ fontSize: 18, fontWeight: 'bold', color: Colors.white }}>{val.name+' '}</Text>
						<Text style={{ fontSize: 14, color: Colors.white, marginLeft: 20, fontWeight:'500', flex: 1 }}>{userType}</Text>
						<Image style={{ width: 16, height: 16 }} source={selectIcon} />
					</View>
					<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15, paddingHorizontal: 20,flex: 1 }}>
						{/* <Image style={{ width: 20, height: 24, marginRight: 5,resizeMode:'contain',tintColor: Colors.white }} source={require('../../images/homeBG/temp.png')} /> */}
						{this.getTemAndPowerView('室内温度:', roomTemperature, '℃')}
						{/* <Image style={{ width: 16, height: 16, marginRight: 5, resizeMode:'contain',tintColor: Colors.white }} source={require('../../images/homeBG/power_light.png')} /> */}
						{this.getTemAndPowerView('总耗能:', val.totalEnergy, 'kWh')}
					</View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, paddingHorizontal: 20,flex: 1 }}>
                        <View style={{flex: 1, flexDirection:'row'}}>
                            <Text style={{color: Colors.white,fontSize: 14,fontWeight: 'bold'}}>房间数量:</Text>
                            <Text style={{color: Colors.white,fontSize:14,fontWeight:'normal',marginLeft: 5}}>{val.roomCount}</Text>
                        </View>
                        <View style={{flex: 1, flexDirection:'row'}}>
                            <Text style={{color: Colors.white,fontSize: 14,fontWeight: 'bold'}}>设备数量:</Text>
                            <Text style={{color: Colors.white,fontSize:14,fontWeight:'normal',marginLeft: 5}}>{val.deviceCount}</Text>
                        </View>
					</View>
					<View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 10, paddingHorizontal: 22 }}>
						{this.getInfoView('湿度', humidity)}
						{this.getInfoView('PM2.5', pm25)}
						{this.getInfoView('甲醛', formaldehyde)}
						{this.getInfoView('CO₂', co2)}
					</View>
				</ImageBackground>
			</TouchableOpacity>
		)
	}

	_extraUniqueKey(item, index) {
		return "house_list_index_" + index;
	}

	getHouseListView() {
		if (!this.state.houseList || !Array.isArray(this.state.houseList)) {
			return null
		}
		if (this.state.houseList.length <= 0) {
			return (
				<View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 150 }}>
					<Text style={{ color: Colors.themeTextLightGray, fontSize: 13 }}>暂无房屋</Text>
				</View>
			)
		}
		return (
			<View style={{ flex: 1 }}>
				<SwipeableFlatList
					ref={e => this.houseList = e}
					contentContainerStyle={{ paddingBottom: 50 }}
					scrollIndicatorInsets={{right: 1}}
					renderQuickActions={({ item, index }) => this.getQuickActions(item, index)}//创建侧滑菜单
					maxSwipeDistance={maxSwipeDistance}
					bounceFirstRowOnMount={false}
					data={this.state.houseList}
					keyExtractor={this._extraUniqueKey}
					renderItem={({ item, index }) => this.renderRowItem(item, index)}
				/>
			</View>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getHouseListView()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.white,
		flex: 1
	},
	contentStyle: {
		paddingBottom: 30
	},
	navTitle: {
		fontSize: 24,
		fontWeight: 'bold',
		marginLeft: 15,
		textAlignVertical: 'center',
		color: Colors.themeTextBlack,
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight: 5
	},
	navImg: {
		width: 18,
		height: 18
	},
	homeCardTouch: {
		paddingHorizontal: 16,
		width: '100%',
		marginTop: 20,
		borderRadius: 5
	},
	homeCardBg: {
		width: '100%',
		minHeight: homeCardH,
		resizeMode: 'contain',
		paddingVertical: 16
	},
	homeCardImage: {
		borderRadius: 5,
		backgroundColor:Colors.white
	},
	unit: {
		fontSize: 12,
		color: Colors.white,
	},
	//侧滑菜单的样式
    quickAContent: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginRight: 18,
        marginTop: 20,
        overflow: 'hidden',
        borderTopRightRadius: 5,
		borderBottomRightRadius: 5,
		paddingVertical:1,
    },
    quick: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: maxSwipeDistance / countSwiper,
    },
    quick2: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: maxSwipeDistance,
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(MyHouse)

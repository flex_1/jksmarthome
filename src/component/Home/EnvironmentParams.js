/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
} from 'react-native';

import { Colors, NetUrls } from '../../common/Constants';
import HeaderLeftView from '../../common/CustomComponent/HeaderLeftView';
import ToastManager from "../../common/CustomComponent/ToastManager";
import { postJson } from '../../util/ApiRequest';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';

const LeftColor = '#E4A87A'
const MiddleColor = '#3FCD8C'
const RightColor = '#2398FE'

class EnvironmentParams extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'参数设置'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text:'保存',onPress:()=>{
                        navigation.getParam('saveBtnClick')()
                    }}
                ]}/>
            ),
            headerBackground: <HeaderBackground/>
        }
	}

	constructor(props) {
		super(props);
		const { getParam, setParams } = this.props.navigation
		this.callBack = getParam('callBack')
		this.type = getParam('type')

		this.state = {
            originMin: 10,
            originMax: 90,
            id: null,
			min: 10,
			max: 90,
			start: 0,
            end: 120,
            name: '',
            unit: '',
            leftText: '',
            middleText: '',
            rightText: ''
        }
        
        setParams({
            saveBtnClick: this.saveQualityParams.bind(this)
        })
	}

	componentDidMount() {
        this.requestQuality()
	}

	componentWillUnmount() {
		
	}

	// 获取数值
	async requestQuality() {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.airQualityConfig,
				params: {
					type: this.type
				}
			})
			SpinnerManager.close()
			if (data && data.code == 0) {
				let res = data.result
				let end = res.end
				let min = res.min
				// 甲醛特殊处理
				if(this.type == 5){
					min = min*100
					end = 15
				}
				if(res){
                    this.setState({
                        id: res.id,
                        originMin: min,
                        min: min,
                        originMax: res.max,
			            max: res.max,
			            start: res.start,
                        end: end,
                        name: res.name,
                        unit: res.unit,
                        leftText: res.range1,
                        middleText: res.range2,
                        rightText: res.range3
                    })
                }
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }
    
    // 保存参数值
	async saveQualityParams() {
		const {pop} = this.props.navigation
		let min = this.state.min
		if(this.type == 5){
			min = this.state.min*1.0/100
		}
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.saveAirConfig,
				params: {
                    id: this.state.id,
                    min: min,
                    max: this.state.max
				}
			})
			SpinnerManager.close()
			if (data && data.code == 0) {
                this.callBack && this.callBack()
                pop()
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	getFloorList() {
        const Colors = this.props.themeInfo.colors
        
		let values = [this.state.min]
        if(this.state.max){
            values = [this.state.min, this.state.max]
		}

		let CustomMarkerL = CustomMarkerLeft
		// 甲醛特殊处理
		if(this.type == 5){
			CustomMarkerL = CustomMarkerPPM
		}

		return (
			<View style={{ paddingVertical: 5, paddingHorizontal: 16 }}>
				<View style={styles.bottomWrapper}>
					<Text style={{ fontSize: 15, color: Colors.themeTextBlack }}>自定义{this.state.name}区间:
                        <Text style={{ color: Colors.themeTextLightGray, marginLeft: 10 }}> (单位: {this.state.unit})</Text>
					</Text>
					<View style={{ width: '100%', marginTop: 30, justifyContent: 'center', alignItems: 'center' }}>
						<MultiSlider
							values={values}
							min={this.state.start}
							max={this.state.end}
							onValuesChange={(values) => {
								this.setState({
									min: values[0],
									max: values[1]
								})
                            }}
							onValuesChangeFinish={(values) => {
								this.setState({
									min: values[0],
									max: values[1]
								})
							}}
							pressedMarkerStyle={{}}
							isMarkersSeparated={true}
							selectedStyle={{
								backgroundColor: MiddleColor,
								height: 20,
							}}
							unselectedStyle={{
								backgroundColor: '#80DDD7',
								height: 20,
                            }}
							customMarkerLeft={CustomMarkerL}
							customMarkerRight={CustomMarkerRight}
						/>
					</View>
                    <View style={{flexDirection:'row',justifyContent:'space-between',paddingHorizontal:15}}>
                        <Text style={{color: LeftColor, fontWeight:'bold'}}>{this.state.leftText}</Text>
                        <Text style={{color: MiddleColor, fontWeight:'bold'}}>{this.state.middleText}</Text>
                        <Text style={{color: RightColor, fontWeight:'bold'}}>{this.state.rightText}</Text>
                    </View>
				</View>
				<View style={{ marginTop: 35, alignItems: 'center' }}>
					{this.state.originMin ? <TouchableOpacity activeOpacity={0.9} style={styles.resetTouch} onPress={() => {
						this.setState({
							min: this.state.originMin,
							max: this.state.originMax
						})
					}}>
						<Text style={{ color: Colors.white }}>恢复</Text>
					</TouchableOpacity> : null}
				</View>
			</View>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getFloorList()}
			</View>
		)
	}
}

// 甲醛（特殊处理）
class CustomMarkerPPM extends React.Component {
	render() {
		return (
			<View style={{ alignItems: 'center', marginBottom: 32 }}>
				<Text style={{color:'#80DDD7'}}>{this.props.currentValue*1.0/100}</Text>
				<Image style={styles.maker}
					source={this.props.pressed ?
						require('../../images/settingIcon/maker_left.png') : require('../../images/settingIcon/maker_left.png')
					}
				/>
			</View>
		)
	}
}

class CustomMarkerLeft extends React.Component {
	render() {
		return (
			<View style={{ alignItems: 'center', marginBottom: 32 }}>
				<Text style={{color:'#80DDD7'}}>{this.props.currentValue}</Text>
				<Image style={styles.maker}
					source={this.props.pressed ?
						require('../../images/settingIcon/maker_left.png') : require('../../images/settingIcon/maker_left.png')
					}
				/>
			</View>
		)
	}
}

class CustomMarkerRight extends React.Component {
	render() {
		return (
			<View style={{ alignItems: 'center', marginBottom: 32 }}>
				<Text style={{color:'#2CC7BC'}}>{this.props.currentValue}</Text>
				<Image style={styles.maker}
					source={this.props.pressed ?
						require('../../images/settingIcon/maker_right.png') : require('../../images/settingIcon/maker_right.png')
					}
				/>
			</View>
		);
	}
}


const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	inputWrapper: {
		width: '100%',
		height: 60,
		backgroundColor: Colors.white,
		borderRadius: 5,
		alignItems: 'center',
		flexDirection: 'row',
		marginTop: 15
	},
	bottomWrapper: {
		width: '100%',
		height: 165,
		backgroundColor: Colors.white,
		borderRadius: 5,
		marginTop: 15,
		padding: 15
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight: 5
	},
	resetTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		width: 140,
		height: 44,
		backgroundColor: Colors.tabActiveColor,
		borderRadius: 4
	},
	navText: {
		color: Colors.tabActiveColor,
		fontSize: 15
	},
	maker: {
		width: 25,
		height: 15,
		backgroundColor: 'transparent',
		resizeMode: 'contain'
	}
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(EnvironmentParams)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Dimensions,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet';
import { connect } from 'react-redux';
import { postPicture } from '../../util/ApiRequest';
import { NetUrls,Colors } from '../../common/Constants';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import { BottomSafeMargin,StatusBarHeight } from '../../util/ScreenUtil';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';

const dismissKeyboard = require('dismissKeyboard');

const screenW = Dimensions.get('window').width;
const homeCardH = (screenW - 16*2 - 10*2) * 0.8

class HouseSetting extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
        headerRight: (
            <HeaderRight buttonArrays={[
                {text:'保存',onPress:()=>{
                    navigation.getParam('settingBtnClick')()
                }},
            ]}/>
        ),
        headerBackground: <HeaderBackground/>
	})

	constructor(props) {
		super(props);
		const {getParam,setParams} = this.props.navigation

		this.callBack = getParam('callBack')
		this.deleteCallBack = getParam('deleteCallBack')
		this.houseName = getParam('name')
		this.houseId = getParam('id')
		this.img = getParam('img')
		this.isDefaultHouse = getParam('isDefaultHouse')

		this.state = {
			text: this.houseName,
			homeBgThumb: this.img ? {uri: this.img} : null,
			imageData: {}
        }
        
        setParams({
            settingBtnClick: ()=>{ 
                dismissKeyboard()
                this.requestUpdateImage() 
            }
        })
	}

	componentDidMount() {
		
	}

	componentWillUnmount(){
		
	}

	//更换房间图片
	async requestUpdateImage() {
		if(!this.state.text){
			ToastManager.show('房屋名称不能为空')
			return
		}
		const {pop} = this.props.navigation
		SpinnerManager.show()
		try {
			let data = await postPicture({
				url: NetUrls.houseSetting,
				filePath: this.state.imageData.path,
				fileName: this.state.imageData.filename || 'hoomeBgImg.jpg',
				params: {
					id: this.houseId,
					type: 1, // 1 背景图 2 布线图
					name: this.state.text
				}
			})
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.callBack && this.callBack()
				pop()
				ToastManager.show('设置成功')
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error));
		}
	}

	getActionSheet() {
		return (
			<ActionSheet
				ref={e => this.actionSheet = e}
				title={'上传或拍照'}
				options={['从相册中选择', '拍一张', '取消']}
				cancelButtonIndex={2}
				onPress={(index) => {
					const { navigate } = this.props.navigation
					if (index == 0) {
						this.pickImage()
					} else if (index == 1) {
						this.pickImagewithCamera()
					}
				}}
			/>
		)
	}

	pickImage() {
		ImagePicker.openPicker({
			width: screenW,
			height: homeCardH,
			cropping: true,
			mediaType: 'photo',
			compressImageQuality: 1,
		}).then(image => {
			console.log(image);
			this.setState({
				homeBgThumb: { uri: image.path },
				imageData: image
			})
			
		}).catch(err => {
			console.log(err);
		})
	}

	pickImagewithCamera() {
		ImagePicker.openCamera({
			width: screenW,
			height: homeCardH,
			cropping: true,
			mediaType: 'photo',
			compressImageQuality: 1,
		}).then(image => {
			console.log(image);
			this.setState({
				homeBgThumb: { uri: image.path },
				imageData: image
			})
			
		}).catch(err => {
			console.log(err);
		})
	}

	getInput() {
		const Colors = this.props.themeInfo.colors
		return (
			<View style={{ width: '100%', marginTop: 15, paddingHorizontal: 16,paddingVertical:10 }}>
				<View style={[styles.inputWrapper, {backgroundColor: Colors.themeBg}]}>
					<Text style={{ marginLeft: 20, fontSize: 15, color: Colors.themeText }}>房屋名称</Text>
					<TextInput
						defaultValue={this.state.text}
						style={[styles.input,{color: Colors.themeText}]}
						placeholder={'请输入房屋名称(不超过8个字)'}
						placeholderTextColor={Colors.themeInactive}
						onChangeText={(text) => {
							this.state.text = text
						}}
						returnKeyType={'done'}
					/>
				</View>
			</View>
		)
	}

	getHouseImage(){
		const Colors = this.props.themeInfo.colors
		const homeBgThumb = this.state.homeBgThumb || require('../../images/homeBG/new_homebg1.jpg');

		return(
			<View style={{marginTop:20,width:'100%',paddingHorizontal: 16, paddingVertical: 10}}>
				<TouchableOpacity activeOpacity={0.8} style={[styles.wrapper, {backgroundColor:Colors.themeBg}]} onPress={()=>{
					this.actionSheet?.show()
				}}>
					<View style={{flexDirection: 'row',}}>
						<Text style={{fontSize: 15,color:Colors.themeText,flex:1}}>选取新房屋图片</Text>
						<Image style={{width: 7,height:13,resizeMode:'contain'}} source={require('../../images/enterLight.png')}/>
					</View>
					<Image style={[styles.bgImage, {backgroundColor: Colors.themeBaseBg}]}  source={homeBgThumb}/>
				</TouchableOpacity>
			</View>
		)
	}

	getDeleteButton(){
		const Colors = this.props.themeInfo.colors
		if(this.isDefaultHouse){
			return null
		}
		return (
			<View style={styles.bottomBtnWrapper}>
				<TouchableOpacity activeOpacity={0.7} style={[styles.deleteTouch, {backgroundColor: Colors.themeBg}]} onPress={() => {
					this.deleteCallBack && this.deleteCallBack()
				}}>
					<Text style={{ color: Colors.red }}>删除</Text>
				</TouchableOpacity>
			</View>
		)
	}

	render() {
		const Colors = this.props.themeInfo.colors
		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				<ScrollView>
					{this.getInput()}
					{this.getHouseImage()}
					{this.getActionSheet()}
				</ScrollView>
				{this.getDeleteButton()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 16,
		paddingLeft: 10
	},
	inputWrapper: {
		width: '100%',
		height: 55,
		borderRadius: 5,
		alignItems: 'center',
		flexDirection: 'row'
	},
	input: {
		flex: 1,
		height: 40,
		marginLeft: 5,
		marginRight: 10,
		textAlign: 'right',
		paddingRight: 10
	},
	wrapper:{
		width:'100%',
		borderRadius:4,
		paddingVertical:15,
		paddingHorizontal: 10,
	},
	deleteTouch: {
        width: '100%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
		borderRadius: 4
    },
    bgImage:{
        width: '100%',
        height: homeCardH,
        marginTop:20,
        borderRadius:4,
    },
    bottomBtnWrapper: {
        position: 'absolute',
        left: 0,
        bottom: BottomSafeMargin,
        width: '100%',
        paddingHorizontal: 16,
		paddingVertical: 10,
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(HouseSetting)

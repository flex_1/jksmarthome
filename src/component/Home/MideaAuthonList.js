/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
    RefreshControl,
    SwipeableFlatList,
    Dimensions,
    DeviceEventEmitter
} from 'react-native';
import { postJson } from '../../util/ApiRequest';
import { NetUrls, Colors, NotificationKeys, NetParams } from '../../common/Constants';
import ToastManager from '../../common/CustomComponent/ToastManager';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import {ListNoContent,FooterEnd,ListLoading,ListError } from "../../common/CustomComponent/ListLoading";
import { Alert } from 'react-native';

//侧滑最大距离
const maxSwipeDistance = 100
//侧滑按钮个数
const countSwiper = 1

const screenH = Dimensions.get('window').height;

class MideaAuthonList extends Component {

	static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'授权列表'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text:'添加授权',onPress:()=>{
                        navigation.getParam('addAuthBtn')()
                    }}
                ]}/>
            ),
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
	}

	constructor(props) {
		super(props);
        const { getParam,setParams,navigate } = props.navigation;

		this.state = {
			authList: null,
            loading: true,
            isRefreshing: false
        }
        
        setParams({
            addAuthBtn: this.addAuth.bind(this) 
        })
	}

	componentDidMount() {
        this.requestmeidiAuthList()
	}

	componentWillUnmount() {
        
	}

    addAuth(){
        this.requestmeidiAuthUrl()
    }

    // 获取授权列表
    async requestmeidiAuthList(){
        try {
			let data = await postJson({
				url: NetUrls.mideaAuthList,
				params: {
				}
            });
            this.setState({
                loading: false,
                isRefreshing: false
            })
			if (data.code == 0) {
                this.setState({
                    authList: data.result || []
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            this.setState({
                loading: false,
                isRefreshing: false
            })
			ToastManager.show('网络错误')
		}
    }

    // 获取授权登录url
    async requestmeidiAuthUrl(){
        SpinnerManager.show()
        try {
			let data = await postJson({
				url: NetUrls.mideaAuthUrl,
				params: {
				}
            });
            SpinnerManager.close()
			if (data.code == 0) {
                const {navigate} = this.props.navigation
                navigate('Authorize',{
                    url: data.result,
                    title: '美的授权',
                    callBack: ()=>{
                        this.requestmeidiAuthList()
                        DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
                    }
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    // 同步设备
    async requestMideaSyncDevice(openUid){
        SpinnerManager.show()
        try {
			let data = await postJson({
				url: NetUrls.mideaSyncDevice,
				params: {
                    openUid: openUid
				}
            });
            SpinnerManager.close()
			if (data.code == 0) {
                this.requestmeidiAuthList()
                if(data.result > 0){
                    DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
                }
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    // 取消授权
    async requestCancelAuth(openUid){
        SpinnerManager.show()
        try {
			let data = await postJson({
				url: NetUrls.mideaCancelAuth,
				params: {
                    openUid: openUid
				}
            });
            SpinnerManager.close()
			if (data.code == 0) {
                this.requestmeidiAuthList()
                if(data.result > 0){
                    DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
                }
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }

    //关闭侧滑栏
	closeSwiper(){
		this.swiperList && this.swiperList.setState({openRowKey:null})
	}

    getQuickActions(rowData, index){
        return (
			<View style={styles.quickAContent}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.quick}  
                    onPress={()=>{
					    this.closeSwiper()
					    Alert.alert(
                            '提示',
                            '确定要取消账户' + rowData.userName + '的授权么？',
                            [   
                                { text: '再想想', style: 'cancel'},
                                { text: '取消授权', onPress: () => {
                                    this.requestCancelAuth(rowData.openUid)
                                }}
                            
                            ],
                        )
				    }}
                >
					<Text style={styles.quickText}>取消授权</Text>
				</TouchableOpacity>
			</View>
		)
    }

    renderNoContent(){
        if(!this.state.authList){
            return <ListError style={styles.noContent} onPress={()=>{ this.requestmeidiAuthList() }}/>
        }else if(this.state.authList.length <= 0){
            return (
                <View style={{alignItems:'center'}}>
                    <ListNoContent style={styles.noContent} text={'暂无授权'}/>
                    <TouchableOpacity style={styles.addAuthTouch} onPress={()=>{
                        this.addAuth()
                    }}>
                        <Text style={styles.addAuthText}>添加授权</Text>
                    </TouchableOpacity>
                </View>
            )
        }else{
            return null
        }
    }

    renderRowItem(rowData, index){
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.item,{backgroundColor: Colors.themeBg}]}>
                <View style={{flex: 1}}>
                    <Text style={{fontSize: 16, color: Colors.themeText}}>{rowData.userName}</Text>
                    <Text style={[styles.createTime,{color: Colors.themeTextLight}]}>{rowData.createTime}</Text>
                    <Text style={[styles.count,{color: Colors.themeTextLight}]}>设备数:{rowData.deviceCount}</Text>
                </View>
                <TouchableOpacity activeOpacity={0.7} style={styles.asyncTouch} onPress={()=>{
                    this.requestMideaSyncDevice(rowData.openUid)
                }}>
                    <Text style={{fontSize: 14, color: Colors.white}}>同步设备</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderAuthListView(){

        if(this.state.loading){
            return <ListLoading/>
        }
        
        return(
            <SwipeableFlatList
                ref={e => this.swiperList = e}
                contentContainerStyle={{ paddingBottom: 80}}
                scrollIndicatorInsets={{right: 1}}
                removeClippedSubviews={false}
                data={this.state.authList}
                keyExtractor={(item, index) => 'auth_'+index}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
                initialNumToRender={10}
                ListEmptyComponent = {this.renderNoContent()}
                renderQuickActions={({ item, index }) => this.getQuickActions(item,index)}//创建侧滑菜单
                maxSwipeDistance={maxSwipeDistance}
                bounceFirstRowOnMount={false}
                refreshControl={
                    <RefreshControl
                        colors={[Colors.themeBGInactive]}
                        tintColor={Colors.themeBGInactive}
                        refreshing={this.state.isRefreshing}
                        onRefresh={() => {
                            if (this.state.isRefreshing) return
                            this.setState({ isRefreshing: true })
                            this.requestmeidiAuthList()
                        }}
                    />
                }
            />
        )
    }

    

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.renderAuthListView()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex:1
	},
	noContent:{
        marginTop:screenH*0.3,
        justifyContent: 'center',
        alignItems: 'center'
    },
    addAuthTouch:{
        justifyContent:'center',
        alignItems:'center',
        marginTop:30,
        width: '40%',
        height: 40,
        backgroundColor: Colors.tabActiveColor,
        borderRadius: 5
    },
    addAuthText:{
        color: Colors.white,
        fontSize: 14
    },
    item:{
        marginTop: 20,
        marginHorizontal: 16,
        paddingVertical: 10,
        borderRadius: 5,
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 16
    },
    createTime:{
        marginVertical: 5,
        fontSize: 13
    },
    count:{
        fontSize: 13
    },
    asyncTouch:{
        paddingHorizontal: 16,
        height: 35, 
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: Colors.tabActiveColor,
        borderRadius: 5,
        marginRight: 16
    },
    //侧滑菜单的样式
	quickAContent: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginRight: 16,
		marginTop: 20,
		overflow: 'hidden',
        paddingVertical: 0.5
	},
	quick: {
		alignItems: 'center',
		justifyContent: 'center',
		width: maxSwipeDistance / countSwiper,
        backgroundColor: Colors.themBgRed,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
	},
    quickText:{
        color: Colors.white,
        fontSize: 14
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(MideaAuthonList)

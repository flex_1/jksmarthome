/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	ImageBackground,
	DeviceEventEmitter,
	SwipeableFlatList,
	Alert,
	Modal
} from 'react-native';
import { postJson } from '../../util/ApiRequest';
import { NetUrls } from '../../common/Constants';
import { Colors } from '../../common/Constants';
import ToastManager from '../../common/CustomComponent/ToastManager';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import Echarts from '../../third/echarts/index';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';

// 查看几年内的数据（除今年外）
const YearsCount = 3

class ElectricityDetail extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'用电详情'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text:'筛选日期',onPress:()=>{
                        navigation.getParam('filterBtnClick')()
                    }}
                ]}/>
            ),
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
	}

	constructor(props) {
		super(props);
		const { getParam, setParams } = this.props.navigation
		this.houseId = getParam('houseId')
		
		this.state = {
			indicators: ['日', '周', '月', '年'],
			select: '日',
			showPopCell: false,
			chartData: null,
			chartTittle:  '用电量',
			chartUnit: 'kWh',
			currentDateType: 1,  // 1 日  2 周  3 月  4 年
			quantity: 0,
			price: 0,
			dateTitle: '',
			modalVisible: false,
			pickerData: [],
			isLoadingChart: false,
            deadline: ''
		}

		setParams({
			filterBtnClick: ()=>{
				this.setState({
					modalVisible: true
				})
			}
		})
	}

	componentDidMount() {
		this.requestElectData()
		this.getDateLists()
	}

	componentWillUnmount() {
		
	}

	// 获取日期数据 (算法)
	getDateLists(){
		let nowDate = new Date()
		let nowYear = parseInt(nowDate.getFullYear())
		let nowMonth = parseInt(nowDate.getMonth() + 1)
		let nowDay = parseInt(nowDate.getDate())

		let pickerData = []
		for(let year=nowYear-YearsCount; year<=nowYear; year++){
			let yearDic = {}
			let months = [{'-':['-']}]
			let montCount = year == nowYear ? nowMonth : 12
			for(let month = 1; month <= montCount; month++){
				let monthDic = {}
				let days = ['-']
				let daysCount = new Date(year,month,0).getDate()
				// 当年 当月
				if(month == nowMonth && year == nowYear){
					daysCount = nowDay	
				}
				for(let day=1; day<=daysCount; day++){
					days.push(day+'日')
				}
				monthDic[month+'月'] = days
				months.push(monthDic)
			}
			yearDic[year+'年'] = months
			pickerData.push(yearDic)
		}
		this.setState({
			pickerData: pickerData,
			selectData: [nowYear+'年', nowMonth+'月', nowDay+'日']
		})
	}

	//请求电量
	async requestElectData(condition) {
		SpinnerManager.show()
		let params = {}
		if(condition){
			params = condition
		}else{
			params.type = this.state.currentDateType
		}
		this.setState({isLoadingChart: true})
		try {
			let data = await postJson({
				url: NetUrls.energyTotal,
				params: {
					...params,
					id: this.houseId
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.setState({
					quantity: data.result?.quantity || 0,
					chartData: data.result?.list || [],
					dateTitle: data.result?.date || '',
					price: data.result?.price || 0,
                    deadline: data.result?.deadline
				},()=>{
					this.setState({
						isLoadingChart: false
					})
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			console.log(JSON.stringify(error));
		}
	}

	// 按年月日 筛选
	filterDetialData(data,index){
		this.setState({ 
			modalVisible: false,
			selectData: data
		})
		let dataStr = data.toString()
		let reg = /[\u4e00-\u9fa5]/g
		dataStr = dataStr.replace(reg, '')
		let params = {date: dataStr}
		this.requestElectData(params)
	}

	// 年月日
	getHeaderSegment() {
        const Colors = this.props.themeInfo.colors

        let list = this.state.indicators.map((val, index) => {
            let bgStyle = Colors.themeBg
            let textStyle = Colors.themeTextLight
            if (this.state.select == val) {
                bgStyle = Colors.themeButton
                textStyle = Colors.white
            }
            return (
                <View style={styles.segmentWrapper} key={'energy_index_' + index}>
                    <TouchableOpacity style={[styles.segmentTouch, { backgroundColor: bgStyle }]} onPress={() => {
                        this.setState({
                            select: val,
							showPopCell: false,
							currentDateType: index + 1
                        },()=>{
							this.requestElectData()
						})
                        
                    }}>
                        <Text style={{ color: textStyle, fontSize: 13 }}>{val}</Text>
                    </TouchableOpacity>
                </View>
            )
        })
        return (
            <View style={{ marginTop: 10, paddingHorizontal: 12, flexDirection: 'row' }}>
                {list}
            </View>
        )
	}
	
	getXDate(timeStamp){
        let currentSelect = this.state.select
        try {
            let d = new Date(timeStamp)
            var year = d.getFullYear(); 
            var month = d.getMonth()+1; 
            var date = d.getDate(); 
            var hour = d.getHours(); 
            var minute = d.getMinutes(); 
            var second = d.getSeconds();

            switch (currentSelect) {
                case '日': return hour + '时'
                case '周': return date + '日'
                case '月': return date + '日'
                case '年': return month + '月'
                default:   return 0
            }
        } catch (error) {
            return 0    
        }
	}

	_Echart(){
        const isDark = this.props.themeInfo.isDark

		if(!this.state.chartData || this.state.isLoadingChart){
			return(
                <View style={{flex:1,justifyContent:'center',alignItems:'center',marginTop:100}}>
                    <Text style={{color:Colors.themeTextLightGray}}>数据加载中...</Text>
                </View>
            )
		}
        if(this.state.chartData.length<=0){
            return(
                <View style={{flex:1,justifyContent:'center',alignItems:'center',marginTop:100}}>
                    <Text style={{color:Colors.themeTextLightGray}}>暂无数据</Text>
                </View>
            )
		}
		let eleData = this.state.chartData
        let eleDataX = []
        let eleDataY = []
        for (const data of eleData) {
			//let timeX = this.getXDate(data.time)
			let timeX = data.timeX
			eleDataX.push(timeX)
			let valueY = data.quantity || 0
			if( typeof(valueY) == 'number' ){
				eleDataY.push({value: valueY, time: data.dateTime})
			}else{
				eleDataY.push(0)
			}
		}
		// eleDataY = [1,2,3,4,5,6,7,8,9,4,5,6,7,0,6,7,8,0,1,2,3,4,5,6,7,8,9,4,5,10,15]
		// eleDataX = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
		// 柱状图 是否可以滚动
		let dataZoom = null
		let animation = true
		if(eleDataY.length > 18){
			dataZoom = {
				type: 'inside',
				start: 0,
				end: 60,
				zoomLock: true,
				filterMode: 'empty'
			}
			animation = false
		}
		let option = {
			color: isDark ? ['#2650D7'] : ['#1D94FE'],
			tooltip: {
				trigger: 'axis',
				axisPointer: {
					type: 'line',
					lineStyle:{
						width:1,
						color: Colors.borderLightGray
					}
				},
				formatter: function(data){
					let axisData = data[0]
                    return (
						axisData.data.time
                        +'</br>用电: '+ axisData.data.value
					)
                }
			},
			legend: {show: false},
			grid: {
				left: '3%',
				right: '4%',
				bottom: '3%',
				containLabel: true
			},
			xAxis: {
				data: eleDataX,
				axisLine: {
					lineStyle: {
						color: Colors.borderLightGray
					}
				},
				axisLabel: {
					textStyle:{
						color: Colors.themeTextLightGray,
					},
				},
				axisTick:{
					show: false
				},
			},
			clickable:true,
			yAxis: {
				axisLine: {
					lineStyle: {
						color: Colors.borderLightGray
					}
				},
				axisLabel: {
					textStyle:{
						color: Colors.themeTextLightGray,
					},
				},
				axisTick:{
					show: false
				},
				splitLine:{
					lineStyle:{
						type: 'dashed',
						color: Colors.borderLightGray
					}
				},
			},
			dataZoom: dataZoom,
			animation: animation,
			series: [{
				name: '用电量',
				type: 'bar',
				// barWidth: 20,
				data: eleDataY
			}]
		};
		return (
			<Echarts
				option={option} 
				height={300} 
				onPress={(data)=>{
					// console.log('123');
				}}
			/>
		)
	}

    renderDeadline(){
        if(!this.state.deadline){
            return null
        }
        const Colors = this.props.themeInfo.colors

        return(
            <Text style={[styles.deadLineText,{color: Colors.themeTextLight}]}>数据统计截止日期: {this.state.deadline}</Text>
        )
    }

	getCurrentVaule(){
        const Colors = this.props.themeInfo.colors

		return (
            <View style={styles.headerContain}>
                <View style={[styles.cardWrapper,{backgroundColor: Colors.themeBg}]}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontSize: 30, fontWeight: 'bold',color: Colors.themeText }}>{this.state.quantity || '0'}</Text>
                        <Text style={[styles.unit,{color: Colors.themeTextLight}]}>kWh</Text>
                    </View>
                    <Text style={styles.electTips}>本{this.state.select}用电量</Text>
                </View>
                <View style={[styles.cardWrapper,{backgroundColor: Colors.themeBg}]}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontSize: 15, color: Colors.themeText, marginTop: 10 }}>￥</Text>
                        <Text style={[styles.price,{color: Colors.themeText}]}>{this.state.price || '0'}</Text>
                    </View>
                    <Text style={styles.electTips}>预估电费</Text>
                </View>
            </View>
        )
	}

    // 电量 图表
    getElerticChartView() {
        const Colors = this.props.themeInfo.colors
        let dateTitle = this.state.dateTitle ? '（'+ this.state.dateTitle  +'）' : ''
        
        return (
			<View style={{ marginTop: 20, width:'100%'}}>
				<View style={{flexDirection:'row',alignItems:'center'}}>
					<Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 16,color: Colors.themeText }}>用电量统计图</Text>
					<Text style={{ fontSize: 18, marginLeft: 5,color: Colors.themeButton }}>{dateTitle}</Text>
				</View>
				<View style={{ marginTop: 10, width:'100%'}}>
                	{this._Echart()}
					<Text style={[styles.chartUnit,{color: Colors.themeTextLight}]}>单位: {this.state.chartUnit}</Text>  
            	</View>
			</View>
            
        )
	}
	
	getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
   	showDoublePicker() {
        window.DoubulePicker.init({
            pickerData: this.state.pickerData,
            pickerConfirmBtnText: '确定  ',
            pickerCancelBtnText: '',
            pickerTitleText: '',
            pickerCancelBtnColor: [255, 255, 255, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: this.state.selectData,
            onPickerConfirm: (data,index) => {
				this.filterDetialData(data,index)
            },
            onPickerCancel: data => {
                this.setState({ modalVisible: false })
            },
            onPickerSelect: (data,index) => {
                
            }
        });
        window.DoubulePicker.show()
    }

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<ScrollView style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getHeaderSegment()}
                {this.renderDeadline()}
				{this.getCurrentVaule()}
				{this.getElerticChartView()}
				{this.getModal()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	contentStyle: {
		paddingBottom: 30
	},
	navTitle: {
		fontSize: 24,
		fontWeight: 'bold',
		marginLeft: 15,
		textAlignVertical: 'center',
		color: Colors.themeTextBlack,
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: '100%',
		paddingRight: 16,
		paddingLeft: 10,
	},
	navText: {
		fontSize: 15,
		color: Colors.tabActiveColor
	},
	segmentWrapper: {
		flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    segmentTouch: {
        height: 30,
        width: '100%',
        borderRadius: 15,
        justifyContent: 'center',
		alignItems: 'center'
	},
	popCellView:{
		position:'absolute', 
		backgroundColor:'#1D94FE',
		justifyContent:"center",
		alignItems:'center',
		borderRadius: 5
	},
	chartUnit:{
		position:'absolute',
		left:16,
		top:'5%',
		fontSize:12
	},
	modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
	},
	headerContain:{ 
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
        paddingHorizontal: 16
	},
	cardWrapper: {
        width: '48%',
        height: 100,
        borderRadius: 5,
        paddingVertical: 10,
		paddingHorizontal: 12,
		justifyContent: 'space-between'
	},
	electTips: {
        color: Colors.themeBGInactive,
        fontSize: 15
    },
    price: {
        fontSize: 30,
        fontWeight: 'bold',
        marginLeft: 5
    },
    unit: {
        fontSize: 15,
        marginLeft: 5,
        marginTop: 10
    },
    deadLineText:{
        marginLeft: 18,
        fontSize: 13,
        marginTop: 5
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ElectricityDetail)

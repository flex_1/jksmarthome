/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	View,
	Text,
	StyleSheet,
	InteractionManager,
	Animated,
	Easing,
	Image,
	Alert,
	Vibration,
	Dimensions,
	Platform,
	TouchableOpacity,
	DeviceEventEmitter
} from 'react-native';
import { postJson, getRequest } from '../../util/ApiRequest';
import { Colors, NetUrls, NotificationKeys } from '../../common/Constants';
import { RNCamera } from 'react-native-camera';
import { StatusBarHeight } from '../../util/ScreenUtil';
import ToastManager from '../../common/CustomComponent/ToastManager';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import YSProtocolDialog from '../CommonPage/YSProtocolDialog';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const statubarHeight = StatusBarHeight
const back_groud_color = Colors.translucent

class AddDeviceQRCode extends Component {

	static navigationOptions = {
		header: null,
        headerBackground: <HeaderBackground hiddenBorder={true}/>
	}

	constructor(props) {
		super(props);
		const {getParam} = this.props.navigation
        this.roomId = getParam('roomId')
        this.callBack = getParam('callBack')

		this.state = {
			show: true,
			animation: new Animated.Value(0),
			showProtocol: false,
			serialNo: '',
		}
	}

	componentDidMount() {
		// InteractionManager.runAfterInteractions(() => {
		// 	this.startAnimation()
		// });

		this.startAnimation()

        // TODO
        // this.checkSerialNo('https://smarthouse.hansa-tec.com/a?c=hs_wifi_001')
	}

	componentWillUnmount() {
		
	}

	startAnimation() {
		if (this.state.show) {
			this.state.animation.setValue(0);
			Animated.timing(this.state.animation, {
				toValue: 1,
				duration: 1500,
				easing: Easing.linear,
			}).start(() => this.startAnimation());
		}
	}

	//页面中Alert功能一样，统一Alert
	alert(title, msg) {
		Alert.alert(
			title,
			msg,
			[
				{
					text: '确定', onPress: () => {
						this.state.show = true
						this.startAnimation()
					}
				}
			],
			{ cancelable: false }
		)
	}

	// 检查 序列号
	async checkSerialNo(seriaNo) {
		let params = {}
		if(this.roomId){
			params.roomId = this.roomId
		}
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.checkSeriaNo,
				params: {
					...params,
					seriaNo: seriaNo
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
                let msg = data.result || data.msg
				this.nextStep(data.result, seriaNo, msg)

			} else if(data.code == 1000){
                let code = data.result?.code   // 配网类型
                if(code == '2'){
                    this.props.navigation.replace('WiFiGuide', {
						typeCode:  data.result?.typeCode,
						remarkImg: data.result?.remarkImg,
                        remark: data.result?.remark
					})
                }
            } else {
				this.alert('提示', data.msg);
			}
		} catch (error) {
			SpinnerManager.close()

			this.alert('提示', '网络错误，请稍后重试');
		}
	}

	nextStep(data, seriaNo, msg){
		const { popToTop, pop } = this.props.navigation

		// 直接添加网关（不需要进入 下一步）
        pop()
        this.callBack && this.callBack()
        DeviceEventEmitter.emit(NotificationKeys.kHouseInfoNotification)
        DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
		Alert.alert(
            '提示',
            msg,
            [ {text: '知道啦', onPress: () => { }, style: 'cancel'}]
        )
	}

	barcodeReceived(e) {
		if (this.state.show) {
			this.state.show = false;
			if (e) {
				// Vibration.vibrate([0, 500], false);
				let result = e.data;

				const isYs = result.indexOf('ys');

				//非萤石摄像头
				if(isYs == -1) { 
					this.checkSerialNo(result);
					return;
				}else{
                    this.setState({
                        showProtocol: true, 
                        serialNo: result
                    });
                }
				//萤石摄像头
				// this.props.navigation.replace('YsProtocolAdvisory', {callBack: () => {
				// 	this.checkSerialNo(result);
				// }});
			} else {
				this.alert('提示', '扫描失败，请将手机对准二维码重新尝试');
			}
		}
	}

	getHeaderView() {
		const { pop, navigate, replace } = this.props.navigation
		return (
			<View style={{position: 'absolute',zIndex:10,top:0,left:0,width:'100%'}}>
				<View style={styles.statusPlacehoder} />
				<View style={styles.header}>
					<TouchableOpacity style={{ height: 44, paddingHorizontal: 10, alignItems: 'center', flexDirection: 'row' }} onPress={() => {
						pop()
					}}>
						<Image style={{ width: 10, height: 16, marginRight: 15 }} source={require('../../images/back_white.png')} />
						<Text style={{ color: Colors.white, fontSize: 24, fontWeight: 'bold' }}>扫码添加设备</Text>
					</TouchableOpacity>
					<View style={{ flex: 1 }} />
					<TouchableOpacity style={styles.navTouch} onPress={() => {
						replace('AddDevice',{roomId: this.roomId,callBack: this.callBack})
					}}>
						<Text style={styles.navRText}>手动添加</Text>
					</TouchableOpacity>
				</View>
			</View>
		)
	}

	getNotAuthorizedView(){
		return(
			<View style={{flex:1,justifyContent:'center',alignItems:'center',paddingHorizontal:40}}>
				<Text  style={{color:Colors.white}}>您已关闭App相机权限，若要开启，请前往设置页面打开App的相机权限</Text>
			</View>
		)
	}

	// ios
	getiOSScanView() {
		return (
			<RNCamera
				style={styles.preview}
				type={RNCamera.Constants.Type.back}
				barCodeTypes={[RNCamera.Constants.BarCodeType.qr]}
				flashMode={RNCamera.Constants.FlashMode.auto}
				captureAudio={false}
				onBarCodeRead={(e) => this.barcodeReceived(e)}
				notAuthorizedView={this.getNotAuthorizedView()}
			>
				
				<View style={{ height: (height - 264) / 3, width: width, backgroundColor: back_groud_color }}>
				</View>
				<View style={{ flexDirection: 'row' }}>
					<View style={styles.itemStyle} />
					<View style={styles.rectangle}>
						<Image
							style={[styles.rectangle, { position: 'absolute', left: 0, top: 0 }]}
							source={require('../../images/icon_scan_rect.png')}
						/>
						<Animated.View style={[styles.animatedStyle, {
							transform: [{
								translateY: this.state.animation.interpolate({
									inputRange: [0, 1],
									outputRange: [0, 200]
								})
							}]
						}]}>
						</Animated.View>
					</View>
					<View style={styles.itemStyle} />
				</View>
				<View style={{ flex: 1, backgroundColor: back_groud_color, width: width, alignItems: 'center' }}>
					<Text style={styles.textStyle}>将二维码放入框内，即可自动扫描</Text>
				</View>
			</RNCamera>
		)
	}

	// android
	getAndroidScanView() {
		return (
			<RNCamera
				style={styles.preview}
				type={RNCamera.Constants.Type.back}
				googleVisionBarcodeType={RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeType.QR_CODE}
				flashMode={RNCamera.Constants.FlashMode.auto}
				captureAudio={false}
				onBarCodeRead={(e) => this.barcodeReceived(e)}
				notAuthorizedView={this.getNotAuthorizedView()}
			>
				
				<View style={{ height: (height - 244) / 3, width: width, backgroundColor: back_groud_color }}>
				</View>
				<View style={{ flexDirection: 'row' }}>
					<View style={styles.itemStyle} />
					<View style={styles.rectangle}>
						<Image
							style={[styles.rectangle, { position: 'absolute', left: 0, top: 0 }]}
							source={require('../../images/icon_scan_rect.png')}
						/>
						<Animated.View style={[styles.animatedStyle, {
							transform: [{
								translateY: this.state.animation.interpolate({
									inputRange: [0, 1],
									outputRange: [0, 200]
								})
							}]
						}]}>
						</Animated.View>
					</View>
					<View style={styles.itemStyle} />
				</View>
				<View style={{ flex: 1, backgroundColor: back_groud_color, width: width, alignItems: 'center' }}>
					<Text style={styles.textStyle}>将二维码放入框内，即可自动扫描</Text>
				</View>
			</RNCamera>
		)
	}

	//萤石云协议弹窗
	renderProtocolDialog() {
		return (
			<YSProtocolDialog
				close = {() => {
					this.setState({
						showProtocol: false,
						show: true,
					}, () => {
						this.startAnimation();
					});
				}}
				serviceAgreement = {() => {
					this.props.navigation.navigate('Protocol',{title:'翰萨科技摄像头服务协议',type:3});
				}}
				privacyPolicy = {() => {
					this.props.navigation.navigate('Protocol',{title:'翰萨科技摄像头隐私政策',type:4});
				}}
				agree = {() => {
					this.setState({showProtocol: false, show: false});
					this.checkSerialNo(this.state.serialNo);
				}}
			 />
		)
	}

	render() {
		let scanView = null;
		if (Platform.OS === 'ios') {
			scanView = this.getiOSScanView()
		} else {
			scanView = this.getAndroidScanView()
		}

		return (
			<View style={styles.container}>
				{this.getHeaderView()}
				{scanView}
				{this.state.showProtocol ? this.renderProtocolDialog() : null}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: Colors.black
	},
	preview: {
		flex: 1,
		paddingTop:44+statubarHeight
	},
	statusPlacehoder:{
		width:'100%',
		height:statubarHeight,
		backgroundColor: back_groud_color,
	},
	header: {
		height: 44,
		width: '100%',
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: back_groud_color,
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 16,
	},
	navRText: {
		color: Colors.white,
		fontSize: 15
	},
	itemStyle: {
		backgroundColor: back_groud_color,
		width: (width - 200) / 2,
		height: 200
	},
	textStyle: {
		color: '#fff',
		marginTop: 20,
		fontWeight: 'bold',
		fontSize: 18
	},
	animatedStyle: {
		height: 2,
		backgroundColor: '#00c050'
	},
	rectangle: {
		height: 200,
		width: 200,
	}
});

export default AddDeviceQRCode

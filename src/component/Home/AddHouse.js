/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	DeviceEventEmitter,
	TextInput,
    BackHandler,
    Keyboard,
    Modal
} from 'react-native';
import { postJson, getRequest } from '../../util/ApiRequest';
import { NetUrls, NotificationKeys } from '../../common/Constants';
import { Colors } from '../../common/Constants';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import LocalTokenHandler from '../../util/LocalTokenHandler';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';

const china_area_json_new = require('../../resources/china_area_new.json')
const dismissKeyboard = require('dismissKeyboard');

let cityData = null

class AddHouse extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: navigation.getParam('force') ? null : <HeaderLeft navigation={navigation} backTitle={navigation.getParam('title')}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text:'保存', onPress:()=>{
                        navigation.getParam('addBtnClick')()
                    }}
                ]}/>
            ),
            headerBackground: <HeaderBackground/>,
            gesturesEnabled: !navigation.getParam('force')
        }
	}

	constructor(props) {
		super(props);
		const {getParam,setParams} = this.props.navigation
		this.callBack = getParam('callBack')
		this.isForce = getParam('force')
		this.houseId = getParam('id')

		this.state = {
			avatarSource: null,
			name: getParam('name') || '',
            citySelectData: getParam('city') || [],
            addressText: getParam('address') || '',
            modalVisible: false
        }
        
        setParams({
            addBtnClick: ()=>{ this.requestAddHouse() }
        })
	}

	componentDidMount() {
		BackHandler.addEventListener('hardwareBackPress',this.onBackButtonPressAndroid);
        this.prepareForCityData()
	}

	componentWillUnmount(){
		BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
	}

	onBackButtonPressAndroid = ()=>{
		return this.isForce
	}

    // 准备城市 数据
    prepareForCityData(){
        let data = []
        for (const province of china_area_json_new) {
            let cityArrs = []
            for (const city of province.cities.city) {
                let areaArrs = []
                for (const area of city.areas.area) {
                    let areaName = area.ssqname
                    areaArrs.push(areaName)
                }
                let cityTemp = {}
                cityTemp[city.ssqname] = areaArrs
                cityArrs.push(cityTemp)
            }
            let provinceTemp = {}
            provinceTemp[province.ssqname] = cityArrs
            data.push(provinceTemp)
        }
        cityData = data
    }

	//网络请求
	async requestAddHouse() {
        Keyboard.dismiss()
		if(!this.state.name){
			ToastManager.show('请输入房屋名称')
			return
		}
        if(!this.state.citySelectData || !this.state.citySelectData.length){
			ToastManager.show('请选择所在区域')
			return
		}
        if(!this.state.addressText){
			ToastManager.show('请输入详细地址')
			return
		}
		const {pop} = this.props.navigation
		let params={}
		if(this.houseId){
			params.id = this.houseId
		}
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.addHouse,
				params: {
					...params,
					name: this.state.name,
                    city: this.state.citySelectData,
                    address: this.state.addressText
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.callBack && this.callBack()
				pop()
                if(this.houseId){
                    ToastManager.show('保存成功')
                }else{
                    ToastManager.show('添加成功')
                }
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error));
		}
	}

    getModal() {
        if(!cityData){
            return null
        }
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        let citySelectData = this.state.citySelectData
        if(!Array.isArray(this.state.citySelectData)){
            citySelectData = []
        }
        window.CustomPicker.init({
            pickerData: cityData,
            pickerTitleText: '选择省市区',
            selectedValue: citySelectData,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false,
                    citySelectData: data
                })
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })
            },
            // isDark: this.props.themeInfo.isDark
        })
    }

    // 获取城市区域
    getPickerView(){
        const Colors = this.props.themeInfo.colors;

        let areaText = '点击选择房屋所在省市区'
        let textStyle = {fontSize:15,color:Colors.themeInactive}
        if(this.state.citySelectData.length ){
            areaText = this.state.citySelectData.toString()
            textStyle.color = Colors.themeText
            if(Platform.OS == 'android'){
                textStyle.height = '100%'
                textStyle.textAlignVertical = 'center'
            }
        }
        return(
            <TouchableOpacity 
                activeOpacity={0.8} 
                style={styles.selectTouch} 
                onPress={()=>{
                    dismissKeyboard()
                    this.setState({
                        modalVisible: true
                    })
                }}
            >
                <Text numberOfLines={2} style={textStyle}>{areaText}</Text>
            </TouchableOpacity>
        )
    }

	getInput() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.wrapper,{backgroundColor: Colors.themeBg}]}>
				<View style={styles.inputWrapper}>
					<Text style={[styles.itemTitle,{color: Colors.themeText}]}>房屋名称</Text>
					<TextInput
						defaultValue={this.state.name}
						style={[styles.input,{color: Colors.themeText}]}
						placeholder={'请输入房屋名称(不超过8个字)'}
                        
						placeholderTextColor={Colors.themeInactive}
						onChangeText={(text) => {
							this.state.name = text
						}}
						returnKeyType={'done'}
					/>
				</View>
                <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                <View style={styles.inputWrapper}>
                    <Text style={[styles.itemTitle,{color: Colors.themeText}]}>所在区域</Text>
                    {this.getPickerView()}
                </View>
                <View style={[styles.line,{backgroundColor: Colors.split}]}/>
                <View style={styles.inputWrapper}>
                    <TextInput
                        defaultValue={this.state.addressText}
						style={[styles.input,{color: Colors.themeText}]}
						placeholder={'请输入详细地址'}
						placeholderTextColor={Colors.themeInactive}
						onChangeText={(text) => {
							this.state.addressText = text
						}}
						returnKeyType={'done'}
					/>
                </View>
			</View>
		)
	}

	logout() {
		LocalTokenHandler.remove(() => {
			DeviceEventEmitter.emit(NotificationKeys.kLogoutNotification)
		})
	}

	getLogoutButton(){
		if(!this.isForce){
			return null
		}
		return (
			<View style={{ paddingHorizontal:16,marginTop:50 }}>
				<TouchableOpacity
					activeOpacity={0.7}
					style={styles.btn}
					onPress={() => { this.logout() }}
				>
					<Text style={{ color:Colors.white,fontSize:15 }}>暂不想创建房屋,退出当前账号</Text>
				</TouchableOpacity>
			</View>
		)
	}


	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                <ScrollView keyboardShouldPersistTaps="handled">
                    {this.getInput()}
				    {this.getLogoutButton()}
                </ScrollView>
                {this.getModal()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	navTitle: {
		fontSize: 24,
		fontWeight: 'bold',
		marginLeft: 15,
		textAlignVertical: 'center',
		color: Colors.themeTextBlue,
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 16,
		paddingLeft: 10
	},
	navText: {
		color: Colors.themeBG,
		fontSize: 15
	},
    wrapper:{
        marginHorizontal: 16, 
        marginTop: 15, 
        paddingHorizontal: 16,
        paddingVertical:5,
        borderRadius: 5
    },
	inputWrapper: {
		width: '100%',
		height: 55,
		alignItems: 'center',
		flexDirection: 'row'
	},
	input: {
		flex: 1,
		height: 40,
		paddingRight: 10,
        fontSize: 15
	},
    selectTouch:{
        flex:1,
        paddingVertical:2,
        justifyContent:'center',
        paddingRight: 10
    },
	btn:{
		width:'100%',
		height: 40,
		backgroundColor:Colors.themBgRed,
		borderRadius:4,
		justifyContent:'center',
		alignItems:'center'
	},
    line:{
        width: '100%',
        height: 1
    },
    titleText:{
        width: '30%',
        fontSize:15
    },
    itemTitle:{
        fontSize: 15,
        fontWeight: '500',
        width: 90
    },
    remarkInput:{
		width:'100%',
		height:80,
		paddingHorizontal:16,
      	textAlignVertical:'top',
		fontSize:14,
      	borderRadius:4,
    },
    btnTouch:{
        width:'100%',
        height:40,
        backgroundColor:Colors.tabActiveColor,
        borderRadius: 4,
        justifyContent:'center',
        alignItems:'center'
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(AddHouse)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	ImageBackground,
	DeviceEventEmitter,
	SwipeableFlatList,
	Alert,
	Modal
} from 'react-native';
import { postJson } from '../../util/ApiRequest';
import { NetUrls } from '../../common/Constants';
import { Colors } from '../../common/Constants';
import ToastManager from '../../common/CustomComponent/ToastManager';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import { BottomSafeMargin } from '../../util/ScreenUtil';

// 查看几年内的数据（除今年外）
const YearsCount = 3

class ElectricityRank extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'用电排行'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text:'用电详情',onPress:()=>{
                        navigation.navigate('ElectricityDetail',{
                            houseId: navigation.getParam('houseId'),
                            title: '用电详情'
                        })
                    }}
                ]}/>
            ),
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
	}

	constructor(props) {
		super(props);
		const { getParam, setParams } = this.props.navigation
		this.houseId = getParam('houseId')
		
		this.state = {
            indicators: ['日', '周', '月', '年','自定义'],
			select: '日',
            currentDateType: 1,  // 1 日  2 周  3 月  4 年
			quantity: 0,
			price: 0,
			modalVisible: false,
            pickerData: [],
            selectData: '',
            
            rankList: null,
            rankIndicators: [{name: '房间', rank: 2}, {name: '设备', rank: 3}, {name: '楼层', rank: 1}],
            rankIndex: 0,
            currentRanking: 2,
            dataParams: null,
            deadline: ''
		}
	}

	componentDidMount() {
		this.requestElectData()
		this.getDateLists()
	}

	componentWillUnmount() {
		
	}

	// 获取日期数据 (算法)
	getDateLists(){
		let nowDate = new Date()
		let nowYear = parseInt(nowDate.getFullYear())
		let nowMonth = parseInt(nowDate.getMonth() + 1)
		let nowDay = parseInt(nowDate.getDate())

		let pickerData = []
		for(let year=nowYear-YearsCount; year<=nowYear; year++){
			let yearDic = {}
			let months = [{'-':['-']}]
			let montCount = year == nowYear ? nowMonth : 12
			for(let month = 1; month <= montCount; month++){
				let monthDic = {}
				let days = ['-']
				let daysCount = new Date(year,month,0).getDate()
				// 当年 当月
				if(month == nowMonth && year == nowYear){
					daysCount = nowDay	
				}
				for(let day=1; day<=daysCount; day++){
					days.push(day+'日')
				}
				monthDic[month+'月'] = days
				months.push(monthDic)
			}
			yearDic[year+'年'] = months
			pickerData.push(yearDic)
		}
		this.setState({
			pickerData: pickerData,
			selectData: [nowYear+'年', nowMonth+'月', nowDay+'日']
		})
	}

	//请求电量
	async requestElectData() {
		SpinnerManager.show()
		let params = {}
		if(this.state.dataParams){
			params = this.state.dataParams
		}else{
			params.type = this.state.currentDateType
        }
		try {
			let data = await postJson({
				url: NetUrls.energyTotalRanking,
				params: {
                    ...params,
                    ranking: this.state.currentRanking,
                    id: this.houseId
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.setState({
					quantity: data.result?.quantity || 0,
                    price: data.result?.price || 0,
                    rankList: data.result?.list || [],
                    deadline: data.result?.deadline
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			console.log(JSON.stringify(error));
		}
	}

	// 按年月日 筛选
	filterDetialData(data,index){
		let dataStr = data.toString()
		let reg = /[\u4e00-\u9fa5]/g
		dataStr = dataStr.replace(reg, '')
		let params = {date: dataStr}
        
        this.setState({ 
			modalVisible: false,
            selectData: data,
            select: '自定义',
            currentDateType: 5,
            dataParams: params
		},()=>{
            this.requestElectData()
        })
	}

	// 年月日
	getHeaderSegment() {
        const Colors = this.props.themeInfo.colors

        let list = this.state.indicators.map((val, index) => {
            let bgStyle = Colors.themeBg
            let textStyle = Colors.themeTextLight
            if (this.state.select == val) {
                bgStyle = Colors.themeButton
                textStyle = Colors.white
            }
            return (
                <View style={styles.segmentWrapper} key={'energy_index_' + index}>
                    <TouchableOpacity style={[styles.segmentTouch, { backgroundColor: bgStyle }]} onPress={() => {
                        if(val == '自定义'){
                            this.setState({
                                modalVisible: true
                            })
                        }else{
                            this.setState({
                                select: val,
                                currentDateType: index + 1,
                                dataParams: null
                            },()=>{
                                this.requestElectData()
                            })
                        }
                    }}>
                        <Text style={{ color: textStyle, fontSize: 13 }}>{val}</Text>
                    </TouchableOpacity>
                </View>
            )
        })
        return (
            <View style={{ marginTop: 10, paddingHorizontal: 12, flexDirection: 'row' }}>
                {list}
            </View>
        )
	}

    renderDeadline(){
        if(!this.state.deadline){
            return null
        }
        const Colors = this.props.themeInfo.colors

        return(
            <Text style={[styles.deadLineText,{color: Colors.themeTextLight}]}>数据统计截止时间: {this.state.deadline}</Text>
        )
    }
	
	getCurrentVaule(){
        const Colors = this.props.themeInfo.colors

        let tips = '本' + this.state.select + '用电量'
        if(this.state.currentDateType == 5){
            tips = this.state.selectData.toString()
            tips = tips.replace(/,/g,"")
            tips = tips.replace(/-/g,"")
        }

		return (
            <View style={styles.headerContain}>
                <View style={[styles.cardWrapper,{backgroundColor: Colors.themeBg}]}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontSize: 30, fontWeight: 'bold',color: Colors.themeText }}>{this.state.quantity || '0'}</Text>
                        <Text style={[styles.unit,{color: Colors.themeTextLight}]}>kWh</Text>
                    </View>
                    <Text style={styles.electTips}>{tips}</Text>
                </View>
                <View style={[styles.cardWrapper,{backgroundColor: Colors.themeBg}]}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontSize: 15, color: Colors.themeText, marginTop: 10 }}>￥</Text>
                        <Text style={[styles.price,{color: Colors.themeText}]}>{this.state.price || '0'}</Text>
                    </View>
                    <Text style={styles.electTips}>预估电费</Text>
                </View>
            </View>
        )
	}

    renderIndicator(){
        const Colors = this.props.themeInfo.colors

        let indicators = this.state.rankIndicators.map((value, index)=>{
            const textWeigt = index == this.state.rankIndex ? 'bold' : 'normal'
            const textColor = index == this.state.rankIndex ? Colors.themeText : Colors.themeTextLight
            const textSize  = index == this.state.rankIndex ? 20: 15

            return(
                <TouchableOpacity 
                    key={'rank_indi_'+index} 
                    style={styles.siteTouch} 
                    onPress={()=>{
                        this.setState({
                            rankIndex: index,
                            currentRanking: value.rank
                        },()=>{
                            this.requestElectData()
                        })
                    }}
                >
                    <Text style={{fontSize: textSize, fontWeight: textWeigt, color: textColor}}>{value.name}</Text>
                </TouchableOpacity>
            )
        })
        return(
            <View style={{paddingLeft:6,flexDirection:'row',marginTop:15}}>
                {indicators}
            </View>
        )
    }

    renderNoDataView(type){
        let text = ''
        if(type == 1){
            text = '数据加载中...'
        }else if(type == 2){
            text = '暂无数据'
        }else if(type == 3){
            text = '暂无数据'
        }
        const Colors = this.props.themeInfo.colors

        return (
            <View style={{width:'100%',alignItems:'center', flex: 1}}>
                <Text style={{fontSize: 14, color: Colors.themeTextLight,marginTop:60}}>{text}</Text>
            </View>
        )
    }
    
    renderRank(){
        const Colors = this.props.themeInfo.colors

        if(!this.state.rankList){
            return this.renderNoDataView(1)
        }
        if(this.state.rankList.length <= 0){
            return this.renderNoDataView(2)
        }
        if(this.state.rankList[0]?.quantity == 0){
            return this.renderNoDataView(3)
        }

        const max = this.state.rankList[0].quantity
        if(!max) return null

        let lineImg = require('../../images/user_manage/chart_line_floor.png')
        // if(this.state.rankIndex == 1){
        //     lineImg = require('../../images/user_manage/chart_line_device.png')
        // }else if(this.state.rankIndex == 2){
        //     lineImg = require('../../images/user_manage/chart_line_room.png')
        // }

        let rankList = this.state.rankList.map((value, index)=>{
            let r = value.quantity * 100.0 / max
            r = r+'%'

            let rankBg = require('../../images/homeBG/numBg.png')
            if(index == 0){
                rankBg = require('../../images/homeBG/numBg1.png')
            }else if(index == 1){
                rankBg = require('../../images/homeBG/numBg2.png')
            }else if(index == 2){
                rankBg = require('../../images/homeBG/numBg3.png')
            }
            return(
                <View key={'rank_'+index} style={styles.listItem}>
                    <View style={[styles.lineWrapper,{backgroundColor: Colors.themeBg}]}>
                        <View style={styles.numberWrapper}>
                            <ImageBackground style={[styles.rankNumImg,{alignItems:'center'}]} source={rankBg}>
                                <Text style={[styles.numberText,{color: Colors.white}]}>{index + 1}</Text>
                            </ImageBackground>
                        </View>
                        <View style={{flex: 1}}>
                            <View style={styles.rankNameWrapper}>
                                <Text style={{fontSize: 14, color: Colors.themeText}}>{value.name}</Text>
                                <Text style={{fontSize: 13, color: Colors.themeTextLight}}>{value.quantity}kWh</Text>
                            </View>
                            <Image style={{width:r,height:8,marginTop: 8}} source={lineImg}/>
                            {/* <View style={{width: r, height: 10, marginTop: 4,borderRadius: 5,backgroundColor: Colors.themeButton}}/> */}
                        </View>
                    </View>
                </View>
            )
        })

        return (
            <ScrollView
                style={{marginTop: 10}}
                scrollIndicatorInsets={{right: 1}}
                contentContainerStyle={{paddingBottom: BottomSafeMargin + 50}}
            >
                {rankList}
            </ScrollView>
        )
    }
	
	getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
   	showDoublePicker() {
        window.DoubulePicker.init({
            pickerData: this.state.pickerData,
            pickerConfirmBtnText: '确定  ',
            pickerCancelBtnText: '',
            pickerTitleText: '',
            pickerCancelBtnColor: [255, 255, 255, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: this.state.selectData,
            onPickerConfirm: (data,index) => {
				this.filterDetialData(data,index)
            },
            onPickerCancel: data => {
                this.setState({ modalVisible: false })
            },
            onPickerSelect: (data,index) => {
                
            }
        });
        window.DoubulePicker.show()
    }

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getHeaderSegment()}
                {this.renderDeadline()}
				{this.getCurrentVaule()}
                {this.renderIndicator()}
				{this.renderRank()}
				{this.getModal()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	segmentWrapper: {
		flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    segmentTouch: {
        height: 30,
        width: '100%',
        borderRadius: 15,
        justifyContent: 'center',
		alignItems: 'center'
	},
	modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
	},
	headerContain:{ 
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
        paddingHorizontal: 16
	},
	cardWrapper: {
        width: '48%',
        height: 100,
        borderRadius: 5,
        paddingVertical: 10,
		paddingHorizontal: 12,
		justifyContent: 'space-between'
	},
	electTips: {
        color: Colors.themeBGInactive,
        fontSize: 15
    },
    price: {
        fontSize: 30,
        fontWeight: 'bold',
        marginLeft: 5
    },
    unit: {
        fontSize: 15,
        marginLeft: 5,
        marginTop: 10
    },
    listItem:{
        flexDirection: 'row',
        width:'100%',
        paddingHorizontal: 16,
        marginTop: 10,
        alignItems:'center'
    },
    lineWrapper:{
        width:'100%',
        borderRadius:4,
        flexDirection:'row',
        alignItems:'center',
        paddingVertical: 8, 
        paddingRight: 10
    },
    siteTouch:{
        marginRight: 5,
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal:10,
        paddingVertical: 10
    },
    numberWrapper:{
        width: 50,
        alignItems:'center',
        justifyContent:'center'
    },
    numberText:{
        alignItems:'center',
        fontWeight:'bold',
        fontSize: 16,
        marginTop: 3
    },
    rankNameWrapper:{
        flexDirection:'row',
        justifyContent:'space-between',
        width:'100%',
        paddingRight: 10
    },
    rankNumImg:{
        width: 30,
        height: 30, 
        resizeMode:'contain'
    },
    deadLineText:{
        marginLeft: 18,
        fontSize: 13,
        marginTop: 5
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ElectricityRank)

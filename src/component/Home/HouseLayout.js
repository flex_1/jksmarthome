/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	Dimensions,
	SwipeableFlatList,
	Modal,
	ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet';
import ImageViewer from 'react-native-image-zoom-viewer';
import { postJson, postPicture } from '../../util/ApiRequest';
import { NetUrls,Colors } from '../../common/Constants';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import { BottomSafeMargin } from '../../util/ScreenUtil';
import {ListNoContent} from "../../common/CustomComponent/ListLoading";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import {ImagesLight, ImagesDark} from '../../common/Themes';

const screenW = Dimensions.get('window').width
const screenH = Dimensions.get('window').height
const homeCardW = (screenW - 16*2)
const homeCardH = homeCardW * 0.618

//侧滑最大距离
let maxSwipeDistance = 100
//侧滑按钮个数
let countSwiper = 1

class HouseLayout extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HeaderLeft navigation={navigation} backTitle={'房屋布线图(平面图)'}/>,
        headerBackground: <HeaderBackground/>
	})

	constructor(props) {
		super(props);
		const {getParam} = this.props.navigation
		this.houseId = getParam('id')

		this.state = {
			dataList: null,
			images:[],
			imageViewVisible: false,
			index: 0
		}
	}

	componentDidMount() {
		this.requestHouseLayout()
	}

	componentWillUnmount(){
		
	}

	//获取 房屋布线图
    async requestHouseLayout() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.houseLayout,
                params: {
                    id: this.houseId
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
				let imgs = data.result || []
				let images = []
				
				for (const val of imgs) {
					images.push({url: val.schematic})
				}

                this.setState({
					dataList: imgs,
					images: images
				})
            } else {
                ToastManager.show(data.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误');
        }
    }

	//添加 房间布线图
	async requestUpdateImage(imageData) {
		SpinnerManager.show()
		try {
			let data = await postPicture({
				url: NetUrls.houseSetting,
				filePath: imageData.path,
                timeout: 30,
				fileName: imageData.filename || 'hoomeBgImg.jpg',
				params: {
					id: this.houseId,
					type: 2, // 1 背景图 2 布线图
				}
			})
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.requestHouseLayout()
				ToastManager.show('添加成功')
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error));
		}
	}

	// 删除 布线图
	async deleteLayoutImage(rowData){
		SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.deleteDiagram,
                params: {
                    url: rowData.schematic
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.requestHouseLayout()
				ToastManager.show('删除成功')
            } else {
                ToastManager.show(data.msg);
            }
        } catch (e) {
            SpinnerManager.close()
            ToastManager.show('网络错误');
        }
	}

	getActionSheet() {
		return (
			<ActionSheet
				ref={e => this.actionSheet = e}
				title={'上传或拍照'}
				options={['从相册中选择', '拍一张', '取消']}
				cancelButtonIndex={2}
				onPress={(index) => {
					if (index == 0) {
						this.pickImage()
					} else if (index == 1) {
						this.pickImagewithCamera()
					}
				}}
			/>
		)
	}

	pickImage() {
		ImagePicker.openPicker({
			width: screenW,
			height: screenH,
			cropping: false,
			mediaType: 'photo',
			compressImageQuality: 1,
		}).then(image => {
			console.log(image);
			this.setState({
				homeBgThumb: { uri: image.path },
			})
			this.requestUpdateImage(image)
		}).catch(err => {
			console.log(err);
		})
	}

	pickImagewithCamera() {
		ImagePicker.openCamera({
			width: screenW,
			height: screenH,
			cropping: false,
			mediaType: 'photo',
			compressImageQuality: 1,
		}).then(image => {
			console.log(image);
			this.setState({
				homeBgThumb: { uri: image.path },
			})
			this.requestUpdateImage(image)
		}).catch(err => {
			console.log(err);
		})
	}

	//查看大图View
	getImageView(){
		return(
			<Modal 
				visible={this.state.imageViewVisible}
				style={{flex: 1,}}
				transparent={true} 
				onRequestClose={() => { }}
			>
				<ImageViewer
					ref={e => this.image_viewer = e}
					imageUrls={this.state.images}
					index={this.state.index}
					flipThreshold={120}
					saveToLocalByLongPress={false}
					enablePreload={true}
					onClick={()=>{
						this.setState({
							imageViewVisible: false
						})
					}}
					onChange={(index)=>{
						this.image_viewer.resetImageByIndex(this.state.index)
						this.state.index = index
					}}
					loadingRender={()=>{
						return(
							<ActivityIndicator size={'small'}/>
						)
					}}
					onCancel={()=>{
						this.setState({
							imageViewVisible: false
						})
					}}
					// renderHeader={()=>{
					// 	return(
					// 		<View style={{width:'100%',paddingTop:44,backgroundColor:'red'}}>
					// 		</View>
					// 	)
					// }}
				/>

            </Modal>
		)
	}

	//侧滑菜单渲染
    getQuickActions(rowData, index) {
		const Colors = this.props.themeInfo.colors
        return (
            <View style={styles.quickAContent}>
                <TouchableOpacity activeOpacity={0.7} onPress={() => {
					this.closeSwiper()
					this.deleteLayoutImage(rowData)
                }}>
                    <View style={[styles.quick, { backgroundColor: Colors.red }]}>
                        <Text style={{ color: Colors.white, fontSize: 14 }}>删除</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
	}
	
	//关闭侧滑栏
    closeSwiper() {
        this.layout_list_swiperFlatlist?.setState({ openRowKey: null })
	}

	_extraUniqueKey(item, index) {
		return "layout_list_index_" + index;
	}

	renderRowItem(rowData, index) {
        const Colors = this.props.themeInfo.colors
        const imgUrl = rowData.schematicThumb || rowData.schematic
        
		return(
			<View style={{paddingHorizontal: 16,marginTop:20,}}>
				<TouchableOpacity activeOpacity={1} style={{width:'100%',borderRadius:5,overflow:'hidden'}} onPress={()=>{
					this.setState({
						imageViewVisible: true,
						index: index
					})
				}}>
					<Image style={[styles.image, {backgroundColor:Colors.themeBg}]} source={{uri: imgUrl}}/>
				</TouchableOpacity>
			</View>
			
		)
	}

	getListView(){
		if(!this.state.dataList || this.state.dataList.length <= 0 ){
            const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
			return (
				<ListNoContent 
                    img={Images.noLayout}
                    text={'您还未上传房屋布线图'}
                />
			)
		}
		
		return(
			<SwipeableFlatList
				ref={e => this.layout_list_swiperFlatlist = e}
				contentContainerStyle={{ paddingBottom: BottomSafeMargin+80 }}
				scrollIndicatorInsets={{right: 1}}
				renderQuickActions={({ item, index }) => this.getQuickActions(item, index)}//创建侧滑菜单
				maxSwipeDistance={maxSwipeDistance}
				bounceFirstRowOnMount={false}
				data={this.state.dataList}
				keyExtractor={this._extraUniqueKey}
				renderItem={({ item, index }) => this.renderRowItem(item, index)}
			/>
		)
	}

	getAddButton(){
		const Colors = this.props.themeInfo.colors
		let updateText = '+继续上传图片  '
		if(!this.state.dataList || this.state.dataList.length <= 0){
			updateText = '+上传图片  '
		}

		return(
			<View style={[styles.bottomBtnWrapper, {backgroundColor: Colors.themeBaseBg}]}>
                <TouchableOpacity activeOpacity={0.7} style={[styles.addTouch, {backgroundColor: Colors.themeBg}]} onPress={() => {
					this.closeSwiper()
					this.actionSheet?.show()
                }}>
					<Text style={{ color: Colors.themeButton }}>{updateText}</Text>
                </TouchableOpacity>
            </View>
		)
	}
	
	render() {
		const Colors = this.props.themeInfo.colors
		return (
			<View style={{
				backgroundColor: Colors.themeBaseBg,
				flex: 1,
			}}>
				{this.getListView()}
				{this.getAddButton()}
				{this.getActionSheet()}
				{this.getImageView()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	image:{
		width:'100%',
		height:homeCardH,
		resizeMode:'contain',
	},
	//侧滑菜单的样式
    quickAContent: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginRight: '5%',
        marginTop: 20,
        overflow: 'hidden',
        borderTopRightRadius: 5,
		borderBottomRightRadius: 5,
		paddingVertical:1,
    },
    quick: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: maxSwipeDistance / countSwiper,
	},
	addTouch: {
        width: '100%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    bottomBtnWrapper: {
        position: 'absolute',
		left: 0,
		bottom: 0,
        paddingBottom: BottomSafeMargin+10,
        width: '100%',
        paddingHorizontal: 16,
		paddingTop: 20,
    },
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(HouseLayout)

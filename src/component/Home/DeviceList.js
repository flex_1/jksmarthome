/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	TextInput,
	DeviceEventEmitter,
	ImageBackground,
	Alert,
	FlatList,
	SwipeableFlatList,
	RefreshControl
} from 'react-native';
import { postJson } from '../../util/ApiRequest';
import { NetUrls,NotificationKeys,NetParams } from '../../common/Constants';
import { Colors } from '../../common/Constants';
import ToastManager from "../../common/CustomComponent/ToastManager";
import {ListNoContent,FooterEnd,ListLoading,ListError,FooterLoading } from "../../common/CustomComponent/ListLoading";
import {ImagesLight, ImagesDark} from '../../common/Themes';
import UpdateDeviceList from '../../util/DeviceUpdate';
import DeviceListItem from "../CommonPage/ListItems/DeviceItem";
import { BottomSafeMargin } from '../../util/ScreenUtil';

const screenW = Dimensions.get('window').width;
const roomBgW = screenW - 32; // 32 为两边间距
const roomBgH = roomBgW * 0.45;

//侧滑最大距离
const maxSwipeDistance = 120
//侧滑按钮个数
const countSwiper = 2

class DeviceList extends Component {

	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			isRefreshing: false,
			deviceList:null,
            totalPage: 0,
            currentPage: 1
		}
        this.initialBreakFn()
	}

	componentDidMount() {
		this.requestDeviceList()
		this.deviceStatusNoti = DeviceEventEmitter.addListener(NotificationKeys.kDeviceStatusNotification,(data)=>{
            if(data && data.isCollectChanged){
                return
            }else{
                this.requestDeviceList()
                this.props.refreshFilter && this.props.refreshFilter()
            }
        })
        
        //设备状态 websocket 变化通知
        this.websocketNoti = DeviceEventEmitter.addListener(NotificationKeys.kWebsocketDeviceNotification,(data)=>{
            if(!data) return
            this.updateDeviceStatus(data)
        })
	}

	componentWillUnmount(){
        this.deviceStatusNoti.remove()
        this.websocketNoti.remove()
	}

    // 用于打断网络的函数
    initialBreakFn(){
        break_fn = null
        this.break_promise = new Promise(function(resolve,reject){
            break_fn = ()=>{
                reject({ 'code': NetParams.networkBreak })
            }
        })
    }

    reloadDeviceList(params){
        break_fn && break_fn()
        this.initialBreakFn()

        this.closeSwiper()
		this.setState({
			loading: false,
			isRefreshing: false,
			deviceList: null,
            currentPage: 1
		},()=>{
			this.requestDeviceList(params)
		})
    }
	
	async requestDeviceList(params){
        params = params || {}
        
		if(this.props.params && this.props.params.typeId){
			params.typeid = this.props.params.typeId
		}
		if(this.props.params && this.props.params.roomId){
			params.roomid = this.props.params.roomId
		}
		if(this.props.params && this.props.params.deviceId){
			params.deviceId = this.props.params.deviceId
		}
		if(this.props.floor) {
			params.floor = this.props.floor
		}
		if(this.props.deviceName){
			params.deviceName = this.props.deviceName
        }
        // 自然分类
        if(this.props.naturalClassification && this.props.naturalClassification.length){
            params.naturalClassification = this.props.naturalClassification.toString()
        }
		this.setState({
			loading:true
		})
		try {
			let data = await postJson({
				url: NetUrls.deviceList,
                break_promise: this.break_promise,
				params: {
                    ...params,
                    showRobot: 1
                }
			});
			this.setState({loading: false,isRefreshing: false})
			if (data.code == 0) {
                let deviceList = []
                if(params.page && params.page > 1){
                    deviceList = this.state.deviceList.concat(data.result.list)
                }else{
                    deviceList = data.result.list || []
                }
				this.setState({
					deviceList: deviceList,
                    totalPage: data.result?.totalPageNum,
                    currentPage: data.result?.curPage
				})
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            if(error.code == NetParams.networkBreak){
                return
            }
			this.setState({loading: false,isRefreshing: false})
			ToastManager.show('网络错误')
		}
    }
    
    // 更新设备状态
    updateDeviceStatus(data){
        let deviceList = this.state.deviceList
        if(!deviceList || deviceList.length <= 0) return
        if(this.state.loading || this.state.isRefreshing) return
        if(this.updating) return

        this.updating = true
        let newDeviceList = UpdateDeviceList(deviceList, data)
        if(newDeviceList){
            this.setState({deviceList: newDeviceList})
        }
        this.updating = false
    }

	//关闭侧滑栏
	closeSwiper(){
		this.devices_swiperflatlist && this.devices_swiperflatlist.setState({openRowKey:null})
	}

    //加载下一页
    onLoadNextPage() {
        if(this.state.loading){
            return
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return
        }
        this.requestDeviceList({page: this.state.currentPage+1})
    }

	//侧滑菜单渲染
	getQuickActions(rowData, index) {
        if(this.props.numColumns != 1){
            return null
        }
		
		const {navigate} = this.props.navigation
		return (
			<View style={styles.quickAContent}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.quick}  
                    onPress={()=>{
					    this.closeSwiper()
					    let deviceData = {id: rowData.deviceId}
					    navigate('Setting_Timing',{
                            isJuniorUser: this.props.isJuniorUser,
                            deviceData: deviceData
                        })
				    }}
                >
					<Image style={{width:40,height:40}} source={require('../../images/deviceIcon/time.png')}/>
				</TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.7} 
                    style={styles.quick} 
                    onPress={()=>{
					    this.closeSwiper()
					    navigate('DeviceSetting',{deviceData:rowData,isFromManager: true})
				    }}
                >
					<Image style={{width:40,height:40}} source={require('../../images/deviceIcon/setting.png')}/>
				</TouchableOpacity>
			</View>
		)
	}

    //footer
    renderFooter() {
        if(!this.state.deviceList || this.state.deviceList.length <= 0){
            return null
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return <FooterEnd/>
        }
        return <FooterLoading/>
    }

	// 渲染设备的每一行
	renderRowItem(rowData, index) {
		return(
			<DeviceListItem
                numColumns={this.props.numColumns}
				rowData={rowData} 
				index={index} 
				navigation={this.props.navigation}
				closeSwiper={()=>{ this.closeSwiper() }}
				updateDeviceCallBack={(target,index)=>{
					// 设备 及时性
					this.state.deviceList[index] = rowData
                    this.setState({
                        ...this.state
                    })
                }}
                collectClick={(collect)=>{
                    this.state.deviceList[index].isCollect = collect
                    this.setState({
                        ...this.state
                    })
                    let params = {isCollectChanged: true}
                    DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification,params)
                }}
                isDark={this.props.isDark}
			/>
		)
    }

    //  显示添加设备按钮 还是 暂无设备文字
    renderSubContent(){
        if(this.props.isJuniorUser){
            return (
                <Text style={{fontSize: 14, color: Colors.themeTextLightGray, marginTop: 10}}>暂无设备</Text>
            )
        }else{
            return (
                <TouchableOpacity style={[styles.moreTouch,{marginTop:5}]} onPress={()=>{
                    const {navigate} = this.props.navigation
                    if(this.props.params && this.props.params.roomId){
                        navigate('RoomSelectDevice',{roomId: this.props.params.roomId})
                    }else{
                        navigate('AddDeviceQRCode')
                    }
                }}>
                    <Text style={{fontSize:14,color:Colors.themeTextLightGray}}>添加设备</Text>
                    <Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
                </TouchableOpacity>
            )
        }
    }
    
    renderNoContent(){
        if(this.props.isSearchList){
            // 如果是搜索结果
            return(
                <View style={{alignItems:'center',marginTop:50}}>
                    <Text style={{color:Colors.themeTextLightGray,fontSize:13}}>暂无搜索结果</Text>
                </View>
            )
        }else{
            const Images = this.props.isDark ? ImagesDark : ImagesLight
            return(
                <View style={{alignItems:'center'}}>
                    <ListNoContent   
                        style={{alignItems: 'center', marginTop: '8%'}}
                        img={Images.noDevice} 
                    />
                    {this.renderSubContent()}
                </View>
            )
        }
    }

	_devicesExtraUniqueKey(item, index){
		return "devices_index_" + index;
	}

	// 设备列表
	getListView(){
		if(this.state.loading && !this.state.deviceList){
			return <ListLoading/>
		}
		if(!this.state.deviceList){
            return <ListError onPress={()=>{ this.requestDeviceList() }}/>
        }
        
		return (
            <SwipeableFlatList
                numColumns={this.props.numColumns}
                key={this.props.numColumns}
                scrollEnabled={this.props.scrollable}
                ref={e => this.devices_swiperflatlist = e}
                contentContainerStyle={styles.contentStyle}
                scrollIndicatorInsets={{right: 1}}
                removeClippedSubviews={false}
                data={this.state.deviceList}
                keyExtractor={this._devicesExtraUniqueKey}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
                initialNumToRender={10}
                ListEmptyComponent = {this.renderNoContent()}
                renderQuickActions={({ item, index }) => this.getQuickActions(item,index)}//创建侧滑菜单
                maxSwipeDistance={maxSwipeDistance}
                bounceFirstRowOnMount={false}
                onEndReached={() => this.onLoadNextPage()}
                onEndReachedThreshold={0.1}
                ListFooterComponent={() => this.renderFooter()}
                refreshControl={
                    <RefreshControl
                        colors={[Colors.themeBGInactive]}
                        tintColor={Colors.themeBGInactive}
                        refreshing={this.state.isRefreshing}
                        onRefresh={() => {
                            if (this.state.isRefreshing) return
                            this.state.currentPage = 1
                            this.setState({
                                isRefreshing: true
                            })
                            this.requestDeviceList()
                        }}
                    />
                }
            />
        )
	}

	render() {
		return this.getListView()
	}
}

const styles = StyleSheet.create({
    contentStyle:{
        paddingBottom: BottomSafeMargin + 10
    },
	deviceIcon:{
		width:60,
		height:60,
		resizeMode:'contain'
	},
	switchTouch:{
		width:80,
		height:30,
		borderRadius:15,
		borderColor:Colors.borderLightGray,
		borderWidth:1,
		justifyContent:'center',
		alignItems:'center'
	},
	statusTextWrapper:{
		height:30,
		borderRadius:15,
		justifyContent:'center',
		alignItems:'center',
		marginRight:10,
		paddingHorizontal: 15,
	},
	icon:{
        width: 40, 
        height: 40, 
        marginLeft: 20, 
        resizeMode:'contain'
	},
	hiddenBtn:{
		marginTop:15,
		marginBottom:5,
		flexDirection: 'row',
		alignItems:'center',
        justifyContent:'center',
        width: '100%',
        height: 40
	},
	//侧滑菜单的样式
	quickAContent: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginRight: 16,
		marginTop: 12,
		overflow: 'hidden',
	},
	quick: {
		alignItems: 'center',
		justifyContent: 'center',
		width: maxSwipeDistance / countSwiper,
	},
	moreTouch:{
		borderColor:Colors.borderLightGray,
		borderWidth:1,
		width:120,
		height:30,
		borderRadius:15,
		justifyContent:'center',
		alignItems:'center',
		flexDirection:'row'
	},
	arrow:{
		width:6,
		height:11,
		marginLeft:10,
		resizeMode:'contain'
	}
});

export default DeviceList

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	TextInput,
	Keyboard,
	DeviceEventEmitter
} from 'react-native';
import { postJson } from '../../util/ApiRequest';
import { Colors, NetUrls,NotificationKeys  } from '../../common/Constants';
import HeaderLeftView from '../../common/CustomComponent/HeaderLeftView';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import { StatusBarHeight } from '../../util/ScreenUtil';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import { Alert } from 'react-native';

class AddDevice extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'手动添加'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text:'扫码添加',onPress:()=>{
                        let roomId = navigation.getParam('roomId')
                        let callBack = navigation.getParam('callBack')
					    navigation.replace('AddDeviceQRCode',{roomId:roomId,callBack:callBack})
                    }}
                ]}/>
            ),
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
	}

	constructor(props) {
		super(props);
		const {getParam} = this.props.navigation
		this.roomId = getParam('roomId')
        this.callBack = getParam('callBack')

		this.state = {
			serialNO: ''
		}
	}

	componentDidMount() {

	}

	// 检查 序列号
	async checkSerialNo(seriaNo) {
		let params = {}
		if(this.roomId){
			params.roomId = this.roomId
		}
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.checkSeriaNo,
				params: {
					...params,
					seriaNo: seriaNo
				}
			});
			SpinnerManager.close()
			if (data.code == 0) {
                let msg = data.result || data.msg
				this.nextStep(data.result, seriaNo, msg)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	nextStep(data, seriaNo, msg){
		const { popToTop,pop } = this.props.navigation

		// 直接添加网关（不需要进入 下一步）
        pop()
        this.callBack && this.callBack()
		DeviceEventEmitter.emit(NotificationKeys.kHouseInfoNotification)
        DeviceEventEmitter.emit(NotificationKeys.kDeviceStatusNotification)
        Alert.alert(
            '提示',
            msg,
            [ {text: '知道啦', onPress: () => { }, style: 'cancel'}]
        )
	}

	getInput() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.inputWrapper,{backgroundColor:Colors.themeBg}]}>
				<Text style={{ marginLeft: 20, fontSize: 15, color: Colors.themeText }}>网关/序列号</Text>
				<TextInput
                    autoFocus={true}
					style={[styles.input,{color: Colors.themeText}]}
					placeholder={'请输入网关或设备序列号'}
					placeholderTextColor={Colors.themeInactive}
					onChangeText={(text) => {
						this.state.serialNO = text
					}}
					returnKeyType={'done'}
				/>
			</View>
		)
	}

	getTips(){
		return(
			<Text style={{marginTop:20,fontSize:14,color:Colors.themeTipsYellow}}>注：确保您要添加的设备已连接网络</Text>
		)
	}

	getButton() {
        const Colors = this.props.themeInfo.colors

		return (
            <TouchableOpacity 
                style={[styles.btn,{backgroundColor:Colors.themeButton}]} 
                activeOpacity={0.7} 
                onPress={()=>{
				    Keyboard.dismiss()
                    let seriaNo = this.state.serialNO
				    if (!seriaNo) {
					    ToastManager.show('请输入网关或序列号')
					    return
				    }
                    if(seriaNo.includes('SNO')){
                        seriaNo = seriaNo.replace('SNO','')
                    }
                    if(seriaNo.includes('sno')){
                        seriaNo = seriaNo.replace('sno','')
                    }
                    if(seriaNo.includes('Sno')){
                        seriaNo = seriaNo.replace('Sno','')
                    }
                    seriaNo = seriaNo.replace('_','')
                    seriaNo = 'SNO_' + seriaNo
				    this.checkSerialNo(seriaNo)
			    }}
            >
				<Text style={{color:Colors.white,fontSize:15}}>确定</Text>
			</TouchableOpacity>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getInput()}
				{this.getTips()}
				{this.getButton()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
        flex: 1,
		paddingHorizontal:16
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 16,
	},
	navRText: {
		color: Colors.themeTextBlack,
		fontSize: 15
	},
	btn: {
		height:40,
		width:'100%',
		borderRadius:5,
		justifyContent:'center',
		alignItems:'center',
		marginTop:30
	},
	inputWrapper:{
		width: '100%',
		height: 55,
		borderRadius:5,
		alignItems:'center',
        flexDirection: 'row',
        marginTop: 20
	},
	input:{
		flex:1,
		height: 40,
		marginLeft:5,
		marginRight:10,
		textAlign: 'right',
		paddingRight: 10
	},
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(AddDevice)

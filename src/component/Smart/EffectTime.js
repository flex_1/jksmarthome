/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
	TextInput,
	DeviceEventEmitter
} from 'react-native';
import { Colors,NetUrls } from '../../common/Constants';
import ToastManager from '../../common/CustomComponent/ToastManager';
import DateUtil from '../../util/DateUtil';
import { postJson } from '../../util/ApiRequest';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
// import DatePicker from 'react-native-datepicker';
import DatePicker from '../../third/Datepicker/datepicker';
import SwitchButton from '../../common/CustomComponent/SwitchButton';

const screenW = Dimensions.get('window').width;
const indicatorW = screenW - 32; // 32 为两边间距
const OneDayStamp = 86400 * 1000

class EffectTime extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'生效日期'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text: '保存',onPress:()=>{
                        navigation.getParam('saveBtnClick')()
                    }}
                ]}/>
            ),
            headerBackground: <HeaderBackground/>
        }
    };

	constructor(props) {
		super(props);
		const {getParam,setParams} = this.props.navigation
		this.callBack = getParam('callBack')
        this.isShowEffectSwitch = getParam('isShowEffectSwitch')

		this.state = {
			days:[
				{id:0,name:'每',value:'0'},
				{id:1,name:'一',value:'2'},
				{id:2,name:'二',value:'3'},
				{id:3,name:'三',value:'4'},
				{id:4,name:'四',value:'5'},
				{id:5,name:'五',value:'6'},
				{id:6,name:'六',value:'7'},
				{id:7,name:'日',value:'1'},
			],
			
			beginDate: getParam('beginDate') || this.getDefaultDate(),
			endDate:getParam('endDate'),
			selects: ['0'],
            isEnabled: getParam('isEnabled')
        }
        
        setParams({
            saveBtnClick: ()=>{ this.save() }
        })
	}

	//处理默认的选中 周期
	getDefaultSelects(){
		if(!this.selects){
			return ['0']
		}
		let selectArr = []
		selectArr = this.selects.split(',')
		if(Array.isArray(selectArr)){
			console.log( selectArr );
			
			return selectArr
		}else{
			return []
		}
	}

	getDefaultDate(){
		let nowDate = Date.parse(new Date())
		return DateUtil(nowDate,'yyyy-MM-dd')
	}
 
	componentDidMount() {
		
	}

	componentWillUnmount(){
		
	}

	save(){
        if(this.isShowEffectSwitch && this.state.endDate){
            const nowDateStamp = Date.parse(new Date())
            const endDateStamp = Date.parse(new Date(this.state.endDate))
            const isExpired = nowDateStamp > (endDateStamp + OneDayStamp)
            if(isExpired){
                ToastManager.show('结束时间不能小于当前时间')
                return
            }
        }
		const {pop} = this.props.navigation
		pop()
		this.callBack && this.callBack(this.state.beginDate,this.state.endDate,this.state.isEnabled)
    }
    
    // 日期
	getDateItem(refName,date, callBack) {
        const Colors = this.props.themeInfo.colors

		return (
			<DatePicker
				ref={refName}
				style={{flex:1}}
				date={date}
				mode="date"
				format="YYYY-MM-DD"
				confirmBtnText="确定"
				cancelBtnText="取消"
				showIcon={false}
				androidMode={'spinner'}
				customStyles={{
					dateInput: {
						borderWidth: 0,
						alignItems:'flex-end',
						justifyContent:'center',
						paddingRight:15
					},
					dateText: {
						color: date ? Colors.themeText : Colors.themeBg,
						fontSize: 15,
						textAlign:'right'
					}
				}}
				onDateChange={(date) => {
					callBack(date)
				}}
			/>
		)
	}

	// 日期
	getDatePicker(){
        if(this.isShowEffectSwitch && !this.state.isEnabled){
            return null
        }
        const Colors = this.props.themeInfo.colors

		return(
			<View style={{paddingVertical:5,paddingHorizontal:16}}>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.inputWrapper,{backgroundColor:Colors.themeBg}]} 
                    onPress={()=>{
					    this.refs.date_picker_begin && this.refs.date_picker_begin.onPressDate()
				    }}
                >
					<Text style={{color:Colors.themeText,fontSize:15}}>开始日期</Text>
					{this.getDateItem('date_picker_begin',this.state.beginDate,(date)=>{
						if(!this.state.endDate){
							this.setState({
								beginDate: date,
							})
							return
						}
						let beginTimeStamp = new Date(date)
						let endTimeStamp = new Date(this.state.endDate)
						let diff = endTimeStamp.getTime() - beginTimeStamp.getTime()

						if(diff < 0 && (date != this.state.endDate ) ){
							ToastManager.show('开始时间不能大于结束时间')
							return
						}
						this.setState({
							beginDate: date,
						})
					})}
					<Image style={styles.rightArrow} source={require('../../images/enter_light.png')} />
				</TouchableOpacity>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.inputWrapper,{backgroundColor:Colors.themeBg}]} 
                    onPress={()=>{
					    this.refs.date_picker_end && this.refs.date_picker_end.onPressDate()
				    }}
                >
					<Text style={{color:Colors.themeText,fontSize:15}}>结束日期</Text>
					{this.getDateItem('date_picker_end',this.state.endDate,(date)=>{
						if(!this.state.beginDate){
							this.setState({
								endDate: date,
							})
							return
						}
						let beginTimeStamp = new Date(this.state.beginDate )
						let endTimeStamp = new Date(date)
						let diff = endTimeStamp.getTime() - beginTimeStamp.getTime()
						
						if(diff < 0 && (date != this.state.endDate ) ){
							ToastManager.show('结束时间不能小于开始时间')
							return
						}
						this.setState({
							endDate: date,
						})
					})}
					<Image style={styles.rightArrow} source={require('../../images/enter_light.png')} />
				</TouchableOpacity>
			</View>
		)
	}

    renderEffectSwicth(){
        if(!this.isShowEffectSwitch){
            return null
        }
        const Colors = this.props.themeInfo.colors

        return(
            <View style={[styles.effectWrapper,{backgroundColor:Colors.themeBg,paddingRight: 0}]}>
                <Text style={{color:Colors.themeText,fontSize:15}}>是否生效</Text>
                <View style={{flex: 1}}/>
                <SwitchButton
                    value = {this.state.isEnabled}
                    onPress = {()=>{
                        this.setState({
                            isEnabled: !this.state.isEnabled
                        })
                    }}
                />
            </View>
        )
    }

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<ScrollView style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.renderEffectSwicth()}
				{this.getDatePicker()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex:1,
	},
	inputWrapper:{
		width: '100%',
		height: 50,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:30,
		paddingHorizontal: 16
	},
    rightArrow:{ 
        marginLeft: 10, 
        width: 7, 
        height: 13, 
        marginRight: 10,
        resizeMode:'contain' 
    },
    effectWrapper:{
        marginHorizontal: 16,
        height: 50,
        borderRadius:5,
        alignItems:'center',
		flexDirection: 'row',
		marginTop:30,
		paddingHorizontal: 16
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(EffectTime)

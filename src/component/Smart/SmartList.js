/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    FlatList,
    Alert,
    RefreshControl,
    SwipeableFlatList
} from 'react-native';
import { Colors, NotificationKeys, NetUrls } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import {ListNoContent,ListLoading,ListError } from "../../common/CustomComponent/ListLoading";
import SmartListItem from "../CommonPage/ListItems/SmartListItem";
import { connect } from 'react-redux';
import {ImagesLight, ImagesDark} from '../../common/Themes';

//侧滑最大距离
const maxSwipeDistance = 180
//侧滑按钮个数
const countSwiper = 3

class SmartList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            samrtList: null,
            loading: true
        }
    }

    componentDidMount() {
        this.requestSmartList()
        this.smartListNoti = DeviceEventEmitter.addListener(NotificationKeys.kSmartListChangeNotification,()=>{
            this.requestSmartList()
        })
    }

    componentWillUnmount() {
        this.smartListNoti.remove()
    }

    // 获取所有智能列表
    async requestSmartList() {
        let params = {}
        if(this.props.roomId){
            params.roomId = this.props.roomId
        }
        try {
            let data = await postJson({
                url: NetUrls.smartList,
                params: {
                    ...params
                }
            });
            this.setState({
                loading: false,
                isRefreshing: false
            })
            if (data.code == 0) {
                this.setState({
                    samrtList: data.result || []
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({
                loading: false,
                isRefreshing: false
            })
            ToastManager.show('网络错误')
        }
    }

    // 复制智能
	async copySmart(id) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.copyIntelligent,
				params: {
					intelligentId: id
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                this.requestSmartList()
                DeviceEventEmitter.emit(NotificationKeys.kSmartListChangeNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误');
		}
	}

    // 删除智能
	async deleteSmart(id) {
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.smartDelete,
				params: {
					intelligentId: id
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
                this.requestSmartList()
                DeviceEventEmitter.emit(NotificationKeys.kSmartListChangeNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show('网络错误');
		}
    }
    
    async collect(id,collect){
        try {
			let data = await postJson({
				url: NetUrls.collect,
				params: {
                    ids: id,
                    type: 3,
                    isAdd: collect
                }
            });
			if (data.code == 0) {
                DeviceEventEmitter.emit(NotificationKeys.kSmartListChangeNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    //头部向导
    getHeaderView() {
        let models = this.state.models.map((val, index) => {
            let dot = null
            let activeText = {}

            if (this.state.currentModel == val.type) {
                dot = <View style={styles.dot} />
                activeText = { color: Colors.themeTextBlack, fontSize: 18 }
            }
            return (
                <TouchableOpacity key={index} activeOpacity={0.7} style={styles.floorTouch} onPress={() => {
                    this.setState({
                        currentModel: val.type,
                    })
                }}>
                    <Text style={[styles.floorText, activeText]}>{val.name}</Text>
                    {dot}
                </TouchableOpacity>
            )
        })

        return (
            <View style={styles.sectionHead}>
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    bounces={false}
                    style={styles.headScroll}
                    contentContainerStyle={styles.headScrollContainer}
                >
                    {models}
                </ScrollView>
            </View>
        )
    }

    _extraUniqueKey(item, index) {
		return "smart_list_index_" + index;
    }
    
    renderRowItem(rowData, index){
        return(
            <SmartListItem 
                rowData={rowData} 
                index={index} 
                navigation={this.props.navigation}
                closeSwiper={()=>{this.closeSwiper()}}
                callBack={()=>{
                    this.closeSwiper()
                }}
                isDark={this.props.isDark}
            />
        )
    }

    //关闭侧滑栏
	closeSwiper() {
		this.smartFlatList && this.smartFlatList.setState({ openRowKey: null })
	}

    //侧滑菜单渲染
	getQuickActions(rowData, index) {
        let collectIcon = rowData.isCollect ? require('../../images/deviceIcon/collected.png') :
            require('../../images/deviceIcon/collect.png')
        
		return (
			<View style={styles.quickAContent}>
                <TouchableOpacity
                    style={styles.quick}
                    activeOpacity={0.7} 
                    onPress={() => {
                        this.collect( rowData.intelligentId, !rowData.isCollect)
				    }}
                >
					<Image style={{width:40,height:40}} source={collectIcon}/>
				</TouchableOpacity>
                <TouchableOpacity
                    style={styles.quick}
                    activeOpacity={0.7} 
                    onPress={() => {
					    this.closeSwiper()
                        Alert.alert(
                            '提示',
                            '确认删除该智能?',
                            [
                                { text: '取消', onPress: () => { }, style: 'cancel' },
                                { text: '删除', onPress: () => { this.deleteSmart(rowData.intelligentId) } },
                            ]
                        )
				    }}
                >
					<Image style={{width:40,height:40}} source={require('../../images/deviceIcon/delete.png')}/>
				</TouchableOpacity>
                <TouchableOpacity
                    style={styles.quick}
                    activeOpacity={0.7} 
                    onPress={() => {
                        this.closeSwiper()
					    this.copySmart(rowData.intelligentId)
				    }}
                >
					<Image style={{width:40,height:40}} source={require('../../images/deviceIcon/copy.png')}/>
				</TouchableOpacity>
			</View>
		)
    }

    // 显示添加设备按钮 还是 暂无文字
    renderSubContent(){
        if(this.props.isJuniorUser){
            return (
                <Text style={{fontSize: 14, color: Colors.themeTextLightGray, marginTop: 10}}>暂无智能</Text>
            )
        }else{
            return (
                <TouchableOpacity style={[styles.moreTouch,{marginTop:5}]} onPress={()=>{
					const {navigate} = this.props.navigation
					navigate('AddSmart',{title:'新增智能', roomId:this.props.roomId})
				}}>
					<Text style={{fontSize:14,color:Colors.themeTextLightGray}}>自定义智能</Text>
					<Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
				</TouchableOpacity>
            )
        }
    }
    
    renderNoContent(){
        const Images = this.props.isDark ? ImagesDark : ImagesLight
        return(
            <View style={{alignItems:'center'}}>
                <ListNoContent
                    style={{ marginTop: '10%'}}
                    img={Images.noSmart} 
                />
				{this.renderSubContent()}
			</View>
        )
    }

    getSmartListView() {
        let list = this.state.samrtList

        //正在加载
		if (this.state.loading && !list) {
			return <ListLoading/>
		}
		// 网络错误
		if (!list) {
            return <ListError onPress={()=>{ this.requestSmartList() }}/>
        }
        
        return (
            <SwipeableFlatList
                ref={e => this.smartFlatList = e}
                contentContainerStyle={{ paddingBottom: 50 }}
                scrollIndicatorInsets={{ right: 1 }}
                removeClippedSubviews={false}
                data={list}
                keyExtractor={this._extraUniqueKey}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
                renderQuickActions={({ item, index }) => this.getQuickActions(item, index)}//创建侧滑菜单
                maxSwipeDistance={maxSwipeDistance}
                bounceFirstRowOnMount={false}
                initialNumToRender={10}
                ListEmptyComponent={this.renderNoContent()}
                refreshControl={
                    <RefreshControl
                        colors={[Colors.themeBGInactive]}
                        tintColor={Colors.themeBGInactive}
                        refreshing={this.state.isRefreshing}
                        onRefresh={() => {
                            this.requestSmartList()
                        }}
                    />
                }
            />
        )
    }

    render() {
        return this.getSmartListView()
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        flex: 1
    },
    navTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        marginLeft: 15,
        textAlignVertical: 'center',
        color: Colors.themeTextBlack,
        flex: 1
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 16,
        marginLeft: 10
    },
    navText: {
        color: Colors.themeBG,
        fontSize: 15
    },
    //侧滑菜单的样式
	quickAContent: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginRight: 16,
		marginTop: 10,
		overflow: 'hidden',
	},
	quick: {
		alignItems: 'center',
		justifyContent: 'center',
		width: maxSwipeDistance / countSwiper,
	},
    moreTouch:{
		borderColor:Colors.borderLightGray,
		borderWidth:1,
		width:120,
		height:30,
		borderRadius:15,
		justifyContent:'center',
		alignItems:'center',
		flexDirection:'row'
	},
	arrow:{
		width:6,
		height:11,
		marginLeft:10,
		resizeMode:'contain'
	}
})

export default SmartList
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    FlatList,
    Alert,
    Modal
} from 'react-native';
import { Colors, NotificationKeys, NetUrls } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import {ListNoContent} from "../../common/CustomComponent/ListLoading";
import SmartExeListItem from "../CommonPage/ListItems/SmartExeListItem";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import { connect } from 'react-redux';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import {ImagesLight, ImagesDark} from '../../common/Themes';

class ExecuteSmart extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'选择智能'}/>,
            headerBackground: <HeaderBackground/>
        }
    };

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation
        this.callBack = getParam('callBack')
        this.state = {
            type:getParam('type'),
            intelligentId: getParam('intelligentId') || null,
            modalVisible: false,
            currentModel: 1,
            refreshing: false,
            samrtList: null,
            loading: true,
            result: null,
            data: null,
            selectData: [],
            smartName:null,
            smartIcon:null,
        }
    }

    componentDidMount() {
        this.requestSmartList()
        this.getDictList();
    }

    componentWillUnmount() {
    }

    // 获取所有智能列表
    async requestSmartList() {
        try {
            let data = await postJson({
                url: NetUrls.smartExecuteList,
                params: {
                    type:this.state.type,
                    intelligentId:this.state.intelligentId
                }
            });
            this.setState({
                loading: false,
                isRefreshing: false
            })
            if (data.code == 0) {
                this.setState({
                    samrtList: data.result || []
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({
                loading: false,
                isRefreshing: false
            })
            ToastManager.show('网络错误')
        }
    }

    // 获取字典列表
    async getDictList() {
            try {
            let data = await postJson({
                url: NetUrls.getDelayDictionary,
                params: {
                    type:this.state.type
                }
            });
            if (data.code == 0) {
                if (data.result && data.result.delayList
                    && data.result.delayList.length > 0
                    && data.result.resultList && data.result.resultList.length > 0 ) {
                    
                    let list = data.result.delayList;
                    let dlist = data.result.resultList;

                    let s_title = []
                    let s_value = []
                    for (const item of list) {
                        s_title.push(item.text)
                    }
                    for (const item of dlist) {
                        s_value.push(item.text)
                    }
                    this.setState({
                        data: [s_title,s_value],
                        result: data.result,
                    })
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    _extraUniqueKey(item, index) {
        return "smart_list_index_" + index;
    }

    renderRowItem(rowData, index){
        return(
            <SmartExeListItem
                rowData={rowData}
                index={index}
                navigation={this.props.navigation}
                isDark={this.props.themeInfo.isDark}
                callBack={()=>{
                    this.setState({
                        modalVisible: true,
                        intelligentId: rowData.intelligentId,
                        smartName: rowData.intelligentName,
                        smartIcon: rowData.conditionIcon,
                        nightIcon: rowData.nightConditionIcon
                    })
                }}
            />
        )
    }

    getModal() {
        if(!this.state.data){
            return null
        }
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}>
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        window.DoubulePicker.init({
            pickerData: this.state.data,
            pickerConfirmBtnText: '确定',
            pickerCancelBtnText: '取消',
            pickerTitleText: '停启用智能',
            pickerCancelBtnColor: [38, 80, 215, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: this.state.selectData,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false,
                    selectData: data
                })
                console.log("selectData===="+data);
                this.handlePickerSelectData(data,index)
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })

            },
            onPickerSelect: data => {

            }
        });
        window.DoubulePicker.show()
    }

    // Picker 确定按钮 点击处理
    handlePickerSelectData(selectData,selectIndex){
        const {pop} = this.props.navigation

        let firstIndex = selectIndex[0]
        let selectValue = selectIndex[1]
        let delay = this.state.result.delayList[selectIndex[0]].id

        let data = {}
        data.executeIntelligentId = this.state.intelligentId //执行智能id
        data.executeJson = selectValue // type==2 传0/1 
        data.executeType = this.state.type //执行类型 ，1-通知 2-智能 3-场景 4-设备
        data.delay = delay //延迟时间
        data.name = this.state.smartName
        data.icon = this.state.smartIcon
        data.dayIcon = this.state.smartIcon
        data.nightIcon = this.state.nightIcon
        data.conditionTitle = selectData[1]
        data.conditionValue = selectData[0]
        this.callBack && this.callBack(data)
        this.setState({
            modalVisible: false
        },()=>{
            pop(2)
        })
    }


    getSmartListView() {
        let list = this.state.samrtList

        //正在加载
        if (this.state.loading && !list) {
            return (
                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                    <Text style={{ color: Colors.themeTextLightGray }}>正在加载...</Text>
                </View>
            )
        }
        // 网络错误
        if (!list) {
            return (
                <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                    <TouchableOpacity
                        onPress={() => {
                            this.requestSmartList()
                        }}
                    >
                        <Text style={{ color: Colors.tabActiveColor, fontSize: 15 }}>网络错误,刷新重试</Text>
                    </TouchableOpacity>
                </View>
            )
        }
        // 暂无智能
        if (list && list.length <= 0) {
            const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
            return (
                <ListNoContent img={Images.noSmart} text={'您还未添加智能'}/>
            )
        }

        return (
            <FlatList
                style={{flex: 1}}
                contentContainerStyle={{ paddingBottom: 50 }}
                scrollIndicatorInsets={{ right: 1 }}
                removeClippedSubviews={false}
                data={list}
                keyExtractor={this._extraUniqueKey}
                renderItem={({ item, index }) => this.renderRowItem(item, index)}
            />
        )
    }

    // 获取 智能详情
    getSmartContent() {

        if (this.state.currentModel != 3) {
            return this.getSmartListView()
        } else {
            return null
        }
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                {this.getSmartListView()}
                {this.getModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        marginLeft: 15,
        textAlignVertical: 'center',
        color: Colors.themeTextBlack,
        flex: 1
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 16,
        paddingLeft: 10
    },
    navText: {
        color: Colors.themeBG,
        fontSize: 15
    },
    modalTouchable:{
        width: '100%',
        height: '100%',
        backgroundColor: Colors.translucent
    },
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ExecuteSmart) 

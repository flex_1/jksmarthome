/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    FlatList,
    Alert,
    Modal,
    Dimensions
} from 'react-native';
import { Colors, NetUrls } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import {ImagesLight, ImagesDark} from '../../common/Themes';
import {ListNoContent,FooterEnd,ListLoading,ListError,FooterLoading } from "../../common/CustomComponent/ListLoading";
import FilterModal from '../CommonPage/FilterModal';

const screenW = Dimensions.get('window').width;

class ExecuteDevice extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'选择设备'}/>,
            headerBackground: <HeaderBackground/>
		}
	}

    constructor(props) {
        super(props);
        const { getParam,setParams } = props.navigation;
        this.callBack = getParam('callBack')
        this.intelligentId = getParam('intelligentId') // 添加依赖设备 需要的参数
        this.filterType = getParam('filterType')  // 0-所有列表 1-首页收藏 2-智能执行  3-设备依赖
        this.floorType = getParam('floorType')

        this.state = {
            loading: true,
            type: getParam('type'),//1--通知，2--停启用智能，3--场景，4--设备 10--依赖
            modalVisible: false,
            data: null,
            pickerTitle: '',
            filterData: null,
            dictData:[],
            selectData: [],
            deviceList: null,
            totalPage: 0,
            currentPage: 1,

            deviceId:null,
            smartName:null,
            smartIcon:null,
            floor:null,
            roomName:null,

            filterVisible: false,
            filters: null,
            filterSelected: [],
            roomId: getParam('roomId'),

            floorList: [],
            selectFloorId: 0,
            selectRoomId: 0
        }
    }

    componentDidMount() {
        this.requestFloorList()
    }

    componentWillUnmount() {
        
    }

    async requestFloorList() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.floorAndRoomList,
                params: {
                    type: this.floorType   //9.智能执行设备   10.设备依赖
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.setState({
                    floorList: data.result || []
                })
                this.getDeviceList(1)
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

    // 获取设备列表
    async getDeviceList(page) {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.smartExecuteDevice,
                params: {
                    type: this.state.type,
                    intelligentId: this.intelligentId,
                    page: page,
                    roomId: this.state.selectRoomId,
                    floor: this.state.selectFloorId
                }
            });
            SpinnerManager.close()
            this.setState({loading: false})
            if (data.code == 0) {
                let deviceList = []
                if(page > 1){
                    deviceList = this.state.deviceList.concat(data.result.deviceOutList)
                }else{
                    deviceList = data.result?.deviceOutList || []
                }
                this.setState({
                    deviceList: deviceList,
                    totalPage: data.result?.totalPageNum,
                    currentPage: data.result?.curPage 
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            this.setState({loading: false})
            ToastManager.show('网络错误')
        }
    }

    // 获取字典列表
    async getDictList(item) {
        try {
            let data = await postJson({
                url: NetUrls.getDelayDictionary,
                params: {
                    type: this.state.type
                }
            });
            if (data.code == 0) {
                if (data.result && data.result.delayList
                    && data.result.delayList.length > 0) {
                    let list = data.result.delayList;
                    this.setState({
                        result:data.result,
                        deviceId:item.deviceId,
                        smartName:item.deviceName,
                        smartIcon:item.deviceIcon,
                        dayIcon:item.dayIcon,
                        nightIcon:item.nightIcon,
                        floor:item.floor,
                        floorText:item.floorText,
                        roomName:item.roomName
                    })
                    this.getConditionList(item.deviceId,item.deviceName,item.deviceIcon,list);
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    /**
     * 根据设备id获取智能执行条件
     **/
    async getConditionList(deviceId,deviceName,deviceIcon,list){
        SpinnerManager.show();
        try {
            let data = await postJson({
                url: NetUrls.getExecuteByDeviceId,
                params: {
                    deviceId:deviceId
                }
            });
            SpinnerManager.close();
            if (data.code == 0) {
                if (data.result && data.result.length>0) {
                    this.setState({
                        childData:data.result,
                        childValues:data.result[0].value
                    })
                    this.dealAndJump(data.result,deviceId,deviceName,deviceIcon,list);
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close();
            ToastManager.show('网络错误')
        }
    }

    //响应或者跳转
    dealAndJump(data,deviceId,deviceName,deviceIcon,timeList){
        const {navigate} = this.props.navigation

        if(data.length <= 1){
            let dlist = data[0].value;
            let s_title = []
            let s_value = []
            for (const item of timeList) {
                s_title.push(item.text)
            }
            for (const item of dlist) {
                s_value.push(item.text)
            }
            this.setState({
                data: [s_title,s_value],
                modalVisible: true,
                pickerTitle: deviceName
            })
        }else{
            navigate('ExecuteDeviceList',{
                title: deviceName,
                deviceId: deviceId,
                deviceIcon: deviceIcon,
                type: this.state.type,
                list: data,
                timeList: timeList,
                floor : this.state.floor,
                floorText: this.state.floorText,
                roomName : this.state.roomName,
                dayIcon: this.state.dayIcon,
                nightIcon: this.state.nightIcon,
                callBack: (data)=>{ this.callBack(data) }
            })
        }
    }

    // Picker 确定按钮 点击处理
    handlePickerSelectData(selectData,selectIndex){
        const {pop} = this.props.navigation
        let result = this.state.childData[0] || {}
        let executeKey = result.executeKey ||  'status'
        let execute = {}
        let selectValue = this.state.childValues[selectIndex[1]].id
        let delay = this.state.result.delayList[selectIndex[0]].id
        execute[executeKey] = selectValue + ',' + delay

        let data = {}
        data.executeJson = JSON.stringify(execute)
        data.deviceId = this.state.deviceId //执行智能id
        data.executeType = this.state.type //执行类型 ，1-通知 2-智能 3-场景 4-设备
        //data.delay = delay //延迟时间
        data.name = this.state.smartName
        data.icon = this.state.smartIcon
        data.dayIcon = this.state.dayIcon
        data.nightIcon = this.state.nightIcon
        data.conditionTitle = selectData[1]
        data.conditionValue = selectData[0]
        data.floor = this.state.floor
        data.floorText = this.state.floorText
        data.roomName = this.state.roomName
        
        this.callBack && this.callBack(data)
        this.setState({
            modalVisible: false
        }, () => {
            pop(2)
        })
    }

    // 获取楼层及房间id
    getParams(param){
        let p = [];
        let {filterResult} = this.state;
        if(filterResult && filterResult.length>0){
            let param1 = filterResult[param[0]].floor;
            p.push(param1);
            if(filterResult[param[0]].room && filterResult[param[0]].room.length>0){
                let param2 = filterResult[param[0]].room[param[1]].roomId;
                p.push(param2);
            }
        }
        return p;
    }

    //加载下一页
    onLoadNextPage() {
        if(this.state.loading){
            return
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return
        }
        this.getDeviceList(this.state.currentPage+1)
    }

    getModal() {
        if(!this.state.data){
            return null
        }
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        window.DoubulePicker.init({
            pickerData: this.state.data,
            pickerConfirmBtnText: '确定',
            pickerCancelBtnText: '取消',
            pickerTitleText: this.state.pickerTitle,
            pickerCancelBtnColor: [38, 80, 215, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: this.state.selectData,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false,
                    selectData: data
                })
                this.handlePickerSelectData(data,index)
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })
            },
            onPickerSelect: data => {

            }
        });
        window.DoubulePicker.show()
    }
    
    renderFooter() {
        if(!this.state.deviceList || this.state.deviceList.length <= 0){
            return null
        }
        if (this.state.currentPage >= this.state.totalPage) {
            return null
        }
        return <FooterLoading/>
    }

    renderRowItem(rowData,index) {
        const Colors = this.props.themeInfo.colors
        const icon = this.props.themeInfo.isDark ? rowData.nightIcon : rowData.dayIcon
        const pLeft = index % 2 == 0 ? '6%' : '3%'
        const pRight = index % 2 == 0 ? '3%' : '6%'

        let room = null
        if(rowData.floorText && rowData.roomName){
            room = <Text style={[styles.roomText,{color:Colors.themeTextLight}]}>{rowData.floorText + '  ' + rowData.roomName}</Text>
        }

        return(
            <View style={{width: '50%',marginTop: 15,paddingLeft: pLeft,paddingRight: pRight}}>
                <TouchableOpacity activeOpacity={0.7} style={styles.itemTouch} onPress={()=>{
                    if(this.state.type == 10){
                        // 添加设备依赖
                        this.callBack && this.callBack(rowData.deviceId)
                    }else{
                        this.getDictList(rowData);
                    }
                }}>
                    <Image style={styles.itemImg} source={{uri: icon}}/>
                    <Text style={[styles.itemText,{color:Colors.themeText}]}>{rowData.deviceName}</Text>
                    {room}
                </TouchableOpacity>
            </View>
        )
    }

    renderDeviceList(){
        let list = this.state.deviceList;

        if(!list && this.state.loading){
            return <ListLoading/>
        }
        if(!list && !this.state.loading){
            return <ListError onPress={()=>{ this.getDeviceList() }}/>
        }
        if (list.length <= 0) {
            const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
            const tips = this.state.roomId ? '该房间暂无设备\r\n您可以点击筛选来选择其他区域设备' : '暂无设备'
            return (
                <ListNoContent img={Images.noDevice} text={tips}/>
            )
        }
        return(
            <FlatList
                contentContainerStyle={{ paddingBottom: 50 }}
                data={list}
                keyExtractor={(item, index)=> "condition_" + index}
                renderItem={({item,index}) => this.renderRowItem(item,index)}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator = {false}
                onEndReached={() => this.onLoadNextPage()}
                onEndReachedThreshold={0.1}
                ListFooterComponent={() => this.renderFooter()}
                numColumns={2}
            />
        )
    }

    _renderRooms(roomList, btnBg){
        if(!roomList){
            return null
        }
        const Colors = this.props.themeInfo.colors
        return(
            roomList.map((value, index)=>{
                const rSelect = value.roomId == this.state.selectRoomId
                const bg = rSelect ? '#ECF6FF' : btnBg
                const color = rSelect ? Colors.newBtnBlueBg : Colors.themeTextLight 
                
                return(
                    <TouchableOpacity key={index} style={[styles.roomTouch,{backgroundColor: bg}]} onPress={()=>{
                        if(rSelect){
                            return
                        }
                        this.setState({
                            selectRoomId: value.roomId,
                            deviceList: null,
                            loading: true,
                            totalPage: 0,
                            currentPage: 1
                        },()=>{
                            this.getDeviceList(0)
                        })
                    }}>
                        <Text numberOfLines={2} style={[styles.roomName,{color: color}]}>{value.roomName}</Text>
                    </TouchableOpacity>
                )
            })
        )
    }

    renderRoomRowItem(rowData, index){
        const Colors = this.props.themeInfo.colors

        const fSelect = rowData.floor == this.state.selectFloorId
        const leftIcon = fSelect ?  <View style={styles.leftIcon}/> : null
        const textColor = fSelect ? Colors.newBtnBlueBg : Colors.themeText
        const btnBg = fSelect ?  Colors.themeBg : Colors.themeBaseBg
        const upArrow = fSelect ? <Image style={styles.upArrow} source={require('../../images/arrowUp.png')}/> : null
        
        return(
            <View>
                <TouchableOpacity style={[styles.floorTouch,{backgroundColor:btnBg}]} onPress={()=>{
                    if(fSelect){
                        return
                    }
                    this.setState({
                        selectFloorId: rowData.floor,
                        selectRoomId: 0,
                        deviceList: null,
                        loading: true,
                        totalPage: 0,
                        currentPage: 1
                    },()=>{
                        this.getDeviceList(0)
                    })
                }}>
                    <Text numberOfLines={2} style={[styles.floorName,{color: textColor}]}>{rowData.floorText}</Text>
                    {upArrow}
                    {leftIcon}
                </TouchableOpacity>
                {fSelect ? this._renderRooms(rowData.room, btnBg) : null}
            </View>
        )
    }

    rednerLeftRoomList(){
        return(
            <FlatList
				contentContainerStyle={{paddingBottom:50}}
				data={this.state.floorList}
				scrollIndicatorInsets={{ right: 1 }}
                removeClippedSubviews={false}
                keyExtractor={(item,index) => 'room_'+index}
                renderItem={({item,index}) => this.renderRoomRowItem(item,index)}
            />
        )
    }

    renderMainView(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={{flexDirection: 'row',flex: 1}}>
                <View style={{flex: 3}}>
                    {this.rednerLeftRoomList()}
                </View>
                <View style={{flex: 7,backgroundColor: Colors.themeBg}}>
                    {this.renderDeviceList()}
                </View>
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]} >
                {this.renderMainView()}
                {this.getModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        flex: 1
    },
    modalTouchable:{
        width: '100%',
        height: '100%',
        backgroundColor: Colors.translucent
    },
    itemTouch:{
        width: '100%',
        borderWidth: 0.5,
        borderColor: '#C8C9CC',
        borderRadius:9,
        height: screenW * 0.7 * 0.41,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5
    },
    itemImg:{
        width: screenW * 0.7 * 0.41 * 0.35,
        height: screenW * 0.7 * 0.41 * 0.35,
        resizeMode: 'contain'
    },
    itemText:{
        fontSize: 13,
        fontWeight: '400',
        marginTop: 5,
        textAlign: 'center'
    },
    roomText:{
        fontSize:12,
        marginTop: 5,
        textAlign: 'center'
    },
    leftIcon:{
        position: 'absolute',
        left: 0,
        top: 0,
        bottom:0, 
        width: 4,
        backgroundColor: Colors.newBtnBlueBg
    },
    upArrow:{
        width:10,
        height:10,
        marginLeft: 5
    },
    floorTouch:{
        height: 50,
        justifyContent: 'center',
        alignItems:'center',
        paddingHorizontal: 5,
        flexDirection:'row',
    },
    floorName:{
        fontSize:14,
        fontWeight: '500',
        textAlign:'center',
        flex:1
    },
    roomTouch:{
        height: 42,
        justifyContent: 'center',
        alignItems:'center',
        paddingHorizontal:5,
    },
    roomName:{
        fontSize: 14,
        textAlign:'center'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ExecuteDevice)

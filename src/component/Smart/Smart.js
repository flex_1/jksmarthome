/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    Alert
} from 'react-native';
import { Colors, NotificationKeys, NetUrls } from '../../common/Constants';
import SmartList from "./SmartList";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import {HSTips} from '../../common/Strings';

class Smart extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} title={'智能'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {icon:require('../../images/recommend.png'),onPress:()=>{
                        navigation.getParam('addBtn')(1)
                    }},
                    {icon:require('../../images/log.png'),onPress:()=>{
                        navigation.navigate('Logs',{title:'智能日志',type:'smart'})
                    }},
                    {icon:require('../../images/add.png'),onPress:()=>{
                        navigation.getParam('addBtn')(3)
                    }},
                ]}/>
            ),
            headerBackground: <HeaderBackground/>
        }
    };

    constructor(props) {
        super(props);
        const {setParams} = props.navigation
        this.state = {
            floors: null
        }

        setParams({
            isJuniorUser: this.props.userInfo.memberType == 2,
            addBtn: this.addBtnClick.bind(this)
        })
    }

    addBtnClick(order){
        if(this.props.userInfo.memberType == 2){
            Alert.alert(
                '提示',
                HSTips.limitsCreatSmart,
                [
                    {text: '知道了', onPress: () => {}, style: 'cancel'}
                ]
            )
        }else{
            if(order == 1){
                this.props.navigation.navigate('SmartRecommend')
            }else if(order == 3){
                this.props.navigation.navigate('AddSmart',{title:'新增智能'})
            }
        }
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                <SmartList 
                    navigation={this.props.navigation}
                    isDark={this.props.themeInfo.isDark}
                    isJuniorUser={this.props.userInfo.memberType == 2}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        marginLeft: 15,
        textAlignVertical: 'center',
        color: Colors.themeTextBlack,
        flex: 1
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingHorizontal: 16,
        marginLeft: 5
    },
    navText: {
        color: Colors.themeBG,
        fontSize: 15
    },
    recommendNavImg:{
        width: 22,
        height: 22,
        resizeMode: 'contain'
    },
    logIcon:{
        width: 17,
        height: 19,
        resizeMode: 'contain'
    },
    navAdd:{
        width: 20,
        height: 20,
        resizeMode: 'contain'
    }
})
    
export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
        userInfo: state.userInfo
    })
)(Smart)

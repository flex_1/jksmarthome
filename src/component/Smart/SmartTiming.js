/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	Alert
} from 'react-native';
import { Colors,NetUrls } from '../../common/Constants';
import ToastManager from '../../common/CustomComponent/ToastManager';
import { postJson } from '../../util/ApiRequest';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import {BottomSafeMargin,StatusBarHeight} from '../../util/ScreenUtil';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import DateTimePicker from '@react-native-community/datetimepicker';
import DatePicker from '../../third/Datepicker/datepicker';

class SmartTiming extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'定时'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text:'保存', onPress:()=>{
                        navigation.getParam('saveBtnClick')()
                    }}
                ]}/>
            ),
            headerBackground: <HeaderBackground/>
        }
    };

	constructor(props) {
		super(props);
		const {getParam,setParams} = this.props.navigation
		this.callBack = getParam('callBack') 
		this.timingData = getParam('timingData') || {}

		this.selects = this.timingData.cyclic

		this.state = {
			days:[
				{id:0,name:'单',value:'0'},
				{id:1,name:'一',value:'2'},
				{id:2,name:'二',value:'3'},
				{id:3,name:'三',value:'4'},
				{id:4,name:'四',value:'5'},
				{id:5,name:'五',value:'6'},
				{id:6,name:'六',value:'7'},
				{id:7,name:'日',value:'1'},
			],
			chosenDate: getParam('currentDate') || new Date(),
			selects: this.getDefaultSelects(),
			operation: this.operation == null ? 1 : this.operation
        }
        
        setParams({
            saveBtnClick: ()=>{ this.save() }
        })
	}

	//处理默认的选中 周期
	getDefaultSelects(){
		if(!this.selects){
			return ['0']
		}
		let selectArr = []
		selectArr = this.selects.split(',')
		if(Array.isArray(selectArr)){
			console.log( selectArr );
			
			return selectArr
		}else{
			return []
		}
	}
 
	componentDidMount() {
		
	}

	componentWillUnmount(){
		
	}

	save(){
		if(this.state.selects.length <= 0){
			ToastManager.show('请选择定时器周期')
			return
		}
		const {pop} = this.props.navigation

		let selected = this.state.selects.concat()
		selected = selected.sort()

		let timeStr = this.state.chosenDate
		let hour = null
		let minute = null
		try {
			if(typeof timeStr == 'object'){
				hour = timeStr.getHours()
				minute = timeStr.getMinutes()
			}else if(typeof timeStr == 'string'){
				hour = timeStr.split(':') && timeStr.split(':')[0]
				minute = timeStr.split(':') && timeStr.split(':')[1]
			}
			if((hour || hour == 0) && (minute || minute == 0)){
				// this.requestSaveTimer( parseInt(hour),parseInt(minute),selected.toString() )

				// 此数据 必须和 接口保持一致
				let data = {}
				if(this.timingData && this.timingData.conditionType){
					data = JSON.parse(JSON.stringify(this.timingData))
				}
				data.status = 1
				data.hour = parseInt(hour)
				data.minute = parseInt(minute)
				data.cyclic = selected.toString()
				data.conditionType = 3
				data.timeruuId = data.timeruuId ? data.timeruuId : null

				this.callBack && this.callBack(data)
				if(this.timingData && this.timingData.conditionType){
					pop(1)
				}else{
					pop(2)
				}
			}else{
				ToastManager.show('时间格式有误')
			}
		} catch (error) {
			ToastManager.show('数据格式有误')
		}
	}

	// 存储 定时
	async requestSaveTimer(hour,minute,cyclic){
		const {pop} = this.props.navigation
		let params = {}
		if(this.sceneId){
			params.sceneId = this.sceneId
		}
		if(this.deviceId){
			params.deviceId = this.deviceId
		}
		if(this.timerId){
			params.id = this.timerId
		}
		if(this.uuid){
			params.uuid = this.uuid
		}
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.addTimer,
				params: {
					...params,
					hour: hour,
					minute: minute,
					cyclic: cyclic,
					status: this.status,
					operation: this.state.operation
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.callBack && this.callBack()
				pop()
				ToastManager.show('创建成功')
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	//删除定时器
	async deleteTimer(){
		const {pop} = this.props.navigation
		let params = {}
		if(this.sceneId){
			params.sceneId = this.sceneId
		}
		if(this.deviceId){
			params.deviceId = this.deviceId
		}
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.delTimer,
				params: {
					...params,
					id : this.timerId,
					uuid: this.uuid,
					cyclic : this.selects.toString()
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.callBack && this.callBack()
				pop()
				ToastManager.show('该定时器已删除')
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
	}

	getIndicatorHead(){
        const Colors = this.props.themeInfo.colors

		let list = this.state.days.map((val,index)=>{
			let i = this.state.selects.indexOf(val.value)
			let selectBg = i == -1 ? {backgroundColor: Colors.themeBg} : {backgroundColor: Colors.themeButton}
			let selectColor = i == -1 ? {color:Colors.themeTextLight} : {color:Colors.white}
			return(
				<TouchableOpacity key={'timing_index_'+index} style={[styles.dayTouch,selectBg]} onPress={()=>{
					if(index == 0 && i==-1){
						this.state.selects = [val.value]
						this.setState({
							selects: this.state.selects
						})
						return
					}
					if(i == -1){
						let todayIndex = this.state.selects.indexOf('0')
						if(todayIndex != -1){
							this.state.selects.splice(todayIndex,1)
						}
						this.state.selects.push(val.value)
					}else{
						this.state.selects.splice(i,1)
					}
					this.setState({
						selects: this.state.selects
					})
				}}>
					<Text style={[{fontSize:15},selectColor]}>{val.name}</Text>
				</TouchableOpacity>
			)
		})

		return(
			<View style={{paddingHorizontal:16,marginTop:20,flexDirection:'row',justifyContent:'space-between'}}>
				{list}
			</View>
		)
	}

	getDateTimeItem() {
        const Colors = this.props.themeInfo.colors

		return (
			<DatePicker
				ref={e => this.timer_date_picker = e}
				style={{flex:1 }}
				date={this.state.chosenDate}
				mode="time"
				format="HH:mm"
				confirmBtnText="确定"
				cancelBtnText="取消"
				showIcon={false}
				androidMode={'spinner'}
				customStyles={{
					dateInput: {
						borderWidth: 0,
						alignItems:'flex-end',
						justifyContent:'center',
						paddingRight:15
					},
					dateText: {
						color: Colors.themeText,
						fontSize: 15,
						textAlign:'right'
					}
				}}
				onDateChange={(time) => {
					this.setState({
						chosenDate: time
					})
				}}
			/>
		)
	}

	getAndroidPicker(){
		if(Platform.OS == 'ios'){
			return null
        }
        const Colors = this.props.themeInfo.colors
		return(
			<View style={{paddingVertical:5,paddingHorizontal:16,marginTop:30}}>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.inputWrapper,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
					    this.timer_date_picker && this.timer_date_picker.onPressDate()
				    }}
                >
					<Text style={{color:Colors.themeTextLight,fontSize:15}}>选择时间</Text>
					{this.getDateTimeItem()}
					<Image style={styles.rightArrow} source={require('../../images/enter_light.png')} />
				</TouchableOpacity>
			</View>
		)
	}

	getDatePicker(){
		if(Platform.OS == 'android'){
			return null
        }
        const Colors = this.props.themeInfo.colors
		return(
			<View style={{marginTop:30,backgroundColor: Colors.white,borderRadius: 5}}>
				<DateTimePicker
                    display='spinner'
                    textColor={Colors.themeText}
					value={this.state.chosenDate}
                    mode={'time'}
                    onChange={(event, date)=>{
                        this.setState({
							chosenDate: date
						})
                    }}
				/>
			</View>
		)
	}

	getBottomBtn() {
		if (this.timerId) {
			return (
				<View style={styles.bottomBtnWrapper}>
					<TouchableOpacity activeOpacity={0.7} style={styles.deleteTouch} onPress={() => {
						Alert.alert(
                            '提示',
                            '确认删除该定时器?',
                            [
                                {text: '取消', onPress: () => {}, style: 'cancel'},
                                {text: '删除', onPress: () => { this.deleteTimer() }},
                            ]
                        )
					}}>
						<Text style={{ color: Colors.white }}>删除</Text>
					</TouchableOpacity>
				</View>
			)
		}
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getIndicatorHead()}
				{this.getDatePicker()}
				{this.getAndroidPicker()}
				{this.getBottomBtn()}
			</View>
			
		)
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.themBGLightGray,
		flex:1,
	},
	scrollContent:{
		paddingBottom: 30,
	},
	inputWrapper:{
		width: '100%',
		height: 50,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		marginTop:15,
		paddingHorizontal: 16
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight:5
	},
	navText:{
		color:Colors.tabActiveColor,
		fontSize:15
	},
	dayTouch:{
		width:32,
		height:32,
		borderRadius:4,
		justifyContent:'center',
		alignItems:'center'
	},
	bottomBtnWrapper: {
		position: 'absolute',
		left: 0,
		bottom: BottomSafeMargin,
		width: '100%',
		paddingHorizontal: 16,
		paddingVertical: 10
	},
	deleteTouch: {
		width: '100%',
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: Colors.themBgRed,
		borderRadius: 4
	},
	operationWrapper:{
		width:'50%',
		height:30,
		justifyContent:'center',
		alignItems:'center',
    },
    rightArrow: { 
        marginLeft: 10, 
        width: 7, 
        height: 13, 
        marginRight: 10,
        resizeMode:'contain' 
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SmartTiming)

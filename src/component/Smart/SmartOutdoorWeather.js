/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	FlatList,
	Modal,
    Alert
} from 'react-native';
import { Colors,NetUrls } from '../../common/Constants';
import ToastManager from '../../common/CustomComponent/ToastManager';
import { postJson } from '../../util/ApiRequest';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import {ListNoContent} from "../../common/CustomComponent/ListLoading";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import { connect } from 'react-redux';
import {ImagesLight, ImagesDark} from '../../common/Themes';

// 条件 里面的 选择设备
class SmartOutdoorWeather extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'室外天气'}/>,
            headerBackground: <HeaderBackground/>
		}
	}

	constructor(props) {
		super(props);
		const {getParam} = this.props.navigation
		this.callBack = getParam('callBack')

		this.state = {
			conditionData: [],
			modalVisible: false,
			conditionSelectData: [],
            weatherName:'',
            conditionType: null,
            houseLocation: null,
            houseId: null,
            houseName: null,

            weatherList: [
                {name: '室外天气', conditionType: 10, dayIcon: require('../../images/smart/outdoor_weather.png'), nightIcon: require('../../images/smart/outdoor_weather_dark.png')},
                {name: '日出|日落', conditionType: 11, dayIcon: require('../../images/smart/outdoor_sunrise.png'), nightIcon: require('../../images/smart/outdoor_sunrise_dark.png')},
                {name: '室外温度', conditionType: 13, dayIcon: require('../../images/smart/outdoor_temp.png'), nightIcon: require('../../images/smart/outdoor_temp_dark.png')},
                {name: '室外湿度', conditionType: 14, dayIcon: require('../../images/smart/outdoor_humi.png'), nightIcon: require('../../images/smart/outdoor_humi_dark.png')},
                {name: '室外PM2.5', conditionType: 15, dayIcon: require('../../images/smart/outdoor_pm25.png'), nightIcon: require('../../images/smart/outdoor_pm25_dark.png')},
            ],
		}
	}

	componentDidMount() {
		this.getHouseLocationInfo()
	}

	componentWillUnmount(){

	}

	// 获取有设备
    async getHouseLocationInfo() {
		SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.houseAddress,
                params: {
                }
			});
			SpinnerManager.close()
            if (data.code == 0) {
				this.setState({
					houseLocation: data.result.city,
                    houseId: data.result.id,
                    houseName: data.result.name
				},()=>{
                    this.checkHouseLocationExist()
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
	}

	
	// 获取 条件 getConditionByDeviceId
	async getConditionsById(conditionType) {
		SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.environmentRange,
                params: {
					type: conditionType
                }
			});
			SpinnerManager.close()
            if (data.code == 0) {
				// 数据获取成功后 弹出modal
				let conditionData = data.result
				if(!conditionData || conditionData.length<=0){
					ToastManager.show('暂无天气选项')
					return
				}
				let pickerData = []
				for (const val of conditionData) {
					let condition = {}
					let conditionUnit = val.conditionUnit || ''
					let values = []
					if(val.value && val.value.length){
						for (const iterator of val.value) {
							values.push(iterator + conditionUnit)
						}
					}else{
						values = ['-']
					}
					condition[val.conditionText + val.title] = values
					pickerData.push(condition)
				}
				this.setState({
					conditionData: conditionData,
					pickerData: pickerData,
                },()=>{
					this.setState({modalVisible: true})
				})

            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
			SpinnerManager.close()
            ToastManager.show('网络错误')
        }
	}

    checkHouseLocationExist(){
        if(this.state.houseId && !this.state.houseLocation){
            const {pop, navigate} = this.props.navigation

            Alert.alert(
                '提示',
                '请填写您的家庭地址，以便我们为您提供更精准的智能服务。',
                [ {text: '下次再说', onPress: () => { pop() }, style: 'cancel'},
                { text: '去设置', onPress: () => {
                    navigate('AddHouse',{
                        title:'房屋设置',
                        id: this.state.houseId,
                        name: this.state.houseName,
                        callBack: () => { this.getHouseLocationInfo() },
                    })
                }}],
                { cancelable: false }
            )
            return false
        }
        return true
    }

	// Picker 确定按钮 点击处理
	handlePickerSelectData(selectData,selectIndex){
		const {pop} = this.props.navigation
		
		let firstIndex = selectIndex[0]
		// let secondIndex = selectIndex[1]
		
		let selectConditionId = this.state.conditionData[firstIndex]['conditionId']
		let conditionUnit = this.state.conditionData[firstIndex]['conditionUnit']
		let selectValue = selectData[1]
		if(conditionUnit && (selectValue != null) ){
			selectValue = selectValue.replace(conditionUnit,'')
		}

		let data = {}
		data.conditionId = selectConditionId
		data.conditionTitle = selectData[0]
		data.conditionValue = selectValue
		data.conditionType = this.state.conditionType
		data.name = this.state.weatherName
        data.conditionUnit = conditionUnit
        data.weatherNightIcon = this.state.nightIcon
        data.weatherDayIcon = this.state.dayIcon

		this.callBack && this.callBack(data)
		this.setState({
			modalVisible: false
		},()=>{
			pop(2)
		})
	}

	// 获取选择框的 Modal
	getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
	}
	
	// 弹出 Modal
    showDoublePicker() {
        window.DoubulePicker.init({
            pickerData: this.state.pickerData,
            pickerConfirmBtnText: '确定  ',
            pickerCancelBtnText: '取消',
            pickerTitleText: this.state.weatherName,
            pickerCancelBtnColor: [38, 80, 215, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: [],
            onPickerConfirm: (data,index) => {
				console.log(data,index)
                this.handlePickerSelectData(data,index)
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })
            },
            onPickerSelect: data => {
            }
        });
        window.DoubulePicker.show()
    }

	renderRowItem(rowData,index) {
        const Colors = this.props.themeInfo.colors
        const split = this.state.weatherList.length == index +1 ? null : <View style={[styles.split,{backgroundColor: Colors.split}]}/>
        const weatherIcon = this.props.themeInfo.isDark ? rowData.nightIcon : rowData.dayIcon

        return(
            <TouchableOpacity activeOpacity={0.8} style={[styles.item,{backgroundColor:Colors.themeBg}]} onPress={()=>{
                if(!this.state.houseId){
                    ToastManager.show('数据加载中，请稍后。')
                    return
                }
                if(!this.checkHouseLocationExist()){
                    return
                }
				this.setState({
					conditionType: rowData.conditionType,
					weatherName: rowData.name,
                    dayIcon: rowData.dayIcon,
                    nightIcon: rowData.nightIcon
				})
				this.getConditionsById(rowData.conditionType)
            }}>
                <Image style={{width:35,height:35,resizeMode:'contain'}} source={weatherIcon}/>
				<View style={{justifyContent:'center',flex: 1,marginLeft: 20, }}>
					<Text style={{color:Colors.themeText,fontSize:15}}>{rowData.name}</Text>
				</View>
                <Image style={{width:8,height:14,resizeMode:'contain'}} source={require('../../images/enterLight.png')}/>
                {split}
            </TouchableOpacity>
        )
	}
	
	_extraUniqueKey(item, index) {
        return "weather_index_" + index;
    }

	getDeviceList(){
		let list = this.state.weatherList

		return(
			<FlatList
				style={{marginTop:10}}
				contentContainerStyle={{paddingBottom:50}}
				data={list}
				scrollIndicatorInsets={{ right: 1 }}
                removeClippedSubviews={false}
                keyExtractor={this._extraUniqueKey}
                renderItem={({item,index}) => this.renderRowItem(item,index)}
            />
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getDeviceList()}
				{this.getModal()}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.white,
		flex:1,
	},
	item:{
		width: '100%',
		borderRadius:5,
        alignItems:'center',
		flexDirection: 'row',
		height: 60,
		paddingHorizontal: 20,
	},
	modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    split:{
        position: 'absolute',
        bottom: 0,
        left: 16,
        width: '100%',
        height: 1
    },
    roomText:{
        color:Colors.themeTextLightGray,
        fontSize:13,
        marginTop:5
    }
	
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SmartOutdoorWeather)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    SwipeableFlatList,
    Alert,
    Modal
} from 'react-native';
import { Colors, NetUrls } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import {ListNoContent} from "../../common/CustomComponent/ListLoading";
import { connect } from 'react-redux';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import {ImagesLight, ImagesDark} from '../../common/Themes';
import { BottomSafeMargin } from '../../util/ScreenUtil';

//侧滑最大距离
const maxSwipeDistance = 100
//侧滑按钮个数
const countSwiper = 1

class ExecuteDeviceList extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={navigation.getParam('title')}/>,
            headerBackground: <HeaderBackground hiddenBorder={true}/>
		}
	}

    constructor(props) {
        super(props);
        const { getParam } = props.navigation;
        this.callBack = getParam('callBack')
        this.state = {
            list: getParam('list'),
            timeList: getParam('timeList'),
            deviceId: getParam('deviceId'),
            type: getParam('type'),
            selectItem: getParam('selectItem'),
            floor : getParam('floor'),
            floorText: getParam('floorText'),
            roomName : getParam('roomName'),
            modalVisible: false,
            data: null,
            selectData: [],
            execute:[],
            pickerTitle: '',
            smartName:getParam('title'),
            smartIcon:getParam('deviceIcon'),
            dayIcon:getParam('dayIcon'),
            nightIcon:getParam('nightIcon')
        }
    }

    componentDidMount() {
        this.initData();
    }

    componentWillUnmount() {
        
    }

    // 数据初始化
    initData(){
        if(this.state.selectItem){

            let json = this.state.selectItem.executeJson;
            let obj = JSON.parse(json);

            let list = this.state.list;
            let execute = [];

            let i = 0
            for (const exeData of list) {
                let executeKey = exeData.executeKey
                let executeValue = obj[executeKey]

                let objItem = {};
                let d = [];
                let data  = [];
                if(executeValue){
                    let vals = executeValue.split(',')
                    let first = this.seachFirstText(vals[1]);
                    let second = this.seachSecondText(vals[0], exeData.value);
                    list[i].executeTitle = first
                    list[i].executeValue = second
                    d.push(vals[0])
                    d.push(vals[1])
                    data.push(first)
                    data.push(second)

                    objItem.index = i
                    objItem.parttern = list[i].executeKey
                    objItem.position = d
                    objItem.data = data
                    execute.push(objItem)
                }
                i++;
            }
            this.setState({
                list:list,
                execute:execute
            })
        }
    }

    //准备选择框数据
    prepareForPicker(index, deviceName){
        let dlist = this.state.list[index].value;
        let s_title = []
        let s_value = []
        for (const item of this.state.timeList) {
            s_title.push(item.text)
        }
        for (const item of dlist) {
            s_value.push(item.text)
        }
        this.setState({
            data: [s_title,s_value],
            modalVisible: true,
            pickerTitle:deviceName
        })
    }

    // 选择框 确定按钮点击事件
    handlePickerSelectData(data,index){

        //保存选择的执行条件
        let execute = this.state.execute;

        let exitIndex = null
        for(let i=0;  i<execute.length; i++){
            if(execute[i].index == this.state.index){
                exitIndex = i
            }
        }
        let obj = {};
        obj.index = this.state.index;
        obj.parttern = this.state.list[this.state.index].executeKey
        let d = [];
        let first = this.getExeId(index[1]);
        let second = this.getTimeId(index[0]);
        d.push(first)
        d.push(second)
        obj.position = d;
        obj.data = data;

        if(exitIndex != null){
            execute[exitIndex] = obj
        }else{
            execute.push(obj)
        }

        let list = this.state.list;
        for(let i in list){
            if(this.state.index == i){
                list[i].executeTitle = data[0];
                list[i].executeValue = data[1];
            }
        }

        this.setState({
            list:list,
            execute:execute
        })
    }

    //获取 执行时间的 id
    getTimeId(param){
        let id = this.state.timeList[param].id
        return id;
    }

    // 获取 执行操作的 id
    getExeId(param){
        let id;
        let {list} = this.state;
        if(list && list.length>0){
            id = list[this.state.index].value[param].id;
        }
        return id;
    }

    // 根据id查询延迟时间text
    seachFirstText(val){
        let result = '';
        let timelist = this.state.timeList;
        if(timelist && timelist.length>0){
            for(const item of timelist){
                if(item.id == val){
                    result = item.text;
                }
            }
        }
        return result;
    }

    // 根据id查询执行目标
    seachSecondText(val,targetList){
        let result = '';
        for(let item of targetList){
            if(item.id == val){
                result = item.text;
            }
        }
        return result;
    }

    // 清空条件
    clearExe(rowData,index){
        
        rowData.executeTitle = null
        rowData.executeValue = null
        this.state.list[index] = rowData

        let newExeData = []
        for (const exeData of this.state.execute) {
            if(exeData.index != index){
                newExeData.push(exeData)
            }
        }

        this.setState({
            execute: newExeData,
            list: this.state.list
        })
    }

    // 保存按钮点击
    saveBtnClick(){
        const {pop} = this.props.navigation

        let exe = this.state.execute;
        if(exe.length<=0){
            ToastManager.show("请选择至少一条执行条件");
            return;
        }
        console.log("exe="+JSON.stringify(exe));
        
        let json = {};
        for (const i in exe) {
            if (exe.hasOwnProperty(i)) {
                json[exe[i].parttern] = exe[i].position.toString()
            }
        }
        let data = {}
        data.deviceId = this.state.deviceId //执行智能id
        data.executeJson = JSON.stringify(json)
        data.executeType = this.state.type //执行类型 ，1-通知 2-智能 3-场景 4-设备
        data.name = this.state.smartName
        // data.icon = this.state.smartIcon
        let title = this.state.execute[0].data
        data.conditionTitle = title[1];
        data.conditionValue = title[0];
        data.dayIcon = this.state.dayIcon
        data.nightIcon = this.state.nightIcon
        data.floor = this.state.floor
        data.floorText = this.state.floorText
        data.roomName = this.state.roomName

        this.callBack && this.callBack(data)
        this.setState({
            modalVisible: false
        }, () => {
            this.state.selectItem ? pop(1) : pop(3)
        })
    }

    getModal() {
        if(!this.state.data){
            return null
        }
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        window.DoubulePicker.init({
            pickerData: this.state.data,
            pickerConfirmBtnText: '确定',
            pickerCancelBtnText: '取消',
            pickerTitleText: this.state.pickerTitle,
            pickerCancelBtnColor: [38, 80, 215, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: this.state.selectData,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false,
                    selectData: data
                })
                this.handlePickerSelectData(data,index);
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })
            },
            onPickerSelect: data => {
            }
        });
        window.DoubulePicker.show()
    }

    _extraConditionUniqueKey(item, index) {
        return "condition_index_" + index;
    }

    // 获取侧滑设置
    getQuickActions(rowData, index){
        return (
			<View style={styles.quickAContent}>
				<TouchableOpacity activeOpacity={0.7} onPress={() => {
					this.closeSwiper()
					this.clearExe(rowData, index)
				}}>
					<View style={[styles.quick, { backgroundColor: Colors.themBgRed }]}>
						<Text style={{ color: Colors.white, fontSize: 14 }}>清空</Text>
					</View>
				</TouchableOpacity>
			</View>
		)
    }

    //关闭侧滑栏
	closeSwiper() {
        this.swiperFlatlist && this.swiperFlatlist.setState({ openRowKey: null })
    }

    renderRowItem(rowData,index) {
        const Colors = this.props.themeInfo.colors
        let executeText = ''
        if(rowData.executeTitle && rowData.executeValue){
            executeText = rowData.executeTitle + ' ' + rowData.executeValue
        }

        return(
            <TouchableOpacity 
                activeOpacity={1} 
                style={[styles.itemTouch,{backgroundColor:Colors.themeBg, borderBottomColor: Colors.themeBaseBg}]} 
                onPress={()=>{
                    if(rowData.executeTitle && rowData.executeValue){
                        let s_title = rowData.executeTitle
                        let s_value = rowData.executeValue
                        this.setState({
                            selectData: [s_title,s_value]
                        })
                    }else if(rowData.executeKey == 'temperature'){
                        this.setState({
                            selectData: ['立即','26°C']
                        })
                    }
                    this.setState({ index: index })
                    this.prepareForPicker(index,rowData.text)
                    this.closeSwiper()
                }}
            >
                <Text style={[styles.itemText,{color: Colors.themeText}]}>{rowData.text}</Text>
                <Text style={[styles.itemDesc,{color: Colors.themeTextLight}]}>{executeText}</Text>
                <Image style={{width:8,height:14,resizeMode:'contain'}} source={require('../../images/enterLight.png')}/>
            </TouchableOpacity>
        )
    }

    getFlatList(){
        let list = this.state.list;
        // 暂无场景
        if (list && list.length <= 0) {
            const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
            return (
                <ListNoContent img={Images.noDevice} text={'暂无设备'}/>
            )
        }
        return(
            <SwipeableFlatList
                ref={e => this.swiperFlatlist = e}
                contentContainerStyle={{ paddingBottom: 50 }}
                style={styles.list}
                data={list}
                keyExtractor={this._extraConditionUniqueKey}
                renderItem={({item,index}) => this.renderRowItem(item,index)}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator = {false}
                renderQuickActions={({ item, index }) => this.getQuickActions(item, index)}
                maxSwipeDistance={maxSwipeDistance}
				bounceFirstRowOnMount={false}
            />
        )
    }

    showButtom(){
        return(
            <TouchableOpacity 
                activeOpacity={0.7} 
                style={[styles.addTouch,{backgroundColor: Colors.tabActiveColor}]} 
                onPress={()=>{
                    this.saveBtnClick();
                }}
            >
                <Text style={{fontSize:15,color:Colors.white}}>确定</Text>
            </TouchableOpacity>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]} >
                {this.getFlatList()}
                {this.getModal()}
                {this.showButtom()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    list:{
        marginTop: 10
    },
    addTouch:{
        marginTop:20,
        marginBottom: BottomSafeMargin + 10,
        height: 44,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderWidth: 1,
        borderColor: Colors.tabActiveColor,
        marginHorizontal: 16,
    },
    modalTouchable:{
        width: '100%',
        height: '100%',
        backgroundColor: Colors.translucent
    },
    itemTouch:{
        paddingHorizontal: 16,
        height:55,
        alignItems:'center',
        flexDirection: 'row',
        borderBottomWidth: 1
    },
    itemText:{
        marginLeft: 20,
        fontSize: 16,
        color:Colors.themeTextBlack,
        flex:1
    },
    itemDesc:{
        position:'absolute',
        right:30,
        marginLeft: 20,
        fontSize: 16,
        color:Colors.themeTextLightGray,
    },
    //侧滑菜单的样式
	quickAContent: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginRight: 0,
		marginTop: 0,
		overflow: 'hidden',
	},
	quick: {
		backgroundColor: Colors.white,
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
        width: maxSwipeDistance / countSwiper,
	},
    
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ExecuteDeviceList)

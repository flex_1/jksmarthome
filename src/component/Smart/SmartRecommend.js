/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    FlatList,
    Alert,
    RefreshControl,
    SwipeableFlatList,
    Dimensions,
    ImageBackground
} from 'react-native';
import {Colors, NotificationKeys, NetUrls} from '../../common/Constants';
import {StatusBarHeight} from '../../util/ScreenUtil';
import {postJson} from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import { connect } from 'react-redux';

const screenW = Dimensions.get('screen').width;
const bgWidth = screenW - 16*2
const bgHeight = bgWidth * 0.45

class SmartRecommend extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'智能推荐'}/>,
            headerBackground: <HeaderBackground/>
		}
	}

    constructor(props) {
        super(props);
        this.state = {
            listData: null
        }
    }

    componentDidMount() {
        this.getRecommendList()
    }

    componentWillUnmount() {
        
    }

    // 获取推荐场景 列表
    async getRecommendList(){
        SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.rmdSmartList,
				params: {
				}
            });
            SpinnerManager.close()
			if (data.code == 0) {
                this.setState({
                    listData: data.result || []
                })
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
            SpinnerManager.close()
			ToastManager.show('网络错误')
		}
    }
    
    getListView(){
        if(!this.state.listData){
            return null
        }

        const {navigate} = this.props.navigation

        let list = this.state.listData.map((val,index)=>{
            return(
                <TouchableOpacity 
                    key={'recommend_list_'+index} 
                    activeOpacity={0.8} 
                    style={{marginBottom:16,borderRadius:4,overflow:'hidden',backgroundColor:Colors.themBGLightGray}} 
                    onPress={()=>{
                        navigate('AddSmart',{title:'推荐智能',id: val.id, isRmdSmart: true})
                    }}
                >
                    <Image style={{width:bgWidth,height:bgHeight}} source={{uri: val.img}}/>
                    <Text style={styles.sceneName}>{val.name}</Text>
                </TouchableOpacity>
            )
        })

        return(
            <View style={styles.wrapper}>
                {list}
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <ScrollView 
                style={[styles.container,{backgroundColor: Colors.themeBaseBg}]} 
                contentContainerStyle={{paddingBottom: 50}}
                scrollIndicatorInsets={{right: 1}}
            >
                {this.getListView()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        flex: 1
    },
    wrapper:{
        marginTop: 10,
        paddingHorizontal:16
    },
    sceneName:{
        color: Colors.white,
        fontSize: 18,
        position: 'absolute',
        left: 15,
        top: 10
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SmartRecommend)

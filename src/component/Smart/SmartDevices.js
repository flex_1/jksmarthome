/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Dimensions,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	FlatList,
	Modal
} from 'react-native';
import { Colors,NetUrls } from '../../common/Constants';
import ToastManager from '../../common/CustomComponent/ToastManager';
import { postJson } from '../../util/ApiRequest';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import {ListNoContent} from "../../common/CustomComponent/ListLoading";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import { connect } from 'react-redux';
import {ImagesLight, ImagesDark} from '../../common/Themes';

const screenW = Dimensions.get('window').width;


class SmartDevices extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'选择设备'}/>,
            headerBackground: <HeaderBackground/>
		}
	}

	constructor(props) {
		super(props);
		const {getParam, setParams} = this.props.navigation
		this.callBack = getParam('callBack')
        this.roomId = getParam('roomId')

		this.state = {
			deviceData: null,
			conditionData: [],
			modalVisible: false,
			
			deviceName:'',
			deviceIcon:'',
            deviceId:'',
            floor:'',
            floorText:'',
            roomName:'',

            floorList: [],
            selectFloorId: 0,
            selectRoomId: 0
		}
	}

	componentDidMount() {
        this.requestFloorList()
	}

	componentWillUnmount(){

	}

    async requestFloorList() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.floorAndRoomList,
                params: {
                    type: 6   //6智能执行设备
                }               
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.setState({
                    floorList: data.result || []
                })
                this.getSmartDevices()
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

	// 获取有设备
    async getSmartDevices() {
		SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.conditionDeviceList,
                params: {
                    floor: this.state.selectFloorId,
                    roomId: this.state.selectRoomId
                }
			});
			SpinnerManager.close()
            if (data.code == 0) {
				this.setState({
					deviceData: data.result || []
				})
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
	}

	
	// 获取 条件 getConditionByDeviceId
	async getConditionsById(id) {
		SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.getConditionByDeviceId,
                params: {
					deviceId: id
                }
			});
			SpinnerManager.close()
            if (data.code == 0) {
				// 数据获取成功后 弹出modal
				let conditionData = data.result
				if(!conditionData || conditionData.length<=0){
					ToastManager.show('该设备暂无条件选择')
					return
				}
				let pickerData = []
				for (const val of conditionData) {
					let condition = {}
					let conditionUnit = val.conditionUnit || ''
					let values = []
					if(val.value && val.value.length){
						for (const iterator of val.value) {
							values.push(iterator + conditionUnit)
						}
					}else{
						values = ['-']
					}
					condition[val.conditionText + val.title] = values
					pickerData.push(condition)
				}
				this.setState({
					conditionData: conditionData,
					pickerData: pickerData,
                },()=>{
					this.setState({modalVisible: true})
				})

            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
			SpinnerManager.close()
            ToastManager.show('网络错误')
        }
	}


	// 处理设备选择
	handlePickerSelectData(selectData,selectIndex){
		const {pop} = this.props.navigation
		
		let firstIndex = selectIndex[0]
		// let secondIndex = selectIndex[1]
		
		let selectConditionId = this.state.conditionData[firstIndex]['conditionId']
		let conditionUnit = this.state.conditionData[firstIndex]['conditionUnit']
		let selectValue = selectData[1]
		if(conditionUnit && (selectValue != null) ){
			selectValue = selectValue.replace(conditionUnit,'')
		}

		let data = {}
		data.conditionId = selectConditionId
		data.conditionTitle = selectData[0]
		data.conditionValue = selectValue
		data.conditionType = 4
		data.deviceId = this.state.deviceId
		data.name = this.state.deviceName
        data.icon = this.state.deviceIcon
        data.dayIcon = this.state.dayIcon
        data.nightIcon = this.state.nightIcon
        data.conditionUnit = conditionUnit
        data.floor = this.state.floor
        data.floorText = this.state.floorText
        data.roomName = this.state.roomName

		this.callBack && this.callBack(data)
		this.setState({
			modalVisible: false
		},()=>{
			pop(2)
		})
	}

	// 获取选择框的 Modal
	getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker()
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
	}
	
	// 弹出 Modal
    showDoublePicker() {
        window.DoubulePicker.init({
            pickerData: this.state.pickerData,
            pickerConfirmBtnText: '确定  ',
            pickerCancelBtnText: '取消',
            pickerTitleText: this.state.pickerTitle,
            pickerCancelBtnColor: [38, 80, 215, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: [],
            onPickerConfirm: (data,index) => {
				console.log(data,index)
                this.handlePickerSelectData(data,index)
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })
            },
            onPickerSelect: data => {
            }
        });
        window.DoubulePicker.show()
    }

    renderRowItem(rowData,index) {
        const Colors = this.props.themeInfo.colors
        const icon = this.props.themeInfo.isDark ? rowData.nightIcon : rowData.dayIcon
        const pLeft = index % 2 == 0 ? '6%' : '3%'
        const pRight = index % 2 == 0 ? '3%' : '6%'

        let room = null
        if(rowData.floorText && rowData.roomName){
            room = <Text style={[styles.roomText,{color:Colors.themeTextLight}]}>{rowData.floorText + '  ' + rowData.roomName}</Text>
        }

        return(
            <View style={{width: '50%',marginTop: 15,paddingLeft: pLeft,paddingRight: pRight}}>
                <TouchableOpacity activeOpacity={0.7} style={styles.itemTouch} onPress={()=>{
                    this.setState({
                        deviceId: rowData.deviceId,
                        deviceName: rowData.deviceName,
                        deviceIcon: rowData.deviceIcon,
                        dayIcon: rowData.dayIcon,
                        floor: rowData.floor,
                        floorText: rowData.floorText,
                        roomName: rowData.roomName,
                        pickerTitle: rowData.deviceName
                    })
                    this.getConditionsById(rowData.deviceId)
                }}>
                    <Image style={styles.itemImg} source={{uri: icon}}/>
                    <Text style={[styles.itemText,{color:Colors.themeText}]}>{rowData.deviceName}</Text>
                    {room}
                </TouchableOpacity>
            </View>
        )
    }

	renderDeviceList(){
		let list = this.state.deviceData

		if(!list){
			return null
		}

		if(list.length <= 0){
            const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
            const tips = this.roomId ? '该房间暂无设备\r\n您可以点击筛选来选择其他区域设备' : '暂无设备'
			return (
				<ListNoContent
					img={Images.noDevice} 
					text={tips}
				/>
			)
		}

		return(
			<FlatList
				style={{marginTop:10}}
				contentContainerStyle={{paddingBottom:50}}
				data={list}
				scrollIndicatorInsets={{ right: 1 }}
                removeClippedSubviews={false}
                keyExtractor={(item,index) => "devices_index_" + index}
                renderItem={({item,index}) => this.renderRowItem(item,index)}
                numColumns={2}
            />
		)
	}

    _renderRooms(roomList, btnBg){
        if(!roomList){
            return null
        }
        const Colors = this.props.themeInfo.colors
        return(
            roomList.map((value, index)=>{
                const rSelect = value.roomId == this.state.selectRoomId
                const bg = rSelect ? '#ECF6FF' : btnBg
                const color = rSelect ? Colors.newBtnBlueBg : Colors.themeTextLight 
                
                return(
                    <TouchableOpacity key={index} style={[styles.roomTouch,{backgroundColor: bg}]} onPress={()=>{
                        if(rSelect){
                            return
                        }
                        this.setState({
                            selectRoomId: value.roomId,
                            deviceData: null,
                            loading: true
                        },()=>{
                            this.getSmartDevices()
                        })
                    }}>
                        <Text numberOfLines={2} style={[styles.roomName,{color: color}]}>{value.roomName}</Text>
                    </TouchableOpacity>
                )
            })
        )
    }

    renderRoomRowItem(rowData, index){
        const Colors = this.props.themeInfo.colors

        const fSelect = rowData.floor == this.state.selectFloorId
        const leftIcon = fSelect ?  <View style={styles.leftIcon}/> : null
        const textColor = fSelect ? Colors.newBtnBlueBg : Colors.themeText
        const btnBg = fSelect ?  Colors.themeBg : Colors.themeBaseBg
        const upArrow = fSelect ? <Image style={styles.upArrow} source={require('../../images/arrowUp.png')}/> : null
        
        return(
            <View>
                <TouchableOpacity style={[styles.floorTouch,{backgroundColor:btnBg}]} onPress={()=>{
                    if(fSelect){
                        return
                    }
                    this.setState({
                        selectFloorId: rowData.floor,
                        selectRoomId: 0,
                        deviceData: null,
                        loading: true
                    },()=>{
                        this.getSmartDevices()
                    })
                }}>
                    <Text numberOfLines={2} style={[styles.floorName,{color: textColor}]}>{rowData.floorText}</Text>
                    {upArrow}
                    {leftIcon}
                </TouchableOpacity>
                {fSelect ? this._renderRooms(rowData.room, btnBg) : null}
            </View>
        )
    }

    rednerLeftRoomList(){
        return(
            <FlatList
				contentContainerStyle={{paddingBottom:50}}
				data={this.state.floorList}
				scrollIndicatorInsets={{ right: 1 }}
                removeClippedSubviews={false}
                keyExtractor={(item,index) => 'room_'+index}
                renderItem={({item,index}) => this.renderRoomRowItem(item,index)}
            />
        )
    }

    renderMainView(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={{flexDirection: 'row',flex: 1}}>
                <View style={{flex: 3}}>
                    {this.rednerLeftRoomList()}
                </View>
                <View style={{flex: 7,backgroundColor: Colors.themeBg}}>
                    {this.renderDeviceList()}
                </View>
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]} >
                {this.renderMainView()}
                {this.getModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
	container: {
		flex:1
	},
    modalTouchable:{
        width: '100%',
        height: '100%',
        backgroundColor: Colors.translucent
    },
	itemTouch:{
        width: '100%',
        borderWidth: 0.5,
        borderColor: '#C8C9CC',
        borderRadius:9,
        height: screenW * 0.7 * 0.41,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5
    },
    itemImg:{
        width: screenW * 0.7 * 0.41 * 0.35,
        height: screenW * 0.7 * 0.41 * 0.35,
        resizeMode: 'contain'
    },
    itemText:{
        fontSize: 13,
        fontWeight: '400',
        marginTop: 5,
        textAlign: 'center'
    },
    roomText:{
        fontSize:12,
        marginTop: 5,
        textAlign: 'center'
    },
    leftIcon:{
        position: 'absolute',
        left: 0,
        top: 0,
        bottom:0, 
        width: 4,
        backgroundColor: Colors.newBtnBlueBg
    },
    upArrow:{
        width:10,
        height:10,
        marginLeft: 5
    },
    floorTouch:{
        height: 50,
        justifyContent: 'center',
        alignItems:'center',
        paddingHorizontal: 5,
        flexDirection:'row',
    },
    floorName:{
        fontSize:14,
        fontWeight: '500',
        textAlign:'center',
        flex:1
    },
    roomTouch:{
        height: 42,
        justifyContent: 'center',
        alignItems:'center',
        paddingHorizontal:5,
    },
    roomName:{
        fontSize: 14,
        textAlign:'center'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SmartDevices)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    FlatList,
    Alert,
    RefreshControl,
    SwipeableFlatList,
} from 'react-native';
import {Colors, NotificationKeys, NetUrls} from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import {ColorsLight, ColorsDark} from '../../common/Themes';

//侧滑最大距离
const maxSwipeDistance = 80
//侧滑按钮个数
const countSwiper = 1

class SmartExcuteList extends Component {

    constructor(props) {
        super(props);
        this.isDark = props.isDark

        this.state = {
            executeList: [],
            pickerData: null,
            pickerName: '',

            currentSelectedValue: [], //当前选中的picker值, eg: [延时,操作]
            deviceExcuteData: null, // 选中设备的 属性数据 (场景 智能 无该值)
            delayList: [],  //延迟 表
            resultList: [], //执行 表
            selectedRowData: null,  //选中的 item rowData
            selectedRowIndex: null,  //选中的 item index
        }
    }

    componentDidMount() {
        
    }

    componentWillUnmount() {
        
    }

    // 获取字典列表
    async getDictList(type) {
        try {
            let data = await postJson({
                url: NetUrls.getDelayDictionary,
                params: {
                    type:type
                }
            });
            if (data.code == 0) {
                if (data.result) {
                    let dlist = data.result.delayList || [];
                    let rlist = data.result.resultList || [];

                    this.setState({
                        delayList: dlist
                    })

                    if(type == 2 || type == 3){
                        // 场景 或 智能
                        let s_title = []
                        let s_value = []
                        for (const item of dlist) {
                            s_title.push(item.text)
                        }
                        for (const item of rlist) {
                            s_value.push(item.text)
                        }
                        this.setState({
                            pickerData:[s_title,s_value],
                            resultList: rlist,
                            deviceExcuteData: null
                        },()=>{
                            this.props.openPicker()
                        })
                    }else{
                        // 设备
                        let obj = this.state.selectedRowData
                        this.getExcutesById(obj.deviceId);
                    }
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    /**
     * 根据设备id获取智能执行条件
     **/
    async getExcutesById(id){
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.getExecuteByDeviceId,
                params: {
                    deviceId:id
                }
            });
            SpinnerManager.close();
            if (data.code == 0) {
                if (data.result && data.result.length>0) {
                    this.setState({
                        deviceExcuteData: data.result,
                        resultList: data.result[0].value
                    },()=>{
                        this.dealAndJump()
                    })
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close();
            ToastManager.show('网络错误')
        }
    }

	// 用于 修改 执行条件里面的设置
    showDoublePicker() {
        window.DoubulePicker.init({
            pickerData: this.state.pickerData,
            pickerConfirmBtnText: '确定  ',
            pickerCancelBtnText: '取消',
            pickerTitleText: this.state.pickerName || '选择',
            pickerCancelBtnColor: [38, 80, 215, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: this.state.currentSelectedValue,
            onPickerConfirm: (data,index) => {
                console.log(data,index)
                this.handlePickerSelectData(data,index)
            },
            onPickerCancel: data => {
                this.props.closePicker()
            },
            onPickerSelect: data => {
            }
        });
        window.DoubulePicker.show()
    }

    //关闭侧滑栏
	closeSwiper() {
        this.swiperFlatlist_execute?.setState({ openRowKey: null })
    }
    
    _extraExcuteUniqueKey(item, index) {
		return "execute_index_" + index;
    }

    // 获取侧滑设置
    getQuickActions(rowData, index){
        return (
			<View style={styles.quickAContent}>
				<TouchableOpacity activeOpacity={0.7} onPress={() => {
					this.closeSwiper()
					Alert.alert(
						'提示',
						'确认删除?',
						[
							{ text: '取消', onPress: () => { }, style: 'cancel' },
							{ text: '删除', onPress: () => {
                                this.props.executeList.splice(index,1)
                                this.props.updateExcuteCallBack(this.props.executeList)
                            }},
						]
					)
				}}>
					<View style={[styles.quick, { backgroundColor: Colors.themBgRed }]}>
						<Text style={{ color: Colors.white, fontSize: 14 }}>删除</Text>
					</View>
				</TouchableOpacity>
			</View>
		)
    }

    // 处理 picker确定的点击 事件
    handlePickerSelectData(selectData,selectIndex){

        let selectValue = this.state.resultList[selectIndex[1]].id
        let delay = this.state.delayList[selectIndex[0]].id

        let data = JSON.parse(JSON.stringify(this.state.selectedRowData))
        data.delay = delay //延迟时间   
        if(this.state.deviceExcuteData){

            let executeKey = this.state.deviceExcuteData[0].executeKey
            let execute = {}
            execute[executeKey] = selectValue + ',' + delay
            data.executeJson = JSON.stringify(execute)

        }else{
            // 场景智能 executeJson 直接取选中值
            data.executeJson = selectValue
        }
        data.conditionValue = selectData[0]
        data.conditionTitle = selectData[1]
        data.conditionUnit = ''

        let executeList = JSON.parse(JSON.stringify(this.props.executeList))
        executeList[this.state.selectedRowIndex] = data
        this.props.closePicker()
        this.props.updateExcuteCallBack(executeList)
    }

    //响应或者跳转
    dealAndJump(){
        const {navigate} = this.props.navigation

        let dlist = this.state.delayList
        let rlist = this.state.resultList
        // 设备 执行的 数据
        let deviceExcuteData = this.state.deviceExcuteData
        //rowData 数据
        const {deviceId,name,dayIcon,nightIcon} = this.state.selectedRowData

        if(deviceExcuteData.length <= 1){
            let s_title = []
            let s_value = []
            for (const item of dlist) {
                s_title.push(item.text)
            }
            for (const item of rlist) {
                s_value.push(item.text)
            }
            this.setState({
                pickerData: [s_title,s_value],
                pickerTitle: name,
            },()=>{
                this.props.openPicker()
            })
        }else{
            navigate('ExecuteDeviceList',{
                title: name,
                deviceId: deviceId,
                dayIcon: dayIcon,
                nightIcon: nightIcon,
                type: 4,
                selectItem: this.state.selectedRowData,
                list: deviceExcuteData,
                timeList: dlist,
                callBack:(data)=>{
                    let executeList = JSON.parse(JSON.stringify(this.props.executeList))
                    executeList[this.state.selectedRowIndex] = data
                    this.props.closePicker()
                    this.props.updateExcuteCallBack(executeList)
                }
            })
        }
    }

    // 处理执行的点击事件
    handleExecuteClick(rowData,index){
        const {navigate} = this.props.navigation
        const executeType = rowData.executeType //执行类型 ，1-通知 2-智能 3-场景 4-设备

        if(executeType == 1){
            navigate('ExecuteNotification',{
                intelligentExecuteId: rowData.intelligentExecuteId,
                type: 1,
                notify: rowData.notify,
                notificationList: rowData.notificationList && JSON.parse(JSON.stringify(rowData.notificationList)),
                callBack: (data)=>{
                    this.props.executeList[index] = data
                    this.props.updateExcuteCallBack(this.props.executeList)
                }
            })
        }
        
        let s_value = rowData.conditionValue || ''
        let s_title = (rowData.conditionTitle || '') + (rowData.conditionUnit || '')
        this.setState({
            currentSelectedValue: [s_value,s_title],
            selectedRowData: JSON.parse(JSON.stringify(rowData)),
            selectedRowIndex: index,
            pickerName: rowData.name
        })

        if(executeType == 2 ){//2-智能
            if(!rowData.executeIntelligentId){
                return
            }
            this.getDictList(2,rowData,index);
        }else if(executeType == 3){//3-场景
            if(!rowData.sceneId){
                return
            }
            this.getDictList(3,rowData,index);
        }else if(executeType == 4){//设备
            if(!rowData.deviceId){
                return
            }
            this.getDictList(4)
        }
    }

    // 渲染 执行的 Item （单元cell）
    renderExecuteRowItem(rowData, index){
        const Colors = this.isDark ? ColorsDark : ColorsLight

        let extra = (rowData.conditionValue + " " + rowData.conditionTitle) + (rowData.conditionUnit ? rowData.conditionUnit : '')
        let icon = this.isDark ? {uri:rowData.nightIcon} : {uri:rowData.dayIcon}
        let room = null
        if(rowData.floorText && rowData.roomName){
            room = <Text numberOfLines={1} style={styles.roomText}>{rowData.floorText + '  ' + rowData.roomName}</Text>
        }

        if(rowData.executeType == 3 && !rowData.sceneId){
            extra = '暂无该场景'
            icon = require('../../images/smart/execute_sence.png')
        }else if(rowData.executeType == 4 && !rowData.deviceId){
            extra = '暂无该设备'
            icon = require('../../images/smart/condition_device.png')
        }else if(rowData.executeType == 2 && !rowData.executeIntelligentId){
            extra = '暂无该智能'
            icon = require('../../images/smart/execute_open.png')
        }else if(rowData.executeType == 1){
            extra = rowData.conditionTitle
            if(!rowData.dayIcon){
                icon = this.isDark ? require('../../images/smart/execute_noti_dark.png')
                    : require('../../images/smart/execute_noti.png')
            }
        }

        return(
            <View style={{width:'100%',marginTop: 10,paddingHorizontal: 16,}}>
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={[styles.item,{ backgroundColor:Colors.themeBg }]} 
                    onPress={()=>{
                        this.closeSwiper()
                        this.handleExecuteClick(rowData,index)
                    }}
                >
                    <Image style={{width:30,height:30,resizeMode:'contain'}} source={icon}/>
                    <View style={{marginLeft: 15,flex: 1}}>
                        <Text style={{fontSize:15,color:Colors.themeText}}>{rowData.name}</Text>
                        {room}
                    </View>
                    <Text style={{fontSize:15,color:Colors.themeTextLight}}>{extra}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    // 渲染执行的 listView （列表）
    getExecuteListView(){
        let executeList = this.props.executeList

        if(!executeList || executeList.length<=0){
            return null
        }

        return (
            <SwipeableFlatList
                ref={e => this.swiperFlatlist_execute = e}
                showsVerticalScrollIndicator={false}
                style={{marginTop: 10}}
				contentContainerStyle={{ paddingBottom: 10 }}
				removeClippedSubviews={false}
				data={executeList}
				keyExtractor={this._extraExcuteUniqueKey}
				renderItem={({ item, index }) => this.renderExecuteRowItem(item, index)}
				renderQuickActions={({ item, index }) => this.getQuickActions(item, index)}//创建侧滑菜单 2:执行
				maxSwipeDistance={maxSwipeDistance}
				bounceFirstRowOnMount={false}
				initialNumToRender={5}
			/>
        )
    }

    render() {
        return this.getExecuteListView()
    }
}

const styles = StyleSheet.create({
    //侧滑菜单的样式
	quickAContent: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginRight: '5%',
		marginTop: 10,
        overflow: 'hidden',
        paddingVertical: 1
	},
	quick: {
		backgroundColor: Colors.white,
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
        width: maxSwipeDistance / countSwiper,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
	},
    item:{
        width:'100%',
        height: 60,
        borderRadius: 4,
        paddingHorizontal: 16,
        flexDirection:'row',
        alignItems:'center'
    },
    extra:{
        fontSize:14,
        color:Colors.themeTextLightGray,
        alignSelf:'flex-end'
    },
    subExtra:{
        fontSize:14,
        color:Colors.themeTextLightGray,
        marginTop:5,
        alignSelf:'flex-end'
    },
    roomText:{
        fontSize:12,
        color:Colors.themeTextLightGray,
        marginTop: 5
    }
});

export default SmartExcuteList

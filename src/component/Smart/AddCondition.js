/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    FlatList,
    Alert,
    RefreshControl,
    SwipeableFlatList,
    Dimensions,
    ImageBackground,
    TextInput
} from 'react-native';
import { Colors } from '../../common/Constants';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import { connect } from 'react-redux';

class AddCondition extends Component {
    
    static navigationOptions = ({ navigation }) => {
		return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'添加条件'}/>,
            headerBackground: <HeaderBackground hiddenBorder={true}/>
		}
	}

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation
        this.callBack = getParam('callBack')

        this.conditionRelation = getParam('conditionRelation')
        this.conditionData = getParam('conditionData')
        this.roomId = getParam('roomId')

        this.state = {
           ignore: []  // 1-时间段 2-手机进入或离开某地 3-定时（共用设备场景定时表） 4-设备
        }
    }

    componentDidMount() {
        this.checkConditionRelation()
    }

    componentWillUnmount() {
        
    }

    checkConditionRelation(){
        let ignore = []
        if(!this.conditionRelation || !this.conditionData || this.conditionData.length <1){
            ignore.push({type:1,reason:'不可单独作为条件'})
        }
        // 或者关系
        else if(this.conditionRelation == 2){
            // 不能选择 时间段
            ignore.push({type:1,reason:'时间段不能用于或关系'})
        }
        // 并且 关系 
        else if(this.conditionRelation == 1){
            for (const condition of this.conditionData) {
                let conditionType = condition.conditionType
                // 定时 和 时间段 不能重复 选择
                if(conditionType !=4 ){
                    ignore.push({type:conditionType,reason:'条件冲突，不可选'})
                }
            }
        }
        this.setState({
            ignore: ignore
        })
    }

    getTitle(){
        const Colors = this.props.themeInfo.colors
        return(
            <View style={{width:'100%',alignItems: 'center',marginTop:30}}>
                <Text style={{fontSize:24,fontWeight:'bold',color:Colors.themeText}}>如果满足某个条件时</Text>
            </View>
        )
    }

    renderSplit(){
        const Colors = this.props.themeInfo.colors
        return <View style={[styles.line,{backgroundColor: Colors.split}]}/>
    }

    getChooses(){
        const Colors = this.props.themeInfo.colors
        const {navigate} = this.props.navigation
        // 1-时间段 2-手机进入或离开某地 3-定时（共用设备场景定时表） 4-设备
        let duringIgnore, locationIgnore, timingIgnore = null
        let duringTitleLight, locationTitleLight, timingTitleLight = {}
        for (const ig of this.state.ignore) {
            if(ig.type == 1){
                duringIgnore = <Text style={styles.ignoreIcon}>{ig.reason}</Text>
                duringTitleLight = {color: Colors.themeTextLight}
            }
            if(ig.type == 2){
                locationIgnore = <Text style={styles.ignoreIcon}>{ig.reason}</Text>
                locationTitleLight = {color: Colors.themeTextLight}
            }
            if(ig.type == 3){
                timingIgnore = <Text style={styles.ignoreIcon}>{ig.reason}</Text>
                timingTitleLight = {color: Colors.themeTextLight}
            }
        }
        // 图标
        const locationIcon = this.props.themeInfo.isDark ? require('../../images/smart/condition_phone_location_dark.png')
            : require('../../images/smart/condition_phone_location.png')
        const timingIcon = this.props.themeInfo.isDark ? require('../../images/smart/condition_timing_dark.png')
            : require('../../images/smart/condition_timing.png')
        const duringIcon = this.props.themeInfo.isDark ? require('../../images/smart/condition_during_dark.png')
            : require('../../images/smart/condition_during.png')
        const deviceIcon = this.props.themeInfo.isDark ? require('../../images/smart/condition_device_dark.png')
            : require('../../images/smart/condition_device.png')
        const weatherIcon = this.props.themeInfo.isDark ? require('../../images/smart/outdoor_weather_dark.png')
            : require('../../images/smart/outdoor_weather.png')
        
        return(
            <View style={{paddingHorizontal: 16,marginTop:50}}>
                {/* <TouchableOpacity activeOpacity={0.8} onPress={()=>{
                    if(locationIgnore){
                        return
                    }
                    ToastManager.show('敬请期待')
                }}>
                    <View style={styles.itemTouch}>
                        <Image style={styles.icon} source={locationIcon}/>
                            <Text style={[styles.itemText,{color: Colors.themeText},locationTitleLight]}>手机进入或离开某地</Text>
                        {locationIgnore}
                        {locationIgnore ? null : <Image style={styles.arrow} source={require('../../images/enterLight.png')}/>}
                    </View>
                    <Text style={[styles.tips,{color: Colors.themeTextLight}]}>开启实时定位功能，选择当前使用手机进入或离开的某个地点作为执行条件</Text>
                </TouchableOpacity> */}
                
                <TouchableOpacity activeOpacity={0.8} onPress={()=>{
                    navigate('SmartOutdoorWeather', {callBack:(data)=>{ this.callBack(data) }})
                }}>
                    <View style={styles.itemTouch}>
                        <Image style={styles.icon} source={weatherIcon}/>
                            <Text style={[styles.itemText,{color: Colors.themeText},locationTitleLight]}>室外天气</Text>
                        {locationIgnore}
                        {locationIgnore ? null : <Image style={styles.arrow} source={require('../../images/enterLight.png')}/>}
                    </View>
                    <Text style={[styles.tips,{color: Colors.themeTextLight}]}>选择一个室外天气作为执行条件，例: 室外天气是 雨天。</Text>
                </TouchableOpacity>

                {this.renderSplit()}
                <TouchableOpacity activeOpacity={0.8} onPress={()=>{
                    if(timingIgnore){
                        return
                    }
                    navigate('SmartTiming',{callBack:(data)=>{ this.callBack(data) }})
                }}>
                    <View style={styles.itemTouch}>
                        <Image style={styles.icon} source={timingIcon}/>
                        <Text style={[styles.itemText,{color: Colors.themeText},timingTitleLight]}>定时</Text>
                        {timingIgnore}
                        {timingIgnore ? null : <Image style={styles.arrow} source={require('../../images/enterLight.png')}/>}
                    </View>
                    <Text style={[styles.tips,{color: Colors.themeTextLight}]}>选择一个时间点作为执行条件，例：每天9 : 00 </Text>
                </TouchableOpacity>
                {this.renderSplit()}
                <TouchableOpacity activeOpacity={0.8} onPress={()=>{
                    if(duringIgnore){
                        return
                    }
                    navigate('SmartDuring',{callBack:(data)=>{ this.callBack(data) }})
                }}>
                    <View style={styles.itemTouch}>
                        <Image style={styles.icon} source={duringIcon}/>
                        <Text style={[styles.itemText,{color: Colors.themeText},duringTitleLight]}>时间段</Text>
                        {duringIgnore}
                        {duringIgnore ? null : <Image style={styles.arrow} source={require('../../images/enterLight.png')}/>}
                    </View>
                    <Text style={[styles.tips,{color: Colors.themeTextLight}]}>选择一个时间段作为执行条件，例：每周一至周五 9 : 00 - 12 : 00</Text>
                </TouchableOpacity>
                {this.renderSplit()}
                <TouchableOpacity activeOpacity={0.8} onPress={()=>{
                    navigate('SmartDevices',{roomId: this.roomId,callBack:(data)=>{ this.callBack(data) }})
                }}>
                    <View style={styles.itemTouch}>
                        <Image style={styles.icon} source={deviceIcon}/>
                        <Text style={[styles.itemText,{color: Colors.themeText}]}>全部设备</Text>
                        <Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
                    </View>
                    <Text style={[styles.tips,{color: Colors.themeTextLight}]}>选择一个设备的属性或数值作为执行条件，例：环境监测PM2.5浓度高于150时</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors
        
        return (
            <ScrollView style={[styles.container,{backgroundColor: Colors.themeBg}]}>
                {this.getTitle()}
                {this.getChooses()}
            </ScrollView> 
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    line:{
        height:1,
        backgroundColor:Colors.splitLightGray,
        flex:1,
        marginLeft: 40,
    },
    itemTouch:{
        paddingVertical: 5,
        alignItems:'center',
        flexDirection: 'row'
    },
    tips:{
        fontSize: 12,
        lineHeight: 15,
        marginLeft: 58,
        marginRight: 20,
        marginBottom: 10
    },
    icon:{
        width:38,
        height:38,
        resizeMode:'contain'
    },
    itemText:{
        marginLeft: 20, 
        fontSize: 18, 
        flex:1
    },
    ignoreIcon:{
        marginRight:10,
        fontSize:13,
        color:Colors.themeTextLightGray
    },
    arrow:{
        width:8,
        height:14,
        resizeMode:'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(AddCondition)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    FlatList,
    Alert,
    NativeModules,
    TextInput,
    Modal,
    Keyboard,
    BackHandler,
    Linking,
    AppState
} from 'react-native';
import {Colors, NotificationKeys, NetUrls} from '../../common/Constants';
import ActionSheet from 'react-native-actionsheet'
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import {BottomSafeMargin,StatusBarHeight} from '../../util/ScreenUtil';
import SmartConditionList from './AddSmart_ConditionList';
import SmartExcuteList from './AddSmart_ExcuteList';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import {ImagesDark, ImagesLight} from '../../common/Themes';
import SwitchButton from '../../common/CustomComponent/SwitchButton';


const JKRNUtils = Platform.select({
    ios: NativeModules.JKRNUtils,
    android: NativeModules.JKCameraAndRNUtils
});

class AddSmart extends Component {

    static navigationOptions = ({ navigation }) => {
        const {getParam} = navigation
        let collectIcon = navigation.getParam('isCollect') ? require('../../images/collect/collect_blue.png') 
            : require('../../images/collect/collect_border.png')

        return {
            headerLeft: (
                <HeaderLeft 
                    navigation={navigation} 
                    backTitle={getParam('title')} 
                    onClick={()=>{
                        getParam('goBack') && getParam('goBack')()
                    }}
                />
            ),
            headerRight: ( getParam('id') ?
                <HeaderRight buttonArrays={[
                    {icon: collectIcon, tintColor: {}, onPress:()=>{
                        getParam('collectBtnClick')()
                    }},
                ]}/> : null
            ),
            headerBackground: <HeaderBackground/>
        }
    };

    constructor(props) {
        super(props);
        const {getParam, setParams} = this.props.navigation
        this.roomId = getParam('roomId')
        this.id = getParam('id') || 0
        this.isRmdSmart = getParam('isRmdSmart')

        this.state = {
            appState: AppState.currentState,
            name: null,
            conditionRelation: 0, // 条件的关系 1:且  2:或
            conditionList: [],
            executeList: [],
            beginDate: null,
            endDate: null,
            conditionModalVisible: false,
            excuteModalVisible: false,
            isCollect: getParam('isCollect'),
            isEditing: false,
            isNoticeOpened: false,
            isSendJpush: 0,
            conditionLimit: 10
        }

        setParams({
            collectBtnClick: ()=>{
                this.collect()
            },
            goBack: this.goBack.bind(this)
        })
    }

    componentDidMount() {
        this.requestSmartDetail()
        this.checkNotificationSwitch()
        BackHandler.addEventListener('hardwareBackPress',this.onBackButtonPressAndroid);
        AppState.addEventListener("change", this._handleAppStateChange);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
        AppState.removeEventListener("change", this._handleAppStateChange);
    }
    
    // 安卓物理返回按钮
    onBackButtonPressAndroid = ()=>{
        if(this.state.isEditing){
            this.goBack()
            return true
        }
		return false
	}

    // 返回
    goBack(){
        const { pop } = this.props.navigation

        if(this.state.isEditing){
            Alert.alert(
                '提示',
                '你有编辑未保存，是否需要保存后退出？',
                [
                    { text: '直接退出', onPress: () => { pop() }},
                    { text: '保存后退出', onPress: () => { this.requestSaveSmartDetail() }},
                    { text: '取消', onPress: () => { }, style: 'cancel'},
                ]
            ) 
        }else{
            pop()
        }
    }

    // 处理App状态
    _handleAppStateChange = nextAppState => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            this.checkNotificationSwitch()
        }
        this.setState({ appState: nextAppState })
    }

    // 监测系统通知开关
    checkNotificationSwitch(){
        // 监测App 推送权限是否开启
        JKRNUtils.getSystemNoticeStatus().then((isOpen) => {
            this.setState({isNoticeOpened: isOpen})
        }).catch((e) => {
            console.log('getSystemNoticeStatus error', e)
        });
    }

    // 智能详情
    async requestSmartDetail() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.smartDetail,
                params: {
                    intelligentId: this.id,
                    isRecommand: this.isRmdSmart ? 1:0
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                let smartData = data.result || {}
                if(this.id == 0){
                    this.setState({
                        conditionLimit: smartData.conditionLimit
                    })
                    return
                }
                this.setState({
                    name: smartData.name,
                    conditionList: smartData.condition || [],
                    executeList: smartData.execute || [],
                    beginDate: smartData.startDate,
                    endDate: smartData.endDate,
                    conditionRelation: smartData.conditionRelation,
                    isSendJpush: smartData.sendJpush,
                    conditionLimit: smartData.conditionLimit
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

    // 保存 智能详情
    async requestSaveSmartDetail() {
        if(!this.state.name){
            ToastManager.show('请输入智能名称')
            return
        }
        if(!this.state.conditionList || this.state.conditionList.length<=0){
            ToastManager.show('至少选择一个条件')
            return
        }
        if(!this.state.executeList || this.state.executeList.length<=0){
            ToastManager.show('至少执行一个任务')
            return
        }
        let params = {}
        if(this.id && !this.isRmdSmart){
            params.intelligentId = this.id
        }
        if(this.roomId){
            params.roomId = this.roomId
        }
        if(this.state.conditionList.length > 1){
            params.conditionRelation = this.state.conditionRelation
        }
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.smartSave,
                params: {
                    ...params,
                    name: this.state.name,
                    startDate: this.state.beginDate,
                    endDate: this.state.endDate,
                    condition: this.state.conditionList,
                    execute: this.state.executeList,
                    sendJpush: this.state.isSendJpush ? 1:0
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                const {pop} = this.props.navigation
                DeviceEventEmitter.emit(NotificationKeys.kSmartListChangeNotification)
                if(this.id){
                    ToastManager.show('智能保存成功')
                }else{
                    ToastManager.show('智能创建成功')
                }
                if(this.isRmdSmart){
                    pop(2)
                }else{
                    pop()
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

    async collect(){
        const { setParams } = this.props.navigation;
        let target = !this.state.isCollect
        try {
			let data = await postJson({
				url: NetUrls.collect,
				params: {
                    ids: this.id,
                    type: 3,
                    isAdd: target
                }
            });
			if (data.code == 0) {
                let tips = target ? '收藏成功' : '取消收藏'
                this.state.isCollect = target
                setParams({isCollect: target})
                ToastManager.show(tips)
                DeviceEventEmitter.emit(NotificationKeys.kSmartListChangeNotification)
			} else {
				ToastManager.show(data.msg)
			}
		} catch (error) {
			ToastManager.show('网络错误')
		}
    }

    // 用于选择 与或关系的 底部弹框
    getActionSheet() {
		return (
			<ActionSheet
				ref={e => this.conditionRelationActionSheet = e}
				title={'请选择条件关系'}
				options={['如果条件同时满足时', '如果条件任一满足时', '取消']}
				cancelButtonIndex={2}
				onPress={(index) => {
                    if(index == 2) return
                    this.setState({
                        conditionRelation: index+1
                    },()=>{
                        this.gotoAddConditions()
                    })
				}}
			/>
		)
    }
    
	getModal() {
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.conditionModalVisible || this.state.excuteModalVisible}
                onShow={() => {
                    //this.showDoublePicker()
                    if(this.state.conditionModalVisible){
                        this.condition_list?.showDoublePicker()
                    }else if(this.state.excuteModalVisible){
                        this.excute_list?.showDoublePicker()
                    }
                }}
                onRequestClose={() => {
                }}
            >
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        excuteModalVisible: false,
                        conditionModalVisible: false,
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
	}
	
    // 输入名称的输入框
    getNameInput(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={{width:'100%',paddingHorizontal: 16,marginTop:10}}>
                <View style={[styles.inputWrapper,{backgroundColor: Colors.themeBg}]}>
                    <TextInput
                        clearButtonMode={'while-editing'}
                        style={[styles.input,{color: Colors.themeText}]}
                        defaultValue={this.state.name}
                        placeholder={'请输入智能名称（最多不超过8个字）'}
                        placeholderTextColor={Colors.themeInactive}
                        onChangeText={(text) => {
							this.setState({
								name: text,
                                isEditing: true
							})
						}}
						returnKeyType={'done'}
                    />
                </View>
            </View>
        )
    }

    //关闭侧滑栏
	closeSwiper() {
        this.condition_list?.closeSwiper()
        this.excute_list?.closeSwiper()
    }
    
    // 进入添加条件 页面
    gotoAddConditions(){
        if(this.state.conditionList && this.state.conditionLimit && this.state.conditionList.length >= this.state.conditionLimit){
            ToastManager.show('条件个数已达上限')
            return
        }
        const {navigate} = this.props.navigation
        navigate('AddCondition',{
            roomId: this.roomId,
            conditionRelation: this.state.conditionRelation,
            conditionData: this.state.conditionList && JSON.parse(JSON.stringify(this.state.conditionList)),
            callBack:(data)=>{
                console.log(data);
                this.state.conditionList.push(data)
                this.setState({
                    conditionList: this.state.conditionList,
                    isEditing: true
                })
            }
        })
    }

    // 进入执行条件 页面
    gotoExecuteConditions(){
        const {navigate} = this.props.navigation
        navigate('AddExecute',{
            roomId: this.roomId,
            //conditionRelation: this.state.conditionRelation,
            intelligentId:this.id,
            executeList: this.state.executeList && JSON.parse(JSON.stringify(this.state.executeList)),
            callBack:(data)=>{
                console.log(data);
                this.state.executeList.push(data)
                this.setState({
                    executeList: this.state.executeList,
                    isEditing: true
                })
            }
        })
    }
    
    // 渲染条件的总View
    getConditionView(){
        const Colors = this.props.themeInfo.colors

        let relationIcon = null
        let relationText = ''
        if(this.state.conditionRelation ==1 && this.state.conditionList.length > 1){
            relationText = '以下条件同时满足'
            relationIcon = require('../../images/smart/and.png')
            
        }else if(this.state.conditionRelation ==2 && this.state.conditionList.length > 1){
            relationText = '以下条件任一满足'
            relationIcon = require('../../images/smart/or.png')
        }

        let addTextView = <Text style={{fontSize:15,color:Colors.themeButton,fontWeight:'bold'}}>+添加条件</Text>
        if(this.state.conditionLimit && this.state.conditionList && this.state.conditionList.length >= this.state.conditionLimit){
            addTextView = <Text style={{fontSize:14,color:Colors.themeTextLight}}>条件个数已达上限</Text>
        }

        return(
            <View style={styles.conditionWrapper}>
                <View style={styles.conditionTitleWrapper}>
                    <Text style={{fontSize:18,fontWeight:'bold',color:Colors.themeText}}>如果</Text>
                    <View style={{flexDirection: 'row',alignItems:'center'}}>
                        <Image style={{width:23,height:23,resizeMode:'contain'}} source={relationIcon}/>
                        <Text style={{fontSize:13,color:Colors.themeButton,marginLeft:5}}>{relationText}</Text>
                    </View>
                </View>
                <SmartConditionList
                    ref={e => this.condition_list = e}
                    isDark = {this.props.themeInfo.isDark}
                    conditionList={this.state.conditionList}
                    navigation = {this.props.navigation}
                    updateConditionCallBack ={ (conditionList)=>{
                        this.setState({
                            conditionList: conditionList,
                            isEditing: true
                        })
                    }}
                    closePicker = {()=>{
                        this.setState({conditionModalVisible: false})
                    }}
                    openPicker = {()=>{
                        this.setState({conditionModalVisible: true})
                    }}
                />
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.addTouch,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        this.closeSwiper()
                        if(this.state.conditionList.length == 1){
                            this.conditionRelationActionSheet?.show()
                        }else{
                            this.gotoAddConditions()
                        }
                    }}
                >
                    {addTextView}
                </TouchableOpacity>
            </View>
            
        )
    }
    
    // 渲染执行的总View
    getExecuteView(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={styles.conditionWrapper}>
                <Text style={{fontSize:18,fontWeight:'bold',color:Colors.themeText,marginLeft: 16}}>执行</Text>
                <SmartExcuteList
                    ref = {e => this.excute_list = e}
                    isDark = {this.props.themeInfo.isDark}
                    executeList={this.state.executeList}
                    navigation = {this.props.navigation}
                    updateExcuteCallBack ={ (executeList)=>{
                        this.setState({
                            executeList: executeList,
                            isEditing: true
                        })
                    }}
                    closePicker = {()=>{
                        this.setState({excuteModalVisible: false})
                    }}
                    openPicker = {()=>{
                        this.setState({excuteModalVisible: true})
                    }}
                />
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.addTouch,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        this.closeSwiper()
                        this.gotoExecuteConditions()
                    }}
                >
                    <Text style={{fontSize:15,color:Colors.themeButton,fontWeight:'bold'}}>+添加执行 </Text>
                </TouchableOpacity>
            </View>
        )
    }

    // 生效时间按钮点击事件
    effectTimeClick(){
        const {navigate} = this.props.navigation
        this.closeSwiper()
        navigate('EffectTime',{
            beginDate: this.state.beginDate,
            endDate: this.state.endDate,
            callBack: (beginDate,endDate)=>{
                this.setState({
                    beginDate: beginDate,
                    endDate: endDate,
                    isEditing: true
                })
            }
        })
    }

    // 执行时间按钮
    getEffectTimeView(){
        const Colors = this.props.themeInfo.colors
        let effectTimeView = <Text style={styles.effectTimeText}>永久</Text>

        if(this.state.beginDate && this.state.endDate){
            let beginDate = this.state.beginDate
            let endDate = this.state.endDate
            effectTimeView = (
                <Text style={styles.effectTimeText}>{beginDate + ' 至 ' + endDate}</Text>
            )
        }else if(this.state.beginDate && !this.state.endDate){
            let beginDate = this.state.beginDate
            effectTimeView = (
                <Text style={styles.effectTimeText}>{beginDate + ' 开始 '}</Text>
            )
        }
        else if(!this.state.beginDate && this.state.endDate){
            let endDate = this.state.endDate
            effectTimeView = (
                <Text style={styles.effectTimeText}>{endDate + ' 结束 '}</Text>
            )
        }
        return(
            <View style={styles.effectTimeBtn}>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.bottomTouch,{backgroundColor: Colors.themeBg}]} 
                    onPress={() => {
                        this.effectTimeClick()
				    }}
                >
					<Text style={{ color: Colors.themeText,fontSize:15,flex: 1 }}>生效日期</Text>
                    {effectTimeView}
                    <Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
				</TouchableOpacity>
			</View>
        )
    }

    // 通知推送开关
    getNotificationSwitch(){
        const Colors = this.props.themeInfo.colors
        let notiBtn = null
        if(this.state.isNoticeOpened){
            notiBtn = (
                <SwitchButton
                    style = {styles.notificationTouch}
                    value = {this.state.isSendJpush}
                    onPress = {()=>{
                        this.setState({
                            isSendJpush : this.state.isSendJpush ? 0:1,
                            isEditing: true
                        })
                    }}
                />
            )
        }else{
            notiBtn = (
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.notificationTouch}
                    onPress={()=>{
                        if(this.state.isNoticeOpened == true){
                            return
                        }
                        if(Platform.OS == 'ios'){
                            Linking.openURL('app-settings:')
                        }else{
                            JKRNUtils.openSystemNoticeView();
                        }
                    }}
                >
                    <Text style={{fontSize: 13,color: Colors.themeTextLight}}>您暂未开启权限，去开启</Text>
                    <Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
                </TouchableOpacity>
            )
        }

        return(
            <View style={[styles.notificationWrapper,{backgroundColor: Colors.themeBg}]}>
				<Text style={{ color: Colors.themeText,fontSize:15 }}>向手机发送通知</Text>
                {notiBtn}
			</View>
        )
    }

    // 获取底部 保存按钮
    getBottomSaveButton(){
        let saveText = (this.isRmdSmart || !this.id) ? '创建' : '保存'
        return(
            <View style={{paddingBottom: BottomSafeMargin}}>
                <TouchableOpacity style={styles.saveTouch} onPress={()=>{
                    Keyboard.dismiss()
                    this.requestSaveSmartDetail()
                }}>
                    <Text style={{color:Colors.white,fontSize: 16}}>{saveText}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
                <ScrollView 
                    contentContainerStyle={styles.scrollContent}
                    keyboardDismissMode={'on-drag'}
                    keyboardShouldPersistTaps={'handled'}
                >
                    {this.getNameInput()}
                    {this.getEffectTimeView()}
                    {this.getNotificationSwitch()}
                    {this.getConditionView()}
                    {this.getExecuteView()}
                </ScrollView>
                {this.getBottomSaveButton()}
                {this.getActionSheet()}
                {this.getModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.themBGLightGray,
        flex: 1
    },
    scrollContent:{
        paddingBottom: BottomSafeMargin + 50,
    },
    inputWrapper:{
        width:'100%',
        borderRadius: 4,
        paddingHorizontal: 16
    },
    input:{
        height:50,
        width:'100%',
        fontSize:16,
        fontWeight: 'bold'
    },
    conditionWrapper:{
        marginTop:20,
        width:'100%'
    },
    addTouch:{
        marginTop:20,
        height: 44,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        marginHorizontal: 16,
    },
    effectTimeText:{
        fontSize:13,
        color:Colors.themeTextLightGray,
    },
    effectTimeBtn:{
        width: '100%',
        marginTop: 10,
        paddingHorizontal: 16
    },
    bottomTouch:{
        height: 50,
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 16,
        borderRadius: 4
    },
    modalTouchable:{ 
        width: '100%', 
        height: '100%', 
        backgroundColor: Colors.translucent
    },
    saveTouch:{
        backgroundColor: Colors.tabActiveColor,
        marginHorizontal: '5%',
        marginVertical: 10,
        height:40,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    conditionTitleWrapper: {
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal: 16
    },
    notificationWrapper:{
        height: 50,
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: 16,
        borderRadius: 4,
        marginTop: 10,
        paddingLeft: 16
    },
    notificationTouch:{
        flex: 1,
        flexDirection: 'row',
        height: '100%',
        alignItems:'center',
        justifyContent:'flex-end',
        paddingRight: 16
    },
    arrow:{
        marginLeft: 8,
        width:8,
        height:14,
        resizeMode:'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(AddSmart)

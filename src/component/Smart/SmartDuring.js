/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Image,
	Text,
	View,
	TouchableOpacity,
	ScrollView,
	Dimensions,
} from 'react-native';

import { Colors,NetUrls } from '../../common/Constants';
import ToastManager from '../../common/CustomComponent/ToastManager';
import { postJson } from '../../util/ApiRequest';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import {BottomSafeMargin} from '../../util/ScreenUtil';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import {ImagesLight, ImagesDark} from '../../common/Themes';
import SwitchButton from '../../common/CustomComponent/SwitchButton';
// import DatePicker from 'react-native-datepicker';
import DatePicker from '../../third/Datepicker/datepicker';

const screenW = Dimensions.get('window').width;
const dayTouchW = 32

class SmartDuring extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'时间段'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text:'保存', onPress:()=>{
                        navigation.getParam('saveBtnClick')()
                    }}
                ]}/>
            ),
            headerBackground: <HeaderBackground/>
        }
    };
	
	constructor(props) {
		super(props);
		const {getParam,setParams} = this.props.navigation
		this.callBack = getParam('callBack')
		this.duringData = getParam('duringData') || {}
		
		this.state = {
			days:[
				// {id:0,name:'每',value:'0'},
				{id:1,name:'一',value:'2'},
				{id:2,name:'二',value:'3'},
				{id:3,name:'三',value:'4'},
				{id:4,name:'四',value:'5'},
				{id:5,name:'五',value:'6'},
				{id:6,name:'六',value:'7'},
				{id:7,name:'日',value:'1'},
			],
			
			beginTime: this.duringData.startTime,
			endTime: this.duringData.endTime,
			isAllDay: this.duringData.wholeDay,
			selects: this.getDefaultSelects(),
        }
        
        setParams({
            saveBtnClick: ()=>{ this.save() }
        })
	}

	//处理默认的选中 周期
	getDefaultSelects(){
		if(!this.duringData.cyclic){
			return []
		}
		let selectArr = []
		selectArr = this.duringData.cyclic.split(',')
		if(Array.isArray(selectArr)){
			console.log( selectArr );
			return selectArr
		}else{
			return []
		}
	}

	getDefaultTime(){
		let nowDate = new Date()
		// 分钟加 0
		let minu = nowDate.getMinutes()
		if (minu < 10) {
			minu = '0' + minu
		}
		return nowDate.getHours() + ':' + minu
	}
 
	componentDidMount() {
		
	}

	componentWillUnmount(){
		
	}

	save(){
		if(this.state.selects.length <= 0){
			ToastManager.show('请选择周期')
			return
		}
		const {pop} = this.props.navigation

		let selected = this.state.selects.concat()
		selected = selected.sort()

		// 此数据 必须和 接口保持一致
		let data = {}
		if(this.duringData && this.duringData.conditionType){
			data = JSON.parse(JSON.stringify(this.duringData))
		}
		data.wholeDay = this.state.isAllDay ? 1:0
		data.cyclic = selected.toString()
		data.startTime = this.state.beginTime
		data.endTime = this.state.endTime
		data.conditionType = 1

		this.callBack && this.callBack(data)
		if(this.duringData && this.duringData.conditionType){
			pop(1)
		}else{
			pop(2)
		}
	}

	// 存储 定时
	async requestSaveTimer(hour,minute,cyclic){
		const {pop} = this.props.navigation
		let params = {}
		if(this.sceneId){
			params.sceneId = this.sceneId
		}
		if(this.deviceId){
			params.deviceId = this.deviceId
		}
		if(this.timerId){
			params.id = this.timerId
		}
		if(this.uuid){
			params.uuid = this.uuid
		}
		SpinnerManager.show()
		try {
			let data = await postJson({
				url: NetUrls.addTimer,
				params: {
					...params,
					hour: hour,
					minute: minute,
					cyclic: cyclic,
					status: this.status,
					operation: this.state.operation
				}
			});
			SpinnerManager.close()
			if (data && data.code == 0) {
				this.callBack && this.callBack()
				pop()
				ToastManager.show('创建成功')
			}else{
				ToastManager.show(data.msg)
			}
		} catch (error) {
			SpinnerManager.close()
			ToastManager.show(JSON.stringify(error))
		}
    }

    // 开始时间点击
    beginTimeClick = (time) => {
        this.setState({
            beginTime: time,
        })
        // 用于判断开始与结束时间
        // if(!this.state.endTime){
        //     this.setState({
        //         beginTime: time,
        //     })
        //     return
        // }
        // let beginHour = time.split(':')[0]
        // let beginMinu = time.split(':')[1]
        // let endHour = this.state.endTime.split(':')[0]
        // let endMinu = this.state.endTime.split(':')[1]
        // if( parseInt(endHour) > parseInt(beginHour) ){
        //     this.setState({
        //         beginTime: time,
        //     })
        // }else if( parseInt(endHour) == parseInt(beginHour) ){
        //     if(parseInt(endMinu) >= parseInt(beginMinu)){
        //         this.setState({
        //             beginTime: time,
        //         })
        //     }else{
        //         ToastManager.show('开始时间不能大于结束时间')
        //         return
        //     }
        // }else{
        //     ToastManager.show('开始时间不能大于结束时间')
        //     return
        // }
    }

    endTimeClick = (time) => {
        this.setState({
            endTime: time,
        })
        // 用于判断开始与结束时间
        // if(!this.state.beginTime){
        //     this.setState({
        //         endTime: time,
        //     })
        //     return
        // }
        // let beginHour = this.state.beginTime.split(':')[0]
        // let beginMinu = this.state.beginTime.split(':')[1]
        // let endHour = time.split(':')[0]
        // let endMinu = time.split(':')[1]
        // if( parseInt(endHour) > parseInt(beginHour) ){
        //     this.setState({
        //         endTime: time,
        //     })
        // }else if( parseInt(endHour) == parseInt(beginHour) ){
        //     if(parseInt(endMinu) >= parseInt(beginMinu)){
        //         this.setState({
        //             endTime: time,
        //         })
        //     }else{
        //         ToastManager.show('结束时间不能小于开始时间')
        //         return
        //     }
        // }else{
        //     ToastManager.show('结束时间不能小于开始时间')
        //     return
        // }
    }
    
    // 工作日 每天
    getWeekDay(){
        const Colors = this.props.themeInfo.colors
		const spaceWidth = (screenW-16*2-dayTouchW*7)/6
		const weekdayW = 5*dayTouchW+spaceWidth*4
		const holidayW = 2*dayTouchW+spaceWidth

		return(
			<View style={styles.headWrapper}>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.weekDayTouch,{width:weekdayW, backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
					    this.setState({
						    selects: ['2','3','4','5','6']
					    })
				    }}
                >
					<Text style={{color:Colors.themeText}}>工作日</Text>
				</TouchableOpacity>
                <TouchableOpacity 
                    activeOpacity={0.7} 
                    style={[styles.weekDayTouch,{marginLeft: 20, width: holidayW, backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
					    this.setState({
						    selects: ['1','2','3','4','5','6','7']
					    })
				    }}
                >
					<Text style={{color: Colors.themeText,fontSize:15}}>每天</Text>
				</TouchableOpacity>
			</View>
		)
	}

    // 一二三四五六日
	getIndicatorHead(){
        const Colors = this.props.themeInfo.colors

		let list = this.state.days.map((val,index)=>{
			let i = this.state.selects.indexOf(val.value)
			let selectBg = i == -1 ? {backgroundColor: Colors.themeBg} : {backgroundColor: Colors.themeButton}
			let selectColor = i == -1 ? {color: Colors.themeTextLight} : {color:Colors.white}
			return(
				<TouchableOpacity key={'timing_index_'+index} style={[styles.dayTouch,selectBg]} onPress={()=>{					
					if(i == -1){
						this.state.selects.push(val.value)
					}else{
						this.state.selects.splice(i,1)
					}
					this.setState({
						selects: this.state.selects
					})
				}}>
					<Text style={[{fontSize:15},selectColor]}>{val.name}</Text>
				</TouchableOpacity>
			)
		})

		return(
			<View style={styles.daysWrapper}>
				{list}
			</View>
		)
	}


	// 时间
	getTimeItem(refName,time,callBack){
        const Colors = this.props.themeInfo.colors

		return (
			<DatePicker
				ref={refName}
				style={{flex:1}}
				date={time}
				mode="time"
				format="HH:mm"
				confirmBtnText="确定"
				cancelBtnText="取消"
				showIcon={false}
				androidMode={'spinner'}
				customStyles={{
					dateInput: {
						borderWidth: 0,
						alignItems:'flex-end',
						justifyContent:'center',
                        paddingRight:15
					},
					dateText: {
						color: time ? Colors.themeText : Colors.themeBg,
						fontSize: 15,
						textAlign:'right'
					}
				}}
				onDateChange={(time) => {
					callBack(time)
				}}
			/>
		)
	}

	// 时间
	getTimePicker(){
        const Colors = this.props.themeInfo.colors
        const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight

		let beginEndView = null
		let switchIcon = Images.switchOn
		if(!this.state.isAllDay){
            switchIcon = Images.switchOff
            
			beginEndView = (
				<>
				    <View style={[styles.line,{backgroundColor: Colors.themeBaseBg}]}/>
                    <TouchableOpacity activeOpacity={0.7} style={styles.inputWrapper} onPress={()=>{
						this.refs.time_picker_begin && this.refs.time_picker_begin.onPressDate()
					}}>
						<Text style={{color:Colors.themeText,fontSize:15}}>开始时间</Text>
						{this.getTimeItem('time_picker_begin',this.state.beginTime,this.beginTimeClick.bind(this))}
						<Image style={styles.rightArrow} source={require('../../images/enter_light.png')} />
					</TouchableOpacity>
					<View style={[styles.line,{backgroundColor: Colors.themeBaseBg}]}/>
					<TouchableOpacity activeOpacity={0.7} style={styles.inputWrapper} onPress={()=>{
						this.refs.time_picker_end && this.refs.time_picker_end.onPressDate()
					}}>
						<Text style={{color:Colors.themeText,fontSize:15}}>结束时间</Text>
						{this.getTimeItem('time_picker_end',this.state.endTime,this.endTimeClick.bind(this))}
						<Image style={styles.rightArrow} source={require('../../images/enter_light.png')} />
					</TouchableOpacity>
			    </>
			)
        }
        
		return(
			<View style={{paddingVertical:5,paddingHorizontal:16}}>
				<View style={[styles.wrapper,{backgroundColor:Colors.themeBg}]}>
					<TouchableOpacity activeOpacity={0.7} style={styles.inputWrapper} onPress={()=>{
						this.setState({
							isAllDay: !this.state.isAllDay
						})
					}}>
						<Text style={{fontSize:15,color:Colors.themeText,flex:1}}>全天</Text>
						<Image style={{width:46,height:28,resizeMode:'contain'}} source={switchIcon}/>
					</TouchableOpacity>
					{beginEndView}
				</View>
			</View>
		)
	}

	render() {
        const Colors = this.props.themeInfo.colors

		return (
			<ScrollView style={[styles.container,{backgroundColor: Colors.themeBaseBg}]}>
				{this.getWeekDay()}
				{this.getIndicatorHead()}
				{this.getTimePicker()}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.themBGLightGray,
		flex:1,
	},
	scrollContent:{
		paddingBottom: 30,
	},
	inputWrapper:{
		width: '100%',
		height: 60,
		borderRadius:5,
		alignItems:'center',
		flexDirection: 'row',
		paddingHorizontal: 16
	},
	navTouch: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 44,
		paddingRight: 15,
		paddingLeft: 10,
		marginRight:5
	},
	navText:{
		color:Colors.tabActiveColor,
		fontSize:15
	},
	dayTouch:{
		width:dayTouchW,
		height:dayTouchW,
		borderRadius:4,
		justifyContent:'center',
		alignItems:'center'
	},
	bottomBtnWrapper: {
		position: 'absolute',
		left: 0,
		bottom: BottomSafeMargin,
		width: '100%',
		paddingHorizontal: 16,
		paddingVertical: 10
	},
	deleteTouch: {
		width: '100%',
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: Colors.themBgRed,
		borderRadius: 4,
		
	},
	operationWrapper:{
		width:'50%',
		height:30,
		justifyContent:'center',
		alignItems:'center',
	},
	weekDayTouch:{
		height:38,
		justifyContent: 'center',
		alignItems:'center',
		backgroundColor:Colors.white,
		borderRadius: 4
	},
	wrapper:{
		marginTop:20,
		width:'100%',
        borderRadius: 5
	},
	line:{
		width:'100%',
		height:1
    },
    headWrapper:{
        width:'100%',
        marginTop: 20,
        paddingHorizontal: 16, 
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    daysWrapper:{
        paddingHorizontal:16,
        marginTop:20,
        flexDirection:'row',
        justifyContent:'space-between'
    },
    rightArrow:{ 
        marginLeft: 10, 
        width: 7, 
        height: 13, 
        marginRight: 10,
        resizeMode:'contain' 
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(SmartDuring)

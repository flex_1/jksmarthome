/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import { Colors, NotificationKeys, NetUrls } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import {StatusBarHeight} from '../../util/ScreenUtil';
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import { connect } from 'react-redux';

class AddExecute extends Component {

    static navigationOptions = ({ navigation }) => {
		return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'添加执行'}/>,
            headerBackground: <HeaderBackground hiddenBorder={true}/>
		}
	}

    constructor(props) {
        super(props);
        const {getParam} = this.props.navigation
        this.callBack = getParam('callBack')
        this.executeList = getParam('executeList')
        this.roomId = getParam('roomId')
        this.state = {
            intelligentId:getParam('intelligentId'),
            ishiddenNotify: this.getNotifyInfo()
        }
    }

    getNotifyInfo(){
        let ishiddenNotify = false
        if(this.executeList && this.executeList.length >= 0){
            for (const execute of this.executeList) {
                if(execute.executeType == 1){
                    ishiddenNotify = true
                    continue
                }
            }
        }
        return ishiddenNotify
    }

    componentDidMount() {
        
    }

    componentWillUnmount() {
        
    }

    // 获取有场景的楼层
    async getSceneFloor() {
        try {
            let data = await postJson({
                url: NetUrls.sceneFloor,
                params: {
                }
            });
            if (data.code == 0) {
                let floors = [{ floor: 0 }]
                if (data.result && data.result.length > 0) {
                    floors = floors.concat(data.result)
                }
                this.setState({
                    floors: floors,
                    
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    getHeaderName(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={{width:'100%',paddingHorizontal: 16,marginTop:20,alignItems:'center'}}>
                <Text style={[styles.topTitleStyle,{color: Colors.themeText}]}>如果满足条件就执行什么</Text>
            </View>
        )
    }

    renderSplit(){
        const Colors = this.props.themeInfo.colors
        return <View style={[styles.line,{backgroundColor: Colors.split}]}/>
    }

    getChooses(){
        const Colors = this.props.themeInfo.colors
        const {navigate} = this.props.navigation

        // 图标
        const smartIcon = this.props.themeInfo.isDark ? require('../../images/smart/execute_open_dark.png')
            : require('../../images/smart/execute_open.png')
        const sceneIcon = this.props.themeInfo.isDark ? require('../../images/smart/execute_sence_dark.png')
            : require('../../images/smart/execute_sence.png')
        const deviceIcon = this.props.themeInfo.isDark ? require('../../images/smart/condition_device_dark.png')
            : require('../../images/smart/condition_device.png')
        const notifyIcon = this.props.themeInfo.isDark ? require('../../images/smart/execute_noti_dark.png')
            : require('../../images/smart/execute_noti.png')

        return(
            <View style={{paddingHorizontal: 16,marginTop:50}}>
                <TouchableOpacity activeOpacity={0.8} style={styles.itemTouch} onPress={()=>{
                    navigate('ExecuteSmart',{
                        intelligentId:this.state.intelligentId,
                        type:2,
                        roomId: this.roomId,
                        callBack:(data)=>{ this.callBack(data) }
                    })
                }}>
                    <Image style={styles.icon} source={smartIcon}/>
                    <Text style={[styles.itemText,{color:Colors.themeText}]}>全部智能</Text>
                    <Image style={{width:8,height:14,resizeMode:'contain'}} source={require('../../images/enterLight.png')}/>
                </TouchableOpacity>
                {this.renderSplit()}
                <TouchableOpacity activeOpacity={0.8} style={styles.itemTouch} onPress={()=>{
                    navigate('ExecuteScene',{
                        executeList:this.executeList,
                        type:3,
                        roomId: this.roomId,
                        callBack:(data)=>{ this.callBack(data) }
                    })
                }}>
                    <Image style={styles.icon} source={sceneIcon}/>
                    <Text style={[styles.itemText,{color:Colors.themeText}]}>全部场景</Text>
                    <Image style={{width:8,height:14,resizeMode:'contain'}} source={require('../../images/enterLight.png')}/>
                </TouchableOpacity>
                {this.renderSplit()}
                <TouchableOpacity activeOpacity={0.8} style={styles.itemTouch} onPress={()=>{
                    navigate('ExecuteDevice',{
                        type: 4,
                        roomId: this.roomId,
                        filterType: 2,
                        floorType: 9,
                        callBack:(data)=>{ this.callBack(data) }
                    })
                }}>
                    <Image style={styles.icon} source={deviceIcon}/>
                    <Text style={[styles.itemText,{color:Colors.themeText}]}>全部设备</Text>
                    <Image style={{width:8,height:14,resizeMode:'contain'}} source={require('../../images/enterLight.png')}/>
                </TouchableOpacity>
                {this.renderSplit()}
                <TouchableOpacity activeOpacity={0.8} style={styles.itemTouch} onPress={()=>{
                    if(this.state.ishiddenNotify){
                        return
                    }
                    navigate('ExecuteNotification',{
                        type: 1,
                        callBack:(data)=>{ this.callBack(data) },
                    })
                }}>
                    <Image style={styles.icon} source={notifyIcon}/>
                    <Text style={[styles.itemText,{color: this.state.ishiddenNotify ? Colors.themeTextLight : Colors.themeText}]}>消息通知</Text>
                    {this.state.ishiddenNotify ? <Text style={[styles.ignoreText,{color:Colors.themeTextLight}]}>已经添加过消息通知</Text> : null}
                    {this.state.ishiddenNotify ? null : <Image style={styles.rightArrow} source={require('../../images/enterLight.png')}/>}
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <ScrollView style={[styles.container,{backgroundColor: Colors.themeBg}]}>
                {this.getHeaderName()}
                {this.getChooses()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    scrollContent:{
        paddingBottom: 80,
    },
    topTitleStyle: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    line:{
        height:1,
        backgroundColor:Colors.splitLightGray,
        flex:1,
        marginLeft: 40,
    },
    itemTouch:{
        height:60,
        alignItems:'center',
        flexDirection: 'row'
    },
    icon:{
        width:38,
        height:38,
        resizeMode:'contain'
    },
    itemText:{
        marginLeft: 20,
        fontSize: 18,
        flex:1
    },
    ignoreText:{
        marginRight:10,
        fontSize:13,
        color:Colors.themeTextLightGray
    },
    rightArrow:{
        width:8,
        height:14,
        resizeMode:'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(AddExecute)

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    FlatList,
    Alert,
    RefreshControl,
    Modal,
} from 'react-native';
import { Colors, NotificationKeys, NetUrls } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';
import { connect } from 'react-redux';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import {ImagesLight, ImagesDark} from '../../common/Themes';
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import {ListNoContent,FooterEnd,ListLoading,ListError } from "../../common/CustomComponent/ListLoading";

class ExecuteScene extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'选择场景'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text: '筛选',onPress:()=>{
                        navigation.state.params.filterModal()
                    }}
                ]}/>
            ),
            headerBackground: <HeaderBackground/>
        }
    };

    constructor(props) {
        super(props);
        const { getParam,setParams } = props.navigation;
        this.callBack = getParam('callBack')
        this.executeList = getParam('executeList')
        this.roomId = getParam('roomId')

        this.state = {
            loading: true,
            type: getParam('type'),//1--通知，2--停启用智能，3--场景，4--设备
            typeFlag: 1,//1--筛选类型，2--添加智能
            modalVisible: false,
            itemData: null,
            filterData: null,
            filterResult: null,
            selectData: [],
            pickerTitle: '',
            list:null,
            result: null,
            sceneId:null,
            smartName:null,
            smartIcon:null,
            floor:null,
            floorText: '',
            roomName:null
        }

        setParams({
            filterModal: this.filterModal.bind(this)
        })
    }

    componentDidMount() {
        this.getSceneList(null, this.roomId);
        this.getDictList();
        this.getFilterList();
    }

    componentWillUnmount() {
        
    }

    // 获取场景列表
    async getSceneList(floor,roomId) {
        try {
            let data = await postJson({
                url: NetUrls.smartExecuteList,
                params: {
                    type:this.state.type,//2-智能 3-场景 4-设备
                    floor:floor,//楼层
                    roomId:roomId,//房间
                }
            });
            this.setState({loading: false})
            if (data.code == 0) {
                this.setState({
                    list: data.result || [],
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            this.setState({loading: false})
            ToastManager.show('网络错误')
        }
    }

    // 获取字典列表
    async getDictList() {
        try {
            let data = await postJson({
                url: NetUrls.getDelayDictionary,
                params: {
                    type:this.state.type
                }
            });
            if (data.code == 0) {
                if (data.result && data.result.delayList
                    && data.result.delayList.length > 0
                    && data.result.resultList && data.result.resultList.length > 0 ) {
                    let result = [];
                    let list = data.result.delayList;
                    let dlist = data.result.resultList;
                    for (const item of list) {
                        let temp = {}
                        let arrs = []
                        for (const child of dlist) {
                            let c = child.text
                            arrs.push(c)
                        }
                        temp[item.text] = arrs;
                        result.push(temp)
                    }
                    this.setState({
                        itemData: result,
                        result: data.result,
                    })
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            ToastManager.show('网络错误')
        }
    }

    // 获取筛选列表
    async getFilterList() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.floorAndRoomList,
                params: {
                    type: this.state.type
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                if (data.result && data.result.length > 0) {
                    let result = [];
                    let list = data.result;
                    for (const item of list) {
                        let temp = {}
                        let arrs = []
                        for (const child of item.room) {
                            let c = child.roomName
                            arrs.push(c)
                        }
                        temp[item.floorText] = arrs;
                        result.push(temp)
                    }
                    this.setState({
                        filterData: result,
                        filterResult: data.result
                    })
                }
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

    dealData(data){
        if(!this.executeList){
            for(let j in data){
                data[j].flag = 0;
            }
            return data;
        }
        let flag = false;
        for(let i in this.executeList){
            if(this.executeList[i].sceneId != null){
                for(let j in data){
                    if(this.executeList[i].sceneId == data[j].sceneId
                        && data[j].roomId == null){
                        flag = true;
                    }
                }
            }
        }
        if(flag){//已经选择过整屋场景，将其余整屋场景设置为不可选择
            for(let j in data){
                if(data[j].roomId == null){
                    data[j].flag = 1;
                }else{
                    data[j].flag = 0;
                }
            }
        }else{
            for(let j in data){
                data[j].flag = 0;
            }
        }
        return data;
    }

    // 筛选
    filterModal(){
        this.setState({
            data: this.state.filterData,
            modalVisible: true,
            pickerTitle:'筛选',
            typeFlag: 1,
        })
    }

    // 处理筛选及保存智能执行
    deal(data,index){
        switch (this.state.typeFlag){
            case 1:// 筛选
                let params = this.getParams(index);
                this.getSceneList(params[0],params[1]);
                break;
            case 2:// 保存智能执行条件
                this.handlePickerSelectData(data,index)
                break;
        }
    }

    // Picker 确定按钮 点击处理
    handlePickerSelectData(selectData,selectIndex){
        const {pop} = this.props.navigation

        let selectValue =this.state.result.resultList[selectIndex[1]].id
        let delay = this.state.result.delayList[selectIndex[0]].id

        let data = {}
        data.sceneId = this.state.sceneId //执行智能id
        data.executeJson = selectValue // type==3 传0/1
        data.executeType = this.state.type //执行类型 ，1-通知 2-智能 3-场景 4-设备
        data.delay = delay //延迟时间
        data.name = this.state.smartName
        data.icon = this.state.smartIcon
        data.dayIcon = this.state.dayIconOn
        data.nightIcon = this.state.nightIconOn
        data.conditionTitle = selectData[1]
        data.conditionValue = selectData[0]
        data.floor = this.state.floor
        data.floorText = this.state.floorText
        data.roomName = this.state.roomName

        this.callBack && this.callBack(data)
        this.setState({
            modalVisible: false
        },()=>{
            pop(2)
        })
    }

    // 获取楼层及房间id
    getParams(param){
        let p = [];
        let {filterResult} = this.state;
        if(filterResult && filterResult.length>0){
            let param1 = filterResult[param[0]].floor;
            p.push(param1);
            if(filterResult[param[0]].room && filterResult[param[0]].room.length>0){
                let param2 = filterResult[param[0]].room[param[1]].roomId;
                p.push(param2);
            }
        }
        return p;
    }

    getModal() {
        if(!this.state.data){
            return null
        }
        return (
            <Modal
                style={{ flex: 1 }}
                animationType="none"
                transparent={true}
                visible={this.state.modalVisible}
                onShow={() => {
                    this.showDoublePicker();
                }}
                onRequestClose={() => {
                }}>
                <TouchableOpacity activeOpacity={1} style={styles.modalTouchable} onPress={() => {
                    this.setState({
                        modalVisible: false
                    }, () => {
                        window.DoubulePicker.hide()
                    })
                }}>
                </TouchableOpacity>
            </Modal>
        )
    }

    // 弹出 Modal
    showDoublePicker() {
        window.DoubulePicker.init({
            pickerData: this.state.data,
            pickerConfirmBtnText: '确定',
            pickerCancelBtnText: '取消',
            pickerTitleText: this.state.pickerTitle,
            pickerCancelBtnColor: [38, 80, 215, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: this.state.selectData,
            onPickerConfirm: (data,index) => {
                this.setState({
                    modalVisible: false,
                    selectData: data
                })
                console.log(data,index);
                this.deal(data,index);
            },
            onPickerCancel: data => {
                this.setState({
                    modalVisible: false
                })
            },
            onPickerSelect: (data,index) => {
                console.log("selectData："+data+",index:"+index);
            }
        });
        window.DoubulePicker.show()
    }

    _extraConditionUniqueKey(item, index) {
        return "condition_index_" + index;
    }

    renderRowItem(rowData,index) {
        const Colors = this.props.themeInfo.colors
        const split = this.state.list.length == index +1 ? null : <View style={[styles.split,{backgroundColor: Colors.split}]}/>
        const sceneIcon = this.props.themeInfo.isDark ? rowData.nightIconOn : rowData.dayIconOn

        let room = null
        if(rowData.floorText && rowData.roomName){
            room = <Text style={[styles.roomText,{color:Colors.themeTextLight}]}>{rowData.floorText + '  ' + rowData.roomName}</Text>
        }
                
        return(
            <TouchableOpacity 
                activeOpacity={0.8} 
                style={[styles.itemTouch,{backgroundColor: Colors.themeBg}]} 
                onPress={()=>{
                    this.setState({
                        data:this.state.itemData,
                        modalVisible: true,
                        pickerTitle:rowData.sceneName,
                        typeFlag: 2,
                        sceneId:rowData.sceneId,
                        smartName:rowData.sceneName,
                        smartIcon:rowData.iconOff,
                        floor:rowData.floor,
                        floorText:rowData.floorText,
                        roomName:rowData.roomName,
                        dayIconOn: rowData.dayIconOn,
                        nightIconOn: rowData.nightIconOn
                    })
                }}
            >
                <Image style={{width:28,height:28,resizeMode:'contain'}} source={{uri: sceneIcon}}/>
                <View style={{marginLeft: 20,flex: 1}}>
                    <Text style={[styles.itemText,{color: Colors.themeText}]}>{rowData.sceneName}</Text>
                    {room}
                </View>
                {/* <Text style={{fontSize: 13, color: Colors.themeTextLight}}>{rowData.flag == 1 ? '条件冲突，不可选':''}</Text> */}
                <Image style={styles.arrowImg} source={require('../../images/enterLight.png')}/>
                {split}
            </TouchableOpacity>
        );
    }

    getFlatList(){
        let list = this.state.list;

        if(!list && this.state.loading){
            return <ListLoading/>
        }
        if(!list && !this.state.loading){
            return <ListError onPress={()=>{ this.getSceneList() }}/>
        }
        if (list.length <= 0) {
            const Images = this.props.themeInfo.isDark ? ImagesDark : ImagesLight
            const tips = this.roomId ? '该房间暂无场景\r\n您可以点击筛选来选择其他区域场景' : '暂无场景'

            return (
                <ListNoContent img={Images.noScene} text={tips}/>
            )
        }
        return(
            <FlatList
                contentContainerStyle={{ paddingBottom: 50 }}
                data={list}
                keyExtractor={this._extraConditionUniqueKey}
                renderItem={({item,index}) => this.renderRowItem(item,index)}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator = {false}
            />
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <View style={[styles.container,{backgroundColor: Colors.themeBaseBg}]} >
                {this.getFlatList()}
                {this.getModal()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navTouch: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        paddingRight: 16,
        paddingLeft: 10
    },
    navText: {
        color: Colors.themeBG,
        fontSize: 15
    },
    modalTouchable:{
        width: '100%',
        height: '100%',
        backgroundColor: Colors.translucent
    },
    itemTouch:{
        paddingHorizontal: 20,
        height:60,
        alignItems:'center',
        flexDirection: 'row'
    },
    itemText:{
        fontSize: 15,
        color:Colors.themeTextBlack
    },
    split:{
        position: 'absolute',
        bottom: 0,
        left: 16,
        width: '100%',
        height: 1
    },
    roomText:{
        fontSize:13,
        marginTop: 5
    },
    arrowImg:{
        width:8,
        height:14,
        resizeMode:'contain'
    }
});

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ExecuteScene)

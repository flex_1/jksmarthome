/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    FlatList,
    Alert,
    TextInput,
    Keyboard
} from 'react-native';
import { Colors, NotificationKeys, NetUrls } from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import HeaderLeft from '../CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../CommonPage/Navigation/HeaderBackground';
import { connect } from 'react-redux';
import HeaderTitle from '../CommonPage/Navigation/HeaderTitle';
import HeaderRight from '../CommonPage/Navigation/HeaderRight';


class ExecuteNotification extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation}/>,
            headerTitle: <HeaderTitle title={'消息通知设置'}/>,
            headerRight: (
                <HeaderRight buttonArrays={[
                    {text: '保存',onPress:()=>{
                        navigation.getParam('saveBtnClick')()
                    }}
                ]}/>
            ),
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
    };

    constructor(props) {
        super(props);
        const {getParam, setParams} = this.props.navigation
        this.callBack = getParam('callBack')
        this.intelligentExecuteId = getParam('intelligentExecuteId')
        this.type = getParam('type')
        this.notify = getParam('notify')
        
        this.state = {
            notify: this.notify,
            notificationList: getParam('notificationList')
        }

        setParams({
            saveBtnClick: this.saveBtnClick.bind(this)
        })
    }

    componentDidMount() {
        if(!this.state.notificationList){
            this.requestNotification()
        }
    }

    componentWillUnmount() {

    }

    saveBtnClick(){
        Keyboard.dismiss()
        if(!this.state.notify){
            ToastManager.show('通知内容不能为空')
            return
        }
        if(this.state.notificationList?.length <= 0){
            ToastManager.show('数据加载中，请稍后')
            return
        }
        let isShowWarning = true
        let executeJson = ''
        let conditionTitle = ''
        for (const data of this.state.notificationList) {
            if(data.ischecked){
                isShowWarning = false
                conditionTitle = conditionTitle ? (conditionTitle + '、' + data.messageTitle)  : data.messageTitle
            }
            executeJson = executeJson + data.ischecked
        }
        if(isShowWarning){
            ToastManager.show('至少选择一种通知方式')
            return
        }

        const {pop, getParam} = this.props.navigation
        let data = {}
        data.notify = this.state.notify
        data.name = '消息通知'
        data.executeJson = executeJson
        data.conditionTitle = conditionTitle
        data.notificationList = JSON.parse(JSON.stringify(this.state.notificationList))
        data.executeType = this.type
        
        this.callBack && this.callBack(data)

        if(!this.notify){
            pop(2)
        }else{
            pop()
        }
    }

    async requestNotification() {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.getMessageNotification,
                params: {
                    id: this.intelligentExecuteId
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
                this.setState({
                    notify: data.result?.context,
                    notificationList: data.result?.list || []
                })
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

    renderInputView(){
        const Colors = this.props.themeInfo.colors

        return(
            <View style={styles.topWrapper}>
                <Text style={{fontSize:12, fontWeight:'500', color: Colors.themeText}}>请在此处填写通知内容</Text>
                <TextInput
					style={[styles.input, {backgroundColor:Colors.themeBg, color:Colors.themeText}]}
					value={this.state.notify}
                    maxLength={100}
                    multiline={true}
                    autoFocus={false}
                    autoCorrect={false}
                    blurOnSubmit={true}
                    placeholderTextColor={Colors.themeInactive}
                    returnKeyLabel={'完成'}
                    returnKeyType={'done'}
                    underlineColorAndroid="transparent"
                    placeholder={''}
                    onChangeText={(text)=>{
						this.setState({
							notify: text
						})
                    }}
				/>
            </View>
        )
    }
    
    renderChooseView(){
        if(!this.state.notificationList){
            return null
        }

        const Colors = this.props.themeInfo.colors
        
        return(
            <View style={styles.bottomWrapper}>
                <Text style={{fontSize:12, fontWeight:'500', marginLeft: 16,color: Colors.themeText}}>请在此处选择通知方式</Text>
                <View style={{marginTop: 20}}>
                    {this.state.notificationList.map((value, index)=>{
                        const checkIcon = value.ischecked == 1 ? require('../../images/connectWifi/selected.png') 
                        : require('../../images/connectWifi/unselected.png')

                        return(
                            <TouchableOpacity key={index} activeOpacity={0.7} style={{flexDirection: 'row',height: 64}} onPress={()=>{
                                let target = value.ischecked == 1 ? 0:1
                                this.state.notificationList[index].ischecked = target
                                this.setState({
                                    notificationList: this.state.notificationList
                                })
                            }}>
                                <View style={{flex: 1,paddingLeft: 16, justifyContent: 'center'}}>
                                    <Text style={{fontSize: 14, fontWeight: '400', color: Colors.themeText}}>{value.messageTitle}</Text>
                                    <Text style={{fontSize: 12, fontWeight: '400', marginTop: 5, color: Colors.themeTextLight}}>{value.messagecontext}</Text>
                                </View>
                                <View style={{justifyContent: 'center', alignItems:'center',paddingHorizontal: 30,height:'100%'}}>
                                    <Image style={{width:20,height:20}} source={checkIcon}/>
                                </View>
                                <View style={[styles.line,{backgroundColor: Colors.themeBaseBg}]}/>
                            </TouchableOpacity>
                        )
                    })}
                </View>
            </View>
        )
    }

    render() {
        const Colors = this.props.themeInfo.colors

        return (
            <ScrollView style={[styles.container,{backgroundColor: Colors.themeBg}]} keyboardDismissMode={'on-drag'}>
                <View style={[styles.split,{backgroundColor: Colors.themeBaseBg}]}/>
                {this.renderInputView()}
                <View style={[styles.split,{backgroundColor: Colors.themeBaseBg}]}/>
                {this.renderChooseView()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    input:{
		width:'100%',
		height: 120,
		paddingTop:10,
		paddingBottom:10,
		paddingHorizontal:16,
      	textAlignVertical:'top',
		fontSize:14,
        marginTop: 10,
        borderWidth: 0.5,
        borderColor: '#BABABA'
	},
    topWrapper:{
        paddingVertical: 20,
        paddingHorizontal: 16
    },
    split:{
        height: 6,
        width: '100%'
    },
    line:{
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        height: 1
    },
    bottomWrapper:{
        paddingVertical: 20
    }
})

export default connect(
    (state) => ({
        themeInfo: state.themeInfo,
    })
)(ExecuteNotification) 

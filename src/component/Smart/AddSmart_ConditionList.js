/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
    Platform,
    DeviceEventEmitter,
    StyleSheet,
    Image,
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    FlatList,
    Alert,
    RefreshControl,
    SwipeableFlatList,
    Dimensions
} from 'react-native';
import {Colors, NetUrls} from '../../common/Constants';
import { postJson } from '../../util/ApiRequest';
import ToastManager from "../../common/CustomComponent/ToastManager";
import SpinnerManager from "../../common/CustomComponent/SpinnerManager";
import DateUtil,{ChangeNumToWeek} from "../../util/DateUtil";
import {ColorsLight, ColorsDark} from '../../common/Themes';

//侧滑最大距离
const maxSwipeDistance = 80
//侧滑按钮个数
const countSwiper = 1

class SmartConditionList extends Component {

    constructor(props) {
        super(props)
        this.isDark = props.isDark

        this.state = {
            conditionList: [],
            pickerData: null,
            pickerDeviceName: '',
            selectedConditionData: null,
            selectConditionIndex: 0,
            currentSelectedValue: [],
        }
    }

    componentDidMount() {
        
    }

    componentWillUnmount() {
        
    }

    // 获取 条件 getConditionByDeviceId
	async getConditionsById(id) {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.getConditionByDeviceId,
                params: {
					deviceId: id
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
				// 数据获取成功后 弹出modal
                let conditionData = data.result
                if(!conditionData || conditionData.length<=0){
					ToastManager.show('该设备暂无条件选择')
					return
				}
                this.prepareForPickerData(conditionData)
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

    // 获取天气的条件数据
    async getWeatherConditionsById(type) {
        SpinnerManager.show()
        try {
            let data = await postJson({
                url: NetUrls.environmentRange,
                params: {
					type: type
                }
            });
            SpinnerManager.close()
            if (data.code == 0) {
				// 数据获取成功后 弹出modal
                let conditionData = data.result
                if(!conditionData || conditionData.length<=0){
					ToastManager.show('该设备暂无条件选择')
					return
				}
                this.prepareForPickerData(conditionData)
            } else {
                ToastManager.show(data.msg)
            }
        } catch (error) {
            SpinnerManager.close()
            ToastManager.show('网络错误')
        }
    }

    prepareForPickerData(conditionData){
        let pickerData = []
        for (const val of conditionData) {
            let condition = {}
			let conditionUnit = val.conditionUnit || ''
			let values = []
			if(val.value && val.value.length){
                for (const iterator of val.value) {
                    values.push(iterator + conditionUnit)
                }
            }else{
                values = ['-']
            }
			condition[val.conditionText + val.title] = values
			pickerData.push(condition)
        }
		this.setState({
            conditionData: conditionData,
            pickerData: pickerData
        },()=>{
            this.props.openPicker()
        })
    }
    
    // 用于 修改 执行条件里面的设置
    showDoublePicker() {
        window.DoubulePicker.init({
            pickerData: this.state.pickerData,
            pickerConfirmBtnText: '确定  ',
            pickerCancelBtnText: '取消',
            pickerTitleText: this.state.pickerDeviceName,
            pickerCancelBtnColor: [38, 80, 215, 1],
            pickerConfirmBtnColor: [38, 80, 215, 1],
            pickerToolBarBg: [255, 255, 255, 1],
            pickerBg: [255, 255, 255, 1],
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: this.state.currentSelectedValue,
            onPickerConfirm: (data,index) => {
                console.log(data,index)
                this.handlePickerSelectData(data,index)
            },
            onPickerCancel: data => {
                this.props.closePicker()
            },
            onPickerSelect: data => {
            }
        });
        window.DoubulePicker.show()
    }

    // 处理 picker确定的点击 事件
    handlePickerSelectData(selectData,selectIndex){
        let firstIndex = selectIndex[0]
        //let secondIndex = selectIndex[1]
        let selectConditionId = this.state.conditionData[firstIndex]['conditionId']
        let conditionUnit = this.state.conditionData[firstIndex]['conditionUnit']
        let conditionTitle = this.state.conditionData[firstIndex]['conditionText'] + this.state.conditionData[firstIndex]['title']
        let selectValue = selectData[1]
        if(conditionUnit && (selectValue != null) ){
            selectValue = selectValue.replace(conditionUnit,'')
        }
        let data = JSON.parse(JSON.stringify(this.state.selectedConditionData))
        data.conditionId = selectConditionId
        data.conditionTitle = conditionTitle
        data.conditionValue = selectValue
        data.conditionUnit = conditionUnit
        let conditionList = JSON.parse(JSON.stringify(this.props.conditionList))
        conditionList[this.state.selectConditionIndex] = data
        
        this.props.updateConditionCallBack(conditionList)
        this.props.closePicker()
    }

    //关闭侧滑栏
	closeSwiper() {
        this.swiperFlatlist_condition?.setState({ openRowKey: null })
    }
    
    _extraConditionUniqueKey(item, index) {
		return "condition_index_" + index;
    }

    // 获取侧滑设置
    getQuickActions(rowData, index){
        return (
			<View style={styles.quickAContent}>
				<TouchableOpacity activeOpacity={0.7} onPress={() => {
					this.closeSwiper()
					Alert.alert(
						'提示',
						'确认删除?',
						[
							{ text: '取消', onPress: () => { }, style: 'cancel' },
							{ text: '删除', onPress: () => {
                                this.props.conditionList.splice(index,1)
                                this.props.updateConditionCallBack(this.props.conditionList)
                            }},
						]
					)
				}}>
					<View style={[styles.quick, { backgroundColor: Colors.themBgRed }]}>
						<Text style={{ color: Colors.white, fontSize: 14 }}>删除</Text>
					</View>
				</TouchableOpacity>
				
			</View>
		)
    }

    // 处理条件的点击事件
    handleConditionClick(rowData,index){

        console.log(rowData);

        const {navigate} = this.props.navigation
        let conditionType = rowData.conditionType
        
        // 1-时间段 2-手机进入或离开某地 3-定时（共用设备场景定时表） 4-设备 10~15-室外天气相关
        if(conditionType == 1){
            console.log( JSON.stringify(rowData) );
            
            navigate('SmartDuring',{
                duringData: JSON.parse(JSON.stringify(rowData)),
                callBack: (data)=>{
                    // 替换 原有数据
                    this.props.conditionList[index] = data
                    this.props.updateConditionCallBack(this.props.conditionList)
                }
            })
        }else if(conditionType == 2){

        }else if(conditionType == 3){
            let currentDate = new Date();
            if(rowData.hour != null && rowData.minute != null){
                currentDate.setHours(rowData.hour)
                currentDate.setMinutes(rowData.minute)
            } 
            navigate('SmartTiming',{
                timingData: JSON.parse(JSON.stringify(rowData)),
                currentDate: currentDate,
                callBack: (data)=>{
                    // 替换 原有数据
                    this.props.conditionList[index] = data
                    this.props.updateConditionCallBack(this.props.conditionList)
                }
            })
        }else if(conditionType == 4){
            if(!rowData.deviceId){
                return
            }
            let s_value = rowData.conditionTitle || ''
            let s_title = (rowData.conditionValue || '') + (rowData.conditionUnit || '')
            // 点击条件里的设备
            this.setState({
                selectedConditionData: JSON.parse(JSON.stringify(rowData)),
                selectConditionIndex: index,
                pickerDeviceName: rowData.name || '设备',
                currentSelectedValue: [s_value,s_title]
            },()=>{
                this.getConditionsById(rowData.deviceId)
            })
        }else if(conditionType == 10|11|13|14|15){
            let s_value = rowData.conditionTitle || ''
            let s_title = (rowData.conditionValue || '') + (rowData.conditionUnit || '')
            this.setState({
                selectedConditionData: JSON.parse(JSON.stringify(rowData)),
                selectConditionIndex: index,
                pickerDeviceName: rowData.name || '室外天气',
                currentSelectedValue: [s_value,s_title]
            },()=>{
                this.getWeatherConditionsById(conditionType)
            })
        }
    }
    
    // 渲染 条件的 Item（单元cell）
    renderConditionRowItem(rowData, index){
        const Colors = this.isDark ? ColorsDark : ColorsLight

        // 1-时间段 2-手机进入或离开某地 3-定时（共用设备场景定时表） 4-设备  10~15-室外天气相关
        let conditionType = rowData.conditionType
        let iconUrl = this.isDark ? rowData.nightIcon : rowData.dayIcon
        let icon = {uri: iconUrl} 
        let name = rowData.name
        let extra = ''
        let subExtra = null
        let room = null

        if(conditionType == 1){
            // 时间段
            if(!iconUrl) {
                icon = this.isDark ? require('../../images/smart/condition_during_dark.png')
                    : require('../../images/smart/condition_during.png')
            }
            if(!name) name = '时间段'
            // 获取 循环周期
            let cyclicText = ChangeNumToWeek(rowData.cyclic)
            let during = '全天'
            if(rowData.startTime && rowData.endTime){
                const startHour = rowData.startTime.split(':')[0]
                const startMin = rowData.startTime.split(':')[1]
                const endtHour = rowData.endTime.split(':')[0]
                const endMin = rowData.endTime.split(':')[1]
                if(parseInt(endtHour) < parseInt(startHour)){
                    during = rowData.startTime+'-次日'+rowData.endTime
                }else if(parseInt(endtHour) == parseInt(startHour)){
                    if(parseInt(endMin) <= parseInt(startMin)){
                        during = rowData.startTime+'-次日'+rowData.endTime
                    }else{
                        during = rowData.startTime+'-'+rowData.endTime
                    }
                }else{
                    during = rowData.startTime+'-'+rowData.endTime
                }
            }else if(rowData.startTime && !rowData.endTime){
                during = rowData.startTime + ' 开始'
            }else if(!rowData.startTime && rowData.endTime){
                during = rowData.endTime + ' 结束'
            }
            if(rowData.wholeDay){
                during = '全天'
            }
            extra = cyclicText
            subExtra = <Text style={styles.subExtra}>{during}</Text>
        }else if(conditionType == 2){
            // 手机进入或离开某地
            if(!iconUrl) {
                icon = this.isDark ? require('../../images/smart/condition_phone_location_dark.png')
                    : require('../../images/smart/condition_phone_location.png')
            }
            if(!name) name = '进入或离开'
            extra = ''
        }else if(conditionType == 3){
            // 定时
            if(!iconUrl) {
                icon = this.isDark ? require('../../images/smart/condition_timing_dark.png')
                    : require('../../images/smart/condition_timing.png')
            }
            if(!name) name = '定时'

            // 分钟加 0
            if(rowData.cyclic == null || rowData.hour == null){
                extra = '已失效'
            }else if(rowData.status == 0){
                extra = '已失效'
            }else{
                let minu = rowData.minute
			    if (minu < 10) {
				    minu = '0' + minu
			    }
			    // 获取 定时循环周期
			    let cyclicText = ChangeNumToWeek(rowData.cyclic)
                extra = cyclicText
                subExtra = <Text style={styles.subExtra}>{rowData.hour + ':' +minu}</Text>
            }
        }else if(conditionType == 4){
            // 设备
            if(!iconUrl) {
                icon = this.isDark ? require('../../images/smart/condition_device_dark.png')
                    : require('../../images/smart/condition_device.png')
            }
            if(!name) name = '设备'
            let conditionUnit = rowData.conditionUnit ? rowData.conditionUnit : ''
            let conditionValue = rowData.conditionValue === null ? '' : rowData.conditionValue
            let valueText = ''
            if(rowData.deviceId){
                if(conditionValue === '-' || conditionValue === ''){
                    valueText = ''
                }else{
                    valueText = conditionValue + conditionUnit
                }
                extra = rowData.conditionTitle + ' ' + valueText
            }else{
                extra = '暂无该设备'
            }
            if(rowData.floorText && rowData.roomName){
                room = <Text style={styles.roomText}>{rowData.floorText + '  ' + rowData.roomName}</Text>
            }
        }else if(conditionType == 10|11|13|14|15){
            if(!iconUrl) {
                icon = this.isDark ? rowData.weatherNightIcon : rowData.weatherDayIcon
            }
            let conditionUnit = rowData.conditionUnit ? rowData.conditionUnit : ''
            let conditionValue = rowData.conditionValue === null ? '' : rowData.conditionValue
            let valueText = ''
            if(conditionValue === '-' || conditionValue === ''){
                valueText = ''
            }else{
                valueText = conditionValue + conditionUnit
            }
            extra = rowData.conditionTitle + ' ' + valueText
        }

        return(
            <View style={{width:'100%',marginTop: 10,paddingHorizontal: 16,}}>
                <TouchableOpacity 
                    activeOpacity={1} 
                    style={[styles.item,{backgroundColor: Colors.themeBg}]} 
                    onPress={()=>{
                        this.closeSwiper()
                        this.handleConditionClick(rowData,index)
                    }}
                >
                    <Image style={{width:30,height:30,resizeMode:'contain'}} source={icon}/>
                    <View style={{marginLeft: 15,flex: 1}}>
                        <Text style={{fontSize:15,color:Colors.themeText}}>{name}</Text>
                        {room}
                    </View>
                    <View >
                        <Text style={styles.extra}>{extra}</Text>
                        {subExtra}
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    // 渲染 条件的 listView
    getConditionListView(){
        let conditionList = this.props.conditionList

        if(!conditionList || conditionList.length<=0){
            return null
        }

        return (
            <SwipeableFlatList
                ref={e => this.swiperFlatlist_condition = e}
                showsVerticalScrollIndicator={false}
                style={{marginTop: 10,}}
				contentContainerStyle={{ paddingBottom: 10 }}
				removeClippedSubviews={false}
				data={conditionList}
				keyExtractor={this._extraConditionUniqueKey}
				renderItem={({ item, index }) => this.renderConditionRowItem(item, index)}
				renderQuickActions={({ item, index }) => this.getQuickActions(item, index)}
				maxSwipeDistance={maxSwipeDistance}
				bounceFirstRowOnMount={false}
				initialNumToRender={5}
			/>
        )
    }

    render() {
        return this.getConditionListView()
    }
}

const styles = StyleSheet.create({
    //侧滑菜单的样式
	quickAContent: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginRight: '5%',
		marginTop: 10,
        overflow: 'hidden',
        paddingVertical: 1
	},
	quick: {
		backgroundColor: Colors.white,
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
        width: maxSwipeDistance / countSwiper,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
	},
    item:{
        width:'100%',
        height: 60,
        borderRadius: 4,
        paddingHorizontal: 16,
        flexDirection:'row',
        alignItems:'center'
    },
    extra:{
        fontSize:14,
        color:Colors.themeTextLightGray,
        alignSelf:'flex-end'
    },
    subExtra:{
        fontSize:14,
        color:Colors.themeTextLightGray,
        marginTop:5,
        alignSelf:'flex-end'
    },
    roomText:{
        fontSize:12,
        color:Colors.themeTextLightGray,
        marginTop: 5
    }
});

export default SmartConditionList

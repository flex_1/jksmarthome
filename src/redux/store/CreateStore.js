/**
 * 描述：
 * @ClassName CreateStore
 * @Author zuomao
 * @Date 2019/6/20
 */

import {createStore,applyMiddleware,compose} from 'redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import RootReducer from '../reducers/RootReducer';

const configureStore = preloadState => {
    return createStore(
		RootReducer,
		preloadState,
		compose(
			applyMiddleware(createLogger, thunk)
		)
	);
}

const store = configureStore();
export default store;
/**
 * 描述：
 * @ClassName UserInfoAction
 * @Author zuomao
 * @Date 2019/6/20
 */

import * as ActionTypes from '../types/ActionTypes';
import { postJson } from "../../util/ApiRequest";
import { NetUrls } from '../../common/Constants';

// redux 传递 字符串
const updateUserName = (userName) => {
    if(userName){
        userName += '尾巴'
    }else{
        userName = '梓源'
    }
    return (dispatch, getState) => {
        dispatch({
            type: ActionTypes.USER_NAME_UPDATED,
            userName: userName
        })
    }
}

// redux 传递 对象
const updateUserInfo = (userInfo) => {
    return async (dispatch, getState) => {
        if(userInfo){
            dispatch({
                type: ActionTypes.USER_INFO_UPDATED,
                userInfo: userInfo
            })
            return
        }
        try {
            let data = await postJson({
                url: NetUrls.homePage,
                params: {}
            });
            if (data.code == 0) {
                dispatch({
                    type: ActionTypes.USER_INFO_UPDATED,
                    userInfo: data.result || {}
                })
            } else {
                throw ''
            }
        } catch (e) {
            dispatch({
                type: ActionTypes.USER_INFO_ERROR,
            })
        }
    }
}

export {
    updateUserName,
    updateUserInfo
}
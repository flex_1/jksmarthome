/**
 * 描述：
 * @ClassName UserInfoAction
 * @Author zuomao
 * @Date 2019/6/20
 */

import * as ActionTypes from '../types/ActionTypes';
import {ColorsLight,ColorsDark} from '../../common/Themes';

const updateAppTheme = (isDark) => {
    let themeInfo = {colors: ColorsLight, isDark: false}
    if(isDark){
        themeInfo = {colors: ColorsDark, isDark: true}
    }
    return (dispatch, getState) => {
        dispatch({
            type: ActionTypes.APP_THEME_UPDATED,
            themeInfo: themeInfo
        })
    }
}

export {
    updateAppTheme
}
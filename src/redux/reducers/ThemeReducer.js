/**
 * 描述：
 * @ClassName UserInfoReducer
 * @Author zuomao
 * @Date 2019/6/20
 */

import * as ActionTypes from '../types/ActionTypes';
import {handleActions} from 'redux-actions';
import {ColorsLight,ColorsDark} from '../../common/Themes';

const initialState = {
    colors: ColorsLight, 
    isDark: false
};

//传递对象
export default handleActions({
    [ActionTypes.APP_THEME_UPDATED] : (state, action) => {
        return {...state, ...action.themeInfo};
    }
},initialState)
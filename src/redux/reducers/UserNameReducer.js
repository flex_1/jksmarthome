/**
 * 描述：
 * @ClassName UserNameReducer
 * @Author zuomao
 * @Date 2019/6/20
 */

import * as ActionTypes from '../types/ActionTypes';
import {handleActions} from 'redux-actions';

const initialState = null;

//传递字符串
export default handleActions({
    [ActionTypes.USER_NAME_UPDATED] : (state, action) => {
        // state: 为上一个状态
        // action:  dispatch 传过来的最新数据
        // return: 返回最新的状态（刷新页面）
        return action.userName;
    }
},initialState)
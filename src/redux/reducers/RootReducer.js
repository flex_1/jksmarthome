/**
 * 描述：
 * @ClassName RootReducer
 * @Author zuomao
 * @Date 2019/6/20
 */

import {combineReducers} from 'redux';
import UserInfoReducer from './UserInfoReducer';
import UserNameReducer from './UserNameReducer';
import ThemeReducer from './ThemeReducer';
 
//所有需要使用到的reducer，都要在这里注册
const RootReducer = combineReducers({
    userInfo: UserInfoReducer,
    userName: UserNameReducer,
    themeInfo: ThemeReducer
});

export default RootReducer;
/**
 * 描述：
 * @ClassName UserInfoReducer
 * @Author zuomao
 * @Date 2019/6/20
 */

import * as ActionTypes from '../types/ActionTypes';
import {handleActions} from 'redux-actions';

const initialState = {};

//传递对象
export default handleActions({
    [ActionTypes.USER_INFO_UPDATED] : (state, action) => {
        return {...state, ...action.userInfo};
    },
    [ActionTypes.USER_INFO_ERROR] : (state, action) => {
        return {};
    },
},initialState)
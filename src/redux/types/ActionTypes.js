/**
 * 描述：
 * @ClassName ActionTypes
 * @Author zuomao
 * @Date 2019/6/20
 */

export const USER_INFO_UPDATED = 'USER_INFO_UPDATED';
export const USER_INFO_ERROR = 'USER_INFO_ERROR';

export const USER_NAME_UPDATED = 'USER_NAME_UPDATED';

// App主题
export const APP_THEME_UPDATED = 'APP_THEME_UPDATED';
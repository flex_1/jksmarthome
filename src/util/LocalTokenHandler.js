// 用来处理 本地化 token 
import AsyncStorage from '@react-native-community/async-storage';

export default class LocalTokenHandler {
    static UserToken = 'kuserTokens';
    
    //存储Token
    static save(tokens,callBack) {
        if(!tokens) return;
        AsyncStorage.setItem(LocalTokenHandler.UserToken,JSON.stringify(tokens),callBack)
    }

    //获取Token,取得的Token是 Json 数据
    static get() {
        let userInfo = AsyncStorage.getItem(LocalTokenHandler.UserToken)
        userInfo = userInfo.then((res)=>{
            res = JSON.parse(res)
            return new Promise(resolve => resolve(res));
        })
        return userInfo
    }

    //移除Token
    static remove(callBack){
        AsyncStorage.removeItem(LocalTokenHandler.UserToken,callBack)
    }
}
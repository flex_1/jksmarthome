// 接收老的设备列表 更新指定设备状态后 返回新的设备列表
const UpdateDeviceList = (deviceList, newState)=> {
    let newDevice = null
    let index = null
    for (let i = 0; i < deviceList.length; i++) {
        const device = deviceList[i];
        if(device.deviceId == newState.deviceId){
            index = i
            newDevice = updateDevice(device, newState)
            break
        }
    }

    if(index == null || newDevice == null){
        return null
    }else{
        let newDeviceList = JSON.parse(JSON.stringify(deviceList))
        newDeviceList[index] = newDevice
        return newDeviceList
    }
}

// 处理不同设备的 状态信息
const updateDevice = ( device, newState)=> {
    let newDevice = JSON.parse(JSON.stringify(device))
    try {
        if(newState.status != null && newState.status != undefined){
            newDevice.status = parseInt(newState.status)
        }
        //空调单独处理
        if(newDevice.attributeTypeName == 'air-condition' || newDevice.attributeTypeName == 'midea-air-condition'){

            newDevice.value = parseInt(newState.temperature)
        }else{
            if(newState.value != null && newState.value != undefined){
                let valueJson = JSON.parse(newState.value)
                for (const key in valueJson) {
                    if (valueJson.hasOwnProperty(key)) {
                        newDevice.value = valueJson[key]
                        break
                    }
                }
            }else if(newState.statusText != null && newState.statusText != undefined){
                let textJson = JSON.parse(newState.statusText)
                for (const key in textJson) {
                    if (textJson.hasOwnProperty(key)) {
                        newDevice.statusText = textJson[key]
                        break
                    }
                }
            }
        }
    } catch (error) {
        
    }
    return newDevice 
}


export default UpdateDeviceList;
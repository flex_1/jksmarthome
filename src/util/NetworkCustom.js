import AsyncStorage from '@react-native-community/async-storage';
import {LocalStorageKeys, CurrentState} from '../common/Constants';

class NetworkCustom {
    host = null
    wsHost = null
    speechHost = null

    async getAndSaveUrl(){
        if(CurrentState != 'inner_ip'){
            return
        }
        const netParams = await AsyncStorage.getItem(LocalStorageKeys.kCustomNetworkParamsInfo)
        let params = JSON.parse(netParams)
        this.host = params?.host
        this.wsHost = params?.wsHost
        this.speechHost = params?.speechHost
    }

    init(netParams){
        this.host = netParams?.host
        this.wsHost = netParams?.wsHost
        this.speechHost = netParams?.speechHost   
    }

    clear(){
        this.host = ''
        this.wsHost = ''
        this.speechHost = ''
    }
}

const netCustom = new NetworkCustom();
export default netCustom;
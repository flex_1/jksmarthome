
// 为项目自定义一些原型方法，方便后续操作
export default class PrototypeFunction {

    // 定义数组删除某个元素
    // 可直接使用 array.remove('a') 来移出某个元素
    static defineArrayRemove() {
        Array.prototype.indexOf = function(val) {
            for(let i = 0; i< this.length; i++){
                if(this[i] == val) return i;
            }
            return -1;
        }
        Array.prototype.remove = function(val) {
            let index = this.indexOf(val)
            if(index > -1){
                this.splice(index,1)
            }
        }
    }

}

// 计算字符串长度 中文算2 英文算1
const StrLength = (str) => {
    let realLength = 0,len = str.length,charCode = -1
    for(var i=0;i<len;i++){
        charCode = str.charCodeAt(i);
        if(charCode>0 && charCode<=128) {
            realLength +=1
        }else{
            realLength += 2;
        }
    }
    return realLength
}

export default StrLength;
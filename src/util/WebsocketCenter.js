
import LocalTokenHandler from './LocalTokenHandler';
import {NetParams,CurrentState} from '../common/Constants';
import netCustom from '../util/NetworkCustom';

// Manager project websocket
export default class WebsocketCenter {

    constructor(props) {
		this.lockReconnect = false;
		this.appWebSocket = null
        this.checkingLock = false
	}

    async openWebSocket(callBack) {
        let tokens = await LocalTokenHandler.get()
        if(!tokens) return

        const url = this.getWsBaseUrl() + '/websocketNotify/' + tokens.token

        appWebSocket = new WebSocket(url);
        appWebSocket.onopen = () => {
            // this.lockReconnect = false;
            // this.openHeartbeatWebsocket()
            // this.websocketInterval && clearInterval(this.websocketInterval)
            console.log('websocket ---> 链接成功');
            this.checkConnecting(callBack)
        }
        appWebSocket.onmessage = (e) => {
            console.log('websocket ---> 接受消息: ' + e.data);
            callBack && callBack(e)
        }
        appWebSocket.onerror = (e) => {
            // this.reconnectWebsocket(callBack)
            console.log('websocket ---> 链接出错: ' + e);
        };
        appWebSocket.onclose = (e) => {
            console.log('websocket ---> 链接被关闭');
        };

        this.appWebSocket = appWebSocket
    }

    getWsBaseUrl(){
        if(CurrentState == 'inner_ip' && netCustom.wsHost){
            return netCustom.wsHost
        }
        return NetParams.wsBaseUrl
    }

    shutDownWebsocket(){
        // this.websocketInterval && clearInterval(this.websocketInterval)
        // this.heartbeatInterval && clearInterval(this.heartbeatInterval)
        this.checkingLock = false
        this.checkConnectInterval && clearInterval(this.checkConnectInterval)
        this.appWebSocket && this.appWebSocket.close()
    }

    // check and reconnect websocket per 20 second
    checkConnecting(callBack){
        if(this.checkingLock) return;
        this.checkingLock = true;
        this.checkConnectInterval = setInterval(() => {
            const wsReadyState = this.appWebSocket.readyState
            if(wsReadyState == 3){
                this.appWebSocket && this.appWebSocket.close()
                this.openWebSocket(callBack)
            }else if(wsReadyState == 1){
                // 如果是链接状态，发送空包
                this.appWebSocket && this.appWebSocket.send('connect')
            }
        }, 20*1000);
    }

    // send a empty data to service per 20 seconds
    openHeartbeatWebsocket(){
        if(this.heartbeat) return;
        this.heartbeat = true
        this.heartbeatInterval = setInterval(()=>{
            this.appWebSocket && this.appWebSocket.send('connect')
        }, 20*1000)
    }

    // reconnect
    reconnectWebsocket(callBack){
        if(this.lockReconnect) return;
        this.lockReconnect = true;
        this.websocketInterval = setInterval(()=>{
            this.appWebSocket && this.appWebSocket.close()
            this.openWebSocket(callBack)
        }, 10*1000)
    }
}
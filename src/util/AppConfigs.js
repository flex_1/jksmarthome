
import {Alert, Linking, Platform, NativeModules} from 'react-native';
import JPush from 'jpush-react-native';
import {postJson,getRequest} from '../util/ApiRequest';
import ToastManager from "../common/CustomComponent/ToastManager";
import {NetUrls, NetParams, LocalStorageKeys} from '../common/Constants';
import deviceInfo from '../util/DeviceInfo';
import LocalTokenHandler from '../util/LocalTokenHandler';
import AsyncStorage from '@react-native-community/async-storage';

const JKRNUtils = NativeModules.JKRNUtils;
//桥接的用户相关信息
const JKUser = Platform.select({
    ios: NativeModules.JKRNUtils,
    android: NativeModules.UserModule
});

/* 
*  1.注册 pushId 到服务器
*  2.版本检测 与 低版本处理
*/

// 苹果 APPstore 应用下载地址 
export const IOS_APPSTORE_URL = 'https://apps.apple.com/cn/app/%E5%B0%8F%E8%90%A8%E7%AE%A1%E5%AE%B6/id1485186693';
// 安卓 应用宝 应用下载地址
export const ANDROID_YINYONGBAO_URL = 'https://sj.qq.com/myapp/detail.htm?apkName=com.jksmarthome'

class AppConfigs {

    //注册 pushId 到服务器
    static async pushRegisterId(){
        const registerID = await getRegister()
        if(!registerID){
            return
        }
        try {
            let res = await postJson({
                url: NetUrls.bindJpushId,
                params: {
                    rid: registerID
                }
            });
            if (res.code == 0) {
                
            } else{
                ToastManager.show(res.msg);
            }
        } catch (error) {
            ToastManager.show('RegisterID 注册失败');
        }
    }

    //版本号检测
    static async checkVersion() {
        try {
			let data = await getRequest({
				url: NetUrls.appVersion,
			});
			if (data.code == 0) {
                // 最低 支持版本
                let lowest_version = data.result && data.result.minVersion
                if(!lowest_version){
                    return
                }
                let current_version = deviceInfo.appVersion
                let isNeedUpdate = false

                let current_arr = current_version.split('.')
                let low_arr = lowest_version.split('.')
                let length = Math.max(current_arr.length, low_arr.length)

                for (let index = 0; index < length; index++) {
                    let c = current_arr[index]
                    let l = low_arr[index]
                    
                    if(c==null && l!=null){
                        isNeedUpdate = true
                        break
                    }else if(c!=null && l==null){
                        isNeedUpdate = false
                        break
                    }else{
                        if(parseInt(c) < parseInt(l)){
                            isNeedUpdate = true
                            break
                        }else if(parseInt(c) > parseInt(l)){
                            isNeedUpdate = false
                            break
                        }
                    }
                }
                if(isNeedUpdate){
                    alertUpdate()
                }
			}
		} catch (error) {
			console.log(JSON.stringify(error));
		}
    }

    // iOS 配置快捷指令
    static async checkShortCuts(){
        if (Platform.OS == 'android') {
            return
        }
        const userInfo = await LocalTokenHandler.get();
        if(!userInfo) {
            return;
        }
        JKRNUtils.registerShortCuts()
    }

    // 检查原生代码中存储的token,并设置原生层的baseurl
    static async checkNativeToken() {
        const userInfo = await LocalTokenHandler.get();
        if(!userInfo) {
            return;
        }
        if (Platform.OS == 'ios') {
            JKUser.checkLocalUserToken(userInfo.token, NetParams.baseUrl);
        }else{
            JKUser.checkLocalUserToken(userInfo.token);
        }
    }

    // 存储登录成功的 手机号
    static async saveAccountNumber(phone){
        if(phone == null){
            return
        }
        let phoneNumbers = await AsyncStorage.getItem(LocalStorageKeys.kStorageAccountNumber)
        if(!phoneNumbers){
            let newPhone = [phone]
            AsyncStorage.setItem(LocalStorageKeys.kStorageAccountNumber,JSON.stringify(newPhone))
        }else{
            let newPhoneNums = JSON.parse(phoneNumbers)
            if( newPhoneNums.includes(phone) ){
                return
            }
            if(newPhoneNums.length >= 20){
                newPhoneNums.pop()
            }
            newPhoneNums.unshift(phone)
            AsyncStorage.setItem(LocalStorageKeys.kStorageAccountNumber,JSON.stringify(newPhoneNums))
        }
    }

    //移出缓存手机号
    static async cleanAccountNumber(){
        AsyncStorage.removeItem(LocalStorageKeys.kStorageAccountNumber)
    }
}

// 获取 RegisterID
function getRegister(){
    return new Promise(function(resolve,reject){
        JPush.getRegistrationID(({registerID})=>{
            if(registerID){
                resolve(registerID) 
            }else{
                resolve('')
            }
        })
    })
}

// 低版本处理
function alertUpdate(){
    let tips = '当前版本过低，请前往AppStore更新至最新版本后使用。'
    if(Platform.OS == 'android'){
        tips = '当前版本过低，请前往应用市场更新至最新版本后使用。'
    }
    Alert.alert(
        '更新',
        tips,
        [
            { 
                text: '去更新',onPress:()=>{
                    if(Platform.OS == 'ios'){
                        Linking.openURL(IOS_APPSTORE_URL)
                    }else{
                        Linking.openURL(ANDROID_YINYONGBAO_URL)
                    }   
                    setTimeout(() => {
                        alertUpdate()
                    }, 200);
                }
            }
        ],
        { cancelable: false }
    )
}

export default AppConfigs
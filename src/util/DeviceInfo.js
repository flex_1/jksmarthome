class DeviceInfo {
    appVersion = ''
    systemVersion = ''
    phoneModel = ''
    phoneBrand = ''
    phoneId = ''
    deviceName = ''
    localAddress = ''
    uniqueId = ''

    init(){
        const DI = require('react-native-device-info')
        
        this.appVersion = DI.getVersion()
        this.phoneModel = DI.getModel()
        this.phoneBrand = DI.getBrand()
        this.phoneId = DI.getUniqueId()
        this.deviceName = DI.getDeviceNameSync()
        this.systemVersion = DI.getSystemVersion()
        this.localAddress = DI.getIpAddressSync()
        this.uniqueId = DI.getUniqueId()
    }
}

const deviceInfo = new DeviceInfo();
export default deviceInfo;
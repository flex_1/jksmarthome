
const DateUtil = (timestamp, formater) => {
    // timestamp时间戳 formater时间格式 
    let date = new Date();
    date.setTime(parseInt(timestamp));
    formater = (formater != null) ? formater : 'yyyy-MM-dd hh:mm';
    Date.prototype.Format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, //月 
            "d+": this.getDate(), //日 
            "h+": this.getHours(), //小时 
            "m+": this.getMinutes(), //分 
            "s+": this.getSeconds(), //秒 
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
            "S": this.getMilliseconds() //毫秒 
        };

        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ?
                (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
        return fmt;
    }
    return date.Format(formater);
}

// 将1,2,3 转换成 周一，二，三
export const ChangeNumToWeek = (str) => {
    let cyclicText = ''
    if (str === null || str == undefined) {
        cyclicText = null
    } else if (str === '0' || str === 0) {
        cyclicText = '执行一次'
    } else {
        let cyclicArr = str.split(',')
        let textArr = []
        for (const iterator of cyclicArr) {
            if (iterator == '1') {
                textArr.push('日')
            }
            else if (iterator == '2') {
                textArr.push('一')
            }
            else if (iterator == '3') {
                textArr.push('二')
            }
            else if (iterator == '4') {
                textArr.push('三')
            }
            else if (iterator == '5') {
                textArr.push('四')
            }
            else if (iterator == '6') {
                textArr.push('五')
            }
            else if (iterator == '7') {
                textArr.push('六')
            }
        }
        cyclicText = '周' + textArr.toString()
    }
    return cyclicText
}

export const ChangeHourMinutestr = (str) => {
    if (str !== "0" && str !== "" && str !== null) {
        let hour = ((Math.floor(str / 60)).toString().length < 2 ?  (Math.floor(str / 60)).toString() :(Math.floor(str / 60)).toString());
        let min = ((str % 60).toString().length < 2 ?  (str % 60).toString() : (str % 60).toString())
        return (hour>0? hour+'小时':'')+min+"分钟";
    } else{
        return "";
    }
}

export const GetDateLabel = (str) => {
    let d = new Date(str).setHours(0, 0, 0, 0);
    let today = new Date().setHours(0, 0, 0, 0);

    let obj = {
        '-86400000': '昨天 ',
        '0': '今天',
    };
    if(IsThisYear(str)){
        return obj[d - today] || DateUtil(str, 'MM-dd') ;
    }else{
        return DateUtil(str, 'yyyy-MM-dd') ;
    }
}

export const GetTimeLabel = (str) => {
    let d = new Date(str).setHours(0, 0, 0, 0);
    let today = new Date().setHours(0, 0, 0, 0);

    let obj = {
        '-86400000': '昨天 '+ DateUtil(str, 'hh:mm'),
        '0': DateUtil(str, 'hh:mm'),
    };
    if(IsThisYear(str)){
        return obj[d - today] || DateUtil(str, 'MM-dd') ;
    }else{
        return DateUtil(str, 'yyyy-MM-dd') ;
    }
}

export const IsThisYear = (time)=>{
    const now = new Date().getFullYear();
    const timeYear = new Date(time).getFullYear();
    return (now - timeYear) == 0
}

export default DateUtil;
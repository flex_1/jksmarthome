
import LocalTokenHandler from './LocalTokenHandler';
import { DeviceEventEmitter } from "react-native";
import {NetParams, NotificationKeys, CurrentState} from '../common/Constants';
import ToastManager from '../common/CustomComponent/ToastManager';
import netCustom from '../util/NetworkCustom';

const baseUrlApi = NetParams.baseUrl + '/api';
const logRequest = true;

export async function getRequest({ url, params, useToken=false, toJson=true }) {
    url = getFullApiUrl(url);
    let paramsArray = [];
    if(params){
        Object.keys(params).forEach(key =>
            paramsArray.push(key + "=" + params[key])
        )
    }
    //获取用户token
    if (useToken) {
        let tokens = await LocalTokenHandler.get()
        if (tokens)
            paramsArray.push('token=' + tokens.token)
        else
            return Promise.reject({ 'code': NetParams.noTokenErrorCode, 'msg': 'token不存在' })
    }

    // 判断是否地址拼接的有没有 ？,当没有的时候，使用 ？拼接第一个参数，如果有参数拼接，则用&符号拼接后边的参数
    if (url.search(/\?/) === -1) {
        url = url + "?" + paramsArray.join("&");
    } else {
        url = url + "&" + paramsArray.join("&");
    }
    console.log('sss.get请求的url: '+ getFullApiUrl(url))

    let response = fetch(getFullApiUrl(url), {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    });
    //转成json
    if (toJson) {
        response = response.then(res => (res.json())).catch(()=> ({'msg': 'API Error'}));
        response = response.then(res => addFunction(res));
    }
    if (logRequest) {
        response.then(text => console.log(text));
    }
    //返回结果
    return response;
}

/**
 * post json 请求
 * @param url
 * @param params
 * @param page
 * @param pageSize
 * @param timeout 超时时间，单位为（秒）,默认为10秒，如果需要设置，可以在网络请求里面自行设置
 */
export async function postJson({ url, params = {}, pageNum = 1, pageSize = 10, toJson = true, useToken = true, timeout=NetParams.networkTimeout, break_promise }) {
    if (logRequest) {
        let logStr = 'method:POST';
        logStr += '\nurl:' + getFullApiUrl(url);
        logStr += '\nheaders' + JSON.stringify({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });
        logStr += '\nparams:' + JSON.stringify(params);
        console.log(logStr);
    }
    //获取用户token
    if (useToken) {
        let tokens = await LocalTokenHandler.get()
        if (tokens){
            params.token = tokens.token
        }else{
            return Promise.reject({ 'code': NetParams.noTokenErrorCode, 'msg': 'token不存在' })
        }
    }
    // 设置网络超时时间
    let abort_fn = null;
    let abort_promise = new Promise(function(resolve,reject){
        abort_fn = ()=>{
            resolve({ 'code': NetParams.networkTimeout, 'msg': '网络超时,请稍后重试' })
        }
    })
    let response = fetch(getFullApiUrl(url), {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(params),
    });
    //转成json
    if (toJson) {
        response = response.then(res => (res.json())).catch(()=> ({'msg': 'API Error'}));
        response = response.then(res => addFunction(res));
    }
    if (logRequest) {
        response.then(text => console.log(text));
    }
    let race_list = [
        response,
        abort_promise
    ]
    break_promise && race_list.push(break_promise)
    let abortable_promise = Promise.race(race_list)
    setTimeout(()=>{
        abort_fn();
    }, timeout*1000);
    //返回结果
    return abortable_promise;
}

/**
 * 合并多个请求
 */
export function postMultipleJson(...requests) {
    let responses = [];
    if (requests instanceof Array) {
        //如果只有一个请求直接返回
        if (requests.length < 2) {
            return postJson(requests[0]);
        }
        for (let index in requests) {
            responses.push(postJson(requests[index]))
        }
        return new Promise.all(responses);
    } else {
        return postJson(requests)
    }
}

/**
 * 上传图片
 * @param url
 * @param filePath
 * @param fileName
 * @param toJson
 */
export async function postPicture({ url, filePath, fileName, params = {}, timeout=15, toJson = true }) {

    //获取用户token
    let tokens = await LocalTokenHandler.get()
    if (tokens){
        params.token = tokens.token
    }else{
        return Promise.reject({ 'code': NetParams.noTokenErrorCode, 'msg': 'token不存在' })
    }

    let formData = new FormData();
    //拼接参数  
    for (const key in params) {
        formData.append(key,params[key])
    }
    if(filePath){
        formData.append('file', {
            uri: filePath,
            type: 'multipart/form-data',
            name: fileName
        })
    }
    if (logRequest) {
        let logStr = 'method:POST';
        logStr += '\nurl:' + getFullApiUrl(url);
        logStr += '\nparams:' + JSON.stringify(formData);
        console.log(logStr);
    }
    // 设置网络超时时间
    let abort_fn = null;
    let abort_promise = new Promise(function(resolve,reject){
        abort_fn = ()=>{
            resolve({ 'code': NetParams.networkTimeout, 'msg': '网络超时,请稍后重试' })
        }
    })
    let res = fetch(getFullApiUrl(url), {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: formData,
    });
    //转成json
    if (toJson) {
        res = res.then(resp => resp.json()).catch(()=> ({'msg': 'API Error'}));
        res = res.then(res => addFunction(res));
    } else {
        res = res.then(res => res.text());
    }
    if (logRequest) {
        res.then(text => console.log(text));
    }
    let abortable_promise = Promise.race([
        res,
        abort_promise
    ])
    setTimeout(()=>{
        abort_fn();
    }, timeout*1000);
    //返回结果
    return abortable_promise;
}

//添加成功判断
function addFunction(res) {
    if (res.code == NetParams.networkTokenOutdatedCode) {
        //如果服务端返回300,说明token失效，并清除token,发送登出的通知
        LocalTokenHandler.remove(() => {
            DeviceEventEmitter.emit(NotificationKeys.kLogoutNotification)
        })
        res.isSuccess = false
        
        ToastManager.show(res.msg)
    } 
    else if(res.code == NetParams.networkTimeout){
        // 网络超时
        res.isSuccess = false
        res.msg = '网络超时'
    }
    else if(res.code == 0){
        //是否成功
        res.isSuccess = true
        //消息重新定义
        //res.msg = res.respMsg;
    }
    return new Promise(resolve => resolve(res));
}

/**
 * 填充路径
 * @param url
 * @return {*}
 */
export function getFullApiUrl(url) {
    //判断是否有值
    if (url) {
        let reUrl = /^(https?|ftp|file):\/\/.+$/;
        if (reUrl.test(url)) {
            return url;
        }else if(CurrentState == 'inner_ip' && netCustom?.host){
            return netCustom?.host + '/api' +url
        }else{
            return baseUrlApi + url;
        }
    }
    return '';
}

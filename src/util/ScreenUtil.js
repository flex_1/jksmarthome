/**
 * Created by zm on 2019/4/1.
 * 屏幕工具类
 * ui设计基准,iphone 6
 * width:750
 * height:1334
 */

/*
 设备的像素密度，例如：
 PixelRatio.get() === 1          mdpi Android 设备 (160 dpi)
 PixelRatio.get() === 1.5        hdpi Android 设备 (240 dpi)
 PixelRatio.get() === 2          iPhone 4, 4S,iPhone 5, 5c, 5s,iPhone 6,xhdpi Android 设备 (320 dpi)
 PixelRatio.get() === 3          iPhone 6 plus , xxhdpi Android 设备 (480 dpi)
 PixelRatio.get() === 3.5        Nexus 6       */

 /*
屏幕宽高：
414 x 896              ip xs Max && ip xr &&  11
375 x 812              ip x && ip xs
414 x 736              ip 8+,7+,6s+,6+
375 x 667              ip 8,7,6,6s
320 x 568              ip se,5,5s,5c
 */

import {
    Dimensions,
    PixelRatio,
    Platform,
    NativeModules,
    StatusBar
} from 'react-native';

const { StatusBarManager } = NativeModules;


export const deviceWidth = Dimensions.get('window').width;      //设备的宽度
export const deviceHeight = Dimensions.get('window').height;    //设备的高度
let fontScale = PixelRatio.getFontScale();                      //返回字体大小缩放比例

let pixelRatio = PixelRatio.get();      //当前设备的像素密度
const defaultPixel = 2;                           //iphone6的像素密度
//px转换成dp
const w2 = 750 / defaultPixel;
const h2 = 1334 / defaultPixel;
const scale = Math.min(deviceHeight / h2, deviceWidth / w2);   //获取缩放比例
/**
 * 设置text为sp
 * @param size sp
 * return number dp
 */
export function setSpText(size) {
    size = Math.round((size * scale + 0.5) * pixelRatio / fontScale);
    return size / defaultPixel;
}

export function scaleSize(size) {

    size = Math.round(size * scale + 0.5);
    return size / defaultPixel;
}

//2022新增 14Pro
const isIp14Pro = ()=>{
    if(Platform.OS == 'android'){
        return false
    }
    if(deviceWidth == 393 && deviceHeight == 852 && pixelRatio == 3){
        return true
    }
    return false
}

//2022新增 14ProMax
const isIp14ProMax = ()=>{
    if(Platform.OS == 'android'){
        return false
    }
    if(deviceWidth == 430 && deviceHeight == 932 && pixelRatio == 3){
        return true
    }
    return false
}

// 12ProMax(6.7英寸), 13 Pro Max , 14plus
const isIp12Max = ()=>{
    if(Platform.OS == 'android'){
        return false
    }
    if(deviceWidth == 428 && deviceHeight == 926 && pixelRatio == 3){
        return true
    }
    return false
}

// 12, 12Pro (6.1英寸) 13,13Pro,14
const isIp12 = ()=>{
    if(Platform.OS == 'android'){
        return false
    }
    if(deviceWidth == 390 && deviceHeight == 844 && pixelRatio == 3){
        return true
    }
    return false
}

// 11ProMax, XsMax（6.5英寸）
const isIpXMax = ()=>{
    if(Platform.OS == 'android'){
        return false
    }
    if(deviceWidth == 414 && deviceHeight == 896 && pixelRatio == 3){
        return true
    }
    return false
}

// 11, XR (6.1英寸)
const isIpXR = ()=>{
    if(Platform.OS == 'android'){
        return false
    }
    if(deviceWidth == 414 && deviceHeight == 896 && pixelRatio == 2){
        return true
    }
    return false
}

// 12mini (5.4英寸) , 11Pro, X, Xs (5.8英寸)
const isIpX = ()=>{
    if(Platform.OS == 'android'){
        return false
    }
    if(deviceWidth == 375 && deviceHeight == 812 && pixelRatio == 3){
        return true
    }
    return false
}

//2020年 12,12Pro,12ProMax 新机尺寸（需要重新做适配）
export const isIsland = isIp14Pro() || isIp14ProMax()   // 2022灵动岛屏幕
export const isNewiPhone = isIp12Max() || isIp12() || isIp14Pro() || isIsland
export const isIosFullScreen = isIpXR() || isIpXMax() || isIpX() || isNewiPhone

// 状态栏高度
export const StatusBarHeight = Platform.select({
    ios: isIosFullScreen ? (isIsland ? 54 : 44) : 20,
    android: StatusBar.currentHeight
})
// 导航栏 高度
export const NavigationBarHeight = Platform.select({
    ios: 44,
    android: 56
})
// 底部安全距离
export const BottomSafeMargin = Platform.select({
    ios: isIosFullScreen ? 34 : 0,
    android: 0
})
// 底部Tabbar高度
export const TabbarHeight = Platform.select({
    ios: 49,
    android: 50
})

// 新iPhone导航栏补偿高度 适配。(React-Navigation版本低，无法自动适配新iphone (从iphone12起))
export const iPhoneHeaderHeightOffset = isNewiPhone ? (isIsland ? 34 : 24) : 0


//屏幕适配相关
export const ScreenAdapt = {
    tabLabelFontSize: Platform.select({
        ios: 11,
        android: 12
    }),
    tabIconHeight: Platform.select({
        ios: 18,
        android: 24
    }),
    tabIconWidth: Platform.select({
        ios: 16,
        android: 26
    }),
    tabHeight: Platform.select({
        android: {
            height: 60
        }
    })
}








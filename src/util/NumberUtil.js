// 保留单位 且将数字 保留 到小数后 N位
const DecimalTheNumber = (x, u=2, hiddenUnit=false) => {
    if(!x){
        return null
    }
    let unit = x.replace(/[\d\.]/g,'')
    if(unit == 'null' || unit == null){
        return null
    }
    let number = x.replace(unit,'')
    let f = parseFloat(number)
    if (isNaN(f)) { 
        f = 0
    } 
    f = Math.round(f*Math.pow(10,u))
    f = f/(Math.pow(10,u)*1.0)
    if(hiddenUnit){
        unit = ''
    }
    return f + unit; 
}

// 获取UUID
const GenerateUUID = () => {
    var d = new Date().getTime();
    if (window.performance && typeof window.performance.now === "function") {
        d += performance.now();
    }
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
}

export {
    DecimalTheNumber,
    GenerateUUID
};
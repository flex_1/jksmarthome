import React, { Component } from 'react';
import { View, StyleSheet, Platform } from 'react-native';
import renderChart from './renderChart';
import echarts from './echarts.min';
import { WebView } from 'react-native-webview';

export default class App extends Component {

  constructor(props) {
    super(props);
    this.setNewOption = this.setNewOption.bind(this);
  }
  

  componentWillReceiveProps(nextProps) {
    // if(nextProps.option !== this.props.option) {
    //   this.refs.chart.reload();
    // }
  }

  setNewOption(option) {
    this.chart.postMessage(JSON.stringify(option));
  }

  render() {
    let htmlSource = Platform.OS === 'ios' ? require('./tpl.html') : {uri:'file:///android_asset/tpl.html'}
    return (
      <View style={{flex: 1,height: this.props.height || 400,}}>
        <WebView
          ref={e => this.cahrt = e}
          scrollEnabled = {false}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          injectedJavaScript = {renderChart(this.props)}
          style={{
            height: this.props.height || 400,
            backgroundColor: this.props.backgroundColor || 'transparent',
          }}
          scalesPageToFit={Platform.OS !== 'ios'}
          originWhitelist={['*']}
          source={htmlSource}
          onMessage={event => this.props.onPress ? this.props.onPress(JSON.parse(event.nativeEvent.data)) : null}
        />
      </View>
    );
  }
}

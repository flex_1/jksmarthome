import getUrls from './APIs';

//通知 keys
export const NotificationKeys = {
    kLoginNotification: 'loginNotification', //登录通知
    kLogoutNotification: 'logoutNotification', // 登出通知

    // 首页
    kHouseInfoNotification: 'HouseInfoChangeNotification', //房屋信息变化通知
    // 智能
    kSmartListChangeNotification: 'SmartListChangeNotification', // 智能列表变化通知
    // 房间
    kRoomInfoNotification: 'RoomInfoChangeNotification', //房间信息变化通知
    kRoomFloorChangeNotification: 'RoomFloorChangeNotification', // 房间楼层发生变化
    //场景
    kSceneFloorNotification: 'SceneFloorChangeNotification', //场景楼层发生变化的通知
    kSceneInfoNotification: 'SceneInfoChangeNotification', //场景信息变化时的通知
    // 设备
    kDeviceFloorNotification: 'DeviceFloorNotification', //设备列表 楼层发生
    kDeviceStatusNotification: 'DeviceStatusChangeNotification', //设备列表中的 设备状态 变化时的通知
    kDeviceManageListNotification: 'DeviceManageListNotification', //设备管理列表 通知

    //所属网关
    kGatewayNeedUpdate: 'GatewayNeedUpdateNotification',  //所属网关页面更新
    
    //开关 绑定成功
    kSwitchSuccessBidingNotification: 'SwitchSuccessBidingNotification', //开关成功绑定的通知

    //websocket 通知处理
    kWebsocketDeviceNotification: 'WebsocketDeviceNotification', // websocket 设备状态变化通知
    kWebsocketReconnectNotification: 'kWebsocketReconnectNotification', // websocket 重连通知
    kWebsocketCloseNotification: 'kWebsocketCloseNotification', 

    //与原生的 交互通知
    kRNShortCutsNotification: 'kRNShortCutsNotification', //设置shortcuts通知内容
    kRNAddSiriSuccess: 'kRNAddSiriSuccess', //添加siri快捷指令成功

    //语音
    kEnterVoiceSetting: 'kEnterVoiceSetting',  //从语音对话框进入语音设置页面的通知

    //推送通知
    kJpushMessageNotification: 'kJpushMessageNotification', //推送通知
}

//本地存储的 keys
export const LocalStorageKeys = {
    kDeviceSearch: 'DeviceSearchHistoryKeys',
    kSceneSearch: 'SceneSearchHistoryKeys',
    kAppGuidePageInfo: 'AppGuidePageInfoKeys',  //开机引导页
    kLoginAgreement: 'LoginProtocolAgreement',  //登录页面 用户协议与隐私政策勾选
    kLastLoginAccount: 'LastLoginAccount',  //记录上次登录的账号
    kAppThemeIsDark: 'AppThemeIsDark', // App主题
    kStorageAccountNumber: 'StorageAccountNumber', // 登录成功后,
    kPrivacyProtocol: 'PersonalPrivacyProtocolShown',  //个人信息保护指引 弹框
    kWifiSsidPassword: 'WifiSsidAndPassWord',  //配网模块 wifi名称与密码,
    kCustomNetworkParamsInfo: 'CustomNetworkParamsInfo',  //自定义的网络设置
}

/**
 *  内网:   inner_ip
 *  本地:   local
 *  测试:   pre
 *  生产:   production
 */ 
export const CurrentState = 'pre'  // 改变此参数 来切换环境


/******************************  请勿修改 **************************/
let host = ''                                      
let wsHost = ''
let speechHost = ''
if(CurrentState == 'inner_ip'){                        
    host = 'http://10.20.30.33:2093'
    wsHost = 'ws://10.20.30.77:2090'
    speechHost = 'http://192.168.212.53:1488'
}else if(CurrentState == 'pre'){                       
    // 测试
    host = 'https://api-test.hansa-tec.com'           
    wsHost = 'wss://websocket-test.hansa-tec.com'
    speechHost = 'https://speech-test.hansa-tec.com/'         
}else if(CurrentState == 'production'){                         
    // 生产                                   
    host = 'https://smarthouse.hansa-tec.com'                  
    wsHost = 'ws://websocket.hansa-tec.com'
    speechHost = 'https://speech.hansa-tec.com'
}else if(CurrentState == 'local'){
    // 本机本地                        
    host = 'http://localhost:2093'
    wsHost = 'ws://websocket-test.hansa-tec.com'
    speechHost = 'http://192.168.212.53:1488'
}                                                               
/****************************  请勿修改 *****************************/


//网络参数
export const NetParams = {
    baseUrl: host, 
    wsBaseUrl: wsHost, // websocket
    speechHost: speechHost, //语音网址
    noTokenErrorCode: -1001,  //token不存在
    networkTimeoutCode: -1003, // 网络超时 错误码
    networkTokenOutdatedCode: '300', // Token失效
    networkBreak: -1010, // 网络被人为中止

    networkTimeout: 15, //普通网络请求时 超时时间
    networkUserReactTimeoutTime: 5, //用于操作设备时交互反馈时间
    networkTransferOwner: 30,  //用户过户时 超时时间
}

//网络请求URL
export const NetUrls = getUrls(speechHost, host);

export const Colors = {
    newTheme: '#25cad2', // 新的主题色，绿色
    themeBG: '#2CC7BC', // 主题色
    themeBGInactive: '#CCCCCC', // 主题暗色
    themBGLightGray: '#F6F6F6', //背景浅灰
    themeTextBlack: '#1E1E1E', //文字颜色 黑
    themeTextLightGray: '#999999', // 文字颜色 浅灰
    themeTextBlue: '#1D94FE', //主题蓝色
    themBgRed: '#FD364B', //主题红
    themeTipsYellow: '#FFAE2B', // 提示黄色
    themPurpe: '#6847F1', // 主题紫色

    answerTextColorGray: '#666666', //语音 回答 浅灰 文字
    splitLightGray: '#EEEEEE', //分割线浅灰色
    tabActiveColor: '#2650D7', //tab被点亮时候的颜色
    borderLightGray: '#E5E5E5', //边框灰色
    white: '#fff', //白色
    black: '#000000', //黑色
    themeNotifiBadgeBg: '#FF9600', // 通知角标颜色
    inputTextColor: '#F7F7F7', // 输入框颜色
    middleGray: '#B7B7B7', // 中灰色
    switchBindBg: '#4689F8', //浅蓝色
    newTextGray: '#969799',  //文字浅灰 新
    newBtnBlueBg: '#008CFF',  //新按钮背景色 蓝

    transparency: 'transparent', // 全透明,
    translucent : 'rgba(0,0,0,0.5)', //半透明底色
    shallowTrans: 'rgba(0,0,0,0.3)', //浅透明
}

// Websocket Tpye Schemes
export const WebsocketKeys = {
    newHouse: 'new_house',  // 当前房屋设备为空 是否弹框去
    newDeviceData: 'device_data',  //设备状态更新
    newEnviData: 'envi_data', //环境参数更新
    newPowerData: 'power_data',  //能耗数据 更新
    houseBeChanged: 'change_house',  //房屋被切换
    floorBeRefeshed: 'refresh_floor', //楼层备切换,
    disableMember: 'disabled_member',  //成员失效
}


// 设备对照表
export const GetdeviceRouter = (tag)=>{
    switch (tag) {
        case 'ventilation': //新风
            return 'Ventilation'

        case 'shutter': //卷帘
            return 'ScrollCurtains'

        case 'curtain'://窗帘
        case 'window'://窗纱
            return 'WindowCurtains'
            
        case 'tv'://电视
            return 'Television'

        case 'ri-air-condition'://红外空调
        case 'air-condition'://空调
            return 'AirConditioner'

        case 'music'://音乐
            return 'Music'

        case 'health'://健康
            return 'Healthy'
           
        case 'tape-light-16': // 灯控
        case 'light':
            return 'LightPanel'

        case 'relay-light':  // 开关
            return 'SwitchPannel'

        case 'camera': //摄像头 需要特殊处理(进原生页面)
            return 'Camera'
        
        case 'air-switch':  //空气开关
            return 'AirSwitch'

        case 'rf':  // 射频
            return 'RFController'

        case 'ri':  // 红外
            return 'RIController'
        
        case 'virtual': //遥控器
        case 'cisco': // 思科
            return 'RemoteControl'

        case 'lora': 
            return 'LoraPannel'

        case 'flash-light':
            return 'FlashLightWebPannel'

        case 'underfloor-heating':  //地暖
            return 'FloorHeating'
        
        case 'color-light':
            return 'ColorLightPanel'

        case 'CCT-light':  // 色温灯
            return 'DoubleLightPanel'

        case 'brand-tv': //品牌电视
            return 'TVRemoteController'
        
        case 'relay-1p': // 1P继电器
            return 'OnePRelay'
        
        case 'MPPT-solar': //太阳能充电
            return 'SolarEnergy'

        case 'louver'://百叶帘
            return 'ScrollAngleCurtains'
        
        case 'sensor-merge':  //传感器类型
            return 'Sensor'
        
        case 'ri-study':  // 红外学习
            return 'RISelfLearn'

        case 'door':  // 门锁
            return 'DoorLock'

        case 'door-yale':  // 指纹密码锁
            return 'DoorYale'

        case 'lifter':
            return 'Elevator'
        
        case 'loudspeaker': //音乐-音箱
            return 'VoiceBox'

        case 'dehumidifier':  //除湿机
            return 'Dehumidifier'

        case 'midea-air-condition':  //美的中央空调
            return 'AirConditioner'

        case 'out-air-condition':
            return 'AirOutdoorUnit'  //空调室外机

        case 'thirdparty-tv-iptv':
            return 'IPTVController'  // IPTV遥控器

        case 'cupboard-drawer':
            return 'Cupboard'    //橱柜

        case 'cupboard-updown':
            return 'ScrollCurtains'  // 橱窗（上下门）

        case 'shutter-wifi':
            return 'ScrollCurtains'

        case 'panel':
            return 'PanelButtonBind'  // 面板绑定
        
        case 'robot':
            return 'RobotHome'  // 机器人
      
        case 'zigbee-panel':
        case 'wifi-485-panel':
            return 'SmartPanel'   //8键面板

        case 'instruction-protocol':  //通讯协议器
            return 'ProtocolController'

        case 'zigbee-door': 
            return 'ZigbeeDoorLock'   // zigbee门锁

        case 'RF433-window':   
            return 'RadioCurtains'   //杜亚射频窗帘
        
        case 'trailer':
            return 'Invertor'    //逆变器

        case 'coulometer':
            return 'Coulometer'   //库仑计

        case 'zigbee-smoke-alarm':
            return 'ZigbeeSmokeAlarm'  //烟雾报警器

        case 'zigbee-human-illuminance':
            return 'ZigbeeHumanInduction'   //人体感应

        case 'zigbee-air-quality':
            return 'ZigbeeAirQuality'    // zigbee空气质量监测

        case 'box':
            return 'SmartVoiceBox'     //智能音箱

        case 'window-group':
            return 'CombineWindow'   //左右窗户
        
        case 'dream-shutter':
            return 'DreamShutter'    //梦幻窗帘

        case 'h5':                //h5 控制页面
            return 'WebPannel'
        
        case 'ble-light':                
        case 'ble-group-light':        //蓝牙组合灯
            return 'BleGroupLight'

        default:
            return null
    }
}

import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity, 
    DeviceEventEmitter,
    Dimensions
} from 'react-native';
import HeaderLeft from '../../component/CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../component/CommonPage/Navigation/HeaderBackground';


const screenW = Dimensions.get('screen').width;
const screenH = Dimensions.get('screen').height;


class ScrollViewEnable extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'SVGDemo'}/>,
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
	}

	constructor(props) {
		super(props);

        this.state={
            
        }
	}

    onScroll(){

    }

	renderScrollView(){
        return(
            <ScrollView
		        
		        pagingEnabled={true}
		        showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
		        onScroll={this.onScroll}
            >
                <View style={[styles.serviceItem,{backgroundColor: 'pink'}]}>
                    <ScrollView
                        style={{width: '100%'}}
                        pagingEnabled={false}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={{backgroundColor: 'blue',width:'100%',height: screenH * 2,justifyContent:'center',alignItems:'center'}}>
                            <Text>第一页吗爱妈妈吗</Text>
                        </View>
                    </ScrollView>
                </View>
                <View style={[styles.serviceItem,{backgroundColor: 'red'}]}>
                    <Text>第2页</Text>
                </View>
                <View style={[styles.serviceItem,{backgroundColor: 'yellow'}]}>
                    <Text>第3页</Text>
                </View>
		    </ScrollView>
        )
    }
	
	render() {
		return (
			<View style={styles.container}>
                {this.renderScrollView()}
            </View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#F6F6F6',
        flex: 1
	},
    serviceItem:{
        height: screenH - 88, 
        justifyContent: 'center',
        alignItems: 'center'
    }
})


export default ScrollViewEnable

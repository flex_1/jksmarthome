import React, { Component } from 'react';
import { 
    Platform, 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity, 
    DeviceEventEmitter,
    TextInput
} from 'react-native';
import { Colors,NetParams,LocalStorageKeys,NotificationKeys, CurrentState} from '../../common/Constants';
import AsyncStorage from '@react-native-community/async-storage';
import LocalTokenHandler from '../../util/LocalTokenHandler';
import HeaderLeft from '../../component/CommonPage/Navigation/HeaderLeft';
import HeaderBackground from '../../component/CommonPage/Navigation/HeaderBackground';
import netCustom from '../../util/NetworkCustom';
import ToastManager from '../CustomComponent/ToastManager';

// 调试页面（只有在测试环境下才会出现）
class DebugPage extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: <HeaderLeft navigation={navigation} backTitle={'调试页面'}/>,
            headerBackground: <HeaderBackground hiddenBorder={true}/>
        }
	}

	constructor(props) {
		super(props);

        this.state={
            host : netCustom?.host || '',
            wsHost :  netCustom?.wsHost || '',
            speechHost : netCustom?.speechHost || '' 
        }
	}

	getAppBaseUrl(){
		let base1 = NetParams.baseUrl
        let base2 = NetParams.wsBaseUrl
        let base3 = NetParams.speechHost

        if(CurrentState == 'inner_ip' && netCustom.host){ 
            base1 = netCustom.host || '空'
            base2 = netCustom.wsHost || '空'
            // base3 = netCustom.speechHost || '空'
        }

		return(
			<View style={{paddingHorizontal: 16,marginTop: 20}}>
				<Text style={{fontSize:15}}>当前网络环境:</Text>
				<Text style={{fontSize:15,marginTop:5}}>{base1}</Text>
                <Text style={{fontSize:15,marginTop:5}}>{base2}</Text>
                <Text style={{fontSize:15,marginTop:5}}>{base3}</Text>
			</View>
		)
	}
	
	getGuipageButton(){
		return(
			<View style={{paddingHorizontal: 16,paddingVertical: 20,paddingHorizontal:30}}>
				<TouchableOpacity activeOpacity={0.7} style={styles.guideBtnTouch} onPress={()=>{
					LocalTokenHandler.remove(()=>{
						AsyncStorage.removeItem(LocalStorageKeys.kAppGuidePageInfo)
						DeviceEventEmitter.emit(NotificationKeys.kLogoutNotification)
					})
				}}>
					<Text style={{color: Colors.white,fontSize: 14}}>登出,且下次进入App显示开机图片</Text>
				</TouchableOpacity>
			</View>
		)
    }
    
    getVoicePromtButton(){
		return(
			<View style={{paddingHorizontal: 16,paddingVertical: 20,paddingHorizontal:30}}>
				<TouchableOpacity activeOpacity={0.7} style={styles.guideBtnTouch} onPress={()=>{
					AsyncStorage.removeItem(LocalStorageKeys.kPrivacyProtocol)
				}}>
					<Text style={{color: Colors.white,fontSize: 14}}>清除个人信息指引弹框的Key</Text>
				</TouchableOpacity>
			</View>
		)
	}

    getSVGDemo(){
		return(
			<View style={{paddingHorizontal: 16,paddingVertical: 20,paddingHorizontal:30}}>
				<TouchableOpacity activeOpacity={0.7} style={styles.guideBtnTouch} onPress={()=>{
					this.props.navigation.navigate('SVGDemo')
				}}>
					<Text style={{color: Colors.white,fontSize: 14}}>SVGDemo</Text>
				</TouchableOpacity>
			</View>
		)
	}

    getScrollViewEnable(){
		return(
			<View style={{paddingHorizontal: 16,paddingVertical: 20,paddingHorizontal:30}}>
				<TouchableOpacity activeOpacity={0.7} style={styles.guideBtnTouch} onPress={()=>{
					this.props.navigation.navigate('ScrollViewEnable')
				}}>
					<Text style={{color: Colors.white,fontSize: 14}}>ScrollViewEnable</Text>
				</TouchableOpacity>
			</View>
		)
	}

    getNetworkInput(){
        if(CurrentState != 'inner_ip'){
            return null
        }
        
        return(
            <View style={{paddingHorizontal: 16}}>
                <View style={{flexDirection: 'row',alignItems: 'center'}}>
                    <Text style={{fontSize: 15, fontWeight: 'bold'}}>host:  </Text>
                    <TextInput
                        defaultValue={this.state.host}
                        style={{flex: 1, height: 50,backgroundColor: 'pink'}}
                        placeholder={'一定要加端口号'}
                        onChangeText={(text)=>{
                            this.setState({
                                host: text
                            })
                        }}
                    />
                </View>
                <View style={{flexDirection: 'row',alignItems: 'center',marginTop: 20}}>
                    <Text style={{fontSize: 15, fontWeight: 'bold'}}>wsHost: </Text>
                    <TextInput
                        defaultValue={this.state.wsHost}
                        style={{flex: 1, height: 50,backgroundColor: 'pink'}}
                        placeholder={'一定要加端口号'}
                        onChangeText={(text)=>{
                            this.setState({
                                wsHost: text
                            })
                        }}
                    />
                </View>
                {/* <View style={{flexDirection: 'row',alignItems: 'center',marginTop: 20}}>
                    <Text style={{fontSize: 15, fontWeight: 'bold'}}>speechHost:  </Text>
                    <TextInput
                        defaultValue={this.state.speechHost}
                        style={{flex: 1, height: 50,backgroundColor: 'pink'}}
                        placeholder={'暂时没用!'}
                        onChangeText={(text)=>{
                            this.setState({
                                speechHost: text
                            })
                        }}
                    />
                </View> */}
                <TouchableOpacity activeOpacity={0.7} style={[styles.guideBtnTouch,{marginTop: 15}]} onPress={()=>{
					AsyncStorage.removeItem(LocalStorageKeys.kCustomNetworkParamsInfo)
                    netCustom.clear()
                    ToastManager.show('清除成功')
				}}>
					<Text style={{color: Colors.white,fontSize: 14}}>清除</Text>
				</TouchableOpacity>
                <TouchableOpacity activeOpacity={0.7} style={[styles.guideBtnTouch,{marginTop: 15}]} onPress={()=>{
                    if(CurrentState != 'inner_ip'){
                        ToastManager.show('非本地环境')
                        return
                    }
                    if(!this.state.host){
                        ToastManager.show('BaseUrl 不能为空')
                        return
                    }
                    if(this.state.wsHost){
                        var url = this.state.wsHost
                        var reg=/ws(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?/
                        if(!reg.test(url)){
                            alert("websocket 地址不合法")
                            return
                        }
                    }
                    let netParams = {
                        host: this.state.host || '',
                        wsHost: this.state.wsHost || '',
                        speechHost: this.state.speechHost || '',
                    }
                    netCustom.init(netParams)
					AsyncStorage.setItem(LocalStorageKeys.kCustomNetworkParamsInfo, JSON.stringify(netParams))
                    ToastManager.show('保存成功')
				}}>
					<Text style={{color: Colors.white,fontSize: 14}}>保存</Text>
				</TouchableOpacity>
            </View>
        )
    }
	
	render() {
		return (
			<ScrollView style={styles.container}>
				{this.getAppBaseUrl()}
				<View style={styles.split}/>
				{/* {this.getGuipageButton()} */}
                {this.getVoicePromtButton()}
                <View style={styles.split}/>
                {this.getNetworkInput()}
                {this.getSVGDemo()}
                {this.getScrollViewEnable()}
			</ScrollView >
		)
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.white
	},
	split:{
		width:'100%',
		height:1,
		backgroundColor: Colors.splitLightGray,
		marginVertical: 10
	},
	guideBtnTouch:{
		width:'100%',
		height:40,
		backgroundColor:Colors.themeBG,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 4
	}
	
})


export default DebugPage

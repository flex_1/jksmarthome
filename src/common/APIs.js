
//网络请求URL
const Apis = {
    /* 登录注册模块 */
    login: '/public/login',  //登录
    logout: '/account/logout', //登出
    register: '/public/register',  //注册
    getMsg: '/public/getMsg', //获取短信验证码
    forgetPwd: '/public/forgetPwd', //忘记密码
    getProtocol: '/public/getProtocol', //获取协议
    appVersion: '/controller/appVersionCheck', // APP 版本信息
    thirdLogin: '/account/thirdLogin', //第三方登录
    thirdLoginBind: '/account/thirdLoginBind', // 绑定手机号
    loginByMsg: '/public/loginByMsg', // 验证码登录
    thirdUnbind: '/account/thirdUnBind', //第三方平台 解绑
    bindJpushId: '/jpush/bind', //绑定 registerID

    /* 首页 设备模块 */
    houseInfo: '/home/houseInfo', //房屋信息
    houseScene: '/home/scene', // 首页房屋场景
    houseAddress: '/house/get', //获取房屋地址信息
    houseList: '/house/list', // 房屋列表
    addHouse: '/house/add', //添加房屋
    deviceType: '/home/deviceType', // 首页 设备类型
    houseUpdate: '/house/update', //更改房屋图片 formData 接口
    chooseHouse: '/house/changeUpdate', // 选择房屋
    loadImgae: '/load/up', //上传图片
    addDevice: '/device/addDevice', // 手动添加设备
    checkSeriaNo: '/device/seriaNo', //检查序列号存不存在
    setDevice: '/device/setDevice', // 获取device设置
    saveDevice: '/device/saveDevice', // 保存device设置
    deletDevice: '/device/del', //删除设备
    deletDeviceV2: '/device/delV2',  //删除设备新接口
    deviceList: '/device/list_v3', //设备列表_V3
    getThumbList: '/content/getPicture', //获取图片列表
    deviceUpdate: '/device/update', // 设备列表中 更改设备 开关 
    voltage: '/device/voltage', //设备电压设置
    electricity: '/device/electricity', //获取设备用电量
    operationlog: '/account/operationlogV2', // 获取 场景或者设备日志
    deleteHouse: '/house/del', //删除房屋
    clearData: '/account/cleardata', //清除数据
    listSecond: '/device/listSecondV2', // 设备 二级列表,
    houseSetting: '/house/diagram', // 房间设置，formData 接口
    houseLayout: '/house/listDiagram', // 房屋布线图
    lockDevice: '/device/lock', // 设备锁定与解锁,
    airQuality: '/house/airQuality', //空气质量监测
    deleteDiagram: '/house/deleteDiagram', // 房屋布线图
    homeCustomerList: '/home/infoTop10Favorite', // 首页收藏列表
    energyTotal: '/house/energyTotal', // 房屋总能耗
    deviceFloor: '/device/listFloorV2', // 设备楼层
    switchBiding: '/device/binding', // 开关绑定
    bindingUpdate: '/device/bindingUpdate', //开关更新
    unbinding: '/device/bindingUntie', //开关解绑
    bindingResult: '/device/bindingResult', //开关绑定状态
    shareList: '/device/share/list', //设备分享列表
    addShare: '/device/share/add', // 创建分享
    shareSetting: '/device/share/update', //修改分享内容
    deleteShare: '/device/share/delete', //删除分享
    updateShare: '/device/share/status', //更改分享状态
    airQualityConfig: '/house/airQuality/config', // 空气质量参数
    saveAirConfig: '/house/airQuality/save', //保存空气质量偏好设置
    sceneSelectDeviceList: '/device/listV4', //场景选择设备列表
    deviceLog: '/device/log', //设备日志
    airQualityChart: '/house/airQualityChart', //新环境监测接口
    homePageDevice: '/house/homePageDevice', //修改默认监测设备
    energyTotalRanking: '/house/energyTotalRanking', //房屋能耗 排行
    dependencyList: '/device/dependencyList', //设备依赖列表
    chooseDpyDevice: '/device/chooseDpyDevice', //选择依赖设备
    saveDpyAttribute: '/device/saveDpyAttribute', //添加删除属性
    getAttribute: '/device/getAttributes', //获取设备属性
    disableAttributes: '/device/disableAttributes', //停用属性
    mideaAuthUrl: '/midea/getLoginUrl', //获取美的授权登录接口
    mideaAuthList: '/midea/userList',  //授权列表
    mideaSyncDevice: '/midea/syncDevice', //同步设备
    mideaCancelAuth: '/midea/cancelAuth',  //美的取消授权
    getHumanTime: '/device/getHumanTime',  //获取无人时间
    bindWifiDevice: '/device/bindWifiDevice',  //绑定 wifi 设备
    getWifiDeviceType: '/device/getWifiDeviceType', // 获取分类
    boxControlRecord: '/device/boxControlRecord',  //音箱控制记录
    
    /* 设备控制 面板 */
    getDeviceStatus: '/device/getDeviceStatus', //获取设备 初始状态
    airConditioner: '/device/control/airConditioner', //空调设置
    television: '/device/control/television', // 电视机控制
    curtain: '/device/control/curtain', //窗帘控制
    music: '/device/control/music', //音乐
    light: '/device/control/light', //灯光
    CCTLight: '/device/control/CCTLight', //双色温灯
    colorLight: '/device/control/colorLight', //灯光
    groupLight: '/device/control/Relay', //排灯
    ventilation: '/device/control/ventilation', //新风
    healthy: '/device/control/healthy',
    bindingList: '/device/bindingList',//绑定设备列表
    bindingOptions: '/device/bindingOptions',//绑定设备选项字典接口
    riNumber : '/device/control/riNumber', //红外
    rfNumber: '/device/control/rfNumber', //射频
    getVirtualButton: '/device/getVirtualButton', //获取遥控器按钮
    virtualButton: '/device/control/virtualButton', //遥控器控制
    underfloor: '/device/control/underfloor', //地暖
    getTVBrand: '/device/getTVBrand', //获取电视机品牌信息
    getTVButtonByBrand: '/device/getTVButtonByBrand', //根据电视品牌获取按钮信息
    ctrTV: '/device/control/TV',  //设备控制-电视机
    getRelay1pInfo: '/device/getRelay1pInfo', //配置页面数据加载接口
    setRelay1p: '/device/control/relay1p', //设置1p继电器参数
    solarEnergy: '/device/control/solar', //太阳能控制面板
    lora: '/device/control/lora',  // 控制 lora
    controlCommon: '/device/controlCommon', //设备控制接口(所有设备，统一接口)
    getMergeDeviceStatus: '/device/getMergeDeviceStatus', //获检测类设备状态
    createRIStudyDevice: '/device/createRIStudyDevice', //创建自定义设备
    saveRIStudyButton: '/device/saveRIStudyButton', //修改按钮
    deleteRIStudyButton: '/device/deleteRIStudyButton', //删除按钮
    bindingRIStudy: '/device/bindingRIStudy', // 红外学习 绑定
    bindingRIStudyResult: '/device/bindingRIStudyResult', // 红外学习 绑定状态查询
    natural_classification: '/device/naturalClassification', //自然分类
    checkmark: '/device/checkmark', //快速学习,
    changeDoorPwd: '/device/changeDoorPwd',  // 删除或修改 门锁密码
    setDeviceProperties: '/device/setDeviceProperties', //窗帘正反 色温灯范围
    outAirSettingInfo: '/device/getOutAirConditionConfig', //美的空调 参数配置
    panelView: '/device/panelView',  //设备绑定面板
    robotPage: '/device/robotPage', //机器人首页
    getRobotStation: '/device/getRobotStation', //获取机器人站点信息
    robotTaskDetail: '/device/robotTaskDetail', //机器人订单详情
    robotTaskCancel: '/device/robotTaskCancel', //机器人订单 取消
    createRobotTask: '/device/createRobotTask', //机器人 创建订单
    robotTaskList: '/device/robotTaskList',  //机器人订单 列表
    virtualButtonName: '/device/virtualButtonName',  //修改虚拟遥控器 别名
    shutterAddress: '/device/shutterAddress',  //窗帘行程
    panelLedOperation: '/device/panelLedOperation',  //wifi面板操作按钮
    getRobotList: '/device/getRobotList', //获取楼宇下有序机器人列表
    getRobotCurrentArea: '/device/getRobotCurrentArea', //获取单个机器人的位置数据
    getRobotMapByMapId: '/device/getRobotMapByMapId', //根据地图id获取地图数据
    zigbeeBtnList: '/device/getZigbeePanelButtonList',  //zigbee面板按钮列表
    getCurrentZigbeePanelButtonList: '/device/getCurrentZigbeePanelButtonList',   //获取当前zigbee面板按钮
    controlBackLightMode: '/device/controlBackLightMode',  //控制zigbee面板 背光灯
    
    /* 房间 模块 */
    roomList: '/room/listV2', //房屋列表
    addRoom: '/room/addRoom', // 添加 修改房间
    roomDetail: '/room/roomDetail', //房屋详情
    deleteRoom: '/room/delete', // 删除房间
    bindDevicesToRoom: '/device/more', //批量添加设备到房间
    roomFloor: '/room/listFloorV2', // 获取房间 楼层
    roomlistDetails: '/room/list_v3', //根据楼层 获取房间 （新接口）
    speechControllArea: '/room/speechControllAreaV2', //获取语音控制区域
    saveVoiceRooms: '/room/saveSpeechControl', //保存房间设置
    getSpeechBroadcast: '/room/getSpeechBroadcast', //获取语音播放开关
    getSpeechSetting: '/room/getSpeechSetting', //语音答复 与 语音唤醒
    setBroadcast: '/room/isSpeechBroadcast', //开启关闭 语音答复
    energyType: '/room/energyType', //设置 日周月年 能耗
    roomSort: '/room/roomSort',  // 房间排序

    /* 场景 模块 */
    sceneList: '/scene/list_v3',   //场景列表
    addScene: '/scene/addScene', // 添加场景 && 修改场景
    delScene: '/scene/delScene', //删除场景
    getScenePicture: '/content/getScenePicture', //获取场景图片列表
    getTimer: '/scene/getTimer', // 获取场景定时器
    addTimer: '/scene/addTimer', // 创建 修改 定时器
    sceneFloor: '/scene/listFloorV2', //有场景的 楼层,
    sceneInfo: '/scene/getScene', //获取场景信息
    delTimer: '/scene/delTimer', //删除定时器
    updateSceneStatus: '/scene/updateStatus', //更改场景状态,
    getSceneDetail: '/scene/getSceneDetail', //获取场景组合
    addSceneDetail: '/scene/addSceneDetail', // 创建修改 场景组合
    delSceneDetail: '/scene/delSceneDetail', //删除场景组合
    setTopScene: '/scene/setIstop', //场景置顶
    getRoomPicture: '/content/getRoomPicture', //房间默认图片
    sceneCopyDevice: '/scene/copySceneDetail', //场景 复制设备
    rmdSceneList: '/scene/recommand/list', // 推荐场景列表
    rmdSceneDetail: '/scene/recommand/detail', //推荐场景 详情
    addRmdSceneDetail: '/scene/recommand/create', //推荐场景创建
    rmdSceneAddDevice: '/scene/recommand/chooseDevice', //推荐场景添加设备
    copyScene: '/scene/copyScene', //复制场景
    labelList: '/scene/labelList', //别名列表
    addLabels: '/scene/addLabels', //添加别名
    addSmartToScene: '/scene/create',  //添加智能到场景
    sceneSmartList: '/scene/getIntelligentList', //获取场景关联智能
    allSmartForScene: '/scene/intelligentList',  //供场景选择的智能列表
    updateSceneIntelligent: '/scene/updateSceneIntelligent',  //场景中智能 停启用 操作
    delSceneIntelligent: '/scene/delSceneIntelligent',  //删除场景中的 智能

    /* 我的 模块 */
    userInfo: '/account/homePage', //用户信息
    updatePassword: '/account/updatePassword', //修改密码
    memberList: '/account/memberPermission/list', //成员列表
    commonInfo: '/account/commonInfo', //通用设置
    saveCommon: '/account/saveCommon', //保存通用设置
    homePage: '/account/homePage', //我的页面
    deleteMember: '/account/memberPermission/delete', //删除成员
    messageList: '/account/message/list', //消息列表
    batchReadMessage: '/message/batchReadMessage',  //消息批量已读
    singleReadMessage: '/message/singleReadMessage', //消息单条已读
    messageCenter: '/message/list', //消息中心
    msgListV3: '/message/list_v3',  //新消息中心
    getConfig: '/message/getConfig_V2', //获取消息配置
    setConfig: '/message/setConfig_V2', //设置消息配置
    listLevel2: '/message/listLevel2', //消息详情列表
    listLevelV3: '/message/listLevel_v3',  //设备消息列表
    createAndUpdate: '/account/memberPermission/createAndUpdate', //创建修改成员，保存权限
    homeOwner: '/account/homeOwner', //过户
    updatePhoto: '/account/updatePhoto', //上传头像
    energy: '/account/energy', //能源
    updateUserThumb: '/account/updatePhoto', //修改用户头像
    updateUserNick: '/account/updateInfo', //修改用户名称
    elecPriceSetting: '/house/electrovalence', //电价设置
    getElectricPrice: '/house/electrodetails', //获取电价 
    debugRequest: '/debug/request', //申请设备调试
    debugRecordList: '/debug/requestList', //申请记录
    sceneLog: '/scene/log', //场景日志
    jpushStatus: '/account/jpushStatus', //是否接受消息推送
    getJpushStatus: '/account/getJpushStatus', //获取接受消息推送的状态
    getFavoriteList: '/favorite/list', //获取收藏列表
    collect: '/favorite/add', //收藏 取消收藏
    loginRecord: '/account/loginRecord', //登录设备管理
    memberLoginRecord: '/account/memberLoginRecord_V2', //成员登录记录
    memberLoginRecordOperation: '/account/memberLoginRecordOperation', //停用拉黑
    changePhoneNumber: '/account/changePhoneNumber', //（换）绑定手机号
    deleteMemberLoginRecord: '/account/deleteMemberLoginRecord',  //删除成员记录
    siriList: '/scene/siriList', //siri列表
    getAddress: '/house/getAddress', //获取房屋地址
    cancellation: '/account/cancellation', //注销账号
    getWechatServiceAccount: '/account/getWechatServiceAccount',  //获取微信二维码
    deleteMessage: '/message/deleteMessage',  //删除消息
    readMessage: '/message/readMessage',  //读取消息
    readAllMessage: '/message/readAllMessage',  //消息 全部已读
    relay1pList: '/device/relay1pList',  //设备通知列表
    deviceJpushStatus: '/device/deviceJpushStatus',

    /* 智能 模块*/
    conditionDeviceList: '/intelligent/conditionDeviceList_v2', //智能 条件 设备列表
    getConditionByDeviceId: '/intelligent/getConditionByDeviceId_v2', // 智能 设备 获取条件
    smartList: '/intelligent/list', //智能列表接口
    smartDetail: '/intelligent/detail', //智能 详情
    smartSave: '/intelligent/save', //智能 保存
    smartActive: '/intelligent/activitedOrInactivate', //智能 停启用
    smartExecuteDevice: '/intelligent/executeTypeV2',   //智能执行设备列表
    smartExecuteList: '/intelligent/executeType',   //智能场景列表（v1 没分页）
    floorAndRoomList: '/home/floorAndRoomV2',   //智能场景筛选列表
    getDelayDictionary: '/intelligent/getDelayDictionary',   //智能场景字典
    getExecuteByDeviceId: '/intelligent/getExecuteByDeviceId',   //智能设备列表执行
    smartDelete: '/intelligent/delete', //智能 删除
    smartLog: '/intelligent/log', // 智能 日志
    copyIntelligent: '/intelligent/copyIntelligent', //复制智能
    addDeviceList: '/scene/chooseDevice',   //批量添加设备
    rmdSmartList: '/intelligent/recommand/list', //推荐智能列表
    updateTimerStatus: '/intelligent/updateTimerStatus', //停启用即将执行的定时
    environmentRange: '/intelligent/environmentRange',  // 关于天气选项
    getMessageNotification: '/intelligent/getMessageNotification',  //获取智能消息推送信息

    /* 关于 控制器 */
    controllerDetail: '/controller/details', //控制器详情
    saveController: '/controller/saveController', //控制器保存设置
    controllerList: '/controller/setmanage', // 控制器列表
    controllerSetting: '/controller/manage', //控制器设置
    deleteContoller: '/controller/delController', //删除控制器
    controllerVersion: '/controller/versions', //控制器版本
    controllerLocation: '/controller/location', //控制器 位置信息
    setmanageSecond: '/controller/setmanageSecond_v2', // 控制器列表 二级页面
    saveGateWay: '/device/saveGateWay', // 保存网关
    delGateway: '/device/delGatewayV2', //解绑网关 新接口
    listSetmanage: '/controller/ListSetmanage', // 设备管理 获取 所有 网关 + 设备
    getwayDevice: '/controller/deviceGateway', //获取网关具体信息
    getwayFunction: '/controller/functionV2', // 网关 下设备的停启用
    getwayLocation: '/controller/getwayLocation', // 获取网关 位置信息
    downWIFI: '/device/downWIFI_v2', //配网
    getWifi: '/device/getWifi_v2', //获取 网络账号密码
    createMultiFunction: '/controller/createMultiFunction', //创建多功能网关
    multiFunctionTypes: '/controller/multiFunctionTypes', //多功能继电器 类型
    multiFunctionDevice: '/controller/multiFunctionDevice', //多功能继电器 列表
    multiFunctionList: '/controller/getMultiFunctionDevice', // 多功能 添加设备 列表
    addMultiFunction: '/controller/addDeivceToMultiGateway', // 添加类型
    removeMultiDevice: '/controller/removeMultiDevice',  //移除设备
    getControllerPicture: '/content/getControllerPicture', //控制器 图片
    gatewayPicture: '/controller/gatewayPicture', //修改网关 图片
    controllerUpdateName: '/controller/updateName', //控制器改名
    discoverDevice: '/device/discoverDevice', // 发现设备
    discoverDeviceBind: '/device/discoverDeviceBind',  // 发现设备绑定
    confirmUnBindDevice: '/device/confirmUnBindDevice',  // 确定解绑设备
    getWifiPanelRGBW: '/device/getWifiPanelRGBW',  //按键网关设置
    setWifiPanelRGBW: '/device/setWifiPanelRGBW',  //保存按键网关
    getPanelBindingList: '/device/getPanelBindingList',  //获取可绑定的设备场景列表
    bindingPanel: '/device/bindingPanel',  //按键绑定
    bindingZigbeeScenePanel: '/device/bindingZigbeeScenePanel',  //zigbee按键绑定
    updatePanel: '/controller/updatePanel',  //wifi面板更改设定
    instructionProtocolType: '/device/instructionProtocolType', //协议器种类
    testSendCmd: '/device/testSendCmd',  //测试发送
    addInstructionProtocol: '/device/addInstructionProtocol_v2',  //添加设备
    initStyleInstructionProtocol: '/device/initStyleInstructionProtocol_v2',  //协议器获取初始按钮
    deleteInstructionProtocol: '/device/deleteInstructionProtocol',  //删除通信协议器 设备
    addInstructionProtocolButton: '/device/addInstructionProtocolButton_v2',  //保存按钮和样式设置
    instructionProtocolButton: '/device/instructionProtocolButton_v2',  //加载按钮
    saveInstructionProtocolButton: '/device/saveInstructionProtocolButton',  //按钮小保存
    addProtocolButton: '/device/addProtocolButton',  //添加协议器按钮
    deleteProtocolButton: '/device/deleteProtocolButton',  //删除协议器按钮
    getTimerTimeAxis: '/intelligent/getTimerTimeAxis',  //定时管理接口
    setTimerEnable: '/intelligent/stop',  //定时管理接口
    upgradePanelInfo: '/controller/upgradePanelInfo',  //固件升级信息
    upgradePanel: '/controller/upgradePanel',  //固件升级接口
    upgradePanelLog: '/controller/upgradePanelLog',  //固件升级日志
    updateSerialNumber: '/controller/updateSerialNumber',  //设备更换
    getWifiPanelGateway: '/controller/getWifiPanelGateway',  //获取wifi面板信息

    commonProtocolType: '/device/commonProtocolType',  //通用协议器（包含红外）种类
    addCommonProtocol: '/device/addCommonProtocol',  //添加通用协议器（包含红外)
    initStyleCommonProtocol: '/device/initStyleCommonProtocol',  //通用协议器（包含红外)初始化
    bindCommonProtocolButton: '/device/saveCommonProtocolButton', //红外学习 开始绑定
    addCommonProtocolButton: '/device/addCommonProtocolButton',  //添加红外学习按钮
    deleteCommonProtocolButton: '/device/deleteCommonProtocolButton',  //删除红外学习按钮
    updateCommonProtocolButton: '/device/updateCommonProtocolButton',  //修改按钮配置
    commonProtocolButton: '/device/commonProtocolButton',  //加载红外学习按钮
    zigBeeDuration: '/controller/zigBeeDuration',  //zigbee允许入网
    

    //视频 监控
    cameraConfig: '/account/cameraConfig',
    getCameraConfig: '/account/getCameraConfig',
    cameraList: '/account/cameraListTrust',
    cameraSetting: '/account/cameraSetting',

    // 网页嵌套地址
    colorPanel:  'https://smarthouse.hansa-tec.com/admin/api/color', // 调色板
}

export default function getUrls(speechHost, host) {
    // 语音控制
    Apis.uploadVoice = speechHost + '/hs-speech/speechControl';
    Apis.jsonSpeechControl = speechHost + '/hs-speech/jsonSpeechControl';

    // 网页嵌套地址
    Apis.questions = host + '/admin/api/question'; // 常见问题
    Apis.aboutUs = host + '/admin/aboutUs'; // 关于我们
    Apis.hsvideo = host + '/admin/api/hsvideo'; // 视频监控
    
    return Apis
}
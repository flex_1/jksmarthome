
import DoubulePicker from 'react-native-picker';

export default class CustomPicker{

    static init({pickerData, pickerTitleText, selectedValue, onPickerConfirm, onPickerCancel, isDark=false, pickerFontSize=20, wheelFlex=[1,1,1]}){
        
        let btnColor = isDark ? [38, 80, 215, 1] : [38, 80, 215, 1]
        let bgColor = isDark ? [30, 30, 30, 1] : [255, 255, 255, 1]
        let itemColor = isDark ? [255, 255, 255, 1] : [20, 20, 20, 1]

        DoubulePicker.init({
            pickerData: pickerData,
            pickerConfirmBtnText: '确定',
            pickerCancelBtnText: '取消',
            pickerTitleText: pickerTitleText,
            pickerCancelBtnColor: btnColor,
            pickerConfirmBtnColor: btnColor,
            pickerToolBarBg: bgColor,
            pickerBg: bgColor,
            pickerFontColor: itemColor,
            pickerTitleColor: itemColor,
            pickerFontSize: 20,
            pickerRowHeight:40,
            selectedValue: selectedValue,
            pickerTextEllipsisLen: 15,
            wheelFlex: wheelFlex,
            onPickerConfirm: (data,index) => {
                onPickerConfirm && onPickerConfirm(data,index)
            },
            onPickerCancel: data => {
                onPickerCancel && onPickerCancel()
            },
            onPickerSelect: data => {
            }
        });
        DoubulePicker.show()
    }

    static hide(){
        DoubulePicker.hide()
    }
}

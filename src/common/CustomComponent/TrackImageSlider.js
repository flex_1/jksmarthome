
import React, {Component} from 'react'
import {
    View,  
    Image,
    PanResponder
} from 'react-native'

export default class TrackImageSlider extends Component{

    static defaultProps = {
        style: {},
        sliderWidth: 300,
        trackImage : require('../../images/slider.png'),
        minimumValue : 0,
        maximumValue : 100,
        thumbTintColor : '#fff',
        thumbWidth : 30,
        value : 0,
        onValueChange : ()=>{},
        onSlidingComplete : ()=>{}
    }

    constructor(props) {
        super(props);
        this.state = {
            value: props.value,
            lastValue: props.value,
        }
	}

	componentWillMount() {
		this._panResponder = PanResponder.create({
      		onMoveShouldSetPanResponder: (evt, gestureState) => true,
      		onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
			onPanResponderGrant: () => {
                // 移动 开始
                this.state.lastValue = this.state.value
			},
			onPanResponderMove: (evt, gs) => {

                const {minimumValue, maximumValue, thumbWidth, sliderWidth} = this.props
                
                let unit = (sliderWidth - thumbWidth)/(maximumValue - minimumValue)
                let dx = gs.dx/unit
                let moveX = parseInt(dx) 
                let value = this.state.lastValue + moveX

				if(value >= maximumValue) value = maximumValue
                if(value <= minimumValue) value = minimumValue

                this.setState({value: value})
                this.props.onValueChange(value)
			},
			onPanResponderRelease: (evt, gs) => {
				// 移动结束
                this.state.lastValue = this.state.value
                this.props.onSlidingComplete(this.state.value)
			}
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.props.value) {
            this.setState({ value: nextProps.value });
        }
    }
    
	render(){
        const {minimumValue, maximumValue, thumbWidth, sliderWidth, thumbTintColor, style} = this.props

        let position = (this.state.value -  minimumValue)* 1.0 / (maximumValue - minimumValue) * (sliderWidth - thumbWidth)
    
		return(
            <View style={[{ height: this.props.thumbWidth,width: sliderWidth}, style]}>
                <Image
                    source={this.props.trackImage}
                    style={{width:'100%',height:'100%',resizeMode:'contain',position:'absolute',left:0}} 
                />
                <View 
                    ref={e => this.sliderTop = e} 
                    {...this._panResponder.panHandlers} 
                    style={{
                        backgroundColor: thumbTintColor,
                        width: thumbWidth,
                        height: thumbWidth,
                        borderRadius: thumbWidth/2,
                        marginLeft: position
                    }}
                />
            </View>
		)
	}
}

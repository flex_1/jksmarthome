import React, {Component} from 'react'
import {ViewPropTypes, Text, StatusBar, StyleSheet, View, Platform, TouchableOpacity} from 'react-native'
import PropTypes from 'prop-types';

const NAV_BAR_HEIGHT_IOS = 44;//导航栏在iOS中的高度
const NAV_BAR_HEIGHT_ANDROID = 50;//导航栏在Android中的高度
const STATUS_BAR_HEIGHT = 20;//状态栏的高度
const StatusBarShape = {//设置状态栏所接受的属性
    barStyle: PropTypes.oneOf(['light-content', 'default',]),
    hidden: PropTypes.bool,
    backgroundColor: PropTypes.string,
};
export default class NavigationBarView extends Component {
    //提供属性的类型检查
    static propTypes = {
        style: ViewPropTypes.style,
        title: PropTypes.string,
        backTitle: PropTypes.string,
        titleView: PropTypes.element,
        titleLayoutStyle: ViewPropTypes.style,
        hide: PropTypes.bool,
        statusBar: PropTypes.shape(StatusBarShape),
        rightButtonOne: PropTypes.element,
        rightButtonTwo: PropTypes.element,
        leftButton: PropTypes.element,
    };
    //设置默认属性
    static defaultProps = {
        statusBar: {
            barStyle: 'light-content',
            hidden: false
        }
    };

    render() {
        let statusBar = !this.props.statusBar.hidden ?
            <View style={styles.statusBar}>
                <StatusBar {...this.props.statusBar} />
            </View> : null;

        let titleView = this.props.titleView ? this.props.titleView :
            <Text ellipsizeMode="head" numberOfLines={1} style={styles.title}>{this.props.title}</Text>;

        let backTitle = this.props.backTitle ?
            <View style={styles.backTitle}>
                <Text style={{color: 'white', fontSize: 18}}
                      onPress={this.props.onBack}>
                    {this.props.backTitle}
                </Text>
            </View> : null;

        let rightButtonLayout = this.props.rightButtonOne || this.props.rightButtonTwo ?
            <View style={styles.rightButtonLayout}>
                {this.getButtonElement(this.props.rightButtonOne)}
                {this.getButtonElement(this.props.rightButtonTwo)}
            </View> : null;

        let content = this.props.hide ? null :
            <View style={styles.navBar}>
                {this.getButtonElement(this.props.leftButton)}
                {backTitle}
                <View style={[styles.navBarTitleContainer, this.props.titleLayoutStyle]}>
                    {titleView}
                </View>
                {rightButtonLayout}
            </View>;
        return (
            <View style={[styles.container, this.props.style]}>
                {statusBar}
                {content}
            </View>
        )
    }

    getButtonElement(data) {
        return data ? (
            <View style={styles.navBarButton}>
                {data ? data : null}
            </View>
        ) : null;
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#2196f3'
    },
    navBarButton: {
        alignItems: 'center'
    },
    navBar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: Platform.OS === 'ios' ? NAV_BAR_HEIGHT_IOS : NAV_BAR_HEIGHT_ANDROID,
        backgroundColor: '#fff'
    },
    navBarTitleContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        left: 10,
        right: 10,
        top: 0,
        bottom: 0
    },
    rightButtonLayout: {
        alignItems: 'center',
        position: 'absolute',
        right: 10,
        top: 0,
        bottom: 0,
        flexDirection: 'row'
    },
    backTitle: {
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        left: 20,
        top: 0,
        bottom: 0
    },
    title: {
        fontSize: 20,
        color: 'white',
    },
    statusBar: {
        height: Platform.OS === 'ios' ? STATUS_BAR_HEIGHT : 0,
    }
});

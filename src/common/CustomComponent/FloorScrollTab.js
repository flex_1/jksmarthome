import React from 'react';
import { TouchableOpacity, StyleSheet, View, Text,FlatList } from 'react-native';
import {ColorsLight, ColorsDark} from '../../common/Themes';
import {Colors} from '../Constants';

const FloorScrollTab = (props) => {
    const {
        floors,
        currentFloor,
        currentIndex,
        isDark,
        onClick
    } = props

    const roomScrollHead = React.useRef()
    

    React.useEffect(()=>{
        roomScrollHead.current?.scrollToIndex({index:currentIndex,viewPosition:0.5})
    },[currentIndex])


    const renderRowItem = React.useCallback((rowData, index) => {
    
        const Colors = isDark ? ColorsDark : ColorsLight

        let underLine = null
        let activeText = {color:Colors.themeTextLight}

		if(currentFloor == rowData.floor){
			underLine = <View style={styles.underLine} />
			activeText = {color:Colors.themeText,fontSize:18}
		}

		return (
			<TouchableOpacity activeOpacity={0.7} style={styles.floorTouch} onPress={()=>{
                onClick(rowData.floor, index)
			}}>
				<Text style={[styles.floorText,activeText]}>{rowData.floorText}</Text>
				{underLine}
			</TouchableOpacity>
		)
    },[currentFloor, isDark])


    const scrollFlatlist = React.useMemo(()=>{
        return(
            <FlatList
                ref={e => roomScrollHead.current = e}
                style={{marginRight: 10}}
			    data={floors}
			    horizontal={true}
			    showsHorizontalScrollIndicator={false}
			    contentContainerStyle={styles.headScrollContainer}
			    removeClippedSubviews={false}
			    keyExtractor={(item, index)=> 'room_header'+index}
			    renderItem={({ item, index }) => renderRowItem(item, index)}
			    initialNumToRender={5}
		    />
        )
    },[floors, currentFloor, isDark])


    return scrollFlatlist
}

const styles = StyleSheet.create({
    headScrollContainer: {
		alignItems: 'center',
		paddingLeft: 10,
		paddingRight:20
	},
    floorText: {
		fontSize: 15,
		fontWeight: 'bold'
	},
    floorTouch: {
		alignItems: 'center',
		paddingHorizontal: 10,
		marginRight: 5
	},
    underLine: {
		width: 12,
		height: 3,
		backgroundColor: Colors.tabActiveColor,
		borderRadius: 0,
		marginTop: 5
	},
})

export default FloorScrollTab;

import React, {Component} from 'react'
import {
    Text,
    StyleSheet, 
    View,  
    TouchableOpacity,
    Animated,
    Dimensions,
    Image
} from 'react-native'

const screenW = Dimensions.get('screen').width;

export default class AnimateButton extends Component{

    static defaultProps = {
        ripple: 3,       //同时存在的圆数量
        initialDiameter: 120,
        endDiameter: 200,
        initialPosition: { top: 100, left: screenW * 0.5 },
        rippleColor: '#46BDA5',
        intervals: 500,      //间隔时间
        spreadSpeed: 2000,      //扩散速度
    }

    constructor(props) {
        super(props);
        
        this.cancelAnimated = false
        this.animatedFun = null
        let rippleArr = [];
        for (let i = 0; i < props.ripple; i++) rippleArr.push(0);

		this.state = {
            anim: rippleArr.map(() => new Animated.Value(0))
        }
    }

    startAnimation() {
        this.state.anim.map((val, index) => val.setValue(0));
        this.animatedFun = Animated.stagger(this.props.intervals, this.state.anim.map((val) => {
            return Animated.timing(val, { toValue: 1, duration: this.props.spreadSpeed })
        }));
        this.cancelAnimated = false;
        this.animatedFun.start(() => { if (!this.cancelAnimated) { this.startAnimation() } })
    }

    stopAnimation() {
        this.cancelAnimated = true;
        this.animatedFun?.stop();
        this.state.anim.map((val, index) => val.setValue(0));
    }

    renderAniButton(){
        const { initialPosition, initialDiameter, endDiameter, rippleColor, style } = this.props;
        let r = endDiameter - initialDiameter;    // 直径变化量,top与left的变化是直径的一半
        let rippleComponent = this.state.anim.map((val, index) => {

            let animateStyle = [{
                borderRadius: 999,
                position: 'absolute',
                backgroundColor: rippleColor
            },{
                opacity: val.interpolate({
                    inputRange: [0, 1],
                    outputRange: [1, 0]
                }),
                height: val.interpolate({
                    inputRange: [0, 1],
                    outputRange: [initialDiameter, endDiameter]
                }),
                width: val.interpolate({
                    inputRange: [0, 1],
                    outputRange: [initialDiameter, endDiameter]
                }),
                top: val.interpolate({
                    inputRange: [0, 1],
                    outputRange: [initialPosition.top - initialDiameter / 2, initialPosition.top - initialDiameter / 2 - r / 2]
                }),
                left: val.interpolate({
                    inputRange: [0, 1],
                    outputRange: [initialPosition.left - initialDiameter / 2, initialPosition.left - initialDiameter / 2 - r / 2]
                })
            }]

            return (
                <Animated.View key={"animatedView_" + index} style={animateStyle}>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={{width:'100%',height:'100%',justifyContent:'center',alignItems:'center'}}
                        onPress={() => {
                            this.props.onClick && this.props.onClick()
                        }}
                    >
                        <Text style={{ fontSize: 18, color: 'white', fontWeight: 'bold' }}>{this.props.centerLabel}</Text>
                    </TouchableOpacity>
                </Animated.View>
            )
        })
        return (
            <View style={style}>
                {rippleComponent}
            </View>
        )
    }

    render() {
        return this.renderAniButton()
    }
}

import React, {Component} from 'react'
import {ViewPropTypes, Text, StatusBar, StyleSheet, View, Platform, TouchableOpacity} from 'react-native'
import PropTypes from 'prop-types';

export default class BadgeTabManager {
    /**
     * 静态badgeTab
     */
    static badgeTab;
    static showBadge;

    static show() {
        this.badgeTab && this.badgeTab.show();
    }

    static clear() {
        this.badgeTab && this.badgeTab.clear();
    }
}

export class BadgeTab extends Component {

    static propTypes = {
        tabImage: PropTypes.element,
    }

    /**
     * 组件被移除的时候
     */
    componentWillUnmount() {
        BadgeTabManager.badgeTab = null;
    }

    render() {
        return (<BadgeTabUtil ref={e => BadgeTabManager.badgeTab = e} tabImage={this.props.tabImage}/>)
    }
}

class BadgeTabUtil extends Component {

    constructor(props) {
        super(props);
    }

    show(){
        BadgeTabManager.showBadge = true
        this.redDot && this.redDot.setNativeProps({
            style : {
                ...styles.badge,
                backgroundColor: 'red'
            }
        })
    }

    clear(){
        BadgeTabManager.showBadge = false
        this.redDot && this.redDot.setNativeProps({
            style : {
                ...styles.badge,
                backgroundColor: 'transparent'
            }
        })
    }

    render() {
        return(
            <View>
                {this.props.tabImage}
                <View ref={e => this.redDot = e} style={[styles.badge,{backgroundColor: BadgeTabManager.showBadge ? 'red':'transparent'}]}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    badge:{
        width:8,
        height:8,
        borderRadius:4,
        position:'absolute',
        top:0,
        right: 0
    }
})


import React from "react";
import {ColorsLight,ColorsDark} from '../Themes';
import { View } from 'react-native';

export default class ColorManager {
    /**
     * 静态toast
     */
    static colors;

    static setDark() {
        this.colors && this.colors.setDark()
    }

    static setLight() {
        this.colors && this.colors.setLight()
    }
}

export class ColorsComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isDark: false,
            color : ColorsLight
        }
        ColorManager.colors = this
    }

    componentDidMount(){

    }

    setDark(){
        this.setState({
            color: ColorsDark
        })
    }

    setLight() {
        this.setState({
            color: ColorsLight
        })
    }
}
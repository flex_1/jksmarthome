import React from "react";
import {Dimensions} from 'react-native';
import Draggable from '../../third/Draggable/Draggable';
import { BottomSafeMargin, StatusBarHeight } from '../../util/ScreenUtil';

const {width, height} = Dimensions.get('window');
const dragSize = width * 0.16

let originX = width - dragSize
let originY = height * 0.5

export default class DraggableManager {

    static drag;

    static changeText(text){
        this.drag && this.drag.changeText(text)
    }

    static show(){
        this.drag && this.drag.show()
    }

    static hide(){
        this.drag && this.drag.hide()
    }
}

export class DraggableComponent extends React.Component {
    /**
     * 组件被移除的时候
     */
    componentWillUnmount() {
        DraggableManager.drag = null;
    }

    render() {
        return (
            <DraggableUtil 
                ref = {e => DraggableManager.drag = e}
                onPress = {()=>{
                    this.props.onPress && this.props.onPress()
                }}
            />
        )
    }
}

export class DraggableUtil extends React.Component {

    constructor(props) {
        super(props);

        _x = originX
        _y = originY
        
        this.state = {
            shouldReverse: false,
            text: '拽我',
            isShow: true
        }
    }

    changeText(text){
        this.setState({
            text: text
        })
    }

    hide(){
        originX = _x
        originY = _y
        this.setState({isShow: false})
    }

    show(){
        this.setState({isShow: true})
    }
    
    render() {
        if(!this.state.isShow){
            return null
        }

        return (
            <Draggable
                imageSource={require('../../images/bottom_tab/center.png')}
                renderText={this.state.text}
                renderSize={dragSize} 
                x={originX} 
                y={originY}
                touchableOpacityProps={{activeOpacity: 1}}
                animatedViewProps={{activeOpacity: 0.1}}
                onShortPressRelease={()=>{
                    this.props.onPress()
                }}
                onDragRelease = {(e, gestureState, bounds)=>{
                    _x += gestureState.dx
                    _y += gestureState.dy
                    this.setState({shouldReverse: true})
                }}
                shouldReverse={this.state.shouldReverse}
                onReverse={()=>{
                    this.setState({shouldReverse: false})
                    if(_x >= (width - dragSize )/2){
                        _x = width - dragSize
                    }else{
                        _x = 0
                    }
                    if(_y < StatusBarHeight){
                        _y = StatusBarHeight
                    }else if(_y > height - dragSize - BottomSafeMargin){
                        _y = height - dragSize - BottomSafeMargin
                    }
                    return {x:_x-originX, y:_y-originY}
                }}
            />
        )
    }
}
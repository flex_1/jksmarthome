
import React from 'react'
import { 
    TouchableOpacity,
    StyleSheet
} from 'react-native'
// import Switch from 'react-native-switch-pro';
import PropTypes from 'prop-types';
import SwitchToggle from './SwitchToggle';

// onPress : 点击事件
// value : 开关状态
// style: 样式
// isAsync: 点击之后 等待结果 再触发  *使用SwitchToggle此参数无效
const SwitchButton = (props) => {

    const statusValue = props.value > 0

    const onSyncPress = props.isAsync ? undefined : ()=>{
        props.onPress && props.onPress()
    }

    const onAsyncPress = !props.isAsync ? undefined : ()=>{
        props.onPress && props.onPress()
    }

    return(
        <TouchableOpacity
            activeOpacity={0.7}
            style={props.style || styles.switchTouch} 
            onPress={()=>{
                props.onPress && props.onPress()
            }}
        >
            <SwitchToggle
                switchOn={statusValue}
                onPress={() => {props.onPress && props.onPress()}}
                backgroundColorOn={props.bgColorOn}
                backgroundColorOff={props.bgColorOff}
                circleColorOff={'white'}
                duration={200}
                containerStyle={{
                    width: 50,
                    height: 30,
                    borderRadius: 15,
                    padding: 2,
                }}
                circleStyle={{
                    width: 26,
                    height: 26,
                    borderRadius: 13,
                }}
            />
        </TouchableOpacity>
    )

    // return (
    //     <TouchableOpacity
    //         activeOpacity={0.7}
    //         style={props.style || styles.switchTouch} 
    //         onPress={()=>{
    //             props.onPress && props.onPress()
    //         }}
    //     >
    //         <CustumSwitch
    //             width={50}
    //             height={30}
    //             backgroundActive={'#2650D7'}
    //             value={statusValue}
    //             isAsync={props.isAsync}
    //             onSyncPress={onSyncPress}
    //             onAsyncPress={onAsyncPress}
    //         /> 
    //     </TouchableOpacity>
    // )
}

// 改写 第三方组件 手势操作
// class CustumSwitch extends Switch{
//     _onPanResponderGrant = (evt, gestureState) => {
//         const { disabled,isAsync } = this.props
//         if (disabled) return
    
//         this.setState({toggleable: true})
//         if(!isAsync){
//             this.animateHandler(this.handlerSize * 6 / 5)
//         } 
//     }
// }

const styles = StyleSheet.create({
    switchTouch:{
        height:'100%',
        justifyContent:'center',
        alignItems: 'center',
        width:80
    },
})

SwitchButton.propTypes = {
    onPress: PropTypes.func,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
    style: PropTypes.object,
    isAsync: PropTypes.bool,
    bgColorOn: PropTypes.string,
    bgColorOff: PropTypes.string
}

SwitchButton.defaultProps = {
    isAsync: false,
    bgColorOn: '#2650D7',
    bgColorOff: '#dddddd'
}

export default SwitchButton

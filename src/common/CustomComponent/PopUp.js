
import React, {Component} from 'react'
import {
    Text,
    StyleSheet, 
    View,  
    TouchableOpacity,
    Animated,
    Easing,
    Image
} from 'react-native'

export default class PopUp extends Component{

    constructor(props) {
        super(props);
        this.right = props.frame.right

		this.state = {
            leftMargin: new Animated.Value(this.right),
        }
        
        this.leftMarginAnimated = Animated.timing(
            this.state.leftMargin,
            {
                toValue: -200,  //透明度动画最终值
                duration: 300,   //动画时长
                easing: Easing.quad,
            }
        )
    }

    // 开始动画
    _startAnimated() {
        this.leftMarginAnimated.start(() => {
            this.state.leftMargin.setValue(this.right)
            this.props.dismiss && this.props.dismiss()
        })
    }

    renderBtns(){
        if(!this.props.buttonArrays){
            return null
        }
        const {width} = this.props.frame
        const {bgColor, textColor, borderColor} = this.props

        let btns = this.props.buttonArrays.map((value, index)=>{
            let borderTop = index == 0 ? {} : {borderTopColor: borderColor, borderTopWidth: 1}
            return(
                <TouchableOpacity
                    key={'btn_'+index}
                    activeOpacity={0.8} 
                    style={[styles.expandItem,borderTop]} 
                    onPress={()=>{
                        value.onPress && value.onPress()
                    }}
                >
                    <Image style={[value.iconStyle?value.iconStyle:styles.expandImg,{tintColor: textColor}]} source={value.icon}/>
                    <Text style={[styles.expandText,{color: textColor}]}>{value.title}</Text>
                </TouchableOpacity>
                
                
            )
        })
        return(
            <View style={[styles.expandWrapper,{backgroundColor: bgColor,width: width}]}>
                {btns}
            </View>
        )
    }

    render() {
        const {top} = this.props.frame
        const {triangleRight,bgColor} = this.props

        return(
            <TouchableOpacity activeOpacity={1} style={styles.modalWrapper}  onPress={()=>{
                this._startAnimated()
            }}>
                <Animated.View style={{position:'absolute',top:top, right: this.state.leftMargin}}>
                    <View style={[styles.arrow, {borderBottomColor: bgColor,marginRight: triangleRight}]}/>
                    {this.renderBtns()}
                </Animated.View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    modalWrapper:{
        position: 'absolute',
        width:'100%',
        height:'100%',
        top: 0,
        left: 0
    },
    expandWrapper:{
        borderRadius: 4
    },
    expandItem:{
        height: 60,
        alignItems: 'center',
        flexDirection: 'row'
    },
    expandText:{
        marginLeft: 10,
        fontSize: 16
    },
    expandImg:{
        marginLeft: 16,
        width: 20,
        height: 20,
        resizeMode: 'contain'  
    },
    arrow:{
        alignSelf: 'flex-end',
        width:0,
        height:0,
        borderStyle:'solid',
        borderWidth: 5,
        borderTopColor: 'transparent', //下箭头颜色
        borderLeftColor: 'transparent', //右箭头颜色
        borderRightColor: 'transparent' //左箭头颜色
    }
})

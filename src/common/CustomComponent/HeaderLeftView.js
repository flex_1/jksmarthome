import React, { Component } from 'react';
import { TouchableOpacity, Image, Platform, Text } from 'react-native';

//返回按钮 宽高属性
let backBtnWidtHeight = Platform.select({
    ios: {  height: 44 },
    android: { height: 44 }
})

const HeaderLeftView = (props) => {

    let backImg = require('../../images/back.png')
    let text = null
    let paddingRightVal = 20
    if (props.backImg) {
        backImg = props.backImg
    }
    if (props.text){
        text = <Text style={{marginLeft:15,fontSize:24,color:'#1E1E1E',fontWeight:'bold'}}>{props.text+' '}</Text>
        paddingRightVal = 5
    }

    return (
        <TouchableOpacity
            activeOpacity = {0.8}
            onPress={() => { props.navigation.pop() }}
            style={{ paddingLeft: 15,paddingRight:paddingRightVal, flexDirection:'row',alignItems: 'center', height:'100%' }}
        >
            <Image style={{ width: 10, height: 18 }} source={backImg} />
            {text}
        </TouchableOpacity>
    )
}

export default HeaderLeftView;
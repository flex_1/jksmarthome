/**
 * 列表加载用到的控件
 */
import React, { Component } from 'react';
import { TouchableOpacity, Image, Platform, Text,View, Dimensions,ActivityIndicator } from 'react-native';

const screenW = Dimensions.get('window').width;
const screenH = Dimensions.get('window').height;

// 列表加载中
export const ListLoading = (props) => {

    let propStyle = props.style
    let text = props.text || '正在加载...'

    return(
        <View style={propStyle ? propStyle:{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
		    <Text style={{ color: '#999999', fontSize: 15 }}>{text}</Text>
	    </View>
    )    
}

// 列表加载错误
export const ListError = (props) => {

    let propStyle = props.style
    let text = props.text || '网络错误,刷新重试'

    return(
        <View style={propStyle ? propStyle:{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
			<TouchableOpacity
                activeOpacity={0.7}
				onPress={() => {
					props.onPress && props.onPress()
				}}
			>
				<Text style={{ color: '#2650D7', fontSize: 15 }}>{text}</Text>
			</TouchableOpacity>
		</View>
    )
}

// 列表无内容
export const ListNoContent = (props) => {

    let imgView = props.img ? <Image style={{width:screenW/2,height:screenW/2,resizeMode:'contain'}} source={props.img} /> : null
    let text = props.text
    let propStyle = props.style

    return (
        <View style={propStyle ? propStyle:{width:'100%',flex: 1, alignItems: 'center',marginTop:'30%' }}>
            {imgView}
            <Text style={{color: '#999999',textAlign: 'center',fontSize: 15,lineHeight: 20}}>{text}</Text>
        </View>
    )
}

// 列表尾部-加载中
export const FooterLoading = () => (
    <View style={{flexDirection: 'row',alignSelf: 'center',alignItems: 'center',paddingTop: 25,paddingBottom: 15}}>
        <ActivityIndicator size={'small'}/>
        <Text style={{color: '#999',fontSize: 12,marginLeft: 4}}>加载中...</Text>
    </View>
)

// 列表尾部-没有更多
export const FooterEnd = () => (
    <View style={{flexDirection: 'row',alignSelf: 'center',alignItems: 'center',paddingTop: 25,paddingBottom: 15}}>
        <Text style={{color: '#999',fontSize: 12,marginLeft: 4}}>没有更多了</Text>
    </View>
)
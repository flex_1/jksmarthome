import React from "react";
import Spinner from 'react-native-loading-spinner-overlay';
import {ActivityIndicator} from '@ant-design/react-native'

export default class SpinnerManager {
    /**
     * 静态spinner
     */
    static spinner;

    static show(text) {
        this.spinner && this.spinner.show(text);
    }

    static close() {
        this.spinner && this.spinner.close();
    }
}

export class SpinnerComponent extends React.Component {
    /**
     * 组件被移除的时候
     */
    componentWillUnmount() {
        SpinnerManager.spinner = null;
    }

    render() {
        return (<SpinnerUtil ref = {e => SpinnerManager.spinner = e}/>);
    }
}

class SpinnerUtil extends React.Component{
    constructor(props){
        super(props)
        this.state={
            visible: false,
            text: null
        }
    }

    show(text){
        this.setState({
            visible:true,
            text: text
        })
    }

    close(){
        this.setState({
            visible:false,
            text: null
        })
    }

    render() {
        return (
            <ActivityIndicator
                animating = {this.state.visible}
                size="small"
                toast
                color={'#fff'}
                text={this.state.text}
            />
        );
    }
}

import React from 'react'
import { 
    TouchableOpacity,
    StyleSheet,
    Text,
    Image
} from 'react-native'
import { Colors } from '../../common/Constants';
import PropTypes from 'prop-types';


const NoContentButton = (props) => {
    return(
        <TouchableOpacity activeOpacity={0.7} style={[styles.moreTouch, props.style]} onPress={()=>{
            props.onPress && props.onPress()
        }}>
            <Text style={{fontSize:14,color:props.textColor}}>{props.text}</Text>
            <Image style={styles.arrow} source={require('../../images/enterLight.png')}/>
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    moreTouch:{
		borderColor:Colors.borderLightGray,
		borderWidth:1,
		width:120,
		height:32,
		borderRadius:16,
		justifyContent:'center',
		alignItems:'center',
		flexDirection:'row'
	},
    arrow:{
		width:6,
		height:11,
		marginLeft:10,
		resizeMode:'contain'
    }
})

NoContentButton.propTypes = {
    onPress: PropTypes.func,
    text: PropTypes.string,
    textColor: PropTypes.string,
    style: PropTypes.object
}

NoContentButton.defaultProps = {
    text: '去添加',
    textColor: Colors.themeTextLightGray,
    style: {}
}

export default NoContentButton

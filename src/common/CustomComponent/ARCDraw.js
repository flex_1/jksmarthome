
import React from 'react';
import {useLayoutEffect, useEffect, useState} from 'react';
import { 
    TouchableOpacity,
    StyleSheet,
    Text,
    Image,
    View
} from 'react-native';

import PropTypes from 'prop-types';
import Svg, {
    Path,
} from 'react-native-svg';


const ARCDraw = (props) => {
    const [angel, setAngel] = useState(-14);

    const renderSVG = () => {
        //半径
        const R = 100
        //线宽
        const strokeWidth = 16
        //步进角度
        const stepAg = 14
        
        const endX1 = R - R*Math.cos(angel*Math.PI/180) + strokeWidth
        const endY1 = R - R*Math.sin(angel*Math.PI/180) + strokeWidth

        const beginX2 = R - R*Math.cos(Math.PI/6) + strokeWidth
        const beginY2 = R + R*Math.sin(Math.PI/6) + strokeWidth
        const endX2 = R + R*Math.cos(Math.PI/6) + strokeWidth
        const endY2 = R + R*Math.sin(Math.PI/6) + strokeWidth

        let dir = 0
        if(angel > (180 - 30)){
            dir = 1
        }

        return (
            <View style={styles.container}>
                <View style={[{alignItems: 'center', justifyContent: 'center',width: '100%'},]}>
                    <View style={{width: 2*(R+strokeWidth), height: 2*(R+strokeWidth)}}>
                        <Svg height="100%" width="100%" style={{backgroundColor: 'pink'}}>
                            <Path 
                                d={`M${beginX2} ${beginY2} A${R} ${R}, 1, ${dir}, 1, ${endX1}, ${endY1}`}
                                fill="transparent" 
                                strokeWidth={strokeWidth} 
                                strokeLinecap="round" 
                                stroke="green"
                            />
                            <Path
                                d={`M${beginX2} ${beginY2} A${R} ${R}, 1, 1, 1, ${endX2}, ${endY2}`}
                                fill="transparent" 
                                strokeWidth={strokeWidth}
                                strokeLinecap="round"
                                stroke="rgba(0,0,0,0.1)"
                            />
                        </Svg>
                        <View style={{position: 'absolute',backgroundColor: '#fff',top: (strokeWidth/2)*3, left: (strokeWidth/2)*3,width: (2*R-strokeWidth), height: (2*R-strokeWidth),borderRadius: (2*R-strokeWidth)/2,justifyContent:'center',alignItems:'center'}}>
                            <Text style={{fontSize: 30}}>28</Text>
                        </View>
                    </View>
                </View>
                <TouchableOpacity style={{marginLeft:20 ,padding: 10}} onPress={()=>{
                    setAngel(angel - stepAg)
                    
                }}>
                    <Text>减减减</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{marginLeft:20 ,padding: 10}} onPress={()=>{
                    setAngel(angel + stepAg)
                }}>
                    <Text>加加加</Text>
                </TouchableOpacity>
                <Text>{angel}</Text>
            </View>
        )
    }

    return (
        renderSVG()
    )
    
}


const styles = StyleSheet.create({
    container: {
		backgroundColor: '#F6F6F6',
        flex: 1
	}
})



export default ARCDraw

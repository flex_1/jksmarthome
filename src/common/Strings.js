/**
 * 描述：
 * 项目中出现的提示文字
 * @Author zuomao
 * @Date 2021.8.4
 */

export const HSTips = {
    limitsCreatScene: '您当前身份为普通成员，无法在该房屋中创建场景。',
    limitsCreatRoom: '您当前身份为普通成员，无法在该房屋中创建房间。',
    limitsCreatSmart: '您当前身份为普通成员，无法在该房屋中创建智能。',
    limitsOrderRoom: '您当前身份为普通成员，无法排序房间。',
    limitsAddDeviceForRoom: '您当前身份为普通成员，无法添加场景或设备。',

    holderDeleteHouse: '该操作将会导致您该房屋下所有数据信息被永久删除，您确定要删除吗？',
    nonHolerRemoveHouse: '该操作将会解绑房屋及房屋下所有的权限（房间和设备），确定要移除吗？',

    memberDisable: '您的房屋权限已失效，系统已为您切换默认房屋，点击确认后刷新。',
    houseBeenChanged: '您当前默认房屋被切换，点击确定按钮后，会返回首页，页面数据会自动更新，以确保您正常使用。',
}

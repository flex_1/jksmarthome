// Themes 提供黑夜模式与明亮模式 下的颜色和图片 参数

//颜色 明亮模式
export const ColorsLight = {
    newTheme: '#25cad2', // 新的主题色，绿色
    themeBg: '#FFFFFF', //主题背景色 白
    themeBaseBg: '#F6F6F6', //浅色主题背景 浅灰
    themeText: '#1E1E1E', //主题文字色 黑
    themeTextLight: '#999999', // 文字颜色 浅灰
    themeButton: '#2650D7', //主题蓝
    themeInactive: '#CCCCCC', // 主题暗色 (Input 的 placeholderTextColor都用这个)

    split: '#EEEEEE', //分割线 灰色
    tabInactive: '#999999',  //底部tab未选中时的颜色 浅灰
    borderLight: '#E5E5E5', //边框 灰色
    activeIndiColor: '#CCCCCC', // 下拉刷新 圈圈颜色
    offlineBg: '#D8D8D8',  //设备离线背景色
    newBtnBlueBg: '#008CFF',  //新按钮背景色 蓝

    yellow: '#FFAE2B', // 黄色
    white: '#FFFFFF',  // 白色
    red: '#FD364B', // 红色
    lightGray: '#EEEEEE', //浅灰色
    lightGreen: '#46BDA5', //浅绿色
    blue: '#2650D7', // 蓝色
    fullyTransparent : 'transparent', //全透明底色
    translucent : 'rgba(0,0,0,0.5)', //半透明底色
    lightTransparent: 'rgba(0,0,0,0.3)', //浅透明
    unselectBg : 'rgba(204,204,204,1)',  //未选中时背景色
}

//颜色 夜间模式
export const ColorsDark = {
    newTheme: '#25cad2', // 新的主题色，绿色
    themeBg: '#1e1e1e', //主题背景色 浅黑
    themeBaseBg: '#111111', //主题背景 深黑
    themeText: '#FFFFFF', //主题文字色 白
    themeTextLight: 'rgba(255,255,255,0.5)', // 文字颜色 浅灰
    themeButton: '#2650D7', //主题蓝
    themeInactive: '#474747', // 主题暗色

    split: 'rgba(229,229,229,0.2)', //分割线 浅灰
    tabInactive: '#474747',  //底部tab未选中时的颜色 浅灰
    borderLight: '#E5E5E5', //边框 灰色
    activeIndiColor: '#CCCCCC', // 下拉刷新 圈圈颜色
    offlineBg: '#555555',  //设备离线背景色
    newBtnBlueBg: '#008CFF',  //新按钮背景色 蓝

    yellow: '#FFAE2B', // 黄色
    white: '#FFFFFF',  // 白色
    red: '#FD364B', // 红色
    lightGray: '#EEEEEE', //浅灰色
    lightGreen: '#46BDA5', //浅绿色
    blue: '#2650D7', // 蓝色
    fullyTransparent : 'transparent', //全透明底色
    translucent : 'rgba(0,0,0,0.5)', //半透明底色
    lightTransparent: 'rgba(0,0,0,0.8)', //浅透明
    unselectBg : '#1e1e1e',  //未选中时背景色
}


//图片 明亮模式
export const ImagesLight = {
    //列表无内容图片
    noRecord: require('../images/no_content/no_record.png'),
    noLayout: require('../images/no_content/no_layout.png'),
    noLog: require('../images/no_content/no_log.png'),
    noScene: require('../images/no_content/no_scene.png'),
    noMember: require('../images/no_content/no_member.png'),
    noRoom: require('../images/no_content/no_room.png'),
    noDevice: require('../images/no_content/no_device.png'),
    noMessage: require('../images/no_content/no_message.png'),
    noSmart: require('../images/no_content/no_smart.png'),
    noController: require('../images/no_content/no_controller.png'),
    
    //缺省图片
    default: require('../images/default.png'),

    //开关
    switchOn: require('../images/switch_on.png'),
    switchOff: require('../images/switch_off.png'),

    //登录log
    appleLog: require('../images/login/apple.png')
}

//图片 黑夜模式
export const ImagesDark = {
    //列表无内容图片
    noRecord: require('../images/no_content/no_record_dark.png'),
    noLayout: require('../images/no_content/no_layout_dark.png'),
    noLog: require('../images/no_content/no_log_dark.png'),
    noScene: require('../images/no_content/no_scene_dark.png'),
    noMember: require('../images/no_content/no_member_dark.png'),
    noRoom: require('../images/no_content/no_room_dark.png'),
    noDevice: require('../images/no_content/no_device_dark.png'),
    noMessage: require('../images/no_content/no_message_dark.png'),
    noSmart: require('../images/no_content/no_smart_dark.png'),
    noController: require('../images/no_content/no_controller_dark.png'),

    //缺省图片
    default: require('../images/default_dark.png'),

    //开关
    switchOn: require('../images/switch_on.png'),
    switchOff: require('../images/switch_off_dark.png'),

    //登录log
    appleLog: require('../images/login/apple_w.png')
}
# 金恪 智能家居

##  官方文档
React-Native:  https://reactnative.cn/docs/getting-started/
React-Navigation:  https://reactnavigation.org/docs/zh-Hans/api-reference.html


## 项目 搭建环境
react-native-cli   :   2.0.1
react-native        :   0.59.9
node.js               :    v11.11.0
npm                    :    6.7.0


## 项目 第三方框架的安装
项目含有多个第三方框架，安卓需要单独处理：

（1）React-Navigation    配置链接:   https://reactnavigation.org/docs/zh-Hans/getting-started.html
（2）react-native-community/async-storage （应该不需要单独配置）


## iOS 适配问题
1   如果使用 React-Navigation 自带的导航栏，则不用考虑 头部适配问题。
2   如果设置了 header:null ,并且自定义头部的话，要考虑头部适配问题：
 （1）无刘海屏幕：  状态栏高度：20   +    导航栏高度:  44
 （2）有刘海屏幕 :    状态栏高度：44   +    导航栏高度:  44
 （3）有刘海屏幕底部安全高度：  34
 
 如果简单的适配就不要去用 <SafeArea> 去做了，自己对比这高度适配一下。
 
 ## 组件库
 本项目集成了 Ant Design 组件库： https://rn.mobile.ant.design/docs/react/introduce-cn


## 关于 redux
使用 redux 之前，记住一句话：  “You Might Not Need Redux”  — 摘自官方文档
redux 是比较消耗性能。原因：
（1）action触发大量reducer的开销
（2）通过connect()连接store的subscriber，在store改变时被触发的开销
（3）渲染大型虚拟DOM的开销。

1  什么情况使用 redux 
    项目中若有某个数据，被很多页面 频繁使用，而且可能 频繁发生改变。 发生改变后，所有用到该数据的页面 需要及时更新状态。
    此时 需要使用 redux 来提高效率。
    
2  怎么使用 redux
    项目中有例子，可以参考例子。找到 redux 文件夹。
    （1）首先在 types/ActionTypes.js 中给 事件 命名 。
    （2）在 actions 目录下创建 一个  关于此次事件的 xxxAction.js 文件。
    （3）在 xxxAction.js 文件 创建 xxx 方法 并导出（dispatch 和 dispatch中的 type 是很关键的）。
    （4）在 reducers 目录下 创建 xxxReducer.js 文件。
    （5）在 xxxReducer.js 文件 中 return 出新的 状态。
    （6）在 reducers/RootReducer.js 中的 combineReducers 中 注册你刚才写的 xxxReducer。 例如：aaaa:  xxxReducer （注意 这个 aaaa 很重要）
    （7）在需要使用到 redux 的页面 （如: DemoPages 3）用 connect（）连接 页面。 connect 是一个高阶函数。connect 中需注意： state => 是连接到 reducer 中的。 bbbb: state.aaaa  其中 bbbb可以自定义命名，aaaa必须为你刚刚在 combineReducers 中 注册的 aaaa。 项目中 使用  this.props.bbbb 来使用 该数据。
    （8）connect 中的 dispatch 是连接到 action的。 使用 : this.props.updateUserNam（）
     
3 注意事项：
    redux 中千万不要放 数据量大的数据。
    
    
    
## 关于 Xcode 13.0 (12.5以上) 无法正常启动的问题

1. 在 RCTCxxBridge.mm 文件中

_initializeModules:(NSArray<id<RCTBridgeModule>> *)modules

改为

_initializeModules:(NSArray<Class> *)modules

可正常运行


2.  在 Libraries/RCTWebSocket/fishhook.c 文件中 

indirect_symbol_bindings[i] = cur->rebindings[j].replacement;

替换为

if (i < ( sizeof(indirect_symbol_bindings) / sizeof(indirect_symbol_bindings[0]))) {
    indirect_symbol_bindings[i] = cur->rebindings[j].replacement;
}


安卓修改 否则无法打包
jpush react native 的 build gradle 改成
compileOnly fileTree(dir: 'libs', include: ['*.jar'])



## 关于 iOS 16.0 UIPickerView 高度适配问题
在 BzwPicker.m 文件中:

    self.pick = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, self.frame.size.width, self.frame.size.height-40)];
    
    改为:
    
    CGFloat pickerOffsetHeight = 40;
    if ([[UIDevice currentDevice].systemVersion doubleValue] >= 16.0 ) {
        pickerOffsetHeight = 0;
    }
    self.pick = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, self.frame.size.width, self.frame.size.height-pickerOffsetHeight)];
    
    

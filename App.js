/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StatusBar,
    View,
    StyleSheet,
    Text,
    TextInput
} from 'react-native';
import Main from './src/component/Main';
import {ToastComponent} from './src/common/CustomComponent/ToastManager';
import {SpinnerComponent} from './src/common/CustomComponent/SpinnerManager';
import {Provider} from 'react-redux';
import store from './src/redux/store/CreateStore';
import {Colors} from "./src/common/Constants";
import PrototypeFunction from './src/util/PrototypeFunction';

let statusBar = {
    backgroundColor: Colors.transparency,
    barStyle: 'dark-content', //light-content
    hidden: false,
    translucent: true
};

class App extends Component {
    constructor(props) {
        super(props);
        // 用于接收安卓(ios) 初始化的参数
        window.appLaunchOptions = props.appLaunchOptions
    }

    componentWillMount(){
        // 防止字体随着系统字体变化而变化
        Text.defaultProps={...(Text.defaultProps||{}),allowFontScaling:false};
        TextInput.defaultProps={...(TextInput.defaultProps||{}),allowFontScaling:false};

        // 解决 字体在 安卓手机上的 BUG
        const fontFamilyStyle = Platform.select({
            android: { fontFamily: '' },
            ios: {}
        })
        const sourceRender = Text.render;
        Text.render = function render(props, ref) {
            return sourceRender.apply(this, [{ ...props, style: [fontFamilyStyle, props.style] }, ref]);
        };

        PrototypeFunction.defineArrayRemove()
    }

    componentDidMount(){
        
    }

    componentWillUnmount(){
        
    }

    getStatusBar() {
        if(Platform.OS  == 'ios'){
            return null
        }
        return <StatusBar {...statusBar} />
    }

    render() {
        return (
            <Provider store={store}>
                <View style={styles.container}>
                    {this.getStatusBar()}
                    <Main/>
                    <ToastComponent/>
                    <SpinnerComponent/>
                </View>
            </Provider>
        )
    }
}

const styles = StyleSheet.create({
	container: {
        flex: 1
    },
})

export default App

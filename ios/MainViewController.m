//
//  MainViewController.m
//  JKSmartHome
//
//  Created by jk_ios on 2019/12/26.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end

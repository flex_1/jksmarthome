/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

#import "AppDelegate.h"


#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import "RNSplashScreen.h"
#import "MainViewController.h"
#import <UMShare/UMShare.h>
#import <UMCommon/UMCommon.h>
#import "DeviceListIntent.h"
#import "SceneListIntent.h"
#import "Keys.h"

#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif

#define ShortCutsNotification @"kShortCutsNotification"


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  //配置 JPush
  [self configJPush:launchOptions];
  
  RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
  RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                   moduleName:@"JKSmartHome"
                                            initialProperties:nil];

  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  MainViewController *rootViewController = [MainViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  [RNSplashScreen show];
  //UMSocial 配置
  [self confitUShareSettings];
  [self configUSharePlatforms];
  [self configShortCuts];
  return YES;
}

#pragma mark - Private
// UMSocial 信息 配置
-(void)confitUShareSettings{
  [UMConfigure initWithAppkey:HansaUMAPPKey channel:nil];
  [UMSocialGlobal shareInstance].universalLinkDic = @{@(UMSocialPlatformType_WechatSession):@"https://smarthouse.hansa-tec.com/",
      @(UMSocialPlatformType_QQ):@"https://smarthouse.hansa-tec.com/qq_conn/101859778",
  };
}

-(void)configUSharePlatforms{
  [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:HansaWeChatAPPKey appSecret:HansaWeChatAPPSecret redirectURL:@"http://www.hansa-tec.com"];
  [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:HansaQQAPPID appSecret:nil redirectURL:nil];
}

-(void)configShortCuts{
  if (@available(iOS 12.0, *)) {
      DeviceListIntent * deviceIntent = [[DeviceListIntent alloc] init];
      deviceIntent.deviceContent = @"快捷进入设备列表(需要在小萨管家中登录)";
      INInteraction * deviceInteraction = [[INInteraction alloc] initWithIntent:deviceIntent response:nil];
      [deviceInteraction donateInteractionWithCompletion:^(NSError * _Nullable error) {

      }];

      SceneListIntent * sceneIntent = [[SceneListIntent alloc] init];
      sceneIntent.sceneContent = @"快捷进入场景列表(需要在小萨管家中登录)";
      INInteraction * sceneInteraction = [[INInteraction alloc] initWithIntent:sceneIntent response:nil];
      [sceneInteraction donateInteractionWithCompletion:^(NSError * _Nullable error) {

      }];

  } else {
      // Fallback on earlier versions
  }
}

-(void)handleShortCutsOrders:(NSUserActivity *)userActivity{
  
  // shortCuts 相关的配置
  if (@available(iOS 12.0, *)) {
      if([userActivity.activityType isEqualToString: NSStringFromClass([DeviceListIntent class])]){
        // 从快捷指令进入设备列表
        [[NSNotificationCenter defaultCenter] postNotificationName:ShortCutsNotification object:nil userInfo:@{@"type":@"device"}];
      }else if([userActivity.activityType isEqualToString: NSStringFromClass([SceneListIntent class])]){
        // 从快捷指令进入场景列表
        [[NSNotificationCenter defaultCenter] postNotificationName:ShortCutsNotification object:nil userInfo:@{@"type":@"scene"}];
      }else if([userActivity.activityType isEqualToString: @"com.smarthome.openScene"]){
        NSDictionary * sceneInfo = userActivity.userInfo;
        NSString * sceneId = sceneInfo[@"sceneId"];
        NSString * sceneName = sceneInfo[@"sceneName"];
        [[NSNotificationCenter defaultCenter] postNotificationName:ShortCutsNotification object:nil userInfo:@{@"type":@"excuteScene",@"sceneId":sceneId,@"sceneName":sceneName}];
      }else if([userActivity.activityType isEqualToString: @"com.smarthome.openDevice"]){
        NSDictionary * deviceInfo = userActivity.userInfo;
        NSString * deviceId = deviceInfo[@"deviceId"];
        NSString * deviceName = deviceInfo[@"deviceName"];
        NSInteger status = [deviceInfo[@"status"] integerValue];
        [[NSNotificationCenter defaultCenter] postNotificationName:ShortCutsNotification object:nil userInfo:@{@"type":@"excuteDevice",@"deviceId":deviceId,@"deviceName":deviceName,@"status":@(status)}];
      }
    
  } else {
    // Fallback on earlier versions
  }
}

-(void)configJPush:(NSDictionary *)launchOptions{
  // JPush初始化配置
#if DEBUG
    [JPUSHService setupWithOption:launchOptions appKey:HansaJPushAPPKey channel:nil apsForProduction:NO];
#else
    [JPUSHService setupWithOption:launchOptions appKey:HansaJPushAPPKey channel:nil apsForProduction:YES];
#endif
  // APNS
  JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
  if (@available(iOS 12.0, *)) {
    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound|JPAuthorizationOptionProvidesAppNotificationSettings;
  }
  [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
  [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
  // 自定义消息
  NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
  [defaultCenter addObserver:self selector:@selector(networkDidReceiveMessage:) name:kJPFNetworkDidReceiveMessageNotification object:nil];
  // 地理围栏
  // [JPUSHService registerLbsGeofenceDelegate:self withLaunchOptions:launchOptions];
}

/// 在这里写支持的旋转方向，为了防止横屏方向，应用启动时候界面变为横屏模式
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    if (self.allowOrentitaionRotation) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - System

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
#if DEBUG
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
#else
  return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options{
  
    BOOL result = [[UMSocialManager defaultManager]  handleOpenURL:url options:options];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void(^)(NSArray<id<UIUserActivityRestoring>> * __nullable restorableObjects))restorationHandler{
  
  if (![[UMSocialManager defaultManager] handleUniversalLink:userActivity options:nil]) {
      // 其他SDK的回调
  }
  [self handleShortCutsOrders:userActivity];
  return YES;
}

#pragma mark - JPush Delegate
//注册 APNS 成功并上报 DeviceToken
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [JPUSHService registerDeviceToken:deviceToken];
}

//iOS 7 APNS
- (void)application:(UIApplication *)application didReceiveRemoteNotification:  (NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
  // iOS 10 以下 Required
  NSLog(@"iOS 7 APNS");
  [JPUSHService handleRemoteNotification:userInfo];
  [[NSNotificationCenter defaultCenter] postNotificationName:J_APNS_NOTIFICATION_ARRIVED_EVENT object:userInfo];
  completionHandler(UIBackgroundFetchResultNewData);
}

//iOS 10 前台收到消息
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center  willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
  NSDictionary * userInfo = notification.request.content.userInfo;
  if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
    // Apns
    NSLog(@"iOS 10 APNS 前台收到消息");
    [JPUSHService handleRemoteNotification:userInfo];
    [[NSNotificationCenter defaultCenter] postNotificationName:J_APNS_NOTIFICATION_ARRIVED_EVENT object:userInfo];
  }
  else {
    // 本地通知 todo
    NSLog(@"iOS 10 本地通知 前台收到消息");
    [[NSNotificationCenter defaultCenter] postNotificationName:J_LOCAL_NOTIFICATION_ARRIVED_EVENT object:userInfo];
  }
  //需要执行这个方法，选择是否提醒用户，有 Badge、Sound、Alert 三种类型可以选择设置
  completionHandler(UNNotificationPresentationOptionAlert);
}

//iOS 10 消息事件回调
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler: (void (^)(void))completionHandler {
  NSDictionary * userInfo = response.notification.request.content.userInfo;
  if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
    // Apns
    NSLog(@"iOS 10 APNS 消息事件回调");
    [JPUSHService handleRemoteNotification:userInfo];
    // 保障应用被杀死状态下，用户点击推送消息，打开app后可以收到点击通知事件
    [[RCTJPushEventQueue sharedInstance]._notificationQueue insertObject:userInfo atIndex:0];
    [[NSNotificationCenter defaultCenter] postNotificationName:J_APNS_NOTIFICATION_OPENED_EVENT object:userInfo];
  }
  else {
    // 本地通知
    NSLog(@"iOS 10 本地通知 消息事件回调");
    // 保障应用被杀死状态下，用户点击推送消息，打开app后可以收到点击通知事件
    [[RCTJPushEventQueue sharedInstance]._localNotificationQueue insertObject:userInfo atIndex:0];
    [[NSNotificationCenter defaultCenter] postNotificationName:J_LOCAL_NOTIFICATION_OPENED_EVENT object:userInfo];
  }
  // 系统要求执行这个方法
  completionHandler();
}

//自定义消息
- (void)networkDidReceiveMessage:(NSNotification *)notification {
  NSDictionary * userInfo = [notification userInfo];
  [[NSNotificationCenter defaultCenter] postNotificationName:J_CUSTOM_NOTIFICATION_EVENT object:userInfo];
}

//获取用户授权后
- (void)jpushNotificationAuthorization:(JPAuthorizationStatus)status withInfo:(NSDictionary *)info {
  
}

- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center openSettingsForNotification:(UNNotification *)notification NS_AVAILABLE_IOS(12.0){
  
}

@end

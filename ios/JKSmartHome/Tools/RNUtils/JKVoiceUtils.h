//
//  JKVoiceUtils.h
//  JKSmartHome
//
//  Created by jk_ios on 2020/12/28.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import <React/RCTBridge.h>

@interface JKVoiceUtils : RCTEventEmitter <RCTBridgeModule>

@end


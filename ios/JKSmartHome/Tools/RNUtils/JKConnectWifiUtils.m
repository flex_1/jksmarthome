//
//  JKConnectWifiUtils.m
//  JKSmartHome
//
//  Created by jk_ios on 2021/12/27.
//  Copyright © 2021 Facebook. All rights reserved.
//

#import "JKConnectWifiUtils.h"

#import "ESPViewController.h"
#import "ESPTouchTask.h"
#import "ESPTouchResult.h"
#import "ESP_NetUtil.h"
#import "ESPTouchDelegate.h"
#import "ESPAES.h"
#import "AFNetworking.h"
#import "ESPTools.h"
#import <CoreLocation/CoreLocation.h>
#import "WJLocalNetWorkManager.h"

#define RN_WIFI_SUCCESS @"WifiConnectSuccess"
#define RN_WIFI_FAIL @"WifiConnectFail"
#define RN_WIFI_CHANGED @"WifiHaveBeenChanged"

#define iOS10 ([[UIDevice currentDevice].systemVersion doubleValue] >= 10.0)


@interface EspTouchDelegateImpl : NSObject<ESPTouchDelegate>

@end

@implementation EspTouchDelegateImpl

-(void) dismissAlert:(UIAlertView *)alertView{
    [alertView dismissWithClickedButtonIndex:[alertView cancelButtonIndex] animated:YES];
}

-(void) showAlertWithResult: (ESPTouchResult *) result{
    NSString *title = nil;
    NSString *message = [NSString stringWithFormat:@"%@ %@" , result.bssid, NSLocalizedString(@"EspTouch-result-one", nil)];
    NSTimeInterval dismissSeconds = 3.5;
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [alertView show];
    [self performSelector:@selector(dismissAlert:) withObject:alertView afterDelay:dismissSeconds];
}

-(void) onEsptouchResultAddedWithResult: (ESPTouchResult *) result{
    NSLog(@"EspTouchDelegateImpl onEsptouchResultAddedWithResult bssid: %@", result.bssid);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showAlertWithResult:result];
    });
    NSString *message = [NSString stringWithFormat:@"%@ %@" , result.bssid, NSLocalizedString(@"EspTouch-result-one", nil)];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"deviceConfigResult" object:message];
}

@end


@interface JKConnectWifiUtils ()<CLLocationManagerDelegate>

@property (nonatomic, strong)NSDictionary *netInfo;
@property (nonatomic, strong) NSCondition *_condition;
@property (atomic, strong) ESPTouchTask *_esptouchTask;
@property (nonatomic, strong) EspTouchDelegateImpl *_esptouchDelegate;


@end

@implementation JKConnectWifiUtils{
    CLLocationManager *_locationManagerSystem;
}

RCT_EXPORT_MODULE();

- (dispatch_queue_t)methodQueue{
  return dispatch_get_main_queue();
}

-(NSArray<NSString *> *)supportedEvents{
  return @[RN_WIFI_SUCCESS,RN_WIFI_FAIL,RN_WIFI_CHANGED];
}

#pragma mark -- React Native Method
RCT_EXPORT_METHOD(requestLocalNetworkAuth: (RCTResponseSenderBlock)callback){
    [WJLocalNetWorkManager requestLocalNetworkAuthorization:^(BOOL isAuth) {
        callback(@[@(isAuth)]);
    }];
}

RCT_EXPORT_METHOD(requestAuth: (RCTResponseSenderBlock)callback){
    [self userLocationAuth:callback];
}

RCT_EXPORT_METHOD(configWifi){
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager startMonitoring];
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        [self wifiViewUpdates];
    }];
}

RCT_EXPORT_METHOD(stopMonitoringWifi){
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager stopMonitoring];
}

RCT_EXPORT_METHOD(startConnectWifi:(NSString *)ssid bssid:(NSString *)bssid password:(NSString *)password){
    [self connectNetwork:ssid bssid:bssid password:password];
}

RCT_EXPORT_METHOD(stopConnectWifi){
    [self cancel];
}

RCT_EXPORT_METHOD(wifiUpdate){
    [self wifiViewUpdates];
}

RCT_EXPORT_METHOD(gotoSystemWifiList){
  NSString * urlString = @"App-Prefs:root=WIFI";
  if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:urlString]]) {
      if (iOS10) {
          [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString] options:@{} completionHandler:nil];
      } else {
          [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=WIFI"] options:@{} completionHandler:nil];
      }
  }
}

#pragma mark -- Private Method
-(void)connectNetwork: (NSString *)ssid bssid:(NSString *)bssid password:(NSString *)password{
    NSString *apSsid = ssid;
    NSString *apBssid = bssid;
    NSString *apPwd = password;
    int taskCount = 1;
    BOOL broadcast = YES;
  
    //开始配网
    // [self._spinner startAnimating];
    dispatch_queue_t  queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
            NSLog(@"ESPViewController do the execute work...");
            // execute the task
            NSArray *esptouchResultArray = [self executeForResultsWithSsid:apSsid bssid:apBssid password:apPwd taskCount:taskCount broadcast:broadcast];
            // show the result to the user in UI Main Thread
            dispatch_async(dispatch_get_main_queue(), ^{
                // [self._spinner stopAnimating];
                
                ESPTouchResult *firstResult = [esptouchResultArray objectAtIndex:0];
                // check whether the task is cancelled and no results received
                if (!firstResult.isCancelled){
                    NSMutableString *mutableStr = [[NSMutableString alloc]init];
                    NSMutableDictionary *resultDic = [[NSMutableDictionary alloc]init];
                    NSUInteger count = 0;
                    // max results to be displayed, if it is more than maxDisplayCount,
                    // just show the count of redundant ones
                    const int maxDisplayCount = 5;
                    if ([firstResult isSuc]) {
                        for (int i = 0; i < [esptouchResultArray count]; ++i) {
                            ESPTouchResult *resultInArray = [esptouchResultArray objectAtIndex:i];
                            NSString *resultStr = [NSString stringWithFormat:@"Bssid: %@, Address: %@\n", resultInArray.bssid, resultInArray.getAddressString];
                            resultDic[@"bssid"] = resultInArray.bssid;
                            resultDic[@"address"] = resultInArray.getAddressString;
                            [mutableStr appendString:resultStr];
                            if (++count >= maxDisplayCount) {
                                break;
                            }
                        }
                        
                        if (count < [esptouchResultArray count]) {
                            //[mutableStr appendString:NSLocalizedString(@"EspTouch-more-results-message", nil)];
                        }
                        [self sendEventWithName:RN_WIFI_SUCCESS body:resultDic];
                        NSLog(@"%@",mutableStr);
                    }else {
                        [self sendEventWithName:RN_WIFI_FAIL body:nil];
                        NSLog(@"%@",@"配网失败");
                    }
                }
            });
        });
}

- (NSArray *) executeForResultsWithSsid:(NSString *)apSsid bssid:(NSString *)apBssid password:(NSString *)apPwd taskCount:(int)taskCount broadcast:(BOOL)broadcast{
      [self._condition lock];
      self._esptouchTask = [[ESPTouchTask alloc]initWithApSsid:apSsid andApBssid:apBssid andApPwd:apPwd];
      // set delegate
      [self._esptouchTask setEsptouchDelegate:self._esptouchDelegate];
      [self._esptouchTask setPackageBroadcast:broadcast];
      [self._condition unlock];
  //    ESPTouchResult *ESPTR = self._esptouchTask.executeForResult;
      NSArray * esptouchResults = [self._esptouchTask executeForResults:taskCount];
      NSLog(@"ESPViewController executeForResult() result is: %@",esptouchResults);
      return esptouchResults;
}

// 取消配网
- (void) cancel{
    [self._condition lock];
    if (self._esptouchTask != nil){
        [self._esptouchTask interrupt];
    }
    [self._condition unlock];
}

- (NSDictionary *)fetchNetInfo{
    NSMutableDictionary *wifiDic = [NSMutableDictionary dictionaryWithCapacity:0];
    wifiDic[@"ssid"] = ESPTools.getCurrentWiFiSsid;
    wifiDic[@"bssid"] = ESPTools.getCurrentBSSID;
    return wifiDic;
}

- (void)wifiViewUpdates {
    self.netInfo = [self fetchNetInfo];
    NSString * ssid = [_netInfo objectForKey:@"ssid"];
    NSString * bssid = [_netInfo objectForKey:@"bssid"];
    if(!ssid){
      ssid = @"";
    }
    if(!bssid){
      bssid = @"";
    }
    [self sendEventWithName:RN_WIFI_CHANGED body:@{@"ssid":ssid, @"bssid":bssid}];
}

- (void)userLocationAuth: (RCTResponseSenderBlock)rnCallBack {
    if (![self getUserLocationAuth]) {
        _locationManagerSystem = [[CLLocationManager alloc]init];
        _locationManagerSystem.delegate = self;
        [_locationManagerSystem requestWhenInUseAuthorization];
    }else{
        rnCallBack(@[@"true"]);
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    BOOL result = NO;
    switch (status) {
           case kCLAuthorizationStatusNotDetermined:
               break;
           case kCLAuthorizationStatusRestricted:
               break;
           case kCLAuthorizationStatusDenied:
                result = YES;
               break;
           case kCLAuthorizationStatusAuthorizedAlways:
               break;
           case kCLAuthorizationStatusAuthorizedWhenInUse:
               break;
               
           default:
               break;
       }
    if (result) {
         UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"EspTouch-location-title", nil) message:NSLocalizedString(@"EspTouch-location-content", nil) preferredStyle:UIAlertControllerStyleAlert];
         
         UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
         UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"设置" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
              [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
         }];
         [alert addAction:action1];
         [alert addAction:action2];
         [[UIApplication sharedApplication].windows[0].rootViewController presentViewController:alert animated:YES completion:nil];
    }
}

- (BOOL)getUserLocationAuth {
    BOOL result = NO;
    switch ([CLLocationManager authorizationStatus]) {
        case kCLAuthorizationStatusNotDetermined:
            break;
        case kCLAuthorizationStatusRestricted:
            break;
        case kCLAuthorizationStatusDenied:
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
            result = YES;
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            result = YES;
            break;
            
        default:
            break;
    }
    return result;
}


@end

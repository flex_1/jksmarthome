//
//  JKConnectWifiUtils.h
//  JKSmartHome
//
//  Created by jk_ios on 2021/12/27.
//  Copyright © 2021 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import <React/RCTBridge.h>

NS_ASSUME_NONNULL_BEGIN

@interface JKConnectWifiUtils : RCTEventEmitter<RCTBridgeModule>

@end

NS_ASSUME_NONNULL_END

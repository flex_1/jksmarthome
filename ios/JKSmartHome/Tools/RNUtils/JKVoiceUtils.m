//
//  JKVoiceUtils.m
//  JKSmartHome
//
//  Created by jk_ios on 2020/12/28.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import "JKVoiceUtils.h"

#import <AVFoundation/AVFoundation.h>
#import "BDSEventManager.h"
#import "BDSASRDefines.h"
#import "BDSASRParameters.h"

#import "BDSWakeupDefines.h"
#import "BDSWakeupParameters.h"
#import "MBProgressHUD.h"
#import "SVProgressHUD.h"
#import "Keys.h"

#define RN_VOICE_WAKEUP @"VocieWakeUpNotification"
#define RN_VOICE_SENDTEXT @"VocieSendTextNotification"
#define RN_ERROR_TEXT @"VocieErrorTextNotification"

#define STATUS_CLOSE @(0)
#define STATUS_WAKE_UP @(1)
#define STATUS_RECOGNIZING @(2)
#define STATUS_RECOGNIZE_DONE @(3)


@interface JKVoiceUtils ()<BDSClientASRDelegate, BDSClientWakeupDelegate>

@property (strong, nonatomic) BDSEventManager *asrEventManager;
@property (strong, nonatomic) BDSEventManager *wakeupEventManager;

@end

@implementation JKVoiceUtils

RCT_EXPORT_MODULE();

-(NSArray<NSString *> *)supportedEvents{
  return @[RN_VOICE_WAKEUP,RN_VOICE_SENDTEXT,RN_ERROR_TEXT];
}

- (dispatch_queue_t)methodQueue{
  return dispatch_get_main_queue();
}

RCT_EXPORT_METHOD(settingUp){
    // 创建语音识别对象
    self.asrEventManager = [BDSEventManager createEventManagerWithName:BDS_ASR_NAME];
    // 设置语音识别代理
    [self configVoiceRecognitionClient];
  
    //创建语音识别对象
    self.wakeupEventManager = [BDSEventManager createEventManagerWithName:BDS_WAKEUP_NAME];
    // 设置语音唤醒代理
    [self startWakeup];
}

//暂停识别
RCT_EXPORT_METHOD(pauseRecongize){
    if(!self.asrEventManager){
      return;
    }
    [self stopRecognizing];
}

// 停止识别
RCT_EXPORT_METHOD(stopRecongize){
    if(!self.asrEventManager){
      return;
    }
    [self stopRecognizing];
}

// 停止唤醒
RCT_EXPORT_METHOD(stopWake){
    if(!self.wakeupEventManager){
      return;
    }
    [self stopWakeup];
    [self sendEventWithName:RN_VOICE_WAKEUP body:STATUS_CLOSE];
}

// 开启唤醒
RCT_EXPORT_METHOD(startWake){
    [self startWakeup];
}

// 开启识别
RCT_EXPORT_METHOD(startRecord){
    [self startRecognize];
}

#pragma mark - Private

- (NSString *)getDescriptionForDic:(NSDictionary *)dic {
    if (dic) {
        return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dic
                                                                              options:NSJSONWritingPrettyPrinted
                                                                                error:nil] encoding:NSUTF8StringEncoding];
    }
    return nil;
}

- (void)startRecognize{
    [self.asrEventManager setParameter:@(NO) forKey:BDS_ASR_ENABLE_LONG_SPEECH];
    [self.asrEventManager setParameter:@(NO) forKey:BDS_ASR_NEED_CACHE_AUDIO];
    [self.asrEventManager setParameter:@"" forKey:BDS_ASR_OFFLINE_ENGINE_TRIGGERED_WAKEUP_WORD];
  
    [self.asrEventManager setParameter:nil forKey:BDS_ASR_AUDIO_FILE_PATH];
    [self.asrEventManager setParameter:nil forKey:BDS_ASR_AUDIO_INPUT_STREAM];
    [self.asrEventManager sendCommand:BDS_ASR_CMD_START];
}

// 关闭识别
-(void)stopRecognizing{
    [self.asrEventManager sendCommand:BDS_ASR_CMD_STOP];
    [self.asrEventManager sendCommand:BDS_ASR_CMD_CANCEL];
}

- (void)startWakeup{
    [self configWakeupClient];
    [self.wakeupEventManager setParameter:nil forKey:BDS_WAKEUP_AUDIO_FILE_PATH];
    [self.wakeupEventManager setParameter:nil forKey:BDS_WAKEUP_AUDIO_INPUT_STREAM];
    [self.wakeupEventManager sendCommand:BDS_WP_CMD_LOAD_ENGINE];
    [self.wakeupEventManager sendCommand:BDS_WP_CMD_START];
}

// 关闭唤醒功能
- (void)stopWakeup{
    [self.wakeupEventManager sendCommand:BDS_WP_CMD_STOP];
    [self.wakeupEventManager sendCommand:BDS_WP_CMD_UNLOAD_ENGINE];
}

// 语音识别错误分析
- (void)recoginzeErrorAnalyzation:(NSInteger)errorCode{
    NSString * errDescription = @"识别出错";
    switch (errorCode) {
      case EVRClientErrorCodeRecoderException:
        errDescription = @"录音设备异常";
        break;
      
      case EVRClientErrorCodeRecoderNoPermission:
        errDescription = @"无录音权限";
        break;
        
      case EVRClientErrorCodeRecoderUnAvailable:
        errDescription = @"录音设备不可用";
        break;
        
      case EVRClientErrorCodeInterruption:
        errDescription = @"录音中断";
        break;
        
      case EVRClientErrorCodeNoSpeech:
        errDescription = @"用户未说话";
        break;
        
      case EVRClientErrorCodeShort:
        errDescription = @"用户说话声音太短";
        break;
        
      case EVRClientErrorCodeLocalTimeout:
        errDescription = @"请求超时";
        break;
        
      case EVRClientErrorCodeServerSpeechTooLong:
        errDescription = @"语音过长";
        break;
        
      case EVRClientErrorCodeDecoderNetworkUnavailable:
        errDescription = @"网络不可用";
        break;
        
      default:
        break;
    }
    [self sendEventWithName:RN_ERROR_TEXT body:errDescription];
}

// 语音唤醒错误分析
- (void)wakeupErrorAnalyzation:(NSInteger)errorCode{
    NSString * errDescription = @"唤醒出错";
    switch (errorCode) {
      case EWakeupEngineRecoderException:
        errDescription = @"录音设备异常";
        break;
      
      case EWakeupEngineRecoderNoPermission:
        errDescription = @"无录音权限";
        break;
        
      case EWakeupEngineRecoderUnAvailable:
        errDescription = @"录音设备不可用";
        break;
        
      case EWakeupEngineRecoderInterruption:
        errDescription = @"录音中断";
        break;
        
      case EWakeupEngineExceptioin:
        errDescription = @"唤醒引擎异常";
        break;
        
      case EWakeupEngineWakeupWordsInvalid:
        errDescription = @"唤醒次异常";
        break;
        
      case EWakeupEngineArchiNotSupportted:
        errDescription = @"引擎不支持该架构";
        break;
        
      default:
        break;
    }
    [self sendEventWithName:RN_ERROR_TEXT body:errDescription];
}

#pragma mark - Configuration

- (void) configVoiceRecognitionClient {
    [self.asrEventManager setDelegate:self];
    
    //设置DEBUG_LOG的级别
    [self.asrEventManager setParameter:@(EVRDebugLogLevelTrace) forKey:BDS_ASR_DEBUG_LOG_LEVEL];
    //配置API_KEY 和 SECRET_KEY 和 APP_ID
    [self.asrEventManager setParameter:@[BaiduAPIKEY, BaiduSECRETKEY] forKey:BDS_ASR_API_SECRET_KEYS];
    [self.asrEventManager setParameter:BaiduAPPID forKey:BDS_ASR_OFFLINE_APP_CODE];
    [self.asrEventManager setParameter:@"1537" forKey:BDS_ASR_PRODUCT_ID];
  
  
    //配置端点检测（二选一）
    [self configModelVAD];
    //    [self configDNNMFE];
    
    //    [self.asrEventManager setParameter:@"15361" forKey:BDS_ASR_PRODUCT_ID];
    // ---- 语义与标点 -----
     [self enableNLU];
//    [self enablePunctuation];
    // ------------------------
    
    //---- 语音自训练平台 ----
     [self configSmartAsr];
}

- (void)configModelVAD {
    NSString *modelVAD_filepath = [[NSBundle mainBundle] pathForResource:@"bds_easr_basic_model" ofType:@"dat"];
    [self.asrEventManager setParameter:modelVAD_filepath forKey:BDS_ASR_MODEL_VAD_DAT_FILE];
    [self.asrEventManager setParameter:@(YES) forKey:BDS_ASR_ENABLE_MODEL_VAD];
}

- (void)configWakeupClient {
    [self.wakeupEventManager setDelegate:self];
    [self.wakeupEventManager setParameter:BaiduAPPID forKey:BDS_WAKEUP_APP_CODE];
    
    [self configWakeupSettings];
}

- (void)configWakeupSettings {
    NSString* dat = [[NSBundle mainBundle] pathForResource:@"bds_easr_basic_model" ofType:@"dat"];
    
    // 自定义唤醒词
    NSString* words = [[NSBundle mainBundle] pathForResource:@"WakeUp" ofType:@"bin"];
    [self.wakeupEventManager setParameter:dat forKey:BDS_WAKEUP_DAT_FILE_PATH];
    [self.wakeupEventManager setParameter:words forKey:BDS_WAKEUP_WORDS_FILE_PATH];
}

- (void)configSmartAsr {
    // 设置语音自训练平台（星云）的模型id。 请参考 https://ai.baidu.com/smartasr/model/
    // 设置私有模型id
    [self.asrEventManager setParameter:@(12373) forKey:BDS_ASR_LM_ID];
    // 设置基础模型，8001：输入法； 8002：搜索
    // [self.asrEventManager setParameter:@"8002" forKey:BDS_ASR_PRODUCT_ID];
//     [self.asrEventManager setParameter:@"8001" forKey:BDS_ASR_PRODUCT_ID];
    
    // 配置星云标点
    [self.asrEventManager setParameter:@(EVR_PUNC_MODE_FULL) forKey:BDS_ASR_PUNCTUATION_EXT_MODE];
}

- (void) enableNLU {
    // ---- 开启语义理解 -----
    [self.asrEventManager setParameter:@(YES) forKey:BDS_ASR_ENABLE_NLU];
    //    [self.asrEventManager setParameter:@"1536" forKey:BDS_ASR_PRODUCT_ID];
}

- (void) enablePunctuation {
    // ---- 开启标点输出 -----
    [self.asrEventManager setParameter:@(NO) forKey:BDS_ASR_DISABLE_PUNCTUATION];
    // 普通话标点
    //    [self.asrEventManager setParameter:@"1537" forKey:BDS_ASR_PRODUCT_ID];
    // 英文标点
    [self.asrEventManager setParameter:@"1737" forKey:BDS_ASR_PRODUCT_ID];
    
}

#pragma mark - MVoiceRecognitionClientDelegate
- (void)VoiceRecognitionClientWorkStatus:(int)workStatus obj:(id)aObj {
    switch (workStatus) {
        case EVoiceRecognitionClientWorkStatusNewRecordData: {
            //self.statusLabel.text = @"记录数据...";
            // [self.fileHandler writeData:(NSData *)aObj];
            break;
        }
            
        case EVoiceRecognitionClientWorkStatusStartWorkIng: {
            //NSDictionary *logDic = [self parseLogToDic:aObj];
            //NSLog(@"sss+ %@", [NSString stringWithFormat:@"CALLBACK: start vr, log: %@\n", logDic]);
            //[self printLogTextView:[NSString stringWithFormat:@"CALLBACK: start vr, log: %@\n", logDic]];
            
            break;
        }
        case EVoiceRecognitionClientWorkStatusStart: {
//            [self printLogTextView:@"CALLBACK: detect voice start point.\n"];
            break;
        }
        case EVoiceRecognitionClientWorkStatusEnd: {
            //self.statusLabel.text = @"结束识别..";
//            [self printLogTextView:@"CALLBACK: detect voice end point.\n"];
            break;
        }
        case EVoiceRecognitionClientWorkStatusFlushData: {
          
            if (aObj) {
                NSString * message = aObj[@"results_recognition"];
                NSDictionary * messageDict = @{@"message":message, @"isFinished":@(0)};
                [self sendEventWithName:RN_VOICE_WAKEUP body:STATUS_RECOGNIZING];
                [self sendEventWithName:RN_VOICE_SENDTEXT body:messageDict];
            }
            break;
        }
        case EVoiceRecognitionClientWorkStatusFinish: {
            
            if (aObj) {
                NSString * message = aObj[@"results_recognition"];
                NSDictionary * messageDict = @{@"message":message, @"isFinished":@(1)};
                [self sendEventWithName:RN_VOICE_SENDTEXT body:messageDict];
                [self sendEventWithName:RN_VOICE_WAKEUP body:STATUS_RECOGNIZE_DONE];
            }
            break;
        }
        case EVoiceRecognitionClientWorkStatusMeterLevel: {
            break;
        }
        case EVoiceRecognitionClientWorkStatusCancel: {
            //[self printLogTextView:@"CALLBACK: user press cancel.\n"];
            //[self onEnd];
            break;
        }
        case EVoiceRecognitionClientWorkStatusError: {
            //[self printLogTextView:[NSString stringWithFormat:@"CALLBACK: encount error - %@.\n", (NSError *)aObj]];
            //[self onEnd];
            NSLog(@"%@", [NSString stringWithFormat:@"识别错误: %@.\n", (NSError *)aObj]);
          
            NSError * err = (NSError *)aObj;
            [self recoginzeErrorAnalyzation: err.code];
            [self sendEventWithName:RN_VOICE_WAKEUP body:STATUS_CLOSE];
            break;
        }
        case EVoiceRecognitionClientWorkStatusLoaded: {
            //[self printLogTextView:@"CALLBACK: offline engine loaded.\n"];
            break;
        }
        case EVoiceRecognitionClientWorkStatusUnLoaded: {
            //[self printLogTextView:@"CALLBACK: offline engine unLoaded.\n"];
            break;
        }
        case EVoiceRecognitionClientWorkStatusChunkThirdData: {
            //[self printLogTextView:[NSString stringWithFormat:@"CALLBACK: Chunk 3-party data length: %lu\n", (unsigned long)[(NSData *)aObj length]]];
            break;
        }
        case EVoiceRecognitionClientWorkStatusChunkNlu: {
            NSString *nlu = [[NSString alloc] initWithData:(NSData *)aObj encoding:NSUTF8StringEncoding];
            //[self printLogTextView:[NSString stringWithFormat:@"CALLBACK: Chunk NLU data: %@\n", nlu]];
            NSLog(@"%@", nlu);
            break;
        }
        case EVoiceRecognitionClientWorkStatusChunkEnd: {
            //[self printLogTextView:[NSString stringWithFormat:@"CALLBACK: Chunk end, sn: %@.\n", aObj]];
//            if (!self.longSpeechFlag) {
//                [self onEnd];
//            }
            break;
        }
        case EVoiceRecognitionClientWorkStatusFeedback: {
            //NSDictionary *logDic = [self parseLogToDic:aObj];
            //[self printLogTextView:[NSString stringWithFormat:@"CALLBACK Feedback: %@\n", logDic]];
            break;
        }
        case EVoiceRecognitionClientWorkStatusRecorderEnd: {
            
            //[self printLogTextView:@"CALLBACK: recorder closed.\n"];
            break;
        }
        case EVoiceRecognitionClientWorkStatusLongSpeechEnd: {
            //[self printLogTextView:@"CALLBACK: Long Speech end.\n"];
            //[self onEnd];
            break;
        }
        default:
            break;
    }
}

- (void)WakeupClientWorkStatus:(int)workStatus obj:(id)aObj
{
    switch (workStatus) {
        case EWakeupEngineWorkStatusStarted: {
            //[self printLogTextView:@"WAKEUP CALLBACK: Started.\n"];
            //self.statusLabel.text = @"等待唤醒...";
            
            break;
        }
        case EWakeupEngineWorkStatusStopped: {
          
            [self sendEventWithName:RN_VOICE_WAKEUP body: STATUS_CLOSE];
          
            //[self printLogTextView:@"WAKEUP CALLBACK: Stopped.\n"];
            //self.statusLabel.text = @"唤醒停止...";
            break;
        }
        case EWakeupEngineWorkStatusLoaded: {
            //self.statusLabel.text = @"唤醒加载...";
            //[self printLogTextView:@"WAKEUP CALLBACK: Loaded.\n"];
            break;
        }
        case EWakeupEngineWorkStatusUnLoaded: {
            //[self printLogTextView:@"WAKEUP CALLBACK: UnLoaded.\n"];
            //self.statusLabel.text = @"唤醒掉线..";
            break;
        }
        case EWakeupEngineWorkStatusTriggered: {
          
            [self sendEventWithName:RN_VOICE_WAKEUP body:STATUS_WAKE_UP];
            
            //[self printLogTextView:[NSString stringWithFormat:@"WAKEUP CALLBACK: Triggered - %@.\n", (NSString *)aObj]];
//            if (self.continueToVR) {
//                self.continueToVR = NO;
//                [self.asrEventManager setParameter:@(YES) forKey:BDS_ASR_NEED_CACHE_AUDIO];
//                [self.asrEventManager setParameter:aObj forKey:BDS_ASR_OFFLINE_ENGINE_TRIGGERED_WAKEUP_WORD];
//                [self voiceRecogButtonHelper];
//            }
            break;
        }
        case EWakeupEngineWorkStatusError: {
          
            NSError * err = (NSError *)aObj;
            [self wakeupErrorAnalyzation:err.code];
            [self sendEventWithName:RN_VOICE_WAKEUP body:STATUS_CLOSE];
            
            //[self printLogTextView:[NSString stringWithFormat:@"WAKEUP CALLBACK: encount error - %@.\n", (NSError *)aObj]];
            break;
        }
            
        default:
            break;
    }
}

@end

//
//  JKRNUtils.h
//  JKSmartHome
//
//  Created by jk_ios on 2020/3/31.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import <React/RCTBridge.h>

@interface JKRNUtils : RCTEventEmitter <RCTBridgeModule>

@end



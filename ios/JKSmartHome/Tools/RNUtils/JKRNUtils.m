//
//  JKRNUtils.m
//  JKSmartHome
//
//  Created by jk_ios on 2020/3/31.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import "JKRNUtils.h"
#import "AppDelegate.h"
#import <Intents/Intents.h>
#import <IntentsUI/IntentsUI.h>
#import "SVProgressHUD.h"
#import "ESPBaseViewController.h"

#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif

#define ShortCutsNotification @"kShortCutsNotification"
#define RNShortCutsNotification @"kRNShortCutsNotification"
#define KAccessTokenOfPrefrence @"KAccessTokenOfPrefrence"
#define kAppCurrentBaseUrl @"kAppCurrentBaseUrl"

#define RN_ADD_SIRI_SUCCESS @"kRNAddSiriSuccess"

@interface JKRNUtils ()<INUIAddVoiceShortcutViewControllerDelegate, INUIEditVoiceShortcutViewControllerDelegate>

@end

@implementation JKRNUtils

RCT_EXPORT_MODULE();

-(NSArray<NSString *> *)supportedEvents{
  return @[RNShortCutsNotification,RN_ADD_SIRI_SUCCESS];
}

// 注册通知
RCT_EXPORT_METHOD(registerShortCuts){
  [[NSNotificationCenter defaultCenter]addObserverForName:ShortCutsNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
    
    NSString * type = note.userInfo[@"type"];
    NSMutableDictionary * params = [NSMutableDictionary new];
    params[@"type"] = type;
    
    //场景
    if([type isEqualToString:@"excuteScene"]){
      NSString * sceneId = note.userInfo[@"sceneId"];
      NSString * sceneName = note.userInfo[@"sceneName"];
      if(sceneId && ![sceneId isEqualToString:@""]){
        params[@"sceneId"] = sceneId;
      }
      if(sceneName && ![sceneName isEqualToString:@""]){
        params[@"sceneName"] = sceneName;
      }
    }
    
    //设备
    if([type isEqualToString:@"excuteDevice"]){
      NSString * deviceId = note.userInfo[@"deviceId"];
      NSString * deviceName = note.userInfo[@"deviceName"];
      NSInteger status = [note.userInfo[@"status"] integerValue];
      if(deviceId && ![deviceId isEqualToString:@""]){
        params[@"deviceId"] = deviceId;
      }
      if(deviceName && ![deviceName isEqualToString:@""]){
        params[@"deviceName"] = deviceName;
      }
      params[@"status"] = @(status);
    }
    
    [self sendEventWithName:RNShortCutsNotification body:params];
  }];
}

//取消
RCT_EXPORT_METHOD(removeShortCutsRegister){
  [[NSNotificationCenter defaultCenter]removeObserver:self name:ShortCutsNotification object:nil];
}
  
//清除用户Token
RCT_EXPORT_METHOD(clearUserToken){
  //清除Token
  [[NSUserDefaults standardUserDefaults] removeObjectForKey:KAccessTokenOfPrefrence];
  [[NSUserDefaults standardUserDefaults] removeObjectForKey:kAppCurrentBaseUrl];
}

//检查原生端有没有存储token 并设置原生层网络请求的 baseUrl
RCT_EXPORT_METHOD(checkLocalUserToken:(NSString *)token baseUrl:(NSString *)baseUrl){
  if(baseUrl && ![baseUrl isEqualToString:@""]){
    [[NSUserDefaults standardUserDefaults] setObject:baseUrl forKey:kAppCurrentBaseUrl];
  }
  if(token && ![token isEqualToString:@""]){
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:KAccessTokenOfPrefrence];
  }
}

//配置siri快捷指令(场景)
RCT_EXPORT_METHOD(addSceneToSiriShortCut:(NSString*)sceneId sceneName:(NSString*)sceneName){
    if (@available(iOS 12.0, *)) {
      [[INVoiceShortcutCenter sharedCenter] getAllVoiceShortcutsWithCompletion:^(NSArray<INVoiceShortcut *> * _Nullable voiceShortcuts, NSError * _Nullable error) {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      
                      INVoiceShortcut * voiceSC = nil;
                      for (INVoiceShortcut *voiceShortcut in voiceShortcuts) {
                          NSString * userInfoSceneId =  voiceShortcut.shortcut.userActivity.userInfo[@"sceneId"];
                          NSString * typeString = voiceShortcut.shortcut.userActivity.activityType;
                          if ([typeString isEqualToString:@"com.smarthome.openScene"] && userInfoSceneId && [userInfoSceneId isEqualToString:sceneId] ) {
                              voiceSC = voiceShortcut;
                              break;
                          }
                      }
                    
                      if(voiceSC){
                        INUIEditVoiceShortcutViewController *vc = [[INUIEditVoiceShortcutViewController alloc] initWithVoiceShortcut:voiceSC];
                        vc.delegate = self;
                        [[UIApplication sharedApplication].windows[0].rootViewController presentViewController:vc animated:YES completion:nil];
                          
                      }else{
                        NSUserActivity *activity = [[NSUserActivity alloc] initWithActivityType:@"com.smarthome.openScene"];
                        //显示在siri 建议或者core spotlight 中的标题
                        activity.title = [NSString stringWithFormat:@"%@:%@",@"场景",sceneName];
                        //core spotlight关键词，和siri无关
                        activity.keywords = [NSSet setWithArray:@[@"执行场景",sceneName]];
                        //携带的用户信息
                        activity.userInfo = @{@"sceneId":sceneId,@"sceneName":sceneName};
                        //开启用于检索
                        activity.eligibleForSearch = YES;
                        //ios12 sirikit
                        //开启sirikit
                        activity.eligibleForPrediction = YES;
                        //建议语句，会显示在添加Siri捷径的接口中
                        activity.suggestedInvocationPhrase = sceneName;
                        
                        INShortcut * shortCut = [[INShortcut alloc] initWithUserActivity:activity];
                        INUIAddVoiceShortcutViewController * vc = [[INUIAddVoiceShortcutViewController alloc]initWithShortcut:shortCut];
                        vc.delegate = self;
                        // 回到主线程
                        [[UIApplication sharedApplication].windows[0].rootViewController presentViewController:vc animated:YES completion:nil];
                      }
                  });
        }];
    }
}

//配置siri快捷指令(设备)
RCT_EXPORT_METHOD(addDeviceOrderToSiriShortCut:(NSString*)deviceId deviceName:(NSString*)deviceName status:(NSInteger)status){
  
  if (@available(iOS 12.0, *)) {
    
    [[INVoiceShortcutCenter sharedCenter] getAllVoiceShortcutsWithCompletion:^(NSArray<INVoiceShortcut *> * _Nullable voiceShortcuts, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    INVoiceShortcut * voiceSC = nil;
                    for (INVoiceShortcut *voiceShortcut in voiceShortcuts) {
                        NSString * userInfoDeviceId =  voiceShortcut.shortcut.userActivity.userInfo[@"deviceId"];
                        NSString * statusString = voiceShortcut.shortcut.userActivity.userInfo[@"status"] ;
                        NSInteger deviceStatus = [statusString integerValue];
                        NSString * typeString = voiceShortcut.shortcut.userActivity.activityType;
                        
                        if ([typeString isEqualToString:@"com.smarthome.openDevice"] && userInfoDeviceId && [userInfoDeviceId isEqualToString:deviceId] && deviceStatus == status) {
                            voiceSC = voiceShortcut;
                            break;
                        }
                    }
                  
                    if(voiceSC){
                      INUIEditVoiceShortcutViewController *vc = [[INUIEditVoiceShortcutViewController alloc] initWithVoiceShortcut:voiceSC];
                      vc.delegate = self;
                      [[UIApplication sharedApplication].windows[0].rootViewController presentViewController:vc animated:YES completion:nil];
                        
                    }else{
                      NSUserActivity *activity = [[NSUserActivity alloc] initWithActivityType:@"com.smarthome.openDevice"];
                      //显示在siri 建议或者core spotlight 中的标题
                      NSString * excuteName = status ? @"打开" : @"关闭";
                      activity.title = [NSString stringWithFormat:@"%@ %@",excuteName,deviceName];
                      //core spotlight关键词，和siri无关
                      activity.keywords = [NSSet setWithArray:@[deviceName,@"打开",@"关闭"]];
                      
                      //携带的用户信息
                      activity.userInfo = @{@"deviceId":deviceId,@"deviceName":deviceName,@"status":@(status)};
                      //开启用于检索
                      activity.eligibleForSearch = YES;
                      //ios12 sirikit
                      //开启sirikit
                      activity.eligibleForPrediction = YES;
                      //建议语句，会显示在添加Siri捷径的接口中
                      activity.suggestedInvocationPhrase = [NSString stringWithFormat:@"%@%@",excuteName,deviceName];
                      INShortcut * shortCut = [[INShortcut alloc] initWithUserActivity:activity];
                      INUIAddVoiceShortcutViewController * vc = [[INUIAddVoiceShortcutViewController alloc]initWithShortcut:shortCut];
                      vc.delegate = self;
                      // 回到主线程
                      [[UIApplication sharedApplication].windows[0].rootViewController presentViewController:vc animated:YES completion:nil];
                    }
                });
    }];
  }
}

// 获取推送通知状态
RCT_EXPORT_METHOD( getSystemNoticeStatus: (RCTPromiseResolveBlock) resolve
rejecter: (RCTPromiseRejectBlock) reject ){
  
  dispatch_async( dispatch_get_main_queue(), ^{
        float systemVersion = [[UIDevice currentDevice].systemVersion floatValue];
    
        if ( systemVersion >= 8.0 && systemVersion < 10.0 ){
          UIUserNotificationSettings *settings = [[UIApplication sharedApplication] currentUserNotificationSettings];
          UIUserNotificationType type = settings.types;
          if ( type == UIUserNotificationTypeNone ){
            return(resolve (@NO) );
          }else  {
            return(resolve (@YES) );
          }
        }else if ( systemVersion >= 10.0 ){
          [[UNUserNotificationCenter currentNotificationCenter] getNotificationSettingsWithCompletionHandler: ^ (UNNotificationSettings * _Nonnull settings) {
             switch ( settings.authorizationStatus ){
               case UNAuthorizationStatusDenied:
                 return(resolve (@NO) );
                 break;
                 
               case UNAuthorizationStatusNotDetermined:
                 return(resolve (@NO) );
                 break;
                 
               case UNAuthorizationStatusAuthorized:
                 return(resolve (@YES) );
                 break;
               
               case UNAuthorizationStatusProvisional:
          
                 break;
                 
               case UNAuthorizationStatusEphemeral:
                 
                 break;
             }
           }];
        }
    });
}

RCT_EXPORT_METHOD(ESTouchConfigNet){
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
    dispatch_async(dispatch_get_main_queue(), ^{
      
      UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:[ESPBaseViewController new]];
      nav.navigationBarHidden = NO;
      nav.modalPresentationStyle = 0;
      [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:YES completion:nil];
      
    });
  });
}

#pragma mark - Delegate
- (void)addVoiceShortcutViewController:(INUIAddVoiceShortcutViewController *)controller didFinishWithVoiceShortcut:(nullable INVoiceShortcut *)voiceShortcut error:(nullable NSError *)error API_AVAILABLE(ios(12.0)){
    [[UIApplication sharedApplication].windows[0].rootViewController dismissViewControllerAnimated:YES completion:^{
      [self sendEventWithName:RN_ADD_SIRI_SUCCESS body:@(1)];
    }];
}

- (void)addVoiceShortcutViewControllerDidCancel:(INUIAddVoiceShortcutViewController *)controller API_AVAILABLE(ios(12.0)){
    [[UIApplication sharedApplication].windows[0].rootViewController dismissViewControllerAnimated:YES completion:^{
    
    }];
}

- (void)editVoiceShortcutViewController:(INUIEditVoiceShortcutViewController *)controller didUpdateVoiceShortcut:(nullable INVoiceShortcut *)voiceShortcut error:(nullable NSError *)error API_AVAILABLE(ios(12.0)){
      [[UIApplication sharedApplication].windows[0].rootViewController dismissViewControllerAnimated:YES completion:^{
          [self sendEventWithName:RN_ADD_SIRI_SUCCESS body:@(2)];
      }];
}

- (void)editVoiceShortcutViewController:(INUIEditVoiceShortcutViewController *)controller didDeleteVoiceShortcutWithIdentifier:(NSUUID *)deletedVoiceShortcutIdentifier API_AVAILABLE(ios(12.0)){
      [[UIApplication sharedApplication].windows[0].rootViewController dismissViewControllerAnimated:YES completion:^{
          [self sendEventWithName:RN_ADD_SIRI_SUCCESS body:@(3)];
      }];
}

- (void)editVoiceShortcutViewControllerDidCancel:(INUIEditVoiceShortcutViewController *)controller API_AVAILABLE(ios(12.0)){
      [[UIApplication sharedApplication].windows[0].rootViewController dismissViewControllerAnimated:YES completion:^{

      }];
}

- (void)dealloc{
  [[NSNotificationCenter defaultCenter]removeObserver:self name:ShortCutsNotification object:nil];
}

@end

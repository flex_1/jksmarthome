//
//  JKAPIRequest.m
//  videoTestOC
//
//  Created by jk_ios on 2020/4/15.
//  Copyright © 2020 jk_ios. All rights reserved.
//

#import "JKAPIRequest.h"
#import "JKNetWorkTool.h"

@implementation JKAPIRequest

+(void)requestUnredMessage:(NSString *)seriNo success:(SuccessHandler)successHandler error:(ErrorHandler)errorHandler{
    
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithDictionary:@{@"devSerial":seriNo}];
    [NetworkTool requestMethod:NetworkMethodPOST urlString:@"/api/ysread/getNotRead" params:params success:^(id data) {
        if(successHandler){
            successHandler(data);
        }
    } error:^(NSError *error) {
        if(errorHandler){
            errorHandler(error);
        }
    } needToken:YES];
}

+(void)requestMessageList:(NSString *)seriNo page:(NSInteger)page success:(SuccessHandler)successHandler error:(ErrorHandler)errorHandler{
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithDictionary:@{@"devSerial":seriNo}];
    params[@"page"] = @(page);
    [NetworkTool requestMethod:NetworkMethodPOST urlString:@"/api/ysread/getReadMessage" params:params success:^(id data) {
        if(successHandler){
            successHandler(data);
        }
    } error:^(NSError *error) {
        if(errorHandler){
            errorHandler(error);
        }
    } needToken:YES];
}

+(void)setAlarmStatus:(NSString *)seriNo alarmIds:(NSArray *)alarmIds success:(SuccessHandler)successHandler error:(ErrorHandler)errorHandler{
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithDictionary:@{@"devSerial":seriNo}];
    params[@"ids"] = [alarmIds componentsJoinedByString:@","];
    [NetworkTool requestMethod:NetworkMethodPOST urlString:@"/api/ysread/modifiedAllRead" params:params success:^(id data) {
        if(successHandler){
            successHandler(data);
        }
    } error:^(NSError *error) {
        if(errorHandler){
            errorHandler(error);
        }
    } needToken:YES];
}

+(void)deleteMessage:(NSString *)seriNo alarmIds:(NSArray *)alarmIds success:(SuccessHandler)successHandler error:(ErrorHandler)errorHandler{
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithDictionary:@{@"devSerial":seriNo}];
    params[@"ids"] = [alarmIds componentsJoinedByString:@","];
    [NetworkTool requestMethod:NetworkMethodPOST urlString:@"/api/ysread/deleteAllRead" params:params success:^(id data) {
        if(successHandler){
            successHandler(data);
        }
    } error:^(NSError *error) {
        if(errorHandler){
            errorHandler(error);
        }
    } needToken:YES];
}

///上传单张图片
+(void)uploadImage:(UIImage *)image fileName:(NSString*)fileName name:(NSString *)name otherInfo:(NSMutableDictionary *)parameters success:(void (^)(NSURLSessionDataTask *task, id responseObject))success error:(void (^)(NSURLSessionDataTask *task, NSError *error))failure{
    
    [NetworkTool POST:@"/api/content/imagesUpload" parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //对图片进行0.5倍的压缩
        NSData *imageData =UIImageJPEGRepresentation(image,0.5);
        //对图片的大小进行裁剪(需要的时候开启)
//        NSData *imageData2 = UIImageJPEGRepresentation([image scaleImageToSize:CGSizeMake(150, 150)], 0.5);
        [formData appendPartWithFileData:imageData
                                    name:name
                                fileName:fileName
                                mimeType:@"image/jpeg"];
    } progress:nil success:success failure:failure];
}

///上传多张图片
+(void)uploadImages:(NSArray *)images fileNames:(NSArray*)fileNames names:(NSArray *)names otherInfo:(NSMutableDictionary *)parameters success:(void (^)(NSURLSessionDataTask *task, id responseObject))success error:(void (^)(NSURLSessionDataTask *task, NSError *error))failure{
    [NetworkTool POST:@"/api/content/imagesUpload" parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        for(int i=0;i<images.count;i++){
            //对图片进行0.5倍的压缩
            NSData *imageData =UIImageJPEGRepresentation(images[i],0.5);
            //对图片的大小进行裁剪(需要的时候开启)
            //        NSData *imageData2 = UIImageJPEGRepresentation([image scaleImageToSize:CGSizeMake(150, 150)], 0.5);
            [formData appendPartWithFileData:imageData
                                        name:names[i]
                                    fileName:fileNames[i]
                                    mimeType:@"image/jpeg"];
        }
    } progress:nil success:success failure:failure];
}

//上传视频
+(void)uploadVideo:(NSString *)fileUrl fileName:(NSString*)fileName name:(NSString *)name otherInfo:(NSMutableDictionary *)parameters success:(void (^)(NSURLSessionDataTask *task, id responseObject))success error:(void (^)(NSURLSessionDataTask *task, NSError *error))failure{
    
    [NetworkTool POST:@"/api/content/videoUpload" parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //对图片进行0.5倍的压缩
        NSURL * url = [NSURL fileURLWithPath:fileUrl];
        NSData *videoData = [NSData dataWithContentsOfURL:url];
        [formData appendPartWithFileData:videoData name:@"file" fileName:@"video.mp4" mimeType:@"application/octet-stream"];
    } progress:nil success:success failure:failure];
}

//获取图片 视频列表
+(void)requestImageList:(NSString *)seriNo pageSize:(NSInteger)pageSize type:(NSInteger)type pageNum:(NSInteger)pageNum success:(SuccessHandler)successHandler error:(ErrorHandler)errorHandler{
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithDictionary:@{@"devSerial":seriNo,@"pageNum":@(pageNum),@"type":@(type)}];
    if(pageSize && pageSize>0){
        params[@"pageSize"] = @(pageSize);
    }
    [NetworkTool requestMethod:NetworkMethodPOST urlString:@"/api/content/getImagesUpload" params:params success:^(id data) {
        if(successHandler){
            successHandler(data);
        }
    } error:^(NSError *error) {
        if(errorHandler){
            errorHandler(error);
        }
    } needToken:YES];
}

//删除图片
+(void)deleteImage:(NSString *)seriNo ids:(NSArray *)ids success:(SuccessHandler)successHandler error:(ErrorHandler)errorHandler{
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithDictionary:@{@"devSerial":seriNo}];
    params[@"id"] = [ids componentsJoinedByString:@","];
    [NetworkTool requestMethod:NetworkMethodPOST urlString:@"/api/content/deleteImage" params:params success:^(id data) {
        if(successHandler){
            successHandler(data);
        }
    } error:^(NSError *error) {
        if(errorHandler){
            errorHandler(error);
        }
    } needToken:YES];
}

@end

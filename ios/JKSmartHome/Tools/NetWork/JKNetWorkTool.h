//
//  OINetWorkTool.h
//  OverseaInvestment
//
//  Created by mao zuo on 16/11/3.
//  Copyright © 2016年 B2C. All rights reserved.
//

#import "AFNetworking.h"

#define NetworkTool [JKNetWorkTool sharedNetworkTool]

//成功与失败的block
typedef void(^SuccessHandler)(id data);
typedef void(^ErrorHandler)(NSError *error);

typedef NS_ENUM(NSInteger, JKNetworkMethod) {
    NetworkMethodGET = 0,
    NetworkMethodPOST,
    NetworkMethodPUT,
};

@interface JKNetWorkTool : AFHTTPSessionManager

///网络工具类的单例
+ (instancetype)sharedNetworkTool;

// 网络请求
- (void)requestMethod:(JKNetworkMethod)method urlString:(NSString *)urlString params:(NSMutableDictionary *)params success:(SuccessHandler)successHandler error:(ErrorHandler)errorHandler needToken:(BOOL)needToken;


//上传文件
- (NSURLSessionDataTask *)POST:(NSString *)URLString
               parameters:(id)parameters
constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                 progress:(NSProgress * __autoreleasing *)uploadProgress
                  success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

@end

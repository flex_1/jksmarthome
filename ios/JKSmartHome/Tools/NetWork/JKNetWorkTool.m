//
//  OINetWorkTool.m
//  OverseaInvestment
//
//  Created by mao zuo on 16/11/3.
//  Copyright © 2016年 B2C. All rights reserved.
//

#import "JKNetWorkTool.h"
#import "SVProgressHUD.h"
#import "AppDelegate.h"


#define JKErrorDomainName  @"com.jkinvest.error"
#define KAccessTokenOfPrefrence @"KAccessTokenOfPrefrence"
#define kAppCurrentBaseUrl @"kAppCurrentBaseUrl"

#define baseUrlString [[NSUserDefaults standardUserDefaults] objectForKey:kAppCurrentBaseUrl]

@implementation JKNetWorkTool

///网络单例
+ (instancetype)sharedNetworkTool {
    static JKNetWorkTool *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *baseURL = [[NSURL alloc] initWithString:@""];
        instance = [[JKNetWorkTool alloc] initWithBaseURL:baseURL];

        //instance.requestSerializer = [AFHTTPRequestSerializer serializer];
        instance.requestSerializer = [AFJSONRequestSerializer serializer];
    
        
        //------------------设置请求Request-----------------
        //设置timeoutInterval时间长度
        [instance.requestSerializer willChangeValueForKey:@"timeoutInterval"];
        instance.requestSerializer.timeoutInterval = 10;
        [instance.requestSerializer didChangeValueForKey:@"timeoutInterval"];
        
        //设置请求头
//        [instance.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//        [instance.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        //--------------------设置响应Response----------------
        //申明返回的结果是json类型
        instance.responseSerializer = [AFJSONResponseSerializer serializer];
        //如果报接受类型不一致请替换一致text/html或别的
        instance.responseSerializer.acceptableContentTypes = [[NSSet alloc] initWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil, nil];
    });
    return instance;
}

#pragma mark - 最底层的网络请求方法
- (void)requestMethod:(JKNetworkMethod)method urlString:(NSString *)urlString params:(NSMutableDictionary *)params success:(SuccessHandler)successHandler error:(ErrorHandler)errorHandler needToken:(BOOL)needToken {
    
    // HTTP请求成功的Block
    void (^success)(NSURLSessionDataTask *task, id responseObject) = ^ (NSURLSessionDataTask *task, id responseObject){
        // responseObject 为字典或者为空，否则返回数据异常
        if ([responseObject isKindOfClass:[NSDictionary class]] && (responseObject != nil)) {
            NSLog(@"%@",responseObject);
            if([responseObject[@"code"] isEqual:@(0)]){
                if(successHandler){
                    successHandler(responseObject);
                }
            }else{
                //出现了业务逻辑的错误(NSLocalizedDescriptionKey键 用来描述错误的原因)
                //errors是个数组，错误提示放在[0]上
                NSString * errorInfo = responseObject[@"msg"];
                NSInteger errCode = [responseObject[@"code"] integerValue];
                NSError * error = [NSError errorWithDomain:JKErrorDomainName code:errCode userInfo:@{NSLocalizedDescriptionKey:errorInfo}];
                if(errorHandler){
                    errorHandler(error);
                }
            }
        }else{
            NSLog(@"未获取到数据");
            if (errorHandler) {
                NSError * error = [NSError errorWithDomain:JKErrorDomainName code:-1 userInfo:@{NSLocalizedDescriptionKey:@"未获取到数据"}];
                errorHandler(error);
            }
        }
    };
    
    // 网络错误的Block
    void(^error)(NSURLSessionDataTask *task, NSError *err) = ^(NSURLSessionDataTask *task, NSError *err) {
        //获取HTTP的错误码
        NSHTTPURLResponse *faileResponse = (NSHTTPURLResponse *)task.response;
        NSInteger statusCode = [faileResponse statusCode];
        NSLog(@"网络单例未能请求到数据，请求返回错误code：%zd",statusCode);
        [SVProgressHUD setMaximumDismissTimeInterval:1.5];
        [SVProgressHUD setMinimumDismissTimeInterval:1.5];
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
        [SVProgressHUD showErrorWithStatus:@"网络错误，请稍后重试"];
        if (errorHandler) {
            errorHandler(err);
        }
    };
    
    // 处理nil参数
    if (params == nil) {
        params = [NSMutableDictionary dictionary];
    }
    
    // 需不需要Token
    if(needToken){
        NSString * accessToken = [[NSUserDefaults standardUserDefaults]objectForKey:KAccessTokenOfPrefrence];
        params[@"token"] = accessToken;
    }
  
    //拼接URL
    NSString * baseUrl = baseUrlString;
    urlString = [baseUrl stringByAppendingString:urlString];
    
    //确定请求的方法POST PUT 和 GET
    switch (method) {
        case NetworkMethodGET:
            [self GET:urlString parameters:params success:success failure:error];
            //[self GET:urlString parameters:params progress:nil success:success failure:error];
            break;
        case NetworkMethodPOST:
            
            [self POST:urlString parameters:params success:success failure:error];
            // [self POST:urlString parameters:params headers:@{@"Accept":@"application/json"} progress:nil success:success failure:error];
            break;
        case NetworkMethodPUT:
            [self PUT:urlString parameters:params success:success failure:error];
        default:
            break;
    }
}

///上传文件图片
- (NSURLSessionDataTask *)POST:(NSString *)URLString
                   parameters:(id)parameters
    constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                     progress:(NSProgress * __autoreleasing *)uploadProgress
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure{
  
    // 处理nil参数
    if (parameters == nil) {
        parameters = [NSMutableDictionary dictionary];
    }
    
    // 需要Token
    NSString * accessToken = [[NSUserDefaults standardUserDefaults]objectForKey:KAccessTokenOfPrefrence];
    parameters[@"token"] = accessToken;
  
    //拼接URL
    NSString * baseUrl = [[NSUserDefaults standardUserDefaults] objectForKey:kAppCurrentBaseUrl];
    URLString = [baseUrl stringByAppendingString:URLString];
    
    NSError *serializationError = nil;
    NSMutableURLRequest *request = [self.requestSerializer multipartFormRequestWithMethod:@"POST" URLString:URLString parameters:parameters constructingBodyWithBlock:block error:&serializationError];
    
    if (serializationError) {
        if (failure) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu"
            dispatch_async(self.completionQueue ?: dispatch_get_main_queue(), ^{
                failure(nil, serializationError);
            });
#pragma clang diagnostic pop
        }
        return nil;
    }
    
    __block NSURLSessionDataTask *task = [self uploadTaskWithStreamedRequest:request progress:uploadProgress completionHandler:^(NSURLResponse * __unused response, id responseObject, NSError *error) {
        if (error) {
            if (failure) {
                failure(task, error);
            }
        } else {
            if (success) {
                success(task, responseObject);
            }
        }
    }];
    [task resume];
    return task;
}

@end

//
//  JKAPIRequest.h
//  videoTestOC
//
//  Created by jk_ios on 2020/4/15.
//  Copyright © 2020 jk_ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

//成功与失败的block
typedef void(^SuccessHandler)(id data);
typedef void(^ErrorHandler)(NSError *error);

@interface JKAPIRequest : NSObject

// 请求消息个数
+(void)requestUnredMessage:(NSString *)seriNo success:(SuccessHandler)successHandler error:(ErrorHandler)errorHandler;

// 请求消息列表
+(void)requestMessageList:(NSString *)seriNo page:(NSInteger)page success:(SuccessHandler)successHandler error:(ErrorHandler)errorHandler;

//消息标记已读
+(void)setAlarmStatus:(NSString *)seriNo alarmIds:(NSArray *)alarmIds success:(SuccessHandler)successHandler error:(ErrorHandler)errorHandler;

//删除消息
+(void)deleteMessage:(NSString *)seriNo alarmIds:(NSArray *)alarmIds success:(SuccessHandler)successHandler error:(ErrorHandler)errorHandler;

//上传图片
+(void)uploadImage:(UIImage *)image fileName:(NSString*)fileName name:(NSString *)name otherInfo:(NSMutableDictionary *)parameters success:(void (^)(NSURLSessionDataTask *task, id responseObject))success error:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

//上传视频
+(void)uploadVideo:(NSString *)fileUrl fileName:(NSString*)fileName name:(NSString *)name otherInfo:(NSMutableDictionary *)parameters success:(void (^)(NSURLSessionDataTask *task, id responseObject))success error:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

//获取图片视频列表
+(void)requestImageList:(NSString *)seriNo pageSize:(NSInteger)pageSize type:(NSInteger)type pageNum:(NSInteger)pageNum success:(SuccessHandler)successHandler error:(ErrorHandler)errorHandler;

//删除图片视频
+(void)deleteImage:(NSString *)seriNo ids:(NSArray *)ids success:(SuccessHandler)successHandler error:(ErrorHandler)errorHandler;

@end



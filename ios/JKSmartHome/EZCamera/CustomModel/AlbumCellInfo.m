//
//  AlbumCellInfo.m
//  videoTestOC
//
//  Created by jk_ios on 2020/4/21.
//  Copyright © 2020 jk_ios. All rights reserved.
//

#import "AlbumCellInfo.h"

@implementation AlbumCellInfo

+(instancetype)alumWithDict:(NSDictionary *)dict{
    AlbumCellInfo * model = [AlbumCellInfo new];
    // 1:相册  2:视频
    NSInteger albumType = [dict[@"type"] integerValue];
    
    model.albumId = dict[@"id"];
    model.deviceName = dict[@"deviceName"];
    model.albumImageurl = dict[@"img"];
    if(dict[@"createTime"]){
        NSTimeInterval time=[dict[@"createTime"] doubleValue]/1000;//传入的时间戳str如果是精确到毫秒的记得要/1000
        NSDate *creatTime=[NSDate dateWithTimeIntervalSince1970:time];
        model.albumCreatTime = creatTime;
    }
    model.size = [dict[@"size"] integerValue];
    model.albumType = albumType;
    model.videoUrl = dict[@"video"];
    model.duration = dict[@"duration"];
    
    return model;
}


//防止崩溃
-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}

@end

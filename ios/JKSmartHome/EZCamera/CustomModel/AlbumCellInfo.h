//
//  AlbumCellInfo.h
//  videoTestOC
//
//  Created by jk_ios on 2020/4/21.
//  Copyright © 2020 jk_ios. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AlbumCellInfo : NSObject

@property (nonatomic, copy) NSString * albumId;
@property (nonatomic, copy) NSString * albumImageurl;
@property (nonatomic, copy) NSDate * albumCreatTime;
@property (nonatomic) NSInteger size;
@property (nonatomic) NSInteger albumType;
@property (nonatomic, copy) NSString * deviceName;
@property (nonatomic, copy) NSString * videoUrl;
@property (nonatomic, copy) NSString * duration;

+(instancetype)alumWithDict:(NSDictionary *)dict;

@end


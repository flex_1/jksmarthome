//
//  CameraMainController.m
//  JKSmartHome
//
//  Created by jk_ios on 2019/12/24.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "CameraMainController.h"
#import "EZCameraInfo.h"
#import "EZOpenSDK.h"
#import "EZPlayer.h"
#import "AppDelegate.h"
#import <Photos/Photos.h>
#import "UIViewController+EZBackPop.h"
#import "DDKit.h"
#import "MBProgressHUD.h"
#import "GlobalKit.h"
#import "MWPhotoBrowser.h"
#import "JKAPIRequest.h"
#import "JKNetWorkTool.h"
#import "EZMessageListViewController.h"
#import "UIImageView+WebCache.h"
#import "AlbumListController.h"
#import "ZFNoramlViewController.h"
#import "AppDelegate.h"
#import "Masonry.h"
#import "UIColor+DDKit.h"

#define kAPPDelegate ((AppDelegate*)[[UIApplication sharedApplication] delegate])
#define kDeleteAlubumNotification @"kDeleteAlubumNotification"

@interface CameraMainController ()<EZPlayerDelegate,MWPhotoBrowserDelegate>

@property (nonatomic, strong) EZPlayer *player;
@property (nonatomic, strong) EZPlayer *talkPlayer;
@property (nonatomic) Boolean isPlaying;
@property (nonatomic) int articulationLevel;
@property (nonatomic) BOOL isOpenSound;
@property (nonatomic) BOOL isFullScreen;
@property (nonatomic) BOOL isTalking;
@property (nonatomic) BOOL isRecordVideo;
@property (nonatomic) BOOL isOnWarrning;
@property (nonatomic, weak) MBProgressHUD *voiceHud;
@property (nonatomic, copy) NSString *filePath;
@property (nonatomic, strong) NSTimer *recordTimer;
@property (nonatomic) NSTimeInterval seconds;
@property (nonatomic, strong)UIScrollView *contentScrollView;
@property (nonatomic, strong)NSMutableArray * imagesArr;
@property (nonatomic, strong)NSMutableArray * photos;
@property (nonatomic) CGFloat rotationBottomMagrin;


@property (nonatomic, strong)NSMutableArray * photoList;
@property (nonatomic, strong)NSMutableArray * videoList;
@property (nonatomic) NSInteger albumType;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vedioAspect;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomMargin;


@property (weak, nonatomic) IBOutlet UIView *bottomLoadingView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *notiRightItem;
@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;
@property (weak, nonatomic) IBOutlet UIView *playerWrapper;
@property (weak, nonatomic) IBOutlet UIButton *cameraBtnUp;
@property (weak, nonatomic) IBOutlet UIButton *cameraBtnRight;
@property (weak, nonatomic) IBOutlet UIButton *cameraBtnDown;
@property (weak, nonatomic) IBOutlet UIButton *cameraBtnLeft;
@property (weak, nonatomic) IBOutlet UIImageView *playImageView;
@property (weak, nonatomic) IBOutlet UIImageView *voiceImageView;
@property (weak, nonatomic) IBOutlet UILabel *hdLabel;
@property (weak, nonatomic) IBOutlet UIImageView *rotationImageView;
@property (weak, nonatomic) IBOutlet UIButton *rotationBtn;

@property (weak, nonatomic) IBOutlet UIImageView *talkImg;
@property (weak, nonatomic) IBOutlet UIImageView *videoImg;
@property (weak, nonatomic) IBOutlet UIImageView *cameraImg;
@property (weak, nonatomic) IBOutlet UIImageView *warningImg;
@property (weak, nonatomic) IBOutlet UIButton *talkBtn;
@property (weak, nonatomic) IBOutlet UIButton *videoBtn;
@property (weak, nonatomic) IBOutlet UIButton *cameraBtn;
@property (weak, nonatomic) IBOutlet UIButton *warningBtn;


@property (weak, nonatomic) IBOutlet UILabel *recordLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomContentView;
@property (weak, nonatomic) IBOutlet UIButton *fullScreenBackBtn;
@property (weak, nonatomic) IBOutlet UIButton *fullScreenRotationBtn;

@property (weak, nonatomic) IBOutlet UIButton *cloudAlbum;
@property (weak, nonatomic) IBOutlet UIButton *cloudVideo;

@end

@implementation CameraMainController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initPlayer];
    [self initUI];
    
    // 适配 ios 15.0 导航栏问题
    if (@available(iOS 15.0, *)) {
        UINavigationBarAppearance *appearance = [[UINavigationBarAppearance alloc] init];
        [appearance configureWithOpaqueBackground];
      
        // appearance.backgroundColor = UIColor.whiteColor;
        //设置导航条标题颜色
        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
        [attributes setValue:UIColor.blackColor forKey:NSForegroundColorAttributeName];
        appearance.titleTextAttributes = attributes;
   
        self.navigationController.navigationBar.standardAppearance = appearance;
        self.navigationController.navigationBar.scrollEdgeAppearance = appearance;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(_player && !self.isPlaying){
        self.isPlaying = YES;
        self.loadingLabel.text = @"加载中...";
        [_player startRealPlay];
    }
    kAPPDelegate.allowOrentitaionRotation = YES;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self checkUnredMessage];
}

- (void)viewWillDisappear:(BOOL)animated {
    if(_player){
        self.isPlaying = NO;
        [_player stopRealPlay];
    }
    if (_talkPlayer){
        self.isTalking = NO;
        [_talkPlayer stopVoiceTalk];
    }
    //结束本地录像
    if(self.isRecordVideo){
        [_player stopLocalRecordExt:^(BOOL ret) {
            [self.recordTimer invalidate];
            self.recordTimer = nil;
            self.isRecordVideo = NO;
            self.filePath = nil;
        }];
    }
    kAPPDelegate.allowOrentitaionRotation = NO;
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

-(void)initPlayer{
    self.recordLabel.hidden = YES;
    self.fullScreenBackBtn.hidden = YES;
    self.fullScreenRotationBtn.hidden = YES;
    
    [EZOpenSDK initLibWithAppKey:_appKey];
    [EZOpenSDK setAccessToken:_accessToken];
        
    //正在播放
    self.isPlaying = YES;
    //高清
    self.articulationLevel = 2;
    //声音
    self.isOpenSound = YES;
    //是否横屏
    self.isFullScreen = NO;
    //是否开启对讲
    self.isTalking = NO;
    //是否录屏
    self.isRecordVideo = NO;
    
     _player = [EZOpenSDK createPlayerWithDeviceSerial:_serialStr cameraNo:_cameraNo];
    
    _player.delegate = self;
    [_player openSound];
    _player.backgroundModeByPlayer = NO;
    [_player setPlayerView:self.playerWrapper];
  
    _talkPlayer = [EZOpenSDK createPlayerWithDeviceSerial:_serialStr cameraNo:_cameraNo];
    _talkPlayer.delegate = self;
    
    [EZOpenSDK getDeviceInfo:self.serialStr completion:^(EZDeviceInfo *deviceInfo, NSError *error) {
        self.deviceInfo = deviceInfo;
        self.isOnWarrning = deviceInfo.defence ? YES : NO;
        if(deviceInfo.isSupportPTZ){
            self.cameraBtnUp.hidden = NO;
            self.cameraBtnLeft.hidden = NO;
            self.cameraBtnDown.hidden = NO;
            self.cameraBtnRight.hidden = NO;
        }
    }];
  
    [self.player startRealPlay];
    [self refreshImageVideoList];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshImageVideoList) name:kDeleteAlubumNotification object:nil];
}

#pragma mark - UI
-(void)initUI{
    [self configRotationUI];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    self.title = self.cameraName;
    
    //1:云相册 2:云视频
    self.albumType = 1;
  
    // 设置statusBar 颜色
    if (@available(iOS 13.0, *)) {
        if(self.isDark){
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDarkContent;
        }
    } else {
      // Fallback on earlier versions
    }
}

-(void)configRotationUI{
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    if([self isPhoneX]){
        height = height-44-44;
    }
    CGFloat adapt = height * 9/16 - width;
    self.rotationBottomMagrin = adapt * 16/9;
}

-(void)setBottomImageList{
    [self.contentScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.bottomContentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if(!self.contentScrollView){
        self.contentScrollView = [[UIScrollView alloc]init];
        self.contentScrollView.frame = self.bottomContentView.bounds;
        self.contentScrollView.contentSize = self.bottomContentView.bounds.size;
        self.contentScrollView.contentInset=UIEdgeInsetsMake(10,0,10,0);
    }
    
    [self.bottomContentView addSubview:self.contentScrollView];
    [self.bottomContentView setClipsToBounds:YES];

    NSMutableArray * list = [NSMutableArray new];
    if(self.albumType == 1){
        list = self.photoList;
    }else{
        list = self.videoList;
    }
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    CGFloat imageW = (screenW - 20*2 - 10*2)/3;
    CGFloat imageH = imageW *3/4;
    NSInteger count = list.count;
    if(count > 3){
        count = 3;
    }
    self.photos = [NSMutableArray new];
    for (int i=0; i<count; i++) {
        NSString * pic = list[i][@"img"];
        NSURL * picUrl = [NSURL URLWithString:pic];
        [self.photos addObject:[MWPhoto photoWithURL:picUrl]];
        UIImageView * imgView = [[UIImageView alloc] init];
        [imgView setBackgroundColor:[UIColor dd_hexStringToColor:@"d9d9d9"]];
        imgView.tag = i;
        imgView.userInteractionEnabled = YES;
        CGFloat x = 20 + (imageW+10) * (i%3);
        CGFloat y = 10 + (imageH+10) * (i/3);
        imgView.frame = CGRectMake(x, y, imageW, imageH);
        [imgView sd_setImageWithURL:picUrl];
        UITapGestureRecognizer * tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapAction:)];
        [imgView addGestureRecognizer:tapGes];
        [self.contentScrollView addSubview:imgView];
    }
    
    CGFloat contentH = (imageH + 10) * (count/3 + 1) + 20;
    self.contentScrollView.frame = self.bottomContentView.bounds;
    self.contentScrollView.contentSize = CGSizeMake(screenW, contentH);
}

-(void)setBottomEmptyView:(NSString *)text imageName:(NSString *)imageName{
    [self.bottomContentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    UIImageView * imageView = [UIImageView new];
    imageView.image = [UIImage imageNamed:imageName];
    [self.bottomContentView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.bottomContentView);
        make.centerY.equalTo(self.bottomContentView).offset(-30);
    }];
    
    UILabel * label = [UILabel new];
    [label setFont: [UIFont systemFontOfSize:12]];
    [label setText:text];
    [label setTextColor:[UIColor lightGrayColor]];
    [self.bottomContentView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.bottomContentView);
        make.top.mas_equalTo(imageView.mas_bottom);
    }];
}

-(void)setBottomErrorView{
    [self.bottomContentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    UIButton * errorBtn = [UIButton new];
    [errorBtn setTitle:@"加载失败，点击重试" forState:UIControlStateNormal];
    [errorBtn setTitleColor:[UIColor systemBlueColor] forState:UIControlStateNormal];
    [errorBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [errorBtn addTarget:self action:@selector(refreshImageVideoList) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomContentView addSubview:errorBtn];
    [errorBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.bottomContentView);
        make.top.mas_equalTo(self.bottomContentView).offset(50);
    }];
}

-(void)showLoadingView{
    [self.bottomContentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
  
    [self.bottomContentView addSubview:self.bottomLoadingView];
    [self.bottomLoadingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.bottomContentView);
        make.top.mas_equalTo(self.bottomContentView).offset(50);
    }];
}

#pragma mark - Network Request
-(void)checkUnredMessage{
    [JKAPIRequest requestUnredMessage:self.serialStr success:^(id data) {
        UIButton*rightButton = [[UIButton alloc]initWithFrame:CGRectMake(0,0,30,30)];
        [rightButton setImage:[UIImage imageNamed:@"warnings_badge"] forState:UIControlStateNormal];
        [rightButton addTarget:self action:@selector(warningBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        if(data[@"result"] && [data[@"result"] intValue] >0){
            [rightButton setImage:[UIImage imageNamed:@"warnings_badge"] forState:UIControlStateNormal];
        }else{
            [rightButton setImage:[UIImage imageNamed:@"warnings"] forState:UIControlStateNormal];
        }
        UIBarButtonItem*rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
        self.navigationItem.rightBarButtonItem= rightItem;
    } error:^(NSError *error) {
        
    }];
}

-(void)uploadImage:(UIImage *)img{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"正在将截图上传云端...";
    NSMutableDictionary * params = [NSMutableDictionary new];
    params[@"devSerial"] = self.serialStr;
    [JKAPIRequest uploadImage:img fileName:@"cutImage.jpg" name:@"file" otherInfo:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [hud hide:YES];
        [UIView dd_showMessage:@"图片上传成功"];
        [self refreshImageVideoList];
    } error:^(NSURLSessionDataTask *task, NSError *error) {
        [hud hide:YES];
        [UIView dd_showMessage:@"图片上传失败,请稍后重试"];
    }];
}

-(void)uploadVideo:(NSString *)url{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"正在将视频上传云端...";
    NSMutableDictionary * params = [NSMutableDictionary new];
    params[@"devSerial"] = self.serialStr;
    [JKAPIRequest uploadVideo:url fileName:@"cutVideo.mp4" name:@"file" otherInfo:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [hud hide:YES];
        [UIView dd_showMessage:@"视频上传成功"];
        [self refreshImageVideoList];
    } error:^(NSURLSessionDataTask *task, NSError *error) {
        [hud hide:YES];
        [UIView dd_showMessage:@"视频上传失败,请稍后重试"];
    }];
}

-(void)requestImageList{
    [self showLoadingView];
    self.photoList = [NSMutableArray new];
    [JKAPIRequest requestImageList:self.serialStr pageSize:3 type:1 pageNum:1 success:^(id data) {
        if(data[@"result"]){
            self.photoList = data[@"result"][@"list"];
        }
        if(self.photoList.count > 0){
            [self setBottomImageList];
        }else{
            [self setBottomEmptyView:@"暂无图片" imageName:@"noPhoto"];
        }
    } error:^(NSError *error) {
        [self setBottomErrorView];
    }];
}

-(void)requestVideoList{
    [self showLoadingView];
    self.videoList = [NSMutableArray new];
    [JKAPIRequest requestImageList:self.serialStr pageSize:3 type:2 pageNum:1 success:^(id data) {
        if(data[@"result"]){
            self.videoList = data[@"result"][@"list"];
        }
        if(self.videoList.count > 0){
            [self setBottomImageList];
        }else{
            [self setBottomEmptyView:@"暂无视频" imageName:@"noVideo"];
        }
    } error:^(NSError *error) {
        [self setBottomErrorView];
    }];
}

-(void)refreshImageVideoList{
    if(self.albumType == 2){
        [self requestVideoList];
        return;
    }
    [self requestImageList];
}


#pragma mark - Private
//可以使用一下语句判断是否是刘海手机：
- (BOOL)isPhoneX {
   BOOL iPhoneX = NO;
     if (UIDevice.currentDevice.userInterfaceIdiom != UIUserInterfaceIdiomPhone) {//判断是否是手机
           return iPhoneX;
       }
       if (@available(iOS 11.0, *)) {
           UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
           if (mainWindow.safeAreaInsets.bottom > 0.0) {
               iPhoneX = YES;
           }
       }
    return iPhoneX;
}
//进入全屏
-(void)goToFullScreen{
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}
//退出全屏
-(void)quitFullScreen{
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}
// APP即将 resignActive
-(void)resignActive{
    [self quitFullScreen];
    
}
// 保存到相册
- (void)saveImageToPhotosAlbum:(UIImage *)savedImage{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusNotDetermined){
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if(status == PHAuthorizationStatusAuthorized){
                UIImageWriteToSavedPhotosAlbum(savedImage, self, @selector(imageSavedToPhotosAlbum: didFinishSavingWithError:contextInfo:), NULL);
            }
        }];
    }else{
        if (status == PHAuthorizationStatusAuthorized){
            UIImageWriteToSavedPhotosAlbum(savedImage, self, @selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:), NULL);
        }
    }
}

- (void)saveRecordToPhotosAlbum{
    //开始本地录像
    NSString *path = @"/OpenSDK/EzvizLocalRecord";
    NSArray * docdirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * docdir = [docdirs objectAtIndex:0];
    
    NSString * configFilePath = [docdir stringByAppendingPathComponent:path];
    if(![[NSFileManager defaultManager] fileExistsAtPath:configFilePath]){
        NSError *error = nil;
        [[NSFileManager defaultManager] createDirectoryAtPath:configFilePath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:&error];
    }
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    dateformatter.dateFormat = @"yyyyMMddHHmmssSSS";
    _filePath = [NSString stringWithFormat:@"%@/%@.mp4",configFilePath,[dateformatter stringFromDate:[NSDate date]]];
    self.recordLabel.text = @"●REC 00:00";
    
    if (!self.recordTimer){
        self.recordTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerStart:) userInfo:nil repeats:YES];
    }
    [_player startLocalRecordWithPathExt:_filePath];
    _seconds = 0;
}

// 指定回调方法
- (void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    NSString *message = nil;
    
    if (!error) {
        if([image isKindOfClass:[NSString class]]){
            message = @"录像已保存至系统相册";
        }else{
            message = @"截图已保存至系统相册";
        }
    }else{
        message = [error description];
    }
    [UIView dd_showMessage:message];
}
// 检查授权
- (void) checkMicPermissionResult:(void(^)(BOOL enable)) retCb{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    
    switch (authStatus){
        case AVAuthorizationStatusNotDetermined://未决
        {
            AVAudioSession *avSession = [AVAudioSession sharedInstance];
            [avSession performSelector:@selector(requestRecordPermission:) withObject:^(BOOL granted) {
                if (granted){
                    if (retCb){
                        retCb(YES);
                    }
                }else {
                    if (retCb){
                        retCb(NO);
                    }
                }
            }];
        }
            break;
            
        case AVAuthorizationStatusRestricted://未授权，家长限制
        case AVAuthorizationStatusDenied://未授权
            if (retCb){
                retCb(NO);
            }
            break;
            
        case AVAuthorizationStatusAuthorized://已授权
            if (retCb){
                retCb(YES);
            }
            break;
            
        default:
            if (retCb){
                retCb(NO);
            }
            break;
    }
}
// 检查 视频 授权
- (void)chekVideoPermissionResult:(void(^)(BOOL enable)) retCb{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusNotDetermined){
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if(status == PHAuthorizationStatusAuthorized){
                retCb(YES);
            }else{
                retCb(NO);
            }
        }];
    }else{
        if (status == PHAuthorizationStatusAuthorized){
            retCb(YES);
        }else{
            retCb(NO);
        }
    }
}

- (void)timerStart:(NSTimer *)timer{
    NSInteger currentTime = ++_seconds;
    self.recordLabel.text = [NSString stringWithFormat:@"REC %02d:%02d", (int)currentTime/60, (int)currentTime % 60];
    if(currentTime % 2 == 0){
        self.recordLabel.text = [NSString stringWithFormat:@"●REC %02d:%02d", (int)currentTime/60, (int)currentTime % 60];
    }
}

-(void)singleTapAction:(id)sender{
    UITapGestureRecognizer *tap = (UITapGestureRecognizer *)sender;
    UIView *view = (UIView *)tap.view;
    NSInteger index = view.tag;
    
    //播放视频
    if(self.albumType == 2){
        self.isPlaying = NO;
        [self.player stopRealPlay];
        
        NSDictionary * video = self.videoList[index];
        NSString * videoUrl = video[@"video"];
        ZFNoramlViewController * playerVC = [ZFNoramlViewController new];
        playerVC.videoUrl = videoUrl;
        playerVC.kVideoCover = video[@"img"];
//        playerVC.videoUrl = @"http://vfx.mtime.cn/Video/2019/02/04/mp4/190204084208765161.mp4";
        
        [self.navigationController pushViewController:playerVC animated:YES];
    
        return;
    }

    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = NO;
    browser.displayNavArrows = YES;
    browser.displaySelectionButtons = NO;
    browser.zoomPhotosToFill = YES;
    browser.alwaysShowControls = YES;
    browser.enableGrid = YES;
    browser.startOnGrid = NO;
    // Present
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:browser];
    if (@available(iOS 15.0, *)) {
        UINavigationBarAppearance *appearance = [[UINavigationBarAppearance alloc] init];
        [appearance configureWithTransparentBackground];
        //设置导航条标题颜色
        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
        [attributes setValue:UIColor.whiteColor forKey:NSForegroundColorAttributeName];
        appearance.titleTextAttributes = attributes;
        nav.navigationBar.standardAppearance = appearance;
        nav.navigationBar.scrollEdgeAppearance = appearance;
    }else{
        NSDictionary *dict = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
        [nav.navigationBar setTitleTextAttributes:dict];
    }
    [self.navigationController presentViewController:nav animated:YES completion:nil];
    
    // Manipulate
    [browser showNextPhotoAnimated:YES];
    [browser showPreviousPhotoAnimated:YES];
    [browser setCurrentPhotoIndex:index];
}

#pragma mark - Button
- (IBAction)playBtnClcik:(id)sender {
    if(self.isPlaying){
        [self.player stopRealPlay];
    }else{
        self.loadingLabel.text = @"加载中...";
        [self.player startRealPlay];
    }
    self.isPlaying = !self.isPlaying;
}
// 声音
- (IBAction)voiceBtnClick:(id)sender {
    if (_isOpenSound){
        [_player closeSound];
        self.voiceImageView.image = [UIImage imageNamed:@"camera_novoice"];
    }else{
        [_player openSound];
        self.voiceImageView.image = [UIImage imageNamed:@"camera_voice"];
    }
    _isOpenSound = !_isOpenSound;
}
//高清
- (IBAction)hdBtnClick:(id)sender {
    EZVideoLevelType type = EZVideoLevelLow;
    int target = self.articulationLevel + 1;
    if(target%3 == 0){
        type = EZVideoLevelLow;
    }else if(target%3 == 1){
        type = EZVideoLevelMiddle;
    }else{
        type = EZVideoLevelHigh;
    }
    self.loadingLabel.text = @"加载中...";
    self.loadingLabel.hidden = NO;
    [EZOpenSDK setVideoLevel:_serialStr cameraNo:_cameraNo videoLevel:type completion:^(NSError *error) {
        if (error){
            return;
        }
        self.articulationLevel = target;
        [self.player stopRealPlay];
        [self.player startRealPlay];
     }];
}
//转屏
- (IBAction)rotationBtnClick:(id)sender {
    if(self.isFullScreen){
        [self quitFullScreen];
    }else{
        [self goToFullScreen];
    }
}
// 旋转
- (IBAction)directionBtnTouchDown:(id)sender {
    EZPTZCommand command;
    if(sender == self.cameraBtnUp){
        command = EZPTZCommandUp;
    }else if(sender == self.cameraBtnLeft){
        command = EZPTZCommandLeft;
    }else if(sender == self.cameraBtnDown){
        command = EZPTZCommandDown;
    }else{
        command = EZPTZCommandRight;
    }
    [EZOpenSDK controlPTZ:_serialStr cameraNo:_cameraNo command:command action:EZPTZActionStart speed:2 result:^(NSError *error) {
            NSLog(@"error is %@", error);
    }];
}

- (IBAction)directionBtnTouchUp:(id)sender {
    EZPTZCommand command;
    if(sender == self.cameraBtnUp){
        command = EZPTZCommandUp;
    }else if(sender == self.cameraBtnLeft){
        command = EZPTZCommandLeft;
    }else if(sender == self.cameraBtnDown){
        command = EZPTZCommandDown;
    }else{
        command = EZPTZCommandRight;
    }
    [EZOpenSDK controlPTZ:_serialStr cameraNo:_cameraNo command:command action:EZPTZActionStop speed:3.0 result:^(NSError *error) {
        NSLog(@"error is %@", error);
    }];
}
// 关闭
- (IBAction)close:(id)sender {
    if (@available(iOS 13.0, *)) {
        if(self.isDark){
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        }
    } else {
      // Fallback on earlier versions
    }
    [[UIApplication sharedApplication].windows[0].rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)recordVideo:(id)sender {
    //结束本地录像
    if(self.isRecordVideo){
        [_player stopLocalRecordExt:^(BOOL ret) {
            NSLog(@"%d", ret);
            self.isRecordVideo = NO;
            [self.recordTimer invalidate];
            self.recordTimer = nil;
//            UISaveVideoAtPathToSavedPhotosAlbum(self.filePath, self, @selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:), NULL);
            [self uploadVideo:self.filePath.copy];
            self.filePath = nil;
        }];
    }else{
        [self saveRecordToPhotosAlbum];
        self.isRecordVideo = YES;
        
//        __weak CameraMainController *weakSelf = self;
//        [self chekVideoPermissionResult:^(BOOL enable) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                if(enable){
//                    [weakSelf saveRecordToPhotosAlbum];
//                    self.isRecordVideo = YES;
//                }else{
//                    [UIView dd_showDetailMessage:@"未开启相册权限"];
//                    self.isRecordVideo = NO;
//                }
//            });
//        }];
    }
}

- (IBAction)recordPicture:(id)sender {
    UIImage *image = [_player capturePicture:100];
//    [self saveImageToPhotosAlbum:image];
//    [self addCutImageToBottomContent:image];
    
    [self uploadImage:image];
}

- (IBAction)openTalk:(id)sender {
    if(!self.isTalking){
        __weak CameraMainController *weakSelf = self;
        [self checkMicPermissionResult:^(BOOL enable) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (enable){
                    if (!weakSelf.voiceHud) {
                        weakSelf.voiceHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    }
                    weakSelf.isTalking = YES;
                    weakSelf.voiceHud.labelText = @"正在开启对讲，请稍候...";
                    [weakSelf.talkPlayer startVoiceTalk];
                }else{
                    [UIView dd_showDetailMessage:@"未开启麦克风权限"];
                }
            });
        }];
    }else{
        self.isTalking = NO;
        [self.talkPlayer stopVoiceTalk];
    }
}

//布防 撤防
- (IBAction)setWarning:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if(self.isOnWarrning){
        [EZOpenSDK setDefence:EZDefenceStatusOffOrSleep deviceSerial:self.serialStr completion:^(NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            self.isOnWarrning = NO;
            [UIView dd_showMessage:@"已撤防"];
        }];
    }else{
        [EZOpenSDK setDefence:EZDefenceStatusOn deviceSerial:self.serialStr completion:^(NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            self.isOnWarrning = YES;
            [UIView dd_showMessage:@"开启布防"];
        }];
    }
}

- (IBAction)selectAlbumAndVideo:(UIButton *)sender {
    if(sender.tag == 0){
        if(self.albumType == 1){
            return;
        }
        [self.contentScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        self.albumType = 1;
        self.videoList = nil;
    }else{
        if(self.albumType == 2){
            return;
        }
        [self.contentScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        self.albumType = 2;
        self.photoList = nil;
    }
    [self refreshImageVideoList];
}

///消息按钮
- (IBAction)warningBtnClick:(id)sender {
    [self performSegueWithIdentifier:@"go2MessageList" sender:self.serialStr];
}

///点击更多
- (IBAction)moreAlbum:(id)sender {
    [self performSegueWithIdentifier:@"go2Albums" sender:self.serialStr];
}

#pragma mark - Delegate
- (void)player:(EZPlayer *)player didPlayFailed:(NSError *)error{
    NSLog(@"player: %@, didPlayFailed: %@", player, error);
    //如果是需要验证码或者是验证码错误
    if (error.code == EZ_SDK_NEED_VALIDATECODE) {
            
        return;
    } else if (error.code == EZ_SDK_VALIDATECODE_NOT_MATCH) {
            
        return;
    } else if (error.code == EZ_SDK_NOT_SUPPORT_TALK) {
        [UIView dd_showDetailMessage:@"设备不支持对讲"];
        self.talkBtn.enabled = NO;
        [self.voiceHud hide:YES];
        return;
    }else{
        if ([player isEqual:_player]){
            [_player stopRealPlay];
            self.isPlaying = NO;
        }
        else{
            [_talkPlayer stopVoiceTalk];
            self.isTalking = NO;
        }
    }
    [UIView dd_showDetailMessage:error.localizedDescription];
    [self.voiceHud hide:YES];
}

- (void)player:(EZPlayer *)player didReceivedMessage:(NSInteger)messageCode{
    if (messageCode == PLAYER_REALPLAY_START){
        self.isPlaying = YES;
        self.cameraBtn.enabled = YES;
        self.videoBtn.enabled = YES;
        self.loadingLabel.hidden = YES;
        self.rotationBtn.enabled = YES;
        self.cameraImg.image = [UIImage imageNamed:@"camera"];
        self.videoImg.image = [UIImage imageNamed:@"camera_video"];
    }else if(messageCode == PLAYER_VOICE_TALK_START){
        //对讲开始 关闭声音
        [self.voiceHud hide:YES];
        [_player closeSound];
        self.isOpenSound = NO;
    }else if (messageCode == PLAYER_VOICE_TALK_END){
        //对讲结束 开启声音
        [_player openSound];
        self.isOpenSound = YES;
    }else if(messageCode == PLAYER_VIDEOLEVEL_CHANGE){
        self.loadingLabel.hidden = NO;
    }else if(messageCode == PLAYER_STREAM_RECONNECT){
        self.loadingLabel.text = @"加载中...";
        self.loadingLabel.hidden = NO;
    }
}

- (void)player:(EZPlayer *)player didReceivedDisplayHeight:(NSInteger)height displayWidth:(NSInteger)width{
    [self.playerWrapper bringSubviewToFront:self.cameraBtnUp];
    [self.playerWrapper bringSubviewToFront:self.cameraBtnRight];
    [self.playerWrapper bringSubviewToFront:self.cameraBtnDown];
    [self.playerWrapper bringSubviewToFront:self.cameraBtnLeft];
    [self.playerWrapper bringSubviewToFront:self.recordLabel];
    [self.playerWrapper bringSubviewToFront:self.fullScreenBackBtn];
    [self.playerWrapper bringSubviewToFront:self.fullScreenRotationBtn];
    
}

// 查看图片 Delegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser{
    return self.photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index{
    if (index < self.photos.count) {
        return [self.photos objectAtIndex:index];
    }
    return nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue destinationViewController] isKindOfClass:[EZMessageListViewController class]]) {
        ((EZMessageListViewController *)[segue destinationViewController]).deviceSerial = self.serialStr;
    }else if([[segue destinationViewController] isKindOfClass:[AlbumListController class]]){
        ((AlbumListController *)[segue destinationViewController]).deviceSerial = self.serialStr;
        ((AlbumListController *)[segue destinationViewController]).albumType = self.albumType ;
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
        toInterfaceOrientation == UIInterfaceOrientationLandscapeRight){
        self.bottomMargin.constant = 1000;
        self.navigationController.navigationBarHidden = YES;
        self.vedioAspect.constant = self.rotationBottomMagrin;
        self.fullScreenBackBtn.hidden = NO;
        self.fullScreenRotationBtn.hidden = NO;
        self.isFullScreen = YES;
        [self.view setBackgroundColor:[UIColor blackColor]];
        
    }else{
        self.bottomMargin.constant = 0;
        self.navigationController.navigationBarHidden = NO;
        self.vedioAspect.constant = 0;
        self.fullScreenBackBtn.hidden = YES;
        self.fullScreenRotationBtn.hidden = YES;
        self.isFullScreen = NO;
        [self.view setBackgroundColor:[UIColor whiteColor]];
    }
}

#pragma mark - Getting Setting

- (void)setIsPlaying:(Boolean)isPlaying{
    _isPlaying = isPlaying;
    if(isPlaying){
        self.playImageView.image = [UIImage imageNamed:@"camera_pause"];
    }else{
        self.playImageView.image = [UIImage imageNamed:@"camera_play"];
        self.loadingLabel.text = @"已暂停";
        self.loadingLabel.hidden = NO;
        self.cameraBtn.enabled = NO;
        self.cameraImg.image = [UIImage imageNamed:@"camera_close"];
        self.videoBtn.enabled = NO;
        self.videoImg.image = [UIImage imageNamed:@"camera_video_close"];
    }
}

-(void)setArticulationLevel:(int)articulationLevel{
    // 0-流畅，1-均衡，2-高清，3-超清
    _articulationLevel = articulationLevel;
    if(articulationLevel%3 == 0){
        self.hdLabel.text = @"流畅";
    }else if(articulationLevel%3 == 1){
        self.hdLabel.text = @"均衡";
    }else{
        self.hdLabel.text = @"高清";
    }
}
//声音
-(void)setIsOpenSound:(BOOL)isOpenSound{
    _isOpenSound = isOpenSound;
    if(isOpenSound){
        self.voiceImageView.image = [UIImage imageNamed:@"camera_voice"];
    }else{
        self.voiceImageView.image = [UIImage imageNamed:@"camera_novoice"];
    }
}
//录像
-(void)setIsRecordVideo:(BOOL)isRecordVideo{
    _isRecordVideo = isRecordVideo;
    if(isRecordVideo){
        self.recordLabel.hidden = NO;
        // self.videoImg.image = [UIImage imageNamed:@"camera_video"];
        
    }else{
        self.recordLabel.hidden = YES;
        // self.videoImg.image = [UIImage imageNamed:@"camera_video_close"];
    }
}
//对讲
-(void)setIsTalking:(BOOL)isTalking{
    _isTalking = isTalking;
    if(isTalking){
        self.talkImg.image = [UIImage imageNamed:@"camera_talk"];
    }else{
        self.talkImg.image = [UIImage imageNamed:@"camera_talk_close"];
    }
}
//布防 撤防
-(void)setIsOnWarrning:(BOOL)isOnWarrning{
    _isOnWarrning = isOnWarrning;
    if(isOnWarrning){
        self.warningImg.image = [UIImage imageNamed:@"camera_warning"];
    }else{
        self.warningImg.image = [UIImage imageNamed:@"camera_no_warning"];
    }
}
// 当前是云相册 还是 云视频
- (void)setAlbumType:(NSInteger)albumType{
    _albumType = albumType;
    if(albumType == 1){
        [self.cloudAlbum setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.cloudAlbum.titleLabel.font = [UIFont systemFontOfSize:18.0 weight:UIFontWeightBold];
        [self.cloudVideo setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        self.cloudVideo.titleLabel.font = [UIFont systemFontOfSize:16.0 weight:UIFontWeightRegular];
    }else{
        [self.cloudVideo setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.cloudVideo.titleLabel.font = [UIFont systemFontOfSize:18.0 weight:UIFontWeightBold];
        [self.cloudAlbum setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        self.cloudAlbum.titleLabel.font = [UIFont systemFontOfSize:16.0 weight:UIFontWeightRegular];
    }
}

#pragma mark - System
-(void)dealloc{
    [EZOpenSDK releasePlayer:_player];
    [EZOpenSDK releasePlayer:_talkPlayer];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDeleteAlubumNotification object:nil];
}

@end

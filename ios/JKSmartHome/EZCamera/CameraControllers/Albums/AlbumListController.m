
#import "AlbumListController.h"

#import "MJRefresh.h"
#import "DDKit.h"
#import "AlbumListCell.h"
#import "Masonry.h"
#import "MBProgressHUD.h"
#import "EZMessagePhotoViewController.h"
#import "NSDate-Utilities.h"
#import "JKAPIRequest.h"
#import <Photos/Photos.h>
#import "ZFNoramlViewController.h"

#define ListPageSize 10
#define kDeleteAlubumNotification @"kDeleteAlubumNotification"

@interface AlbumListController ()<MWPhotoBrowserDelegate>

@property (nonatomic) BOOL isSelectedAll;
@property (nonatomic, strong) NSMutableArray *selectedMessageArray;
@property (nonatomic, strong) NSDate *lastDate;
@property (nonatomic, strong) NSDate *beginTime;
@property (nonatomic, strong) NSDate *endTime;
@property (nonatomic) NSInteger totalPage;
@property (nonatomic) NSInteger currentIndex;

@property (nonatomic, strong) NSMutableArray *albumList;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSMutableArray *sections;
@property (nonatomic, strong) NSMutableArray *photos;

@property (nonatomic, weak) IBOutlet UIBarButtonItem *selectedAll;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *deleteSelected;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *editButton;


@property (strong, nonatomic) IBOutlet UIImageView *noMessageImageView;
@property (strong, nonatomic) IBOutlet UILabel *noMessagelabel;


@end

@implementation AlbumListController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    if(self.albumType == 1){
        self.title = @"云相册";
        self.noMessageImageView.image = [UIImage imageNamed:@"noPhoto"];
        self.noMessagelabel.text = @"暂无相册";
    }else{
        self.title = @"云视频";
        self.noMessageImageView.image = [UIImage imageNamed:@"noVideo"];
        self.noMessagelabel.text = @"暂无视频";
    }
    
    if(!_albumList)
        _albumList = [NSMutableArray new];
    
    if(!_sections)
        _sections = [NSMutableArray new];
    
    if(!_selectedMessageArray)
        _selectedMessageArray = [NSMutableArray new];
    
    
    [self addHeaderRefresh];
    
    //获取过去3天的告警消息，开发者可以自己设置时间范围。
    _beginTime = [NSDate dateWithTimeIntervalSinceNow:-3600 * 24 * 7];
    _endTime = [NSDate date];
    
    if(!_dateFormatter){
        _dateFormatter = [[NSDateFormatter alloc] init];
    }
    
    // 适配 ios15 顶部多出的一点距离
    if (@available(iOS 15.0, *)) {
        self.tableView.sectionHeaderTopPadding = 0;
    }
       
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    if (self.tableView.allowsMultipleSelectionDuringEditing) {
        [self editTableView:self.navigationItem.rightBarButtonItem];
    }
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private
// Refresh
-(void)refreshPhotos{
    self.photos = [NSMutableArray new];
    for (int i = 0; i < self.albumList.count; i++) {
        AlbumCellInfo * album = self.albumList[i];
        NSString * pic = album.albumImageurl;
        NSURL * picUrl = [NSURL URLWithString:pic];
        [self.photos addObject:[MWPhoto photoWithURL:picUrl]];
    }
}

-(void)photoClick:(NSInteger)index{
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = NO;
    browser.displayNavArrows = YES;
    browser.displaySelectionButtons = NO;
    browser.zoomPhotosToFill = YES;
    browser.alwaysShowControls = YES;
    browser.enableGrid = YES;
    browser.startOnGrid = NO;
    // Present
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:browser];
    NSDictionary *dict = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    [nav.navigationBar setTitleTextAttributes:dict];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
    
    // Manipulate
    [browser showNextPhotoAnimated:YES];
    [browser showPreviousPhotoAnimated:YES];
    [browser setCurrentPhotoIndex:index];
}

#pragma mark - Photos Delegate
// 查看图片 Delegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser{
    return self.photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index{
    if (index < self.photos.count) {
        return [self.photos objectAtIndex:index];
    }
    return nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_sections.count == 1)
    {
        return _albumList.count;
    }
    else
    {
        if (section == 0) {
            return [_sections[1][@"index"] integerValue];
        }
        else if (section == _sections.count - 1)
        {
            return _albumList.count - [_sections[section][@"index"] integerValue];
        }
        else
        {
            return [_sections[section + 1][@"index"] integerValue] - [_sections[section][@"index"] integerValue];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Configure the cell...
    AlbumListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AlbumCell" forIndexPath:indexPath];
    cell.deviceSerial = self.deviceSerial;
    
    AlbumCellInfo * info = [_albumList dd_objectAtIndex:[[_sections dd_objectAtIndex:indexPath.section][@"index"] integerValue] + indexPath.row];
    [cell setAlbumInfo:info];
    
    if (tableView.allowsMultipleSelectionDuringEditing == YES) {
        if([_selectedMessageArray containsObject:info])
        {
            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
        else
        {
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger selecIndex = [[_sections dd_objectAtIndex:indexPath.section][@"index"] integerValue] + indexPath.row;
    AlbumCellInfo *info = [_albumList dd_objectAtIndex:selecIndex];
    
    if(tableView.allowsMultipleSelectionDuringEditing)
    {
        if ([self.selectedMessageArray containsObject:info])
        {
            [self.selectedMessageArray removeObject:info];
        }
        else
        {
            [self.selectedMessageArray addObject:info];
        }
        if(self.selectedMessageArray.count > 0)
        {
            self.deleteSelected.title = [NSString stringWithFormat:@"%@(%d)",@"删除",(int)[self.selectedMessageArray count]];
            self.deleteSelected.enabled = YES;
            
        }
        else
        {
            self.deleteSelected.title = @"删除";
            self.deleteSelected.enabled = NO;
            
        }
        return;
    }
//    AlbumListCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if(info.albumType == 1){
        [self photoClick:selecIndex];
    }else{
        ZFNoramlViewController * playerVC = [ZFNoramlViewController new];
        playerVC.videoUrl = info.videoUrl;
        playerVC.kVideoCover = info.albumImageurl;
        [self.navigationController pushViewController:playerVC animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.allowsMultipleSelectionDuringEditing)
    {
        AlbumCellInfo *info = [_albumList dd_objectAtIndex:[[_sections dd_objectAtIndex:indexPath.section][@"index"] integerValue] + indexPath.row];
        if ([self.selectedMessageArray containsObject:info])
        {
            [self.selectedMessageArray removeObject:info];
        }
        else
        {
            [self.selectedMessageArray addObject:info];
        }
        if(self.selectedMessageArray.count > 0)
        {
            self.deleteSelected.title = [NSString stringWithFormat:@"%@(%d)",@"删除",(int)[self.selectedMessageArray count]];
            self.deleteSelected.enabled = YES;
        }
        else
        {
            self.deleteSelected.title = @"删除";
            self.deleteSelected.enabled = NO;
            
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, self.view.bounds.size.width, 20)];
    NSString *key = [self.sections dd_objectAtIndex:section][@"date"];
    headerLabel.text = [NSString stringWithFormat:@"  %@",[self dateStringWithUserDefine:key]];
    headerLabel.font = [UIFont systemFontOfSize:14.0f];
    headerLabel.backgroundColor = [UIColor dd_hexStringToColor:@"0xf0f0f3"];
    return headerLabel;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        AlbumCellInfo *info = [_albumList dd_objectAtIndex:[[_sections dd_objectAtIndex:indexPath.section][@"index"] integerValue] + indexPath.row];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"正在删除，请稍候...";
        [JKAPIRequest deleteImage:self.deviceSerial ids:@[info.albumId] success:^(id data) {
            [self.albumList removeObject:info];
            [hud hide:YES];
            [self.sections removeAllObjects];
            [self tableViewDidReload:self.albumList];
            [self refreshPhotos];
            [[NSNotificationCenter defaultCenter] postNotificationName:kDeleteAlubumNotification object:nil];
        } error:^(NSError *error) {
            [hud hide:YES];
        }];
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/
 
#pragma mark - MJRefresh Methods

- (void)addHeaderRefresh
{
    __weak typeof(self) weakSelf = self;
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.currentIndex = 1;
        [weakSelf.noMessageImageView removeFromSuperview];
        [weakSelf.noMessagelabel removeFromSuperview];
        
        [JKAPIRequest requestImageList:self.deviceSerial pageSize:ListPageSize type:self.albumType pageNum:weakSelf.currentIndex success:^(id data) {
            [weakSelf.albumList removeAllObjects];
            NSDictionary * listData = data[@"result"];
            weakSelf.totalPage = [listData[@"totalPageNum"] integerValue];
            weakSelf.currentIndex = [listData[@"curPage"] integerValue];
            
            NSArray *list = listData[@"list"];
            NSMutableArray * albumList = [NSMutableArray new];
            
            for (int i = 0; i < list.count; i++) {
                NSDictionary * album = list[i];
                [albumList addObject:[AlbumCellInfo alumWithDict:album]];
            }
            [weakSelf.albumList addObjectsFromArray:albumList];
            [self refreshPhotos];
            if(weakSelf.currentIndex < weakSelf.totalPage){
                [weakSelf addFooter];
            }
            [weakSelf tableViewDidReload:albumList];
            [weakSelf.tableView.header endRefreshing];
            if(weakSelf.albumList.count > 0)
                weakSelf.navigationItem.rightBarButtonItem = self.editButton;
            
        } error:^(NSError *error) {
            [weakSelf.tableView.header endRefreshing];
        }];
    }];
    self.tableView.header.automaticallyChangeAlpha = YES;
    [self.tableView.header beginRefreshing];
}

- (void)addFooter
{
    __weak typeof(self) weakSelf = self;
    self.tableView.footer  = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [JKAPIRequest requestImageList:self.deviceSerial pageSize:ListPageSize type:self.albumType pageNum:weakSelf.currentIndex+1 success:^(id data) {
            
            NSDictionary * listData = data[@"result"];
            weakSelf.currentIndex = [listData[@"curPage"] integerValue];
            NSArray *list = listData[@"list"];
            
            if(!list || list.count <= 0){
                [weakSelf.tableView.footer endRefreshing];
                weakSelf.tableView.footer.hidden = YES;
                return;
            }
            
            NSMutableArray * albumList = [NSMutableArray new];
            for (int i = 0; i < list.count; i++) {
                NSDictionary * album = list[i];
                [albumList addObject:[AlbumCellInfo alumWithDict:album]];
            }
            
            [weakSelf.albumList addObjectsFromArray:albumList];
            [self refreshPhotos];
            [weakSelf tableViewDidReload:albumList];
            [weakSelf.tableView.footer endRefreshing];
            
            if(weakSelf.currentIndex >= weakSelf.totalPage){
                weakSelf.tableView.footer.hidden = YES;
            }
        } error:^(NSError *error) {

        }];
    }];
}

#pragma mark - Action Methods

- (void)tableViewDidReload:(NSArray *)albumList
{
    if(albumList.count == 0)
    {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.tableView reloadData];
        [self.tableView addSubview:self.noMessageImageView];
        [self.tableView addSubview:self.noMessagelabel];
        
        self.noMessageImageView.frame = CGRectMake((self.tableView.bounds.size.width - 94)/2.0, self.tableView.center.y - 150, 94, 94);
        self.noMessagelabel.frame = CGRectMake((self.tableView.frame.size.width - 100)/2.0, self.noMessageImageView.frame.origin.y + self.noMessageImageView.frame.size.height + 10, 100, 20.0);
        return;
    }
    if(self.currentIndex == 1)
    {
        [self.sections removeAllObjects];
        [self.tableView.footer endRefreshing];
        self.lastDate = [albumList[0] albumCreatTime];
        self.dateFormatter.dateFormat = @"yyyy-MM-dd";
        NSString *key = [self.dateFormatter stringFromDate:self.lastDate];
        NSDictionary *dict = @{@"index":@0, @"date":key};
        [self.sections addObject:dict];
    }
    for (int i = 0; i < albumList.count; i++) {
        AlbumCellInfo *info = [albumList dd_objectAtIndex:i];
        if(![info.albumCreatTime isSameToDate:self.lastDate])
        {
            NSInteger index = [self.albumList indexOfObject:info];
            self.dateFormatter.dateFormat = @"yyyy-MM-dd";
            NSString *key = [self.dateFormatter stringFromDate:info.albumCreatTime];
            NSDictionary *dict = @{@"index":@(index),@"date":key};
            [self.sections addObject:dict];
        }
        self.lastDate = info.albumCreatTime;
    }
    [self.tableView reloadData];
}

- (IBAction)editTableView:(id)sender
{
    if(self.tableView.editing){
        self.tableView.allowsMultipleSelectionDuringEditing = NO;
        [self.tableView setEditing:NO animated:YES];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editTableView:)];
        [self.navigationController setToolbarHidden:YES animated:YES];
        self.deleteSelected.title = @"删除";
        self.deleteSelected.enabled = NO;
        
        [self.selectedMessageArray removeAllObjects];
        self.tableView.header.hidden = NO;
        if(self.currentIndex < self.totalPage)
            self.tableView.footer.hidden = NO;
    }else{
        self.tableView.allowsMultipleSelectionDuringEditing = YES;
        [self.tableView setEditing:YES animated:YES];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(editTableView:)];
        [self.navigationController setToolbarHidden:NO animated:YES];
        self.tableView.header.hidden = YES;
        self.tableView.footer.hidden = YES;
        self.selectedAll.enabled = YES;
        self.deleteSelected.enabled = NO;
        
        if (self.albumList.count > 10)
        {
            self.selectedAll.enabled = NO;
        }
    }
}

- (IBAction)selectedAll:(id)sender
{
    _isSelectedAll = !_isSelectedAll;
    if(_isSelectedAll)
    {
        [self.selectedMessageArray removeAllObjects];
        [self.selectedMessageArray addObjectsFromArray:self.albumList];
        self.deleteSelected.title = [NSString stringWithFormat:@"%@(%d)",@"删除",(int)[self.selectedMessageArray count]];
        self.deleteSelected.enabled = YES;
        
    }
    else
    {
        self.deleteSelected.title = @"删除";
        
        [self.selectedMessageArray removeAllObjects];
        self.deleteSelected.enabled = NO;
        
    }
    [self.tableView reloadData];
    self.tableView.footer.hidden = YES;
}

- (IBAction)deleteMessage:(id)sender
{
//    if(self.selectedMessageArray.count > 10)
//    {
//        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
//        hud.mode = MBProgressHUDModeText;
//        hud.userInteractionEnabled = NO;
//        hud.labelText = @"数量不能超过10";
//        [hud show:YES];
//        [hud hide:YES afterDelay:1.0f];
//        return;
//    }
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"确定删除？" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *useCamera = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        __weak MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"正在删除，请稍候...";
        
        NSMutableArray *ids = [NSMutableArray new];
        for (int i = 0; i < self.selectedMessageArray.count; i++) {
            AlbumCellInfo *info = [self.selectedMessageArray dd_objectAtIndex:i];
            [ids addObject:info.albumId];
        }
        
        [JKAPIRequest deleteImage:self.deviceSerial ids:ids success:^(id data) {
            [hud hide:YES];
            [self.albumList removeObjectsInArray:self.selectedMessageArray];
            [self editTableView:self.navigationItem.rightBarButtonItem];
            if(self.albumList.count > 0){
                [self.sections removeAllObjects];
                [self tableViewDidReload:self.albumList];
            }else{
                [self.tableView.header beginRefreshing];
            }
            [self refreshPhotos];
            [[NSNotificationCenter defaultCenter] postNotificationName:kDeleteAlubumNotification object:nil];
        } error:^(NSError *error) {
            hud.labelText = error.localizedDescription;
            hud.mode = MBProgressHUDModeText;
            [hud hide:YES afterDelay:1.2];
        }];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:useCamera];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


- (NSString *)dateStringWithUserDefine:(NSString *)dateString
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc ] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [formatter dateFromString:dateString];
    
    NSArray *weekDays = @[@"周日",
                          @"周一",
                          @"周二",
                          @"周三",
                          @"周四",
                          @"周五",
                          @"周六"];
    
    if ([date isThisYear])
    {
        if ([date isToday])
        {
            return @"今天";
        }
        else if ([date weekday] > 0 && [date weekday] <= 7)
        {
            return [NSString stringWithFormat:@"%ld-%ld %@", (long)[date month], (long)[date day], weekDays[[date weekday]-1]];
        }
    }
    else
    {
        if ([date weekday] > 0 && [date weekday] <= 7)
        {
            return [NSString stringWithFormat:@"%@ %@", dateString, weekDays[[date weekday]-1]];
        }
    }
    
    return nil;
}

@end

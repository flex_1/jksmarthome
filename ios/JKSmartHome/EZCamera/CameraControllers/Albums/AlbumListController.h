//
//  EZMessageListViewController.h
//  EZOpenSDKDemo
//
//  Created by DeJohn Dong on 15/11/3.
//  Copyright © 2015年 Ezviz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EZDeviceInfo.h"



@interface AlbumListController : UITableViewController

@property (nonatomic, copy) NSString * deviceSerial;
@property (nonatomic)NSInteger albumType; 

@end

//
//  CameraMainController.h
//  JKSmartHome
//
//  Created by jk_ios on 2019/12/24.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EZDeviceInfo.h"

@interface CameraMainController : UIViewController

@property (nonatomic, strong) EZDeviceInfo *deviceInfo;

@property (nonatomic,copy) NSString *appKey;
@property (nonatomic,copy) NSString *accessToken;
@property (nonatomic,copy) NSString *urlStr;
@property (nonatomic,copy) NSString *serialStr;
@property (nonatomic,copy) NSString *cameraName;
@property (nonatomic, assign) BOOL isDark;
@property (nonatomic, assign) NSInteger cameraNo;

@end


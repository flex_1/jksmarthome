//
//  EZMessagePhotoViewController.m
//  EZOpenSDKDemo
//
//  Created by DeJohn Dong on 15/11/16.
//  Copyright © 2015年 Ezviz. All rights reserved.
//

#import "EZMessagePhotoViewController.h"
#import "EZMessagePlaybackViewController.h"

@interface EZMessagePhotoViewController ()<MWPhotoBrowserDelegate>

@property (nonatomic, weak) IBOutlet UIView *messageDetailInfoView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (nonatomic, weak) IBOutlet UILabel *contentLabel;
@property (nonatomic, weak) IBOutlet UIButton *recordButton;

@end

@implementation EZMessagePhotoViewController

- (void)viewDidLoad {
    
    self.delegate = self;
    self.displayActionButton = NO;
    self.displaySelectionButtons = NO;
    self.alwaysShowControls = YES;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view bringSubviewToFront:self.messageDetailInfoView];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy/MM/dd HH:mm:ss";
    self.timeLabel.text = [formatter stringFromDate:self.info.alarmStartTime];
    self.contentLabel.text = [NSString stringWithFormat:@"%@:%@",@"来自",self.info.alarmName];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    if (@available(iOS 15.0, *)) {
        UINavigationBarAppearance *appearance = [[UINavigationBarAppearance alloc] init];
        [appearance configureWithDefaultBackground];
        
        appearance.backgroundColor = UIColor.blackColor;
        //设置导航条标题颜色
        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
        [attributes setValue:UIColor.whiteColor forKey:NSForegroundColorAttributeName];
        appearance.titleTextAttributes = attributes;
     
        self.navigationController.navigationBar.standardAppearance = appearance;
        self.navigationController.navigationBar.scrollEdgeAppearance = appearance;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
    if (@available(iOS 15.0, *)) {
        UINavigationBarAppearance *appearance = [[UINavigationBarAppearance alloc] init];
        [appearance configureWithOpaqueBackground];
        
        //设置导航条标题颜色
        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
        [attributes setValue:UIColor.blackColor forKey:NSForegroundColorAttributeName];
        appearance.titleTextAttributes = attributes;
     
        self.navigationController.navigationBar.standardAppearance = appearance;
        self.navigationController.navigationBar.scrollEdgeAppearance = appearance;
    }
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    EZAlarmInfo *info = sender;
    
    EZMessagePlaybackViewController *messagePlaybackVC = [segue destinationViewController];
    messagePlaybackVC.info = info;
    messagePlaybackVC.deviceSerial = self.deviceSerial;    
}

#pragma mark - MWPhotoBrowserDelegate Methods

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return 1;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    MWPhoto *photo = [[MWPhoto alloc] initWithImage:_image];
    return photo;
}


- (void)updateNavigation
{
    self.title = @"消息详情";
}

- (IBAction)go2Next:(id)sender
{
    [self performSegueWithIdentifier:@"go2MessagePlayback" sender:self.info];
}

@end

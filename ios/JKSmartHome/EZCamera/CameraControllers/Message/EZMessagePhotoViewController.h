//
//  EZMessagePhotoViewController.h
//  EZOpenSDKDemo
//
//  Created by DeJohn Dong on 15/11/16.
//  Copyright © 2015年 Ezviz. All rights reserved.
//

#import "MWPhotoBrowser.h"
#import "EZAlarmInfo.h"
#import "EZDeviceInfo.h"

@interface EZMessagePhotoViewController : MWPhotoBrowser

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, copy) NSString * deviceSerial;
@property (nonatomic, strong) EZAlarmInfo *info;

@end

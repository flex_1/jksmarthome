//
//  EZMessageListViewController.m
//  EZOpenSDKDemo
//
//  Created by DeJohn Dong on 15/11/3.
//  Copyright © 2015年 Ezviz. All rights reserved.
//

#import "EZMessageListViewController.h"

#import "MJRefresh.h"
#import "DDKit.h"
#import "MessageListCell.h"
#import "Masonry.h"
#import "MBProgressHUD.h"
#import "EZMessagePhotoViewController.h"
#import "NSDate-Utilities.h"
#import "EZOpenSDK.h"
#import "JKAPIRequest.h"

#define EZOPENSDK [EZOpenSDK class]
#define EZMessageListPageSize 10

@interface EZMessageListViewController ()

@property (nonatomic) BOOL isSelectedAll;
@property (nonatomic, strong) NSMutableArray *selectedMessageArray;
@property (nonatomic, strong) NSDate *lastDate;
@property (nonatomic, strong) NSDate *beginTime;
@property (nonatomic, strong) NSDate *endTime;
@property (nonatomic) NSInteger totalPage;
@property (nonatomic) NSInteger currentIndex;
@property (nonatomic, strong) NSMutableArray *messageList;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSMutableArray *sections;
@property (nonatomic) NSInteger lastIndex;
@property (nonatomic, weak) IBOutlet UIImageView *noMessage;
@property (nonatomic, weak) IBOutlet UILabel *noMessageLabel;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *selectedAll;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *deleteSelected;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *readAll;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *editButton;


@end

@implementation EZMessageListViewController

- (void)dealloc
{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = @"消息";
    [self requestUnredMessage];
    
    if(!_messageList)
        _messageList = [NSMutableArray new];
    
    if(!_sections)
        _sections = [NSMutableArray new];
    
    if(!_selectedMessageArray)
        _selectedMessageArray = [NSMutableArray new];
    
    [self addHeaderRefresh];
    
    //获取过去3天的告警消息，开发者可以自己设置时间范围。
    _beginTime = [NSDate dateWithTimeIntervalSinceNow:-3600 * 24 * 7];
    _endTime = [NSDate date];
    
    if(!_dateFormatter){
        _dateFormatter = [[NSDateFormatter alloc] init];
    }
    
    // 适配 ios15 顶部多出的一点距离
    if (@available(iOS 15.0, *)) {
        self.tableView.sectionHeaderTopPadding = 0;
    }
    
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)viewWillDisappear:(BOOL)animated {
    if (self.tableView.allowsMultipleSelectionDuringEditing) {
        [self editTableView:self.navigationItem.rightBarButtonItem];
    }
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private
//检查消息未读数量
- (void)requestUnredMessage{
  [JKAPIRequest requestUnredMessage:self.deviceSerial success:^(id data) {
      self.title = [NSString stringWithFormat:@"%@（%@）",@"消息", data[@"result"] ];
  } error:nil];
}
  
- (NSDate *)getDateWithTimeStr:(NSString *)str{
    NSTimeInterval time=[str doubleValue]/1000;//传入的时间戳str如果是精确到毫秒的记得要/1000
    NSDate *detailDate=[NSDate dateWithTimeIntervalSince1970:time];
    return detailDate;
}

//将字典转换成对象
-(EZAlarmInfo *)convertDictToAlarInfo:(NSDictionary *)message{
    
    EZAlarmInfo *info = [EZAlarmInfo new];
    
    info.alarmId = message[@"alarmId"];
    info.deviceSerial = message[@"devSerial"];
    info.alarmName = message[@"channelName"];
    info.alarmPicUrl = message[@"url"];
    info.isEncrypt = [message[@"crypt"] boolValue];
    info.isRead = [message[@"isRead"] boolValue];
    info.deviceName = message[@"deviceName"];
    info.category = message[@"category"];
    info.customerType = message[@"customerType"];
    info.customerInfo = message[@"customerInfo"];
    
    if(message[@"alarmStartTime"]){
        NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat= @"yyyy-MM-dd HH:mm:ss";
        NSDate * alarmStartTime = [formatter dateFromString:message[@"alarmStartTime"]];
        info.alarmStartTime = alarmStartTime;
    }
        
    info.cameraNo = [message[@"channel"] integerValue];
    info.alarmType = 1;
    info.recState = [message[@"recState"] integerValue];
    info.delayTime = [message[@"delayTime"] integerValue];
    
    return  info;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_sections.count == 1)
    {
        return _messageList.count;
    }
    else
    {
        if (section == 0) {
            return [_sections[1][@"index"] integerValue];
        }
        else if (section == _sections.count - 1)
        {
            return _messageList.count - [_sections[section][@"index"] integerValue];
        }
        else
        {
            return [_sections[section + 1][@"index"] integerValue] - [_sections[section][@"index"] integerValue];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Configure the cell...
    MessageListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageCell" forIndexPath:indexPath];
    cell.deviceSerial = self.deviceSerial;
    
    EZAlarmInfo *info = [_messageList dd_objectAtIndex:[[_sections dd_objectAtIndex:indexPath.section][@"index"] integerValue] + indexPath.row];
    [cell setAlarmInfo:info];
    if (tableView.allowsMultipleSelectionDuringEditing == YES) {
        if([_selectedMessageArray containsObject:info])
        {
            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
        else
        {
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EZAlarmInfo *info = [_messageList dd_objectAtIndex:[[_sections dd_objectAtIndex:indexPath.section][@"index"] integerValue] + indexPath.row];
    
    if(tableView.allowsMultipleSelectionDuringEditing)
    {
        if ([self.selectedMessageArray containsObject:info])
        {
            [self.selectedMessageArray removeObject:info];
        }
        else
        {
            [self.selectedMessageArray addObject:info];
        }
        if(self.selectedMessageArray.count > 0)
        {
            self.deleteSelected.title = [NSString stringWithFormat:@"%@(%d)",@"删除",(int)[self.selectedMessageArray count]];
            self.deleteSelected.enabled = YES;
            self.readAll.title = [NSString stringWithFormat:@"%@(%d)",@"标记已读",(int)[self.selectedMessageArray count]];
            self.readAll.enabled = YES;
        }
        else
        {
            self.deleteSelected.title = @"删除";
            self.deleteSelected.enabled = NO;
            self.readAll.title = @"标记已读";
            self.readAll.enabled = NO;
        }
        return;
    }
    MessageListCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"go2MessagePhoto" sender:@{@"image":cell.actionImageView.image?:[UIImage new],@"alarmInfo":info}];
    [self requestUnredMessage];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.allowsMultipleSelectionDuringEditing)
    {
        EZAlarmInfo *info = [_messageList dd_objectAtIndex:[[_sections dd_objectAtIndex:indexPath.section][@"index"] integerValue] + indexPath.row];
        if ([self.selectedMessageArray containsObject:info])
        {
            [self.selectedMessageArray removeObject:info];
        }
        else
        {
            [self.selectedMessageArray addObject:info];
        }
        if(self.selectedMessageArray.count > 0)
        {
            self.deleteSelected.title = [NSString stringWithFormat:@"%@(%d)",@"删除",(int)[self.selectedMessageArray count]];
            self.deleteSelected.enabled = YES;
            self.readAll.title = [NSString stringWithFormat:@"%@(%d)",@"标记已读",(int)[self.selectedMessageArray count]];
            self.readAll.enabled = YES;
        }
        else
        {
            self.deleteSelected.title = @"删除";
            self.deleteSelected.enabled = NO;
            self.readAll.title = @"标记已读";
            self.readAll.enabled = NO;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, self.view.bounds.size.width, 20)];
    NSString *key = [self.sections dd_objectAtIndex:section][@"date"];
    headerLabel.text = [NSString stringWithFormat:@"  %@",[self dateStringWithUserDefine:key]];
    headerLabel.font = [UIFont systemFontOfSize:14.0f];
    headerLabel.backgroundColor = [UIColor dd_hexStringToColor:@"0xf0f0f3"];
    return headerLabel;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        EZAlarmInfo *info = [_messageList dd_objectAtIndex:[[_sections dd_objectAtIndex:indexPath.section][@"index"] integerValue] + indexPath.row];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"正在删除，请稍候...";
        [JKAPIRequest deleteMessage:self.deviceSerial alarmIds:@[info.alarmId] success:^(id data) {
            [self.messageList removeObject:info];
            [hud hide:YES];
            [self.sections removeAllObjects];
            [self tableViewDidReload:self.messageList];
            [self requestUnredMessage];
        } error:^(NSError *error) {
            
        }];
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSDictionary *dict = sender;
    EZAlarmInfo *info = dict[@"alarmInfo"];
    if(!info.isRead){
        [JKAPIRequest setAlarmStatus:self.deviceSerial alarmIds:@[info.alarmId] success:^(id data) {
            info.isRead = YES;
            [self.tableView reloadData];
        } error:^(NSError *error) {
            
        }];
    }
    EZMessagePhotoViewController *nextVC = [segue destinationViewController];
    nextVC.image = dict[@"image"];
    nextVC.info = info;
    nextVC.deviceSerial = self.deviceSerial;
}

#pragma mark - MJRefresh Methods

- (void)addHeaderRefresh
{
    __weak typeof(self) weakSelf = self;
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.currentIndex = 1;
        [weakSelf.noMessage removeFromSuperview];
        [weakSelf.noMessageLabel removeFromSuperview];
        
        [JKAPIRequest requestMessageList:self.deviceSerial page:weakSelf.currentIndex success:^(id data) {
        
            NSDictionary * listData = data[@"result"];
            weakSelf.totalPage = [listData[@"totalPageNum"] integerValue];
            weakSelf.currentIndex = [listData[@"curPage"] integerValue];
            
            NSArray *list = listData[@"list"];
            NSMutableArray * alarmList = [NSMutableArray new];
            for (int i = 0; i < list.count; i++) {
                NSDictionary * message = list[i];
                [alarmList addObject:[self convertDictToAlarInfo:message]];
            }

            [weakSelf.messageList removeAllObjects];
            [weakSelf.messageList addObjectsFromArray:alarmList];
          
            if(weakSelf.currentIndex < weakSelf.totalPage){
                [weakSelf addFooter];
            }
            [weakSelf tableViewDidReload:alarmList];
            [weakSelf.tableView.header endRefreshing];
            if(weakSelf.messageList.count > 0)
                weakSelf.navigationItem.rightBarButtonItem = self.editButton;
        } error:^(NSError *error) {
            [weakSelf.tableView.header endRefreshing];
        }];
    }];
    self.tableView.header.automaticallyChangeAlpha = YES;
    [self.tableView.header beginRefreshing];
}

- (void)addFooter
{
    __weak typeof(self) weakSelf = self;
    self.tableView.footer  = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [JKAPIRequest requestMessageList:self.deviceSerial page:weakSelf.currentIndex+1 success:^(id data) {
            
            NSDictionary * listData = data[@"result"];
            weakSelf.currentIndex = [listData[@"curPage"] integerValue];
            NSArray *list = listData[@"list"];
            
            if(!list || list.count <= 0){
                [weakSelf.tableView.footer endRefreshing];
                weakSelf.tableView.footer.hidden = YES;
                return;
            }
            
            NSMutableArray * alarmList = [NSMutableArray new];
            for (int i = 0; i < list.count; i++) {
                NSDictionary * message = list[i];
                [alarmList addObject:[self convertDictToAlarInfo:message]];
            }
            [weakSelf.messageList addObjectsFromArray:alarmList];
            [weakSelf tableViewDidReload:alarmList];
            [weakSelf.tableView.footer endRefreshing];
            
            if(weakSelf.currentIndex >= weakSelf.totalPage){
                weakSelf.tableView.footer.hidden = YES;
            }
        } error:^(NSError *error) {

        }];
    }];
}

#pragma mark - Action Methods

- (void)tableViewDidReload:(NSArray *)messageList
{
    if(messageList.count == 0)
    {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.tableView reloadData];
        [self.tableView addSubview:self.noMessage];
        [self.tableView addSubview:self.noMessageLabel];
        
        self.noMessage.frame = CGRectMake((self.tableView.bounds.size.width - 94)/2.0, self.tableView.center.y - 150, 94, 94);
        self.noMessageLabel.frame = CGRectMake((self.tableView.frame.size.width - 100)/2.0, self.noMessage.frame.origin.y + self.noMessage.frame.size.height + 10, 100, 20.0);
        return;
    }
    if(self.currentIndex == 1)
    {
        [self.sections removeAllObjects];
        [self.tableView.footer endRefreshing];
        self.lastDate = [messageList[0] alarmStartTime];
        self.dateFormatter.dateFormat = @"yyyy-MM-dd";
        NSString *key = [self.dateFormatter stringFromDate:self.lastDate];
        NSDictionary *dict = @{@"index":@0, @"date":key};
        [self.sections addObject:dict];
    }
    for (int i = 0; i < messageList.count; i++) {
        EZAlarmInfo *info = [messageList dd_objectAtIndex:i];
        if(![info.alarmStartTime isSameToDate:self.lastDate])
        {
            NSInteger index = [self.messageList indexOfObject:info];
            self.dateFormatter.dateFormat = @"yyyy-MM-dd";
            NSString *key = [self.dateFormatter stringFromDate:info.alarmStartTime];
            NSDictionary *dict = @{@"index":@(index),@"date":key};
            [self.sections addObject:dict];
        }
        self.lastDate = info.alarmStartTime;
    }
    [self.tableView reloadData];
}

- (IBAction)editTableView:(id)sender
{
    if(self.tableView.editing){
        self.tableView.allowsMultipleSelectionDuringEditing = NO;
        [self.tableView setEditing:NO animated:YES];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editTableView:)];
        [self.navigationController setToolbarHidden:YES animated:YES];
        self.deleteSelected.title = @"删除";
        self.deleteSelected.enabled = NO;
        self.readAll.title = @"标记已读";
        self.readAll.enabled = NO;
        [self.selectedMessageArray removeAllObjects];
        self.tableView.header.hidden = NO;
        if(self.currentIndex < self.totalPage)
            self.tableView.footer.hidden = NO;
    }else{
        self.tableView.allowsMultipleSelectionDuringEditing = YES;
        [self.tableView setEditing:YES animated:YES];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(editTableView:)];
        [self.navigationController setToolbarHidden:NO animated:YES];
        self.tableView.header.hidden = YES;
        self.tableView.footer.hidden = YES;
        self.selectedAll.enabled = YES;
        self.deleteSelected.enabled = NO;
        self.readAll.enabled = NO;
        if (self.messageList.count > 10)
        {
            self.selectedAll.enabled = NO;
        }
    }
}

- (IBAction)selectedAll:(id)sender
{
    _isSelectedAll = !_isSelectedAll;
    if(_isSelectedAll)
    {
        [self.selectedMessageArray removeAllObjects];
        [self.selectedMessageArray addObjectsFromArray:self.messageList];
        self.deleteSelected.title = [NSString stringWithFormat:@"%@(%d)",@"删除",(int)[self.selectedMessageArray count]];
        self.deleteSelected.enabled = YES;
        self.readAll.title = [NSString stringWithFormat:@"%@(%d)",@"标记已读",(int)[self.selectedMessageArray count]];
        self.readAll.enabled = YES;
    }
    else
    {
        self.deleteSelected.title = @"删除";
        self.readAll.title = @"标记已读";
        [self.selectedMessageArray removeAllObjects];
        self.deleteSelected.enabled = NO;
        self.readAll.enabled = NO;
    }
    [self.tableView reloadData];
    self.tableView.footer.hidden = YES;
}

- (IBAction)deleteMessage:(id)sender
{
//    if(self.selectedMessageArray.count > 10)
//    {
//        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
//        hud.mode = MBProgressHUDModeText;
//        hud.userInteractionEnabled = NO;
//        hud.labelText = @"数量不能超过10";
//        [hud show:YES];
//        [hud hide:YES afterDelay:1.0f];
//        return;
//    }
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"确定删除？" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *useCamera = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        __weak MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"正在删除，请稍候...";
        NSMutableArray *alarmIds = [NSMutableArray new];
        for (int i = 0; i < self.selectedMessageArray.count; i++) {
            EZAlarmInfo *info = [self.selectedMessageArray dd_objectAtIndex:i];
            [alarmIds addObject:info.alarmId];
        }
        [JKAPIRequest deleteMessage:self.deviceSerial alarmIds:alarmIds success:^(id data) {
            [hud hide:YES];
            [self.messageList removeObjectsInArray:self.selectedMessageArray];
            [self editTableView:self.navigationItem.rightBarButtonItem];
            if(self.messageList.count > 0){
                [self.sections removeAllObjects];
                [self tableViewDidReload:self.messageList];
            }else{
                [self.tableView.header beginRefreshing];
            }
            [self requestUnredMessage];
        } error:^(NSError *error) {
            hud.labelText = error.localizedDescription;
            hud.mode = MBProgressHUDModeText;
            [hud hide:YES afterDelay:1.2];
        }];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:useCamera];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)readMessage:(id)sender
{
//    if(self.selectedMessageArray.count > 10)
//    {
//        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
//        hud.mode = MBProgressHUDModeText;
//        hud.userInteractionEnabled = NO;
//        hud.labelText = @"数量不能超过10";
//        [hud show:YES];
//        [hud hide:YES afterDelay:1.0f];
//        return;
//    }
    __weak MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = NO;
    hud.labelText = @"正在设置已读，请稍候...";
    NSMutableArray *alarmIds = [NSMutableArray new];
    for (int i = 0; i < self.selectedMessageArray.count; i++) {
        EZAlarmInfo *info = [self.selectedMessageArray dd_objectAtIndex:i];
        [alarmIds addObject:info.alarmId];
    }
    
    [JKAPIRequest setAlarmStatus:self.deviceSerial alarmIds:alarmIds success:^(id data) {
        [hud hide:YES];
        for (int i = 0; i < self.selectedMessageArray.count; i++) {
            EZAlarmInfo *info = [self.selectedMessageArray dd_objectAtIndex:i];
            info.isRead = YES;
        }
        [self editTableView:self.navigationItem.rightBarButtonItem];
        [self.tableView reloadData];
        [self requestUnredMessage];

    } error:^(NSError *error) {
        hud.labelText = error.localizedDescription;
        hud.mode = MBProgressHUDModeText;
        [hud hide:YES afterDelay:1.2];
    }];
}

- (NSString *)dateStringWithUserDefine:(NSString *)dateString
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc ] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [formatter dateFromString:dateString];
    
    NSArray *weekDays = @[@"周日",
                          @"周一",
                          @"周二",
                          @"周三",
                          @"周四",
                          @"周五",
                          @"周六"];
    
    if ([date isThisYear])
    {
        if ([date isToday])
        {
            return @"今天";
        }
        else if ([date weekday] > 0 && [date weekday] <= 7)
        {
            return [NSString stringWithFormat:@"%ld-%ld %@", (long)[date month], (long)[date day], weekDays[[date weekday]-1]];
        }
    }
    else
    {
        if ([date weekday] > 0 && [date weekday] <= 7)
        {
            return [NSString stringWithFormat:@"%@ %@", dateString, weekDays[[date weekday]-1]];
        }
    }
    
    return nil;
}

@end

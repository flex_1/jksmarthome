//
//  MessageListCell.h
//  EZOpenSDKDemo
//
//  Created by DeJohn Dong on 15/11/3.
//  Copyright © 2015年 Ezviz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalKit.h"
#import "AlbumCellInfo.h"

@interface AlbumListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *albumImageView;
@property (weak, nonatomic) IBOutlet UILabel *albumTitle;
@property (weak, nonatomic) IBOutlet UILabel *albumDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *albumTimeLabel;


@property (nonatomic, copy) NSString *deviceSerial;

- (void)setAlbumInfo:(AlbumCellInfo *)info;

@end

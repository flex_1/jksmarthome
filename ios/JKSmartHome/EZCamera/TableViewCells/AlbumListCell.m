//
//  MessageListCell.m
//  EZOpenSDKDemo
//
//  Created by DeJohn Dong on 15/11/3.
//  Copyright © 2015年 Ezviz. All rights reserved.
//

#import "AlbumListCell.h"

#import "UIImageView+EzvizOpenSDK.h"
#import "UIImageView+AFNetworking.h"
#import "DDKit.h"
#import "EZOpenSDK.h"


@implementation AlbumListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state    
    
}


- (void)setAlbumInfo:(AlbumCellInfo *)info
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"HH:mm:ss";
    self.albumTimeLabel.text = [formatter stringFromDate:info.albumCreatTime];
    
    
    if(info.albumType == 1){
        NSMutableString * preString = [NSMutableString stringWithString:@"大小: "];
        NSString * sizeString = [NSString stringWithFormat:@"%ldkb",(long)info.size];
        self.albumDescriptionLabel.text = [preString stringByAppendingString:sizeString];
    }else{
        if(info.duration && ![info.duration isKindOfClass:[NSNull class]]){
            NSMutableString * preString = [NSMutableString stringWithString:@"时长: "];
            self.albumDescriptionLabel.text = [preString stringByAppendingString:info.duration];
        }
    }
    
    self.albumTitle.text = info.deviceName;
    
    __weak typeof(self) weakSelf = self;
    NSURL *url = [NSURL URLWithString:info.albumImageurl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";
    UIImage * placeHolderImage = [UIImage imageNamed:@"preview_screenshot_sel"];
    if(info.albumType == 2){
        placeHolderImage = [UIImage imageNamed:@"playback"];
    }
    [self.albumImageView setBackgroundColor:[UIColor dd_hexStringToColor:@"d9d9d9"]];
    [self.albumImageView ez_setImageWithURLRequest:request
                                   placeholderImage:nil
                                            success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSData * _Nonnull data) {
                                                weakSelf.albumImageView.image = [[UIImage alloc] initWithData:data];
                                            }
                                            failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                                                NSLog(@"error = %@",error);
                                            }];
    
}

@end

//
//  JKCameraRNUtils.m
//  JKSmartHome
//
//  Created by jk_ios on 2019/12/24.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "JKCameraRNUtils.h"
#import "CameraMainController.h"

@implementation JKCameraRNUtils

@synthesize bridge = _bridge;

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(openCameraView:(NSString*)serialStr appKey:(NSString*)appKey accessToken:(NSString*)accessToken cameraNo:(NSInteger)cameraNo cameraName:(NSString*)cameraName isDark:(BOOL)isDark){
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
      dispatch_async(dispatch_get_main_queue(), ^{
        UINavigationController * cameraNavigationController = [[UIStoryboard storyboardWithName:@"Camera" bundle:nil] instantiateInitialViewController] ;
        CameraMainController *cameraVC = (CameraMainController *)cameraNavigationController.childViewControllers.firstObject;
        cameraNavigationController.modalPresentationStyle = 0;
        cameraVC.appKey = appKey;
        cameraVC.accessToken = accessToken;
        cameraVC.serialStr = serialStr;
        cameraVC.isDark = isDark;
        cameraVC.cameraNo = cameraNo;
        cameraVC.cameraName = cameraName;
        
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:cameraNavigationController animated:YES completion:nil];
        
      });
    });
}

@end

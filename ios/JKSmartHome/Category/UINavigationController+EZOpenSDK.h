//
//  UINavigationController+EZOpenSDK.h
//  EZOpenSDKDemo
//
//  Created by DeJohn Dong on 15/11/2.
//  Copyright © 2015年 Ezviz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (EZOpenSDK)

- (BOOL)ez_shouldAutorotate;

@end

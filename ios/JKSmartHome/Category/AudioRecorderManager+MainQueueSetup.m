//
//  AudioRecorderManager+MainQueueSetup.m
//  JKSmartHome
//
//  Created by jk_ios on 2019/9/29.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "AudioRecorderManager+MainQueueSetup.h"

@implementation AudioRecorderManager (MainQueueSetup)

+ (BOOL)requiresMainQueueSetup{
  return YES;
}

@end

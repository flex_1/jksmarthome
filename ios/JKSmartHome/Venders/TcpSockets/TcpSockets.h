#import "TcpSocketClient.h"

#import <React/RCTEventEmitter.h>
#import "CocoaAsyncSocket.h"

@interface TcpSockets : RCTEventEmitter<SocketClientDelegate>

@end
